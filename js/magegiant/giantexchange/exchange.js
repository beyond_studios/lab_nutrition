/**
 * Magegiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magegiant.com license that is
 * available through the world-wide-web at this URL:
 * https://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPoints
 * @copyright   Copyright (c) 2014 Magegiant (https://magegiant.com/)
 * @license     https://magegiant.com/license-agreement/
 */
/*Overwrite Giantpoints Core*/
MagegiantGiantPointsCore.updater._getActionFromUrl = function (url) {
    var matches = url.match(/giantpoints\/ajax\/([^\/]+)\//);
    if (!matches || !matches[1]) {
        matches = url.match(/giantexchange\/index\/([^\/]+)\//);
        if (!matches || !matches[1]) {
            return null;
        }
    }
    return matches[1];
}
/*GiantPointsExchange Class*/
GiantPointsExchange = Class.create();
GiantPointsExchange.prototype = {
    initialize: function (config) {
        this.formContainer = $(config.formContainer);
        this.ajaxLoader = $(config.ajaxLoader);
        this.sendFriendButton = $(config.sendFriendButton);
        this.sendFriendUrl = config.sendFriendUrl;
        this.msgContainer = $$(config.msgContainerSelector).first();
        this.successMessageBoxCssClass = config.successMessageBoxCssClass;
        this.errorMessageBoxCssClass = config.errorMessageBoxCssClass;
        this.config = config;
        this.observer();
    },
    observer: function () {
        this.sendFriendButton.observe('click', function () {
            this.sendPoint();

        }.bind(this));
    },
    sendPoint: function () {
        var me = this;
        var sendfriend_validator = new Validation(this.config.formContainer);
        var params = Form.serializeElements(this.formContainer.select('input, textarea'));
        var requestOptions = {
            onCreate: function () {
                me.removeMsg();
                me.sendFriendButton.disabled = true;
                me.ajaxLoader.show();
            },
            method: 'post',
            parameters: params,
            onComplete: function (transport) {
                me._onAjaxSendfriendActionCompleteFn(transport)
            }
        };
        MagegiantGiantPointsCore.updater.startRequest(this.sendFriendUrl, requestOptions);
    },
    _onAjaxSendfriendActionCompleteFn: function (transport) {
        this.sendFriendButton.disabled = false;
        this.ajaxLoader.hide();
        try {
            eval("var json = " + transport.responseText + " || {}");
        } catch (e) {
            this.showError(this.jsErrorMsg);
            return;
        }
        if (json.success) {
            var successMsg = this.jsSuccessMsg;
            if (("messages" in json) && ("length" in json.messages) && json.messages.length > 0) {
                successMsg = json.messages;
            }
            this.showSuccess(successMsg);
        } else {
            var errorMsg = this.jsErrorMsg;
            if (("messages" in json) && ("length" in json.messages) && json.messages.length > 0) {
                errorMsg = json.messages;
            }
            this.showError(errorMsg);
        }
    },
    showSuccess: function (msg, afterShowFn) {
        MagegiantGiantPointsCore.showMsg(msg, this.successMessageBoxCssClass, this.msgContainer);
        //add effect for height change
        var afterShowFn = afterShowFn || new Function();
        new Effect.Morph(this.msgContainer, {
            style: {
                height: this.msgContainer.down().getHeight() + 'px'
            },
            duration: 0.3,
            afterFinish: function (e) {
                afterShowFn();
            }
        });
    },
    showError: function (msg, afterShowFn) {
        MagegiantGiantPointsCore.showMsg(msg, this.errorMessageBoxCssClass, this.msgContainer);
        //add effect for height change
        var afterShowFn = afterShowFn || new Function();
        new Effect.Morph(this.msgContainer, {
            style: {
                height: this.msgContainer.down().getHeight() + 'px'
            },
            duration: 0.3,
            afterFinish: function (e) {
                afterShowFn();
            }
        });
    },
    removeMsg: function () {
        if (this.msgContainer.down()) {
            var me = this;
            new Effect.Morph(this.msgContainer, {
                style: {
                    height: 0 + 'px'
                },
                duration: 0,
                afterFinish: function (e) {
                    MagegiantGiantPointsCore.removeMsgFromBlock(me.msgContainer, me.errorMessageBoxCssClass);
                    MagegiantGiantPointsCore.removeMsgFromBlock(me.msgContainer, me.successMessageBoxCssClass);
                }
            });
        }
    }
}