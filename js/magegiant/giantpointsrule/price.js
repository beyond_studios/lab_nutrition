/**
 * Magegiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magegiant.com license that is
 * available through the world-wide-web at this URL:
 * https://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPoints
 * @copyright   Copyright (c) 2014 Magegiant (https://magegiant.com/)
 * @license     https://magegiant.com/license-agreement/
 */
/********** Giant Points Rule Price **********/
var GiantPointsRulePrice = Class.create();
GiantPointsRulePrice.prototype = {
    initialize: function (templateEl, listPrices, finalPrice, priceFormat) {
        this.templateEl = $(templateEl);
        this.listPrices = listPrices;

        this.generatedOldPrice = false;
        this.oldPrices = [];

        this.finalPrice = parseFloat(finalPrice);
        this.priceFormat = priceFormat;

        this.isShowed = false;
    },
    showPointPrices: function(points, ruleOption) {
        this.generateOldPrice();
        this.templateEl.down('.points').innerHTML = points;
        var discount = 0;
        if (typeof ruleOption.sliderOption == 'undefined') {
            return false;
        }
        var pointStep = parseInt(ruleOption.sliderOption.pointStep);
        if (pointStep < 1) {
            discount = parseFloat(ruleOption.stepDiscount);
        } else {
            var timesDiscount = Math.floor(points / pointStep);
            discount = parseFloat(ruleOption.stepDiscount) * timesDiscount;
        }
        maxDiscount = parseFloat(ruleOption.maxDiscount);
        if(maxDiscount > 0 && discount > maxDiscount){
            discount = maxDiscount;
        }

        var price = this.finalPrice - discount;
        if (this.finalPrice < discount) {
            price = 0;
        }
        this.templateEl.down('.price .price').innerHTML = formatCurrency(price, this.priceFormat);
        for (var i = 0; i < this.listPrices.length; i++) {
            var priceEl = this.listPrices[i];
            var oldPrice = this.oldPrices[i];
            priceEl.innerHTML = this.templateEl.innerHTML;
            if (priceEl.className == 'regular-price'
                || priceEl.className == 'full-product-price'
                ) {
                oldPrice.show();
            }
        }
        this.isShowed = true;
        return true;
    },
    clearPrices: function () {
        if (this.isShowed == false) {
            return false;
        }
        if (this.generatedOldPrice == false) {
            return false;
        }
        for (var i = 0; i < this.listPrices.length; i++) {
            var priceEl = this.listPrices[i];
            var oldPrice = this.oldPrices[i];
            priceEl.innerHTML = oldPrice.innerHTML;
            oldPrice.hide();
        }
        this.isShowed = false;
        return true;
    },
    generateOldPrice: function() {
        if (this.generatedOldPrice) {
            return false;
        }
        var oldPrices = [];
        for (var i = 0; i < this.listPrices.length; i++) {
            var priceEl = this.listPrices[i];
            var parentEl = Element.extend(priceEl.parentNode);
            var oldPrice = Element.clone(priceEl, 1);
            oldPrice.addClassName('old-price');
            parentEl.insertBefore(oldPrice, priceEl);
            oldPrice.hide();
            oldPrices.push(oldPrice);
        }
        this.oldPrices = oldPrices;
        this.generatedOldPrice = true;
        return true;
    }
}
