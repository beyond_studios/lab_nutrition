/**
 * Magegiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magegiant.com license that is
 * available through the world-wide-web at this URL:
 * https://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPoints
 * @copyright   Copyright (c) 2014 Magegiant (https://magegiant.com/)
 * @license     https://magegiant.com/license-agreement/
 */
MagegiantGiantPointsCheckboxSpending = Class.create();
MagegiantGiantPointsCheckboxSpending.prototype = {
    initialize: function (config) {
        this.containerSelector = $$(config.containerSelector).first();
        this.checkboxRules = $$(config.checkboxRules);
        // init urls
        this.applyPointsUrl = config.applyPointsUrl;
        this.initObserver();
    },
    initObserver: function () {
        var me = this;
        this.checkboxRules.each(function (el) {
            el.observe('change', function (evt) {
                me.applyPoint(evt);
            })
        })
    },
    applyPoint: function (evt) {
        var me = this;
        var el = evt.target;
        var ruleId = el.value;
        var is_used = el.checked;
        var params = 'rule_id=' + ruleId + '&is_used=' + is_used;
        this.changeActionFromUrl();
        var requestOptions = {
            method: 'post',
            parameters: params,
            postBody: params,
            onComplete: function (transport) {
                me.restoreActionFromUrl();
            }

        };
        MagegiantGiantPointsCore.updater.startRequest(this.applyPointsUrl, requestOptions);
    },
    changeActionFromUrl: function () {
        MagegiantGiantPointsCore.updater._getActionFromUrl = function (url) {
            var matches = url.match(/giantpointsrule\/checkout_spending\/([^\/]+)\//);
            if (!matches || !matches[1]) {
                return null;
            }
            return matches[1];
        }
    },
    restoreActionFromUrl: function () {
        MagegiantGiantPointsCore.updater._getActionFromUrl = function (url) {
            var matches = url.match(/giantpoints\/ajax\/([^\/]+)\//);
            if (!matches || !matches[1]) {
                return null;
            }
            return matches[1];
        }
    }
};
