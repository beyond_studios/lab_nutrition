/**
 * Add In Mage::
 *
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the EULA that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL: http://add-in-mage.com/support/presales/eula/
 *
 *
 * PROPRIETARY DATA
 * 
 * This file contains trade secret data which is the property of Add In Mage:: Ltd. 
 * This file is submitted to recipient in confidence.
 * Information and source code contained herein may not be used, copied, sold, distributed, 
 * sub-licensed, rented, leased or disclosed in whole or in part to anyone except as permitted by written
 * agreement signed by an officer of Add In Mage:: Ltd.
 * 
 * 
 * @category    AddInMage
 * @package     js
 * @copyright   Copyright (c) 2013 Add In Mage:: Ltd. (http://www.add-in-mage.com)
 * @license     http://add-in-mage.com/support/presales/eula/  End User License Agreement (EULA)
 * @author      Add In Mage:: Team <team@add-in-mage.com>
 */


var ExtraChooserHash = $H();

var ExtraConditionsForm = new Class.create();
ExtraConditionsForm.prototype = {
	    initialize : function(parent, newChildUrl){
	        this.newChildUrl  = newChildUrl;
	        this.updateElement = null;
	        this.chooserSelectedItems = ExtraChooserHash;
	        
	        var parentId = parent.getAttribute('id');
	       
	        var elems = $(parent).getElementsByClassName('step-loaded');
	        
	        for (var i=0; i<elems.length; i++) {
	            this.initParam(elems[i]);
	        }
	    },

	    initParam: function (container) {
	    	
	    	
	        container.rulesObject = this;	        
	        var elem = Element.down(container, '.data-chooser');
	        if (elem) {
	        	
	        	this.initInputField(elem);
	        	
	            var trig = elem.down('.rule-chooser-trigger');
	            if (trig) {
	                Event.observe(trig, 'click', this.toggleChooser.bind(this, container));
	            }
	            
	        }

	        var remove = Element.down(container, '.rule-param-remove');
	        if (remove) {
	            Event.observe(remove, 'click', this.removeRuleEntry.bind(this, container));
	        }
	    },

	    showChooserElement: function (chooser) {
	    	
	        this.chooserSelectedItems = ExtraChooserHash;
	        
	       	if (chooser.hasClassName('no-split')) {
	       		this.chooserSelectedItems.get(this.getInput()).set(this.updateElement.value, 1);
	       	} else {
	        	var values = this.updateElement.value.split(','), s = '';
	        	
	        	for (i=0; i<values.length; i++) {
	        		s = values[i].strip();
	        		if (s!='') {
	        			this.chooserSelectedItems.get(this.getInput()).set(s,1);
	        		}
	        	}
	      	}
	   
	        new Ajax.Request(chooser.getAttribute('url'), {
            evalScripts: true,
            parameters: {'form_key': FORM_KEY, 'selected[]':this.chooserSelectedItems.get(this.getInput()).keys() },
            onSuccess: function(transport) {	            	
                if (this._processSuccess(transport)) {	   
                	//chooser;
                	
                	chooser.update(transport.responseText);
                	widgetChooser.openDialog(chooser);
                	chooser.style.display = 'block';
                	this.showChooserLoaded(chooser);
                	
                }
            }.bind(this),
            onFailure: this._processFailure.bind(this)
        });
	    },

	    showChooserLoaded: function(chooser) {
	    	
        	widgetChooser.dialogWindow.showCenter();
        	widgetChooser.dialogWindow.updateWidth();
        	widgetChooser.dialogWindow.updateHeight();
	    },

	    showChooser: function (container, event) {
	        var chooser = container.up('.set-container');
	        if (!chooser) {
	            return;
	        }
	        chooser = chooser.down('.rule-chooser');
	        if (!chooser) {
	            return;
	        }
	        this.showChooserElement(chooser);
	    },

	    hideChooser: function (container, event) {
	        var chooser = container.up('.set-container');
	        if (!chooser) {
	            return;
	        }
	        chooser = chooser.down('.rule-chooser');
	        if (!chooser) {
	            return;
	        }
	        chooser.style.display = 'none';
	    },

	    toggleChooser: function (container, event) {
	    	
	        var chooser = container.next('.rule-chooser');
	        this.initInputField(container);
	        
	        if (!chooser) {
	            return;
	        }     
	        
	        if (chooser.style.display=='block') {
	            chooser.style.display = 'none';	           
	            this.cleanChooser(container, event);
	            
	        } else {
	            this.showChooserElement(chooser);
	       
	        }
	        
	    },

	    cleanChooser: function (container, event) {
	        var chooser = container.up('.set-container').down('.rule-chooser');
	        if (!chooser) {
	            return;
	        }
	        chooser.style.display = 'none';	      
	        chooser.innerHTML = '';
	    },

	    initInputField: function (container) {

	        var elem = Element.down(container, 'input.input-text');
	       
	        if (elem) {
	            elem.focus();
	            this.updateElement = elem;
	        }
	        
	    },

	    hideParamInputField: function (container, event) {
	    	this.initInputField(container);
	    	this.hideChooser(container, event);
	    },

	    _processSuccess : function(transport) {
	        if (transport.responseText.isJSON()) {
	            var response = transport.responseText.evalJSON();
	            if (response.error) {
	                alert(response.message);
	            }
	            if(response.ajaxExpired && response.ajaxRedirect) {
	                setLocation(response.ajaxRedirect);
	            }
	            return false;
	        }
	        return true;
	    },

	    _processFailure : function(transport) {
	        location.href = BASE_URL;
	    },


	    removeRuleEntry: function (container, event) {
	        var li = Element.up(container, 'li');
	        li.parentNode.removeChild(li);
	    },

	    chooserGridInit: function (grid) {
	        //grid.reloadParams = {'selected[]':this.chooserSelectedItems.keys()};
	    },

	    getInput: function () {
	    	return this.updateElement.readAttribute('id');
	    },
	    
	    chooserGridRowInit: function (grid, row) {
	    	this.initInputField($(grid.containerId).up('.set-container').down('.data-chooser'));	    	
	        if (!grid.reloadParams) {
	            grid.reloadParams = {'selected[]':this.chooserSelectedItems.get(this.getInput()).keys()};
	        }
	    },

	    chooserGridRowClick: function (grid, event) {
	    	
	        var trElement = Event.findElement(event, 'tr');
	        var isInput = Event.element(event).tagName == 'INPUT';
	        if (trElement) {
	            var checkbox = Element.select(trElement, 'input');
	            if (checkbox[0]) {
	                var checked = isInput ? checkbox[0].checked : !checkbox[0].checked;
	                grid.setCheckboxChecked(checkbox[0], checked);

	            }
	        }
	       
	    },

	    chooserGridCheckboxCheck: function (grid, element, checked) {
	    	
	    	tmp_chooser = $(grid.containerId).up('.rule-chooser').getAttribute('chooser');
	    	
	    	this.initInputField($(tmp_chooser).down('.data-chooser'));	 
	    
	        if (checked) {
	        	var isTH = element.up(1).tagName == 'TH';
	            if (!isTH) {	            	
	                this.chooserSelectedItems.get(this.getInput()).set(element.value,1);
	            }
	        } else {
	            this.chooserSelectedItems.get(this.getInput()).unset(element.value);
	        }
	        
	        grid.reloadParams = {'selected[]':this.chooserSelectedItems.get(this.getInput()).keys()};
	        
	        this.updateElement.value = this.chooserSelectedItems.get(this.getInput()).keys().join(', ');
	    }
};
