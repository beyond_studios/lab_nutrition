/* top menu */

function myFunctionSelect(myEl){
	var vfind = myEl.value;
	var pid_arr = $('pgrid_products').value.split(",");
	if(!myEl.checked){
		var aryWithoutFound = pid_arr.filter(function(value) { return value != vfind });
		str = aryWithoutFound.toString();
		str = str.replace(/(^[,\s]+)|([,\s]+$)/g, '');
	} else {
		str = $('pgrid_products').value + "," + vfind;
		str = str.replace(/(^[,\s]+)|([,\s]+$)/g, '');
	}
	$('pgrid_products').value = str;
}
	
function myCatSelect(myEl){
	var vfind = myEl.value;
	var cid_arr = $('pgrid_cats').value.split(",");
	if(!myEl.checked){
		var aryCatFound = cid_arr.filter(function(value) { return value != vfind });
		str = aryCatFound.toString();
		str = str.replace(/(^[,\s]+)|([,\s]+$)/g, '');
	} else {
		str = $('pgrid_cats').value + "," + vfind;
		str = str.replace(/(^[,\s]+)|([,\s]+$)/g, '');
	}
	$('pgrid_cats').value = str;
}

/* vertical menu */

function myFunctionSelectVer(myEl){
	var vfind = myEl.value;
	var pid_arr = $('ver_pgrid_products').value.split(",");
	if(!myEl.checked){
		var aryWithoutFound = pid_arr.filter(function(value) { return value != vfind });
		str = aryWithoutFound.toString();
		str = str.replace(/(^[,\s]+)|([,\s]+$)/g, '');
	} else {
		str = $('ver_pgrid_products').value + "," + vfind;
		str = str.replace(/(^[,\s]+)|([,\s]+$)/g, '');
	}
	$('ver_pgrid_products').value = str;
}
	
function myCatSelectVer(myEl){
	var vfind = myEl.value;
	var cid_arr = $('ver_pgrid_cats').value.split(",");
	if(!myEl.checked){
		var aryCatFound = cid_arr.filter(function(value) { return value != vfind });
		str = aryCatFound.toString();
		str = str.replace(/(^[,\s]+)|([,\s]+$)/g, '');
	} else {
		str = $('ver_pgrid_cats').value + "," + vfind;
		str = str.replace(/(^[,\s]+)|([,\s]+$)/g, '');
	}
	$('ver_pgrid_cats').value = str;
}