jQuery.fn.bstooltip = jQuery.fn.tooltip;
jQuery(document).ready(function($){
	'use strict';
	var header = $('.header'),
		header_bottom = $('.header-bottom'),
		window_height = $(window).height(),
		window_width = $(window).width();

	//Remove conflict between prototype and bootstrap functions 
    if (Prototype.BrowserFeatures.ElementExtensions) {
        var disablePrototypeJS = function (method, pluginsToDisable) {
            var handler = function (event) {
                event.target[method] = undefined;
                setTimeout(function () {
                    delete event.target[method];
                }, 0);
            };
            pluginsToDisable.each(function (plugin) { 
                jQuery(window).on(method + '.bs.' + plugin, handler);
            });
        },
        pluginsToDisable = ['collapse', 'dropdown', 'modal', 'tooltip', 'popover', 'tab'];
        disablePrototypeJS('show', pluginsToDisable);
        disablePrototypeJS('hide', pluginsToDisable);
    }

    $('[data-toggle="tooltip"]').bstooltip();

	/*Cancel hide Bootstrap dropdown-menu*/
	$('.dropdown-menu').click(function (event) { 
		var dd_close = $(event.target).hasClass('dd-close');
		if (!dd_close) event.stopPropagation();
	})

	$(".hasitems #panel").css("height", window_height - (header.outerHeight(true) - header_bottom.outerHeight(true)));

	$("#category-treeview").treeview({animated: "slow", collapsed: false, unique: true});

	var _topheight = window_width > 1080 ? $(".header-container").outerHeight() : 0;
	$('.main-slider .slider-down').click(function(){
		slider_button = $(this).parent().nextAll(':visible:first');
	 	$('body, html').animate({ scrollTop: slider_button.offset().top - _topheight }, 'slow');
	});

	var objetivos = $('.objetivos .table-responsive').scrollLeft();
    var activeLeft = ($('.objects-list li.active').length) ? $('.objects-list li.active').position().left : 0;

    $('.objetivos .table-responsive').animate({
        scrollLeft: objetivos + activeLeft
    });

	var _movemenu = false;
	$(".mobile-menu-icon .menu-icon").click(function () {
		if (!_movemenu) {
			jQuery(".header-bottom .dp-menu #wp-nav-container").appendTo(".mobile-menu-icon .dropdown-menu");
		}
	});
	$(".header-bottom .menu-icon").click(function () {
		$("#menu-button").trigger("click");
	});

	$(".btn-file input[type='file']").change(function() {
		if ($(this).val()) {
			$('#upload-file-info').html($(this).val());
		}
		else{
			$('#upload-file-info').html('Ningún archivo seleccionado');
		}
	});

    $('.tab-link').click(function(){
        var tab_id = $(this).data('tab');
        $('.tab-link').removeClass('current');
        $('.tab-content').removeClass('current');

        $('#log-'+tab_id).addClass('current');
        $("#"+tab_id).addClass('current');

        setTimeout(function(){
        	$('.tab-content.current input:first').focus();
        }, 500)
    });

	if(window_width > 991) {
		$(window).scroll(function () {
            if ($(this).scrollTop() > $(".header_top").outerHeight()) {
                $('.page').css('padding-top', header.outerHeight());
                header.addClass("f-nav");
            } else {
                $('.page').css('padding-top','');
                header.removeClass("f-nav");
            }
    	});
	}

	var _search = $(".form-search #search");
	$(".header-search-icon .header-icon-parent").click(function () {
		if (header.hasClass('header-fixed')) {
			$('body').addClass("header-active");
		} else {
			$('body').toggleClass("header-active");
			setTimeout(function () {
				if ($('body').hasClass('header-active')) {
				_search.focus();
			}
			}, 300);
		}
	});
	$(".header-search .header-search-close").click(function () {
		$('body').removeClass("header-active");
	});

	//Hover fact image
	$('.factimage-content')
	// content-facts mouse actions
	.on('mouseover', function(){
	  $(this).find('img').css({'transform': 'scale('+ $(this).attr('data-scale') +')'});
	})
	.on('mouseout', function(){
	  $(this).find('img').css({'transform': 'scale(1)'});
	})
	.on('mousemove', function(e){
	  $(this).find('img').css({'transform-origin': ((e.pageX - $(this).offset().left) / $(this).width()) * 100 + '% ' + ((e.pageY - $(this).offset().top) / $(this).height()) * 100 +'%'});
	})

	updateSuma($);
	updateQty($);
	newsletter_ajax();

	var owl = $(".main-container .owl-manufacturer");
	//switchOwl(owl);
	$(window).on('resize',function(){
		//switchOwl(owl);

		$(".hasitems #panel").css("height", $(window).height() - (header.outerHeight(true) - header_bottom.outerHeight(true)));
	});
});

jQuery(window).load(function() {
	var _wd = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
	var body = jQuery('body');
	if (_wd < 768) {
		if(body.hasClass('manufacturer-index-index')){
			_scrollatop('.manufacturer-list');
		} else if(body.hasClass('manufacturer-index-view')) {
			_scrollatop('.category-products');
		} else if(body.hasClass('category-objetivo')) {
			_scrollatop('#menu_lateral_8');
		} else if(body.hasClass('category-categorias')){
			_scrollatop('#menu_lateral_7');
		} else if(body.hasClass('catalog-category-view')){
			_scrollatop('.col-main');
		} else if (body.hasClass('catalogsearch-result-index')) {
			_scrollatop('.page-title');
		}

		jQuery("#menu_lateral_7 a.title").trigger("click");
	}
})

function switchOwl(slider){
	var width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
	if ( width >= 768 && slider.data('owl.carousel') == undefined) {
		slider.owlCarousel({
			autoplay: false,
	    	nav: true,
	    	navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
	    	dots: false,
	    	responsiveClass:true,
	    	responsive: {0: { items:2 }, 480: { items: 3}, 767: { items: 4},1000: { items: 5 }},
	    	margin: 0,
		});
	} else if ( width < 768 && slider.data('owl.carousel') != undefined ) {
		slider.data('owl.carousel').destroy();
	}
}

function updateSuma($) {
	var qty_el = $("#qty");

	$("#qty-increase").click(function () {
		var qty = parseInt(qty_el.val());
		if( !isNaN( qty )) qty_el.val(qty + 1);
		qty_el.trigger("change");
		return false;
	})
	$("#qty-decrease").click(function () {
		var qty = parseInt(qty_el.val());
		if( !isNaN( qty ) && qty > 1 ) qty_el.val(qty - 1);
		qty_el.trigger("change");
		return false;
	})
}

function updateQty($) {
	$( ".qty-arrows input" ).on( "click", function() {
		setTimeout(function(){
		  $('.cart-actions .btn-update').trigger('click');
		}, 2000);
	});
	$( ".box-qty input.qty" ).change(function() {
		setTimeout(function(){
		  $('.cart-actions .btn-update').trigger('click');
		}, 2000);
	});
}

function _scrollatop(_hacia){
	if (_hacia != "" || _hacia != null) {
		setTimeout( function(){
			jQuery('html, body').animate({ scrollTop: jQuery(_hacia).offset().top }, 1200);
		}, 100);
	}
}

/*Nwesletter Popup*/
function newsletter_ajax() {
	jQuery(".ajaxform").submit(function(event) {
  	event.preventDefault();
  	var $form = jQuery( this );
    var url = $form.attr('action');
	var rev_validator = new Validation($form.attr('id'));
	var res = rev_validator.validate();
  	if (res) {
	    jQuery.ajax({
	        url: url,
	        data: $form.serialize(),
	        type: 'POST',
	        dataType: 'json',
	        beforeSend:function(msg){
	            $form.find('.messagepopup').show().html('<span class="success">Agregando suscripción...</span>');
	        },
	        complete: function(msg){
	            $form.find('.messagepopup').html("");
	            obj = JSON.parse(msg.responseText);
	            if (obj.errorMsg) {
	                $form.find('.messagepopup').html('<span class="error">'+obj.errorMsg+'</span>');
	                setTimeout(function(){
	                    jQuery(".messagepopup").slideUp();
	                }, 3000);
	            }
	            if(obj.successMsg) {
	                $form.find(".messagepopup").html('<span class="success">'+obj.successMsg+'</span>');
	                $form.find(".input-text-email-newsletter-popup").val("");
	                setTimeout(function(){
	                    jQuery('.messagepopup').slideUp();
	                }, 3000);
	            }
	        }
	    });
	}
	});
}

function minusAcordion() {
	setTimeout(function() {
		if (!jQuery('.product_categories_list_menu .accor').hasClass('minus')){
			jQuery('.product_categories_list_menu .accor').trigger('click');
		}
		if (jQuery('.sub-category-expanded').parent().parent().parent().parent().children('.accor').hasClass('minus')) {
			if (jQuery('.product_categories_list_menu ul .product_categories_list_menu a').hasClass('sub-category-expanded')) {
				jQuery('.sub-category-expanded').parent().parent().parent().parent().children('.accor').trigger('click');
			}
		}
	}, 40);
}

function onChangeCheckboxtoSelect(_select, _list){
	var select = jQuery(_select).find("option"),
		list = jQuery(_list).find('input');
	list.change(function(){
		var _input = this.value;
		select.each(function(){
			if ((this.value != "") && this.value == _input) {
				jQuery(this).prop("selected", true);
			}
		})
	})
}

function _showLoginPopup(){
	var lrpopup = jQuery('.header-user .header-user-content');
	if (lrpopup.attr('data-toggle') == "dropdown") {
		jQuery('.header-user .dropdown-toggle').trigger('click');
	}
	else{
		jQuery('.header-user .header-user-content').trigger('click');
	}
}

function validationOnchange(fields, onclick) {
	onclick = onclick || false;
	var validar = false, counter = 0, obj_lng = 0;
    if (fields.length > 0) {
    	obj_lng = fields.length;
    	fields.forEach(function(field) {
    		if (!onclick) {
	        if ($(field).nodeName == "INPUT") {
	            $(field).observe('keyup', function(e){
	                validar = Validation.validate($(field));
	                if (!validar) {
	                    $(field).focus();
	                }
	            });
	        }
	        else if ($(field).nodeName == "SELECT"){
	            $(field).observe('change', function(e){
	                validar = Validation.validate($(field));
	                if (!validar) {
	                    $(field).focus();
	                }
	            });
	        }
	    }
	    else {
	    	validar = Validation.validate($(field));
	    	if (validar) {
	    		counter++;
	    	}
	    }
	    })
    }
    return (counter == obj_lng);
}

(function($) {
	$.fn.donetyping = function(callback){
	    var _this = $(this);
	    var x_timer;
	    _this.keyup(function (){
	        clearTimeout(x_timer);
	        x_timer = setTimeout(clear_timer, 1000);
	    }); 

	    function clear_timer(){
	        clearTimeout(x_timer);
	        callback.call(_this);
	    }
	}
})(jQuery);