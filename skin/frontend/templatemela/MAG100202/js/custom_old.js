var ItemsCarousel = {};

var widthClassOptions = [];
var widthClassOptions = ({
	bestseller       : 'bestseller_default_width',
	newproduct       : 'newproduct_default_width',
	featured         : 'featured_default_width',
	special          : 'special_default_width',
	additional       : 'additional_default_width',
	related          : 'related_default_width',
	upsell	         : 'upsell_default_width',
	crosssell        : 'crosssell_default_width',
	brand			 : 'brand_default_width',
	blog			 : 'blog_default_width',
	bestpromotions	 : 'bestpromotions_default_width',
	marcas           : 'marcas_default_width'
});

var $k =jQuery.noConflict();
$k(document).ready(function(){
    /*header-user-content*/
    $k(".header-user-content").fancybox({
        afterLoad: function () {
            $k(".fancybox-wrap").addClass("fancybox-wrap-newslettergetspecialoffers fancybox-skin-suscription");
            $k(".fancybox-skin").addClass("fancybox-skin-login");
        }}
    );

    $k('.btn-checkout-popup').click(function () {
        $k.fancybox([{
        	href : '#button-checkout-popup',
        	afterLoad: function () {
	            $k(".fancybox-wrap").addClass("fancybox-wrap-checkout");
	            $k(".fancybox-skin").addClass("fancybox-skin-checkout");
	        }},
        ]);
    });

    $k(".newslettergetspecialoffers-link").fancybox({
        afterLoad: function () {
            $k(".fancybox-wrap").addClass("fancybox-wrap-newslettergetspecialoffers");
            $k(".fancybox-skin").addClass("fancybox-skin-newslettergetspecialoffers");
        }
    });

    $k('.image_fancybox').fancybox({
        afterLoad: function () {
            $k(".fancybox-wrap").addClass("fancybox-wrap-fact");
        }
    });

	//$k('input[type="checkbox"]').tmMark();
	//$k('body:not(.catalog-product-view) input[type="radio"]').tmMark();
	
	$k(".form-language select").selectbox();
	$k(".tm_top_currency select").selectbox();
	$k(".limiter select").selectbox();
	$k(".sort-by select").selectbox(); 
	$k(".block-brand-nav select").selectbox();

	/*Cancel hide Bootstrap dropdown-menu*/
    jQuery('.dropdown-menu').click( function(event){event.stopPropagation();})
	
	$k('.cart-label').click(function() {
    	//$k('#nav-mobile').slideUp();
    	if ($k('#menu-content').is(":visible")) {$k('#menu-content').slideUp();}
  	});

  	$k('.client-profile').click(function() {
  		//$k('#nav-mobile').slideUp();
  		if ($k('#menu-content').is(":visible")) {$k('#menu-content').slideUp();}
  	});
	
	$k('.nav-responsive').click(function() {/*$k('#nav-mobile').slideToggle();*/});

    $k("#category-treeview").treeview({animated: "slow", collapsed: false, unique: true});
	
	$k(".tm_headerlinks_inner").click(function(){$k(".tm_headerlinkmenu .links").slideToggle('slow');});

	_topheight = jQuery(window).width() > 1080 ? 80 : 0;
	jQuery('.flexslider .slider-down').click(function(){
		slider_button = jQuery(this).parent().nextAll(':visible:first');
	 	jQuery('body, html').animate({ scrollTop: slider_button.offset().top - _topheight }, 'slow');
	});

	$k( ".qty-arrows input" ).on( "click", function() {
		setTimeout(function(){
		  $k('.cart-actions .btn-update').trigger('click');
		}, 2000);
	});
	$k( ".box-qty input.qty" ).change(function() {
		setTimeout(function(){
		  $k('.cart-actions .btn-update').trigger('click');
		}, 2000);
	});

	/*Delay de menu sin popup*/
	$k("#advancedmenu .menu a.item-noarrow").hover( function() {
	    var element = $k(this).parent();
	    setTimeout( function() {
	       $k(element).addClass("active");
	    }, 500);
	}, function() {
	    var elem = $k(this).parent();
	    setTimeout( function() {
	       $k(elem).removeClass("active");
	    }, 500);
	});

	if (window.location.href.indexOf("/changepass") > -1) {
		$k('.ch-pass').css('display','');
	    $k('.no-show-has-pass').css('display','none');
	}

	$k('.img-photo img').click(function(){
		$k('#file-photo').trigger('click');
	});

	jQuery(".btn-file input[type='file']").change(function() {
		if (jQuery(this).val()) {
			jQuery('#upload-file-info').html(jQuery(this).val());
		}
		else{
			jQuery('#upload-file-info').html('Ningún archivo seleccionado');
		}
	});

    $k('.tab-link').click(function(){
        var tab_id = $k(this).attr('data-tab');
        $k('.tab-link').removeClass('current');
        $k('.tab-content').removeClass('current');

        $k('#log-'+tab_id).addClass('current');
        $k("#"+tab_id).addClass('current');

        setTimeout(function(){
        	$k('.tab-content.current input:first').focus();
        }, 500)        
        $k.fancybox.reposition();
    });

    $k('[data-toggle="tooltip"]').tooltip(); 

    /*Accordion Nosotros*/
    $k(".title-nosotros").click(function () {
    	$k(this).next(".p-content").slideToggle();
    	$k(this).toggleClass("title-open");
    });

    mobileToggleMenu();
	menuResponsive();
	mobileToggleColumn();
	productCarouselAutoSet();
	productListAutoSet();
	mobileTabToggle();
	tableMakeResponsive();
	productsOwlCarousel();

	//Hover fact image
	$k('.factimage-content')
	// content-facts mouse actions
	.on('mouseover', function(){
	  $k(this).children('img').css({'transform': 'scale('+ $k(this).attr('data-scale') +')'});
	})
	.on('mouseout', function(){
	  $k(this).children('img').css({'transform': 'scale(1)'});
	})
	.on('mousemove', function(e){
	  $k(this).children('img').css({'transform-origin': ((e.pageX - $k(this).offset().left) / $k(this).width()) * 100 + '% ' + ((e.pageY - $k(this).offset().top) / $k(this).height()) * 100 +'%'});
	})

	/*var _input = $k('.header .form-search input.input-text');
	$k('.header .form-search button.button').click(function(){
		if (_input.val() == 'Buscar') _input.val("");
		_input.focus();
	})*/

	//searchbyCats();
});

window.onload = function() {
	var _wd = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
	if (_wd < 768) {
		if($k('body').hasClass('manufacturer-index-index')){
			_scrollatop('.manufacturer-list');
		} else if($k('body').hasClass('manufacturer-index-view')) {
			_scrollatop('.category-products');
		} else if($k('body').hasClass('category-objetivo')) {
			_scrollatop('#menu_lateral_8');
		} else if($k('body').hasClass('category-categorias')){
			_scrollatop('#menu_lateral_7');
		} else if($k('body').hasClass('catalog-category-view')){
			_scrollatop('.col-main');
		} else if ($k('body').hasClass('catalogsearch-result-index')) {
			_scrollatop('.page-title');
		}

		$k("#menu_lateral_7 a.title").trigger("click");
	}
};

function _scrollatop(_hacia){
	if (_hacia != "" || _hacia != null) {
		setTimeout( function(){
			jQuery('html, body').animate({ scrollTop: jQuery(_hacia).offset().top }, 1200);	
		}, 100);
	}
}

/*Nwesletter Popup*/
function _newsletter_ajax(form_id) {
  $k(form_id).submit(function() {
  	var _email = $k('#newsletter');
  	if (_email.hasClass('validation-failed') == false || _email != '') {
	    $k.ajax({
	        url: '/newsletter/subscriber/newajax/',
	        data: $k(this).serialize(),
	        type: 'POST',
	        dataType: 'json',
	        beforeSend:function(msg){
	            $k('.messagepopup_success').html("<span>Agregando suscripción...</span>");
	        },
	        complete: function(msg){
	            $k('.messagepopup_success').html("");
	            obj = JSON.parse(msg.responseText);
	            if (obj.errorMsg) {
	                $k('.messagepopup_error').html("<span>"+obj.errorMsg+"</span>");
	                setTimeout(function(){
	                    $k(".messagepopup_error").html('');
	                }, 3000);
	            }
	            if(obj.successMsg) {
	                $k(".messagepopup_success").html('<span>'+obj.successMsg+'</span>');
	                $k(".input-text-email-newsletter-popup").val("");
	                setTimeout(function(){
	                    $k('.messagepopup_success').html("");
	                    $k.fancybox.close();
	                }, 3000);
	            }
	        }
	    });
	} else {
		return false;
	}
    return false;
	});
}

function carousel_about_us(){
	if ($k(window).width() <= 991) {
		$k('.lab_bg_objetivos').insertAfter('.lab_bg_asesores');
		$k('.carousel-about-us').bxSlider({captions: true,responsive: true,controls: false,});
	}
}
 
function mobileToggleMenu(){	
	if ($k(window).width() > 479 && $k(window).width() < 1080) {
		$k(".mob-toggle h6 .mobile_togglemenu").remove();
		$k(".mob-toggle h6").append( "<a class='mobile_togglemenu'>&nbsp;</a>" ).addClass('toggle');
		$k("#footer .mobile_togglemenu ,#footer .block .block-title .mobile_togglemenu ").click(function(){
			$k(this).parent().toggleClass('active').parent().find('ul').toggle('slow');
			$k('html,body').animate({ scrollTop: $k(this).offset().top}, 'slow');
		});

	} else{
		$k("#footer h6 , #footer .block .block-title").parent().find('ul').removeAttr('style');
		$k("#footer h6 , #footer .block .block-title").removeClass('active');
		$k("#footer h6 , #footer .block .block-title").removeClass('toggle');
		$k("#footer .mobile_togglemenu ,#footer .block .block-title .mobile_togglemenu ").remove();
	}	
} 

function mobileToggleColumn(){	
	if ($k(window).width() < 768){
		$k('.sidebar .mobile_togglecolumn').remove();
		$k(".sidebar .block-title" ).append( "<span class='mobile_togglecolumn'>&nbsp;</span>" );

		$k(".sidebar .block-title" ).addClass('toggle');
		$k(".sidebar .mobile_togglecolumn").click(function(){
			$k(this).parent().toggleClass('active').parent().find('.block-content').toggle('slow');
		});
	}
	else{ 
		$k(".sidebar .block-title" ).parent().find('.block-content').removeAttr('style');
		$k(".sidebar .block-title" ).removeClass('toggle');
		$k(".sidebar .block-title" ).removeClass('active');
		$k('.sidebar .mobile_togglecolumn').remove();
	}
}

function menuResponsive(){
	if ($k(window).width() < 1080){
		 var elem = document.getElementById("nav");
		if(typeof elem !== 'undefined' && elem !== null) {
			document.getElementById("nav").id= "nav-mobile";
		
		if($k('.main-navigation').hasClass('treeview')!=true){
			$k(".nav-inner #nav-mobile").treeview({
				animated: "slow",
				collapsed: true,
				unique: true
			});
			$k('.nav-inner #nav-mobile a.active').parent().removeClass('expandable');
			$k('.nav-inner #nav-mobile a.active').parent().addClass('collapsable');
			$k('.nav-inner #nav-mobile .collapsable ul').css('display','block');
		}
	  }
		$k('.header-search').appendTo($k('.header-background'));
		//$k(".header-cart").insertAfter($k(".header-logo"));
		$k(".header-cart").insertAfter($k(".asesoria-mobile"));
		$k('.header-user').appendTo('.header_top');
		$k('.header-toplinks').insertAfter($k('.header-cart'));
	}
	else{	
		$k(".nav-inner .hitarea").remove();	 
		$k("#nav-mobile").removeClass('treeview');
		$k(".nav-inner ul").removeAttr('style');
		$k('.nav-inner li').removeClass('expandable');
		$k('.nav-inner li').removeClass('collapsable');
		var elem = document.getElementById("nav-mobile");
		if(typeof elem !== 'undefined' && elem !== null) {
			document.getElementById("nav-mobile").id= "nav";
		}
	}

}

function productsOwlCarousel() {
	$k('.wrapper .owl-carousel').each(function () {
		var objID = $k(this).attr('id');
		switch(objID) {
			case "upsell-carousel":
			ItemsCarousel = ({
				0: { items: 2 },
				450: { items: 2	},
				600: { items: 3	},
				1000: { items: 4 }
			});
			case "crosssell-carousel":
			ItemsCarousel = ({
				0: { items: 1 },
				450: { items: 1	},
				600: { items: 1	},
				1000: { items: 1 }
			});
			break;
			default:
			ItemsCarousel = ({
				0: { items: 2 },
				450: { items: 2	},
				600: { items: 3	},
				1000: { items: 4 }
			});
			break;
		}
		$k(this).owlCarousel({
	    	autoplay: false,
	    	nav: true,
	    	navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
	    	dots: false,
	    	responsiveClass:true,
	    	responsive: ItemsCarousel,
	    	margin: 0,
	    });
	})
}
 
function productCarouselAutoSet() { 
	$k(".wrapper .product-carousel, .additional-carousel .product-carousel").each(function() {
		var objectID = $k(this).attr('id');
		var myObject = objectID.replace('-carousel',''); 
		if(myObject.indexOf("-") >= 0)
			myObject = myObject.substring(0,myObject.indexOf("-"));
		if(widthClassOptions[myObject])
			var myDefClass = widthClassOptions[myObject];
		else
			var myDefClass= 'grid_default_width';
		var items = 0;
		switch (objectID) {
		    case "home-carousel":
		        items = 4;
		        break;
		    case "home-carousel3":
		        items = 4;
		        break;
		    case "home-carousel4":
		        items = 4;
		        break;
			case "manufacture-carousel":
				items = 5;
				break;
			case "marcas-carousel":
				items = 4;
				break;
			case "upsell-carousel":
				items: 4;
		}

		var slider = $k(".wrapper #" + objectID + ", .additional-carousel #"+ objectID );
		slider.sliderCarousel({
			defWidthClss : myDefClass,
			subElement   : '.slider-item',
			subClass     : 'product-block',
			firstClass   : 'first_item_tm',
			lastClass    : 'last_item_tm',
			slideSpeed : 200,
			paginationSpeed : 800,
			autoPlay : false,
			stopOnHover : false,
			goToFirst : true,
			goToFirstSpeed : 1000,
			goToFirstNav : true,
			pagination : false,
			paginationNumbers: false,
			responsive: true,
			responsiveRefreshRate : 200,
			baseClass : "slider-carousel",
			theme : "slider-theme",
			autoHeight : true,
			itemsSlider: items
		});
		
		var nextButton = $k(this).parent().find('.next');
		var prevButton = $k(this).parent().find('.prev');
		nextButton.click(function(){
			slider.trigger('slider.next');
		})
		prevButton.click(function(){
			slider.trigger('slider.prev');
		})
	});
}

function productListAutoSet() { 
	$k("ul.products-grid").each(function(){	
	var objectID = $k(this).attr('id');
		if(objectID.length >0){
			if(widthClassOptions[objectID.replace('-grid','')])
				var myDefClass= widthClassOptions[objectID.replace('-grid','')];
			}else{ 
			var myDefClass= 'grid_default_width';
		}
		$k(this).smartColumnsRows({
			defWidthClss : myDefClass,
			subElement   : 'li',
			subClass     : 'product-block'
		});
	});
}
 
function tableMakeResponsive(){
	if ($k(window).width() < 640){
		// SUPER PRODUCT TABLE
		if($k("table#super-product-table").length != 0) {
			if($k("#super-table").length == 0) {
				$k('<div id="super-table"></div>').insertBefore('#super-product-table');
			}
			$k('table#super-product-table').addClass("table-responsive");
			$k('table#super-product-table thead').addClass("table-head");
			$k('table#super-product-table tfoot').addClass("table-foot");
			$k('table#super-product-table tr').addClass("row-responsive");
			$k('table#super-product-table td').addClass("column-responsive clearfix");
			$k("table#super-product-table").responsiveTable({prefix:'tm_responsive',target:'#super-table'});
		}
		// WISHLIST TABLE
		if($k("table#wishlist-table").length != 0) {
			if($k("#new-wishlist-table").length == 0) {
				$k('<div id="new-wishlist-table"></div>').insertBefore('#wishlist-table');
			}
			$k('table#wishlist-table').addClass("table-responsive");
			$k('table#wishlist-table thead').addClass("table-head");
			$k('table#wishlist-table tfoot').addClass("table-foot");
			$k('table#wishlist-table tr').addClass("row-responsive");
			$k('table#wishlist-table td').addClass("column-responsive clearfix");
			$k("table#wishlist-table").responsiveTable({prefix:'tm_responsive',target:'#new-wishlist-table'});
		}
		// DOWNLOADABLE TABLE
		if($k("table#my-downloadable-products-table").length != 0) {
			if($k("#downloadable-products-table").length == 0) {
				$k('<div id="downloadable-products-table"></div>').insertBefore('#my-downloadable-products-table');
			}			 
			$k('table#my-downloadable-products-table').addClass("table-responsive");
			$k('table#my-downloadable-products-table thead').addClass("table-head");
			$k('table#my-downloadable-products-table tfoot').addClass("table-foot");
			$k('table#my-downloadable-products-table tr').addClass("row-responsive");
			$k('table#my-downloadable-products-table td').addClass("column-responsive clearfix");
			$k("table#my-downloadable-products-table").responsiveTable({prefix:'tm_responsive',target:'#downloadable-products-table'});
		}
	}else{
		// SUPER PRODUCT TABLE
		if($k("table#super-product-table").length != 0) {
			$k('table#super-product-table').removeClass("table-responsive");
			$k('table#super-product-table thead').removeClass("table-head");
			$k('table#super-product-table tfoot').removeClass("table-foot");
			$k('table#super-product-table tr').removeClass("row-responsive");
			$k('table#super-product-table td').removeClass("column-responsive");
			$k("#super-table").remove();
		}
		// WISHLIST TABLE
		if($k("table#wishlist-table").length != 0) {
			$k('table#wishlist-table').removeClass("table-responsive");
			$k('table#wishlist-table thead').removeClass("table-head");
			$k('table#wishlist-table tfoot').removeClass("table-foot");
			$k('table#wishlist-table tr').removeClass("row-responsive");
			$k('table#wishlist-table td').removeClass("column-responsive");
			$k("#new-wishlist-table").remove();
		}
		// DOWNLOADABLE TABLE
		if($k("table#my-downloadable-products-table").length != 0) {
			$k('table#my-downloadable-products-table').removeClass("table-responsive");
			$k('table#my-downloadable-products-table thead').removeClass("table-head");
			$k('table#my-downloadable-products-table tfoot').removeClass("table-foot");
			$k('table#my-downloadable-products-table tr').removeClass("row-responsive");
			$k('table#my-downloadable-products-table td').removeClass("column-responsive");
			$k("#downloadable-products-table").remove();
		}
	}
}

function mobileTabToggle(){
	if ($k(window).width() < 992){
		$k('#product_tabs_description_tabbed_contents .toggle .mobile_togglemenu').trigger('click');
	}else{
		var nav = $k('.header-background');
		$k(window).scroll(function () {
            if ($k(this).scrollTop() > 35) {
                $k('.paddingfix').css('padding-top','84px');
                nav.addClass("f-nav");
                $k('.megnor-advanced-menu-popup').css('top','77px');                
            } else {
                $k('.paddingfix').css('padding-top','');
                nav.removeClass("f-nav");
                $k('.megnor-advanced-menu-popup').css('top','121px');
            }
    	});
	}
}

function searchbyCats() {
	var _container = $k('.container_searchpopup');
	var parent = $k('#filtercats');
	var _q = $k("#search");
	_q.keyup(function () {
        var filter = $k(this).val();
        parent.find('li').each(function () {
            if (filter == "") {
            	_container.hide();
                $k(this).slideUp();
            } else if ($k(this).text().search(new RegExp(filter, "i")) < 0) {
            	_container.show();
                $k(this).slideUp();
            } else {
            	_container.show();
                $k(this).slideDown();
            }
        });
    });

    parent.find('li a').click( function (e) {
    	var _text = $k(e.target).text();
    	_q.val(_text);
    })
}

function searchbyGFilters(_sources){
    var availableTags = _sources;        
    var customRenderMenu = function(ul, items){
        var self = this;
        var categoryArr = [];        
        function contain(item, array) {
            var contains = false;
            $k.each(array, function (index, value) {
                if (item == value) {
                    contains = true;
                    return false;
                }
            });
            return contains;
        }
        
        $k.each(items, function (index, item) {
            if (! contain(item.category, categoryArr)) {
                categoryArr.push(item.category);
            }
        });
        
        $k.each(categoryArr, function (index, category) {
            ul.append("<header class='header-auto'><h4>"+ category + "</h4></header>");
            $k.each(items, function (index, item) {
                if (item.category == category) {
                    self._renderItemData(ul, item);
                }
            });
        });
    };
        
    $k("#search").autocomplete({
        source: availableTags,
        appendTo: "#container_searchpopup",
        open: function() {
			$k('#container_searchpopup .fa').css('display','block');
		},
		close: function () {
			$k('#container_searchpopup .fa').css('display','none');
		},
		position: { my : "right top", at: "right bottom" },
		select: function( event, ui ) {
		    window.location.href = ui.item.value;
		},
        create: function () {
            //access to jQuery Autocomplete widget differs depending
            $k(this).data('uiAutocomplete')._renderMenu = customRenderMenu;
        }
    });
}

function minusAcordion(){
	setTimeout(function() {
		if (!$k('.product_categories_list_menu .accor').hasClass('minus')){
			$k('.product_categories_list_menu .accor').trigger('click');
		}
		if ($k('.sub-category-expanded').parent().parent().parent().parent().children('.accor').hasClass('minus')) {
			if ($k('.product_categories_list_menu ul .product_categories_list_menu a').hasClass('sub-category-expanded')) {
				$k('.sub-category-expanded').parent().parent().parent().parent().children('.accor').trigger('click');
			}
		}
	}, 40);
}

function onChangeCheckboxtoSelect(_select, _list){
	var select = $k(_select).find("option");
	var list = $k(_list).find('input');
	list.change(function(){
		var _input = this.value;
		select.each(function(){
			if ((this.value != "") && this.value == _input) {
				$k(this).prop("selected", true);
			}
		})
	})
}

function _showLoginPopup(){
    $k('.header-user-content[data-tab="tab-1"]').trigger('click');
}

function showLoginRegisterPopup(isloggedin){
	if (isloggedin) {
		jQuery('.header-user-content[data-tab="tab-1"]').fancybox({
	        modal: true,
	        afterLoad: function () {
	            $k(".fancybox-wrap").addClass("fancybox-skin-suscription");
	            $k(".fancybox-skin").addClass("fancybox-skin-login");
	        }
	    }).trigger('click');
	    jQuery('#billing-new-address-form').addClass('hidden');
	    jQuery('.shipping_other_address').addClass('hidden');
	}
	else{
		jQuery('#billing-new-address-form').removeClass('hidden');
		jQuery('.shipping_other_address').removeClass('hidden');
	}
}

function validationOnchange(_fields) {
	var fields = _fields;
    var validar = false;
    if (fields.length > 0) {
    	fields.forEach(function(field) {
	        if ($(field).nodeName == "INPUT") {
	                $(field).observe('keyup', function(e){
	                validar = Validation.validate($(field));
	                if (!validar) {
	                    $(field).focus();
	                }
	            });
	        }
	        else if ($(field).nodeName == "SELECT"){
	            $(field).observe('change', function(e){
	                validar = Validation.validate($(field));
	                if (!validar) {
	                    $(field).focus();
	                }
	            });
	        }
	        
	    })
    }
}