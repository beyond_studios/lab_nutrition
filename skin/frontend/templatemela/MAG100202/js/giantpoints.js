;(function() {
    var loyaltyDecorateStatus= function(){
        var valuePoints = jQuery(".value-points").html();
        var valueFriendzone = jQuery(".value-frienzone").html();
        var valueGileos = jQuery(".value-gileos").html();
        var valueRelacion = jQuery(".value-relacion").html();
        var getSkinUrl = jQuery(".get-skin-url").html();

        var status = 'icon_tranqui_medium.png';
        if(valuePoints==valueFriendzone) {
            status = 'icon_spartan_medium.png';
        }
        if(valuePoints==valueRelacion) {
            status = 'icon_maceta_medium.png';
        }
        var srcIconStatus = getSkinUrl + "images/points/"+status;
    };

	var calculateBar = function(valuePoints, valueRelacion) {
		var percentageCurrently = Math.floor((valuePoints*100)/valueRelacion);
		if (percentageCurrently > 100) percentageCurrently = 100;
		jQuery(".loyalty-account-score1-barra-item").css("width","calc("+percentageCurrently+"% + 0px)");
	};

	var changeColorIconBar = function(getSkinUrl, valuePoints, valueGileos) {
		/*GILEOS*/
		var imgGileos = 'icon_spartan_medium.png';
		var imgGileosNegative = 'big_icon_badge.png';
		var imgGileosSrc = imgGileosNegative;
		if(valuePoints>=valueGileos) {
			imgGileosSrc = imgGileos;
            jQuery(".loyalty-account-score1-barra-gileos-content img").removeClass("loyalty-account-score1-barra-gileos-content-img");
            jQuery(".loyalty-account-score1-barra-gileos-content img").addClass("loyalty-account-score1-barra-gileos-content-img-active");
		}

		var url = getSkinUrl+"images/points/"+imgGileosSrc;
		jQuery(".loyalty-account-score1-barra-gileos-content img").attr("src",url);
	}

	var definePositionGileo = function(valueGileos, valueRelacion) {
		var percentageGileoGlobe = Math.floor((valueGileos*100)/valueRelacion);
		jQuery(".loyalty-account-score1-barra-gileos-content").css("left",'calc('+percentageGileoGlobe+'% - 35px)');
	};

	var loyaltyDefineTitleProfile = function(valuePoints, valueGileoTitle, valueRelacionTitle, valueFriendzoneTitle, valueGileos, valueRelacion){
		var title = valueFriendzoneTitle;
		if(valuePoints>=valueGileos) {
			title=valueGileoTitle;
		}
		if(valuePoints>=valueRelacion) {
			title=valueRelacionTitle;
		}
		jQuery(".loyalty-account-aviso1-element3-content strong").html(title);
	}

	var loyaltyDefineCommentProfile = function(valuePoints, valueGileoComentario, valueRelacionComentario, valueFriendzoneComentario, valueGileos, valueRelacion) {
		var comment = valueFriendzoneComentario;
		if(valuePoints>=valueGileos) {
			comment=valueGileoComentario;
		}
		if(valuePoints>=valueRelacion) {
			comment=valueRelacionComentario;
		}
		jQuery(".loyalty-account-aviso1-element3-content .description").html(comment);
	}

	var loyaltyChangeIconCenter = function(getSkinUrl, valuePoints, valueGileos, valueRelacion) {
		var status = 'icon_tranqui_medium.png';
		if(valuePoints>=valueGileos){
			status = 'icon_spartan_medium.png';
		}
		if(valuePoints>=valueRelacion){
			status = 'icon_maceta_medium.png';
		}
		var srcIconStatus = getSkinUrl + "images/points/"+status;
		jQuery(".loyalty-account-aviso1-element2-content .loyalty-account-icon").attr("src",srcIconStatus);
	};

	var loyaltyYouNeedLittle = function(valuePoints, valueGileos, valueRelacion){
		var number = 0;

		if(valuePoints<valueRelacion){
			number = (valueRelacion-valuePoints);
		}
		if(valuePoints<valueGileos){
			number = (valueGileos-valuePoints);
		}
		if(number>0) {
			jQuery(".puntoacumulados-value-faltan-content").html(number+" Pts");
		}
	};

    var loyaltyDecorateMyProfile = function() {
		
		setTimeout(function(){
			jQuery(".customer-account-index .dashboard").css("display",'block');
		}, 1000);
        var valuePoints = parseInt(jQuery(".value-points").html());
        var valueFriendzone = parseInt(jQuery(".value-friendzone").html());
        var valueGileos = parseInt(jQuery(".value-gileo").html());
        var valueRelacion = parseInt(jQuery(".value-relacion").html());
        var getSkinUrl = jQuery(".get-skin-url").html();
		definePositionGileo(valueGileos, valueRelacion);
		calculateBar(valuePoints, valueRelacion);
		changeColorIconBar(getSkinUrl, valuePoints, valueGileos);

        if (isNaN(valuePoints) || valuePoints <= 0) {
            jQuery(".loyalty-account-score1-barra-item-arrow").remove();
        }
		/*Comment*/
		var valueGileoComentario = jQuery(".value-gileo-comentario").html();
		var valueRelacionComentario = jQuery(".value-relacion-comentario").html();
		var valueFriendzoneComentario = jQuery(".value-friendzone-comentario").html();

		loyaltyDefineCommentProfile(valuePoints, valueGileoComentario, valueRelacionComentario, valueFriendzoneComentario, valueGileos, valueRelacion);

		/*Title*/
		var valueGileoTitle = jQuery(".value-gileo-titulo").html();
		var valueRelacionTitle = jQuery(".value-relacion-titulo").html();
		var valueFriendzoneTitle = jQuery(".value-friendzone-titulo").html();
		loyaltyDefineTitleProfile(valuePoints, valueGileoTitle, valueRelacionTitle, valueFriendzoneTitle, valueGileos, valueRelacion);

		/*Icon center*/
		loyaltyChangeIconCenter(getSkinUrl, valuePoints, valueGileos, valueRelacion);
		/*You need*/
		loyaltyYouNeedLittle(valuePoints, valueGileos, valueRelacion);

        jQuery("#shopping-cart-totals-table").append(jQuery("#cartTrPoint"));

    };

    Event.observe(window, 'load', function() {

		loyaltyDecorateStatus();
	    loyaltyDecorateMyProfile();
	});

})();