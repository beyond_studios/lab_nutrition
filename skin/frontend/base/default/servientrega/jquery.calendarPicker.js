jQuery.fn.calendarPicker = function(options) {
  // --------------------------  start default option values --------------------------
  if (!options.date) {
    options.date = new Date();
  }

  if (typeof(options.years) == "undefined")
    options.years=1;

  if (typeof(options.hiddenDay) == "undefined")
    options.hiddenDay = ["-1"];
  if (typeof(options.beforeShowHolidays) == "undefined")
    options.beforeShowHolidays = ["0/00"];

  if (typeof(options.aditionalDay) == "undefined"){
    options.aditionalDay=0;
    options.local_day = options.date.getDate();
    options.local_months = options.date.getMonth();
    options.local_year = options.date.getYear();

  } else {
    var aditionalDay = aditionaldate(new Date(),options.aditionalDay,options.hiddenDay,options.beforeShowHolidays);
    options.date.setDate(options.date.getDate()+ aditionalDay);
    options.local_day = options.date.getDate();
    options.local_months = options.date.getMonth();
    options.local_year = options.date.getYear();

  }

  if (typeof(options.months) == "undefined")
    options.months=3;

  if (typeof(options.days) == "undefined")
    options.days=4;

  if (typeof(options.showDayArrows) == "undefined")
    options.showDayArrows=true;

  if (typeof(options.useWheel) == "undefined")
    options.useWheel=true;

  if (typeof(options.callbackDelay) == "undefined")
    options.callbackDelay=500;
  
  if (typeof(options.monthNames) == "undefined")
    options.monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

  if (typeof(options.dayNames) == "undefined")
    options.dayNames = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

  


  // --------------------------  end default option values --------------------------

  var calendar = {currentDate: options.date};
  calendar.options = options;

  //build the calendar on the first element in the set of matched elements.
  var theDiv = this.eq(0);//jQuery(this);
  theDiv.addClass("calBox");

  //empty the div
  theDiv.empty();


  var divYears = jQuery("<div>").addClass("calYear");
  var divMonths = jQuery("<div>").addClass("calMonth");
  var divDays = jQuery("<div>").addClass("calDay");


  theDiv.append(divYears).append(divMonths).append(divDays);

  calendar.changeDate = function(date) {
    calendar.currentDate = date;

    var fillYears = function(date) {
      var year = date.getFullYear();
      var t = new Date();
      var aditionalDay = aditionaldate(t,options.aditionalDay,options.hiddenDay,options.beforeShowHolidays);
      t.setDate(t.getDate() + aditionalDay);
      divYears.empty();
      var nc = options.years*2+1;
      var w = parseInt((theDiv.width()-4-(nc)*4)/nc)+"px";
      for (var i = year - options.years; i <= year + options.years; i++) {
        var d = new Date(date);
        d.setFullYear(i);
        var span = jQuery("<span>").addClass("calElement").attr("millis", d.getTime()).html(i).css("width",w);
        if (d.getYear() == t.getYear())
          span.addClass("today");
        if (d.getYear() == calendar.currentDate.getYear())
          span.addClass("selected");
        divYears.append(span);
      }
    }

    var fillMonths = function(date) {
      var month = date.getMonth();
      var t = new Date();
      var aditionalDay = aditionaldate(t,options.aditionalDay,options.hiddenDay,options.beforeShowHolidays);
      t.setDate(t.getDate() + aditionalDay);

      divMonths.empty();
      var oldday = date.getDay();
      var nc = options.months*2+1;
      var w = parseInt((theDiv.width()-4-(nc)*4)/nc)+"px";
      for (var i = -options.months; i <= options.months; i++) {
        var d = new Date(date);

        var oldday = d.getDate();
        d.setMonth(month + i);

        if (d.getDate() != oldday) {
          d.setMonth(d.getMonth() - 1);
          d.setDate(28);
        }
        var span = jQuery("<span>").addClass("calElement").attr("millis", d.getTime()).html(options.monthNames[d.getMonth()]).css("width",w);
        if (d.getYear() == t.getYear() && d.getMonth() == t.getMonth())
          span.addClass("today");
        if (d.getYear() == calendar.currentDate.getYear() && d.getMonth() == calendar.currentDate.getMonth())
          span.addClass("selected");
        divMonths.append(span);

      }
    }

    var fillDays = function(date) {
      var day = date.getDate();
      var t = new Date();
      var aditionalDay = aditionaldate(t,options.aditionalDay,options.hiddenDay,options.beforeShowHolidays);
      t.setDate(t.getDate() + aditionalDay);
      divDays.empty();
      var nc = options.days*2+1;

      var w = parseInt((theDiv.width()-4-(options.showDayArrows?12:0)-(nc)*4)/(nc-(options.showDayArrows?2:0)))+"px";
      for (var i = -options.days; i <= options.days; i++) {
        var d = new Date(date);
        d.setDate(day + i)
        if ((d.getDate(day+i) < options.local_day || d.getMonth() < options.local_months) && d.getMonth() <= options.local_months && d.getYear() <= options.local_year ) {
        var span = jQuery("<span>").addClass("calElementNot").attr("millisNot", d.getTime())
        }else {
        var span = jQuery("<span>").addClass("calElement").attr("millis", d.getTime())
        }
        
        var beforeShowHolidays = d.getDate(day+i)+'/'+(d.getMonth() + 1);
        
        //hiddenDay
        if (in_array(d.getDay(),options.hiddenDay)) {
          var span = jQuery("<span>").addClass("calElementNot").attr("millisNot", d.getTime())  
        }
        if (in_array(beforeShowHolidays,options.beforeShowHolidays) ) {
          var span = jQuery("<span>").addClass("calElementNot").attr("millisNot", d.getTime())  
        }
          
        if (i == -options.days && options.showDayArrows) {
          span.addClass("prev fa fa-angle-left");
        } else if (i == options.days && options.showDayArrows) {
          span.addClass("next fa fa-angle-right");
        } else {
          span.html("<span class=day>"+options.dayNames[d.getDay()]+"</span><br /><span class=dayNumber>" + d.getDate() + "</span>").css("width",w);
          if (d.getYear() == t.getYear() && d.getMonth() == t.getMonth() && d.getDate() == t.getDate())
            span.addClass("today");
          if (d.getYear() == calendar.currentDate.getYear() && d.getMonth() == calendar.currentDate.getMonth() && d.getDate() == calendar.currentDate.getDate())
            span.addClass("selected");
        }
        divDays.append(span);

      }
    }

    var deferredCallBack = function() {
      if (typeof(options.callback) == "function") {
        if (calendar.timer)
          clearTimeout(calendar.timer);

        calendar.timer = setTimeout(function() {
          options.callback(calendar);
        }, options.callbackDelay);
      }
    }


    fillYears(date);
    fillMonths(date);
    fillDays(date);

    deferredCallBack();

  }

  theDiv.click(function(ev) {

    var el = jQuery(ev.target).closest(".calElement");
    if (el.hasClass("calElement")) {
      //console.log(el.attr("millis"));
      calendar.changeDate(new Date(parseInt(el.attr("millis"))));
    }
  });


  //if mousewheel
  if (jQuery.event.special.mousewheel && options.useWheel) {
    divYears.mousewheel(function(event, delta) {
      var d = new Date(calendar.currentDate.getTime());
      d.setFullYear(d.getFullYear() + delta);
      calendar.changeDate(d);
      return false;
    });
    divMonths.mousewheel(function(event, delta) {
      var d = new Date(calendar.currentDate.getTime());
      d.setMonth(d.getMonth() + delta);
      calendar.changeDate(d);
      return false;
    });
    divDays.mousewheel(function(event, delta) {
      var d = new Date(calendar.currentDate.getTime());
      d.setDate(d.getDate() + delta);
      calendar.changeDate(d);
      return false;
    });
  }


  calendar.changeDate(options.date);

  return calendar;
};

function in_array(needle, haystack) {
    for(var i in haystack) {
        if(haystack[i] == needle) return true;
    }
    return false;
}
function aditionaldate(date,aditional,hiddenDay,beforeShowHolidays){

  var day = date.getDay();
  /*if (date.getDay() == 5 && date.getHours() >= 12 ) {
    aditional++;
  }*/
  for (var i = 0; i < aditional+1; i++) {
      if (day > 6) {day = 0}
      var bolean = true;
      if (in_array(day,hiddenDay)) {
          aditional++;
          bolean = false;
      }
      var date_string = date.getDate()+'/'+(date.getMonth()+1);
      date.setDate(date.getDate()+1);
      if (in_array(date_string,beforeShowHolidays) ) {
            if (bolean) {
              aditional++;
            }
      }
      day++;
  }

  return aditional;
}