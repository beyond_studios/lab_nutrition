document.addEventListener("DOMContentLoaded", function () {
    document.getElementsByTagName("BODY")[0].addEventListener("click", function (e) {
        if (e.target && udxSelectorMatches(e.target, "[data-gtm-click-id]")) {
            var sku = e.target.getAttribute("data-gtm-click-id");
            var name = e.target.getAttribute('data-gtm-click-name');
            var price = e.target.getAttribute('data-gtm-click-price');
            var category = e.target.getAttribute('data-gtm-click-category');

            var productClickDataLayer = {
                'event': 'productClick',
                'ecommerce': {
                    'click': {
                        'products': [{
                            'name': name,
                            'id': sku,
                            'price': price,
                            'category': category
                        }]
                    }
                }
            };
            dataLayer.push(productClickDataLayer);
        }
    });
});
