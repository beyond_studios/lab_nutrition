function gtmCheckoutStep(step) {
    udxPostAjax('/gtm/enhancedEcommerce/checkout', { step: step }, function(response) {
        response = JSON.parse(response);
        if (typeof response.event !== 'undefined') {
            dataLayer.push(response);
        }
    });
}

Checkout.prototype.originalGotoSection = Checkout.prototype.gotoSection;
Checkout.prototype.gotoSection = function (section, reloadProgressBlock) {
    var obj = $('opc-login');
    if (obj === null || !obj.hasClassName('active')) {
        gtmCheckoutStep(this.currentStep);
    }
    this.originalGotoSection(section, reloadProgressBlock);
};

Review.prototype.originalNextStep = Review.prototype.nextStep;
Review.prototype.nextStep = function (transport) {
    gtmCheckoutStep('review');
    this.originalNextStep(transport);
};
