<?php
/*
NOTICE OF LICENSE

This source file is subject to the SafeMageEULA that is bundled with this package in the file LICENSE.txt.

It is also available at this URL: https://www.safemage.com/LICENSE_EULA.txt

Copyright (c)  SafeMage (https://www.safemage.com/)
*/

namespace SafeMage\Adminlog;

class Config
{
    const PATH_DUPLICATED_CACHE_LIFETIME = 'safemage_adminlog/general/duplicated_cache_lifetime';

	const STOP_FILENAME = 'safemage_adminlog.stop';


	public static function getInstance()
	{
		static $inst = null;
		if ($inst === null) {
			$inst = new self();
		}
		return $inst;
	}

	public function getIsEnabled($store = null)
	{
		if ($node = \Mage::getConfig()->getNode('modules')) {
			$modules = $node->children();
			$active = filter_var($modules->SafeMage_Adminlog->active, FILTER_VALIDATE_BOOLEAN);
            return $active;
		}

        return false;
	}

	public function getDuplicatedCacheLifetime($store = null)
    {
        return (int)\Mage::getStoreConfig(self::PATH_DUPLICATED_CACHE_LIFETIME, $store);
    }

	protected function _getStopFilename()
	{
		return self::STOP_FILENAME;
	}

	public function isStop()
	{
        $stopFile = \Mage::getBaseDir('var') . DS . 'log' . DS . $this->_getStopFilename();
		if (file_exists($stopFile)) {
			return true;
		}

		return false;
	}
}
