<?php
/*
NOTICE OF LICENSE

This source file is subject to the SafeMageEULA that is bundled with this package in the file LICENSE.txt.

It is also available at this URL: https://www.safemage.com/LICENSE_EULA.txt

Copyright (c)  SafeMage (https://www.safemage.com/)
*/

namespace SafeMage\Adminlog;

use SafeMage\Adminlog\Config;
use SafeMage\Adminlog\Debug;
use SafeMage\Adminlog\Json;


class Db
{
    protected $_db;

	protected $_dbWrite;

	protected $_rowsToInsert;

	public static function getInstance()
	{
		static $inst = null;
		if ($inst === null) {
			$inst = new self();
		}
		return $inst;
	}

    protected function _getDbSettings()
	{
		$settings = array();
		$config  = \Mage::getConfig()->getResourceConnectionConfig("default_setup");
		$hostPort = (string)$config->host;
		$settings['username']   = (string)$config->username;
		$settings['password']   = (string)$config->password;
		$settings['dbname']     = (string)$config->dbname;
		//$settings['persistent'] = true;  // Really helps

		$aHostPort = explode(':', $hostPort);
		$settings['host'] = $aHostPort[0];
		$settings['port'] = (count($aHostPort) > 1) ? $aHostPort[1] : '3306';

        return $settings;
	}
	
	public function get()
	{
		if ( !$this->_db ) {
			$s = $this->_getDbSettings();
            $sConn = "mysql:host={$s['host']};port={$s['port']};dbname={$s['dbname']};charset=utf8";
			$this->_db = new \PDO($sConn, $s['username'], $s['password']);

            //$this->_db = \Zend_Db::factory('Pdo_Mysql', $s);
		}
		return $this->_db;
	}

	public function getWrite()
	{
		if (!$this->_dbWrite) {
			$this->_dbWrite = new \Varien_Db_Adapter_Pdo_Mysql($this->_getDbSettings());
		}

		return $this->_dbWrite;
	}

	protected function _ifUriLike($uri, $parts)
	{
		foreach($parts as $part) {
			if (stristr($uri, $part)) {
				return true;
			}
		}

		return false;
	}

	protected function _prepareSqlGetTableData($table)
	{
	    $sqlFinal = "SELECT * FROM {$table}";
	    $cases = array(
		    // case 1
		    array(
			    // URI
			    array(
			        '/sales_order/'
				),
				// Callback to get param
				function() { return \Mage::app()->getRequest()->getParam('order_id'); },
			    // Table & field
				array(
				    'sales_flat_order'                => 'entity_id = %s', 
		 	        'sales_flat_order_status_history' => 'parent_id = %s',
				    'sales_flat_order_address'        => 'parent_id = %s', 
		        ),
				// SQL
				"SELECT * FROM %s WHERE %s",
				// Skip tables
				array(
				),
			),
		    // case 2
		    array(
			    // URI
			    array(
					'/sales_order/massHold/',
					'/sales_order/massUnhold/',
					'/sales_order/massCancel/',
					'/sales_order/massPrint/',
					'/ecommerce/masssend/',
				),
				// Callback to get param
				function() { $ids = \Mage::app()->getRequest()->getParam('order_ids'); return implode(',', $ids); },
			    // Table & field
				array(
				    'sales_flat_order'                => 'entity_id IN (%s)', 
		 	        'sales_flat_order_status_history' => 'parent_id IN (%s)',
		        ),
				// SQL
				"SELECT * FROM %s WHERE %s",
				// Skip tables
				array(
				    'cataloginventory_stock_item'
				),
			),	
		    // case 3
		    array(
			    // URI
			    array(
			        '/catalog_product/save/'
				),
				// Callback to get param
				function() { return \Mage::app()->getRequest()->getParam('id'); },
			    // Table & field
				array(
					'catalog_product_entity'               => 'entity_id = %s',
					'catalog_product_entity_varchar'       => 'entity_id = %s',
					'catalog_product_entity_decimal'       => 'entity_id = %s',
					'catalog_product_entity_int'           => 'entity_id = %s',
					'catalog_product_entity_text'          => 'entity_id = %s',
					'catalog_product_entity_datetime'      => 'entity_id = %s',
					'catalog_product_entity_group_price'   => 'entity_id = %s',
					'catalog_product_entity_tier_price'    => 'entity_id = %s',
					'catalog_product_entity_media_gallery' => 'entity_id = %s',

					'catalog_product_flat_1'               => 'entity_id = %s',
					'catalog_product_flat_2'               => 'entity_id = %s',
					'catalog_product_flat_3'               => 'entity_id = %s',
					'catalog_product_flat_4'               => 'entity_id = %s',
					'catalog_product_flat_5'               => 'entity_id = %s',
					'catalog_product_flat_6'               => 'entity_id = %s',
					'catalog_product_flat_7'               => 'entity_id = %s',
					'catalog_product_flat_8'               => 'entity_id = %s',
					'catalog_product_flat_9'               => 'entity_id = %s',
					'catalog_product_flat_10'               => 'entity_id = %s',
					'catalog_product_flat_11'               => 'entity_id = %s',
					'catalog_product_flat_12'               => 'entity_id = %s',
					'catalog_product_flat_13'               => 'entity_id = %s',
					'catalog_product_flat_14'               => 'entity_id = %s',
					'catalog_product_flat_15'               => 'entity_id = %s',
					'catalog_product_flat_16'               => 'entity_id = %s',
					'catalog_product_flat_17'               => 'entity_id = %s',
					'catalog_product_flat_18'               => 'entity_id = %s',
					'catalog_product_flat_19'               => 'entity_id = %s',
					'catalog_product_flat_20'               => 'entity_id = %s',

					'catalog_product_website'              => 'product_id = %s',

					'cataloginventory_stock_item'          => 'product_id = %s',

					//'core_url_rewrite'                     => 'product_id = %s',

					'catalog_category_product'             => 'product_id = %s',

					'weee_discount'                        => 'entity_id = %s',

					'deal_deal_product'                    => 'product_id = %s',
		        ),
				// SQL
				"SELECT * FROM %s WHERE %s",
				// Skip tables
				array(
					'core_url_rewrite'
				),
			),
			// case 4
			array(
				// URI
				array(
					'/customer/save/'
				),
				// Callback to get param
				function() {
					$id = \Mage::app()->getRequest()->getParam('id');
					$customerId = \Mage::app()->getRequest()->getParam('customer_id');
					return $id ? $id : $customerId;
				},
				// Table & field
				array(
					'customer_entity'         => 'entity_id = %s',
					'customer_entity_varchar' => 'entity_id = %s',
					'customer_entity_int'     => 'entity_id = %s',
				),
				// SQL
				"SELECT * FROM %s WHERE %s",
				// Skip tables
				array(
				),
			),
			// case 5
			array(
				// URI
				array(
					'/catalog_category/save/',
					'/catalog_category/move/',
					'/catalog_category/delete/',
				),
				// Callback to get param
				function() { return \Mage::app()->getRequest()->getParam('id'); },
				// Table & field
				array(
					'catalog_category_entity'            => 'entity_id = %s',
					'catalog_category_entity_varchar'    => 'entity_id = %s',
					'catalog_category_entity_int'        => 'entity_id = %s',
					'catalog_category_entity_text'       => 'entity_id = %s',
					'catalog_category_entity_datetime'   => 'entity_id = %s',
					'catalog_category_entity_decimal'    => 'entity_id = %s',
					'catalog_category_flat_store_1'      => 'entity_id = %s',

					'catalog_category_product_index'     => 'category_id = %s',

					'core_url_rewrite'                   => 'category_id = %s',
				),
				// SQL
				"SELECT * FROM %s WHERE %s",
				// Skip tables
				array(
					'core_url_rewrite',
					'catalog_product_entity_varchar'
				),
			),
		);   

		// Render SQL
		foreach($cases as $case) {
		    if (count($case) < 5) {
			    continue;
			}
			
			$uris = $case[0];

			if ($this->_ifUriLike(\Mage::app()->getRequest()->getRequestUri(), $uris)) {

				$value  = $case[1]();
				$tables = $case[2];
				$sqlPattern = $case[3];
				$skipTables = $case[4];

				if (in_array($table, $skipTables)) {
					$sqlFinal = '';
				    break;
				}	
							
				if (isset($tables[$table])) {
					if (empty($value)) {
						$sqlFinal = '';
					} else {
						$tableCondPattern = $tables[$table];
						$tableCond = sprintf($tableCondPattern, $value);
						$sqlFinal = sprintf($sqlPattern, $table, $tableCond);
					}
				}
			}
		}

		//\Mage::log($sqlFinal);
        return $sqlFinal;		
	}

	public function getTableData($table)
	{
		if (Config::getInstance()->isStop()) {
			return array();
		}

		$sql = $this->_prepareSqlGetTableData($table);
		//\Mage::log($sql);
		$timeStart = microtime(true);
		$res = $this->get()->query($sql);  // TOO MUCH QUERIES
		if ( !$res ) {
			return null;
		}

		$durationSec = microtime(true) - $timeStart;
        if (Debug::canLog($durationSec)) {
			$uri = \Mage::app()->getRequest()->getRequestUri();
			$message = sprintf("getTableData(): %.4f seconds - %s - %s", $durationSec, $uri, $sql);
			Debug::log($message);
		}

		$res->setFetchMode(\PDO::FETCH_ASSOC);
		//ini_set('memory_limit', '2096M');
		$data = $res->fetchAll();
		return $data;
	}
	
	public function getTablePrimaryKeys($table)
	{
		$res = $this->get()->query("SHOW KEYS FROM {$table} WHERE key_name = 'PRIMARY'");
		if ( !$res ) {
			return null;
		}

		$res->setFetchMode(\PDO::FETCH_ASSOC);
		$primaryKeys = $res->fetchAll();
		$pks = array();
        if ( $primaryKeys )
		if ( count($primaryKeys) ) {
			foreach($primaryKeys as $pkInfo) {
				$pks[]= $pkInfo['Column_name'];
			}

			return $pks;
		}

		return null;
	}

    /*
	protected function _isDiffDuplicated($type, $id, $change)
    {
        $changeHash = md5($type . $id . serialize($change));

        $cacheKey = 'saved_change_' . $type . '_' . $id;
        if ( $changeHashSaved = \Mage::app()->getCache()->load($cacheKey) ) {
            if ( $changeHashSaved == $changeHash ) {
                return true;
            }
        }

        $cacheLifetime = Config::getInstance()->getDuplicatedCacheLifetime();
        \Mage::app()->getCache()->save($changeHash, $cacheKey, array(), $cacheLifetime);

        return false;
    }
    */
	
	public function saveAllDiff($entities)
	{
		foreach($entities as $type => $a) {
			foreach($a as $id => $diff) {
                /*
				if ( $this->_isDiffDuplicated($type, $id, $diff) ) {
					continue;
				}
                */

				$info = new \Varien_Object(array(
					'object_id' => $id,
					'object' => $type,
					'diff' => $diff
				));
				$this->_saveDiff($info);
				unset($info);
			}
		}	        
	}
	
	protected function _saveDiff($info)
	{
	    if ( Debug::LOG_ENABLED ) {
		    Debug::log('saveDiff');
			Debug::log($info);
		}
		
		$objectId = $info['object_id'];
		$object = $info['object'];
		$diff = $info['diff'];

		foreach($diff as $name => &$change) {
			$change[0] = Json::getInstance()->htmlEncode($change[0]);
			$change[1] = Json::getInstance()->htmlEncode($change[1]);
		}

		$diffJson = \Zend_Json::encode($diff);
		//$diffJson = Db::getInstance()->get()->quote($diffJson);
		//$diffJson = \Zend_Json::prettyPrint($diffJson,  array("indent" => "  "));

		$storeId = \Mage::app()->getRequest()->getParam('store');
		$userId = Session::getUser() ? Session::getUser()->getUserId() : null;
		$ip = \Mage::helper('core/http')->getRemoteAddr();
        $actionName = implode('_', array(
            \Mage::app()->getRequest()->getModuleName(),
            \Mage::app()->getRequest()->getControllerName(),
            \Mage::app()->getRequest()->getActionName()
        ));
		$createdAt = \Mage::getModel('core/date')->gmtDate();

        $data = array(
            'object_id'   => $objectId,
            'object'      => $object,
            'diff'        => $diffJson,
            'store_id'    => (int)$storeId,
            'user_id'     => $userId,
            'ip'          => $ip,
            'action_name' => $actionName,
            'created_at'  => $createdAt,
        );

		if (!$this->_rowsToInsert) {
			$this->_rowsToInsert = array();
		}
		$this->_rowsToInsert[]= $data;

		/*
		try {
            $db = \Zend_Db::factory('Pdo_Mysql', $this->_getDbSettings());
            $db->insert('safemage_adminlog', $data);
		} catch (\Exception $e) {
			Debug::log('Exception');
		    Debug::log($e->getMessage());
		}
		*/
	}

	public function log()
	{
		if ($this->_rowsToInsert) {
			try {
				$db = $this->getWrite();
				$db->insertMultiple('safemage_adminlog', $this->_rowsToInsert);
			} catch (\Exception $e) {
				Debug::log($e->getMessage());
			}
		}
	}
}





