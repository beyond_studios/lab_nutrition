<?php
/*
NOTICE OF LICENSE

This source file is subject to the SafeMageEULA that is bundled with this package in the file LICENSE.txt.

It is also available at this URL: https://www.safemage.com/LICENSE_EULA.txt

Copyright (c)  SafeMage (https://www.safemage.com/)
*/

namespace SafeMage\Adminlog;

class Cache
{
	public static function getInstance()
	{
		static $inst = null;
		if ($inst === null) {
			$inst = new self();
		}
		return $inst;
	}

    public function save($data, $key, $tags = null)
	{
	    $json = \Zend_Json::encode($data);
	    \Mage::app()->getCache()->save($json, $key, $tags);
	}
	
	public function load($key)
	{
	    $json = \Mage::app()->getCache()->load($key);
		try {
			$data = \Zend_Json::decode($json);
			return $data;
		} catch (\Exception $e) {

		}

		return false;
	}
}
