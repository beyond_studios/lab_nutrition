<?php
/*
NOTICE OF LICENSE

This source file is subject to the SafeMageEULA that is bundled with this package in the file LICENSE.txt.

It is also available at this URL: https://www.safemage.com/LICENSE_EULA.txt

Copyright (c)  SafeMage (https://www.safemage.com/)
*/

namespace SafeMage\Adminlog;

use SafeMage\Adminlog\Db;
use SafeMage\Adminlog\Monitor\Table;
use SafeMage\Adminlog\Parser\Sql;
//use SafeMage\Adminlog\Monitor\Table\Diff;
//use SafeMage\Adminlog\Reflector;
//use SafeMage\Adminlog\Config;

class Monitor
extends \Varien_Object
{
    const BACKTRACE_LIMIT = 14;

    public static function getInstance()
    {
        static $inst = null;
        if ($inst === null) {
            $inst = new self();
        }
        return $inst;
    }

    protected function _getResources($trace)
    {
        $resources = array();
        foreach($trace as $i => $data) {
            if (isset($data['object'])) {
                if ( $data['object'] instanceof \Mage_Core_Model_Resource_Abstract ) {
                    $resources[]= $data['object'];
                }
            }
        }

        return $resources;
    }

    public function onQueryStart($sql)
    {
        /*
        # V1
        if ( $adapter = $this->backtraceQueryStart($sql) ) {
            $this->setObject($this);
            $this->setMethod('onQueryHook');
            $adapter->setQueryHook($this);
        }
        */

        # V2
        if ( $this->_isEnabled() ) {
            if ($tableName = Sql::getInstance()->parseTableNameFromSql($sql)) {

                //$trace = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, 14);
                $trace = debug_backtrace();   // SafeMage Fix

                $resources = $this->_getResources($trace);
                $resource = array_shift($resources);
                Table::getInstance()->add($tableName, $resource);
            }
        }
    }

    public function onQueryEnd()
    {
        /*
        # V1
        if ( $adapter = $this->backtraceQueryEnd() ) {
            Monitor\Table::getInstance()->onQueryEnd();
        }
        */

        /*
        # V2
        if ( $this->_isEnabled() ) {
            Monitor\Table::getInstance()->onQueryEnd();
        }
        */
    }

    protected function _isEnabled()
    {
        if ( !Config::getInstance()->getIsEnabled() ) {
            return false;
        }

        $res = Session::getUser() ? true : false;
        return $res;
    }

    public function __destruct()
    {
        Table::getInstance()->onQueryEnd();
    }
}
