<?php
/*
NOTICE OF LICENSE

This source file is subject to the SafeMageEULA that is bundled with this package in the file LICENSE.txt.

It is also available at this URL: https://www.safemage.com/LICENSE_EULA.txt

Copyright (c)  SafeMage (https://www.safemage.com/)
*/

namespace SafeMage\Adminlog\Parser;

class Sql
{
	public static function getInstance()
	{
		static $inst = null;
		if ($inst === null) {
			$inst = new self();
		}
		return $inst;
	}

    public function getMainTable()
    {
        return 'safemage_adminlog';
    }

    public function parseTableNameFromInsert($sql)
	{
		$result = preg_match("/INSERT([ ]+)INTO([ ]+)`([\S]+)`/", $sql, $match);
		if ( $match )
		if ( !empty($match[3]) ) {
			return $match[3];
		}

		return null;
	}

	public function parseTableNameFromUpdate($sql)
	{
		$result = preg_match("/UPDATE([ ]+)`([\S]+)`/", $sql, $match);
		if ( $match )
		if ( !empty($match[2]) ) {
			return $match[2];
		}

		return null;
	}

	public function parseTableNameFromDelete($sql)
	{
		$result = preg_match("/DELETE([ ]+)FROM([ ]+)`([\S]+)`/", $sql, $match);
		if ( $match )
			if ( !empty($match[3]) ) {
				return $match[3];
			}

		return null;
	}

	public function parseTableNameFromSql($sql)
	{
		if ( !is_string($sql) ) {
			return null;
		}

		$tableNameParsed = null;
		if ( $tableName = $this->parseTableNameFromInsert($sql) ) {
			$tableNameParsed = $tableName;
		} elseif ( $tableName = $this->parseTableNameFromUpdate($sql) ) {
			$tableNameParsed = $tableName;
		} elseif ( $tableName = $this->parseTableNameFromDelete($sql) ) {
			$tableNameParsed = $tableName;
		}

        if ($tableNameParsed == $this->getMainTable()) {
            return null;
        }

		return $tableNameParsed;
	}
}