<?php
/*
NOTICE OF LICENSE

This source file is subject to the SafeMageEULA that is bundled with this package in the file LICENSE.txt.

It is also available at this URL: https://www.safemage.com/LICENSE_EULA.txt

Copyright (c)  SafeMage (https://www.safemage.com/)
*/

namespace SafeMage\Adminlog;

class Debug
{
	const LOG_FILE = 'safemage_adminlog.log';

	const LOG_ENABLED = 0;

	const LOG_EXTENDED_ENABLED = 0;

	const LOG_PERFORMANCE_ENABLED = 1;

	const CRITICAL_SECONDS = 0.01;

    public static function log($data)
	{
	    \Mage::log($data, null, self::LOG_FILE);
	}

	public static function canLog($durationSec)
    {
	    $can = self::LOG_PERFORMANCE_ENABLED && ($durationSec >= self::CRITICAL_SECONDS);
		return $can;
    }
}
