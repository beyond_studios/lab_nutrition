<?php
/*
NOTICE OF LICENSE

This source file is subject to the SafeMageEULA that is bundled with this package in the file LICENSE.txt.

It is also available at this URL: https://www.safemage.com/LICENSE_EULA.txt

Copyright (c)  SafeMage (https://www.safemage.com/)
*/

namespace SafeMage\Adminlog;

class Session
{
    public static function getUser()
	{
        /*
		if ( isset($_SESSION) ) {
			if ($_SESSION['admin']['user']) {
				if ($_SESSION['admin']['user']->getUserId()) {
					return $_SESSION['admin']['user'];
				}
			}
		}
        */

		/*
		if ($user = \Mage::getSingleton('admin/session')->getUser()) {
			if ($user->getUserId()) {
				return $user;
			}
		}
		*/

		// FIX for 1.9.3.2
		$ns = \Zend_Session::namespaceGet('admin');
		if (!empty($ns['user'])) {
			$user = $ns['user'];
			if ($user->getUserId()) {
				return $user;
			}
		}

		return null;
	}
}
