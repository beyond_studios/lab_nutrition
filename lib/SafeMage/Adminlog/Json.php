<?php
/*
NOTICE OF LICENSE

This source file is subject to the SafeMageEULA that is bundled with this package in the file LICENSE.txt.

It is also available at this URL: https://www.safemage.com/LICENSE_EULA.txt

Copyright (c)  SafeMage (https://www.safemage.com/)
*/

namespace SafeMage\Adminlog;

class Json
{
	public static function getInstance()
	{
		static $inst = null;
		if ($inst === null) {
			$inst = new self();
		}
		return $inst;
	}
	
	public function htmlEncode($contents)
	{
		$from = array(
		  "\r",
		  "\n",
		  //"'"
		);
		$to = array(
		  "",
		  "\\n",
		  //'"'
		);
		$contents = str_replace($from, $to, $contents);
		//$contents = addslashes($contents);
		return $contents;
	}
	
	public function htmlDecode($contents)
	{
	    //$json = str_replace(array("\r", "\n"), array("", ""), $json);
		$contents = str_replace(array("\\n"), array("\n"), $contents);
		return $contents;
	}
}
