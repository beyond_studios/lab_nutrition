<?php
/*
NOTICE OF LICENSE

This source file is subject to the SafeMageEULA that is bundled with this package in the file LICENSE.txt.

It is also available at this URL: https://www.safemage.com/LICENSE_EULA.txt

Copyright (c)  SafeMage (https://www.safemage.com/)
*/

namespace SafeMage\Adminlog\Monitor\Table;

use SafeMage\Adminlog\Reflector;
use SafeMage\Adminlog\Debug;
use SafeMage\Adminlog\Cache;
use SafeMage\Adminlog\Config;
use SafeMage\Adminlog\Db;
use SafeMage\Adminlog\Skip;


class Diff
extends \Varien_Object
{
    //protected $_eavEntities;

    //protected $_flatEntities;

    protected $_entities;

    public static function getInstance()
    {
        static $inst = null;
        if ($inst === null) {
            $inst = new self();
        }
        return $inst;
    }

    public function processRow($rowKey, $from, $to, $resource)
    {
        if ( empty($resource) ) {
            return;
        }

        if (Skip::getInstance()->resourceObject($resource)) {
            return;
        }

        $idField = $resource->getIdFieldName();

        // EAV entity
        if ( method_exists($resource, 'getEntityType') ) {
            //$objType = $resource->getType();

            // Non-Flat mode
            if (is_object($resource->getEntityType())) {
                $objType = $resource->getEntityType()->getEntityModel();
            } else {
                return;
            }

            if ( $this->_isEavRow($from) || $this->_isEavRow($to) ) {
                $this->processEavRow($rowKey, $from, $to, $objType);
            } else {
                $this->processFlatRow($rowKey, $from, $to, $objType, $idField);
            }

        // Flat entity
        } else {
            $objType = Reflector::getInstance()->getFlatResourceAlias($resource);
            $this->processFlatRow($rowKey, $from, $to, $objType, $idField);
        }
    }

    protected function _isEavRow($row)
    {
        if ( isset($row['attribute_id']) && isset($row['entity_id']) && isset($row['value']) ) {
            return true;
        }

        return false;
    }

    public function processEavRow($rowKey, $from, $to, $type)
    {
        if ( empty($this->_entities[$type]) ) {
            $this->_entities[$type]= array();
        }
        $entitiesOfType = &$this->_entities[$type];

        $entityId = $from ? (int)$from['entity_id'] : (int)$to['entity_id'];
        $attr = $from ? $this->getAttribute((int)$from['attribute_id']) : $this->getAttribute((int)$to['attribute_id']);
        $field = $attr['attribute_code'];

        if ( empty($entitiesOfType[$entityId]) ) {
            $entitiesOfType[$entityId]= array();
        }
        $entity = &$entitiesOfType[$entityId];

        // ADD
        if ( empty($from) && $to ) {
            //\Mage::log('ADD');
            //\Mage::log($to);

            $entity[$field]= array(null, $to['value']);

        // EDIT
        } else if ( $from && $to ) {
            //\Mage::log('EDIT');
            //\Mage::log($from);
            //\Mage::log($to);

            $entity[$field]= array($from['value'], $to['value']);

        // DELETE
        } else if ( $from && empty($to) ) {
            //\Mage::log('DELETE');
            //\Mage::log($from);


            $entity[$field]= array($from['value'], null);
        }
    }

    public function getAttribute($attributeId)
    {
        $attributes = Cache::getInstance()->load('safemage_eav_attributes');
        if ( !$attributes ) {
            $collection = \Mage::getResourceModel('eav/entity_attribute_collection');
            $attributes = array();
            foreach($collection as $attribute) {
                $attributes[$attribute->getAttributeId()] = array(
                    'attribute_code' => $attribute->getAttributeCode(),
                );
            }

            Cache::getInstance()->save($attributes, 'safemage_eav_attributes', array('EAV'));
        }

        if ( isset($attributes[$attributeId]) ) {
            return $attributes[$attributeId];
        }

        return null;
    }

    protected function isOfNumeralsOnly($rowKey)
    {
        preg_match("/[0-9,]+/i", $rowKey, $matches);
        if ( isset($matches[0]) ) {
            if ( $matches[0] == $rowKey ) {
                return true;
            }
        }
        return false;
    }


    public function processFlatRow($rowKey, $from, $to, $type, $idField)
    {
        if ( empty($this->_entities[$type]) ) {
            $this->_entities[$type]= array();
        }
        $entitiesOfType = &$this->_entities[$type];

        $entityId = $from ? (int)$from[$idField] : (int)$to[$idField];
        if ( empty($entityId) ) {                   // for eav/attribute_option editing
            $entityId = $rowKey;
        }

        // for core/cache_option, ..
        if ( !$this->isOfNumeralsOnly($rowKey) ) {
            //$entityId = $rowKey;

            $entityId = $type;
            if ( empty($from) && $to ) {
                $to = array($rowKey => $to['value']);
            } else if ( $from && $to ) {
                $from = array($rowKey => $from['value']);
                $to = array($rowKey => $to['value']);
            } else if ( $from && empty($to) ) {
                $from = array($rowKey => $from['value']);
            }
        }

        // for core/config_data
        $some = $from ? $from : $to;
        if ( isset($some['config_id']) && isset($some['path']) ) {

            $path2 = $some['path'];
            $entityId = $type;
            $key = $rowKey . ' ' . $path2;

            if ($to) {
                $to = array($key => $to['value']);
            }
            if ($from) {
                $from = array($key => $from['value']);
            }
        }


        if ( empty($entitiesOfType[$entityId]) ) {
            $entitiesOfType[$entityId]= array();
        }
        $entity = &$entitiesOfType[$entityId];

        // ADD
        if ( empty($from) && $to ) {
            //\Mage::log('ADD');
            //\Mage::log($to);

            foreach($to as $key => $value) {
                $entity[$key]= array(null, $value);
            }

        // EDIT
        } else if ( $from && $to ) {
            //\Mage::log('EDIT');
            //\Mage::log($from);
            //\Mage::log($to);

            $keysDiff = @array_keys(array_diff_assoc($from, $to));
            foreach($keysDiff as $key) {
                $entity[$key]= array($from[$key], $to[$key]);
            }

        // DELETE
        } else if ( $from && empty($to) ) {
            //\Mage::log('DELETE');
            //\Mage::log($from);


            foreach($from as $key => $value) {
                $entity[$key]= array($value, null);
            }
        }
    }

    /*
    public function start()
    {
        $this->_entities = array();
    }
    */

    /*
    protected function _isDuplicated($type, $id, $change)
    {
        $changeHash = md5($type . $id . serialize($change));

        $cacheKey = 'saved_change_' . $type . '_' . $id;
        if ( $changeHashSaved = \Mage::app()->getCache()->load($cacheKey) ) {
            if ( $changeHashSaved == $changeHash ) {
                return true;
            }
        }

        $cacheLifetime = Config::getInstance()->getDuplicatedCacheLifetime();
        \Mage::app()->getCache()->save($changeHash, $cacheKey, array(), $cacheLifetime);

        return false;
    }
    */

    public function complete()
    {
        if ( !empty($this->_entities) ) {

            if ( Debug::LOG_ENABLED ) {
                //Debug::log('complete');
                //Debug::log($this->_entities);
            }

            Db::getInstance()->saveAllDiff($this->_entities);
            Db::getInstance()->log();
        }
    }
}