<?php
/*
NOTICE OF LICENSE

This source file is subject to the SafeMageEULA that is bundled with this package in the file LICENSE.txt.

It is also available at this URL: https://www.safemage.com/LICENSE_EULA.txt

Copyright (c)  SafeMage (https://www.safemage.com/)
*/

namespace SafeMage\Adminlog\Monitor;

use SafeMage\Adminlog\Monitor\Table\Diff;
use SafeMage\Adminlog\Debug;
use SafeMage\Adminlog\Db;
use SafeMage\Adminlog\Reflector;
use SafeMage\Adminlog\Skip;


class Table
{
	const ENABLED = 1;

    protected static $_tableToLog;

	protected static $_tableToLogResource;


	protected $_tableToSkip = array(
		'cron_schedule',
		'index_process',

		'catalogrule_product_price',
		'sales_flat_quote',
		'tag_summary',
		'm_searchindex',
		'catalogsearch_fulltext',
		'catalog_product_index_price',
		'catalog_product_index_group_price',
		'catalog_product_index_tier_price',
		'catalog_product_index_website',
		'catalog_product_index_eav_decimal',
		'catalog_product_index_eav',
		'catalog_product_entity_media_gallery_value',
		'catalogrule_affected_product',
		'index_process_event',
		'index_event',
		'cataloginventory_stock_status',

		'sales_flat_order_item',
		'sales_flat_order_payment',
		'sales_flat_order_grid',

		'sales_flat_quote_address',
		'sales_flat_quote_item',
		'sales_flat_quote_item_option',
		'sales_flat_quote_payment',
		
		'quoteadv_log_admin',
		'quoteadv_group_allow',
		'searchanise_queue',
		'catalogsearch_query',
		'catalogsearch_result',
		
		'udropship_vendor_product_assoc',
		'udropship_vendor_product',
		
		'udmulti_group_price',
		'udmulti_tier_price',
		
		'smtppro_email_log',
		'core_flag',
		'sales_flat_quote_shipping_rate',

		'contactlab_commons_log_entity',
	);

	protected $_skipTableLike = array(
		'_tmp',
		'_idx',
		'_index',
		'quoteadv_',
	);

	protected $_fieldsToSkip = array(
		'customer_entity' => array(
		    'updated_at'
		),
		'customer_address_entity' => array(
			'updated_at'
		),
		'catalog_product_entity' => array(
			'updated_at'
		),
		'catalog_category_entity' => array(
			'updated_at'
		),
		'cataloginventory_stock_item' => array(
			'low_stock_date'
		)
	);

	protected $_resourcesToSkip = array(
		//'core/url_rewrite',
		'cron/schedule',
		'index/process',
		'catalogsearch/fulltext',
		'adminnotification/inbox',
		'catalog/product_index_eav',
		'catalog/product_index_price',
		'catalog/category_product_index',
        'reports/event',
		'admin/rule',

		'sales/quote_address',
		'sales/quote_payment',
		
		'core/flag',
	);

    protected $_skipActionsLike = array(
        '_massReindex'
    );

	protected $_skipUriLike = array(
		'/import/start',
		'system_convert_gui',
		'system_convert_profile',
	);

	protected static $_data;

	protected $_db;

	protected $_oDiff;

    protected $_hashCache = array();

    protected $_tableDataCache = array();


    public static function getInstance()
	{
		static $inst = null;
		if ($inst === null) {
			$inst = new self();
		}
		return $inst;
	}

	protected function _init()
	{
		if ( !self::$_tableToLog ) {
			self::$_tableToLog = array();
			self::$_tableToLogResource = array();
		}
	}

	public function exist($table)
	{
		$this->_init();

		$res = in_array($table, self::$_tableToLog);
		return $res;
	}

	public function skip($table)
	{
        $res = in_array($table, $this->_tableToSkip);
		return $res;
	}

	public function skipLike($table)
	{
		foreach($this->_skipTableLike as $tablePart) {
			if (stristr($table, $tablePart)) {
				return true;
			}
		}

        return false;
	}

	public function skipField($table, $field)
	{
		if ( !isset($this->_fieldsToSkip[$table]) ) {
			return false;
		}

		$res = in_array($field, $this->_fieldsToSkip[$table]);
		return $res;
	}

	public function skipResource($alias)
	{
		$res = in_array($alias, $this->_resourcesToSkip);
		return $res;
	}

	public function add($table, $resource = null)
	{
		if ( !self::ENABLED ) {
			return;
		}

		$this->_init();

		if ($this->skip($table) || $this->exist($table)) {
			return;
		}

		if ($this->skipLike($table)) {
			return;
		}

		self::$_tableToLog[]= $table;
		if ( $resource ) {
			self::$_tableToLogResource[$table]= $resource;
		}

		if ( $data = Db::getInstance()->getTableData($table) ) {
			//\Mage::log('add getTableData() ' . $table);         // 7 times (!)
			self::$_data[$table]= $data;
		}
	}

    public function getSavedData($table)
	{
		$data = isset(self::$_data[$table]) ? self::$_data[$table] : array();
		return $data;
	}

	public function getAll()
	{
		$this->_init();

		return self::$_tableToLog;
	}

	public function getResource($table)
	{
		$resource = isset(self::$_tableToLogResource[$table]) ? self::$_tableToLogResource[$table] : null;
		return $resource;
	}

    public function skipActionLike($actionName)
    {
        foreach($this->_skipActionsLike as $str) {
            if (stristr($actionName, $str)) {
                return true;
            }
        }

        return false;
    }

	public function skipUriLike($uri)
	{
		foreach($this->_skipUriLike as $uriLike) {
			if (stristr($uri, $uriLike)) {
				return true;
			}
		}

		return false;
	}

	public function onQueryEnd()
	{
        $actionName = \Mage::app()->getRequest()->getModuleName() . '_' . \Mage::app()->getRequest()->getControllerName() . '_' . \Mage::app()->getRequest()->getActionName();
		$uri = \Mage::app()->getRequest()->getRequestUri();
		if ($this->skipActionLike($actionName)) {
            return;
        }

		if ($this->skipUriLike($uri)) {
			return;
		}

        if ( !self::ENABLED ) {
			return;
		}

        $tables = $this->getAll();
		if ( count($tables) ) {
			//\Mage::log($tables);
			foreach ($tables as $table) {
				$resource = $this->getResource($table);

				$b = Db::getInstance()->getTableData($table);
				//\Mage::log('onQueryEnd getTableData() ' . $table);  // 138 times (!!!!!)
				$a = $this->getSavedData($table);

				/*
				$hash = $this->getHash($a, $b);
				if (in_array($hash, $this->_hashCache)) {
				} else {
					$this->_hashCache[] = $hash;

					$this->_diff($table, $a, $b);
				}
                */

				$this->_diff($table, $a, $b);
			}
		}

		Diff::getInstance()->complete();
	}

	protected function _deleteSkippedFields($table, &$a)
	{
		foreach($a as $field => $value) {
			if ( $this->skipField($table, $field) ) {
				unset($a[$field]);
			}
		}
	}

	protected function _setAssocKeyAsDbPrimary($table, $a)
	{
		$pks = Db::getInstance()->getTablePrimaryKeys($table);

		$newA = array();
		if ( count($a) ) {
			if ( count($pks) == 1 ) {
				$pk = current($pks);
				foreach($a as $row) {
					$key = $row[$pk];
					$newA[$key]= $row;
				}
			} elseif ( count($pks) > 1 ) {
				foreach($a as $row) {
					$keyValues = array();
					foreach($pks as $ind => $pk) {
						$keyValues[]= $row[$pk];
					}
					$keys = implode(',', $keyValues);
					$newA[$keys]= $row;
				}
			}
		}

		return $newA;
	}

	protected function _diff($table, $a, $b)
    {
		$a = $this->_setAssocKeyAsDbPrimary($table, $a);
		$b = $this->_setAssocKeyAsDbPrimary($table, $b);

        $keysA = array_keys($a);
		$keysB = array_keys($b);

		$keys = array_unique( array_merge($keysA, $keysB) );
        $resource = isset(self::$_tableToLogResource[$table]) ? self::$_tableToLogResource[$table] : null;

		if (Skip::getInstance()->resourceObject($resource)) {
			return;
		}

		$objType = null;
		// EAV entity
		if ( method_exists($resource, 'getType') ) {
			$objType = $resource->getType();
		// Flat entity
		} else {
			$objType = Reflector::getInstance()->getFlatResourceAlias($resource);
		}
		if ( $this->skipResource($objType) ) {
			return;
		}


		foreach($keys as $key) {
			// ADD
			if ( (!isset($a[$key])) && isset($b[$key]) ) {
				$rowB = $b[$key];

				if ( Debug::LOG_EXTENDED_ENABLED ) {
					Debug::log($table . ' ADD (' . get_class($resource) . ') ' . $resource->getIdFieldName());
					Debug::log($rowB);
				}

				Diff::getInstance()->processRow($key, null, $rowB, $resource);

			// EDIT
			} elseif ( isset($a[$key]) && isset($b[$key]) ) {
				$rowA = $a[$key];
				$rowB = $b[$key];

                $diff = array_diff_assoc($rowB, $rowA); // FIX !!!  array_diff() is WRONG

				if ( count($diff) ) {

					$this->_deleteSkippedFields($table, $diff);
                    if ( count($diff) ) {

						$diffKeys = array_keys($diff);
						$rowAdiff = array();
						foreach($rowA as $keyA => $valA) {
							if ( in_array($keyA, $diffKeys) ) {
								$rowAdiff[$keyA]= $valA;
							}
						}

						$rowBdiff = array();
						foreach($rowB as $keyB => $valB) {
							if ( in_array($keyB, $diffKeys) ) {
								$rowBdiff[$keyB]= $valB;
							}
						}

						if ( Debug::LOG_EXTENDED_ENABLED ) {
							Debug::log($table . ' EDIT (' . get_class($resource) . ') ' . $resource->getIdFieldName());

							Debug::log('FROM');
							Debug::log($rowA);
							//Debug::log($rowAdiff);

							Debug::log('TO');
							Debug::log($rowB);
							//Debug::log($rowBdiff);
						}

						Diff::getInstance()->processRow($key, $rowA, $rowB, $resource);

					} else {

					}
				}

			// DELETE
			} elseif ( isset($a[$key]) && (!isset($b[$key])) ) {
				$rowA = $a[$key];

				if ( Debug::LOG_EXTENDED_ENABLED ) {
					Debug::log($table . ' DELETE (' . get_class($resource) . ') ' . $resource->getIdFieldName());
					Debug::log($rowA);
				}

				Diff::getInstance()->processRow($key, $rowA, null, $resource);
			}
		}
	}

	/*
	public function getHash($a, $b)
	{
		// Memory consumtion fix
		$sa = serialize($a);
		$hash = crc32($sa);
		unset($a);
		unset($sa);

		$sb = serialize($b);   // Out of memory
		$hash .= crc32($sb);
		unset($b);
		unset($sb);

		return $hash;
	}
    */
}