<?php
/*
NOTICE OF LICENSE

This source file is subject to the SafeMageEULA that is bundled with this package in the file LICENSE.txt.

It is also available at this URL: https://www.safemage.com/LICENSE_EULA.txt

Copyright (c)  SafeMage (https://www.safemage.com/)
*/

namespace SafeMage\Adminlog;

class Reflector
{
	public static function getInstance()
	{
		static $inst = null;
		if ($inst === null) {
			$inst = new self();
		}
		return $inst;
	}

    public function getFlatResourceAlias($oResource)
    {
	    $reflectionClass = new \ReflectionClass(get_class($oResource));

		$resourceModel = '';
		$mainTable = '';

		if ($reflectionClass->hasProperty('_resourceModel')) {
			$reflectionProperty = $reflectionClass->getProperty('_resourceModel');
			$reflectionProperty->setAccessible(true);
			$resourceModel = $reflectionProperty->getValue($oResource);
		}

		if ($reflectionProperty = $reflectionClass->hasProperty('_mainTable')) {
			$reflectionProperty = $reflectionClass->getProperty('_mainTable');
			$reflectionProperty->setAccessible(true);
			$mainTable = $reflectionProperty->getValue($oResource);
		}

		$alias = "{$resourceModel}/{$mainTable}";
		return $alias;
	}	
}
