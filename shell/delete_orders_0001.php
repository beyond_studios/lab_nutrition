<?php
require_once 'abstract.php';

class Shell_Delete_Orders extends Mage_Shell_Abstract
{
    /**
     * Parse string with id's and return array
     *
     * @param string $string
     * @return array
     */
    protected function _parseString($string)
    {
        $ids = array();
        if (!empty($string)) {
            $ids = explode(',', $string);
            $ids = array_map('trim', $ids);
        }
        return $ids;
    }
    /**
     * Run script
     *
     */
    public function run()
    {
        if ($this->getArg('orders'))
        {
            if($this->getArg('orders') == 'all')
            {
                $collection = Mage::getModel('sales/order')->getCollection();
                foreach($collection as $order)
                {
                    $id = $order->getId();
                    try{
                        $order->delete();
                        echo "order #".$id." is removed".PHP_EOL;
                    }catch(Exception $e){
                        echo "order #".$id." could not be removed: ".$e->getMessage().PHP_EOL;
                    }
                }
                echo "complete.".PHP_EOL;
            }
            else
            {
                $ids = $this->_parseString($this->getArg('orders'));
                foreach($ids as $id){
                    try{
                        Mage::getModel('sales/order')->load($id)->delete();
                        echo "order #".$id." is removed".PHP_EOL;
                    }catch(Exception $e){
                        echo "order #".$id." could not be removed: ".$e->getMessage().PHP_EOL;
                    }
                }
                echo "complete.".PHP_EOL;
            }
        } else {
            echo $this->usageHelp();
        }
    }
 
    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php -f delete_orders.php -- [options]
 
  --orders all                          Delete all orders
  --orders <order_id>                   Delete order by id
  help                                  This help
 
  <order_id>               Comma separated id's of orders
 
USAGE;
    }
}
 
$shell = new Shell_Delete_Orders();
$shell->run();
?>
