<?php
class Strobe_VisaNet_EticketController extends Mage_Core_Controller_Front_Action
{
    
    const XML_PATH_ENABLED      = "payment/visanet_eticket/active";
    const XML_PATH_ENVIRONMENT  = "payment/visanet_eticket/environment";
        
    const XML_PATH_CODE_SANDBOX  = "payment/visanet_eticket/merchant_code_sandbox";
    const XML_PATH_URL_SUCCESS_SANDBOX  = "payment/visanet_eticket/transaction_ok_sandbox";
    
    const XML_PATH_CODE_PROD  = "payment/visanet_eticket/merchant_code";
    const XML_PATH_URL_SUCCESS_PROD  = "payment/visanet_eticket/transaction_ok";
    
    
    public function preDispatch()
    {
        parent::preDispatch();

        if( !Mage::getStoreConfigFlag(self::XML_PATH_ENABLED) ) {
            $this->norouteAction();
        }
        
        
        $COD_ACCION = array("101" => "Operación Denegada. Tarjeta Vencida. Verifique los datos en su tarjeta e ingréselos correctamente.",
                             "102" => "Operación Denegada. Contactar con entidad emisora de su tarjeta.",
                             "104" => "Operación Denegada. Operación no permitida para esta tarjeta. Contactar con la entidad emisora de su tarjeta.",
                             "106" => "Operación Denegada. Intentos de clave secreta excedidos. Contactar con la entidad emisora de su tarjeta.",
                             "107" => "Operación Denegada. Contactar con la entidad emisora de su tarjeta.",
                             "108" => "Operación Denegada. Contactar con la entidad emisora de su tarjeta.",
                             "109" => "Operación Denegada. Contactar con el comercio.",
                             "110" => "Operación Denegada. Operación no permitida para esta tarjeta. Contactar con la entidad emisora de su tarjeta.",
                             "111" => "Operación Denegada. Contactar con el comercio.",
                             "112" => "Operación Denegada. Se requiere clave secreta.",
                             "116" => "Operación Denegada. Fondos insuficientes. Contactar con entidad emisora de su tarjeta",
                             "117" => "Operación Denegada. Clave secreta incorrecta.",
                             "118" => "Operación Denegada. Tarjeta Inválida. Contactar con entidad emisora de su tarjeta.",
                             "119" => "Operación Denegada. Intentos de clave secreta excedidos. Contactar con entidad emisora de su tarjeta.",
                             "121" => "Operación Denegada.",
                             "126" => "Operación Denegada. Clave secreta inválida.",
                             "129" => "Operación Denegada. Código de seguridad invalido. Contactar con entidad emisora de su tarjeta",
                             "180" => "Operación Denegada. Tarjeta Inválida. Contactar con entidad emisora de su tarjeta.",
                             "181" => "Operación Denegada. Tarjeta con restricciones de débito. Contactar con entidad emisora de su tarjeta.",
                             "182" => "Operación Denegada. Tarjeta con restricciones de crédito. Contactar con entidad emisora de su tarjeta.",
                             "183" => "Operación Denegada. Problemas de comunicación. Intente más tarde.",
                             "190" => "Operación Denegada. Contactar con entidad emisora de su tarjeta.",
                             "191" => "Operación Denegada. Contactar con entidad emisora de su tarjeta.",
                             "192" => "Operación Denegada. Contactar con entidad emisora de su tarjeta.",
                             "199" => "Operación Denegada.",
                             "201" => "Operación Denegada. Tarjeta vencida. Contactar con entidad emisora de su tarjeta.",
                             "202" => "Operación Denegada. Contactar con entidad emisora de su tarjeta",
                             "204" => "Operación Denegada. Operación no permitida para esta tarjeta. Contactar con entidad emisora de su tarjeta.",
                             "206" => "Operación Denegada. Intentos de clave secreta excedidos. Contactar con la entidad emisora de su tarjeta.",
                             "207" => "Operación Denegada. Contactar con entidad emisora de su tarjeta..",
                             "208" => "Operación Denegada. Contactar con entidad emisora de su tarjeta.",
                             "209" => "Operación Denegada. Contactar con entidad emisora de su tarjeta.",                             
                             "263" => "Operación Denegada. Contactar con el comercio.",
                             "264" => "Operación Denegada. Entidad emisora de la tarjeta no está disponible para realizar la autenticación.",
                             "265" => "Operación Denegada. Clave secreta del tarjetahabiente incorrecta. Contactar con entidad emisora de su tarjeta.",
                             "266" => "Operación Denegada. Tarjeta Vencida. Contactar con entidad emisora de su tarjeta.",
                             "280" => "Operación Denegada. Clave secreta errónea. Contactar con entidad emisora de su tarjeta.",
                             "290" => "Operación Denegada. Contactar con entidad emisora de su tarjeta.",
                             "300" => "Operación Denegada. Número de pedido del comercio duplicado. Favor no atender.",
                             "306" => "Operación Denegada. Contactar con entidad emisora de su tarjeta.",
                             "401" => "Operación Denegada. Contactar con el comercio.",
                             "402" => "Operación Denegada.",
                             "403" => "Operación Denegada. Tarjeta no autenticada.",
                             "404" => "Operación Denegada. Contactar con el comercio.",
                             "405" => "Operación Denegada. Contactar con el comercio.",
                             "406" => "Operación Denegada. Contactar con el comercio.",
                             "407" => "Operación Denegada. Contactar con el comercio.",
                             "408" => "Operación Denegada. Código de seguridad no coincide. Contactar con entidad emisora de su tarjeta",
                             "409" => "Operación Denegada. Código de seguridad no procesado por la entidad emisora de la tarjeta",
                             "410" => "Operación Denegada. Código de seguridad no ingresado.",
                             "411" => "Operación Denegada. Código de seguridad no procesado por la entidad emisora de la tarjeta",
                             "412" => "Operación Denegada. Código de seguridad no reconocido emisora de la tarjeta",
                             "413" => "Operación Denegada. Contactar con entidad emisora de su tarjeta.",
                             "414" => "Operación Denegada.",
                             "415" => "Operación Denegada.",
                             "416" => "Operación Denegada.",
                             "417" => "Operación Denegada.",
                             "418" => "Operación Denegada.",
                             "419" => "Operación Denegada.",
                             "420" => "Operación Denegada. Tarjeta no es VISA.",
                             "421" => "Operación Denegada. Contactar con entidad emisora de su tarjeta.",
                             "422" => "Operación Denegada. El comercio no está configurado para usar este medio de pago. Contactar con el comercio.",
                             "423" => "Operación Denegada. Se canceló el proceso de pago.",
                             "424" => "Operación Denegada.",
                             "666" => "Operación Denegada. Problemas de comunicación. Intente más tarde.",
                             "667" => "Operación Denegada. Transacción sin respuesta de Verified by Visa.",
                             "668" => "Operación Denegada. Contactar con el comercio.",
                             "669" => "Operación Denegada. Contactar con el comercio.",
                             "670" => "Operación Denegada. Contactar con el comercio.",
                             "904" => "Operación Denegada.",
                             "909" => "Operación Denegada. Problemas de comunicación. Intente más tarde.",
                             "910" => "Operación Denegada.",
                             "912" => "Operación Denegada. Entidad emisora de la tarjeta no disponible",
                             "913" => "Operación Denegada.",
                             "916" => "Operación Denegada.",
                             "928" => "Operación Denegada.",
                             "940" => "Operación Denegada.",
                             "941" => "Operación Denegada.",
                             "942" => "Operación Denegada.",
                             "943" => "Operación Denegada.",
                             "945" => "Operación Denegada.",
                             "946" => "Operación Denegada. Operación de anulación en proceso.",
                             "947" => "Operación Denegada. Problemas de comunicación. Intente más tarde.",
                             "948" => "Operación Denegada.",
                             "949" => "Operación Denegada.",
                             "965" => "Operación Denegada."      
        );
        
        define("COD_ACCION", serialize($COD_ACCION));
    }    
    
    /**
     * Get singleton with payment model
     *
     * @return Strobe_VisaNet_Model
     */
    public function getPayment()
    {
        return Mage::getSingleton('visanet/eticket');
    }

    /**
     * Get singleton with model checkout session
     *
     * @return Mage_Checkout_Model_Session
     */
    public function _getCheckout()
    {
        return Mage::getSingleton('checkout/session');
    }
    
    
    public function checkEticket($order, $eticket, $shop_code, $debug){
      //Url for Eticket Generation
      if($debug){
        $eticket_url = "http://qas.multimerchantvisanet.com/wsconsulta/wsconsultaeticket.asmx?wsdl";
      }else{
        $eticket_url = "https://www.multimerchantvisanet.com/WSConsulta/WSConsultaEticket.asmx?WSDL";
      }
      //Generating the base document
      $dom = new DOMDocument('1.0', 'utf-8');
      //Parameters
      $codTienda = $dom->createElement('parametro', $shop_code);
      $codTiendaAttribute = $dom->createAttribute('id');
      $codTiendaAttribute->value = 'CODTIENDA';
      $codTienda->appendChild($codTiendaAttribute);

      $eticket = $dom->createElement('parametro',$eticket);
      $eticketAttribute = $dom->createAttribute('id');
      $eticketAttribute->value = 'ETICKET';
      $eticket->appendChild($eticketAttribute);

      //End Parameters
      $parametros = $dom->createElement('parametros');
      //Adding Parameters to the parameters tag
      $parametros->appendChild($codTienda);
      $parametros->appendChild($eticket);
      //End Adding
      $check_eticket = $dom->createElement('consulta_eticket');
      $check_eticket->appendChild($parametros);
      $dom->appendChild($check_eticket);
      $xml = $dom->saveXML();
      //Soap Client
      $client = new SoapClient($eticket_url);
      $result = $client->ConsultaEticket( array("xmlIn"=>$xml) );
      //Parsing Results
      $response = new DOMDocument();
      $isValid=$response->loadXML($result->ConsultaEticketResult);
      if(!$isValid){
        //Exception throw when xml is invalid
        throw new Exception("No se pudo procesar la orden");
      }      
      
      //Checking Response
      $domXPath = new DOMXPath($response);
      $mensajes = $domXPath->query("//respuesta_eticket/mensajes/mensaje");
      //Si la cantidad de mensajes es > 0 tenemos un error :(
      $length = $mensajes->length;
      if($length > 0){
        for($i = 0 ; $i < $length; $i++){
          throw new Exception($mensajes->item($i)->nodeValue);
        }
      }

      $params = array();
      $eticketNode = $domXPath->query('//respuesta_eticket/pedido/operacion/campo[@id="estado"]');
      $params['estado'] = $eticketNode->item(0)->nodeValue;

      $codTienda = $domXPath->query('//respuesta_eticket/pedido/operacion/campo[@id="cod_tienda"]');
      $params['cod_tienda'] = $codTienda->item(0)->nodeValue;

      $norden = $domXPath->query('//respuesta_eticket/pedido/operacion/campo[@id="nordent"]');
      $params['nordent'] = $norden->item(0)->nodeValue;

      $amount = $domXPath->query('//respuesta_eticket/pedido/operacion/campo[@id="imp_autorizado"]');
      $params['importe'] = $amount->item(0)->nodeValue;
      
      $pan = $domXPath->query('//respuesta_eticket/pedido/operacion/campo[@id="pan"]');
      $params['pan'] = $pan->item(0)->nodeValue;
      
      $nombretarjetahabiente = $domXPath->query('//respuesta_eticket/pedido/operacion/campo[@id="nombre_th"]');      
      $params['nombretarjetahabiente'] = $nombretarjetahabiente->item(0)->nodeValue;
      
      $fechahora = $domXPath->query('//respuesta_eticket/pedido/operacion/campo[@id="fechayhora_tx"]');      
      $params['fechayhora'] = $fechahora->item(0)->nodeValue;
      
      $codaccion = $domXPath->query('//respuesta_eticket/pedido/operacion/campo[@id="cod_accion"]');      
      $params['cod_accion'] = $codaccion->item(0)->nodeValue;
      
      $params['moneda'] = "Soles";
            
      
      if($params['estado'] != "AUTORIZADO") {        
          $motivos = unserialize(COD_ACCION);                     
          $msg  = Mage::helper('visanet')->__('<b>Transacción Denegada.</b><br />'.
                                                    'Número de pedido: '.$order->getIncrementId().' <br />'.
                                                    'Nombre del Tarjetahabiente:'.$params["nombretarjetahabiente"].' <br />'.
                                                    'Fecha y Hora del Pedido:'.$params["fechayhora"].' <br />'.
                                                    'Motivo de la Denegación:'.$motivos[$params['cod_accion']].' <br /><br />'.
                                                    '');
          $this->_getCheckout()->addError($msg);
          
      }
      if($params['importe'] != $order->getGrandTotal()){
        throw new Exception('El monto de la transaccion es incorrecta.');
      }
      if($params['nordent'] != $order->getIncrementId()){
        throw new Exception('Tu orden es la incorrecta .');
      }
      return $params;

    }
    /**
     * Notification Endpoint
     *
     * @return Nothing
     */
    public function endpointAction()
    {        
        $shop_code = 0;
        $store_data = "Test Message";
        $debug_mode = false;
        $prefix = Mage::getConfig()->getTablePrefix();
        $table = $prefix."etickets";
        
        if ($this->getRequest()->isPost()) {
            
            if (Mage::getStoreConfig(self::XML_PATH_ENVIRONMENT)=="Sandbox") {
                $shop_code    = Mage::getStoreConfig(self::XML_PATH_CODE_SANDBOX);
                $return_url   = Mage::getStoreConfig(self::XML_PATH_URL_SUCCESS_SANDBOX);
                $debug_mode = true;
            } else {
                $shop_code    = Mage::getStoreConfig(self::XML_PATH_CODE_PROD);
                $return_url   = Mage::getStoreConfig(self::XML_PATH_URL_SUCCESS_PROD);
                $debug_mode = false;
            }


            //Si es nulo, mostramos error y nos vamos
            if(!isset($_POST['eticket'])){
                $this->_getCheckout()->addError("No pudimos procesar tu pedido en estos momentos");
                $this->_redirect('checkout/cart');
                return;
            }
            //Retrieve the eticket
            $eticket = $_POST['eticket'];

            //Obtenemos la orden basandonos en el eticket
            $read = Mage::getSingleton('core/resource')->getConnection('core_read');
            $query =
            $order_id = $read->fetchOne("SELECT order_id FROM $table WHERE eticket='{$eticket}' LIMIT 1");

            if(!$order_id){
                $this->_getCheckout()->addError("Tu eticket es invalido");
                $this->_redirect('checkout/cart');
                return;
            }
            //Load the order
            $order = Mage::getModel('sales/order');
            $order->load($order_id);
            //$customer = Mage::getModel('customer/customer')->load($order->getCustomerId())->getData();
            //$nombretarjetahabiente = $customer->getFirstname()." ".$customer->getLastName();
            //Retrieve params or error

            $orderdetail = "";
            foreach($order->getAllItems() as $item){
                    $orderdetail = $orderdetail.$item->getName().", ";
            }
            
            try{
                $params = $this->checkEticket($order, $eticket, $shop_code, $debug_mode);
                //Order is correct
                $order->setStatus('payment_confirmed_visanet');
                #$order->setData('state','complete');
                $history = $order->addStatusHistoryComment(
                                __('Orden pagada con Visanet')
                    );
                $history->setIsCustomerNotified(true);                
                $order->save();
                                                
                $order->sendNewOrderEmail();
                $order->setEmailSent(true);
                $order->save();
                                
                // Enviar email.
                $order->sendOrderUpdateEmail(true, 'Pago confirmado por Visanet');
                
                
                //$event = Mage::getModel('moneybookers/event')->setEventData($this->getRequest()->getParams());
                $quoteId = "Pago Completo";
                $this->_getCheckout()->setLastSuccessQuoteId($quoteId);

                                
                $msg  = Mage::helper('visanet')->__('Número de pedido: '.$order->getIncrementId().' <br />'.
                                                    'Nombre del Tarjetahabiente:'.$params["nombretarjetahabiente"].' <br />'.
                                                    'Número de Tarjeta enmascarada:'.$params["pan"].' <br />'.
                                                    'Fecha y Hora del Pedido:'.$params["fechayhora"].' <br />'.
                                                    'Importe de la Transacción:'.$params["importe"].' <br />'.
                                                    'Moneda:'.$params["moneda"].' <br />'.
                                                    'Descripción del producto:'.$orderdetail.' <br /><br />'.
                                                    '');
                $this->_getCheckout()->addSuccess($msg);

                
                $this->_redirect('checkout/onepage/success');
                return;
            }catch (Exception $e){
                    /*$order->setState(Mage_Sales_Model_Order::STATE_CANCELED,
                        Mage_Sales_Model_Order::STATE_CANCELED,
                        $e->getMessage(),
                        false
                    );
                    $order->save();*/
                    $order->cancel();
                    $order->addStatusToHistory(Mage_Sales_Model_Order::STATE_CANCELED, $e->getMessage());
                    $order->save();

                    //$order->sendOrderUpdateEmail(true, "<span style='font-weight:bold;color:#E3181F;'>".$e->getMessage()."</span>");

                    $msg  = Mage::helper('visanet')->__('La transacci&oacute;n de tu orden N: '.$order->getIncrementId().' ha sido denegada.');
                    Mage::getSingleton('checkout/session')->addException($e,
                    $msg
                    );
                    //$this->send_mail($e->getMessage());
                    parent::_redirect('checkout/cart');
                    return;
            }
        } else {
            $this->_getCheckout()->addError("No pudimos procesar tu pedido en estos momentos");
            $this->_redirect('checkout/cart');
            return;
        }
    }

    public function generateEticket($shop_code,$order_id,$mount,$message,$debug=False){
      //Url for Eticket Generation
      if($debug){
        $eticket_url = "http://qas.multimerchantvisanet.com/wsgenerareticket/wseticket.asmx?wsdl";
      }else{
        $eticket_url = "https://www.multimerchantvisanet.com/WSGenerarEticket/WSEticket.asmx?WSDL";
      }

      //Generating the base document
      $dom = new DOMDocument('1.0', 'utf-8');
      //Parameters
      $canal = $dom->createElement('parametro', 3);
      $canalAttribute = $dom->createAttribute('id');
      $canalAttribute->value = 'CANAL';
      $canal->appendChild($canalAttribute);

      $producto = $dom->createElement('parametro', 1);
      $productoAttribute = $dom->createAttribute('id');
      $productoAttribute->value = 'PRODUCTO';
      $producto->appendChild($productoAttribute);

      $codTienda = $dom->createElement('parametro', $shop_code);
      $codTiendaAttribute = $dom->createAttribute('id');
      $codTiendaAttribute->value = 'CODTIENDA';
      $codTienda->appendChild($codTiendaAttribute);

      $numOrden = $dom->createElement('parametro', $order_id);
      $numOrdenAttribute = $dom->createAttribute('id');
      $numOrdenAttribute->value = 'NUMORDEN';
      $numOrden->appendChild($numOrdenAttribute);

      $mount = $dom->createElement('parametro', number_format($mount, 2));
      $mountAttribute = $dom->createAttribute('id');
      $mountAttribute->value = 'MOUNT';
      $mount->appendChild($mountAttribute);

      $datoComercio = $dom->createElement('parametro',$message);
      $datoComercioAttribute = $dom->createAttribute('id');
      $datoComercioAttribute->value = 'DATO_COMERCIO';
      $datoComercio->appendChild($datoComercioAttribute);

      //End Parameters
      $parametros = $dom->createElement('parametros');
      //Adding Parameters to the parameters tag
      $parametros->appendChild($canal);
      $parametros->appendChild($producto);
      $parametros->appendChild($codTienda);
      $parametros->appendChild($numOrden);
      $parametros->appendChild($mount);
      $parametros->appendChild($datoComercio);
      //End Adding
      $nuevo_eticket = $dom->createElement('nuevo_eticket');
      $nuevo_eticket->appendChild($parametros);
      $dom->appendChild($nuevo_eticket);
      $xml = $dom->saveXML();
      //Soap Client
      $client = new SoapClient($eticket_url);
      $result = $client->GeneraEticket( array("xmlIn"=>$xml) );
      //Parse Result
      $response = new DOMDocument();
      $isValid=$response->loadXML($result->GeneraEticketResult);
      if(!$isValid){
        //Exception throw when xml is invalid
        throw new Exception('La transacci&oacute;n de tu orden N: '.$order_id.' ha sido denegada');
      }
      //Checking Response
      $domXPath = new DOMXPath($response);
      $mensajes = $domXPath->query("//eticket/mensajes/mensaje");
      //Si la cantidad de mensajes es > 0 tenemos un error :(
      $length = $mensajes->length;
      if($length > 0){
        for($i = 0 ; $i < $length; $i++){
          throw new Exception($mensajes->item($i)->nodeValue);
        }
      }
      //We dont have any error, lets return the eticket
      $eticketNode = $domXPath->query('//eticket/registro/campo[@id="ETICKET"]');
      $eticket = $eticketNode->item(0)->nodeValue;
      return $eticket;
    }
    /**
     * Generates Redirect Url for payment method
     */
    public function getRedirectUrl($shop_code,$order_id,$mount,$message,$eticket,$debug=False){
      //Url for Eticket Generation

      if($debug){
        $form_url = "http://qas.multimerchantvisanet.com/formularioweb/formulariopago.asp";
      }else{
        $form_url = "https://www.multimerchantvisanet.com/formularioweb/formulariopago.asp";
      }

      $params = implode("&",array(
                      'codtienda='.$shop_code,
                      'numorden='.$order_id,
                      'mount='.number_format($mount, 2),
                      'dato_comercio='.urlencode($message),
                      'eticket='.$eticket
                                  ));
      $ch = curl_init($form_url);
      curl_setopt($ch, CURLOPT_HEADER, FALSE);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      curl_setopt($ch, CURLOPT_POST, TRUE);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
      $content = curl_exec($ch);
      curl_close($ch);
      $redirect = str_replace("formulariopago.aspx",$form_url,$content);
      return $redirect;
    }

    /**
     * Order Place and redirect to SafetyPay Express service
     */
    public function paymentAction()
    {

        $shop_code = 0;
        $store_data = "Test Message";
        $debug_mode = False;
        $prefix = Mage::getConfig()->getTablePrefix();
        $table = $prefix."etickets";
        
        if (Mage::getStoreConfig(self::XML_PATH_ENVIRONMENT)=="Sandbox") {
            $shop_code    = Mage::getStoreConfig(self::XML_PATH_CODE_SANDBOX);
            $return_url   = Mage::getStoreConfig(self::XML_PATH_URL_SUCCESS_SANDBOX);
            $debug_mode = true;
        } else {
            $shop_code    = Mage::getStoreConfig(self::XML_PATH_CODE_PROD);
            $return_url   = Mage::getStoreConfig(self::XML_PATH_URL_SUCCESS_PROD);
            $debug_mode = false;
        }
        

        try {
            $session = $this->_getCheckout();
            $order = Mage::getModel('sales/order');
            $order->loadByIncrementId($session->getLastRealOrderId());
            $order->save();
            $eticket = $this->generateEticket($shop_code, $session->getLastRealOrderId(), $order->getGrandTotal(), $store_data,$debug_mode);
            $redirect_content = $this->getRedirectUrl($shop_code, $session->getLastRealOrderId(), $order->getGrandTotal(), $store_data,$eticket,$debug_mode);
            $session->getQuote()->setIsActive(false)->save();
            /*$session->setSafetypayQuoteId($session->getQuoteId());
            $session->setSafetypayRealOrderId($session->getLastRealOrderId());
            */
            $session->clear();
            $this->loadLayout();
            $this->renderLayout();

            if (!$order->getId()) {
                Mage::throwException('No order for processing found');
            }
            $order->setState(Mage_Sales_Model_Order::STATE_PENDING_PAYMENT,
                Mage_Sales_Model_Order::STATE_PENDING_PAYMENT,
                Mage::helper('visanet')->__('The shopper has been redirected to VisaNet service using the Token URL.'),
                true
            );
            
            //$order->sendNewOrderEmail();
            //$order->setEmailSent(true);
            $order->save();
            //Write Model - Hacky
            $write = Mage::getSingleton('core/resource')->getConnection('core_write');
            $write->query("INSERT INTO $table values(?,?,?)",array($eticket,$order->getId(),$session->getLastRealOrderId()));
            $write->commit();
            echo $redirect_content;
        } catch (Exception $e){
            $order->setState(Mage_Sales_Model_Order::STATE_CANCELED,
                Mage_Sales_Model_Order::STATE_CANCELED,
                $e->getMessage(),
                false
            );
            $order->save();
             $msg  = Mage::helper('visanet')->__('La transacci&oacute;n de tu orden N: '.$session->getLastRealOrderId().' ha sido denegada.');
            Mage::getSingleton('checkout/session')->addException($e,
               $msg
            );
            $order->sendOrderUpdateEmail(true, "<span style='font-weight:bold;color:#E3181F;'>".$e->getMessage()."</span>");
            
            parent::_redirect('checkout/cart');
        }
    }
    function send_mail($error){
      //Mage::helper('checkout')->sendPaymentFailedEmail($this->getOnepage()->getQuote(), $error);
    }
    
    
    /**
     * Action to which the customer will be returned when the payment is made.
     *
     * @return Nothing
     */
    public function successAction()
    {

      /*$event = Mage::getModel('safetypay/event')
                 ->setEventData($this->getRequest()->getParams());
        try {
            $quoteId = $event->successEvent();

            $message = $event->confirmationEvent();
            $this->getResponse()->setBody($message);

            $this->_getCheckout()->setLastSuccessQuoteId($quoteId);
            $this->_redirect('checkout/onepage/success');
            return;
        } catch (Mage_Core_Exception $e) {
            $this->_getCheckout()->addError($e->getMessage());
        } catch(Exception $e) {
            Mage::logException($e);
        }
        $this->_redirect('checkout/cart');*/
    }

    /**
     * Action to which the customer will be returned if the payment process is
     * cancelled.
     * Cancel order and redirect user to the shopping cart.
     */
    /*public function cancelAction()
    {
        $event = Mage::getModel('safetypay/event')
                 ->setEventData($this->getRequest()->getParams());
        $message = $event->cancelEvent();
        $this->_getCheckout()->setQuoteId($this->_getCheckout()->getSafetypayQuoteId());
        $this->_getCheckout()->addError($message);
        $this->_redirect('checkout/cart');
    }*/
}
