<?php
/**
 * VisaNet Payment E-Ticket Model
 */
class Strobe_VisaNet_Model_Eticket extends Mage_Payment_Model_Method_Cc
{
    protected $_formBlockType = 'visanet/form';
    protected $_infoBlockType = 'visanet/info';
    protected $_code = 'visanet_eticket';
    
    public function validate()
    {
        $this->allowCurr = explode(',', $this->getConfigData('abankcurrency'));
        $paymentInfo = $this->getInfoInstance();
        if ($paymentInfo instanceof Mage_Sales_Model_Order_Payment)
            $this->currentCurr = $paymentInfo->getOrder()
                                            ->getOrderCurrencyCode();
        else
            $this->currentCurr = $paymentInfo->getQuote()
                                            ->getQuoteCurrencyCode();

        if (!in_array($this->currentCurr, $this->allowCurr))
            Mage::throwException(
            	  Mage::helper('visanet')
            	  	->__('Select another currency type for your order with VisaNet.') . "\n\n"
	            . Mage::helper('visanet')
	            	->__("Currencies available: %s", $this->getConfigData('abankcurrency')) . "\n\n"
	            . Mage::helper('visanet')
	            	->__("Don't worry, using VisaNet can pay with many other currencies to complete your payment.")
			);

        return $this;
    }
    
    /**
     * Return url for redirection after order placed
     *
     * @return string
     */
    public function getOrderPlaceRedirectUrl()
    {
        return Mage::getUrl('visanet/eticket/payment');
    }

    /**
     * Per client requirement we need to set MoneyAmount to null if this payment method is selected
     *
     * @return Mage_Payment_Model_Method_Abstract
     */
    public function assignData($data)
    {
        parent::assignData($data);

        if (!($data instanceof Varien_Object)) {
            $data = new Varien_Object($data);
        }
        $info = $this->getInfoInstance();
        $info->setMoneyAmount(null);
        $info->save();
        return $this;
    }
}
