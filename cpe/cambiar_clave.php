<?php
session_start();
if(isset($_SESSION["ruc_empresa"]) && trim($_SESSION["ruc_empresa"])!=""){
include("cn/config".$_SESSION["ruc_empresa"].".php");
}else{
include("cn/config.php");
}
if(!isset($_SESSION["usuariofe"]) || trim($_SESSION["usuariofe"])==""){
	echo "<script>location.href ='index.php';</script>";
}
if(isset($_SESSION["cambiar_clave"]) && trim($_SESSION["cambiar_clave"])!=1){
	echo "<script>location.href ='inicio.php';</script>";
}
$_SESSION['avizofinsesion']="";
if(isset($_SESSION['tiempo']) ) { 
	$vida_session = time() - $_SESSION['tiempo']; 
	if($vida_session > $tiempo_bloqueo) 
	{ 
		session_unset();
		session_destroy(); 
		session_start();
		$_SESSION['avizofinsesion']=1;
		echo "<script>location.href='index.php';</script>";
	} 
} 

$_SESSION['tiempo'] = time() ; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <link href="img/icons/favicon.ico" type="image/x-icon" rel="shortcut icon">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Facturaci&oacute;n electr&oacute;nica</title>
<link href="css/estilos.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
<link href="css/kendo.common.min.css" rel="stylesheet">
<!--<link href="css/kendo.flat.min.css" rel="stylesheet">-->
<link href="css/kendo.bootstrap.min.css" rel="stylesheet"> 
<link href="css/kendo.dataviz.bootstrap.min.css" rel="stylesheet" />
 <script src="js/kendo.web.min.js"></script>
  <script src="js/scripts.js"></script>
  <link href="css/jquery.alerts.css" rel="stylesheet"> 
 <script src="js/jquery.alerts.js"></script>
</head>
<body>
<div id="bgtransparent" style="display:none"></div>
<div id="cargador" style="display:none"><div id="text"><img src="css/Bootstrap/loading-image.gif" /></div><div id="text">Procesando</div></div>
<div id="contenedor_admin">
<?php include("view/cabecera.php"); ?>
<form method="post" name="formusuario" id="formusuario">
<div id="conteformclave">
<div id="titulo" class="titulo1">
Cambiar contrase&ntilde;a
</div>
<div id="textoform">
Usuario :
</div>
<div id="textboxform">
<input type="text" class="k-textbox" name="user" id="user"  readonly value="<?php echo $_SESSION["usuariofe"]; ?>" />
</div>
<div id="textoform">
Nueva contrase&ntilde;a :
</div>
<div id="textboxform">
<input type="password" name="pass" id="pass" maxlength="20" class="k-textbox" />
</div>
<div id="textoform">
Repetir contrase&ntilde;a :
</div>
<div id="textboxform">
<input type="password" name="repass" id="repass" maxlength="20" class="k-textbox" />
</div>
<div id="buttomsave">
<input type="hidden" name="tipo" id="tipo" value="cambiarclave" />
<input type="button" name="save" id="save" value="Guardar" onclick="cambiar_clave();" class="k-button" />
</div>
</div>
</form>
</div>
</body>
</html>