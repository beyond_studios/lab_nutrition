<?php
session_start();
include("cn/config.php");
$correo=trim($_SESSION["correo"]);
if($correo!=""){
	$acceso=1;
}else{
	$acceso=0;
}
unset($_SESSION["ruc_empresa"]);
if(trim($_SESSION["cambiar_clave"])==1){
	echo "<script>location.href ='cambiar_clave.php';</script>";
}
if(trim($_SESSION["usuariofe"])!=""){
	echo "<script>location.href ='inicio.php';</script>";
}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <link href="img/icons/favicon.ico" type="image/x-icon" rel="shortcut icon">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Facturaci&oacute;n electr&oacute;nica</title>
<link href="css/estilos.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
<link href="css/kendo.common.min.css" rel="stylesheet">
<link href="css/kendo.bootstrap.min.css" rel="stylesheet"> 
<link href="css/kendo.dataviz.bootstrap.min.css" rel="stylesheet" />
 <script src="js/kendo.all.min.js"></script>
  <script src="js/scripts.js"></script>
<link href="css/jquery.alerts.css" rel="stylesheet"> 
 <script src="js/jquery.alerts.js"></script>
 <script>
$(document).ready(function() {
	$( "body" ).css( "min-height","+="+window.innerHeight);
	$("#btn_pdf").kendoButton({
                       imageUrl: "img/file_extension_pdf.png"
                    });
	$("#btn_xml").kendoButton({
                       imageUrl: "img/file_extension_xml.png"
                    });
<?php
		if($_SESSION['avizofinsesion']==1){
		?>
			jAlert("Lo sentimos, su tiempo de inactividad cerr\u00f3 la sesi\u00f3n, vuelva a conectarse ", "Aviso");
		<?php
		unset($_SESSION['avizofinsesion']);
		}
		?>
		<?php if(trim($_SESSION["archivo_vacio"])==1){ ?>
jAlert('El documento no existe.','Aviso');
<?php 
unset($_SESSION["archivo_vacio"]);
} ?>
		document.getElementById("serie_fac").value="F";
		$("#serie_fac").focus();
		$('#user').attr('disabled', true);
		$('#pass').attr('disabled', true);
		$('#captcha').attr('disabled', true);
		$('#buttomsaveini').hide();
		$('#btn_pdf').hide();
        $('#btn_xml').hide();
		$("#fecha_fac").kendoDatePicker({
                    format: "dd/MM/yyyy"
                });
				$("#cdocu_fac").change(function(e) {
					if($("#cdocu_fac").val()=="01"){
						document.getElementById("serie_fac").value="F";
						}
					else if($("#cdocu_fac").val()=="03"){
						document.getElementById("serie_fac").value="B";
						}
					else if($("#cdocu_fac").val()=="20"){
						document.getElementById("serie_fac").value="R";
						}
					else if($("#cdocu_fac").val()=="67"){
						document.getElementById("serie_fac").value="P";
						}
					else{
						document.getElementById("serie_fac").value="";
					}
					$("#serie_fac").focus();
				});	
				
				$('input[name="opini"]').change(function(e) {
					if($("#opini:checked").val()=="login"){
						$('#buttomsaveini').show();
						$('#buttomsave').hide();
						$('#cdocu_fac').attr('disabled', true);
	                    $('#ndocu_fac').attr('disabled', true);
						$('#serie_fac').attr('disabled', true);
	                    $('#monto_fac').attr('disabled', true);
	                    var datepicker = $("#fecha_fac").data("kendoDatePicker");
                        datepicker.enable(false);
						$('#user').attr('disabled', false);
						$('#pass').attr('disabled', false);
						$('#captcha').attr('disabled', false);
						document.getElementById("tipo").value="login";
						$('#btn_pdf').hide();
                        $('#btn_xml').hide();
						document.getElementById("ndocu_fac").value="";
						document.getElementById("serie_fac").value="";
	                    document.getElementById("monto_fac").value="";
	                    document.getElementById("docgen_fac").value="";
						$("#user").focus();
						}else{
						$('#buttomsaveini').hide();
						$('#buttomsave').show();
						$('#cdocu_fac').attr('disabled', false);
	                    $('#ndocu_fac').attr('disabled', false);
						$('#serie_fac').attr('disabled', false);
	                    $('#monto_fac').attr('disabled', false);
	                    var datepicker = $("#fecha_fac").data("kendoDatePicker");
                        datepicker.enable(true);
						$('#user').attr('disabled', true);
						$('#pass').attr('disabled', true);
						$('#captcha').attr('disabled', true);
						document.getElementById("tipo").value="buscaanonima";
						document.getElementById("user").value="";
						document.getElementById("pass").value="";
						document.getElementById("captcha").value="";
						document.getElementById("cdocu_fac").value="01";
						document.getElementById("serie_fac").value="F";
						$("#serie_fac").focus();
						
							}
							 
							document.getElementById("docgen_fac").value="";
});

})
</script>
</head>

<body>
<div id="bgtransparent" style="display:none"></div>
<div id="cargador" style="display:none"><div id="text"><img src="css/Bootstrap/loading-image.gif" /></div><div id="text">Procesando</div></div>
<div id="iniciar_sesion"></div>
<div id="registrarse"></div>
<div id="olvidar_contrasena"></div>
<div id="consulta_anonima"></div>
<div id="contenedor">
<?php include("view/cabecera.php"); ?>
<?php
if($multiempresa==1){
$buscar_factura_anonima="buscar_factura_anonima_multiempresas()";
$validar_usuario="validar_usuario_multiempresas()";
$altura_registro=300;
}else{
$buscar_factura_anonima="buscar_factura_anonima()";
$validar_usuario="validar_usuario()";
$altura_registro=270;
}
?>
<div id="contenido2inicio">
<img src="img/fe1.png" />
</div>
<div id="contenidolinicio">
<div id="conteform">
<form name="forminicio" id="forminicio" method="post">
<input type="hidden" name="tipo" id="tipo" value="buscaanonima" />
<div id="titulo" class="titulo1">
Iniciar sesi&oacute;n
</div>
<?php
if($multiempresa==1){
?>
<div id="texto">
RUC del emisor
</div>
<div id="textbox">
<input type="text" style="width:100%;" name="ruc_empresa" id="ruc_empresa" maxlength="11" />
</div>
<?php
}
?>
<div id="buttomoption">
<input type="radio" name="opini" id="opini" value="login" /> Usuario Registrado
</div>
<div id="texto">
Usuario
</div>
<div id="textbox">
<input type="text" style="width:100%;" name="user" id="user" maxlength="100" onKeyUp="if(event.keyCode == 13){<?php echo $validar_usuario;?> }" />
</div>
<div id="texto">
Contrase&ntilde;a
</div>
<div id="textbox">
<input type="password" name="pass" id="pass" style="width:100%;" maxlength="11"  onKeyUp="if(event.keyCode == 13){<?php echo $validar_usuario;?> }"  />
</div>
<div id="texto">

</div>
<div id="textbox">
<img src="captcha.php" name="imgcap" id="imgcap"  /> <img src="img/refresh.png" onClick="recargar_captcha()" title="Recargar" style="max-width:24px; margin-bottom:13px;" />
</div>
<div id="texto">
Ingrese el c&oacute;digo
</div>
<div id="textbox">
<input type="text" name="captcha" id="captcha" maxlength="6" style="width:100%;"  />
</div>
<div id="buttomoption">
<input type="radio" name="opini" id="opini" value="buscaanonima" checked="checked" /> Usuario Libre
</div>
<div id="texto">
<span class="oblig">*</span> Documento
</div>
<div id="textbox">
<select name="cdocu_fac" id="cdocu_fac" style="width:100%;">
<option value="01">Factura</option>
<option value="03">Boleta Vta.</option>
<option value="07">Nota: Cr&eacute;dito</option>
<option value="08">Nota: D&eacute;bito</option>
<option value="20">Retenci&oacute;n Electr&oacute;nico</option>
<option value="67">Percepci&oacute;n Electr&oacute;nico</option>
</select>
</div>
<div id="texto">
<span class="oblig">*</span> Serie
</div>
<div id="textbox">
<input type="text" name="serie_fac" id="serie_fac" style="width:100%;text-transform:uppercase" maxlength="4"  />
</div>
<div id="texto">
<span class="oblig">*</span> N&uacute;mero
</div>
<div id="textbox">
<input type="text" name="ndocu_fac" id="ndocu_fac" style="width:100%;" maxlength="7" onkeypress="return validar_solonumeros(event, this, true);" />
</div>
<div id="texto">
<span class="oblig">*</span> Fecha
</div>
<div id="textbox" style="text-align:left">
<input type="text" name="fecha_fac" id="fecha_fac" value="<?php echo date("d/m/Y"); ?>" maxlength="10" />
</div>
<div id="texto">
<span class="oblig">*</span> Monto
</div>
<div id="textbox">
<input type="text" name="monto_fac" id="monto_fac" maxlength="60" style="width:100%;" onkeypress="return validar_solonumeros_dec(event, this, true);" />
</div>
<div id="buttomsave">
<!--<input type="button" name="btn_pdf2" id="btn_pdf2" style="width:120px;" value="PDF" onclick="generar_factura_anonima('.pdf');" class="k-button" />-->
<input type="hidden" name="docgen_fac" id="docgen_fac" value="" />
<input type="button" name="btn_buscar" id="btn_buscar" value="Buscar" onclick="<?php echo $buscar_factura_anonima;?>" style="width:120px;" class="k-button" /> <input type="button" name="btn_nuevo" id="btn_nuevo" value="Nueva consulta" onclick="buscar_nuevo_factura_anonima()" style="width:120px;" class="k-button" /><br /><br /> <button type="button" id="btn_pdf" name="btn_pdf" style="width:120px;"  onclick="generar_factura_anonima('.pdf');"> PDF</button> 
<button type="button" id="btn_xml" name="btn_xml" style="width:120px;"  onclick="generar_factura_anonima('.xml');"> XML</button>
</div>
<div id="buttomsaveini">
<input class="k-button" type="button" name="save" id="save" value="Ingresar" onclick="<?php echo $validar_usuario;?>" style="width:120px;" /> <input type="button" name="back" id="back" value="Nueva consulta" style="width:120px;" class="k-button" onclick="buscar_nuevo_login();" /> 
</div>
<div id="lado2_reg">
No est&aacute; registrado <a onclick="generar_popup('registrarse',450,<?php echo $altura_registro; ?>,'view/registrarse.php')"><img src="img/icon_new.png" title="Buscar tickets" onclick="seleccionar_tickets()" style="margin:0; padding:0;max-height:20px;cursor:pointer; vertical-align:middle" /> Registrarse</a>
</div>
</form>
</div>
</div>

<?php include("view/pie.php"); ?>
</div>
</body>
</html>