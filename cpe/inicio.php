<?php
session_start();
if(isset($_SESSION["ruc_empresa"]) && trim($_SESSION["ruc_empresa"])!=""){
include("cn/config".$_SESSION["ruc_empresa"].".php");
}else{
include("cn/config.php");
}
$_SESSION["buscarfactura"]["p3"]=date('d/m/Y');
$fecha=date('Y-m-d');
$nuevafecha = strtotime ( '-1 month' , strtotime ( $fecha ) ) ;
$nuevafecha = date ( 'd/m/Y' , $nuevafecha );
$_SESSION["buscarfactura"]["p2"]=$nuevafecha;
if(trim($_SESSION["usuariofe"])==trim($correo_admin)){
$_SESSION["buscarfactura"]["nomcli"]="";
$_SESSION["buscarfactura"]["ruccli"]="";
}else{
$_SESSION["buscarfactura"]["nomcli"]=$_SESSION["nomcli"];
$_SESSION["buscarfactura"]["ruccli"]=$_SESSION["ruccli"];
}
if(!isset($_SESSION["usuariofe"]) || trim($_SESSION["usuariofe"])==""){
	echo "<script>location.href ='index.php';</script>";
}
if(isset($_SESSION["cambiar_clave"]) && trim($_SESSION["cambiar_clave"])==1){
	echo "<script>location.href ='cambiar_clave.php';</script>";
}
$_SESSION['avizofinsesion']="";
if(isset($_SESSION['tiempo']) ) { 
	$vida_session = time() - $_SESSION['tiempo']; 
	if($vida_session > $tiempo_bloqueo) 
	{ 
		session_unset();
		session_destroy(); 
		session_start();
		$_SESSION['avizofinsesion']=1;
		echo "<script>location.href='index.php';</script>";
	} 
} 

$_SESSION['tiempo'] = time() ; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <link href="img/icons/favicon.ico" type="image/x-icon" rel="shortcut icon">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Facturaci&oacute;n electr&oacute;nica</title>
<link href="css/estilos.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
<link href="css/kendo.common.min.css" rel="stylesheet">
<!--<link href="css/kendo.flat.min.css" rel="stylesheet">-->
<link href="css/kendo.bootstrap.min.css" rel="stylesheet"> 
<link href="css/kendo.dataviz.bootstrap.min.css" rel="stylesheet" />
 <script src="js/kendo.all.min.js"></script>
  <script src="js/scripts.js"></script>
  <link href="css/jquery.alerts.css" rel="stylesheet"> 
 <script src="js/jquery.alerts.js"></script>
  <script>
$(document).ready(function() {
	$( "body" ).css( "min-height","+="+window.innerHeight);
<?php if(isset($_SESSION["archivo_vacio"]) && trim($_SESSION["archivo_vacio"])==1){ ?>
jAlert('El documento no existe.','Aviso');
<?php 
unset($_SESSION["archivo_vacio"]);
} ?>
function startChange() {
	var startDate = start.value(),
	endDate = end.value();
	
	if (startDate) {
		startDate = new Date(startDate);
		startDate.setDate(startDate.getDate());
		end.min(startDate);
	} else if (endDate) {
		start.max(new Date(endDate));
	} else {
		endDate = new Date();
		start.max(endDate);
		end.min(endDate);
	}
	}
	
	function endChange() {
	var endDate = end.value(),
	startDate = start.value();
	
	if (endDate) {
		endDate = new Date(endDate);
		endDate.setDate(endDate.getDate());
		start.max(endDate);
	} else if (startDate) {
		end.min(new Date(startDate));
	} else {
		endDate = new Date();
		start.max(endDate);
		end.min(endDate);
	}
	}
	
	var start = $("#p2").kendoDatePicker({
		format: "dd/MM/yyyy",
		change: startChange
	}).data("kendoDatePicker");
	
	var end = $("#p3").kendoDatePicker({
		format: "dd/MM/yyyy",
		change: endChange
	}).data("kendoDatePicker");
	
	start.max(end.value());
	end.min(start.value());
	$("#contenedorgrid").kendoGrid({
						  dataSource: {
						       transport: {
							    read:  {
                                    url: "controller/llenar_facturas_grilla.php",
                                    }
			            
	                    },
							error: function(e) {
								jAlert('Error de conexi\u00f3n.', "Aviso");
			
		}	,
			pageSize: 10,			
	                },
				
									
					filterable: true,
                    
                    sortable: true,
                    columnMenu: true,
                    pageable: {
                        refresh: true,
                        pageSizes: true
                    },
                  change: function() {
              var text = "";
              var grid = this;
 
              grid.select().each(function() {
                  var dataItem = grid.dataItem($(this));
                  idgrupo = dataItem.idgrupo;
				  nommar = dataItem.nommar;
				  fecha_fin = dataItem.fecha_fin;
				    document.getElementById("idgrupo").value=idgrupo;
					 document.getElementById("nommar").value=nommar;
					 document.getElementById("fecha_fin").value=fecha_fin;
				 		 				 
              });
          },
                       
                       
                       columns: [{
					            attributes: {
								style: "width: 10%"
                                },
								headerAttributes: {
                                style: "width: 10%"
                                },
                                field: "fecha",
								title: "Fecha"
								
								},{
					            attributes: {
								style: "white-space: nowrap;width: 10%"
                                },
								headerAttributes: {
                                style: "width: 10%"
                                },
                                field: "nomdoc",
								title: "Doc."
								
								},{
					            attributes: {
								style: "width: 10%"
                                },
								headerAttributes: {
                                style: "width: 10%"
                                },
                                field: "ndocu",
								title: "Numero"
								
								},{
					            attributes: {
								style: "width: 30%"
                                },
								headerAttributes: {
                                style: "width: 30%"
                                },
                                field: "nomcli",
								title: "Cliente"
								
								},{
					            attributes: {
								style: "text-align:right;width: 14%"
                                },
								headerAttributes: {
                                style: "width: 14%"
                                },
                                field: "monto",
								title: "Monto"
								
								},{
					            attributes: {
								style: "width: 10%"
                                },
								headerAttributes: {
                                style: "width: 10%"
                                },
                                field: "flag",
								title: "Status"
								
								},{
					            attributes: {
								style: "width: 8%"
                                },
								headerAttributes: {
                                style: "width: 8%"
                                },
                                template: "<a #: ocultar # onclick='generar_documento(\"#: documento #.pdf\",#: isdocu #)'><img src='img/file_extension_pdf.png' /> <span class='linkdoc'>PDF</span></a>",
								title: " "
								
								},{
					            attributes: {
								style: "width: 8%"
                                },
								headerAttributes: {
                                style: "width: 8%"
                                },
                                template: "<a #: ocultar # onclick='generar_documento(\"#: documento #.xml\",#: isdocu #)'><img src='img/file_extension_xml.png' /> <span class='linkdoc'>XML</span></a>",
								title: " "
								
								}
                            
                        ]

						
                    });
})
</script>
</head>
<body>
<div id="contenedor_admin">
<?php include("view/cabecera.php"); ?>
<div id="titulo">
Facturas
</div>
<div id="opciones">
<ul>
<?php if(basename($_SERVER['PHP_SELF'])=="inicio.php" && $_SESSION["usuariofe"]==$correo_admin){ ?>
<li><a href="usuarios.php" style="border:1px solid #FFF">Admin. Usuarios</a></li>
<?php } ?>
<?php if(basename($_SERVER['PHP_SELF'])=="usuarios.php"){ ?>
<li><a href="inicio.php" style="border:1px solid #FFF">Inicio</a></li>
<?php } ?>
<li><?php if($_SESSION["usuariofe"]!=""){ ?><a style="border:1px solid #FFF" onclick="cerrar_sesion_sistema();">Cerrar sesi&oacute;n</a><?php } ?></li>
</ul>
</div>
<div id="barraopciones">
<form name="frmfacturas" id="frmfacturas" method="post">
   <input type="hidden" name="type" id="type" value="1" />
 <?php  if($_SESSION["usuariofe"]==$correo_admin){ ?>
   Cliente <input type="text" name="nomcli_fac" id="nomcli_fac" onkeyup="if(event.keyCode == 13){buscar_facturas()}" value="" /> 
 <?php }else{ ?>
	 RUC <input type="text" name="ruccli_fac" id="ruccli_fac" readonly="readonly" onkeyup="if(event.keyCode == 13){buscar_facturas()}" value="<?php
echo $_SESSION["ruccli"];
?>" /> 
	 Cliente <input type="text" name="nomcli_fac" id="nomcli_fac" readonly="readonly" onkeyup="if(event.keyCode == 13){buscar_facturas()}" value="<?php
echo $_SESSION["nomcli"];
?>" />  
	<?php } ?>
  

  Desde <input type="text" id="p2" name="p2" onkeyup="if(event.keyCode == 13){buscar_facturas()}" maxlength="10" style="width:100px;" value="<?php
echo $_SESSION["buscarfactura"]["p2"];
?>"  />
  Hasta <input type="text" id="p3" name="p3" onkeyup="if(event.keyCode == 13){buscar_facturas()}" maxlength="10" style="width:100px;" value="<?php
echo $_SESSION["buscarfactura"]["p3"];
?>"  />&nbsp;&nbsp;
<img src="img/icon_search.png" title="Buscar facturas" onclick="buscar_facturas()" style="margin:0; padding:0;max-height:20px;cursor:pointer; vertical-align:middle" />&nbsp;&nbsp; 
<img src="img/excel.png" title="Excel" onclick="location.href='controller/generar_excel.php?op=fac'" style="margin:0; padding:0;max-height:20px;cursor:pointer; vertical-align:middle" /> 
</form>
</div>
<div id="contenedorgrid" class="contenedorgrid">
</div>
<input type="hidden" name="idgrupo" id="idgrupo" />
<input type="hidden" name="nommar" id="nommar" />
<input type="hidden" name="fecha_fin" id="fecha_fin" />
</div>
</body>
</html>