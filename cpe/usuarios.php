<?php
session_start();
$_SESSION["busqueda_estado"]=1;
if(isset($_SESSION["ruc_empresa"]) && trim($_SESSION["ruc_empresa"])!=""){
include("cn/config".$_SESSION["ruc_empresa"].".php");
}else{
include("cn/config.php");
}
$_SESSION["buscarusuario"]["ruccli"]="";
$_SESSION["buscarusuario"]["nomcli"]="";
if(trim($_SESSION["usuariofe"])==""){
	echo "<script>location.href ='index.php';</script>";
}
if(trim($_SESSION["usuariofe"])!=trim($correo_admin)){
	echo "<script>location.href ='index.php';</script>";
}
if(isset($_SESSION["cambiar_clave"]) && trim($_SESSION["cambiar_clave"])==1){
	echo "<script>location.href ='cambiar_clave.php';</script>";
}
$_SESSION['avizofinsesion']="";
if(isset($_SESSION['tiempo']) ) { 
	$vida_session = time() - $_SESSION['tiempo']; 
	if($vida_session > $tiempo_bloqueo) 
	{ 
		session_unset();
		session_destroy(); 
		session_start();
		$_SESSION['avizofinsesion']=1;
		echo "<script>location.href='index.php';</script>";
	} 
} 

$_SESSION['tiempo'] = time() ; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <link href="img/icons/favicon.ico" type="image/x-icon" rel="shortcut icon">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>NavaSoft</title>
<link href="css/estilos.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
<link href="css/kendo.common.min.css" rel="stylesheet">
<!--<link href="css/kendo.flat.min.css" rel="stylesheet">-->
<link href="css/kendo.bootstrap.min.css" rel="stylesheet"> 
<link href="css/kendo.dataviz.bootstrap.min.css" rel="stylesheet" />
 <script src="js/kendo.web.min.js"></script>
  <script src="js/scripts.js"></script>
  <link href="css/jquery.alerts.css" rel="stylesheet"> 
 <script src="js/jquery.alerts.js"></script>

  <script>
$(document).ready(function() {
	$( "body" ).css( "min-height","+="+window.innerHeight);
	$("#contenedorgrid").kendoGrid({
						  dataSource: {
						       transport: {
							    read:  {
                                    url: "controller/llenar_usuarios_grilla.php",
                                    }

	              
									            
	                    },
					  error: function(e) {
								jAlert('Error de conexi\u00f3n.', "Aviso");
			
		}	,
			pageSize: 10,			
	                },
					
					 selectable: "multiple",				
					filterable: true,
                    
                    sortable: true,
                    columnMenu: true,
                    pageable: {
                        refresh: true,
                        pageSizes: true
                    },
                  change: function() {
              var text = "";
              var grid = this;
 
              grid.select().each(function() {
                  var dataItem = grid.dataItem($(this));
                  idusuario = dataItem.idusuario;
				  usuario = dataItem.usuario;
				  document.getElementById("idusuario").value=idusuario;
				  document.getElementById("usuariomail").value=usuario;
				 		 				 
              });
          },
                       
                       
                      columns: [{
			attributes: {
								style: "width: 25%"
                                },
								headerAttributes: {
                                style: "width: 25%"
                                },
			field: "usuario",
			title: "Usuario",
			
		} , {
			attributes: {
								style: "width: 25%"
                                },
								headerAttributes: {
                                style: "width: 25%"
                                },
			field: "ruccli",
			title: "RUC",
			
		} , {
		attributes: {
								style: "width: 50%"
                                },
								headerAttributes: {
                                style: "width: 50%"
                                },
			field: "nomcli",
			title: "Cliente",
			
		} /*, {
			width: 40,
			field: "seguridad",
			title: "Bloq.",
			attributes: {
				"class": "#= userclass #",
                },
		} */
		]

						
                    });
})
</script>
</head>
<body>
<div id="bgtransparent" style="display:none"></div>
<div id="cargador" style="display:none"><div id="text"><img src="css/Bootstrap/loading-image.gif" /></div><div id="text">Procesando</div></div>
<div id="agregar_usuario_admin"></div>
<div id="registrarse"></div>
<div id="ver_detalle"></div>
<div id="contenedor_admin">
<?php include("view/cabecera.php"); ?>
<div id="titulo">
Usuarios
</div>
<div id="opciones">
<ul>
<?php if(basename($_SERVER['PHP_SELF'])=="inicio.php" && $_SESSION["usuariofe"]==$correo_admin){ ?>
<li><a href="usuarios.php" style="border:1px solid #FFF">Admin. Usuarios</a></li>
<?php } ?>
<?php if(basename($_SERVER['PHP_SELF'])=="usuarios.php"){ ?>
<li><a href="inicio.php" style="border:1px solid #FFF">Inicio</a></li>
<?php } ?>
<li><?php if($_SESSION["usuariofe"]!=""){ ?><a style="border:1px solid #FFF" onclick="cerrar_sesion_sistema();">Cerrar sesi&oacute;n</a><?php } ?></li>
</ul>
</div>
<div id="barraopciones">
<form name="frmusuarios" id="frmusuarios" method="post">
<input type="hidden" name="type" id="type" value="2" />
RUC <input type="text" name="ruccli_usu" id="ruccli_usu" onkeyup="if(event.keyCode == 13){buscar_usuarios()}" value="" />
Cliente <input type="text" name="nomcli_usu" id="nomcli_usu" onkeyup="if(event.keyCode == 13){buscar_usuarios()}" value="" />
 <img src="img/icon_search.png" title="Buscar tickets" onclick="buscar_usuarios()" style="margin:0; padding:0;max-height:20px;cursor:pointer; vertical-align:middle" /> 
<img src="img/icon_new.png" onclick="generar_popup('registrarse',450,270,'view/registrarse_admin.php')" title="Agregar" style="margin:0; padding:0;max-height:20px;cursor:pointer; vertical-align:middle" />
<img src="img/icon_delete.png" onclick="eliminar_grupo()" title="Dar de baja" style="margin:0; padding:0;max-height:20px;cursor:pointer; vertical-align:middle" />
<img src="img/ver_mas.png" onclick="ver_detalle_acceso()" title="Ver detalle" style="margin:0; padding:0;max-height:20px;cursor:pointer; vertical-align:middle" />
</form>
</div>
<div id="contenedorgrid" class="contenedorgrid">
</div>
<input type="hidden" name="idusuario" id="idusuario" />
<input type="hidden" name="usuariomail" id="usuariomail" />
</div>
</body>
</html>