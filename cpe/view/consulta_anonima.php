<?php
session_start();
include("../cn/config.php");
?>
 <script>
            $(document).ready(function() {
				$('#btn_pdf').hide();
                $('#btn_xml').hide();
                $("#fecha_fac").kendoDatePicker({
                    format: "dd/MM/yyyy"
                });
            });
        </script>
<form name="formfacanonima" id="formfacanonima" method="post">
<div id="onclose"><img src="img/close.png" onclick="cerrar_ventana('consulta_anonima')" /></div>
<div id="titulo">
Consulta de facturas:
</div>
<div id="divfila">
<div id="lado1">
<span class="oblig">*</span> Documento
</div>
<div id="lado2">
: <select name="cdocu_fac" id="cdocu_fac">
<?php
include('../nusoap/nusoap.php');
$client = new nusoap_client($ruta_ws,true);
$err  = $client->getError();
if ($err) {
 echo '<h2>Constructor error</h2><pre>' . $err . '</pre>';
}
$param = array();
$result = $client->call('sp_webfe_tipodocumento',$param);
 $i=0;
if ($client->fault) {
echo 'No se pudo completar la operación'; 
die(); 
} else { // No 
$err = $client->getError();
// Hay algun error ? 
if ($err) { // Si 
echo 'Error:' . $err; 
die(); 
} 
} 
if(trim($result["sp_webfe_tipodocumentoResult"]["TipoDocumento"][0]["cdocu"])!=""){
$contadorxml=$result["sp_webfe_tipodocumentoResult"]["TipoDocumento"];
}else{
$contadorxml=$result["sp_webfe_tipodocumentoResult"];
}
for($j=0;$j<count($contadorxml);$j++){
if($contadorxml==0){
$fila2=$result["sp_webfe_tipodocumentoResult"]["TipoDocumento"];
}else{
$fila2=$result["sp_webfe_tipodocumentoResult"]["TipoDocumento"][$j];
}
echo "<option value='".$fila2["cdocu"]."'>".$fila2["nomdoc"]."</option>";
}
?>
</select>
</div>
</div>
<div id="divfila">
<div id="lado1">
<span class="oblig">*</span> N&uacute;mero
</div>
<div id="lado2">
: <input type="text" name="ndocu_fac" id="ndocu_fac" maxlength="80" />
</div>
</div>
<div id="divfila">
<div id="lado1">
<span class="oblig">*</span> Fecha
</div>
<div id="lado2">
: <input type="text" name="fecha_fac" id="fecha_fac" value="<?php echo date("d/m/Y"); ?>" maxlength="10" />
</div>
</div>
<div id="divfila">
<div id="lado1">
<span class="oblig">*</span> Monto
</div>
<div id="lado2">
: <input type="text" name="monto_fac" id="monto_fac" maxlength="60" onkeypress="return validar_solonumeros_dec(event, this, true);" />
</div>
</div>
<div id="lado4">
<input type="hidden" name="docgen_fac" id="docgen_fac" value="" />
<input type="hidden" name="tipo" id="tipo" value="buscaanonima" />
<input type="button" name="btn_buscar" id="btn_buscar" value="Buscar" onclick="buscar_factura_anonima();" class="k-button" /> <input type="button" name="btn_nuevo" id="btn_nuevo" value="Nuevo" onclick="buscar_nuevo_factura_anonima();" class="k-button" /> <input type="button" name="btn_pdf" id="btn_pdf" value="PDF" onclick="generar_factura_anonima('.pdf');" class="k-button" /> <input type="button" name="btn_xml" id="btn_xml" value="XML" onclick="generar_factura_anonima('.xml');" class="k-button" />
</div>
</form>