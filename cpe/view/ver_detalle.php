<?php
session_start();
include('../nusoap/nusoap.php');
if(trim($_SESSION["ruc_empresa"])!=""){
include("../cn/config".$_SESSION["ruc_empresa"].".php");
}else{
include("../cn/config.php");
}
unset($_SESSION['detalle']);
	$titulo="Ver accesos del usuario ".$_GET["usuario"];
	$client = new nusoap_client($ruta_ws,true);
	$err  = $client->getError();
if ($err) {
 echo '<h2>Constructor error</h2><pre>' . $err . '</pre>';
}
$param = array("idusuario"=>$_GET["idusuario"]);
$result = $client->call('sp_webfe_GetAcceso',$param);
if ($client->fault) {
echo 'No se pudo completar la operaci贸n'; 
die();
} else { 
$err = $client->getError();
if ($err) {
echo 'Error:' . $err; 
die(); 
} 
} 
if(isset($result["sp_webfe_GetAccesoResult"]["Accesos"]) && trim($result["sp_webfe_GetAccesoResult"]["Accesos"][0]["ip"])!=""){
$contadorxml=$result["sp_webfe_GetAccesoResult"]["Accesos"];
}else{
$contadorxml=$result["sp_webfe_GetAccesoResult"];
}
if($contadorxml>0){
for($j=0;$j<count($contadorxml);$j++){
if(count($contadorxml)==1){
$fila2=$result["sp_webfe_GetAccesoResult"]["Accesos"];
}else{
$fila2=$result["sp_webfe_GetAccesoResult"]["Accesos"][$j];
}
$_SESSION["detalle"][]=array("ip" => $fila2["ip"],"nombre" => $fila2["nombre"],"fecha" => $fila2["fecha"],"hora" => $fila2["hora"]);
}
}
?>
<div id="onclose"><img src="img/close.png" onclick="cerrar_ventana('ver_detalle')" /></div>
<div id="lado100" style="font-weight:bold; text-align:center">
<?php echo $titulo; ?>
</div>
<div id="lado100">
<div id="gridprod_detalle" style="height: 200px">
</div>
<script>
$(document).ready(function() {
$("#gridprod_detalle").kendoGrid({
						  dataSource: {
						       transport: {
							    read:  {
                                    url: "controller/Llenar_detalle_Grilla.php",
                                    }

	                       
									            
	                    },
					error: function(e) {
    jAlert("Request failed with status " + e.status, "Aviso");
  },
						
	                },
                        columns: [{					          
					            attributes: {
								style: "width: 20%"
                                },
								headerAttributes: {
                                style: "width: 20%"
                                },
                               template: '#: ip#',
								title: "IP"								
								},{					          
					            attributes: {
								style: "width: 20%"
                                },
								headerAttributes: {
                                style: "width: 20%"
                                },
                               template: '#: nombre#',
								title: "Nombre de PC"								
								} ,{					          
					            attributes: {
								style: "width: 20%"
                                },
								headerAttributes: {
                                style: "width: 20%"
                                },
                               template: '#: fecha#',
								title: "Fecha"								
								},{					          
					            attributes: {
								style: "width: 40%"
                                },
								headerAttributes: {
                                style: "width: 40%"
                                },
                               template: '#: hora#',
								title: "Hora"								
								}                          
                        ]						
                    });
});
</script>
