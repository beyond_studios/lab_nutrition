<?php
session_start();
include("cn/config.php");
$correo="";
if(isset($_SESSION["correo"])){
 $correo=trim($_SESSION["correo"]); 
}
if($correo!=""){
  $acceso=1;
}else{
  $acceso=0;
}
$nivusu=2;
unset($_SESSION["ruc_empresa"]);
if(isset($_SESSION["cambiar_clave"]) && trim($_SESSION["cambiar_clave"])==1){
  echo "<script>location.href ='cambiar_clave.php';</script>";
}
if(isset($_SESSION["usuariofe"]) && trim($_SESSION["usuariofe"])!=""){
  echo "<script>location.href ='inicio.php';</script>";
}


?>
<!DOCTYPE html>
<html lang="es">
<head>
<title>Facturaci&oacute;n electr&oacute;nica</title>
<meta charset="utf-8" />
<link rel="shortcut icon" href="/favicon.ico" />
<link rel="alternate" title="Pozoler�a RSS" type="application/rss+xml" href="/feed.rss" />
 <link href="css/jquery.bxslider.css" rel="stylesheet" media="screen" />
 <link href="css/estilos_ini.css" rel="stylesheet" media="screen" />
 <link href="css/kendo.common.min.css" rel="stylesheet">
  <link href="css/kendo.bootstrap.min.css" rel="stylesheet">
 <link href="css/bootstrap.min.css" rel="stylesheet" media="screen" />
 <link href="css/jquery.alerts.css" rel="stylesheet" media="screen"> 
 <link href="//cdn.bootcss.com/tether/1.3.6/css/tether.min.css" rel="stylesheet">
 <link href="css/font-awesome.min.css" rel="stylesheet">
</head>
 
<body>
   <div id="bgtransparent" style="display:none"></div>
<div id="cargador" style="display:none"><div id="text"><img src="img/loading.gif" style="max-height: 100px;" /></div></div>
    <header>
   <div class="container py-3"> 
     <img src="img/logo.png" style="height: 75px;">
   </div>  
    </header>
    <h4 class="azulnava text-center pb-3">
FACTURACI&Oacute;N ELECTR&Oacute;NICA
</h4>
    <section>
      <article>
      <div class="container">
  <div class="row">
    <div class="col-xs-12 col-sm-7">
     <?php
if($multiempresa==1){
$buscar_factura_anonima="buscar_factura_anonima_multiempresas()";
$validar_usuario="validar_usuario_multiempresas()";
$altura_registro=300;
}else{
$buscar_factura_anonima="buscar_factura_anonima()";
$validar_usuario="validar_usuario()";
$altura_registro=270;
}
?>
<form method="post" name="forminicio" id="forminicio">
  <input type="hidden" name="tipo" id="tipo" value="buscaanonima" />
 <?php
if($multiempresa==1){
?>
<div class="form-group row">
 <div class="col-xs-12 col-sm-12">
 
<input type="text" class="form-control" name="ruc_empresa" id="ruc_empresa" maxlength="11" placeholder="R.U.C. del emisor" />
</div>
</div>
 <?php
}
?>
<div class="form-group row">
 <div class="col-xs-12 col-sm-12">
  <input type="radio" class="form-check-input" name="opini" id="opini1" value="login" />
  <label class="form-check-label" for="opini1">
    Usuario Registrado
  </label> 
</div>
</div>
<div class="form-group row">
<div class="col-xs-6 col-sm-6">
<input type="text" class="form-control" name="user" id="user" maxlength="100" onKeyUp="if(event.keyCode == 13){<?php echo $validar_usuario;?> }" placeholder="Usuario" />
</div> 
 <div class="col-xs-6 col-sm-6">
 <input type="password" class="form-control" name="pass" id="pass" maxlength="11"  onKeyUp="if(event.keyCode == 13){<?php echo $validar_usuario;?> }" placeholder="Contrase&ntilde;a"  />
</div>
</div>
<div class="form-group row">
<div class="col-xs-6 col-sm-6">
<input type="text" class="form-control" name="captcha" id="captcha" maxlength="6" placeholder="Ingrese el c&oacute;digo"  />
</div> 
 <div class="col-xs-6 col-sm-6">
 <img src="captcha.php" name="imgcap" id="imgcap"  /> <img src="img/refresh.png" onClick="recargar_captcha()" title="Recargar" style="max-width:24px; margin-bottom:13px;" />
</div>
</div>
<div class="form-group row">
 <div class="col-xs-12 col-sm-12">
  <input type="radio" class="form-check-input" name="opini" id="opini2" value="buscaanonima" checked="checked" />
  <label class="form-check-label" for="opini2">
    Usuario Libre
  </label> 
</div>
</div>
<div class="form-group row form-inline">
<div class="col-xs-6 col-sm-6">
<select class="custom-select my-1 mr-sm-2" name="cdocu_fac" id="cdocu_fac">
<option value="01">Factura</option>
<option value="03">Boleta Vta.</option>
<option value="07">Nota: Cr&eacute;dito</option>
<option value="08">Nota: D&eacute;bito</option>
<option value="20">Retenci&oacute;n Electr&oacute;nico</option>
<option value="67">Percepci&oacute;n Electr&oacute;nico</option>
</select>
</div> 
 <div class="col-xs-6 col-sm-6">
 <input type="text" class="form-control" name="serie_fac" id="serie_fac" style=" text-transform:uppercase;width: 30%;" maxlength="4" placeholder="Serie"  />
 <input type="text" class="form-control" name="ndocu_fac" id="ndocu_fac" maxlength="7" style="width: 68%;" onkeypress="return validar_solonumeros(event, this, true);" placeholder="N&uacute;mero" />
</div>
</div>
<div class="form-group row">
<div class="col-xs-6 col-sm-6">
<input type="text" class="form-control" name="fecha_fac" id="fecha_fac" value="<?php echo date("d/m/Y"); ?>" maxlength="10" />
</div> 
 <div class="col-xs-6 col-sm-6">
<input class="form-control" type="text" name="monto_fac" id="monto_fac" maxlength="60"  onkeypress="return validar_solonumeros_dec(event, this, true);" placeholder="Monto" />

</div>
</div>
<input type="hidden" name="docgen_fac" id="docgen_fac" value="" />
<div id="buttomsave">
<div class="form-group row">
<div class="col-xs-6 col-sm-6">
<input type="button" class="btn btn-primary w-100" name="btn_buscar" id="btn_buscar" value="Buscar" onclick="<?php echo $buscar_factura_anonima;?>" />
</div> 
 <div class="col-xs-6 col-sm-6">
 <input type="button" class="btn btn-secondary w-100" name="btn_nuevo" id="btn_nuevo" value="Nueva consulta" onclick="buscar_nuevo_factura_anonima()" />
</div>
</div>
</div>
<div id="buttomsaveini" style="display: none;">
<div class="form-group row">
<div class="col-xs-6 col-sm-6">
<input class="btn btn-primary w-100" type="button" name="save" id="save" value="Ingresar" onclick="<?php echo $validar_usuario;?>"  />
</div> 
 <div class="col-xs-6 col-sm-6">
 <input type="button" class="btn btn-secondary w-100" name="back" id="back" value="Nueva consulta" onclick="buscar_nuevo_login();" /> 
</div>
</div>
</div>

<div class="form-group row">
<div class="col-xs-6 col-sm-6 text-center">
  <button type="button" class="btn btn-link" data-toggle="modal" data-target="#exampleModalLong" id="btnregistrar"><i class="fa fa-plus"></i>&nbsp; Registrarse</button>
</div> 
</div>
</form>
    </div>
    <div class="col-xs-12 col-sm-5">
      <img src="img/fe1.png" style="max-width: 100%" class="img-responsive mx-auto d-block" />
    </div>
  </div>
</div>
    </article>
    </section>
    
  <footer>
    <div class="container-fluid">
      <div class="row text-center bgazulnava text-white" style=" height: 35px;">
      <div class="col-xs-12 col-sm-12 my-auto">
     Todos los derechos reservados | 2018 | IT Consultores NavaSoft SAC | www.navasoft.com.pe
</div>
</div>

</div>
    </footer>
<!-- Modal -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Registrarse</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <div class="modal-body">
<form name="formregistrar" id="formregistrar" method="post">
<input type="hidden" name="tipoing" id="tipoing" value="registrar" />
 <?php 
if($multiempresa==1){
$buscar_clientes="buscar_clientes_multiempresas(".$nivusu.");";
}else{
$buscar_clientes="buscar_clientes_ini(".$nivusu.");";
}
 if($multiempresa==1){ 
  ?>
  <div class="form-group row">
 <div class="col-xs-12 col-sm-12">

<input type="text" class="form-control" name="ruc_empresa_ing" id="ruc_empresa_ing" maxlength="11" placeholder="R.U.C. del emisor" />
 
</div>
</div>
<?php } ?>
 <div class="form-group row">
 <div class="col-xs-12 col-sm-12">
<div class="input-group">
  <input type="text" class="form-control" name="ruc_reg" id="ruc_reg" maxlength="11" placeholder="R.U.C." />
  <div class="input-group-prepend">
    <div class="input-group-text">
     <img  src="img/icon_search.png"  title="Buscar cliente" onclick="<?php echo $buscar_clientes;?>" style="margin:0 0 0 5px; padding:0;max-height:100%;cursor:pointer; vertical-align:middle" />
    </div>
  </div>
</div>
</div>

</div>
 <div class="form-group row">
 <div class="col-xs-12 col-sm-12">
<input type="text" class="form-control" name="nombres_reg" id="nombres_reg" maxlength="60" readonly="readonly" placeholder="Cliente" /> 
</div>
</div>
<div class="form-group row">
 <div class="col-xs-12 col-sm-12">
<input type="text" class="form-control" name="email_reg" id="email_reg" maxlength="80" placeholder="Email" /> 
</div>
</div>
<div class="form-group row">
 <div class="col-xs-12 col-sm-12">
<input type="password" class="form-control" name="contrasena_reg" id="contrasena_reg" maxlength="20" placeholder="Contrase&ntilde;a" /> 
</div>
</div>
<div class="form-group row">
 <div class="col-xs-12 col-sm-12">
<input type="password" class="form-control" name="recontrasena_reg" id="recontrasena_reg" maxlength="20" placeholder="Repetir contrase&ntilde;a" /> 
</div>
</div>
</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" onclick="registrar_usuario(<?php echo $nivusu;?>);">Guardar</button>
      </div>
    </div>
  </div>
</div>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script>
 <script src="//cdn.bootcss.com/tether/1.3.6/js/tether.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
 <!--<script src="js/kendo.web.min.js"></script> -->
<script src="js/jquery.bxslider.js"></script>
 <script src="js/kendo.web.min.js"></script>
 <script src="js/scripts.js"></script>
 <script src="js/jquery.alerts.js" type="text/javascript"></script>
 <script src="js/bootbox.min.js"></script>
 <script type="text/javascript">
 $(document).ready(function() {
  
   document.getElementById("user").focus();
   $("#btnregistrar").on("click",function(event){  
     $('#formregistrar').trigger("reset"); 
     <?php 
 if($multiempresa==1){ 
  ?>
     $('#ruc_empresa_ing').attr('readonly', false);
     <?php 
 }
  ?>
     $('#ruc_reg').attr('readonly', false);
});
});
</script>
<script>
$(document).ready(function() {
  $( "body" ).css( "min-height","+="+window.innerHeight);
  $("#btn_pdf").kendoButton({
                       imageUrl: "img/file_extension_pdf.png"
                    });
  $("#btn_xml").kendoButton({
                       imageUrl: "img/file_extension_xml.png"
                    });
<?php
    if(isset($_SESSION['avizofinsesion']) && $_SESSION['avizofinsesion']==1){
    ?>
    bootbox.alert('Lo sentimos, su tiempo de inactividad cerr\u00f3 la sesi\u00f3n, vuelva a conectarse.');
    <?php
    unset($_SESSION['avizofinsesion']);
    }
    ?>
    <?php if(isset($_SESSION['archivo_vacio']) && trim($_SESSION["archivo_vacio"])==1){ ?>
          bootbox.alert('El documento no existe.');
<?php 
unset($_SESSION["archivo_vacio"]);
} ?>
    /*document.getElementById("serie_fac").value="F";*/
    $("#serie_fac").focus();
    $('#user').attr('disabled', true);
    $('#pass').attr('disabled', true);
    $('#captcha').attr('disabled', true);
    $('#buttomsaveini').hide();
    $('#btn_pdf').hide();
        $('#btn_xml').hide();
    $("#fecha_fac").kendoDatePicker({
                    format: "dd/MM/yyyy"
                });
        $("#cdocu_fac").change(function(e) {
          /*if($("#cdocu_fac").val()=="01"){
            document.getElementById("serie_fac").value="F";
            }
          else if($("#cdocu_fac").val()=="03"){
            document.getElementById("serie_fac").value="B";
            }
          else if($("#cdocu_fac").val()=="20"){
            document.getElementById("serie_fac").value="R";
            }
          else if($("#cdocu_fac").val()=="67"){
            document.getElementById("serie_fac").value="P";
            }
          else{
            document.getElementById("serie_fac").value="";
          }*/
          document.getElementById("serie_fac").value="";
          $("#serie_fac").focus();
        }); 
        
        $('input[type=radio][name=opini]').change(function(e) {
          if($("input[type=radio][name=opini]:checked").val()=="login"){
            $('#buttomsaveini').show();
            $('#buttomsave').hide();
            $('#cdocu_fac').attr('disabled', true);
                      $('#ndocu_fac').attr('disabled', true);
            $('#serie_fac').attr('disabled', true);
                      $('#monto_fac').attr('disabled', true);
                      var datepicker = $("#fecha_fac").data("kendoDatePicker");
                        datepicker.enable(false);
            $('#user').attr('disabled', false);
            $('#pass').attr('disabled', false);
            $('#captcha').attr('disabled', false);
            document.getElementById("tipo").value="login";
            $('#btn_pdf').hide();
                        $('#btn_xml').hide();
            document.getElementById("ndocu_fac").value="";
            document.getElementById("serie_fac").value="";
                      document.getElementById("monto_fac").value="";
                      document.getElementById("docgen_fac").value="";
            $("#user").focus();
            }
            if($("input[type=radio][name=opini]:checked").val()=="buscaanonima"){
            $('#buttomsaveini').hide();
            $('#buttomsave').show();
            $('#cdocu_fac').attr('disabled', false);
                      $('#ndocu_fac').attr('disabled', false);
            $('#serie_fac').attr('disabled', false);
                      $('#monto_fac').attr('disabled', false);
                      var datepicker = $("#fecha_fac").data("kendoDatePicker");
                        datepicker.enable(true);
            $('#user').attr('disabled', true);
            $('#pass').attr('disabled', true);
            $('#captcha').attr('disabled', true);
            document.getElementById("tipo").value="buscaanonima";
            document.getElementById("user").value="";
            document.getElementById("pass").value="";
            document.getElementById("captcha").value="";
            document.getElementById("cdocu_fac").value="01";
          /*  document.getElementById("serie_fac").value="F";*/
            $("#serie_fac").focus();
            
              }
               
              document.getElementById("docgen_fac").value="";
});

})
</script>
     <!� Llamamos al  JavaScript de Bootstrap �>
</body>
</html>
