<?php
session_start();
include('../nusoap/nusoap.php');
if(isset($_SESSION["ruc_empresa"]) && trim($_SESSION["ruc_empresa"])!=""){
include("../cn/config".$_SESSION["ruc_empresa"].".php");
}else{
include("../cn/config.php");
}
ini_set('max_execution_time', 0);
unset($_SESSION["excelfac"]);
if($_SESSION["usuariofe"]==$correo_admin){
	$nivusu=1;
}else{
	$nivusu=2;
}
$i=0;
if(trim($_SESSION["buscarfactura"]["ruccli"])!="" || trim($_SESSION["buscarfactura"]["nomcli"])!=""){
$nerror=0;
/*$bit = ini_get('error_reporting'); 
while ($bit > 0) { 
 $nerror=$nerror+1;
    } */
$client = new nusoap_client($ruta_ws,true);
$err  = $client->getError();
if ($err) {
 /*echo '<h2>Constructor error</h2><pre>' . $err . '</pre>';*/
 $nerror=$nerror+1;
}
$param = array("fechafin"=>$_SESSION["buscarfactura"]["p3"],"fechaini"=>$_SESSION["buscarfactura"]["p2"],"ruccli"=>$_SESSION["buscarfactura"]["ruccli"],"nomcli"=>utf8_decode($_SESSION["buscarfactura"]["nomcli"]),"nivusu"=>$nivusu);
$result = $client->call('sp_webfe_Busqueda_MstFac',$param);
if ($client->fault) {
/*echo 'No se pudo completar la operación'; */
/*die(); */
$nerror=$nerror+1;
} else { 
$err = $client->getError();
if ($err) {
/*echo 'Error:' . $err; */
/*die();*/ 
$nerror=$nerror+1;
} 
}
if($nerror>0){
	$ocultar="style=display:none";
	$data[0]=array("ndocu" => "","ruccli" => "","codcli" => "","nomcli" => "ERROR DE CONEXIÓN","nomdoc" => "","fecha" => "","monto" => "","flag" => "","documento" =>"","isdocu" => "","ocultar" => $ocultar);
	header("Content-type: application/json");

echo json_encode($data);
exit();
}
if(isset($result["sp_webfe_Busqueda_MstFacResult"]["Facturas"]) && isset($result["sp_webfe_Busqueda_MstFacResult"]["Facturas"][0]["ndocu"]) && trim($result["sp_webfe_Busqueda_MstFacResult"]["Facturas"][0]["ndocu"])!=""){
$contadorxml=$result["sp_webfe_Busqueda_MstFacResult"]["Facturas"];
}else{
$contadorxml=$result["sp_webfe_Busqueda_MstFacResult"];
}
if($contadorxml>0){
for($j=0;$j<count($contadorxml);$j++){
if(count($contadorxml)==1){
$fila2=$result["sp_webfe_Busqueda_MstFacResult"]["Facturas"];
}else{
$fila2=$result["sp_webfe_Busqueda_MstFacResult"]["Facturas"][$j];
}
if(is_numeric(substr(trim($fila2["ndocu"]), 0, 1))) {
	$isdocu=0;
	$ocultar="style=display:none";
	$flag="Manual";
}else{
	$isdocu=1;
	if(trim($fila2["flag"])=="*"){
		$ocultar="style=display:none";
		$flag="Electrónico";
	}else{
		$ocultar="";
		$flag="Electrónico";
	}
	
}
if(trim($fila2["mone"])=="S"){
$signo="S/. ";
}else{
$signo="US$ ";
}
$data[$j]=array("ndocu" => $fila2["ndocu"],"ruccli" => $fila2["ruccli"],"codcli" => $fila2["codcli"],"nomcli" => utf8_encode($fila2["nomcli"]),"nomdoc" => $fila2["nomdoc"],"fecha" => $fila2["fecha"],"monto" => $signo.$fila2["monto"],"flag" => utf8_encode(utf8_decode($flag)),"documento" => trim($ruccia_web)."-".trim($fila2["cdocu"])."-".trim($fila2["ndocu"]),"isdocu" => $isdocu,"ocultar" => $ocultar);
$_SESSION["excelfac"][]=array("ndocu" => $fila2["ndocu"],"ruccli" => $fila2["ruccli"],"codcli" => $fila2["codcli"],"nomcli" => utf8_encode($fila2["nomcli"]),"nomdoc" => $fila2["nomdoc"],"fecha" => $fila2["fecha"],"monto" => $signo.$fila2["monto"],"flag" => utf8_decode($flag),"documento" => trim($ruccia_web)."-".trim($fila2["cdocu"])."-".trim($fila2["ndocu"]));
	$i=$i+1;
}
}
}
if($i==0){
$data=array();
}
header("Content-type: application/json");

echo json_encode($data);

	

?>