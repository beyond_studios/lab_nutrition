<?php
session_start();
if(isset($_SESSION["ruc_empresa"]) && trim($_SESSION["ruc_empresa"])!=""){
include("../cn/config".$_SESSION["ruc_empresa"].".php");
}else{
include("../cn/config.php");
}
include('../nusoap/nusoap.php');

ini_set('max_execution_time', 0);
include('../controller/funciones.php');
if(isset($_POST["tipo"]) && $_POST["tipo"]=="buscar"){
$descripcion_error="";
$client = new nusoap_client($ruta_ws,true);
$numero_error=0;
$err  = $client->getError();
if ($err) {
	$descripcion_error.='<h2>Constructor error</h2><pre>' . $err . '</pre>';
 /*echo '<h2>Constructor error</h2><pre>' . $err . '</pre>';*/
 $numero_error=$numero_error+1;
}
$param = array("ruccli"=>$_POST["ruc"]);
$result = $client->call('sp_webfe_Validar_Cliente',$param);
 $i=0;
if ($client->fault) {
	$descripcion_error.= 'No se pudo completar la operación';
/*echo 'No se pudo completar la operación'; */
$numero_error=$numero_error+1;
/*die(); */
} else { 
$err = $client->getError();
 
if ($err) { 
$descripcion_error.= 'Error:' . $err;
/*echo 'Error:' . $err; */
$numero_error=$numero_error+1;
/*die(); */
} 
}
if($numero_error>0){
if($ver_detalle_error==0){
	$descripcion_error="Error de conexi&oacute;n";
}
$msg = array("nomcli" => "ERROR", 'emailfe2' => $descripcion_error);
header('content-type: application/json');
echo json_encode($msg);
}else if(isset($result["sp_webfe_Validar_ClienteResult"]["Cliente"]) && count($result["sp_webfe_Validar_ClienteResult"]["Cliente"])>1){
$_SESSION["registro"]["nomcli"]=$result["sp_webfe_Validar_ClienteResult"]["Cliente"]["nomcli"];
$_SESSION["registro"]["ruccli"]=$result["sp_webfe_Validar_ClienteResult"]["Cliente"]["ruccli"];
$_SESSION["registro"]["codcli"]=$result["sp_webfe_Validar_ClienteResult"]["Cliente"]["codcli"];
$_SESSION["registro"]["emailfe2"]=$result["sp_webfe_Validar_ClienteResult"]["Cliente"]["emailfe2"];
$msg = array('nomcli' => $result["sp_webfe_Validar_ClienteResult"]["Cliente"]["nomcli"], 'emailfe2' => $result["sp_webfe_Validar_ClienteResult"]["Cliente"]["emailfe2"]);
header('content-type: application/json');
echo json_encode($msg);
}else{
$_SESSION["registro"]["nomcli"]="";
$_SESSION["registro"]["ruccli"]="";
$_SESSION["registro"]["codcli"]="";
$_SESSION["registro"]["emailfe2"]="";
echo 0;
} 
}
if(isset($_POST["tipo"]) && $_POST["tipo"]=="login"){
if($_SESSION['cap_code']==$_REQUEST["captcha"]){
$descripcion_error="";
$numero_error=0;
$client = new nusoap_client($ruta_ws,true);
$err  = $client->getError();
if ($err) {
$descripcion_error.="<h2>Constructor error</h2><pre>" . $err . "</pre>";
/* echo '<h2>Constructor error</h2><pre>' . $err . '</pre>';*/
$numero_error=$numero_error+1;
}
$param = array("cusuario" => $_POST["user"],"cclave" => $_POST["pass"],"codcia" => $codcia_web);
$result = $client->call('sp_webfe_login',$param);
 $i=0;
if ($client->fault) {
$numero_error=$numero_error+1;
$descripcion_error.='No se pudo completar la operación';
/*echo 'No se pudo completar la operación'; 
die();*/ 
} else {
$err = $client->getError();

if ($err) { 
$descripcion_error.='Error:' . $err;
/*
echo 'Error:' . $err; 
die(); */
$numero_error=$numero_error+1;
} 
}
if($numero_error>0){
if($ver_detalle_error==0){
	$descripcion_error="Error de conexi&oacute;n";
}
$msg = array("error" => "ERROR", 'desc_error' => $descripcion_error);
header('content-type: application/json');
echo json_encode($msg);

}else if(isset($result["sp_webfe_loginResult"]["Usuarios"]) && count($result["sp_webfe_loginResult"]["Usuarios"])>1){
	$_SESSION["usuariofe"]=trim($result["sp_webfe_loginResult"]["Usuarios"]["usuariofe"]);
	$_SESSION["ruccli"]=$result["sp_webfe_loginResult"]["Usuarios"]["ruccli"];
	$_SESSION["codcli"]=$result["sp_webfe_loginResult"]["Usuarios"]["codcli"];
	$_SESSION["nomcli"]=$result["sp_webfe_loginResult"]["Usuarios"]["nomcli"];
	$_SESSION["diasclave"]=$result["sp_webfe_loginResult"]["Usuarios"]["diasclave"];
	$_SESSION["codcia"]=$result["sp_webfe_loginResult"]["Usuarios"]["codcia"];
	$_SESSION["nomcia"]=$result["sp_webfe_loginResult"]["Usuarios"]["nomcia"];
	$_SESSION["dircia"]=$result["sp_webfe_loginResult"]["Usuarios"]["dircia"];
	$_SESSION["telcia"]=$result["sp_webfe_loginResult"]["Usuarios"]["telcia"];
	$_SESSION["ruccia"]=$result["sp_webfe_loginResult"]["Usuarios"]["ruccia"];
	$_SESSION["idusuario"]=$result["sp_webfe_loginResult"]["Usuarios"]["idusuario"];
$ip=getRealIP();
$nombre_host = gethostbyaddr($_SERVER['REMOTE_ADDR']);
$param2 = array("equipo_ip" => $ip,"equipo_nombre" => $nombre_host,"idusuario" => $_SESSION["idusuario"]);
$result2 = $client->call('sp_webfe_Insertar_Acceso',$param2);
if ($client->fault) {
echo 'No se pudo completar la operación'; 
/*die();*/ 
} else { // No 
$err2 = $client->getError();
// Hay algun error ? 
if ($err2) { // Si 
echo 'Error:' . $err2; 
/*die(); */
} 
} 
if($_SESSION["diasclave"]>=$vigencia_clave){
$_SESSION["cambiar_clave"]=1;
$_SESSION["clave_actual"]=$_POST["pass"];
echo 4;
}else{
echo 1;
}
	
}else{
	echo 2;
}
}else{
	echo 3;
}
}
if(isset($_POST["tipoing"]) && $_POST["tipoing"]=="registrar"){
	$ruccli_reg=$_SESSION["registro"]["ruccli"];
	$codcli_reg=$_SESSION["registro"]["codcli"];
	$email_reg=$_POST["email_reg"];
	$contrasena_reg=$_POST["contrasena_reg"];
	$numero_error=0;
	if(trim($email_reg)!=trim($_SESSION["registro"]["emailfe2"])){
		echo 0;
		}else{
		$client = new nusoap_client($ruta_ws,true);
		$err  = $client->getError();
if ($err) {
 /*echo '<h2>Constructor error</h2><pre>' . $err . '</pre>';*/
 $numero_error=$numero_error+1;
}
$param = array("usuario"=>$email_reg,"clave"=>$contrasena_reg,"ruccli"=>$ruccli_reg,"codcli"=>$codcli_reg);
$result = $client->call('sp_webfe_Insertar_Usuario',$param);
 $i=0;
if ($client->fault) {
$numero_error=$numero_error+1;
/*echo 'No se pudo completar la operación'; 
die(); */
} else { 
$err = $client->getError();

if ($err) { 
$numero_error=$numero_error+1;
/*echo 'Error:' . $err; 
die(); */
} 
}
if($numero_error>0){
echo "ERROR";
}else{
echo 1;
}
		
			}
}
if(isset($_POST["tipo"]) && $_POST["tipo"]=="eliminar"){
		$client = new nusoap_client($ruta_ws,true);
		$err  = $client->getError();
if ($err) {
 echo '<h2>Constructor error</h2><pre>' . $err . '</pre>';
}
$param = array("idusuario"=>$_POST["idusuario"]);
$result = $client->call('sp_webfe_bajar_usuario',$param);
 $i=0;
if ($client->fault) {
echo 'No se pudo completar la operación'; 
die(); 
} else { // No 
$err = $client->getError();
// Hay algun error ? 
if ($err) { // Si 
echo 'Error:' . $err; 
die(); 
} 
}
		echo 1;
			
}
if(isset($_POST["tipo"]) && $_POST["tipo"]=="cambiarclave"){
	if($_SESSION["clave_actual"]==$_POST["pass"]){
		echo 2;
		}else{
		$client = new nusoap_client($ruta_ws,true);
		$err  = $client->getError();
if ($err) {
 echo '<h2>Constructor error</h2><pre>' . $err . '</pre>';
}
$param = array("idusuario"=>$_SESSION["idusuario"],"clave"=>$_POST["pass"]);
$result = $client->call('sp_webfe_cambiar_clave',$param);
 $i=0;
if ($client->fault) {
echo 'No se pudo completar la operación'; 
die(); 
} else { // No 
$err = $client->getError();
// Hay algun error ? 
if ($err) { // Si 
echo 'Error:' . $err; 
die(); 
} 
}
$_SESSION["cambiar_clave"]="";
$_SESSION["clave_actual"]="";
		echo 1;
		}
}
?>