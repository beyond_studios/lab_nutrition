<?php
function queBrowser() {
$user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';  

if (strpos($user_agent, 'Opera') !== false) {  
	$browser = 'Opera';  
} elseif (strpos($user_agent, 'Chrome') !== false) {  
	$browser = 'Google Chorme';  
} elseif (strpos($user_agent, 'Firefox/2') !== false) {  
	$browser = 'Firefox 2';  
} elseif (strpos($user_agent, 'Firefox/3') !== false) {  
	$browser = 'Firefox 3';  
} elseif (strpos($user_agent, 'Firefox') !== false) {  
	$browser = 'Firefox (Version desconocida)';  
} elseif (strpos($user_agent, 'Safari') !== false) {  
	$browser = 'Safari';  
} elseif (strpos($user_agent, 'MSIE 6') !== false) {  
	$browser = 'Internet Explorer 6';  
} elseif (strpos($user_agent, 'MSIE 7') !== false) {  
	$browser = 'Internet Explorer 7'; 
} elseif (strpos($user_agent, 'MSIE 8') !== false) {  
	$browser = 'Internet Explorer 8'; 
} elseif (strpos($user_agent, 'MSIE') !== false) {  
	$browser = 'Internet Explorer (Version desconocida)'; 
} else {  
	$browser = 'Otro';  
}

return $browser;

}
/*limpiar los caracteres prohibidos */
function eliminar_caracteres_prohibidos($valor)
{
$caracteres_prohibidos = array("'","/","<",">",";","\"");    
return str_replace($caracteres_prohibidos,"",$valor);
}
/*callcula el IP */
function getRealIP() {
if (!empty($_SERVER['HTTP_CLIENT_IP']))
	return $_SERVER['HTTP_CLIENT_IP'];

if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
	return $_SERVER['HTTP_X_FORWARDED_FOR'];

	return $_SERVER['REMOTE_ADDR'];
}
/*verifica el ordenador */
function verificar_ordenador(){
$tablet_browser = 0;
$mobile_browser = 0;
$body_class = 'desktop';

if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
	$tablet_browser++;
	$body_class = "tablet";
}

if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
	$mobile_browser++;
	$body_class = "mobile";
}

if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
	$mobile_browser++;
	$body_class = "mobile";
}

$mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
$mobile_agents = array(
'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
'newt','noki','palm','pana','pant','phil','play','port','prox',
'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
'wapr','webc','winw','winw','xda ','xda-');

if (in_array($mobile_ua,$mobile_agents)) {
	$mobile_browser++;
}

if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'opera mini') > 0) {
	$mobile_browser++;
	//Check for tablets on opera mini alternative headers
	$stock_ua = strtolower(isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA'])?$_SERVER['HTTP_X_OPERAMINI_PHONE_UA']:(isset($_SERVER['HTTP_DEVICE_STOCK_UA'])?$_SERVER['HTTP_DEVICE_STOCK_UA']:''));
	if (preg_match('/(tablet|ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
		$tablet_browser++;
	}
}
if ($tablet_browser > 0) {
	// Si es tablet has lo que necesites
	$variable_br= 'tablet';
}
else if ($mobile_browser > 0) {
	// Si es dispositivo mobil has lo que necesites
	$variable_br= 'mobil';
}
else {
	// Si es ordenador de escritorio has lo que necesites
	$variable_br= 'escritorio';
} 
return $variable_br;
}
function recortar_texto($texto, $limite=100){	
	$texto = trim($texto);
	$texto = strip_tags($texto);
	$tamano = strlen($texto);
	$resultado = '';
	if($tamano <= $limite){
		return $texto;
	}else{
		$texto = substr($texto, 0, $limite);
		$palabras = explode(' ', $texto);
		$resultado = implode(' ', $palabras);
		$resultado .= '...';
	}	
	return $resultado;
}
?>