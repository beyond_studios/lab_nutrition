<?php
$style = '
<style type="text/css">
*{
font-family: Arial, Helvetica, Verdana, sans-serif;
font-size: 10px;
}

h1, h2 {
text-indent: 10px;
}

tr#titulo{
color: white;
}

tr#filas {
background-color: white;
}

h1, tr#filas, tr#total {
color: #3f547f;
}


h1 {
font-size: 14px;
text-align: center;
}

p {
text-indent: 15px;
color: black;
}

.razon {
color: black;
font-weight: bold;
font-size: 10px;
text-decoration: underline;
}

.der {
text-indent: 45em;
}

.resalt {
font-size: 10px;
color: #5f78af;
}

.centrar {
text-align: center;
}

#centrar {
position: relative;
left: 20%;
}

.derecha {
text-align: right;
}

.forceder {
clear: right;
position: absolute;
float: right;
}

td#gris {
background-color: #cccccc;
}

td {
font-size: 10px;
}

#cabe td.razon {
padding-left: 50px;
width: 200px;
}

table#buscar{
margin: 0px;
padding: 0px;
width: 100%;
border-spacing: 0px;
border-collapse: collapse;
display: table;
}

table#buscar td {
empty-cells: show;
table-layout: fixed;
border: 1px solid #3d568d;
border-spacing: 0px;
padding: 2px 5px;
font-size: 10px;
}

tr#titulo {
background-color: #5f78af;
text-align: center;
}

tr#filas {
text-indent: 10px;
color: black;
}

tr#total {
font-weight:bold;
font-size:16px;
}

tr#filas td.anulado {
background-color: #ff9999;
}

tr#filas td.aprobado {
background-color: #88aa55;
}

tr#filas td.desaprobado {
text-decoration: line-through;
color: #3f547f;
}
table#cabecera{
margin: 0px;
padding: 0px;
width: 100%;
border-spacing: 0px;
border-collapse: collapse;
display: table;
}
table#cabecera .logotit{
font-size: 30px;
text-align: center;
font-weight:bold;
color:#082F75;
}
table#cabecera .titulo{
font-size: 18px;
text-align: center;
color: #5f78af;
font-weight:bold;
}
table#cabecera .col1{
width: 33%;
text-align:left;
}
table#cabecera .col2{
width: 34%;
vertical-align:middle;
text-align:center;
vertical-align:bottom;
}
table#cabecera .col3{
width: 33%;
vertical-align:middle;
}
table#cabecera .col4{
width: 100%;
vertical-align:middle;
text-align:center;
}
</style>	
';
/*inicia sesion */
session_start();
if(isset($_SESSION["ruc_empresa"]) && trim($_SESSION["ruc_empresa"])!=""){
include("../cn/config".$_SESSION["ruc_empresa"].".php");
}else{
include("../cn/config.php");
}
ini_set('max_execution_time', 0);
$fecha_actual=date("d/m/Y");
$hora_actual=date("h:i:s");
$excelespecial=0;
if($_REQUEST['op']=="fac"){
$title1 = "FACTURAS DEL ".$_SESSION["buscarfactura"]["p2"]." AL ".$_SESSION["buscarfactura"]["p3"];
$title2 = "";
$title3 = "";
$file = "facturas.xls";
$cabecera = "";
$cabecera .= '<table id="cabecera">';
$cabecera .= '<tr>';
$cabecera .= '<td class="col1"  colspan="4"><span class="logotit">'.$_SESSION["nomcia"].'</span><br>'.$fecha_actual.' '.$hora_actual.'</td>';	
$cabecera .= '<td class="col2" colspan="4"></td>';	
$cabecera .= '<td class="col3" colspan="2"></td>';	
$cabecera .= '</tr>';
$cabecera .= '<tr>';
$cabecera .= '<td class="col1"></td>';	
$cabecera .= '<td class="col2" colspan="4"><span class="titulo">';
$cabecera .= $title1;
$cabecera .='</span></td>';	
$cabecera .= '<td class="col3" colspan="2"></td>';	
$cabecera .= '</tr>';
$cabecera .= '<tr>';
$cabecera .= '<td class="col1"></td>';	
$cabecera .= '<td class="col2" colspan="4"></td>';	
$cabecera .= '<td class="col3" colspan="2"></td>';	
$cabecera .= '</tr>';
$cabecera .= '</table>';	
$content = '';
$content .= '<table id="buscar">';
	$i=0;
	$s1=0;
	$s2=0;
	$s3=0;
	$s4=0;
	$s5=0;
	$s6=0;
	$t1=0;
	$t2=0;
	$t3=0;
	$t4=0;
	$t5=0;
	$t6=0;
	$acum_linea="";
	$acum_sublinea="";
	$acum_tipo="";
	$acum_umed="";
	$content .= '
    <tr id="titulo">
      <td>FECHA</td>
      <td>DOCUMENTO</td>
      <td>N&Uacute;MERO</td>
      <td>CLIENTE</td>
	  <td>MONTO</td>
      <td>STATUS</td>
    </tr>';

	for ($i = 0; $i < (sizeof($_SESSION['excelfac'])); $i++) {
	 $content .= '<tr id="filas">';
	  $content .= '<td>&nbsp;'.$_SESSION['excelfac'][$i]['fecha'].'</td>';
	  $content .= '<td>'.$_SESSION['excelfac'][$i]['nomdoc'].'</td>';
	  $content .= '<td>'.$_SESSION['excelfac'][$i]['ndocu'].'</td>';
	  $content .= '<td>'.$_SESSION['excelfac'][$i]['nomcli'].'</td>';
	  $content .= '<td align="right">'.$_SESSION['excelfac'][$i]['monto'].'</td>';
	  $content .= '<td>'.$_SESSION['excelfac'][$i]['flag'].'</td>';
	   $content .= '</tr>';
	}
$content .= '</table>';
}
$html = '';
$html .= '<html><head>';
$html .= $style;
$html .= '</head><body>';
$html .= $cabecera;
$html .= $content;
$html .= '</body></html>';	


header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header('Content-Disposition: attachment; filename='.$file);
header("Content-Transfer-Encoding: binary ");
echo $html; 
?>