function generar_popup(nombrediv,ancho,alto,ruta){
			$('#'+nombrediv).empty();
		var kendoWindowdc = $('#'+nombrediv).kendoWindow({
			
            title: false,
            width:ancho,
			height:alto,
			content:ruta,
			modal: true,
			visible:false
        });
    
    kendoWindowdc.data("kendoWindow").center().open();
}
function cerrar_ventana(nombrecapa){
			$("#"+nombrecapa).data("kendoWindow").close();
						}


function cerrar_sesion_sistema(){
			jConfirm("¿Est\u00e1 seguro de cerrar esta sesi\u00f3n?", "Atenci\u00f3n", function(r) {
	  
			if(r) {
	location.href='controller/salir.php';
			}
			});
	}

function validar_solonumeros_dec(event,element,_float)
{
event = event || window.event;
var charCode = event.which || event.keyCode;
if (charCode == 8 || charCode == 13 || (_float ? (element.value.indexOf('.') == -1 ? charCode == 46 : false) : false))
	return true;
else if ((charCode < 48) || (charCode > 57))
	return false;
	return true;
}
	
					function validar_solonumeros(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 45 || charCode > 57))
            return false;
 
         return true;
      }
	  
	  
/* ---------------------------------------------------fe---------------------------------------------*/
function buscar_clientes_multiempresas(nivusu){
	if($.trim(document.getElementById("ruc_empresa_ing").value)==""){
	bootbox.alert("Debe ingresar obligatoriamente el ruc del emisor.");
	return false;
}
	$.ajax ({
	url:            'controller/multiempresa.php',								 
	type:   		'post',											
	data: 			'ruc_empresa='+document.getElementById("ruc_empresa_ing").value,		
	success:  function(request, settings)
	{
		if(request==0){
			bootbox.alert('RUC del emisor es incorrecto; porfavor vuelva a ingresar correctamente.');			
		}else{
			$('#ruc_empresa_ing').attr('readonly', true);
			buscar_clientes_ini(nivusu);
		}
	
	}
			
        });

}
	function buscar_clientes(nivusu){
if($.trim(document.getElementById("ruc_reg").value)!="" && $.trim(document.getElementById("nombres_reg").value)==""){
$('#bgtransparent').show();
$('#cargador').show();
	$.ajax ({
	url:                    'controller/usuarios.php',
	type:   		'post',	
	data: 			'tipo=buscar&ruc='+document.getElementById("ruc_reg").value,
	success:  function(request, settings)
	{
		$('#bgtransparent').hide();
$('#cargador').hide();	
       if(request["nomcli"]=="ERROR"){
			jAlert(request["emailfe2"], "Aviso");
		}else if(request==0){
			jAlert("El usuario no existe o ya se encuentra registrado",'Aviso');
			document.getElementById("nombres_reg").value="";
			}else{
             $('#ruc_reg').attr('readonly', true);
			document.getElementById("nombres_reg").value=request["nomcli"];
			if(nivusu==1){
			document.getElementById("email_reg").value=request["emailfe2"];
			}
	
				}
						 
	}
});
}
	}
	function buscar_clientes_ini(nivusu){
if($.trim(document.getElementById("ruc_reg").value)!="" && $.trim(document.getElementById("nombres_reg").value)==""){
$('#bgtransparent').show();
$('#cargador').show();
	$.ajax ({
	url:                    'controller/usuarios.php',
	type:   		'post',	
	data: 			'tipo=buscar&ruc='+document.getElementById("ruc_reg").value,
	success:  function(request, settings)
	{
		$('#bgtransparent').hide();
$('#cargador').hide();	
       if(request["nomcli"]=="ERROR"){
			bootbox.alert(request["emailfe2"]);
		}else if(request==0){
			bootbox.alert("El usuario no existe o ya se encuentra registrado");
			document.getElementById("nombres_reg").value="";
			}else{
             $('#ruc_reg').attr('readonly', true);
			document.getElementById("nombres_reg").value=request["nomcli"];
			if(nivusu==1){
			document.getElementById("email_reg").value=request["emailfe2"];
			}
	
				}
						 
	}
});
}
	}
	function registrar_usuario(nivusu){
var formu = document.getElementById("formregistrar");
if(formu.ruc_reg.value.length==0){
	bootbox.alert("Debe ingresar el ruc");
	return false;
}
if(formu.nombres_reg.value.length==0){
	bootbox.alert("Debe ingresar el cliente");
	return false;
}
if(formu.email_reg.value.length==0){
	bootbox.alert("Debe ingresar el correo electr\u00f3nico");
	return false;
}
var correo=$.trim(formu.email_reg.value);
if( !(/^[^@\s]+@[^@\.\s]+(\.[^@\.\s]+)+$/.test(correo))){
	bootbox.alert("Debe ingresar un correo electr\u00f3nico v\u00e1lido.");
	return false;
}
if(formu.contrasena_reg.value.length==0){
	bootbox.alert("Debe ingresar la contrase\u00f1a");
	return false;
}
if(formu.contrasena_reg.value.length<5){
	bootbox.alert("La contrase\u00f1a debe tener m\u00ednimo 5 car\u00e1cteres");
	return false;
}
if(formu.recontrasena_reg.value.length==0){
	bootbox.alert("Debe repetir la contrase\u00f1a");
	return false;
}
if(formu.contrasena_reg.value != formu.recontrasena_reg.value){
	bootbox.alert("Las contrase\u00f1as no coinciden.");
	return false;
}
var re = /^[0-9-A-z]*$/;
if (!re.test(formu.contrasena_reg.value)) {
	bootbox.alert('La contrase\u00f1a debe ser alfanum\u00e9rica');
	return false;
}

bootbox.confirm({
    message: "¿Est\u00e1 seguro que desea agregar el cliente?",
    buttons: {
        confirm: {
            label: 'Si',
            className: 'btn-success'
        },
        cancel: {
            label: 'No',
            className: 'btn-danger'
        }
    },
    callback: function (result) {
       $('#bgtransparent').show();
$('#cargador').show();
$.ajax ({
	url:            'controller/usuarios.php',								 
	type:   		'post',											
	data: 			$('#formregistrar').serialize(),		
	success:  function(request, settings)
	{
		if(request=="ERROR"){
		bootbox.alert('Error de conexi\u00f3n.');
		}else if(request==0){
			$('#bgtransparent').hide();
			$('#cargador').hide();
			bootbox.alert('La cuenta de correo no coincide con la ingresada.');

			return false;
		}
		else{
			$('#exampleModalLong').modal('toggle');	
			$('#bgtransparent').hide();
			$('#cargador').hide();
			if(nivusu==1){
				var grid = $("#contenedorgrid").data("kendoGrid");
				 grid.dataSource.read();
			}
			bootbox.alert('El usuario fue registrado satisfactoriamente.');
		}
		
	
	}
});
    }
});

}
function registrar_usuario_log(nivusu){
var formu = document.getElementById("formregistrar");
if(formu.ruc_reg.value.length==0){
	jAlert("Debe ingresar el ruc", "Aviso");
	return false;
}
if(formu.nombres_reg.value.length==0){
	jAlert("Debe ingresar el cliente", "Aviso");
	return false;
}
if(formu.email_reg.value.length==0){
	jAlert("Debe ingresar el correo electr\u00f3nico", "Aviso");
	return false;
}
var correo=$.trim(formu.email_reg.value);
if( !(/^[^@\s]+@[^@\.\s]+(\.[^@\.\s]+)+$/.test(correo))){
	jAlert("Debe ingresar un correo electr\u00f3nico v\u00e1lido.", "Aviso");
	return false;
}
if(formu.contrasena_reg.value.length==0){
	jAlert("Debe ingresar la contrase\u00f1a", "Aviso");
	return false;
}
if(formu.contrasena_reg.value.length<5){
	jAlert("La contrase\u00f1a debe tener m\u00ednimo 5 car\u00e1cteres", "Aviso");
	return false;
}
if(formu.recontrasena_reg.value.length==0){
	jAlert("Debe repetir la contrase\u00f1a", "Aviso");
	return false;
}
if(formu.contrasena_reg.value != formu.recontrasena_reg.value){
	jAlert("Las contrase\u00f1as no coinciden.", "Aviso");
	return false;
}
var re = /^[0-9-A-z]*$/;
if (!re.test(formu.contrasena_reg.value)) {
	jAlert('La contrase\u00f1a debe ser alfanum\u00e9rica', "Aviso");
	return false;
}
	jConfirm("¿Est\u00e1 seguro que desea agregar el cliente?", "Atenci\u00f3n", function(r) {
	  
			if(r) {
$('#bgtransparent').show();
$('#cargador').show();
$.ajax ({
	url:            'controller/usuarios.php',								 
	type:   		'post',											
	data: 			$('#formregistrar').serialize(),		
	success:  function(request, settings)
	{
		if(request=="ERROR"){
		jAlert('Error de conexi\u00f3n.', "Aviso");
		}else if(request==0){
			$('#bgtransparent').hide();
			$('#cargador').hide();
			jAlert('La cuenta de correo no coincide con la ingresada.', "Aviso");
			return false;
		}
		else{
			cerrar_ventana('registrarse');
			$('#bgtransparent').hide();
			$('#cargador').hide();
			if(nivusu==1){
				var grid = $("#contenedorgrid").data("kendoGrid");
				 grid.dataSource.read();
			}
			jAlert('El usuario fue registrado satisfactoriamente.', "Aviso");
		}
		
	
	}
});
  }
			
        });
}
function validar_usuario_multiempresas(){
	if($.trim(document.getElementById("ruc_empresa").value)==""){
	bootbox.alert("Debe ingresar obligatoriamente el ruc del emisor.");
	return false;
}
	$.ajax ({
	url:            'controller/multiempresa.php',								 
	type:   		'post',											
	data: 			'ruc_empresa='+document.getElementById("ruc_empresa").value,		
	success:  function(request, settings)
	{
		if(request==0){
			bootbox.alert('RUC del emisor es incorrecto; porfavor vuelva a ingresar correctamente.');			
		}else{
			$('#ruc_empresa').attr('readonly', true);
			validar_usuario();
		}
	
	}
			
        });
}
function validar_usuario(){
var formu = document.getElementById("forminicio");
if(formu.user.value.length==0){
	bootbox.alert("Debe ingresar el correo electr\u00f3nico");
	return false;
}
if(formu.pass.value.length==0){
	bootbox.alert("Debe ingresar la contrase\u00f1a");
	return false;
}
$('#bgtransparent').show();
$('#cargador').show();
$.ajax ({
	url:            'controller/usuarios.php',								 
	type:   		'post',											
	data: 			$('#forminicio').serialize(),		
	success:  function(request, settings)
	{
		if(request["error"]=="ERROR"){
			$('#bgtransparent').hide();
			$('#cargador').hide();
			bootbox.alert(request["desc_error"]);
		}else if(request==2){
			$('#bgtransparent').hide();
			$('#cargador').hide();
			bootbox.alert('El usuario es incorrecto.');
			return false;
		}else if(request==3){
			$('#bgtransparent').hide();
			$('#cargador').hide();
			bootbox.alert('El c\u00f3digo capcha es incorrecto.');
			return false;
		}else if(request==4){
			location.href="cambiar_clave.php";
		}
		else{
			location.href="inicio.php"
		}
  }
			
        });
}
function pad (str, max) {
  str = str.toString();
  return str.length < max ? pad("0" + str, max) : str;
}
function buscar_factura_anonima_multiempresas(){
	if($.trim(document.getElementById("ruc_empresa").value)==""){
	bootbox.alert("Debe ingresar obligatoriamente el RUC del emisor.");

	return false;
}
	$.ajax ({
	url:            'controller/multiempresa.php',								 
	type:   		'post',											
	data: 			'ruc_empresa='+document.getElementById("ruc_empresa").value,		
	success:  function(request, settings)
	{
		if(request==0){
			bootbox.alert("RUC del emisor es incorrecto; porfavor vuelva a ingresar correctamente.");
		}else{
			$('#ruc_empresa').attr('readonly', true);
			buscar_factura_anonima();
		}
	
	}
			
        });
}
function buscar_factura_anonima(){
	document.getElementById("ndocu_fac").value=pad(document.getElementById("ndocu_fac").value, 7);
	$('#bgtransparent').show();
    $('#cargador').show();
	$.ajax ({
	url:            'controller/facturas.php',								 
	type:   		'post',											
	data: 			$('#forminicio').serialize(),		
	success:  function(request, settings)
	{
	$('#bgtransparent').hide();
    $('#cargador').hide();	
	if(request["error"]=="ERROR"){
		bootbox.alert(request["desc_error"]);
	}else if(request!=0){
		bootbox.alert({
    message: 'Seleccione su descarga:<br><div class="mt-2 text-center"><button type="button" class="btn btn-danger" id="btnpdf" name="btnpdf" style="width:120px;"  onclick="generar_factura_anonima(\'.pdf\');"><i class="fa fa-file-pdf-o"></i> PDF</button> <button type="button" class="btn btn-light" id="btnxml" name="btnxml" style="width:120px;"  onclick="generar_factura_anonima(\'.xml\');"><i class="fa fa-file-code-o"></i> XML</button></div>',
    buttons: {
      ok: {
        label: 'Cerrar'
      }
    }
});


	/*$('#btn_pdf').show();
    $('#btn_xml').show();
	$('#cdocu_fac').attr('disabled', true);
	$('#serie_fac').attr('disabled', true);
	$('#ndocu_fac').attr('disabled', true);
	$('#monto_fac').attr('disabled', true);
	var datepicker = $("#fecha_fac").data("kendoDatePicker");
    datepicker.enable(false);*/
	document.getElementById("docgen_fac").value=request;
	}else{
		bootbox.alert('El documento no existe.');
	}
	}
	
			
        });
}
function buscar_nuevo_login(){
	document.getElementById("user").value="";
	document.getElementById("pass").value="";
	document.getElementById("captcha").value="";
	recargar_captcha();
	$("#user").focus();
}
function buscar_nuevo_factura_anonima(){
	
	/*$('#btn_pdf').hide();
    $('#btn_xml').hide();*/
	$('#cdocu_fac').attr('disabled', false);
	$('#ndocu_fac').attr('disabled', false);
	$('#monto_fac').attr('disabled', false);
	$('#serie_fac').attr('disabled', false);
	var datepicker = $("#fecha_fac").data("kendoDatePicker");
    datepicker.enable(true);
	document.getElementById("cdocu_fac").value="01";
	document.getElementById("serie_fac").value="F";
	$("#serie_fac").focus();
	document.getElementById("ndocu_fac").value="";
	document.getElementById("monto_fac").value="";
	document.getElementById("docgen_fac").value="";
}
function generar_factura_anonima(ext){
	$.ajax ({
	url:            'controller/variables_sesion.php',								 
	type:   		'post',											
	data: 			'type=3&documento='+document.getElementById("docgen_fac").value+ext,		
	success:  function(request, settings)
	{
location.href="controller/generar_archivo.php";
		}
			
        });
	
}
function restaFechas(f1,f2)
 {
 var aFecha1 = f1.split('/'); 
 var aFecha2 = f2.split('/'); 
 var fFecha1 = Date.UTC(aFecha1[2],aFecha1[1]-1,aFecha1[0]); 
 var fFecha2 = Date.UTC(aFecha2[2],aFecha2[1]-1,aFecha2[0]); 
 var dif = fFecha2 - fFecha1;
 var dias = Math.floor(dif / (1000 * 60 * 60 * 24)); 
 return dias;
 }
function buscar_facturas(){
	var p2 = document.getElementById("p2").value;
    var p3=document.getElementById("p3").value;
	resta=restaFechas(p2,p3);
	if(resta>31){
		jAlert("El periodo m\u00e1ximo entre fechas es de 31 dias", "Aviso");
	return false;
	}
	$.ajax ({
	url:            'controller/variables_sesion.php',								 
	type:   		'post',											
	data: 			$('#frmfacturas').serialize(),		
	success:  function(request, settings)
	{
		
var gridprod = $("#contenedorgrid").data("kendoGrid");
gridprod.dataSource.data([]);
gridprod.dataSource.read();
gridprod.dataSource.page(1);	
		}
			
        });
}
function buscar_usuarios(){
	$.ajax ({
	url:            'controller/variables_sesion.php',								 
	type:   		'post',											
	data: 			$('#frmusuarios').serialize(),		
	success:  function(request, settings)
	{
var gridprod = $("#contenedorgrid").data("kendoGrid");
gridprod.dataSource.data([]);
gridprod.dataSource.read();
gridprod.dataSource.page(1);	
		}
			
        });
}
function eliminar_grupo(){
if(document.getElementById("idusuario").value==""){
	jAlert("Debe seleccionar un usuario", "Aviso");
	return false;
}
		jConfirm("¿Est\u00e1 seguro de dar de baja al usuario?", "Atenci\u00f3n", function(r) {	  
			if(r) {
				$.ajax ({
	url:            'controller/usuarios.php',								 
	type:   		'post',											
	data: 			'tipo=eliminar&idusuario='+document.getElementById("idusuario").value,		
	success:  function(request, settings)
	{
		/*jAlert(request, "Aviso");*/
		var grid = $("#contenedorgrid").data("kendoGrid");
				 grid.dataSource.read();
				 document.getElementById("idusuario").value=="";
		 }
				 });
				 }
				 });
}
function ver_detalle_acceso(){
	if(document.getElementById("idusuario").value==""){
	jAlert("Debe seleccionar un usuario", "Aviso");
	return false;
}usuario
	generar_popup('ver_detalle',900,300,'view/ver_detalle.php?idusuario='+document.getElementById("idusuario").value+'&usuario='+document.getElementById("usuariomail").value)
}
function recargar_captcha(){
obj=document.getElementById("imgcap");
if (!obj) obj=window.document.all.imgcap;
	if (obj){
		obj.src="captcha.php";
	}
}
function generar_documento(documento,docfe){
	 if(docfe==0){
		jAlert('El documento no existe.','Aviso');
		return false;
		}
	$.ajax ({
	url:            'controller/variables_sesion.php',								 
	type:   		'post',											
	data: 			'type=3&documento='+documento,		
	success:  function(request, settings)
	{
location.href="controller/generar_archivo.php";
		}
			
        });
}
 function cambiar_clave(){
						 if(document.getElementById("pass").value==""){
		jAlert('Debe ingresar la contrase\u00f1a.','Aviso');
		return false;
		}
		if(document.getElementById("pass").value.length<5){
		jAlert('La contrase\u00f1a debe tener al menos 5 car\u00e1cteres.','Aviso');
		return false;
		}
		if(document.getElementById("repass").value==""){
		jAlert('Debe repetir la contrase\u00f1a.','Aviso');
		return false;
		}
		if(document.getElementById("user").value==document.getElementById("pass").value){
		jAlert('Las contrase\u00f1ano debe ser igual al RUC.','Aviso');
		return false;
		}
		if(document.getElementById("repass").value!=document.getElementById("pass").value){
		jAlert('Las contrase\u00f1as debenser iguales.','Aviso');
		return false;
		}
		$('#bgtransparent').show();
		$('#cargador').show();
		$.ajax ({
				
            url:                    "controller/usuarios.php",								 
            type:                   'post',											
            data:                   $('#formusuario').serialize(),		

            

            success:  function(request, settings)
            {
            if(request==2){
					$('#bgtransparent').hide();
		            $('#cargador').hide();
				jAlert("La nueva contrase\u00f1a debe ser diferente a la anterior",'Aviso');
				}
				else{
					$('#bgtransparent').hide();
		            $('#cargador').hide();
					location.href="inicio.php";
					}
     
            }
			
        });
						 }
						 
						 