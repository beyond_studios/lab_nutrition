<?php

class Mage_Catalog_Block_Product_Promotions extends Mage_Catalog_Block_Product_Abstract
{
    protected $category_url;

    public function getProductCollection()
    {
        $limit = $this->getLimit();
    	$_category = Mage::getModel('catalog/category')->load($this->getCategoryId());

        $order = "position";
        $dir = "ASC";

        if ($_category) {
            $_productCollection = $_category->getProductCollection();

            $this->category_url = $_category->getUrl();
        }
        else {
            $_category = Mage::getModel('catalog/category')->load(11);
            $_productCollection = $_category->getProductCollection();

            $this->category_url = $_category->getUrl();
        }
        $_productCollection
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('status',array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
            ->addStoreFilter(Mage::app()->getStore()->getId())
            ->setOrder($order, $dir);
        $_productCollection->getSelect()->limit($limit);

        return $_productCollection;
    }

    public function getCategoryUrl()
    {
        return $this->category_url;
    }

}