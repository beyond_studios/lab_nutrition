<?php

class Mage_Catalog_Block_Product_Bestseller extends Mage_Catalog_Block_Product_Abstract
{
    protected $category_url;

    public function getProductCollection()
    {
        $limit = $this->getLimit();
    	if ($this->getCategoryId()) {
            $_category = Mage::getModel('catalog/category')->load($this->getCategoryId());
            $_productCollection = $_category->getProductCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('status',array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
            ->addAttributeToFilter('visibility',array('eq' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH))
            ->addStoreFilter(Mage::app()->getStore()->getId());
            $_productCollection->getSelect()->order('position','desc');

            $this->category_url = $_category->getUrl();
        }
        else
        {
            $bestseller = 1;
            $_productCollection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('prod_bestseller')
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('prod_bestseller', array('like' => $bestseller))
            ->addAttributeToFilter('status',array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
            ->addAttributeToFilter('visibility',array('eq' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH));
            $sortBySeason = "ISNULL(bestseller_order), CAST(`bestseller_order` AS SIGNED) ASC";
            $_productCollection->addAttributeToSort('bestseller_order', 'ASC')
                     ->getSelect()
                     ->reset('order')
                     ->order(new Zend_Db_Expr($sortBySeason));

            $this->category_url = Mage::getModel("catalog/category")->load(7)->getUrl();
        }
        $_productCollection->getSelect()->limit($limit);

        return $_productCollection;
    }

    public function getCategoryUrl()
    {
        return $this->category_url;
    }
}