<?php


$installer = $this;
$installer->startSetup();

$installer->run("
    ALTER TABLE  {$this->getTable('storelocator')} 
    ADD COLUMN `almacen_id_navasoft`  VARCHAR(4) NULL DEFAULT '000' AFTER `almacen_id`
    ");

$installer->endSetup();