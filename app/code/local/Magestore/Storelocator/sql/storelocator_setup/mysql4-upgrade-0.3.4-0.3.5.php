<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection()
->addColumn($installer->getTable('storelocator/storelocator'),'contact_person', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
    'nullable'  => true,
    'length'    => 255,
    'comment'   => 'Nombre Contacto'
    ));
$installer->endSetup();
