Store pickup

<?php

$installer = $this;
$installer->startSetup();
$installer->getConnection()
->addColumn($installer->getTable('storelocator/storelocator'),'store_pickup', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
    'nullable'  => false,
    'comment'   => 'Recogo en Tienda',
    'default'   => '0',
    ));   
$installer->endSetup();