<?php

$installer = $this;
$installer->startSetup();
$installer->getConnection()
->addColumn($installer->getTable('storelocator/storelocator'),'almacen_id', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
    'nullable'  => true,
    'comment'   => 'ID Almacen PIM',
    'default'   => null,
    ));
$installer->endSetup();
