<?php

$installer = $this;
$installer->startSetup();
$installer->getConnection()
->addColumn($installer->getTable('storelocator/storelocator'),'upon_delivery', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
    'nullable'  => false,
    'comment'   => 'Delivery - Contra Entrega',
    'default'   => '0',
    ));
$installer->getConnection()
->addColumn($installer->getTable('storelocator/storelocator'),'radio_delivery', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_FLOAT,
    'nullable'  => true,
    'comment'   => 'Radio - Contra Entrega',
    'default'   => 0,
    ));
$installer->endSetup();
