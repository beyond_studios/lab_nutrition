<?php   
class Forms_LibroReclamaciones_Block_Frontend_Form extends Mage_Core_Block_Template{   

    protected $_form;
    
    public function __construct() {
        parent::__construct();
                
        $this->_form = new Zend_Form();
    }
    
    public function getForm() {

        $params = $this->getRequest()->getParams();

        $departamentos = Mage::helper('provincedropdown')->getRegionPerArray();
        $_depts = array();
        foreach ($departamentos as $key => $value) {
            $_depts[$value['value']] = $value['label'];
        }
        
        $districts = array();
        if ($params && isset($params['departamento_cliente']) && $params['departamento_cliente'] != "") {
            $districts = Mage::helper('provincedropdown')->getCitiesAsDropdown($params['departamento_cliente']);
        }
        
        $this->_form->addElement(
            'text',
            'nombre_cliente',
            array(
                'validators' => array(
                    array(
                        'notEmpty', true,
                        array(
                            'messages' => array(
                                Zend_Validate_NotEmpty::IS_EMPTY =>'Debes ingresar un Nombre.'
                            )
                        )
                    )
                ),
                'required' => true
            )
        );

        $this->_form->addElement(
            'text',
            'apellidos_cliente'
        );
        
        $this->_form->addElement(
            'text',
            'domicilio_cliente',
            array(
                'validators' => array(
                    array(
                        'notEmpty', true,
                        array(
                            'messages' => array(
                                Zend_Validate_NotEmpty::IS_EMPTY =>'Debes ingresar un Domicilio.'
                            )
                        )
                    )
                ),
                'required' => true
            )
        );
        
        
        $distrito = array(''=>'-- Por favor seleccione --');
        $distrito = $distrito + $districts;

        $this->_form->addElement(
            'select',
            'departamento_cliente',
            array(
                'multiOptions' => $_depts,
                'separator' => ''
            )
        );
        
        $this->_form->addElement(
            'select',
            'distrito_cliente',
            array(
                'multiOptions' => $distrito,
                'separator' => ''
            )
        );
        
        $this->_form->addElement(
            'text',
            'dni_cliente'
        );
        
        $this->_form->addElement(
            'text',
            'telefono_cliente',
            array(
                'validators' => array(
                    array(
                        'notEmpty', true,
                        array(
                            'messages' => array(
                                Zend_Validate_NotEmpty::IS_EMPTY =>'Debes ingresar un Telefono.'
                            )
                        )
                    )
                ),
                'required' => true
            )
        );
        
        $this->_form->addElement(
            'text',
            'email_cliente',
            array(
                'filters' => array('StringToLower'),
                'validators' => array(
                    array(
                        'notEmpty', true,
                        array(
                            'messages' => array(
                                Zend_Validate_NotEmpty::IS_EMPTY =>'Debes ingresar un Email.'
                            )
                        )
                    ),
                    array(
                        'EmailAddress', true,
                        array(
                            'messages' => array(
                                Zend_Validate_EmailAddress::INVALID_FORMAT => 'Debe ingresar un email valido',
                                Zend_Validate_EmailAddress::INVALID_HOSTNAME => 'Hostname no válido'
                            )
                        )
                    ),
                ),
                'required' => true
            )
        );
        
        $this->_form->addElement(
            'text',
            'padremadre_cliente'
        );        
        
        
        $this->_form->addElement(
            'radio', 
            'tiposervicio', 
            array(
                'multiOptions' => array(
                    'producto' => 'Producto',
                    'servicio' => 'Servicio'
                ),
                'separator' => '',
                'validators' => array(
                    array(
                        'notEmpty',
                        true,
                        array(
                            'messages' => array(
                                Zend_Validate_NotEmpty::IS_EMPTY => 'Debes seleccionar una Opción'
                            )
                        )
                    )
                ),
                'required' => true
            )
       );
        
       $this->_form->addElement(
            'radio', 
            'tipo_reclamo', 
            array(
                'multiOptions' => array(
                    'reclamo' => 'Reclamo',
                    'queja' => 'Queja'
                ),
                'separator' => '',
                'validators' => array(
                    array(
                        'notEmpty',
                        true,
                        array(
                            'messages' => array(
                                Zend_Validate_NotEmpty::IS_EMPTY => 'Debes seleccionar una Opción'
                            )
                        )
                    )
                ),
                'required' => true
            )
       );
       
       $this->_form->addElement(
            'textarea', 
            'descripcion_tiposervicio', 
            array(
                'required' => false,
            )
       );
       
       $this->_form->addElement(
            'textarea', 
            'descripcion_reclamo', 
            array(
                'required' => false,
            )
       );
       
       $this->_form->addElement(
            'textarea', 
            'acciones_proveedor', 
            array(
                'required' => false,
            )
       );

       $this->_form->addElement(
            'text', 
            'date_create', 
            array(
                'validators' => array(
                    array(
                        'notEmpty', true,
                        array(
                            'messages' => array(
                                Zend_Validate_NotEmpty::IS_EMPTY =>'Debes ingresar un una fecha.'
                            )
                        )
                    ),
                    array(
                        'Date',true,
                        array(
                            'messages' => array(
                                Zend_Validate_Date::INVALID =>'Debes ingresar un una fecha válida.'
                            )
                        )
                    )
                ),
                'required' => true
            )
       );
        
        return $this->_form;
    }
    
    
    
    public function setForm($frm) {
        $this->_form = $frm;
    }
    
    public function hasError($element, $errors) {
        if (isset($errors[$element])) {
            return true;
        } else {
            return false;
        }
    }
    
    public function renderElementErrors($element, $errors, $options=array()) {
        $lista = "<ul";
        foreach ($options as $index=>$value) {
            $lista.=" ".$index."='".$value."'";
        }       
        $lista .= ">";
        
        if ($this->hasError($element, $errors)) {            
            foreach ($errors[$element] as $index=>$value) {
                $lista.="<li>".$value."</li>";
                break;
            }        
        }        
        $lista .= "</ul>";
        return $lista;
    }
    
    public function renderSelect($element, $options=array(), $val="") {
        $lista = "<select name='".$element."' ";
        foreach ($options as $index=>$value) {
            $lista.=" ".$index."='".$value."'";
        }       
        $lista .= ">";
        
        $options = $this->_form->getElement($element)->getMultiOptions();
        
        foreach ($options as $index=>$value) {
            $sel = "";
            if ($index==$val) {
                $sel="selected='selected'";
            }
            $lista.="<option value='".$index."' ".$sel.">".$value."</option>";
        }
        
        $lista .= "</select>";
        return $lista;
    }
    

}