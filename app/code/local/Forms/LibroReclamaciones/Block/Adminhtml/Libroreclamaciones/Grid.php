<?php

class Forms_LibroReclamaciones_Block_Adminhtml_Libroreclamaciones_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct()
    {
        parent::__construct();
        
        $this->setId('libroreclamacionesGrid');
        $this->setDefaultSort('libroreclamaciones_id');
        $this->setDefaultDir('desc');
        $this->setSaveParametersInSession(true);  
        $this->setFilterVisibility(false);        
    }
 
    protected function _getCollectionClass()
    {
        return 'forms/libroreclamaciones';
    }
    
    protected function _prepareCollection()
    {
        $filter   = $this->getParam($this->getVarNameFilter(), null);
        
        $collection = Mage::getModel($this->_getCollectionClass())->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
 
    protected function _addColumnFilterToCollection($column)
    {
        if ($this->getCollection()) {
            $field = ( $column->getFilterIndex() ) ? $column->getFilterIndex() : $column->getIndex();
            if ($column->getFilterConditionCallback()) {
                call_user_func($column->getFilterConditionCallback(), $this->getCollection(), $column);
            } else {
                $cond = $column->getFilter()->getCondition();
                if ($field && isset($cond)) {
                    $this->getCollection()->addFieldToFilter($field , $cond);
                }
            }
        }
        return $this;
    }
    
    protected function _prepareColumns()
    {
        
        $this->addColumn('libroreclamaciones_id', array(
            'header'    => Mage::helper('libroreclamaciones')->__('ID'),
            'align'     =>'right',
            'width'     => '50px',
            'index'     => 'libroreclamaciones_id',
            'filter'    => false,
        ));
        
        $this->addColumn('codigo_reclamo', array(
            'header'    => Mage::helper('libroreclamaciones')->__('Codigo'),
            'align'     =>'left',
            'index'     => 'codigo_reclamo',
            'filter'    => false,
        ));
 
        $this->addColumn('fecha_registro', array(
            'header'    => Mage::helper('libroreclamaciones')->__('Fecha Registro'),
            'align'     =>'left',
            'index'     => 'fecha_registro',
            'type'      => 'date',
            'format'    => Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM),
            'filter'    => false,
        ));

        $this->addColumn('date_create', array(
            'header'    => Mage::helper('libroreclamaciones')->__('Fecha Reclamo'),
            'align'     =>'left',
            'index'     => 'date_create',
            'type'      => 'date',
            'format'    => Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM),
            'filter'    => false,
        ));
        
        $this->addColumn('nombre_cliente', array(
            'header'    => Mage::helper('libroreclamaciones')->__('Nombre'),
            'align'     =>'left',
            'index'     => 'nombre_cliente',
            'filter'    => false,
        ));
 
        $this->addColumn('distrito_cliente', array(
            'header'    => Mage::helper('libroreclamaciones')->__('Distrito'),
            'align'     =>'left',
            'index'     => 'distrito_cliente',
            'filter'    => false,
        ));
        
        $this->addColumn('domicilio_cliente', array(
            'header'    => Mage::helper('libroreclamaciones')->__('Domicilio'),
            'align'     =>'left',
            'index'     => 'domicilio_cliente',
            'filter'    => false,
        ));
 
        $this->addColumn('dni_cliente', array(
            'header'    => Mage::helper('libroreclamaciones')->__('Dni'),
            'align'     => 'left',
            'index'     => 'dni_cliente',
            'filter'    => false,
        ));
        
        $this->addColumn('telefono_cliente', array(
            'header'    => Mage::helper('libroreclamaciones')->__('Telefono'),
            'align'     => 'left',
            'index'     => 'telefono_cliente',
            'filter'    => false,
        ));
        
        $this->addColumn('email_cliente', array(
            'header'    => Mage::helper('libroreclamaciones')->__('Email'),
            'align'     => 'left',
            'index'     => 'email_cliente',
            'filter'    => false,
        ));
        
        $this->addColumn('tiposervicio', array(
            'header'    => Mage::helper('libroreclamaciones')->__('Servicio'),
            'align'     => 'left',
            'index'     => 'tiposervicio',
            'filter'    => false,
        ));
        
        $this->addColumn('tipo_reclamo', array(
            'header'    => Mage::helper('libroreclamaciones')->__('Concepto'),
            'align'     => 'left',
            'index'     => 'tipo_reclamo',
            'filter'    => false,
        ));

        
        
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('libroreclamaciones')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('libroreclamaciones')->__('Ver'),
                        'url'       => array('base'=> '*/*/ver'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
 
        $this->addExportType('*/*/exportCsv', Mage::helper('libroreclamaciones')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('libroreclamaciones')->__('Excel XML'));
        
        return parent::_prepareColumns();
    }
 
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/ver', array('id' => $row->getId()));
    }
    
    public function getGridUrl()
    {
      return $this->getUrl('*/*/', array('_current'=>true));
    }
    
    
    

}
