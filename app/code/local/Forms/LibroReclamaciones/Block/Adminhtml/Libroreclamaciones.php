<?php

class Forms_LibroReclamaciones_Block_Adminhtml_Libroreclamaciones extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {        
        parent::__construct();
        $this->_controller = 'adminhtml_libroreclamaciones';
        $this->_blockGroup = 'libroreclamaciones';
        $this->_headerText = Mage::helper('libroreclamaciones')->__('Libro de Reclamaciones');
        //$this->_addButtonLabel = Mage::helper('libroreclamaciones')->__('Agregar Item');
        $this->_removeButton("add");
        
    }

    protected function _prepareLayout() { 
        $this->setChild('grid', $this->getLayout()
                ->createBlock($this->_blockGroup . '/' . $this->_controller . '_grid', $this->_controller . '.grid')->setSaveParametersInSession(true));
        return parent::_prepareLayout();
    }

}
