<?php
$installer = $this;
$installer->startSetup();
$prefix = Mage::getConfig()->getTablePrefix();
$table=$prefix."libroreclamaciones";

$sql=<<<SQLTEXT
    ALTER TABLE `$table` ADD COLUMN `distrito_cliente` VARCHAR(200) CHARACTER SET utf8 DEFAULT NULL
SQLTEXT;

$installer->run($sql);

$installer->endSetup();