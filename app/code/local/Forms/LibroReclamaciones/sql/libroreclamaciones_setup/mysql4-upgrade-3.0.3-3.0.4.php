

<?php 


$installer = $this;
$installer->startSetup();
$installer->run("
 
ALTER TABLE `{$this->getTable('libroreclamaciones')}` ADD `departamento_cliente` varchar(50);
ALTER TABLE `{$this->getTable('libroreclamaciones')}` ADD `apellidos_cliente` varchar(255);
");

$installer->endSetup();