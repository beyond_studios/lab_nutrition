<?php

$nautoincrement = "1282";

$installer = $this;
$installer->startSetup();
$prefix = Mage::getConfig()->getTablePrefix();
$table=$prefix."libroreclamaciones";

$sql=<<<SQLTEXT
create table $table(
    libroreclamaciones_id int not null auto_increment, 
    codigo_reclamo varchar(255), 
    fecha_registro datetime, 
    nombre_cliente varchar(255), 
    domicilio_cliente varchar(255), 
    dni_cliente varchar(12), 
    telefono_cliente varchar(30), 
    email_cliente varchar(255), 
    padremadre_cliente varchar(255), 
    tiposervicio ENUM('producto','servicio'),
    descripcion_tiposervicio text,
    tipo_reclamo ENUM('reclamo','queja'),
    descripcion_reclamo text,
    acciones_proveedor text,
    primary key(libroreclamaciones_id));
SQLTEXT;

$installer->run($sql);

$sql=<<<SQLTEXT
    ALTER TABLE `$table` AUTO_INCREMENT = $nautoincrement
SQLTEXT;
$installer->run($sql);



// Estableciendo Plantilla de email 
$configValuesMap = array(
    'forms/libroreclamaciones_email/template' => 'libroreclamaciones_email_email_template',
    'forms/libroreclamaciones_emailadmin/template' => 'libroreclamaciones_emailadmin_email_template'
);

foreach ($configValuesMap as $configPath=>$configValue) {
    $installer->setConfigData($configPath, $configValue);
}



$installer->endSetup();
