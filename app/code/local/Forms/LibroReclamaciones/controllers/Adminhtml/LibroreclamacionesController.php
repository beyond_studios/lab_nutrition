<?php
class Forms_LibroReclamaciones_Adminhtml_LibroreclamacionesController extends Mage_Adminhtml_Controller_Action
{
    
    protected function _initAction() {
        $this->loadLayout()->_setActiveMenu('customer/libroreclamaciones');
        return $this;
    }   

    public function indexAction()
    {
        ini_set("display_errors", 1);       
        
       $this->_initAction();
       $this->_title($this->__("Libro de Reclamaciones"));       
       $this->_addContent($this->getLayout()->createBlock('libroreclamaciones/adminhtml_libroreclamaciones'));
       $this->renderLayout();
    }
    
    public function verAction() {
        
        $this->_initAction();
        
        $id     = $this->getRequest()->getParam('id');
        $model  = Mage::getModel('forms/libroreclamaciones')->load($id);
        
        $this->_addContent($this->getLayout()->createBlock('libroreclamaciones/adminhtml_verreclamacion')
                                             ->setTemplate('libroreclamaciones/ver.phtml')
                                             ->setData("objReclamo", $model));
        $this->renderLayout();
    }
    
    /*public function exportCsvAction() {

        ini_set("display_errors", 1);  
        $fileName = $this->_getExportFileName('csv');

        $content = $this->getLayout()->createBlock('libroreclamaciones/adminhtml_libroreclamaciones_grid')
                ->getCsvFile();

        $this->_prepareDownloadResponse($fileName, $content);
    }*/
    
    public function exportCsvAction() {
        $fileName   = 'libroreclamaciones-'.date("d-m-Y-His").'.csv';          
        $grid  = $this->getLayout()->createBlock('libroreclamaciones/adminhtml_libroreclamaciones_grid');  

        $grid->addColumnAfter('padremadre_cliente', 
                array(
                    'header'    => Mage::helper('customer')->__('Padre o Madre'),
                    'width'     => '200',
                    'index'     => 'padremadre_cliente' ,
                    'filter'    => false,
                ), 'tipo_reclamo');

        $grid->addColumnAfter('descripcion_tiposervicio', 
                array(
                    'header'    => Mage::helper('customer')->__('Descripción Servicio'),
                    'width'     => '200',
                    'index'     => 'descripcion_tiposervicio' ,
                    'filter'    => false,
                ), 'padremadre_cliente');
        
        $grid->addColumnAfter('descripcion_reclamo', 
                array(
                    'header'    => Mage::helper('customer')->__('Descripción Concepto'),
                    'width'     => '200',
                    'index'     => 'descripcion_reclamo',
                    'filter'    => false,
                ), 'descripcion_tiposervicio'); 

        $grid->addColumnAfter('acciones_proveedor', 
                array(
                    'header'    => Mage::helper('customer')->__('Acciones del Proveedor'),
                    'width'     => '200',
                    'index'     => 'acciones_proveedor',
                    'filter'    => false,
                ), 'descripcion_reclamo');   
        
        
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());        
    }

    public function saveAction(){

        if ($data = $this->getRequest()->getPost()) {

            try {

                $model = Mage::getModel("forms/libroreclamaciones")->load($data['id_libroreclamaciones']);
                $date_ = str_replace("/", "-", $data["date_create"]);
                $data["date_create_new"] = date("Y-m-d H:i:s", strtotime($date_));
                $model->setDateCreate($data["date_create_new"]);
                $model->save();
            } catch (Exception $e) {
                Mage::getSingleton('core/session')->addError("Error al registrar". $e->getMessage());
            }
            Mage::getSingleton('core/session')->addSuccess("registro actualizado");
            
        }

        $this->_redirect('*/*/index');
    }
    
    

    /**
     * Export stylist grid to XML format
     */
    /*public function exportXmlAction() {

        $fileName = $this->_getExportFileName('xml');

        $content = $this->getLayout()->createBlock('libroreclamaciones/adminhtml_libroreclamaciones_grid')
                ->getExcelFile();

        $this->_prepareDownloadResponse($fileName, $content);
    }*/
    
    
    public function exportXmlAction() {
        $fileName   = 'libroreclamaciones-'.date("d-m-Y-His").'.xml';          
        $grid  = $this->getLayout()->createBlock('libroreclamaciones/adminhtml_libroreclamaciones_grid');  

        $grid->addColumnAfter('padremadre_cliente', 
                array(
                    'header'    => Mage::helper('customer')->__('Padre o Madre'),
                    'width'     => '200',
                    'index'     => 'padremadre_cliente' ,
                    'filter'    => false,
                ), 'tipo_reclamo');

        $grid->addColumnAfter('descripcion_tiposervicio', 
                array(
                    'header'    => Mage::helper('customer')->__('Descripción Servicio'),
                    'width'     => '200',
                    'index'     => 'descripcion_tiposervicio' ,
                    'filter'    => false,
                ), 'padremadre_cliente');
        
        $grid->addColumnAfter('descripcion_reclamo', 
                array(
                    'header'    => Mage::helper('customer')->__('Descripción Concepto'),
                    'width'     => '200',
                    'index'     => 'descripcion_reclamo',
                    'filter'    => false,
                ), 'descripcion_tiposervicio'); 

        $grid->addColumnAfter('acciones_proveedor', 
                array(
                    'header'    => Mage::helper('customer')->__('Acciones del Proveedor'),
                    'width'     => '200',
                    'index'     => 'acciones_proveedor',
                    'filter'    => false,
                ), 'descripcion_reclamo');   
        
        
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile());        
    }
    
    
    
    private function _getExportFileName($extension='csv') {
        $name = 'libro_reclamaciones';
        return $name . '.' . $extension;
    }
    /**
     * Check if admin has permissions to visit related pages
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('customer/libroreclamaciones');
    }
    
    
}

    