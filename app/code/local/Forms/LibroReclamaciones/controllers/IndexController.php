<?php
class Forms_LibroReclamaciones_IndexController extends Mage_Core_Controller_Front_Action {

    const EMAIL_TEMPLATE_XML_PATH = 'forms/libroreclamacionesemail_email/template';
    
    const XML_PATH_EMAIL_RECIPIENT  = 'libroreclamaciones/email/recipient_email';
    const XML_PATH_EMAIL_SENDER     = 'libroreclamaciones/email/sender_email_identity';
    const XML_PATH_EMAIL_TEMPLATE   = 'libroreclamaciones/email/email_template';
    const XML_PATH_EMAIL_TEMPLATE_ADMIN   = 'libroreclamaciones/email/emailadmin_template';
    const XML_PATH_ENABLED          = 'libroreclamaciones/libroreclamaciones/enabled';
    
    public function preDispatch()
    {
        parent::preDispatch();

        if( !Mage::getStoreConfigFlag(self::XML_PATH_ENABLED) ) {
            $this->norouteAction();
        }
    }

    public function indexAction()
    {
        ini_set("display_errors", 1);
        $now = Mage::getModel('core/date')->timestamp(time());
        
        $this->loadLayout();  
        
        $params = $this->getRequest()->getParams();
        
        $formBlock = $this->getLayout()->createBlock("libroreclamaciones/frontend_form")->setTemplate("libroreclamaciones/form.phtml");
        
        $form = $formBlock->getForm();
        
        if ($this->getRequest()->isPost()) {
            if ($form->isValid($params)) {
                
                // Guardamos 
                $data = $form->getValues();
                $date_ = str_replace("/", "-", $data["date_create"]);
                $data["date_create"] = date("Y-m-d H:i:s", strtotime($date_));
                $data["fecha_registro"] = date("Y-m-d H:i:s", $now);

                $data["codigo_reclamo"] = "";                
                $mLibroReclamaciones = Mage::getModel("forms/libroreclamaciones")->setData($data);
                $insertId = $mLibroReclamaciones->save()->getId();

                //Actualizamos el Código de Reclamo
                //$numero = str_pad(strval($insertId), 8, "0", STR_PAD_LEFT);
                $numero = $insertId;
                //$codigo = "WEB-001-".substr(date("Y"), 1, strlen(date("Y"))-1)."-".$numero;
                $codigo = "WEB-001-".$numero;
                
                $data["codigo_reclamo"] = $codigo;
                $datachanged = array('codigo_reclamo'=>$codigo);
                
                $model = Mage::getModel('forms/libroreclamaciones')->load($insertId);
                $model->addData($datachanged);
                try {
                    $model->setId($insertId)->save();
                } catch (Exception $e){
                    echo $e->getMessage();
                }
                
                //---------------------------
                //Envio del Email al Cliente 
                //---------------------------                
                $email = $data["email_cliente"];
                $vars = Array('nombre'=> $data["nombre_cliente"]);
                
                $mailTemplate = Mage::getModel('core/email_template');                
                $mailTemplate->setDesignConfig(array('area' => 'frontend'))
                    ->setReplyTo($email)
                    ->sendTransactional(
                        Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE),
                        Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER),
                        $email,
                        null,
                        $data
                    );
                
                if (!$mailTemplate->getSentSuccess()) {
                    throw new Exception();
                }
                            
                // -------------------------------------
                // Envio de Email a los Administradores
                //---------------------------------------
                $recipients = explode(",",Mage::getStoreConfig(self::XML_PATH_EMAIL_RECIPIENT));
                $mailTemplate = Mage::getModel('core/email_template');
                
                
                foreach($recipients as $recipient){
                    $mailTemplate->setDesignConfig(array('area' => 'frontend'))
                    ->setReplyTo($email)
                    ->sendTransactional(
                        Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE_ADMIN),
                        Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER),
                        $recipient,
                        null,
                        $data
                    );
                    if (!$mailTemplate->getSentSuccess()) {
                        throw new Exception();
                    }
                }
                
                
                Mage::getSingleton('core/session')->addSuccess("Su reclamo ha sido registrado correctamente con el código: ".$codigo);
                $this->getResponse()->setRedirect(Mage::getUrl("libro-reclamaciones"));
            }
            
            $formBlock->setData("errors", $form->getMessages());
            $formBlock->setData("values", $form->getValues());            
        }
        $this->getLayout()->getBlock("libroreclamaciones.index")->setChild("libroreclamaciones.form", $formBlock);
        
         
	$this->getLayout()->getBlock("head")->setTitle($this->__("Libro de Reclamaciones"));
	$breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
        $breadcrumbs->addCrumb("home", array(
                "label" => $this->__("Home"),
                "title" => $this->__("Home"),
                "link"  => Mage::getBaseUrl()
		   ));

        $breadcrumbs->addCrumb("libro de reclamaciones", array(
                "label" => $this->__("Libro de Reclamaciones"),
                "title" => $this->__("Libro de Reclamaciones")
		   ));

        $this->renderLayout();         
    }
    
    public function successAction() {    
        $this->_forward("index",null,null,array("exito"=>1));
    }
    
}
