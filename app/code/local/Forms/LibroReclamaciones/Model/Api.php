<?php

class Forms_LibroReclamaciones_Model_Api extends Forms_LibroReclamaciones_Model_Api_Resource
{

    public function __construct()
    {
        $this->_attributesMap = array(
            'listar' => array( 'param1','param2' ),
        );
    }

    /** 
     * Revuelve el listado de los libros de reclamaciones
     * @param null|object|array $filters
     * @return array
     */    
    public function listar($filters= null) {
        
        $apiHelper = Mage::helper('api');
        $filters = $apiHelper->parseFilters($filters, $this->_attributesMap['listar']);
        
        // Para el uso de filtros
        $x = "";
        foreach ($filters as $k => $v) {
            $x.=$k."->".$v;
        }
        
        $collection = Mage::getModel('forms/libroreclamaciones')->getCollection()
                //->addFieldToFilter('status',1)                
                ->addOrder('fecha_registro', 'DESC');
        
        $reclamaciones = array();
        foreach ($collection as $r) {
            $reclamaciones[] = $this->_getAttributes($r, 'reclamaciones');
        }
        return $reclamaciones;        
    }
	
	
}
