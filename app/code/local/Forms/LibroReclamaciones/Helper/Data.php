<?php
class Forms_LibroReclamaciones_Helper_Data extends Mage_Core_Helper_Abstract
{
    
    const XML_PATH_ENABLED   = 'forms/libroreclamaciones/enabled';

    public function isEnabled()
    {
        return Mage::getStoreConfig( self::XML_PATH_ENABLED );
    }
    
}
	 