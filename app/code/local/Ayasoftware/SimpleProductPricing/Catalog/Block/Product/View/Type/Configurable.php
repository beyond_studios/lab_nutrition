<?php
/**
 * @category    Ayasoftware
 * @package     Ayasoftware_SimpleProductPricing
 * @author      EL HASSAN MATAR <support@ayasoftware.com>
 */
 
class Ayasoftware_SimpleProductPricing_Catalog_Block_Product_View_Type_Configurable extends Mage_Catalog_Block_Product_View_Type_Configurable {

    public function getJsonConfig() {
        if (!Mage::getStoreConfig('spp/setting/enableModule')) {
            return parent::getJsonConfig();
        }
        if(!$this->getProduct()->getUseSimpleProductPricing()){
            return parent::getJsonConfig();
        }
        $saveCache = false;
        $cache = Mage::app()->getCache();
        $cacheKey = $this->getProduct()->getId();
        if (intval(Mage::getStoreConfig('spp/setting/spp_cache_lifetime')) > 0) {
            if ($cache->load("jsonconfig_" . $cacheKey)) {
                $cache_jsonconfig = unserialize($cache->load("jsonconfig_" . $cacheKey));
                return $cache_jsonconfig;
            } else {
                $saveCache = true;
            }
        }
        $showOutOfStock = false;
        $config = Zend_Json::decode(parent::getJsonConfig());
        if (Mage::getStoreConfig('spp/details/show')) {
            $showOutOfStock = true;
        }
        $productsCollection = $this->getAllowProducts();
        //Create the extra price and tier price data/html we need.
        foreach ($productsCollection as $product) {
            $productId = $product->getId();
            if(Mage::app()->getStore()->isAdmin()) {
                $product = Mage::getModel('catalog/product')->load($productId);
            }
            $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productId);
            if( ( $stockItem->getQty() <= 0 || !$stockItem->getIsInStock() ) && $stockItem->getManageStock() ) {
                $stockInfo[$productId] = array(
                    "stockLabel" => $this->__('Out of stock'),
                    "stockQty" => intval($stockItem->getQty()),
                    "is_in_stock" => false,
                );
            } else {
                $stockInfo[$productId] = array(
                    "stockLabel" => $this->__('In Stock'),
                    "stockQty" => intval($stockItem->getQty()),
                    "is_in_stock" => true,
                );
            }
            $finalPrice = $product->getFinalPrice();
            if ($product->getCustomerGroupId()) {
                $finalPrice = $product->getGroupPrice();
            }
            if ($product->getTierPrice()) {
                $tprices = array();
                foreach ($tierprices = $product->getTierPrice() as $tierprice) {
                    $tprices[] = $tierprice['price'];
                }
                $tierpricing = min($tprices);
            } else {
                $tierpricing = '';
            }
            $has_image = false; 
            if($product->getData('thumbnail') && ($product->getData('thumbnail') != 'no_selection')) {
                $has_image = true; 
            }
            $childProducts[$productId] = array(
                "price" => $this->_registerJsPrice($this->_convertPrice($product->getPrice())),
                "finalPrice" => $this->_registerJsPrice($this->_convertPrice($finalPrice)),
                "tierpricing" => $this->_registerJsPrice($this->_convertPrice($tierpricing)),
                "has_image" => $has_image
            );
            if (Mage::getStoreConfig('spp/details/productname')) {
                $ProductNames[$productId] = array(
                    "ProductName" => $product->getName()
                );
            }
            if (Mage::getStoreConfig('spp/details/shortdescription')) {
                $shortDescriptions[$productId] = array(
                    "shortDescription" => $this->helper('catalog/output')->productAttribute($product, nl2br($product->getShortDescription()), 'short_description')
                );
            }
            if (Mage::getStoreConfig('spp/details/description')) {
                $Descriptions[$productId] = array(
                    "Description" => $this->helper('catalog/output')->productAttribute($product, nl2br($product->getDescription()), 'description')
                );
            }

            if (Mage::getStoreConfig('spp/details/productAttributes')) {
                $config['productAttributes'] = true;
                $config['product_attributes_markup'] = Mage::getStoreConfig('spp/markup/product_attributes_markup');
            } else {
                $config['productAttributes'] = false;
            }
        }
        if (Mage::getStoreConfig('spp/details/customstockdisplay')) {
            $config['customStockDisplay'] = true;
            $config['product_customstockdisplay_markup'] = Mage::getStoreConfig('spp/markup/product_customstockdisplay_markup'); 
        } else {
            $config['customStockDisplay'] = false;
        }
        $config['showOutOfStock'] = $showOutOfStock;
        $config['stockInfo'] = $stockInfo;
        $config['childProducts'] = $childProducts;
        $config['showPriceRangesInOptions'] = true;
        $config['rangeToLabel'] = $this->__('-');
        if (Mage::getStoreConfig('spp/setting/hideprices')) {
            $config['hideprices'] = true;
        } else {
            $config['hideprices'] = false;
        }
        if (Mage::getStoreConfig('spp/details/disable_out_of_stock_option')) {
            $config['disable_out_of_stock_option'] = true;
        } else {
            $config['disable_out_of_stock_option'] = false;
        }
        if (Mage::getStoreConfig('spp/details/productname')) {
            $config['productName'] = $this->getProduct()->getName();
            $config['ProductNames'] = $ProductNames;
            $config['product_name_markup'] = Mage::getStoreConfig('spp/markup/product_name_markup');
            $config['updateProductName'] = true;
        } else {
            $config['updateProductName'] = false;
        }
        if (Mage::getStoreConfig('spp/details/shortdescription')) {
            $config['shortDescription'] = $this->getProduct()->getShortDescription();
            $config['shortDescriptions'] = $shortDescriptions;
            $config['product_shortdescription_markup'] = Mage::getStoreConfig('spp/markup/product_shortdescription_markup');
            $config['updateShortDescription'] = true;
        } else {
            $config['updateShortDescription'] = false;
        }
        if (Mage::getStoreConfig('spp/details/description')) {
            $config['description'] = $this->getProduct()->getDescription();
            $config['Descriptions'] = $Descriptions;
            $config['product_description_markup'] = Mage::getStoreConfig('spp/markup/product_description_markup');
            $config['updateDescription'] = true;
        } else {
            $config['updateDescription'] = false;
        }
        
        if (Mage::getStoreConfig('spp/details/showfromprice')) {
            $config['showfromprice'] = true;
        } else {
            $config['showfromprice'] = false;
        }
        if (Mage::getStoreConfig('spp/media/updateproductimage')) {
            $config['updateproductimage'] = true;
            $config['product_image_markup'] = Mage::getStoreConfig('spp/markup/product_image_markup');
        } else {
            $config['updateproductimage'] = false;
        }
        $config['priceFromLabel'] = $this->__('From:');
        if (Mage::app()->getStore()->isCurrentlySecure()) {
            $config['ajaxBaseUrl'] = Mage::getUrl('spp/ajax/', array('_secure' => true));
        } else {
            $config['ajaxBaseUrl'] = Mage::getUrl('spp/ajax/');
        }
        $config['zoomtype'] = Mage::getStoreConfig('spp/media/zoomtype');
        $jsonConfig = Zend_Json::encode($config);
        if (intval(Mage::getStoreConfig('spp/setting/spp_cache_lifetime')) > 0 && $saveCache) {
            $cache->save(serialize($jsonConfig), "jsonconfig_" . $cacheKey, array("jsonconfig_cache"), intval(Mage::getStoreConfig('spp/setting/spp_cache_lifetime')));
        }
        return $jsonConfig;
    }

    /**
     * Get Allowed Products
     *
     * @return array
     */
    public function canShowOutOfStockProducts() {

        $category_name = Mage::getSingleton('catalog/layer')
                ->getCurrentCategory()
                ->getName();
        if (!$this->hasAllowProducts()) {
            $products = array();
            $allProducts = $this->getProduct()->getTypeInstance(true)
                    ->getUsedProducts(null, $this->getProduct());
            foreach ($allProducts as $product) {
                $products[] = $product;
            }
            $this->setAllowProducts($products);
        }
        return $this->getData('allow_products');
    }

    public function getAllowProducts() {
        if (!$this->hasAllowProducts()) {
            $products = array();
            $skipSaleableCheck = Mage::helper('catalog/product')->getSkipSaleableCheck();
            $allProducts = $this->getProduct()->getTypeInstance(true)
                    ->getUsedProducts(null, $this->getProduct());
            foreach ($allProducts as $product) {
                if (Mage::getStoreConfig('spp/details/show')) {
                    $products[] = $product;
                } else {
                    if ($product->isSaleable() || $skipSaleableCheck) {
                        $products[] = $product;
                    }
                }
            }
            $this->setAllowProducts($products);
        }
        return $this->getData('allow_products');
    }

}
