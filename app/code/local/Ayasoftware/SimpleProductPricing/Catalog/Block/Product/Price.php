<?php

/**
 * @category    Ayasoftware
 * @package     Ayasoftware_SimpleProductPricing
 * @copyright   2015 Ayasoftware (http://www.ayasoftware.com)
 * @author      EL HASSAN MATAR <support@ayasoftware.com>
 */
class Ayasoftware_SimpleProductPricing_Catalog_Block_Product_Price extends Mage_Catalog_Block_Product_Price {

    public function getDisplayMinimalPrice() {
        if( ! Mage::getStoreConfig('spp/setting/enableModule')) {
            return parent::getDisplayMinimalPrice();
        }
        $product = $this->getProduct();
        if(is_object($product) && $product->isConfigurable()) {
            return false;
        }
        return parent::getDisplayMinimalPrice();
    }

    public function _toHtml() {
        if( ! Mage::getStoreConfig('spp/setting/enableModule')) {
            return parent::_toHtml();
        }
        $product = $this->getProduct();
        if( ! $product->getUseSimpleProductPricing()) {
            return parent::_toHtml();
        }
        if( ! Mage::getStoreConfig('spp/details/showfromprice')) {
            return parent::_toHtml();
        }
        $prices = array();
        $htmlToInsertAfter = '<div class="price-box">';

        if($this->getTemplate() == 'catalog/product/price.phtml') {
            if(is_object($product) && $product->isConfigurable()) {
                $extraHtml = '<span class="label" id="configurable-price-from-'
                        . $product->getId()
                        . $this->getIdSuffix()
                        . '"><span class="configurable-price-from-label">';


                $prices = Mage::helper('spp')->getCheapestChildPrice($product);
                if($prices['Min']['finalPrice'] != $prices['Max']['finalPrice']) {
                    $extraHtml = '<span class="label" id="configurable-price-from-'
                            . $product->getId()
                            . $this->getIdSuffix()
                            . '"><span class="configurable-price-from-label">';
                    $extraHtml .= $this->__('From:');
                    $extraHtml .= '</span></span>';
                }
                $product->setPrice($prices['Min']['price']);
                $product->setFinalPrice($prices['Min']['finalPrice']);
                $extraHtml .= '</span></span>';
                $priceHtml = parent::_toHtml();
                #manually insert extra html needed by the extension into the normal price html
                return substr_replace($priceHtml, $extraHtml, strpos($priceHtml, $htmlToInsertAfter) + strlen($htmlToInsertAfter), 0);
            }
        }
        return parent::_toHtml();
    }

}
