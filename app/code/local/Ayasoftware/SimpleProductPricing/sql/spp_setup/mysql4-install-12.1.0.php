<?php

$installer = new Mage_Eav_Model_Entity_Setup($this->_resourceName);
$installer->startSetup();

$installer->addAttribute('catalog_product', 'use_simple_product_pricing', array(
    'type' => 'int',
    'backend' => '',
    'frontend' => '',
    'label' => 'Use simple product pricing',
    'note' => 'If set to "Yes", configurable product will use simple product pricing',
    'input' => 'boolean',
    'class' => '',
    'source' => 'eav/entity_attribute_source_boolean',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible' => true,
    'required' => false,
    'user_defined' => false,
    'default' => '1',
    'searchable' => false,
    'filterable' => false,
    'comparable' => false,
    'visible_on_front' => false,
    'unique' => false,
    'apply_to' => 'configurable',
    'used_in_product_listing'  => true,
    'is_configurable' => false,
));
$attributeId = $installer->getAttributeId('catalog_product', 'use_simple_product_pricing');

foreach ($installer->getAllAttributeSetIds('catalog_product') as $attributeSetId) {
    try {
        $attributeGroupId = $installer->getAttributeGroupId('catalog_product', $attributeSetId, 'General');
    } catch (Exception $e) {
        $attributeGroupId = $installer->getDefaultAttributeGroupId('catalog_product', $attributeSetId);
    }
    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attributeGroupId, $attributeId);
}

$installer->endSetup();
