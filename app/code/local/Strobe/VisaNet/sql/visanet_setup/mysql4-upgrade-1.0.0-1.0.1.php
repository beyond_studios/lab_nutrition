<?php
$installer = $this;
$installer->startSetup();
$prefix = Mage::getConfig()->getTablePrefix();
$table=$prefix."etickets";

$sql=<<<SQLTEXT
    ALTER TABLE `$table` ADD COLUMN `increment_id` VARCHAR(50) CHARACTER SET utf8 DEFAULT NULL
SQLTEXT;

$installer->run($sql);

$installer->endSetup();