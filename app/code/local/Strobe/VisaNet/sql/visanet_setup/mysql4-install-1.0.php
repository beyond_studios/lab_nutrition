<?php
$installer = $this;
$installer->startSetup();
$installer->run("CREATE TABLE IF NOT EXISTS {$this->getTable('etickets')} (
        `eticket` varchar(45)  NOT NULL,
        `order_id` int(11) unsigned NOT NULL,
        PRIMARY KEY (`eticket`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

$installer->endSetup();
