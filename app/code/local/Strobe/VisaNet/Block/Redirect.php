<?php

class Strobe_VisaNet_Block_Redirect extends Mage_Core_Block_Abstract
{
    protected function _toHtml()
    {
        $debug = false;

        if($debug){
            $form_url = "http://cal2testing.sytes.net/formularioweb/formulariopago.asp";
        }else{
            $form_url = "https://www.multimerchantvisanet.com/formularioweb/formulariopago.asp";
        }

        $form = new Varien_Data_Form();
        $form->setAction($form_url)
            ->setId('visanet_checkout')
            ->setName('visanet_checkout')
            ->setMethod('POST')
            ->setUseContainer(true);

        $session = Mage::getSingleton('checkout/session');
        $form->addField("codtienda", 'hidden', array('name'=>"codtienda", 'value'=> $session->getShopCode() ));
        $form->addField("numorden", 'hidden', array('name'=>"numorden", 'value'=> $session->getLastRealOrderId() ));
        $form->addField("mount", 'hidden', array('name'=>"mount", 'value'=> $session->getGrandTotal() ));
        $form->addField("dato_comercio", 'hidden', array('name'=>"dato_comercio", 'value'=> "Pago por Bembos" ));
        $form->addField("eticket", 'hidden', array('name'=>"eticket", 'value'=> $session->getEticket() ));

        $html = '<html><body>';
        $html.= $this->__('Serás redireccionado a la pasarela de VisaNet en pocos segundos...');
        $html.= $form->toHtml();
        $html.= '<script type="text/javascript">document.getElementById("visanet_checkout").submit();</script>';
        $html.= '</body></html>';
        
        return $html;
    }
}