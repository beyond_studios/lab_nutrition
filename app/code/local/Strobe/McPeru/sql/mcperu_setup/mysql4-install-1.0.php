<?php
$installer = $this;
$installer->startSetup();
$installer->run("CREATE TABLE IF NOT EXISTS {$this->getTable('mcperu')} (
  `O1` varchar(50) NOT NULL,
  `O2` varchar(50) NOT NULL,
  `O3` varchar(50) NOT NULL,
  `O4` varchar(50) NOT NULL,
  `O5` varchar(50) NOT NULL,
  `O6` varchar(50) NOT NULL,
  `O7` varchar(50) NOT NULL,
  `O8` varchar(50) NOT NULL,
  `O9` varchar(50) NOT NULL,
  `O10` varchar(50) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");
$installer->endSetup();
