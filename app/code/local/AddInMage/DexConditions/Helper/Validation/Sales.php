<?php
/**
 * Add In Mage::
 *
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the EULA that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL: http://add-in-mage.com/support/presales/eula/
 *
 *
 * PROPRIETARY DATA
 * 
 * This file contains trade secret data which is the property of Add In Mage:: Ltd. 
 * This file is submitted to recipient in confidence.
 * Information and source code contained herein may not be used, copied, sold, distributed, 
 * sub-licensed, rented, leased or disclosed in whole or in part to anyone except as permitted by written
 * agreement signed by an officer of Add In Mage:: Ltd.
 * 
 * 
 * MAGENTO EDITION NOTICE
 * 
 * This software is designed for Magento Community edition.
 * Add In Mage:: Ltd. does not guarantee correct work of this extension on any other Magento edition.
 * Add In Mage:: Ltd. does not provide extension support in case of using a different Magento edition.
 * 
 * 
 * @category    AddInMage
 * @package     AddInMage_DexConditions
 * @copyright   Copyright (c) 2013 Add In Mage:: Ltd. (http://www.add-in-mage.com)
 * @license     http://add-in-mage.com/support/presales/eula/  End User License Agreement (EULA)
 * @author      Add In Mage:: Team <team@add-in-mage.com>
 */

class AddInMage_DexConditions_Helper_Validation_Sales extends Mage_Core_Helper_Abstract
{
		
	public function filter($extra)
	{
		
		if(isset($extra['use_sales_behavior']) && $extra['use_sales_behavior'] == true) {
			if(isset($extra['sales']) && count($extra['sales'])) {
				sort($extra['sales']);
				foreach ($extra['sales'] as $key => $Srule) {
					if(isset($Srule['status_criterion'])) {
						if($Srule['status_criterion'] == 'any') {
							if(isset($Srule['statuses']))
								unset($extra['sales'][$key]['statuses']);
						} else {
							if(isset($Srule['statuses']) && !count($Srule['statuses'])) {
								$extra['sales'][$key]['status_criterion'] = 'any';
								unset($extra['sales'][$key]['statuses']);
							}
						}
					}
					if(isset($Srule['store_criterion'])) {
						if($Srule['store_criterion'] == 'any') {
							if(isset($Srule['stores']))
								unset($extra['sales'][$key]['stores']);
						} else {
							if(isset($Srule['stores']) && empty($Srule['stores'])) {
								$extra['sales'][$key]['store_criterion'] = 'any';
								unset($extra['sales'][$key]['stores']);
							}
						}
					}
				}
					
			} else {
				$extra['use_sales_behavior'] = false;
				unset($extra['sales']);
			}
		} else {
			if(isset($extra['sales']))
				unset($extra['sales']);
		}
		
		return $extra;
	}
}