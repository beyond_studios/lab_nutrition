<?php
/**
 * Add In Mage::
 *
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the EULA that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL: http://add-in-mage.com/support/presales/eula/
 *
 *
 * PROPRIETARY DATA
 * 
 * This file contains trade secret data which is the property of Add In Mage:: Ltd. 
 * This file is submitted to recipient in confidence.
 * Information and source code contained herein may not be used, copied, sold, distributed, 
 * sub-licensed, rented, leased or disclosed in whole or in part to anyone except as permitted by written
 * agreement signed by an officer of Add In Mage:: Ltd.
 * 
 * 
 * MAGENTO EDITION NOTICE
 * 
 * This software is designed for Magento Community edition.
 * Add In Mage:: Ltd. does not guarantee correct work of this extension on any other Magento edition.
 * Add In Mage:: Ltd. does not provide extension support in case of using a different Magento edition.
 * 
 * 
 * @category    AddInMage
 * @package     AddInMage_DexConditions
 * @copyright   Copyright (c) 2013 Add In Mage:: Ltd. (http://www.add-in-mage.com)
 * @license     http://add-in-mage.com/support/presales/eula/  End User License Agreement (EULA)
 * @author      Add In Mage:: Team <team@add-in-mage.com>
 */

class AddInMage_DexConditions_Helper_Types_Day extends Mage_Core_Helper_Abstract
{
	
	const ENABLE_DISCOUNT 	= 1;
	const DISABLE_DISCOUNT 	= 0;
	
	public function getConditionNinId($extra, $ruleId)
	{
		
		if(isset($extra['use_daily_behavior']) && (bool)$extra['use_daily_behavior'] == true) {
				
			if(!$this->isValidSettings($extra)) return null;
			
			$nowTimeString = (int)date('His', Mage::getModel('core/date')->timestamp(time()));
			$action = $extra['hourly_action'];
		
			foreach ($extra['timeframe'] as $timeframe) {

				if(!$this->isValidCondition($timeframe)) continue;
				
				$can_process = false;
				$start = (int)implode('', $timeframe['from']);
				$end = (int)implode('', $timeframe['to']);
		
				if($action == self::ENABLE_DISCOUNT) {
					if(!(($start <= $nowTimeString) && ($nowTimeString <= $end))) {
						continue;
					} else {
						$can_process = true;
						break;
					}
				} else {
					if(($start <= $nowTimeString) && ($nowTimeString <= $end)) {
						$can_process = false;
						break;
					}
					else {
						$can_process = true;
						continue;
					}
				}
			}
			
			if(!$can_process) return $ruleId;
		}	
		return null;
	}
	
	protected function isValidSettings($extra)
	{
		if(!isset($extra['hourly_action']) || !in_array($extra['hourly_action'], array(self::ENABLE_DISCOUNT, self::DISABLE_DISCOUNT)))
			return false;
	
		if(!isset($extra['timeframe']) || !is_array($extra['timeframe']) || !count($extra['timeframe']))
			return false;
	
		return true;
	}
	
	public function isValidCondition($condition, $onSave = false)
	{
		$is_less = false;
		$is_equal = false;
		
		$hourly_action_from = (int)implode('', $condition['from']);
		$hourly_action_to = (int)implode('', $condition['to']);
		
		if($hourly_action_to == $hourly_action_from) 
			if($onSave) $is_equal = true;
			else return false;
		
		if($hourly_action_to < $hourly_action_from)
			if($onSave) $is_less = true;
			else return false;
			
		if(!$onSave) return true;
		else {
			
			$errors = array();
			
			if($is_equal || $is_less) $errors[] = Mage::helper('dexconditions')->__("Please check timeframes on the 'Daily Condition' tab.");
			if($is_equal) $errors[] = Mage::helper('dexconditions')->__("Daily Condition End Time should not be equal to Daily Condition Start Time.");
			if($is_less) $errors[] = Mage::helper('dexconditions')->__("Daily Condition End Time should be greater than Daily Condition Start Time.");
			
			return $errors;
		}
	}
}