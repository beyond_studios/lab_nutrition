<?php
/**
 * Add In Mage::
 *
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the EULA that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL: http://add-in-mage.com/support/presales/eula/
 *
 *
 * PROPRIETARY DATA
 * 
 * This file contains trade secret data which is the property of Add In Mage:: Ltd. 
 * This file is submitted to recipient in confidence.
 * Information and source code contained herein may not be used, copied, sold, distributed, 
 * sub-licensed, rented, leased or disclosed in whole or in part to anyone except as permitted by written
 * agreement signed by an officer of Add In Mage:: Ltd.
 * 
 * 
 * MAGENTO EDITION NOTICE
 * 
 * This software is designed for Magento Community edition.
 * Add In Mage:: Ltd. does not guarantee correct work of this extension on any other Magento edition.
 * Add In Mage:: Ltd. does not provide extension support in case of using a different Magento edition.
 * 
 * 
 * @category    AddInMage
 * @package     AddInMage_DexConditions
 * @copyright   Copyright (c) 2013 Add In Mage:: Ltd. (http://www.add-in-mage.com)
 * @license     http://add-in-mage.com/support/presales/eula/  End User License Agreement (EULA)
 * @author      Add In Mage:: Team <team@add-in-mage.com>
 */

class AddInMage_DexConditions_Helper_Types_Sales extends Mage_Core_Helper_Abstract
{
	const ENABLE_DISCOUNT 	= 1;
	const DISABLE_DISCOUNT 	= 0;
	
	const ALL_TRUE 			= 'all_true';
	const ALL_FALSE 		= 'all_false';
	const ANY_TRUE 			= 'any_true';
	const ANY_FALSE 		= 'any_false';
	
	
	public function getConditionNinId($extra, $ruleId)
	{
		
		if(isset($extra['use_sales_behavior']) && (bool)$extra['use_sales_behavior'] == true) {
			
			// Validate settings
			if(!$this->isValidSettings($extra)) return null;			
			
			$matchRuleSC = false;
			$ruleSC = $extra['sales_condition'];
			$action = $extra['sales_action'];
			$results = array();
			
			foreach ($extra['sales'] as $condition) {	
				// Validate condition
				if(!$this->isValidCondition($condition)) continue;
				$results[] = $this->checkCondition($condition);
			}
			
			if($ruleSC == self::ALL_TRUE) {
				$matchRuleSC = (in_array(0, $results)) ? false : true;
			} elseif($ruleSC == self::ALL_FALSE) {
				$matchRuleSC = (in_array(1, $results)) ? false : true;
			} elseif($ruleSC == self::ANY_TRUE) {
				$matchRuleSC = (in_array(1, $results)) ? true : false;
			} elseif($ruleSC == self::ANY_FALSE) {
				$matchRuleSC = (in_array(0, $results)) ? true : false;
			}

			
			
			if($action == self::ENABLE_DISCOUNT && !$matchRuleSC)
				return $ruleId;
			if($action == self::DISABLE_DISCOUNT && $matchRuleSC) 
				return $ruleId;
		}
		
		return null;
	}
	
	
	protected function checkCondition($condition)
	{
		$calcValue = ($this->calculate($condition)) ? $this->calculate($condition) : 0;
		$origValue = $condition['qa'];
		$result = 0;
	
		switch ($condition['condition']) {
			case '==':
				$result = ($calcValue == $origValue) ? 1 : 0;
				break;				
			case '!=':
				$result = ($calcValue != $origValue) ? 1 : 0;
				break;
			case '>=':
				$result = ($calcValue >= $origValue) ? 1 : 0;
				break;
			case '<=':
				$result = ($calcValue <= $origValue) ? 1 : 0;
				break;			
			case '>':
				$result = ($calcValue > $origValue) ? 1 : 0;
				break;			
			case '<':
				$result = ($calcValue < $origValue) ? 1 : 0;
				break;			
		}
		return $result;
	}
	
	protected function calculate($condition)
	{
		
		$dateRange = $this->getDateRange($condition);
		$statuses = $this->getStatuses($condition);
		$stores = $this->getStores($condition);
		$subject = $condition['criterion'];
	
		$value = 0;
		$resourceCollection = Mage::getResourceModel('sales/report_order_updatedat_collection')
								->setDateRange($dateRange['from'], $dateRange['to'])
								->addOrderStatusFilter($statuses)
								->addStoreFilter($stores);
		
		$resourceCollection->load();
		
		if($resourceCollection->getSize()) {
			foreach ($resourceCollection as $row)
				
				$value += (int)$row->getData($subject);
		}
		
		return $value;	
	}
	
	protected function getStores($condition)
	{
		$storeCrit = (isset($condition['store_criterion'])) ? $condition['store_criterion'] : 'any';
		$stores = null;
		
		if($storeCrit == 'specified' && isset($condition['stores']) && !empty($condition['stores'])) {
			$stores = explode(',', $condition['stores']);
		}
	
		return $stores;
	}
	
	protected function getStatuses($condition)
	{
		$statuses = $this->getOrderStatuses();
		$statusCrit = (isset($condition['status_criterion'])) ? $condition['status_criterion'] : 'any';
		
		if($statusCrit == 'specified' && isset($condition['statuses'])) {
			if(is_array($condition['statuses']) && count($condition['statuses'])){
				$statuses = $condition['statuses'];
			}
		}
		
		return $statuses;
	}
	
	protected function getDateRange($condition)
	{
		$dateEnd = new Zend_Date(Mage::getModel('core/date')->timestamp(time()));
		$dateStart = clone $dateEnd;
		
		$dateEnd->setHour(23);
		$dateEnd->setMinute(59);
		$dateEnd->setSecond(59);
		
		$dateStart->setHour(0);
		$dateStart->setMinute(0);
		$dateStart->setSecond(0);		
		
		$shift = (isset($condition['interval'])) ? $condition['interval'] : null;
		$date = array('from' => null, 'to' => null);
		
		switch ($condition['for']) {
			case 'hour':
				$now = new Zend_Date(Mage::getModel('core/date')->timestamp(time()));
				$date = array('from' => $now->setMinute(0)->setSecond(0)->toString('YYYY-MM-dd HH:m:s'), 'to' => $dateEnd->toString('YYYY-MM-dd HH:m:s'));
				break;
			case 'previous_hour':
				$now = new Zend_Date(Mage::getModel('core/date')->timestamp(time()));
				$hourEnd = clone $now;
				$date = array('from' => $now->setMinute(0)->setSecond(0)->subHour(1)->toString('YYYY-MM-dd HH:m:s'), 'to' => $hourEnd->setMinute(59)->setSecond(59)->subHour(1)->toString('YYYY-MM-dd HH:m:s'));
				break;
			case 'today':
				$from = $dateStart->toString('YYYY-MM-dd HH:m:s');
				$date = array('from' => $from, 'to' => $dateEnd->toString('YYYY-MM-dd HH:m:s'));
				break;
			case 'yesterday':
				$yesterday = $dateStart->subDay(1)->toString('YYYY-MM-dd HH:m:s');
				$date = array('from' => $yesterday, 'to' => $dateEnd->subDay(1)->toString('YYYY-MM-dd HH:m:s'));
				break;
			case 'week':
				$firstWeekDay = Mage::getStoreConfig('general/locale/firstday', Mage::app()->getStore());
				if(is_null($firstWeekDay)) $firstWeekDay = 1;
				
				$newDate = new Zend_Date();
				$newDate->setYear($dateStart->getYear())
						->setWeek($dateStart->getWeek())
						->set($firstWeekDay, Zend_Date::WEEKDAY_DIGIT)
						->setHour(0)
		 				->setMinute(0)
		 				->setSecond(0);	
				
				$date = array('from' => $newDate->toString('YYYY-MM-dd HH:m:s'), 'to' => $dateEnd->toString('YYYY-MM-dd HH:m:s'));
				break;
			case 'month':				
				$newDate = new Zend_Date();
				$newDate->setYear($dateStart->getYear())
						->setMonth($dateStart->getMonth())
						->set(1, Zend_Date::DAY)
						->setHour(0)
		 				->setMinute(0)
		 				->setSecond(0);	
				
				$date = array('from' => $newDate->toString('YYYY-MM-dd HH:m:s'), 'to' => $dateEnd->toString('YYYY-MM-dd HH:m:s'));
				break;
			case 'year':				
				$newDate = new Zend_Date();
				$newDate->setYear($dateStart->getYear())
						->set(1, Zend_Date::DAY_OF_YEAR)
						->setHour(0)
		 				->setMinute(0)
		 				->setSecond(0);	
				
				$date = array('from' => $newDate->toString('YYYY-MM-dd HH:m:s'), 'to' => $dateEnd->toString('YYYY-MM-dd HH:m:s'));
				break;
				
			case 'last_x_hours':
				$now = new Zend_Date(Mage::getModel('core/date')->timestamp(time()));
				$date = array('from' => $now->subHour($shift)->toString('YYYY-MM-dd HH:m:s'), 'to' => $dateEnd->toString('YYYY-MM-dd HH:m:s'));
				break;
			case 'last_x_days':
				$date = array('from' => $dateStart->subDay($shift-1)->toString('YYYY-MM-dd HH:m:s'), 'to' => $dateEnd->toString('YYYY-MM-dd HH:m:s'));
				break;
			case 'last_x_weeks':
				$date = array('from' => $dateStart->subWeek($shift)->toString('YYYY-MM-dd HH:m:s'), 'to' => $dateEnd->toString('YYYY-MM-dd HH:m:s'));
				break;
			case 'last_x_months':
				$date = array('from' => $dateStart->subMonth($shift)->toString('YYYY-MM-dd HH:m:s'), 'to' => $dateEnd->toString('YYYY-MM-dd HH:m:s'));
				break;
			case 'last_x_years':
				$date = array('from' => $dateStart->subYear($shift)->toString('YYYY-MM-dd HH:m:s'), 'to' => $dateEnd->toString('YYYY-MM-dd HH:m:s'));
				break;
		}
		
		return $date;
	}
	
	
	protected function isValidSettings($extra)
	{
		if(!isset($extra['sales_action']) || !in_array($extra['sales_action'], array(self::ENABLE_DISCOUNT, self::DISABLE_DISCOUNT)))
			return false;
		
		if(!isset($extra['sales_condition']) || !in_array($extra['sales_condition'], array(self::ALL_TRUE, self::ALL_FALSE, self::ANY_TRUE, self::ANY_FALSE)))
			return false;
		
		if(!isset($extra['sales']) || !count($extra['sales']))
			return false;
		
		return true;
	}
	
	public function isValidCondition($condition, $onSave = false)
	{		
		$miscAction = false;
		$miscInterval = false;
		
		if(!isset($condition['criterion']) || !in_array($condition['criterion'], $this->getCriterionValuesSource(true)))
			if($onSave) $miscAction = true;
			else return false;
		
		if(!isset($condition['condition']) || !in_array($condition['condition'], $this->getConditionValuesSource(true)))
			if($onSave) $miscAction = true;
			else return false;

			
		if(!isset($condition['for']) || !in_array($condition['for'], $this->getForValuesSource(true)))
			if($onSave) $miscAction = true;
			else return false;
		
		if(in_array($condition['for'], $this->getForValuesSource(true, true))) {
			$badInt = false;
			if(!isset($condition['interval'])) $badInt = true;
			else {
				$validator = new Zend_Validate_Digits();
				if(!$validator->isValid($condition['interval'])) $badInt = true;
				
				if($condition['interval'] == '0')
					if($onSave) $miscInterval = true;
				else return false;
			}
			if($badInt)
				if($onSave) $miscAction = true;
				else return false;
		}
		
		if(isset($condition['qa'])) {
			$validator = new Zend_Validate_Digits();
			if(!$validator->isValid($condition['qa']))
				if($onSave) $miscAction = true;
				else return false;
		}
		
		if(!$onSave) return true;
		else {
			
			$errors = array();
			
			if($miscAction || $miscInterval) $errors[] = Mage::helper('dexconditions')->__("Please check conditions on the 'Sales Dependent Conditions' tab.");
			if($miscAction) $errors[] = Mage::helper('dexconditions')->__("Some of your conditions have important data missing. Please fill all the necessary fields.");
			if($miscInterval) $errors[] = Mage::helper('dexconditions')->__("The intervals specified in the conditions can not be equal to zero.");
			
			return $errors;
		}
	}
	
	
	public function getConditionValues()
	{
		$conditions = array();
		
		foreach ($this->getConditionValuesSource() as $select) 	$conditions[] = $select;
		
		return $conditions;
	}
	
	protected function getConditionValuesSource($validation = false)
	{
		if($validation)
			return array('==', '!=', '>=', '<=', '>', '<');
		
		return 
				array(
						'==' => array('label' => Mage::helper('dexconditions')->__('is'), 'value' => '=='),
						'!=' => array('label' => Mage::helper('dexconditions')->__('is not'), 'value' => '!='),
						'>=' => array('label' => Mage::helper('dexconditions')->__('is equal to or greater than'), 'value' => '>='),
						'<=' => array('label' => Mage::helper('dexconditions')->__('is equal to or less than'), 'value' => '<='),
						'>' => array('label' => Mage::helper('dexconditions')->__('is greater than'), 'value' => '>'),
						'<' => array('label' => Mage::helper('dexconditions')->__('is less than'), 'value' => '<')				
				);
	}
	
	public function getForValues()
	{
		$conditions = array();		
		
		foreach ($this->getForValuesSource() as $select) $conditions[] = $select;	
			
		return $conditions;
	}
	
	public function getSalesPastIntValues()
	{
		return array(
				'last_x_hours' => Mage::helper('dexconditions')->__('hour(s)'),
				'last_x_days' => Mage::helper('dexconditions')->__('day(s)'),
				'last_x_weeks' => Mage::helper('dexconditions')->__('week(s)'),
				'last_x_months' => Mage::helper('dexconditions')->__('month(s)'),
				'last_x_years' => Mage::helper('dexconditions')->__('year(s)'));
	}
	
	public function getForValuesSource($validation = false, $special = false)
	{
		if($validation) {
			
			if($special) return array('last_x_hours', 'last_x_days', 'last_x_weeks', 'last_x_months', 'last_x_years');
			return array('hour', 'previous_hour', 'today', 'yesterday', 'week', 'month', 'year', 'last_x_hours', 'last_x_days', 'last_x_weeks', 'last_x_months', 'last_x_years');
		}
		
		return
				array(
						'hour' => array('label' => Mage::helper('dexconditions')->__('during the current hour'), 'value' => 'hour'),
						'previous_hour' => array('label' => Mage::helper('dexconditions')->__('in the prevous hour'), 'value' => 'previous_hour'),
						'today' => array('label' => Mage::helper('dexconditions')->__('today'), 'value' => 'today'),
						'yesterday' => array('label' => Mage::helper('dexconditions')->__('yesterday'), 'value' => 'yesterday'),
						'week' => array('label' => Mage::helper('dexconditions')->__('during this week'), 'value' => 'week'),
						'month' => array('label' => Mage::helper('dexconditions')->__('during this month'), 'value' => 'month'),
						'year' => array('label' => Mage::helper('dexconditions')->__('during this year'), 'value' => 'year'),
						'last_x_hours' => array('label' => Mage::helper('dexconditions')->__('during the last X hours'), 'value' => 'last_x_hours'),
						'last_x_days' => array('label' => Mage::helper('dexconditions')->__('during the last X days'), 'value' => 'last_x_days'),
						'last_x_weeks' => array('label' => Mage::helper('dexconditions')->__('during the last X weeks'), 'value' => 'last_x_weeks'),
						'last_x_months' => array('label' => Mage::helper('dexconditions')->__('during the last X months'), 'value' => 'last_x_months'),
						'last_x_years' => array('label' => Mage::helper('dexconditions')->__('during the last X years'), 'value' => 'last_x_years'),
				);
	}
	
	public function getOrderStatuses()
	{
		$statuses = Mage::getModel('sales/order_config')->getStatuses();
        $values = array();
        foreach ($statuses as $code => $label) {
        	if (false === strpos($code, 'pending')) {
            	$values[] = array(
                	'label' => Mage::helper('reports')->__($label),
                    'value' => $code
                );
            }
        }
        return $values;
	}
	
	public function getStoreCritValues()
	{
		return array(
				array('label' => Mage::helper('dexconditions')->__('on all websites'), 'value' => 'any'),
				array('label' => Mage::helper('dexconditions')->__('at the following stores:'), 'value' => 'specified'));
	}
	
	public function getOrderStatusValues()
	{
		return array(
				array('label' => Mage::helper('dexconditions')->__('with any order status'), 'value' => 'any'),
				array('label' => Mage::helper('dexconditions')->__('with the following order statuses:'), 'value' => 'specified'));
	}
	
	public function getCriterionValues()
	{
		$conditionsSource = $this->getCriterionValuesSource();
		return array(
    						array('label' => Mage::helper('dexconditions')->__('-- Please select --'), 'value' => ''),
    						array('label' => Mage::helper('dexconditions')->__('Amount'), 'value' => $conditionsSource['amount']),
    						array('label' => Mage::helper('dexconditions')->__('Quantity'), 'value' => $conditionsSource['quantity'])); 
	}
	
	
	public function getStoreSwitcherValues()
	{
		return Mage::helper('dexconditions')->getStoreSwitcherValues();		
	}
	
	public function getCriterionValuesSource($validation = false)
	{
		if($validation)
			return array(
					'total_income_amount', 
					'total_revenue_amount', 
					'total_profit_amount', 
					'total_invoiced_amount', 
					'total_paid_amount', 
					'total_refunded_amount', 
					'total_tax_amount',
					'total_tax_amount_actual',
					'total_shipping_amount',
					'total_shipping_amount_actual',
					'total_discount_amount',
					'total_discount_amount_actual',
					'total_canceled_amount',
					'orders_count',
					'total_qty_ordered',
					'total_qty_invoiced'
					);
		
		return 
				array(
						'amount' => array(
							array('label' => Mage::helper('dexconditions')->__('If total income amount'), 'value' => 'total_income_amount'),
							array('label' => Mage::helper('dexconditions')->__('If total revenue amount'), 'value' => 'total_revenue_amount'),
							array('label' => Mage::helper('dexconditions')->__('If total profit amount'), 'value' => 'total_profit_amount'),
							array('label' => Mage::helper('dexconditions')->__('If total invoiced amount'), 'value' => 'total_invoiced_amount'),
							array('label' => Mage::helper('dexconditions')->__('If total paid amount'), 'value' => 'total_paid_amount'),
							array('label' => Mage::helper('dexconditions')->__('If total refunded amount'), 'value' => 'total_refunded_amount'),
							array('label' => Mage::helper('dexconditions')->__('If total tax amount'), 'value' => 'total_tax_amount'),
							array('label' => Mage::helper('dexconditions')->__('If actual total tax amount'), 'value' => 'total_tax_amount_actual'),
							array('label' => Mage::helper('dexconditions')->__('If total shipping amount'), 'value' => 'total_shipping_amount'),
							array('label' => Mage::helper('dexconditions')->__('If actual total shipping amount'), 'value' => 'total_shipping_amount_actual'),
							array('label' => Mage::helper('dexconditions')->__('If total discount amount'), 'value' => 'total_discount_amount'),
							array('label' => Mage::helper('dexconditions')->__('If actual total discount amount'), 'value' => 'total_discount_amount_actual'),
							array('label' => Mage::helper('dexconditions')->__('If total canceled amount'), 'value' => 'total_canceled_amount'),
						),
						
						'quantity' => array( 
							array('label' => Mage::helper('dexconditions')->__('If order quantity'), 'value' => 'orders_count'),
    						array('label' => Mage::helper('dexconditions')->__('If total quantity of items ordered'), 'value' => 'total_qty_ordered'),
    						array('label' => Mage::helper('dexconditions')->__('If total quantity of items invoiced'), 'value' => 'total_qty_invoiced')
    			));
	}
}