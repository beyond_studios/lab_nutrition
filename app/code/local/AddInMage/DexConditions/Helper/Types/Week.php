<?php
/**
 * Add In Mage::
 *
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the EULA that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL: http://add-in-mage.com/support/presales/eula/
 *
 *
 * PROPRIETARY DATA
 * 
 * This file contains trade secret data which is the property of Add In Mage:: Ltd. 
 * This file is submitted to recipient in confidence.
 * Information and source code contained herein may not be used, copied, sold, distributed, 
 * sub-licensed, rented, leased or disclosed in whole or in part to anyone except as permitted by written
 * agreement signed by an officer of Add In Mage:: Ltd.
 * 
 * 
 * MAGENTO EDITION NOTICE
 * 
 * This software is designed for Magento Community edition.
 * Add In Mage:: Ltd. does not guarantee correct work of this extension on any other Magento edition.
 * Add In Mage:: Ltd. does not provide extension support in case of using a different Magento edition.
 * 
 * 
 * @category    AddInMage
 * @package     AddInMage_DexConditions
 * @copyright   Copyright (c) 2013 Add In Mage:: Ltd. (http://www.add-in-mage.com)
 * @license     http://add-in-mage.com/support/presales/eula/  End User License Agreement (EULA)
 * @author      Add In Mage:: Team <team@add-in-mage.com>
 */

class AddInMage_DexConditions_Helper_Types_Week extends Mage_Core_Helper_Abstract
{
	
	const ENABLE_DISCOUNT 	= 1;
	const DISABLE_DISCOUNT 	= 0;
	
	public function getConditionNinId($extra, $ruleId)
	{	
		
		if(isset($extra['use_weekly_behavior']) && (bool)$extra['use_weekly_behavior'] == true) {
			
			if(!$this->isValidSettings($extra)) return null;
			
			$days = $extra['action_days'];
			$current_day = date( "w", Mage::app()->getLocale()->storeTimeStamp());
		
			if($extra['daily_action'] == self::ENABLE_DISCOUNT) {
				if(!in_array($current_day, $days)) return $ruleId;
			} else {
				if(in_array($current_day, $days)) return $ruleId;
			}
		}
		
		return null;
	}
	
	public function isValidSettings($extra, $onSave = false)
	{
		
		$days = range(0, 6);
		$badConf = false;
		
		if(!$onSave) {
			if(!isset($extra['daily_action']) || !in_array($extra['daily_action'], array(self::ENABLE_DISCOUNT, self::DISABLE_DISCOUNT)))
				return false;
		
			if(!isset($extra['action_days']) || !is_array($extra['action_days']) || !count($extra['action_days']))
				return false;
		}
		
		foreach ($extra['action_days'] as $day) {
			if(!in_array($day, $days)) {
				if($onSave) $badConf = true;
				else return false;
			}
		}
		
		if(!$onSave) return true;
		else {
			
			$errors = array();
				
			if($badConf) $errors[] = Mage::helper('dexconditions')->__("Please select valid days on the 'Weekly Condition' tab.");
			return $errors;
		}
	}
}