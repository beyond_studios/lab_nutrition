<?php
/**
 * Add In Mage::
 *
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the EULA that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL: http://add-in-mage.com/support/presales/eula/
 *
 *
 * PROPRIETARY DATA
 * 
 * This file contains trade secret data which is the property of Add In Mage:: Ltd. 
 * This file is submitted to recipient in confidence.
 * Information and source code contained herein may not be used, copied, sold, distributed, 
 * sub-licensed, rented, leased or disclosed in whole or in part to anyone except as permitted by written
 * agreement signed by an officer of Add In Mage:: Ltd.
 * 
 * 
 * MAGENTO EDITION NOTICE
 * 
 * This software is designed for Magento Community edition.
 * Add In Mage:: Ltd. does not guarantee correct work of this extension on any other Magento edition.
 * Add In Mage:: Ltd. does not provide extension support in case of using a different Magento edition.
 * 
 * 
 * @category    AddInMage
 * @package     AddInMage_DexConditions
 * @copyright   Copyright (c) 2013 Add In Mage:: Ltd. (http://www.add-in-mage.com)
 * @license     http://add-in-mage.com/support/presales/eula/  End User License Agreement (EULA)
 * @author      Add In Mage:: Team <team@add-in-mage.com>
 */

class AddInMage_DexConditions_Block_Adminhtml_Promo_Quote_Edit_Tab_Extra_Form_Sales extends AddInMage_DexConditions_Block_Adminhtml_Promo_Quote_Edit_Tab_Extra_Conditions
{
	public function addFormData($form, $extra_data)
	{
		$fieldset = $form->addFieldset('extra_sales_fieldset', array(
       		'legend' => Mage::helper('dexconditions')->__('Sales Dependent Conditions')
       	));
		
		$name_prefix = 'extra';
		$yesno = Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray();
		
		$use_sales_behavior = $fieldset->addField('use_sales_behavior', 'select', array(
       		'label'     => Mage::helper('dexconditions')->__('Sales Dependent Conditions'),
       		'name'      => $name_prefix.'[use_sales_behavior]',
       		'note'  	=> Mage::helper('dexconditions')->__('Enable or disable sales dependent conditions.'),
       		'values' 	=> Mage::getModel('adminhtml/system_config_source_enabledisable')->toOptionArray()
       	));

       
       	$sales_action = $fieldset->addField('sales_action', 'select', array(
       		'label'     => Mage::helper('dexconditions')->__('Action'),
       		'name'      => $name_prefix.'[sales_action]',
       		'note'  	=> Mage::helper('dexconditions')->__('Choose if you want to apply this discount or not if the conditions below are met.'),
       		'values' 	=> array(
					       		array('value' => 1, 'label' => Mage::helper('dexconditions')->__('Apply this discount')),
					       		array('value' => 0, 'label' => Mage::helper('dexconditions')->__('Do not apply this discount'))
					       )
       	));
     
		$sales_condition = $fieldset->addField('sales_condition', 'select', array(
       		'label'     => '',
       		'name'      => $name_prefix.'[sales_condition]',
       		//'note'  	=> Mage::helper('dexconditions')->__('Please, select the condition.'),
       		'values' 	=> array(
       							array('label' => Mage::helper('dexconditions')->__('ALL'), 'value' => array(
       								array('value' => 'all_true', 'label' => Mage::helper('dexconditions')->__('If ALL of these conditions are TRUE')),
		        					array('value' => 'all_false', 'label' => Mage::helper('dexconditions')->__('If ALL of these conditions are FALSE'))
       							)),
			       				array('label' => Mage::helper('dexconditions')->__('ANY'), 'value' => array(
			       						array('value' => 'any_true', 'label' => Mage::helper('dexconditions')->__('If ANY of these conditions are TRUE')),
			       						array('value' => 'any_false', 'label' => Mage::helper('dexconditions')->__('If ANY of these conditions are FALSE'))
			       				)),
       						),      
       	));
		
		
		
		$fieldset->addType('sales_conditions', 'AddInMage_DexConditions_Block_Adminhtml_Promo_Quote_Edit_Tab_Renderer_Sales');
        
        
        $sales_conditions = $fieldset->addField('sales_conditions', 'sales_conditions', array(
          	'label'     => '',
           	'name'      => 'sales_conditions'
        ))->setExtra($extra_data)->setRenderer($this->getLayout()->createBlock('dexconditions/adminhtml_promo_quote_edit_tab_renderer_sales_constructor'));
		
		$dependence = $this->getFormAfter();
		
		$dependence
			->addFieldMap($use_sales_behavior->getHtmlId(), $use_sales_behavior->getName())
			->addFieldMap($sales_condition->getHtmlId(), $sales_condition->getName())
			->addFieldMap($sales_action->getHtmlId(), $sales_action->getName())
			->addFieldDependence($sales_condition->getName(), $use_sales_behavior->getName(), 1)
			->addFieldDependence($sales_action->getName(), $use_sales_behavior->getName(), 1);
			    
	}
}
