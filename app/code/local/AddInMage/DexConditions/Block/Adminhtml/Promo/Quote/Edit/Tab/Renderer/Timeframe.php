<?php
/**
 * Add In Mage::
 *
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the EULA that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL: http://add-in-mage.com/support/presales/eula/
 *
 *
 * PROPRIETARY DATA
 * 
 * This file contains trade secret data which is the property of Add In Mage:: Ltd. 
 * This file is submitted to recipient in confidence.
 * Information and source code contained herein may not be used, copied, sold, distributed, 
 * sub-licensed, rented, leased or disclosed in whole or in part to anyone except as permitted by written
 * agreement signed by an officer of Add In Mage:: Ltd.
 * 
 * 
 * MAGENTO EDITION NOTICE
 * 
 * This software is designed for Magento Community edition.
 * Add In Mage:: Ltd. does not guarantee correct work of this extension on any other Magento edition.
 * Add In Mage:: Ltd. does not provide extension support in case of using a different Magento edition.
 * 
 * 
 * @category    AddInMage
 * @package     AddInMage_DexConditions
 * @copyright   Copyright (c) 2013 Add In Mage:: Ltd. (http://www.add-in-mage.com)
 * @license     http://add-in-mage.com/support/presales/eula/  End User License Agreement (EULA)
 * @author      Add In Mage:: Team <team@add-in-mage.com>
 */

class AddInMage_DexConditions_Block_Adminhtml_Promo_Quote_Edit_Tab_Renderer_Timeframe extends Varien_Data_Form_Element_Abstract
{
	protected $_addRowButtonHtml = array ();
    
	public function getElementHtml()
	{
   
		$time_frames = $this->getTimeFrames();
		sort($time_frames);
		
		$html = $this->_getAddRowButtonHtml('timeframes');
				
		$html .= '<ul id="timeframe-sets">';
		
		if ($time_frames) {	
			foreach ($time_frames as $i => $set) {
				$values = array();
				$values['from'] = (isset($set['from'])) ? implode(',', $set['from']) : '00,00,00';
				$values['to'] = (isset($set['to'])) ? implode(',', $set['to']) : '00,00,00';
				$html .= $this->_getRowTemplateHtml($i, $values);
			}
		}
		
		$html .= '</ul>';		
		return $html;
    }
    
    
    
    protected function _getRowTemplateHtml($i = 0, $set)
    {
    	$onclick = "Element.remove($(this).up('li'));";
    	$html  = '<li>';
		$html .= '<div class="set-container" index="'.$i.'">';
		 
		$html .= '<div class="time-container">';
		$html .= '<label>'. Mage::helper('dexconditions')->__('Start Time:') .'</label>';
		$html .= '<span>';
		$html .= trim($this->getTimeContainerHtml('extra_timeframe_'.$i.'_from', array('name' => 'extra[timeframe]['.$i.'][from]', 'value' => $set['from'])));
		$html .= '</span>';
		$html .= '</div>';
		 
		$html .= '<div class="time-container">';
		$html .= '<label>'. Mage::helper('dexconditions')->__('End Time:') .'</label>';
		$html .= '<span>';
		$html .= trim($this->getTimeContainerHtml('extra_timeframe_'.$i.'_to', array('name' => 'extra[timeframe]['.$i.'][to]', 'value' => $set['to'])));
		$html .= '</span>';
		$html .= '</div>';
		$html .= '<p><a href="javascript:void(0)" onclick="'.$onclick.'">' . Mage::helper('dexconditions')->__('Remove time frame') . '</a></p>';
	
		$html .= '</div>';
		$html  .= '</li>';
	
		return $html;
    }
    
    protected function getTimeFrames()
    {
    	$extra = $this->getExtra();
    	return ($extra && isset($extra['timeframe']) && count($extra['timeframe'])) ? $extra['timeframe'] : array();
    }
    
    protected function _getSelected($key, $value)
    {
    	return $this->getData('value/' . $key) == $value ? 'selected="selected"' : '';
    }
    
    protected function getTimeContainerHtml($id, $config)
    {
    	$element = new Varien_Data_Form_Element_Time($config);
    	$element->setId($id);
    	$element->setForm($this->getForm());
    	return $element->toHtml();
    }
    
    protected function _getAddRowButtonHtml($container)
    {
    	if (! isset($this->_addRowButtonHtml [$container])) {
    		$this->_addRowButtonHtml [$container] = Mage::app()->getLayout()->createBlock('adminhtml/widget_button')
    		->setType('button')
    		->setClass('add')
    		->setLabel(Mage::helper('dexconditions')->__('Add Time Frame'))
    		->setOnClick("decAddTimeframeSet();")
    		->toHtml();
    	}
    
    	return $this->_addRowButtonHtml [$container];
    }
}