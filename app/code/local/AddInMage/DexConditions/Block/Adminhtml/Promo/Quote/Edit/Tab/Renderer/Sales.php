<?php
/**
 * Add In Mage::
 *
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the EULA that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL: http://add-in-mage.com/support/presales/eula/
 *
 *
 * PROPRIETARY DATA
 * 
 * This file contains trade secret data which is the property of Add In Mage:: Ltd. 
 * This file is submitted to recipient in confidence.
 * Information and source code contained herein may not be used, copied, sold, distributed, 
 * sub-licensed, rented, leased or disclosed in whole or in part to anyone except as permitted by written
 * agreement signed by an officer of Add In Mage:: Ltd.
 * 
 * 
 * MAGENTO EDITION NOTICE
 * 
 * This software is designed for Magento Community edition.
 * Add In Mage:: Ltd. does not guarantee correct work of this extension on any other Magento edition.
 * Add In Mage:: Ltd. does not provide extension support in case of using a different Magento edition.
 * 
 * 
 * @category    AddInMage
 * @package     AddInMage_DexConditions
 * @copyright   Copyright (c) 2013 Add In Mage:: Ltd. (http://www.add-in-mage.com)
 * @license     http://add-in-mage.com/support/presales/eula/  End User License Agreement (EULA)
 * @author      Add In Mage:: Team <team@add-in-mage.com>
 */

class AddInMage_DexConditions_Block_Adminhtml_Promo_Quote_Edit_Tab_Renderer_Sales extends Varien_Data_Form_Element_Abstract
{
	protected $_addRowButtonHtml = array ();
	
	
	public function getElementHtml()
	{
   
		$sales_conditions = $this->getSalesConditions();
		sort($sales_conditions);
		
		$html = $this->_getAddRowButtonHtml('sales');
				
		$html .= '<ul id="sales-conditions">';
		
		if ($sales_conditions) {	
			foreach ($sales_conditions as $i => $set) {
				$values = array();
				$values['criterion'] = (isset($set['criterion'])) ? $set['criterion'] : '';
				$values['for'] = (isset($set['for'])) ? $set['for'] : 'today';
				$values['interval'] = (isset($set['interval'])) ? $set['interval'] : '';
				$values['condition'] = (isset($set['condition'])) ? $set['condition'] : '==';
				$values['qa'] = (isset($set['qa'])) ? $set['qa'] : '';
				$values['status_criterion'] = (isset($set['status_criterion'])) ? $set['status_criterion'] : 'any';
				$values['statuses'] = (isset($set['statuses'])) ? $set['statuses'] : array();
				if(Mage::helper('dexconditions')->canShowStoreSwitcher()) {
					$values['store_criterion'] = (isset($set['store_criterion'])) ? $set['store_criterion'] : 'any';
					$values['stores'] = (isset($set['stores'])) ? $set['stores'] : Mage::app()->getStore(true)->getWebsiteId();
				}				
				
				$html .= $this->_getRowTemplateHtml($i, $values);
			}
		}
		
		$html .= '</ul>';		
		return $html;
    }
    
    
    
    protected function _getRowTemplateHtml($i = 0, $set)
    {
    	$onclick = "Element.remove($(this).up('li'));";
    	$html  = '<li>';
    	$html .= '<div class="set-container" index="'.$i.'">';
		 
		$html .= '<div class="scondition-container">';
		$html .= '<label></label>';
		$html .= '<span>';
		$html .= trim($this->getCriterionSelectHtml($i, $set['criterion']));
		$html .= '</span>';
		$html .= '</div>';
		 
		if(Mage::helper('dexconditions')->canShowStoreSwitcher()) {
				
			$html .= '<div class="scondition-container lset">';
			$html .= '<span '.$this->getStoreSelectorStyle($set['store_criterion']).' id="stores-lset-'.$i.'" class="lset stores-group"></span>';
			$html .= '<span class="condition-complex">';
			$html .= trim($this->getStoreConditionSelectHtml($i, $set['store_criterion']));
			$html .= '</span>';
			$html .= '</div>';
				
			$html .= '<div class="scondition-container" '.$this->getStoreSelectorStyle($set['store_criterion']).' id="stores-'.$i.'">';
			$html .= '<label></label>';
			$html .= '<span class="condition-complex">';
			$html .= trim($this->getStoresSelectHtml($i, $set['stores']));
			$html .= '</span>';
			$html .= '</div>';
				
		}
		
		$html .= '<div class="scondition-container">';
		$html .= '<label></label>';
		$html .= '<span class="condition-complex">';
		$html .= trim($this->getForSelectHtml($i, $set['for'], $set['interval'])) .''. trim($this->getForInputHtml($i, $set['interval']));
		$html .= '</span>';
		$html .= '</div>';
		
		$html .= '<div class="scondition-container">';
		$html .= '<label></label>';
		$html .= '<span class="condition-complex">';
		$html .= trim($this->getConditionSelectHtml($i, $set['condition'])) .''. trim($this->getConditionInputHtml($i, $set['qa']));
		$html .= '</span>';
		$html .= '</div>';
		
		$html .= '<div class="scondition-container lset">';
		$html .= '<span '.$this->getStatusSelectorStyle($set['status_criterion']).' id="statuses-lset-'.$i.'" class="lset statuses-group"></span>';
		$html .= '<span class="condition-complex">';
		$html .= trim($this->getStatusSelectHtml($i, $set['status_criterion']));
		$html .= '</span>';
		$html .= '</div>';
		
		$html .= '<div class="scondition-container" '.$this->getStatusSelectorStyle($set['status_criterion']).' id="statuses-'.$i.'">';
		$html .= '<label></label>';
		$html .= '<span class="condition-complex">';
		$html .= trim($this->getStatusesSelectHtml($i, $set['statuses']));
		$html .= '</span>';
		$html .= '</div>';
		
		$html .= '<p><a href="javascript:void(0)" onclick="'.$onclick.'">' . Mage::helper('dexconditions')->__('Remove condition') . '</a></p>';
	
		$html .= '</div>';
		$html  .= '</li>';
		return $html;
    }
    
    protected function getStoreConditionSelectHtml($i, $value)
    {
    	$config = array(
    			'name' 		=> 'extra[sales]['.$i.'][store_criterion]',
    			'class'		=> 'validate-select',
    			'values'	=> Mage::helper('dexconditions/types_sales')->getStoreCritValues(),
    			'onchange'	=> 'decHandleStores(this);',
    			'value'		=> $value
    	);
    
    	$element = new Varien_Data_Form_Element_Select($config);
    	$element->setId('extra_sales_'.$i.'_store_criterion');
    	$element->setForm($this->getForm());
    	return $element->toHtml();
    }
    
    protected function getStoresSelectHtml($i, $value)
    {
    	$config = array(
    			'name' 		=> 'extra[sales]['.$i.'][stores]',
    			'class'		=> 'validate-select',
    			'values'	=> Mage::helper('dexconditions/types_sales')->getStoreSwitcherValues(),
    			'value'		=> $value
    	);
    
    	$element = new Varien_Data_Form_Element_Select($config);
    	$element->setId('extra_sales_'.$i.'_stores');
    	$element->setForm($this->getForm());
    	return $element->toHtml();
    }

    
    protected function getStoreSelectorStyle($value)
    {
    	return ($value == 'specified') ? 'style="display:block"' : 'style="display:none"';
    }
    
    protected function getStatusSelectorStyle($value)
    {
    	return ($value == 'specified') ? 'style="display:block"' : 'style="display:none"';
    }
    
    protected function getStatusesSelectHtml($i, $value)
    {

    	$config = array(
    			'name' 		=> 'extra[sales]['.$i.'][statuses]',
    			'class'		=> 'statuses',
    			'values'	=> Mage::helper('dexconditions/types_sales')->getOrderStatuses(),
    			'value'		=> $value,
    	);
    
    	$element = new Varien_Data_Form_Element_Multiselect($config);
    	$element->setId('extra_sales_'.$i.'_statuses');
    	$element->setForm($this->getForm());
    	return $element->toHtml();
    }
    
    protected function getStatusSelectHtml($i, $value)
    {
    	$config = array(
    			'name' 		=> 'extra[sales]['.$i.'][status_criterion]',
    			'class'		=> 'validate-select',
    			'values'	=> Mage::helper('dexconditions/types_sales')->getOrderStatusValues(),
    			'value'		=> $value,
    			'onchange'	=> 'decHandleStatuses(this);'
    	);
    
    	$element = new Varien_Data_Form_Element_Select($config);
    	$element->setId('extra_sales_'.$i.'_status_criterion');
    	$element->setForm($this->getForm());
    	return $element->toHtml();
    }
    
    protected function getCriterionSelectHtml($i, $value)
    {
    	$config = array(
    			'name' 		=> 'extra[sales]['.$i.'][criterion]',
    			'class'		=> 'validate-select',
    			'values'	=> Mage::helper('dexconditions/types_sales')->getCriterionValues(),
    			'value'		=> $value,
    			'onchange'	=> 'decHandleCrit(this);'
    	);
    
    	$element = new Varien_Data_Form_Element_Select($config);
    	$element->setId('extra_sales_'.$i.'_criterion');
    	$element->setForm($this->getForm());
    	return $element->toHtml();
    }
    
    
    protected function getForSelectHtml($i, $value, $interval)
    {
    	$config = array(
    			'name' 		=> 'extra[sales]['.$i.'][for]',
    			'values'	=> Mage::helper('dexconditions/types_sales')->getForValues(),
    			'value'		=> $value,
    			'onchange'	=> 'decHandleFor(this);'
    	);
    
    	if($interval || $interval == '0')
    		$config['class'] = 'criterion';
    		
    		
    	$element = new Varien_Data_Form_Element_Select($config);
    	$element->setId('extra_sales_'.$i.'_for');
    	$element->setForm($this->getForm());
    	return $element->toHtml();
    }
    
    protected function getForInputHtml($i, $value)
    {
    	$config = array(
    			'name' 		=> 'extra[sales]['.$i.'][interval]',
    			'class' 	=> 'criterion',
    			'value'		=> $value,
    	);
    	
    	if($value == '')
    		$config['style'] = 'display:none';
    	
    	
    	$element = new Varien_Data_Form_Element_Text($config);
    	$element->setId('extra_sales_'.$i.'_interval');
    	$element->setForm($this->getForm());
    	return $element->toHtml();
    }
    
    protected function getConditionInputHtml($i, $value)
    {
    	$config = array(
    			'name' 		=> 'extra[sales]['.$i.'][qa]',
    			'class' 	=> 'criterion',
    			'value'		=> $value,
    	);
    
    	$element = new Varien_Data_Form_Element_Text($config);
    	$element->setId('extra_sales_'.$i.'_qa');
    	$element->setForm($this->getForm());
    	return $element->toHtml();
    }
    
    protected function getConditionSelectHtml($i, $value)
    {
    	$config = array(
    			'name' 		=> 'extra[sales]['.$i.'][condition]',
    			'values'	=> Mage::helper('dexconditions/types_sales')->getConditionValues(),
    			'value'		=> $value,
    			'class' 	=> 'criterion'
    	);
    
    	$element = new Varien_Data_Form_Element_Select($config);
    	$element->setId('extra_sales_'.$i.'_condition');
    	$element->setForm($this->getForm());
    	return $element->toHtml();
    }
    
    protected function getSalesConditions()
    {
    	$extra = $this->getExtra();
    	return ($extra && isset($extra['sales']) && count($extra['sales'])) ? $extra['sales'] : array();
    }
   
    
    protected function _getAddRowButtonHtml($container)
    {
    	if (! isset($this->_addRowButtonHtml [$container])) {
    		$this->_addRowButtonHtml [$container] = Mage::app()->getLayout()->createBlock('adminhtml/widget_button')
    		->setType('button')
    		->setClass('add')
    		->setLabel(Mage::helper('dexconditions')->__('Add Condition'))
    		->setOnClick("decAddSsSet();")
    		->toHtml();
    	}
    
    	return $this->_addRowButtonHtml [$container];
    }
}