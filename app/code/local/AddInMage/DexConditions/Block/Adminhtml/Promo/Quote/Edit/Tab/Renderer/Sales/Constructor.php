<?php
/**
 * Add In Mage::
 *
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the EULA that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL: http://add-in-mage.com/support/presales/eula/
 *
 *
 * PROPRIETARY DATA
 * 
 * This file contains trade secret data which is the property of Add In Mage:: Ltd. 
 * This file is submitted to recipient in confidence.
 * Information and source code contained herein may not be used, copied, sold, distributed, 
 * sub-licensed, rented, leased or disclosed in whole or in part to anyone except as permitted by written
 * agreement signed by an officer of Add In Mage:: Ltd.
 * 
 * 
 * MAGENTO EDITION NOTICE
 * 
 * This software is designed for Magento Community edition.
 * Add In Mage:: Ltd. does not guarantee correct work of this extension on any other Magento edition.
 * Add In Mage:: Ltd. does not provide extension support in case of using a different Magento edition.
 * 
 * 
 * @category    AddInMage
 * @package     AddInMage_DexConditions
 * @copyright   Copyright (c) 2013 Add In Mage:: Ltd. (http://www.add-in-mage.com)
 * @license     http://add-in-mage.com/support/presales/eula/  End User License Agreement (EULA)
 * @author      Add In Mage:: Team <team@add-in-mage.com>
 */

class AddInMage_DexConditions_Block_Adminhtml_Promo_Quote_Edit_Tab_Renderer_Sales_Constructor extends Mage_Adminhtml_Block_Catalog_Form_Renderer_Fieldset_Element
{
	/**
	 * Initialize block template
	 */
	protected function _construct()
	{
		$this->setTemplate('addinmage/dexconditions/promo/quote/renderer/sales.phtml');
	}
	
	public function getSalesPastIntHash()
	{
		$values = Mage::helper('dexconditions/types_sales')->getSalesPastIntValues();
	
		$hash = 'var forSalesPastInt = $H({';
	
		$numItems = count($values);
		$i = 0;
		foreach ($values as $key => $value) {
			if(++$i === $numItems)
				$hash .= ''.$key.':\''. $value .'\'';
			else
				$hash .= ''.$key.':\''. $value .'\',';
		}
	
		$hash .= '});';
	
		return $hash;
	}
	
	
	public function getSelectValueHash()
	{
		$values = Mage::helper('dexconditions/types_sales')->getCriterionValuesSource();
		
		$hash = 'var forSalesValue = $H({';
		
		foreach ($values['amount'] as $value) {
			$hash .= ''.$value['value'].':\''. Mage::helper('dexconditions')->__('amount') .'\',';
		}
		
		$numItems = count($values['quantity']);
		$i = 0;
		foreach ($values['quantity'] as $value) {
			if(++$i === $numItems) 			
				$hash .= ''.$value['value'].':\''. Mage::helper('dexconditions')->__('quantity') .'\'';
			else
				$hash .= ''.$value['value'].':\''. Mage::helper('dexconditions')->__('quantity') .'\',';
		}
		
		$hash .= '});';
		
		return $hash;
	}
	
	   
	protected function _getScriptTemplate()
	{
		$onclick = "Element.remove($(this).up('+li+'));";
	
		$html = '<div class="set-container" index="\'+s+\'">';
		 
		$html .= '<div class="scondition-container">';
		$html .= '<label></label>';
		$html .= '<span>';
		$html .= trim($this->getCriterionSelectHtml());
		$html .= '</span>';
		$html .= '</div>';
		
		if(Mage::helper('dexconditions')->canShowStoreSwitcher()) {
			
			$html .= '<div class="scondition-container lset">';
			$html .= '<span style="display:none" id="stores-lset-\'+s+\'" class="lset stores-group"></span>';
			$html .= '<span class="condition-complex">';
			$html .= trim($this->getStoreConditionSelectHtml());
			$html .= '</span>';
			$html .= '</div>';
			
			$html .= '<div class="scondition-container" style="display:none" id="stores-\'+s+\'">';
			$html .= '<label></label>';
			$html .= '<span class="condition-complex">';
			$html .= trim($this->getStoresSelectHtml());
			$html .= '</span>';
			$html .= '</div>';	
			
		}
		 
		$html .= '<div class="scondition-container">';
		$html .= '<label></label>';
		$html .= '<span class="condition-complex">';
		$html .= trim($this->getForSelectHtml()) .''. trim($this->getForInputHtml());
		$html .= '</span>';
		$html .= '</div>';
		
		$html .= '<div class="scondition-container">';
		$html .= '<label></label>';
		$html .= '<span class="condition-complex">';
		$html .= trim($this->getConditionSelectHtml()) .''. trim($this->getConditionInputHtml());
		$html .= '</span>';
		$html .= '</div>';
		
		$html .= '<div class="scondition-container lset">';
		$html .= '<span style="display:none" id="statuses-lset-\'+s+\'" class="lset statuses-group"></span>';
		$html .= '<span class="condition-complex">';
		$html .= trim($this->getStatusSelectHtml());
		$html .= '</span>';
		$html .= '</div>';
		
		$html .= '<div class="scondition-container" style="display:none" id="statuses-\'+s+\'">';
		$html .= '<label></label>';
		$html .= '<span class="condition-complex">';
		$html .= trim($this->getStatusesSelectHtml());
		$html .= '</span>';
		$html .= '</div>';
		
		$html .= '<p><a href="javascript:void(0)" onclick="'.$onclick.'">' . Mage::helper('dexconditions')->__('Remove condition') . '</a></p>';
	
		$html .= '</div>';
	
		return $html;
	}
	
	protected function getStoreConditionSelectHtml()
	{
		$config = array(
				'name' 		=> 'extra[sales][\'+s+\'][store_criterion]',
				'class'		=> 'validate-select',
				'values'	=> Mage::helper('dexconditions/types_sales')->getStoreCritValues(),
				'onchange'	=> 'decHandleStores(this);'
		);
	
		$element = new Varien_Data_Form_Element_Select($config);
		$element->setId('extra_sales_\'+s+\'_store_criterion');
		$element->setForm($this->getElement()->getForm());
		return $element->toHtml();
	}
	
	protected function getStoresSelectHtml()
	{
		$config = array(
				'name' 		=> 'extra[sales][\'+s+\'][stores]',
				'class'		=> 'validate-select',
				'values'	=> Mage::helper('dexconditions/types_sales')->getStoreSwitcherValues()
		);
	
		$element = new Varien_Data_Form_Element_Select($config);
		$element->setId('extra_sales_\'+s+\'_stores');
		$element->setForm($this->getElement()->getForm());
		return $element->toHtml();
	}
	
	protected function getStatusesSelectHtml()
	{
		$config = array(
				'name' 		=> 'extra[sales][\'+s+\'][statuses]',
				'class'		=> 'statuses',
				'values'	=> Mage::helper('dexconditions/types_sales')->getOrderStatuses()
		);
	
		$element = new Varien_Data_Form_Element_Multiselect($config);
		$element->setId('extra_sales_\'+s+\'_statuses');
		$element->setForm($this->getElement()->getForm());
		return $element->toHtml();
	}
	
	protected function getStatusSelectHtml()
	{
		$config = array(
				'name' 		=> 'extra[sales][\'+s+\'][status_criterion]',
				'class'		=> 'validate-select',
				'values'	=> Mage::helper('dexconditions/types_sales')->getOrderStatusValues(),
				'onchange'	=> 'decHandleStatuses(this);'
		);
	
		$element = new Varien_Data_Form_Element_Select($config);
		$element->setId('extra_sales_\'+s+\'_status_criterion');
		$element->setForm($this->getElement()->getForm());
		return $element->toHtml();
	}
	
	
	protected function getCriterionSelectHtml()
	{
		$config = array(
				'name' 		=> 'extra[sales][\'+s+\'][criterion]',
				'class'		=> 'validate-select',
				'values'	=> Mage::helper('dexconditions/types_sales')->getCriterionValues(),						
				'onchange'	=> 'decHandleCrit(this);'
		);
		
		$element = new Varien_Data_Form_Element_Select($config);
		$element->setId('extra_sales_\'+s+\'_criterion');
		$element->setForm($this->getElement()->getForm());
		return $element->toHtml();
	}
	
	
	protected function getForSelectHtml()
	{
		$config = array(
				'name' 		=> 'extra[sales][\'+s+\'][for]',
				'values'	=> Mage::helper('dexconditions/types_sales')->getForValues(),
				'onchange'	=> 'decHandleFor(this);'
		);
	
		$element = new Varien_Data_Form_Element_Select($config);
		$element->setId('extra_sales_\'+s+\'_for');
		$element->setForm($this->getElement()->getForm());
		return $element->toHtml();
	}
	
	protected function getForInputHtml()
	{
		$config = array(
				'name' 		=> 'extra[sales][\'+s+\'][interval]',
				'class' 	=> 'criterion',
				'style'		=> 'display:none'
		);
	
		$element = new Varien_Data_Form_Element_Text($config);
		$element->setId('extra_sales_\'+s+\'_interval');
		$element->setForm($this->getElement()->getForm());
		return $element->toHtml();
	}
	
	protected function getConditionInputHtml()
	{
		$config = array(
				'name' 		=> 'extra[sales][\'+s+\'][qa]',
				'class' 	=> 'criterion'
		);
	
		$element = new Varien_Data_Form_Element_Text($config);
		$element->setId('extra_sales_\'+s+\'_qa');
		$element->setForm($this->getElement()->getForm());
		return $element->toHtml();
	}
	
	protected function getConditionSelectHtml()
	{
		$config = array(
				'name' 		=> 'extra[sales][\'+s+\'][condition]',
				'values'	=> Mage::helper('dexconditions/types_sales')->getConditionValues(),	
				'class' 	=> 'criterion'
		);
	
		$element = new Varien_Data_Form_Element_Select($config);
		$element->setId('extra_sales_\'+s+\'_condition');
		$element->setForm($this->getElement()->getForm());
		return $element->toHtml();
	}
    
    /**
     * Disable field in default value using case
     *
     */
    public function checkFieldDisable()
    {
    	if ($this->canDisplayUseDefault() && $this->usedDefault()) {
    		$this->getElement()->setDisabled(true);
    	}
    	return $this;
    }
}
