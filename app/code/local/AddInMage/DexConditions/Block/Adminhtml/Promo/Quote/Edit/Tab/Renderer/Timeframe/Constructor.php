<?php
/**
 * Add In Mage::
 *
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the EULA that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL: http://add-in-mage.com/support/presales/eula/
 *
 *
 * PROPRIETARY DATA
 * 
 * This file contains trade secret data which is the property of Add In Mage:: Ltd. 
 * This file is submitted to recipient in confidence.
 * Information and source code contained herein may not be used, copied, sold, distributed, 
 * sub-licensed, rented, leased or disclosed in whole or in part to anyone except as permitted by written
 * agreement signed by an officer of Add In Mage:: Ltd.
 * 
 * 
 * MAGENTO EDITION NOTICE
 * 
 * This software is designed for Magento Community edition.
 * Add In Mage:: Ltd. does not guarantee correct work of this extension on any other Magento edition.
 * Add In Mage:: Ltd. does not provide extension support in case of using a different Magento edition.
 * 
 * 
 * @category    AddInMage
 * @package     AddInMage_DexConditions
 * @copyright   Copyright (c) 2013 Add In Mage:: Ltd. (http://www.add-in-mage.com)
 * @license     http://add-in-mage.com/support/presales/eula/  End User License Agreement (EULA)
 * @author      Add In Mage:: Team <team@add-in-mage.com>
 */

class AddInMage_DexConditions_Block_Adminhtml_Promo_Quote_Edit_Tab_Renderer_Timeframe_Constructor extends Mage_Adminhtml_Block_Catalog_Form_Renderer_Fieldset_Element
{
	/**
	 * Initialize block template
	 */
	protected function _construct()
	{
		$this->setTemplate('addinmage/dexconditions/promo/quote/renderer/timeframe.phtml');
	}
	   
	protected function _getScriptTemplate()
	{
		$onclick = "Element.remove($(this).up('+li+'));";
	
		$html = '<div class="set-container" index="\'+i+\'">';
		 
		$html .= '<div class="time-container">';
		$html .= '<label>'. Mage::helper('dexconditions')->__('Start Time:') .'</label>';
		$html .= '<span>';
		$html .= trim($this->getTimeContainerHtml('extra_timeframe_\'+i+\'_from', array('name' => 'extra[timeframe][\'+i+\'][from]')));
		$html .= '</span>';
		$html .= '</div>';
		 
		$html .= '<div class="time-container">';
		$html .= '<label>'. Mage::helper('dexconditions')->__('End Time:') .'</label>';
		$html .= '<span>';
		$html .= trim($this->getTimeContainerHtml('extra_timeframe_\'+i+\'_to', array('name' => 'extra[timeframe][\'+i+\'][to]')));
		$html .= '</span>';
		$html .= '</div>';
		$html .= '<p><a href="javascript:void(0)" onclick="'.$onclick.'">' . Mage::helper('dexconditions')->__('Remove time frame') . '</a></p>';
	
		$html .= '</div>';
	
		return $html;
	}
	
	
	protected function getTimeContainerHtml($id, $config)
	{
		$element = new Varien_Data_Form_Element_Time($config);
		$element->setId($id);
		$element->setForm($this->getElement()->getForm());
		return $element->toHtml();
	}
    
    /**
     * Disable field in default value using case
     *
     */
    public function checkFieldDisable()
    {
    	if ($this->canDisplayUseDefault() && $this->usedDefault()) {
    		$this->getElement()->setDisabled(true);
    	}
    	return $this;
    }
}
