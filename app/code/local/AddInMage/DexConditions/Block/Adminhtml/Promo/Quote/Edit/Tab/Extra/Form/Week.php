<?php
/**
 * Add In Mage::
 *
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the EULA that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL: http://add-in-mage.com/support/presales/eula/
 *
 *
 * PROPRIETARY DATA
 * 
 * This file contains trade secret data which is the property of Add In Mage:: Ltd. 
 * This file is submitted to recipient in confidence.
 * Information and source code contained herein may not be used, copied, sold, distributed, 
 * sub-licensed, rented, leased or disclosed in whole or in part to anyone except as permitted by written
 * agreement signed by an officer of Add In Mage:: Ltd.
 * 
 * 
 * MAGENTO EDITION NOTICE
 * 
 * This software is designed for Magento Community edition.
 * Add In Mage:: Ltd. does not guarantee correct work of this extension on any other Magento edition.
 * Add In Mage:: Ltd. does not provide extension support in case of using a different Magento edition.
 * 
 * 
 * @category    AddInMage
 * @package     AddInMage_DexConditions
 * @copyright   Copyright (c) 2013 Add In Mage:: Ltd. (http://www.add-in-mage.com)
 * @license     http://add-in-mage.com/support/presales/eula/  End User License Agreement (EULA)
 * @author      Add In Mage:: Team <team@add-in-mage.com>
 */

class AddInMage_DexConditions_Block_Adminhtml_Promo_Quote_Edit_Tab_Extra_Form_Week extends AddInMage_DexConditions_Block_Adminhtml_Promo_Quote_Edit_Tab_Extra_Conditions
{
	public function addFormData($form)
	{
		$fieldset = $form->addFieldset('extra_weekly_fieldset',         		
        	array('legend' => Mage::helper('dexconditions')->__('Weekly Conditions'))
        );
		
		$name_prefix = 'extra';
		$yesno = Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray();
		
		$week_action = array(
			array('value' => 1, 'label' => Mage::helper('dexconditions')->__('Apply this discount on specific days')),
		    array('value' => 0, 'label' => Mage::helper('dexconditions')->__('Do not apply this discount on specific days'))
		);
		
		$use_weekly_behavior = $fieldset->addField('use_weekly_behavior', 'select', array(
			'label'     => Mage::helper('dexconditions')->__('Weekly Conditions'),
		    'name'      => $name_prefix.'[use_weekly_behavior]',
		    'note'  	=> Mage::helper('dexconditions')->__('Enable or disable weekly conditions.'),
		    'values' 	=> Mage::getModel('adminhtml/system_config_source_enabledisable')->toOptionArray()
		));
		
		$daily_action = $fieldset->addField('daily_action', 'select', array(
			'label'     => Mage::helper('dexconditions')->__('Action'),
		    'name'      => $name_prefix.'[daily_action]',
		    'note'  	=> Mage::helper('dexconditions')->__('Choose if you want to apply this discount or not on the selected days.'),
		    'values' 	=> $week_action
		));
		
		$action_days = $fieldset->addField('action_days', 'multiselect', array(
			'label'     => Mage::helper('dexconditions')->__('Days'),
		    'name'      => $name_prefix.'[action_days][]',
		    'note'  	=> Mage::helper('dexconditions')->__('Please specify days for the action.'),
		    'values' 	=> Mage::getModel('adminhtml/system_config_source_locale_weekdays')->toOptionArray(),
		    'required'  => true
		));
		
		$dependence = $this->getFormAfter();
		
		$dependence
			->addFieldMap($use_weekly_behavior->getHtmlId(), $use_weekly_behavior->getName())
			->addFieldMap($daily_action->getHtmlId(), $daily_action->getName())
			->addFieldMap($action_days->getHtmlId(), $action_days->getName())
			->addFieldDependence($daily_action->getName(), $use_weekly_behavior->getName(), 1)
			->addFieldDependence($action_days->getName(), $use_weekly_behavior->getName(), 1);
			    
	}
}
