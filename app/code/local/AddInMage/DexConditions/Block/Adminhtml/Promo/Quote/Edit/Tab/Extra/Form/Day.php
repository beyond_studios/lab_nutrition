<?php
/**
 * Add In Mage::
 *
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the EULA that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL: http://add-in-mage.com/support/presales/eula/
 *
 *
 * PROPRIETARY DATA
 * 
 * This file contains trade secret data which is the property of Add In Mage:: Ltd. 
 * This file is submitted to recipient in confidence.
 * Information and source code contained herein may not be used, copied, sold, distributed, 
 * sub-licensed, rented, leased or disclosed in whole or in part to anyone except as permitted by written
 * agreement signed by an officer of Add In Mage:: Ltd.
 * 
 * 
 * MAGENTO EDITION NOTICE
 * 
 * This software is designed for Magento Community edition.
 * Add In Mage:: Ltd. does not guarantee correct work of this extension on any other Magento edition.
 * Add In Mage:: Ltd. does not provide extension support in case of using a different Magento edition.
 * 
 * 
 * @category    AddInMage
 * @package     AddInMage_DexConditions
 * @copyright   Copyright (c) 2013 Add In Mage:: Ltd. (http://www.add-in-mage.com)
 * @license     http://add-in-mage.com/support/presales/eula/  End User License Agreement (EULA)
 * @author      Add In Mage:: Team <team@add-in-mage.com>
 */

class AddInMage_DexConditions_Block_Adminhtml_Promo_Quote_Edit_Tab_Extra_Form_Day extends AddInMage_DexConditions_Block_Adminhtml_Promo_Quote_Edit_Tab_Extra_Conditions
{
	public function addFormData($form, $extra_data)
	{
		$fieldset = $form->addFieldset('extra_daily_fieldset', array(
        		'legend' => Mage::helper('dexconditions')->__('Daily Conditions')
        ));
		
		$name_prefix = 'extra';
		$yesno = Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray();
		
		$day_action = array(
        		array('value' => 1, 'label' => Mage::helper('dexconditions')->__('Apply this discount at specific time intervals')),
        		array('value' => 0, 'label' => Mage::helper('dexconditions')->__('Do not apply this discount at specific time intervals'))
        );
        
        $use_daily_behavior = $fieldset->addField('use_daily_behavior', 'select', array(
        		'label'     => Mage::helper('dexconditions')->__('Daily Conditions'),
        		'name'      => $name_prefix.'[use_daily_behavior]',
        		'note'  	=> Mage::helper('dexconditions')->__('Enable or disable daily conditions.'),
        		'values' 	=> Mage::getModel('adminhtml/system_config_source_enabledisable')->toOptionArray()
        ));
        
        $hourly_action = $fieldset->addField('hourly_action', 'select', array(
        		'label'     => Mage::helper('dexconditions')->__('Action'),
        		'name'      => $name_prefix.'[hourly_action]',
        		'note'  	=> Mage::helper('dexconditions')->__('Choose if you want to apply this discount or not during the specified time intervals.'),
        		'values' 	=> $day_action
        ));
        
        $fieldset->addType('time_frame', 'AddInMage_DexConditions_Block_Adminhtml_Promo_Quote_Edit_Tab_Renderer_Timeframe');
        
        
        $timeframe_conditions = $fieldset->addField('time_frame', 'time_frame', 
        	array(
        			'label'	=> '',
        			'name'	=> 'time_frame'
        ))->setExtra($extra_data)->setRenderer($this->getLayout()->createBlock('dexconditions/adminhtml_promo_quote_edit_tab_renderer_timeframe_constructor'));
		
		$dependence = $this->getFormAfter();
		
		$dependence
			->addFieldMap($use_daily_behavior->getHtmlId(), $use_daily_behavior->getName())
			->addFieldMap($hourly_action->getHtmlId(), $hourly_action->getName())
			->addFieldDependence($hourly_action->getName(), $use_daily_behavior->getName(), 1);
			    
	}
}
