<?php
/**
 * Add In Mage::
 *
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the EULA that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL: http://add-in-mage.com/support/presales/eula/
 *
 *
 * PROPRIETARY DATA
 * 
 * This file contains trade secret data which is the property of Add In Mage:: Ltd. 
 * This file is submitted to recipient in confidence.
 * Information and source code contained herein may not be used, copied, sold, distributed, 
 * sub-licensed, rented, leased or disclosed in whole or in part to anyone except as permitted by written
 * agreement signed by an officer of Add In Mage:: Ltd.
 * 
 * 
 * MAGENTO EDITION NOTICE
 * 
 * This software is designed for Magento Community edition.
 * Add In Mage:: Ltd. does not guarantee correct work of this extension on any other Magento edition.
 * Add In Mage:: Ltd. does not provide extension support in case of using a different Magento edition.
 * 
 * 
 * @category    AddInMage
 * @package     AddInMage_DexConditions
 * @copyright   Copyright (c) 2013 Add In Mage:: Ltd. (http://www.add-in-mage.com)
 * @license     http://add-in-mage.com/support/presales/eula/  End User License Agreement (EULA)
 * @author      Add In Mage:: Team <team@add-in-mage.com>
 */

class AddInMage_DexConditions_Model_Customer_Sales_Report_Order_Updatedat_Collection extends Mage_Sales_Model_Mysql4_Report_Order_Updatedat_Collection
{

    protected $_customer = null;
    
    /**
     * Set customer id
     *
     * @return AddInMage_DexConditions_Model_Customer_Sales_Report_Order_Updatedat_Collection
     */
    public function addCustomerFilter($customerId)
    {
    	$this->_customer = $customerId;
    	return $this;
    }
    
    
    /**
     * Apply customer filter
     *
     * @return AddInMage_DexConditions_Model_Customer_Sales_Report_Order_Updatedat_Collection
     */
    protected function _applyCustomerFilter()
    {
        if (is_null($this->_customer)) {
            return $this;
        }
        $customer = $this->_customer;
        $this->getSelect()->where('customer_id = ?', $customer);
        return $this;
    }

    /**
     * Add selected data
     *
     * @return AddInMage_DexConditions_Model_Customer_Sales_Report_Order_Updatedat_Collection
     */
    protected function _initSelect()
    {
        if ($this->_inited) {
            return $this;
        }

        $columns = $this->_getSelectedColumns();

        $mainTable = $this->getResource()->getMainTable();

        $selectOrderItem = $this->getConnection()->select()
            ->from($this->getTable('sales/order_item'), array(
                'order_id'           => 'order_id',
                'total_qty_ordered'  => 'SUM(qty_ordered - IFNULL(qty_canceled, 0))',
                'total_qty_invoiced' => 'SUM(qty_invoiced)',
            ))
            ->where('parent_item_id IS NULL')
            ->group('order_id');

        $select = $this->getSelect()
            ->from(array('e' => $mainTable), $columns)
            ->join(array('oi' => $selectOrderItem), 'oi.order_id = e.entity_id', array())
            ->where('e.state NOT IN (?)', array(
                    Mage_Sales_Model_Order::STATE_PENDING_PAYMENT,
                    Mage_Sales_Model_Order::STATE_NEW
                ));

        $this->_applyCustomerFilter();
        $this->_applyStoresFilter();
        $this->_applyOrderStatusFilter();

        if ($this->_to !== null) {
            $select->where('DATE(e.updated_at) <= DATE(?)', $this->_to);
        }

        if ($this->_from !== null) {
            $select->where('DATE(e.updated_at) >= DATE(?)', $this->_from);
        }

        if (!$this->isTotals()) {
            $select->group($this->_periodFormat);
        }

        $this->_inited = true;
        return $this;
    }

    /**
     * Load
     *
     * @param boolean $printQuery
     * @param boolean $logQuery
     * @return AddInMage_DexConditions_Model_Customer_Sales_Report_Order_Updatedat_Collection
     */
    public function load($printQuery = false, $logQuery = false)
    {
        if ($this->isLoaded()) {
            return $this;
        }
        $this->_initSelect();
        $this->setApplyFilters(false);
        return parent::load($printQuery, $logQuery);
    }

}
