<?php

/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Groupcat
 */
class Amasty_Groupcat_Model_Observer
{

    public function checkCartAdd(Varien_Event_Observer $observer)
    {
        if ($observer->getEvent()->getControllerAction()->getFullActionName() == "checkout_cart_add") {
            $result    = false;
            $productId = Mage::app()->getRequest()->getParam('product');

            $activeRules = Mage::helper('amgroupcat')->getActiveRulesForProductPrice($productId);
            if ($activeRules) {
                $result = true;
            }
            // check deeper rules (product categories, forbid and etc)
            $productChecker = Mage::getModel('amgroupcat/observerProduct');
            $result         = $result || $productChecker->checkProductRestrictions($observer, $productId, false);

            if ($result) {
                Mage::getSingleton('core/session')->addError('This product is restricted to purchase');
                header("Location: " . $_SERVER['HTTP_REFERER']);
                die();
            }
        }
    }

}