<?php
/**
* @copyright  Copyright (c) 2009 AITOC, Inc. 
*/
class Megnor_Manufacturer_Block_Product_List extends Mage_Catalog_Block_Product_List 
{
    /**
     * Product Collection
     *
     * @var Mage_Eav_Model_Entity_Collection_Abstract
     */
    protected $_productCollection;
    protected $_manufacturer;
    protected $_categories;
  
    public function __construct(){

        $this->_categories = Mage::getModel('catalog/category')->getCategories(8, 0, true, true, true);
    
        $manufacturers = Mage::registry('manufacturer_data');
        if (isset($manufacturers[$this->getRequest()->getParam('id')])){
            $this->_manufacturer = $manufacturers[$this->getRequest()->getParam('id')];
        }
        else {
            $this->_manufacturer = Mage::getModel('manufacturer/manufacturer')->load($this->getRequest()->getParam('id'))
                ->getManufacturerName();
            $manufacturers[$this->getRequest()->getParam('id')] = $this->_manufacturer;
            Mage::register('manufacturer_data', $manufacturers);
        }
    }

    /**
     * Retrieve loaded product collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    protected function _getProductCollection()
    {
        if (is_null($this->_productCollection)) {

            $sort = "manufacturer_prod_order";
            $toolbar = Mage::getBlockSingleton('catalog/product_list_toolbar');

            $collection = Mage::getResourceModel('catalog/product_collection');
            $attributes = Mage::getSingleton('catalog/config')
                ->getProductAttributes();
            //$collection->setDefaultOrder($sort);
            
            $collection->addAttributeToSelect($attributes)
                ->addMinimalPrice()
                ->addFinalPrice()
                ->addTaxPercents()
                ->addStoreFilter();
            $collection->addAttributeToSelect($sort);

            $productIds = Mage::getModel('manufacturer/manufacturer')->getProductsByManufacturer($this->_manufacturer, Mage::app()->getStore()->getId());
            
            
            $collection->addAttributeToFilter(Mage::helper('manufacturer')->getAttributeCode(), array('eq' => $this->_manufacturer), 'left');

            $toolbar->setDefaultOrder($sort);
            $_currentOrder = $toolbar->getCurrentOrder();

            Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
            Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);

            if ($_currentOrder == $sort) {
                /*Add new attribute to set order for manufacturer products*/
                $collection->getSelect()
                ->joinLeft(array('cat_v' => 'catalog_product_entity_varchar'),
                    "(`cat_v`.`attribute_id`='".$this->getAttributeIdbyCode($sort)."') AND (`cat_v`.`entity_id` = `e`.`entity_id`)"
                    ,array('manufacturer_prod_order' => 'cat_v'.'.value'));
                //$collection->addAttributeToSort($sort, 'ASC');

                /*Sort manufacturer products by new attribute and show nulls at the end*/
                $sortBySeason = 'ISNULL(manufacturer_prod_order), CAST(manufacturer_prod_order AS SIGNED) ASC';
                $collection->getSelect()->order(new Zend_Db_Expr($sortBySeason));
            }

            $this->_productCollection = $collection;
        }
        return $this->_productCollection;
    }

    private function getAttributeIdbyCode($attr_code)
    {
        $attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', $attr_code);
        $id = $attribute->getId();
        return $id;
    }
    
    private function ifAttributeExist($attr_code)
    {
        $entity = 'catalog_product';
        $code = $attr_code;
        $attr = Mage::getResourceModel('catalog/eav_attribute')
            ->loadByCode($entity,$code);

        if ($attr->getId()) {
            return true;
        }
        return false;
    }
    
    protected function _toHtml()
    {
        if ($this->_getProductCollection()->count()){
            return parent::_toHtml();
        }
        $custom_message = Mage::getModel('manufacturer/manufacturer')->load($this->getRequest()->getParam('id'))->getCustomMessage();
        $custom_message = '<div class="manufacturer_message">'.$custom_message.'</div>';

        return $custom_message . parent::_toHtml();
    }

    /**
     * Retrieve list of names of objectives
     *
     * @return string
     */

    public function getObjetivos($_product) {
        $objetivos = '';
        $categories = $_product->getCategoryIds();
        $children = $this->_categories;

        foreach ($children as $category) {
            if (in_array($category->getId(), $categories) && $category->getLevel() == 3) {
                $objetivos .=  $category->getName().', ';
            }
        }
        $objetivos = trim($objetivos,', ');
        return $objetivos;
    }    
}
