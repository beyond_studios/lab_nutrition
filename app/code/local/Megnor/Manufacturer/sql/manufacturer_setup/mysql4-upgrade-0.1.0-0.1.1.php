<?php

$installer = $this;

$installer->startSetup();

$installer->getConnection()
  ->addColumn($installer->getTable('manufacturer'),
    'filebanner', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '',
        ), 'File Banner');
 
$installer->getConnection()->addColumn($installer->getTable('manufacturer'),
    'titlebanner', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '',
        ), 'Title Banner');

$this->endSetup();