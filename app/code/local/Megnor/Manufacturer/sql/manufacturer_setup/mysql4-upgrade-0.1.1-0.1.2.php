<?php

$installer = $this;
$installer->startSetup();
 
$installer->getConnection()->addColumn($installer->getTable('manufacturer'),
    'custom_message', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '',
        ), 'Custom Message');

$this->endSetup();