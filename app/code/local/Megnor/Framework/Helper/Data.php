<?php
/**
 * TemplateMela
 * @copyright  Copyright (c) 2010 TemplateMela. (http://www.templatemela.com)
 * @license    http://www.templatemela.com/license/
 */

class Megnor_Framework_Helper_Data extends Mage_Core_Helper_Abstract {
	
	public function isNewProduct($_product) {			
		$now = date("Y-m-d");
		$newsFrom = substr($_product->getData('news_from_date'),0,10);
		$newsTo = substr($_product->getData('news_to_date'),0,10);		
		if(($now >= $newsFrom) && ($now <= $newsTo)){
			return '<div class="new-label">'.$this->__('New').'</div>';
			//return true;
		}else{
			return false;
		}
    }
	
	public function isSpecialProduct($_product) {
		$finalPrice = $_product->getFinalPrice();
		$price =  $_product->getPrice();
		if($finalPrice < $price){
			return '<div class="sale-label">'.$this->__('Sale').'</div>';
			//return true;
		}else{
			return false;
		}
    }
	
}
