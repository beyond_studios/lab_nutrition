<?php

class Megnor_AdvancedMenu_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function isIE6()
    {
        if (!isset($_SERVER['HTTP_USER_AGENT'])) return;
        $userAgent = $_SERVER['HTTP_USER_AGENT'];
        preg_match('/MSIE ([0-9]{1,}[\.0-9]{0,})/', $userAgent, $matches);
        if (!isset($matches[1])) return;
        $version = floatval($matches[1]); #Mage::log($version);
        $flag = false; if( $version <= 6.0 ) $flag = true;
        return $flag;
    }
    public function getTreeCategories($parentId,$productcollection,$all = '',$classIsChild, $isChild,$currentCategoryId )
    {
        $allCats = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('is_active','1')
            ->addAttributeToFilter('include_in_menu','1')
            ->addAttributeToFilter('parent_id',array('eq' => $parentId))
            ->addAttributeToSort('position', 'asc');
      $html = '';
        $class = ($classIsChild) ? "sub-cat-list" : "cat-list";
        $html .= '<ul class="'.$class.'">';
        
        foreach($allCats as $category) {
            
            $subcats = $category->getChildren();
            $subhtml = '';
            if ($isChild) {
                if(!empty($subcats)){
                    $subhtml = $this->getTreeCategories($category->getId(),$productcollection,$all,true , true,$currentCategoryId);
                }
            }
            
            
            $_productCollection =  clone $productcollection;
            $productIdsCollection = $_productCollection->getAllIds();
            
            $_testproductCollection = Mage::getResourceModel('catalog/product_collection')
                                        ->addAttributeToFilter('entity_id',array('in'=>$productIdsCollection))
                                        ->addCategoryFilter($category);
            
            if(Mage::getStoreConfig('cataloginventory/options/show_out_of_stock')=='0')
            {
                //Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($_testproductCollection);
            }

            // Condición para que muestre solo los que tienen más de un producto.
            if (count($_testproductCollection) >= 0) {
                $html .= '<li><div class="product_categories_list_menu">';
                if ((!empty($subhtml))) {
                    if ($subhtml != '<ul class="sub-cat-list"></ul>') {
                        $html .= '<span class="accor"></span>';
                    } else {
                        $html .= '<span class="no-accor"></span>';
                    }
                } else {
                    $html .= '<span class="no-accor"></span>';
                }
                $active = ($category->getId() == $currentCategoryId)? ' active': '';
                $expanded = ($active!='')? ' sub-category-expanded':''; 
                $html .= '<a class="ajaxfilter'.$active.$expanded.'" data-url="' . $category->getUrl() . '"
                             name="cat" value="' . $category->getId() . '"
                             href="' . $category->getUrl() . '">' . $category->getName() . '</a>';
        
                if (!empty($subhtml)) {
                    $html .= $subhtml;
                }
                $todos= '<li><div class="product_categories_list_menu"><span class="no-accor"></span>
                    <a class="tag-todos" href="'.$all['url'].'">Todos</a></div></li>';
                $html .= '</div></li>';
            }
        }
        $html .= $todos.'</ul>';
        return $html;
    }
	
	
	
}
