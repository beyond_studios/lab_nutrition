<?php
/**
 * Magegiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the magegiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPointsExchange
 * @copyright   Copyright (c) 2014 Magegiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * Giantpointsexchange Block
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPointsExchange
 * @author      Magegiant Developer
 */
class Magegiant_GiantPointsExchange_Block_Exchange extends Mage_Core_Block_Template
{

    /**
     * prepare block's layout
     *
     * @return Magegiant_GiantPointsExchange_Block_Giantpointsexchange
     */
    public function isShow()
    {
        return Mage::helper('giantexchange/config')->isEnabled();
    }

    /**
     * get Send point to a friend url
     *
     * @return string
     */
    public function getSendFriendUrl()
    {
        $isSecure = Mage::app()->getStore()->isCurrentlySecure();

        return Mage::getUrl('giantexchange/index/sendfriend', array('_secure' => $isSecure));

    }

    public function getBlockMap()
    {
        $updater = Mage::getModel('giantexchange/updater');
        $result  = array();
        foreach ($updater->getMap() as $action => $blocks) {
            $result[$action] = array_keys($blocks);
        }
        return $result;
    }
}