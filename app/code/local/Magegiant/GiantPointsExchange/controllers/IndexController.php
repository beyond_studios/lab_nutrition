<?php
/**
 * Magegiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the magegiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPointsExchange
 * @copyright   Copyright (c) 2014 Magegiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * GiantPointsExchange Index Controller
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPointsExchange
 * @author      Magegiant Developer
 */
class Magegiant_GiantPointsExchange_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * index action
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Process send point to a friend
     */
    public function sendfriendAction()
    {
        try {
            $friend_email = $this->getRequest()->getParam('friend_email');
            $point_amount = $this->getRequest()->getParam('point_amount');
            $comment      = strip_tags($this->getRequest()->getParam('comment'));
            $result       = array(
                'success'  => false,
                'messages' => array(),
            );
            if (!$friend_email || $friend_email === '') {
                $result['messages'][] = $this->__('You must enter a friend\'s email address to send them points!');

                return $this->_responseAjax($result);
            }

            if (!Mage::helper('giantpoints/validation')->isValidEmail($friend_email)) {
                $result['messages'][] = $this->__('The e-mail addres you entered is invalid.');

                return $this->_responseAjax($result);
            }

            if (!$point_amount || $point_amount <= 0) {
                $result['messages'][] = $this->__('You must enter a valid number of points to send your friend.');

                return $this->_responseAjax($result);
            }

            $friend             = Mage::getModel('customer/customer')->setWebsiteId(Mage::app()->getWebsite()->getId())->loadByEmail($friend_email);
            $sender             = Mage::helper('giantpoints/customer')->getCustomer();
            $points_sent_string = Mage::helper('giantpoints')->addLabelForPoint($point_amount);
            //Verify the sender has enough points of given type.
            if (!$sender->getId()) {
                $result['messages'][] = $this->__('You must log in or sign up before sending points to a friend!');

                return $this->_responseAjax($result);
            }
            $rewardAccount = Mage::helper('giantpoints/customer')->getAccount();
            $balance       = $rewardAccount->getBalance();
            if (!$balance) {
                $balance = 0;
            }
            $exchangData = array(
                'sender_id'    => $rewardAccount->getId(),
                'friend_email' => $friend_email,
                'point_amount' => $point_amount,
                'comment'      => $comment,
            );
            if ($point_amount > $balance) {
                $result['messages'][]  = $this->__('You cannot send %s to your friend when you only have %s!',
                    $points_sent_string,
                    Mage::helper('giantpoints')->addLabelForPoint($balance));
                $exchangData['notice'] = Mage::helper('core')->jsonEncode($result['messages']);
                $this->_updateExchange($exchangData, false);

                return $this->_responseAjax($result);
            }

            //Verify if friend exists. If not and option enabled in configuration an invitation email to the store is sent to the user
            $friend_id = $friend->getId();
            if (!$friend_id) {
                if (Mage::helper('giantexchange/config')->allowSendEmailToUnregister($sender->getStoreId())) {
                    if (!Mage::getModel('giantexchange/exchange')->notifyFriendEmail($sender, $friend_email, $points_sent_string, $comment, true)) {
                        Mage::helper('giantpoints')->log($this->__("Error sending invitation email to %s to register an account and never miss points again (%s from %s (%s)).", $friend_email, $points_sent_string, $sender->getName(), $sender->getEmail()));
                        $result['messages'][] = $this->__("There is no customer with that email address (%s).", $friend_email);
                    } else {
                        $result['messages'][] = $this->__("There is no customer with that email address (%s). An invitation to the store was sent, but no points.", $friend_email);
                    }
                } else {
                    $result['messages'][] = $this->__("There is no customer with that email address (%s).", $friend_email);
                }
                $exchangData['notice'] = Mage::helper('core')->jsonEncode($result['messages']);
                $this->_updateExchange($exchangData, false);

                return $this->_responseAjax($result);
            }

            if ($friend_email == $sender->getEmail()) {
                $result['messages'][]  = $this->__("You cannot send points to yourself!!");
                $exchangData['notice'] = Mage::helper('core')->jsonEncode($result['messages']);
                $this->_updateExchange($exchangData, false);

                return $this->_responseAjax($result);
            }

            if (!$comment || empty ($comment)) {
                $comment = '';
            }

            $is_transfer_successful = $this->_transferPointsToFriend($point_amount, $friend_id, $comment);
            if ($is_transfer_successful) {
                $result['success']    = true;
                $result['messages'][] = $this->__('You have successfully sent %s to %s!', $points_sent_string, $friend->getName());
                if (!Mage::getModel('giantexchange/exchange')->notifyFriendEmail($sender, $friend, $points_sent_string, $comment)) {
                    Mage::helper('giantpoints')->log(Mage::helper('giantpoints')->__("Error sending email notification to %s (%s) for receiving %s points from %s (%s).", $friend->getName(), $friend->getEmail(), $points_sent_string, $sender->getName(), $sender->getEmail()));
                    $exchangData['notice'] = Mage::helper('giantpoints')->__("Error sending email notification to %s (%s) for receiving %s points from %s (%s).", $friend->getName(), $friend->getEmail(), $points_sent_string, $sender->getName(), $sender->getEmail());
                }
                $layout               = Mage::app()->getLayout();
                $fullTargetActionName = 'giantpoints_index_index';
                $result['blocks']     = $this->getUpdater()->getBlocks($layout, $fullTargetActionName);
                $this->_updateExchange($exchangData);

                return $this->_responseAjax($result);
            } else {
                $result['messages'][] = $this->__('Points could not be sent to your friend.');
            }
        } catch (Exception $ex) {
            $result['messages'][]  = $this->__($ex->getMessage());
            $exchangData['notice'] = Mage::helper('core')->jsonEncode($result['messages']);
            $this->_updateExchange($exchangData);
        }

    }

    public function getUpdater()
    {
        return Mage::getSingleton('giantexchange/updater');
    }

    protected function _responseAjax($result)
    {
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    /**
     * @param $point_amount
     * @param $friend_id
     * @param $comment
     */
    protected function _transferPointsToFriend($point_amount, $friend_id, $comment)
    {
        $receiver     = Mage::getModel('customer/customer')->load($friend_id);
        $sender       = Mage::helper('giantpoints/customer')->getCustomer();
        $obj          = new Varien_Object(array(
            'point_amount' => $point_amount,
            'comment'      => $comment,
            'receiver'     => $receiver->getEmail(),
            'sender'       => $sender->getEmail()
        ));
        $receiverData = array(
            'customer'      => $receiver,
            'action_object' => $obj,
            'notice'        => null,
        );
        $senderData   = array(
            'customer'      => $sender,
            'action_object' => $obj,
            'notice'        => null,
        );
        try {
            Mage::helper('giantpoints/action')->createTransaction(
                'exchange_send', $senderData
            );
            Mage::helper('giantpoints/action')->createTransaction(
                'exchange_receive', $receiverData
            );

        } catch (Exception $e) {
            Mage::helper('giantpoints')->log($e->getMessage());
        }

        return true;

    }

    /**
     * update exchange table data
     *
     * @param      $exchageData
     * @param bool $status
     */
    protected function _updateExchange($exchageData, $status = true)
    {
        if (!Mage::helper('giantexchange/config')->isEnabledExchangeLog())
            return $this;
        Mage::getModel('giantexchange/exchange')->updateExchange($exchageData, $status);

        return $this;
    }
}