<?php
/**
 * MageGiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageGiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsExchange
 * @copyright   Copyright (c) 2014 MageGiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * Giantpointsexchange Model
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsExchange
 * @author      MageGiant Developer
 */
class Magegiant_GiantPointsExchange_Model_Exchange extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('giantexchange/exchange');
    }

    /**
     * @param        $customer
     * @param        $friend
     * @param        $pointsString
     * @param string $personal_comment
     * @param bool   $invitation
     * @return mixed
     */
    public function notifyFriendEmail($customer, $friend, $pointsString, $personal_comment = '', $invitation = false)
    {
        /* @var $translate Mage_Core_Model_Translate */
        $translate = Mage::getSingleton('core/translate');
        $translate->setTranslateInline(false);
        /* @var $email Mage_Core_Model_Email_Template */
        $email = Mage::getModel('core/email_template');

        $sender = Mage::helper('giantpoints/config')->getEmailConfig('sender', $customer->getStoreId());

        $email->setDesignConfig(array(
                'area'  => 'frontend',
                'store' => $customer->getStoreId())
        );

        if ($invitation) {
            $vars     = array(
                'customer_name'  => $customer->getName(),
                'customer_email' => $customer->getEmail(),
                'friend_email'   => $friend,
                'store_name'     => $customer->getStore()->getName(),
                'point_amount'   => $pointsString,
                'comment'        => $personal_comment
            );
            $template = Mage::helper('giantexchange/config')->getInvitationEmailTemplate($customer->getStoreId());
            $email->sendTransactional($template, $sender, $friend, '', $vars);

        } else {
            $vars     = array(
                'customer_name'  => $customer->getName(),
                'customer_email' => $customer->getEmail(),
                'friend_name'    => $friend->getName(),
                'friend_email'   => $friend->getEmail(),
                'store_name'     => $customer->getStore()->getName(),
                'point_amount'   => $pointsString,
                'comment'        => $personal_comment
            );
            $template = Mage::helper('giantexchange/config')->getNotificationEmailTemplate($customer->getStoreId());
            try {
                $email->sendTransactional($template, $sender, $friend->getEmail(), $friend->getName(), $vars);
            } catch (Exception $e) {
            }
        }

        $translate->setTranslateInline(true);

        return $email->getSentSuccess();
    }

    /**
     * update into giant_points_exchagen table
     *
     * @param      $data
     * @param bool $status
     */
    public function updateExchange($data, $status = true)
    {
        $this->setData($data)
            ->setCreatedAt(now());
        if ($status) {
            $this->setStatus(Magegiant_GiantPointsExchange_Model_Status::STATUS_COMPLETE);
        } else {
            $this->setStatus(Magegiant_GiantPointsExchange_Model_Status::STATUS_FAIL);
        }
        try {
            $this->save();
        } catch (Exception $e) {
            Mage::helper('giantpoints')->log($e->getMessage());
        }

        return $this;
    }

}