<?php
/**
 * MageGiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageGiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsExchange
 * @copyright   Copyright (c) 2014 MageGiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/** @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/**
 * create giantpointsexchange table
 */
$installer->run("

DROP TABLE IF EXISTS {$this->getTable('giantexchange/exchange')} ;
CREATE TABLE {$this->getTable('giantexchange/exchange')} (
  `exchange_id` int(11) unsigned NOT NULL auto_increment,
  `sender_id` int(10) unsigned NOT NULL ,
  `friend_email` varchar(255) NOT NULL default '',
  `point_amount` int(11) unsigned NOT NULL ,
  `comment` text NOT NULL default '',
  `notice`  text NOT NULL default '',
  `status` smallint(5) NOT NULL default '0',
  `created_at` datetime NULL,
  KEY `FK_GIANTPOINTS_EXCHANGE_SENDER_ID` (`sender_id`),
  CONSTRAINT `FK_GIANTPOINTS_EXCHANGE_SENDER_ID` FOREIGN KEY (`sender_id`) REFERENCES {$this->getTable('giantpoints/customer')} (`reward_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY (`exchange_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

");

$installer->endSetup();

