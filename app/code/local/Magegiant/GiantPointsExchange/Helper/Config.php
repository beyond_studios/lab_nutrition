<?php
/**
 * MageGiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageGiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsExchange
 * @copyright   Copyright (c) 2014 MageGiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * GiantPointsExchange Helper
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsExchange
 * @author      MageGiant Developer
 */
class Magegiant_GiantPointsExchange_Helper_Config extends Mage_Core_Helper_Abstract
{
    const XML_PATH_ENABLED         = 'giantpoints/exchange/is_enabled';
    const XML_PATH_EXCHANGE_CONFIG = 'giantpoints/exchange/';

    /**
     * check is enabled customer exchagen or not
     *
     * @param null $storeId
     * @return mixed
     */
    public function isEnabled($storeId = null)
    {
        if (!$storeId) {
            $storeId = Mage::app()->getStore()->getId();
        }

        return Mage::getStoreConfigFlag(self::XML_PATH_ENABLED, $storeId);
    }

    /**
     * @param      $name
     * @param null $storeId
     * @return mixed
     */
    public function getExchangeConfig($name, $storeId = null)
    {
        if (!$storeId) {
            $storeId = Mage::app()->getStore()->getId();
        }

        return Mage::getStoreConfig(self::XML_PATH_EXCHANGE_CONFIG . $name, $storeId);

    }

    /**
     * @param null $store
     * @return mixed
     */
    public function allowSendEmailToUnregister($store = null)
    {
        return $this->getExchangeConfig('notification_customer_unregister', $store);
    }

    /**
     * get email template send to unregister customer
     *
     * @param null $store
     * @return mixed
     */
    public function getInvitationEmailTemplate($store = null)
    {
        return $this->getExchangeConfig('notification_customer_unregister_email_template', $store);
    }

    /**
     * @param null $store
     * @return mixed
     */
    public function getNotificationEmailTemplate($store = null)
    {
        return $this->getExchangeConfig('notification_send_friend_success_email_template', $store);
    }

    /**
     *
     * @param null $store
     * @return mixed
     */
    public function isEnabledExchangeLog($store = null)
    {
        return $this->getExchangeConfig('is_enabled_log', $store);
    }
}