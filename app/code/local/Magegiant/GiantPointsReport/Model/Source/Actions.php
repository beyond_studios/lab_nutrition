<?php
/**
 * MageGiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageGiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPoints
 * @copyright   Copyright (c) 2014 MageGiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * GiantPointsReport Mysql4 Transaction Collection Model
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPointsReport
 * @author      Magegiant Developer
 */
class Magegiant_GiantPointsReport_Model_Source_Actions
{
	/**
	 * Options getter
	 *
	 * @return array
	 */
	public function toOptionArray()
	{
		$array = array();
		foreach ($this->toArray() as $value => $label) {
			$array[] = array('value' => $value, 'label' => $label);
		}

		return $array;
	}

	/**
	 * Get options in "key-value" format
	 *
	 * @return array
	 */
	public function toArray()
	{
		$helper  = Mage::helper('giantpointsreport');
		$actions = array(
			'change_by_admin' => $helper->__('Admin'),
			'order_invoiced'  => $helper->__('Purchase Order')
		);

		if ($helper->isPluginEnabled('Magegiant_GiantPointsBehavior')) {
			$behaviorActions = array(
				'customer_register'            => $helper->__('Sign-up'),
				'customer_subscription'        => $helper->__('Newsletter'),
				'review_approved'              => $helper->__('Review'),
				'customer_tag_product'         => $helper->__('Product Tag'),
				'customer_participate_in_poll' => $helper->__('Taking Poll'),
				'customer_send_friend'         => $helper->__('Send Friend'),
				'customer_birthday'            => $helper->__('Birthday'),
				'google_on'                    => $helper->__('Google +1'),
				'facebook_like'                => $helper->__('Facebook Like'),
				'tweet'                        => $helper->__('Tweet'),
				'purchase_facebook_share'      => $helper->__('FB Share Purchase'),
				'purchase_twitter_tweet'       => $helper->__('Tweet Purchase'),
			);
			$actions         = array_merge($actions, $behaviorActions);
		}

		if ($helper->isPluginEnabled('Magegiant_GiantPointsRefer')) {
			$referActions = array(
				'customer_referral_signup' => $helper->__('Referral Sign-up'),
				'invitee_invoiced'         => $helper->__('Invited Earn'),
				'referral_invoiced'        => $helper->__('Referral Earn'),
				'milestone_referrals'      => $helper->__('Milestone Referrals')
			);
			$actions      = array_merge($actions, $referActions);
		}

		if ($helper->isPluginEnabled('Magegiant_GiantPointsExchange')) {
			$exchangeActions = array(
				'exchange_receive' => $helper->__('Transfer Points')
			);
			$actions         = array_merge($actions, $exchangeActions);
		}

		if ($helper->isPluginEnabled('Magegiant_GiantPointsCoupon')) {
			$couponActions = array(
				'coupon' => $helper->__('Redeem Coupon')
			);
			$actions       = array_merge($actions, $couponActions);
		}

		return $actions;
	}
}