<?php
/**
 * MageGiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageGiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPoints
 * @copyright   Copyright (c) 2014 MageGiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * GiantPointsReport Mysql4 Transaction Collection Model
 * 
 * @category    Magegiant
 * @package     Magegiant_GiantPointsReport
 * @author      Magegiant Developer
 */
class Magegiant_GiantPointsReport_Model_Mysql4_Transaction_Collection extends Magegiant_GiantPoints_Model_Mysql4_Transaction_Collection
{
    protected $_isGroupSql = false;
    protected $_earnTotals;
    protected $_spentTotals;
    protected $_moneySpentTotals;

    public function getEarnTotals() {
        if (!$this->_earnTotals)
            $this->prepareEarningSpendingTotal();
        return $this->_earnTotals;
    }

    public function getSpentTotals() {
        if (!$this->_spentTotals)
            $this->prepareEarningSpendingTotal();
        return $this->_spentTotals;
    }

    public function setIsGroupCountSql($value) {
        $this->_isGroupSql = $value;
        return $this;
    }
    
    public function addCreateDayFilter($date) {
        $day = date('Y-m-d', $date);
        $this->getSelect()->where('date(created_time) = ?', $day);
        return $this;
    }

    
    /**
     * prepare Spend Point, Earn Point, money spent total
     * 
     * @return Magegiant_GiantPointsReport_Model_Mysql4_Transaction_Collection
     */
    public function prepareEarningSpendingTotal() {
        if ($storeId = Mage::app()->getRequest()->getParam('store'))
            $this->addFieldToFilter('store_id', $storeId);
        $this->getSelect()->reset(Zend_Db_Select::COLUMNS);
        $this->getSelect()
                //->group('action_type')
                ->columns(array(
                    'earned' => 'SUM(IF( action_type = 2 OR (action_type = 3 AND point_amount > 0) OR (action_type = 4 AND point_amount < 0), 0, point_amount))',
                    'spent' => '-SUM(IF( action_type = 2 OR (action_type = 3 AND point_amount > 0) OR (action_type = 4 AND point_amount < 0), point_amount, 0))',
                ));
        
        if($datas = $this->getFirstItem()){
            $this->_earnTotals  = $datas->getEarned();
            $this->_spentTotals = $datas->getSpent();
        }
        return $this;
    }
    
    
    /**
     * prepare data
     * 
     * @return Magegiant_GiantPointsReport_Model_Mysql4_Transaction_Collection
     */
    public function prepareCostLoyaltyMember(){
        // Add Store Filter Data
        if ($storeId = Mage::app()->getRequest()->getParam('store'))
            $this->addFieldToFilter('store_id', $storeId);
        $this->addFieldToFilter('main_table.status', array(
                'neq' => Magegiant_GiantPoints_Model_Transaction::STATUS_CANCELED
            ));
        $this->addFieldToFilter('order_id', array('notnull' => true));
        $this->getSelect()->reset(Zend_Db_Select::COLUMNS)
                ->columns(array(
                    'customer_id' => 'customer_id',
                    'order_base_amount' => 'order_base_amount',
                ))->group('order_id')
                ->where('action_type = ?', 2);
        $view = clone $this->getSelect();
        $this->getSelect()->reset()
                ->from(array('main_table' => new Zend_Db_Expr('(' . $view->__toString() . ')')), array())
                ->columns(array(
                    'customer_id' => 'customer_id',
                    'money_spent' => 'SUM(order_base_amount)',
                ))
                ->group('customer_id');
        $customer_number = 0;
        $money_spent_totals = 0;
        foreach ($this as $row){
            $money_spent_totals += $row->getMoneySpent();
            $customer_number++;
        }

        return New Varien_Object(array('customer_number'=>$customer_number, 'money_spent_totals'=>$money_spent_totals));
    }
    /**
     * prepare data for calc average Average value of money per point
     * 
     * @return Magegiant_GiantPointsReport_Model_Mysql4_Transaction_Collection
     */
    public function prepareAverageValuePerPoint(){
        // Add Store Filter Data
        if ($storeId = Mage::app()->getRequest()->getParam('store'))
            $this->addFieldToFilter('store_id', $storeId);
        $this->addFieldToFilter('main_table.status', array(
                'neq' => Magegiant_GiantPoints_Model_Transaction::STATUS_CANCELED
            ));
        $this->addFieldToFilter('order_id', array('notnull' => true));
        $this->getSelect()->reset(Zend_Db_Select::COLUMNS)
                ->columns(array(
                    'points_spent' => '-SUM(point_amount)',
                    'money_spent' => 'order_base_amount',
                ))
                ->where('action_type = ?', 2);
        return $this;
    }

    /**
     * prepare data helper for Point earned / points spend
     * 
     * @param type $range
     * @param type $customStart
     * @param type $customEnd
     * @return Magegiant_GiantPointsReport_Model_Mysql4_Transaction_Collection
     */
    public function prepareSummaryEarnspend($range, $customStart, $customEnd) {
        $this->addFieldToFilter('status', array(
                'neq' => Magegiant_GiantPoints_Model_Transaction::STATUS_CANCELED
            ));

        $this->getSelect()
            ->reset(Zend_Db_Select::COLUMNS)
            ->columns(array(
				'earn' => 'SUM(IF( action_type = 2 OR (action_type = 3 AND point_amount > 0) OR (action_type = 4 AND point_amount < 0), 0, point_amount))',
				'spend' => '-SUM(IF( action_type = 2 OR (action_type = 3 AND point_amount > 0) OR (action_type = 4 AND point_amount < 0), point_amount, 0))',
            ));
        $dateRange = Mage::getResourceSingleton('reports/order_collection')->getDateRange($range, $customStart, $customEnd);
		$tzRangeOffsetExpression = $this->_getTZRangeOffsetExpression(
			$range, 'change_date', $dateRange['from'], $dateRange['to']
		);
		$this->getSelect()
            ->columns(array(
                'range' => $tzRangeOffsetExpression
            ))
            ->order('range', Zend_Db_Select::SQL_ASC)
            ->group($tzRangeOffsetExpression);
        $this->addFieldToFilter('change_date', $dateRange);
        return $this;
    }

    /**
     * prepare number loyal members
     * 
     * @param type $range
     * @param type $customStart
     * @param type $customEnd
     * @return Magegiant_GiantPointsReport_Model_Mysql4_Transaction_Collection
     */
	public function prepareSummaryLoyalty($range, $customStart, $customEnd)
	{
		$this->setMainTable('giantpoints/transaction');
		$this->getSelect()->reset(Zend_Db_Select::COLUMNS);

		$dateRange = Mage::getResourceSingleton('reports/order_collection')->getDateRange($range, $customStart, $customEnd);

		$tzRangeOffsetExpression = $this->_getTZRangeOffsetExpression(
			$range, 'change_date', $dateRange['from'], $dateRange['to']
		);
		$this->getSelect()
			->columns(array(
				'range' => $tzRangeOffsetExpression,
				'total' => 'COUNT(DISTINCT customer_id)'
			))
			->where('main_table.action_type = 2')
			->order('range', Zend_Db_Select::SQL_ASC)
			->group($tzRangeOffsetExpression);

		$this->addFieldToFilter('change_date', $dateRange);

		return $this;
	}
	protected function _getTZRangeOffsetExpression($range, $attribute, $from = null, $to = null)
	{
		return str_replace(
			'{{attribute}}',
			Mage::getResourceModel('sales/report_order')
				->getStoreTZOffsetQuery($this->getMainTable(), $attribute, $from, $to),
			$this->_getRangeExpression($range)
		);
	}
	protected function _getRangeExpression($range)
	{
		switch ($range)
		{
			case '24h':
				$expression = $this->getConnection()->getConcatSql(array(
					$this->getConnection()->getDateFormatSql('{{attribute}}', '%Y-%m-%d %H:'),
					$this->getConnection()->quote('00')
				));
				break;
			case '7d':
			case '1m':
				$expression = $this->getConnection()->getDateFormatSql('{{attribute}}', '%Y-%m-%d');
				break;
			case '1y':
			case '2y':
			case 'custom':
			default:
				$expression = $this->getConnection()->getDateFormatSql('{{attribute}}', '%Y-%m');
				break;
		}

		return $expression;
	}
}