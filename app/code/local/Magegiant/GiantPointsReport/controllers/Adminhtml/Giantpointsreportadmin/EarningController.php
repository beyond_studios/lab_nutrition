<?php
/**
 * MageGiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageGiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPoints
 * @copyright   Copyright (c) 2014 MageGiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * GiantPoints Earned Report Controller
 * 
 * @category    Magegiant
 * @package     Magegiant_GiantPointsReport
 * @author      Magegiant Developer
 */
class Magegiant_GiantPointsReport_Adminhtml_Giantpointsreportadmin_EarningController extends Mage_Adminhtml_Controller_Action
{    
    /**
     * index Action
     */
    public function indexAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('giantpoints/report/earning')
            ->_addBreadcrumb(
                $this->__('Reward Points'),
                $this->__('Reward Points')
            );
        $this->_title($this->__('Reward Points'))
            ->_title($this->__('Report'))
            ->_title($this->__('Earned Points'));
        
        $this->getLayout()->getBlock('giantpointsreport.earning')
            ->setTitle($this->__('Earned Points Report'));
        
        $this->renderLayout();
    }
    
    /**
     * grid Action
     */
    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
    
    /**
     * export grid items to CSV file
     */
    public function exportCsvAction()
    {
        $fileName   = 'earningreport.csv';
        $content    = $this->getLayout()
                           ->createBlock('giantpointsreport/adminhtml_earning_grid')
                           ->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export grid items to XML file
     */
    public function exportXmlAction()
    {
        $fileName   = 'earningreport.xml';
        $content    = $this->getLayout()
                           ->createBlock('giantpointsreport/adminhtml_earning_grid')
                           ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }
    
    /**
     * export grid items to XML Excel file
     */
    public function exportExcelAction()
    {
        $fileName   = 'earningreport.xml';
        $content    = $this->getLayout()
                           ->createBlock('giantpointsreport/adminhtml_earning_grid')
                           ->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }
    
    /**
     * get allowed report
     * 
     * @return boolean
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('giantpoints/report');
    }
}
