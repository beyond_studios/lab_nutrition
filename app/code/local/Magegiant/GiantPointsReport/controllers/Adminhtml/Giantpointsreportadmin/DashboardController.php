<?php
/**
 * MageGiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageGiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPoints
 * @copyright   Copyright (c) 2014 MageGiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * GiantPointsreport Adminhtml Dashboard Controller
 * 
 * @category    Magegiant
 * @package     Magegiant_GiantPointsReport
 * @author      Magegiant Developer
 */
class Magegiant_GiantpointsReport_Adminhtml_DashboardController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('giantpoints/report')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Reward Points Report'), Mage::helper('adminhtml')->__('Reward Points Report'));
		return $this;
	}
 
	public function indexAction() {
		$this->_initAction();
        $this->_title($this->__('Reward Points Report'))
			->_title($this->__('Reward Points Report'));
        $this->renderLayout();
	}
	public function ajaxBlockAction()
	{
		$output   = '';
		$blockTab = $this->getRequest()->getParam('block');
		if (in_array($blockTab, array('tabs_loyalty', 'tabs_earnspend'))) {
			$output = $this->getLayout()->createBlock('giantpointsreport/adminhtml_dashboard_' . $blockTab)->toHtml();
		}
		$this->getResponse()->setBody($output);
		return;
	}

	public function tunnelAction()
	{
		$httpClient = new Varien_Http_Client();
		$gaData = $this->getRequest()->getParam('ga');
		$gaHash = $this->getRequest()->getParam('h');
		if ($gaData && $gaHash) {
			$newHash = Mage::helper('adminhtml/dashboard_data')->getChartDataHash($gaData);
			if ($newHash == $gaHash) {
				if ($params = unserialize(base64_decode(urldecode($gaData)))) {
					$response = $httpClient->setUri(Mage_Adminhtml_Block_Dashboard_Graph::API_URL)
						->setParameterGet($params)
						->setConfig(array('timeout' => 5))
						->request('GET');

					$headers = $response->getHeaders();

					$this->getResponse()
						->setHeader('Content-type', $headers['Content-type'])
						->setBody($response->getBody());
				}
			}
		}
	}
}