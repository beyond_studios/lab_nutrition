<?php
/**
 * Magegiant
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the magegiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magegiant
 * @package     Magegiant_GiantpointsReport
 * @copyright   Copyright (c) 2014 Magegiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * Giantpointsreport Adminhtml Block
 * 
 * @category    Magegiant
 * @package     Magegiant_GiantpointsReport
 * @author      Magegiant Developer
 */
class Magegiant_GiantPointsReport_Block_Adminhtml_Dashboard extends Mage_Adminhtml_Block_Template
{
	protected $_hasData = false;
	protected $_dataHelperName = null;
	public function __construct(){
		parent::__construct();
		$this->setTemplate('magegiant/giantpointsreport/dashboard.phtml');
	}

	public function getSwitchUrl(){
		if ($url = $this->getData('switch_url'))
			return $url;
		return $this->getUrl('*/*/*', array('_current'=>true, 'period'=>null));
	}

	public function isHasData(){
		return $this->_hasData;
	}
	public function getDataHelper(){
		return Mage::helper($this->_dataHelperName);
	}

	protected function _prepareData(){
		$availablePeriods = array_keys($this->helper('adminhtml/dashboard_data')->getDatePeriods());
		$period = $this->getRequest()->getParam('period');

		$this->getDataHelper()->setParam('period',
			($period && in_array($period, $availablePeriods)) ? $period : '7d'
		);
	}

	public function getHeadTitle(){
		return '';
	}
	/**
	 * get comment content for dashboard
	 *
	 * @return string
	 */
	public function getCommentContent() {
		return '';
	}
}