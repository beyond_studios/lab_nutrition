<?php
/**
 * MageGiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageGiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPoints
 * @copyright   Copyright (c) 2014 MageGiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * GiantPoints Report Earned Grid Block
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPointsReport
 * @author      Magegiant Developer
 */
class Magegiant_GiantPointsReport_Block_Adminhtml_Earning_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('earningreportGrid');
        $this->setDefaultSort('period');
        $this->setDefaultDir('DESC');
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(true);
        $this->_prepareDateTypeReport();
    }

    /**
     * Prepare date range type to report
     *
     * @return Magegiant_GiantPointsReport_Block_Adminhtml_Earned_Grid
     */
    public function _prepareDateTypeReport()
    {
        $filter = Mage::app()->getRequest()->getParam('filter', null);
        if (is_string($filter)) {
            $data = Mage::helper('adminhtml')->prepareFilterString($filter);
            if (isset($data['report_period'])) {
                $this->setPeriodType($data['report_period']);
            }
        }
        switch ($this->getPeriodType()) {
            case 'month':
                $this->setPeriodFormat("DATE_FORMAT(change_date, '%Y-%m')");
                break;
            case 'year':
                $this->setPeriodFormat("DATE_FORMAT(change_date, '%Y')");
                break;
            default :
                $this->setPeriodFormat("DATE_FORMAT(change_date, '%Y-%m-%d')");
                break;
        }

        return $this;
    }

    /**
     * prepare collection for block to display
     *
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('giantpoints/transaction_collection')
            ->addFieldToFilter('main_table.status', array(
                'neq' => Magegiant_GiantPoints_Model_Transaction::STATUS_CANCELED
            ));

        // Add Store Filter Data
        if ($this->_getStore()->getId()) {
            $collection->addFieldToFilter('main_table.store_id', $this->_getStore()->getId());
        }

        // Prepare collection columns
        $actions       = array_keys(Mage::helper('giantpointsreport')->getEarningActions());
        $earnedActions = implode("','", $actions);
        $columns       = array(
            'period' => $this->getPeriodFormat(),
            'total'  => "SUM(IF( action_type = 2 OR (action_type = 3 AND point_amount > 0) OR (action_type = 4 AND point_amount < 0), 0, point_amount))"
        );
        foreach ($actions as $action) {
            if ($action == 'other') {
                $columns['other'] = "SUM(IF(action_type = 1 AND action_code NOT IN ('$earnedActions'), point_amount, 0))";
            } else {
                $columns[$action] = "SUM(IF(action_code = '$action' AND point_amount > 0, point_amount, 0))";
            }
        }
        $collection->getSelect()->reset(Zend_Db_Select::COLUMNS)
            ->columns($columns)
            ->group($this->getPeriodFormat());

        // Change to Flat View - Can Filter and Search
        $viewSelect = clone $collection->getSelect();
        $collection->getSelect()->reset()
            ->from(array('main_table' => new Zend_Db_Expr('(' . $viewSelect->__toString() . ')')));

        $this->setCollection($collection);
        parent::_prepareCollection();

        return $this;
    }

    /**
     * prepare columns for this grid
     *
     * @return Magegiant_GiantPointsReport_Block_Adminhtml_Order_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('period', array(
            'header'       => Mage::helper('giantpointsreport')->__('Period'),
            'index'        => 'period',
            'type'         => 'date',
            'period_type'  => $this->getPeriodType(),
            'renderer'     => 'adminhtml/report_sales_grid_column_renderer_date',
            'totals_label' => Mage::helper('giantpointsreport')->__('Total'),
            'align'        => 'right',
        ));

        $this->addColumn('total', array(
            'header' => Mage::helper('giantpointsreport')->__('Total Earned Points'),
            'index'  => 'total',
            'type'   => 'number',
        ));

        foreach (Mage::helper('giantpointsreport')->getEarningActions() as $action => $label) {
            $this->addColumn($action, array(
                'header' => $label,
                'index'  => $action,
                'type'   => 'number',
            ));
        }

        $this->addExportType('*/*/exportCsv', Mage::helper('giantpointsreport')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('giantpointsreport')->__('XML'));
        $this->addExportType('*/*/exportExcel', Mage::helper('adminhtml')->__('Excel XML'));

        return parent::_prepareColumns();
    }

    protected function _getStore()
    {
        $storeId = (int)$this->getRequest()->getParam('store', 0);

        return Mage::app()->getStore($storeId);
    }

    /**
     * get url for each row in grid
     *
     * @return string
     */
    public function getRowUrl($row)
    {
        return '';
    }

    /**
     * get grid url (use for ajax load)
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }
}
