<?php
/**
 * MageGiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageGiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPoints
 * @copyright   Copyright (c) 2014 MageGiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * GiantPoints Report Spent Grid Block
 * 
 * @category    Magegiant
 * @package     Magegiant_GiantPointsReport
 * @author      Magegiant Developer
 */
class Magegiant_GiantPointsReport_Block_Adminhtml_Spending_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('spendingreportGrid');
        $this->setDefaultSort('period');
        $this->setDefaultDir('DESC');
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(true);
        $this->_prepareDateTypeReport();
    }
    
    /**
     * Prepare date range type to report
     * 
     * @return Magegiant_GiantPointsReport_Block_Adminhtml_Spent_Grid
     */
    public function _prepareDateTypeReport()
    {
        $filter = Mage::app()->getRequest()->getParam('filter', null);
        if (is_string($filter)) {
            $data = Mage::helper('adminhtml')->prepareFilterString($filter);
            if (isset($data['report_period'])) {
                $this->setPeriodType($data['report_period']);
            }
        }
        switch ($this->getPeriodType()) {
            case 'month':
                $this->setPeriodFormat("DATE_FORMAT(change_date, '%Y-%m')");
                break;
            case 'year':
                $this->setPeriodFormat("DATE_FORMAT(change_date, '%Y')");
                break;
            default :
                $this->setPeriodFormat("DATE_FORMAT(change_date, '%Y-%m-%d')");
                break;
        }
        return $this;
    }
    
    /**
     * prepare collection for block to display
     *
     * @return Magegiant_GiantPointsReport_Block_Adminhtml_Spent_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('giantpoints/transaction_collection')
            ->addFieldToFilter('main_table.status', array(
                'neq' => Magegiant_GiantPoints_Model_Transaction::STATUS_CANCELED
            ));
        
        // Add Store Filter Data
        if ($this->_getStore()->getId()) {
            $collection->addFieldToFilter('main_table.store_id', $this->_getStore()->getId());
        }
        
        // Remove duplicate order data
        $collection->addFieldToFilter('order_id', array('notnull' => true))
            ->getSelect()->reset(Zend_Db_Select::COLUMNS)
            ->columns(array(
                'period'        => $this->getPeriodFormat(),
                'spent_points'  => '-SUM(IF( action_type = 2 OR (action_type = 3 AND point_amount > 0) OR (action_type = 4 AND point_amount < 0), point_amount, 0))',
                'order_id'      => 'order_id',
                'total_discount' => 'SUM(base_discount)',
                'order_grand_total' => 'SUM(order_base_amount)',
				'total_order'   => 'COUNT(DISTINCT order_id)',
            ))->group($this->getPeriodFormat());
        
        // Change to Flat View - Can Filter and Search
		$viewSelect = clone $collection->getSelect();
        $collection->getSelect()->reset()
            ->from(array('main_table' => new Zend_Db_Expr('(' . $viewSelect->__toString() . ')')));
        
        $this->setCollection($collection);
        parent::_prepareCollection();
        
        return $this;
    }
    
    /**
     * prepare columns for this grid
     *
     * @return Magegiant_GiantPointsReport_Block_Adminhtml_Order_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('period', array(
            'header'    => Mage::helper('giantpointsreport')->__('Period'),
            'index'     => 'period',
            'type'      => 'date',
            'period_type'   => $this->getPeriodType(),
            'renderer'      => 'adminhtml/report_sales_grid_column_renderer_date',
            'totals_label'  => Mage::helper('giantpointsreport')->__('Total'),
            'align'     => 'right',
        ));
        
        $this->addColumn('spent_points', array(
            'header'    => Mage::helper('giantpointsreport')->__('Total Spent'),
            'index'     => 'spent_points',
            'type'      => 'number',
        ));
        
        $this->addColumn('total_order', array(
            'header'    => Mage::helper('giantpointsreport')->__('Number of Orders'),
            'index'     => 'total_order',
            'type'      => 'number',
        ));
        
        $this->addColumn('total_discount', array(
            'header'    => Mage::helper('giantpointsreport')->__('Discount for Using Points'),
            'index'     => 'total_discount',
            'type'      => 'price',
            'currency_code' => $this->_getStore()->getBaseCurrency()->getCode(),
        ));
        
        $this->addColumn('order_grand_total', array(
            'header'    => Mage::helper('giantpointsreport')->__('Grand Total Of Orders Using Points'),
            'index'     => 'order_grand_total',
            'type'      => 'price',
            'currency_code' => $this->_getStore()->getBaseCurrency()->getCode(),
        ));
        
        $this->addExportType('*/*/exportCsv', Mage::helper('giantpointsreport')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('giantpointsreport')->__('XML'));
        $this->addExportType('*/*/exportExcel', Mage::helper('adminhtml')->__('Excel XML'));
        
        return parent::_prepareColumns();
    }
    
    protected function _getStore()
    {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }
    
    /**
     * get url for each row in grid
     *
     * @return string
     */
    public function getRowUrl($row)
    {
        return '';
    }
    
    /**
     * get grid url (use for ajax load)
     * 
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }
}
