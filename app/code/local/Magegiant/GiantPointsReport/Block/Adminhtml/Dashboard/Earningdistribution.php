<?php
/**
 * Magegiant
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magegiant.com license that is
 * available through the world-wide-web at this URL:
 * http://www.Magegiant.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magegiant
 * @package     Magegiant_GiantpointsReport
 * @copyright   Copyright (c) 2012 Magegiant (http://www.Magegiant.com/)
 * @license     http://www.Magegiant.com/license-agreement.html
 */

/**
 * giantpointsreport Dashboard Report_Left_Earningdistribution Adminhtml Block
 * 
 * @category    Magegiant
 * @package     Magegiant_GiantpointsReport
 * @author      Magegiant Developer
 */
class Magegiant_GiantPointsReport_Block_Adminhtml_Dashboard_Earningdistribution
    extends Magegiant_GiantPointsReport_Block_Adminhtml_Dashboard
{
	public function __construct(){
		parent::__construct();
		$this->setTemplate('magegiant/giantpointsreport/dashboard/distribution.phtml');
		$data = $this->getEarningDistribution();
		if(is_array($data) && count($data)){
			$this->_hasData = true;
		}
	}
    
    /**
     * prepare datas for report chart
     * 
     * @return type
     */
	public function getEarningDistribution()
    {
        $collection = Mage::getResourceModel('giantpoints/transaction_collection')
            ->addFieldToFilter('status', array(
                'neq' => Magegiant_GiantPoints_Model_Transaction::STATUS_CANCELED
            ));
        if ($storeId = $this->getRequest()->getParam('store', 0)) {
            $collection->addFieldToFilter('store_id', $storeId);
        }
        $actions = array_keys(Mage::helper('giantpointsreport')->getEarningActions());
        $earnedActions = implode("','", $actions);
        $columns = array();
        foreach ($actions as $action) {
			if($action == 'other'){
				$columns['other'] = "SUM(IF(action_type = 1 AND action_code NOT IN ('$earnedActions'), point_amount, 0))";
			} else {
				$columns[$action] = "SUM(IF(action_code = '$action' AND point_amount > 0, point_amount, 0))";
			}
        }

        $collection->getSelect()->reset(Zend_Db_Select::COLUMNS)->columns($columns);
        $totals = $collection->getFirstItem();
        if ($totals && is_object($totals)) {
			$returnValue = $totals->getData();
			foreach($returnValue as $key => $value){
				if(!$value){
					unset($returnValue[$key]);
				}
			}
            return $returnValue;
        }
        return array();
    }

	public function getHeadTitle(){
		return $this->__('Earnings Distribution');
	}
    /**
     * get comment content for dashboard
     *
     * @return string
     */
    public function getCommentContent() {
        return $this->__('Show customer earning sources');
    }
}
