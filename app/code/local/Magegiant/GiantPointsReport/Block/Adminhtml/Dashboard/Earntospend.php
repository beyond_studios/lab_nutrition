<?php
/**
 * Magegiant
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magegiant.com license that is
 * available through the world-wide-web at this URL:
 * http://www.Magegiant.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magegiant
 * @package     Magegiant_GiantpointsReport
 * @copyright   Copyright (c) 2012 Magegiant (http://www.Magegiant.com/)
 * @license     http://www.Magegiant.com/license-agreement.html
 */

/**
 * giantpointsreport Dashboard Left Earntospend Adminhtml Block
 * 
 * @category    Magegiant
 * @package     Magegiant_GiantpointsReport
 * @author      Magegiant Developer
 */

class Magegiant_GiantPointsReport_Block_Adminhtml_Dashboard_Earntospend extends Magegiant_GiantPointsReport_Block_Adminhtml_Dashboard
{
	public function __construct(){
		parent::__construct();
		$this->setTemplate('magegiant/giantpointsreport/dashboard/earntospend.phtml');
		if($this->getEarnTotal()){
			$this->_hasData = true;
		}
	}
	public function getEarnTotal(){
		return Mage::getResourceModel('giantpointsreport/transaction_collection')
            ->getEarnTotals();
	}
	public function getSpendTotal(){
		return Mage::getResourceModel('giantpointsreport/transaction_collection')
			->getSpentTotals();
	}

	public function getHeadTitle(){
		return $this->__('Spending/Earning Ratio');
	}
    /**
     * get comment content for dashboard
     *
     * @return string
     */
    public function getCommentContent() {
        return $this->__('');
    }
}
