<?php
/**
 * Magegiant
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magegiant.com license that is
 * available through the world-wide-web at this URL:
 * http://www.Magegiant.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magegiant
 * @package     Magegiant_GiantpointsReport
 * @copyright   Copyright (c) 2012 Magegiant (http://www.Magegiant.com/)
 * @license     http://www.Magegiant.com/license-agreement.html
 */

/**
 * giantpointsdashboard Adminhtml Dashboard Right Averageorderpointearned Block
 * 
 * @category    Magegiant
 * @package     Magegiant_GiantpointsReport
 * @author      Magegiant Developer
 */
class Magegiant_GiantPointsReport_Block_Adminhtml_Dashboard_Diagrams extends Mage_Adminhtml_Block_Widget_Tabs
{
	public function __construct(){
        parent::__construct();
        $this->setId('diagram_tab');
        $this->setDestElementId('diagram_tab_content');
        $this->setTemplate('widget/tabshoriz.phtml');
    }
    
     /**
     * prepare columns for this dashboard
     *
     * @return Magegiant_GiantpointsReport_Block_Adminhtml_Dashboard_Right_Diagrams
     */
    protected function _prepareLayout(){
    	$this->addTab('loyalty',array(
    		'label'		=> $this->__('Loyal Customers'),
    		'content'	=> $this->getLayout()->createBlock('giantpointsreport/adminhtml_dashboard_tabs_loyalty')->toHtml(),
    		'active'	=> true,
    	));
    	
    	$this->addTab('earnspend',array(
			'label'		=> $this->__('Points Earned and Spent'),
			'content'	=> $this->getLayout()->createBlock('giantpointsreport/adminhtml_dashboard_tabs_earnspend')->toHtml(),
		));
//
//		$this->addTab('averageorderpointearned',array(
//			'label'		=> $this->__('Avg. order value vs avg number of points earned from orders'),
//			'content'	=> '<div>abc</div>'//$this->getLayout()->createBlock('magegiant/giantpointsreport/adminhtml_dashboard_right_averageorderpointearned')->toHtml(),
//		));
    	return parent::_prepareLayout();
    }
}