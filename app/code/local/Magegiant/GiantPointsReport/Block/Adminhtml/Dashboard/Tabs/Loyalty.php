<?php
/**
 * Magegiant
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magegiant.com license that is
 * available through the world-wide-web at this URL:
 * http://www.Magegiant.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magegiant
 * @package     Magegiant_GiantpointsReport
 * @copyright   Copyright (c) 2012 Magegiant (http://www.Magegiant.com/)
 * @license     http://www.Magegiant.com/license-agreement.html
 */

/**
 * giantpointsdashboard Adminhtml Dashboard Right Numberloyaltymember Block
 * 
 * @category    Magegiant
 * @package     Magegiant_GiantpointsReport
 * @author      Magegiant Developer
 */
class Magegiant_GiantPointsReport_Block_Adminhtml_Dashboard_Tabs_Loyalty extends Magegiant_GiantPointsReport_Block_Adminhtml_Dashboard_Graph
{
	public function __construct(){
		$this->_google_chart_params = array(
			'cht' => 'lc',
			'chf' => 'bg,s,f4f4f4|c,lg,90,ffffff,0.1,ededed,0',
			'chco' => 'db4814',
			'chxt' => 'x,y,y',
			'chxlexpend' => '|2:||'.$this->__('(Members)'),
			'chxs' => '1,db4814,10,1,lt,db4814,db4814',
		);
		$this->setHtmlId('loyalty');
		parent::__construct();
//		$this->setTemplate('magegiant/giantpointsreport/dashboard/template.phtml');
//		$this->_prepareData();
	}

//	public function getDataContent(){
//		$range = '1y';//$this->getParam('period');
//		$collection = Mage::getResourceModel('giantpointsreport/transaction_collection')
//			->prepareNumberloyaltymember($range,0,0);
//
//		if ($this->getParam('store'))
//			$collection->addFieldToFilter('store_id',$this->getParam('store'));
//		if($collection->getSize() > 0){
//			$this->_hasData = true;
//		}
//		return $collection->getData();
//	}
	/**
	 * Prepare chart data
	 *
	 * @return void
	 */
	protected function _prepareData()
	{
		$this->setDataHelperName('giantpointsreport/dashboard_loyalty');
		$this->getDataHelper()->setParam('store', $this->getRequest()->getParam('store'));
		$this->getDataHelper()->setParam('website', $this->getRequest()->getParam('website'));
		$this->getDataHelper()->setParam('group', $this->getRequest()->getParam('group'));

		$this->setDataRows('total');
		$this->_axisMaps = array(
			'x' => 'range',
			'y' => 'total'
		);

		parent::_prepareData();
	}
    /**
     * get comment content for dashboard
     *
     * @return string
     */
    public function getCommentContent(){
        return $this->__('Number of loyal customers in your store over time.');
    }

}