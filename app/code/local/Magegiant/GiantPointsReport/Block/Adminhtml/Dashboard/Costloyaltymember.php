<?php
/**
 * Magegiant
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magegiant.com license that is
 * available through the world-wide-web at this URL:
 * http://www.Magegiant.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magegiant
 * @package     Magegiant_GiantpointsReport
 * @copyright   Copyright (c) 2012 Magegiant (http://www.Magegiant.com/)
 * @license     http://www.Magegiant.com/license-agreement.html
 */

/**
 * giantpointsreport Dashboard Left Costloyatlymember Adminhtml Block
 * 
 * @category    Magegiant
 * @package     Magegiant_GiantpointsReport
 * @author      Magegiant Developer
 */

class Magegiant_GiantpointsReport_Block_Adminhtml_Dashboard_Costloyaltymember extends Mage_Adminhtml_Block_Dashboard_Bar {

    protected function _construct() {
        parent::_construct();
        $this->setTemplate('magegiant/giantpointsreport/dashboard/pattern.phtml');
    }

    /**
     * get content for title
     * 
     * @return type
     */
    public function getHeadTitle() {
        return Mage::helper('giantpointsreport')->__('A loyal customer cost');
    }

    /**
     * get calc average of cost than render content for Cost of loyal member
     * 
     * @return type
     */
    public function getContent() {
        $collection = Mage::getResourceModel('giantpointsreport/transaction_collection')->prepareCostLoyaltyMember();
        //$data = $collection->getFirstItem();
        if ($collection->getCustomerNumber()) {
            $average = $collection->getMoneySpentTotals() / $collection->getCustomerNumber();
        } else {
            $average = 0;
        }
        $currency = Mage::getModel('directory/currency')->load(Mage::app()->getStore()->getBaseCurrencyCode());
        return $currency->format($average);
    }

    /**
     * get comments
     * 
     * @return type
     */
    public function getCommentContent() {
        return Mage::helper('giantpointsreport')->__('The average cost of a loyal customer.');
    }

}