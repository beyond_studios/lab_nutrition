<?php
/**
 * Magegiant
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magegiant.com license that is
 * available through the world-wide-web at this URL:
 * http://www.Magegiant.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magegiant
 * @package     Magegiant_GiantpointsReport
 * @copyright   Copyright (c) 2012 Magegiant (http://www.Magegiant.com/)
 * @license     http://www.Magegiant.com/license-agreement.html
 */

/**
 * giantpointsreport Dashboard Left Averagevalueperpoint Adminhtml Block
 * 
 * @category    Magegiant
 * @package     Magegiant_GiantpointsReport
 * @author      Magegiant Developer
 */
class Magegiant_GiantPointsReport_Block_Adminhtml_Dashboard_Averagevalueperpoint extends Mage_Adminhtml_Block_Dashboard_Bar
{
	protected function _construct(){
		parent::_construct();
		$this->setTemplate('magegiant/giantpointsreport/dashboard/pattern.phtml');
	}
    
    /**
     * Head title for Your average value per point
     * 
     * @return type
     */
    public function getHeadTitle(){
        return Mage::helper('giantpointsreport')->__('Average value of 1 point spent');
    }
    
    /**
     * get contents to display
     * 
     * @return type
     */
    public function getContent(){
         $collection = Mage::getResourceModel('giantpointsreport/transaction_collection')->prepareAverageValuePerPoint();
         $data = $collection->getFirstItem();
         if($data->getPointSpent()){
             $average = $data->getMoneySpent()/$data->getPointSpent();
         }else {
             $average = 0;
         }
         $currency = Mage::getModel('directory/currency')->load(Mage::app()->getStore()->getBaseCurrencyCode());
        return $currency->format($average);
    }
    
    /**
     * get comments
     * 
     * @return type
     */
    public function getCommentContent(){
        return Mage::helper('giantpointsreport')->__('This is the average value of 1 point spent. It means that for every 1 point given to Customers, you can get %s in revenue.', $this->getContent());
    }
}
