<?php
/**
 * Magegiant
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magegiant.com license that is
 * available through the world-wide-web at this URL:
 * http://www.Magegiant.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magegiant
 * @package     Magegiant_GiantpointsReport
 * @copyright   Copyright (c) 2012 Magegiant (http://www.Magegiant.com/)
 * @license     http://www.Magegiant.com/license-agreement.html
 */

/**
 * giantpointsreport Dashboard Left TotalPointsBalance Adminhtml Block
 * 
 * @category    Magegiant
 * @package     Magegiant_GiantpointsReport
 * @author      Magegiant Developer
 */
class Magegiant_GiantpointsReport_Block_Adminhtml_Dashboard_Totalspointsbalance extends Mage_Adminhtml_Block_Dashboard_Bar
{
	protected function _construct(){
		parent::_construct();
		$this->setTemplate('magegiant/giantpointsreport/dashboard/pattern.phtml');
	}
    
    /**
     * get head title for dashboard
     *
     * @return string
     */
    public function getHeadTitle(){
        return Mage::helper('giantpointsreport')->__('Total Points');
    }
    
    /**
     * get content for dashboard
     *
     * @return string
     */
    public function getContent(){
        $collection = Mage::getResourceModel('giantpoints/customer_collection');
		if ($storeId = Mage::app()->getRequest()->getParam('store')){
			$collection->getSelect()
					   ->joinLeft(
						array('ct' => $collection->getTable('customer/entity')),
						'main_table.customer_id = ct.entity_id',
						array('store_id' => 'ct.store_id'))
						->where('store_id = ?', $storeId);
		}
		$collection->getSelect()
					->columns(array(
						'totals' => 'SUM(main_table.point_balance)'
					));
        $totals_points_balance = $collection->getFirstItem()->getTotals();
        return Mage::helper('giantpoints')->addLabelForPoint($totals_points_balance, Mage::app()->getStore());
    }
    
    /**
     * get explanation for dashboard
     *
     * @return string
     */
    public function getCommentContent(){
        return Mage::helper('giantpointsreport')->__('Total points in customers\' balances.');
    }
}
