<?php
/**
 * Magegiant
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magegiant.com license that is
 * available through the world-wide-web at this URL:
 * http://www.Magegiant.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magegiant
 * @package     Magegiant_GiantpointsReport
 * @copyright   Copyright (c) 2012 Magegiant (http://www.Magegiant.com/)
 * @license     http://www.Magegiant.com/license-agreement.html
 */

/**
 * giantpointsdashboard Adminhtml Dashboard Right Numberloyaltymember Block
 * 
 * @category    Magegiant
 * @package     Magegiant_GiantpointsReport
 * @author      Magegiant Developer
 */
class Magegiant_GiantPointsReport_Block_Adminhtml_Dashboard_Tabs_Earnspend extends Magegiant_GiantPointsReport_Block_Adminhtml_Dashboard_Graph
{
	public function __construct(){
		$this->_google_chart_params = array(
			'cht'  => 'lc',
			'chf'  => 'bg,s,f4f4f4|c,lg,90,ffffff,0.1,ededed,0',
			'chdl' => $this->__('Earned').'|'.$this->__('Spent'),
			'chco' => '2424ff,ff3300',
			'chxt' => 'x,y,y',
			'chxla'=> '2:|'.$this->__('(Points)'),
			'chxp' => '2,100',
			'chxs' => '1,660066,10,1,lt,660066,660066',
		);
		$this->setHtmlId('earnspend');
		parent::__construct();
	}
	/**
	 * Prepare chart data
	 *
	 * @return void
	 */
	protected function _prepareData(){
		$this->setDataHelperName('giantpointsreport/dashboard_earnspend');
		$this->getDataHelper()->setParam('store', $this->getRequest()->getParam('store'));

		$this->setDataRows(array('earn', 'spend'));
		$this->_axisMaps = array(
			'x'	=> 'range',
			'y' => 'earn',
			'y' =>  'spend'
		);

		parent::_prepareData();
	}
    /**
     * get comment content for dashboard
     *
     * @return string
     */
    public function getCommentContent(){
		return $this->__('Number of points your customers earned and spent.');
    }

}