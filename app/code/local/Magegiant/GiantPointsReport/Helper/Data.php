<?php
/**
 * MageGiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageGiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsReport
 * @copyright   Copyright (c) 2014 MageGiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * GiantPointsReport Helper
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsReport
 * @author      MageGiant Developer
 */
class Magegiant_GiantPointsReport_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function isEnabled(){
		return Mage::getStoreConfig('giantpoints/report_configuration/enable');
	}
	/**
	 * get Earning Action to show On Report
	 *
	 * @return array
	 */
	public function getEarningActions()
	{
		$action = explode(',', Mage::getStoreConfig('giantpoints/report_configuration/show_fields'));
		$originAction = Mage::getModel('giantpointsreport/source_actions')->toArray();
		foreach($originAction as $value => $label){
			if(!in_array($value, $action)){
				unset($originAction[$value]);
			}
		}
		$originAction['other'] = Mage::helper('giantpointsreport')->__('Others');
		return $originAction;
	}
	public function isPluginEnabled($pluginName = null)
	{
		return Mage::helper('core')->isModuleEnabled($pluginName);
	}

}