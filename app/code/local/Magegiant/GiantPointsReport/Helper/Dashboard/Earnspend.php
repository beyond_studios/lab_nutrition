<?php
/**
 * MageGiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageGiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPoints
 * @copyright   Copyright (c) 2014 MageGiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * GiantPointsReport Helper
 * 
 * @category    Magegiant
 * @package     Magegiant_GiantpointsReport
 * @author      Magegiant Developer
 */
class Magegiant_GiantPointsReport_Helper_Dashboard_Earnspend extends Mage_Adminhtml_Helper_Dashboard_Abstract
{
    /**
    * prepare collection for this dashboard
    *    
    */
    protected function _initCollection(){
		$this->_collection = Mage::getResourceSingleton('giantpointsreport/transaction_collection')
			->prepareSummaryEarnspend($this->getParam('period'), 0, 0);

		if ($this->getParam('store')) {
			$this->_collection->addFieldToFilter('store_id', $this->getParam('store'));
		} else if ($this->getParam('website')){
			$storeIds = Mage::app()->getWebsite($this->getParam('website'))->getStoreIds();
			$this->_collection->addFieldToFilter('store_id', array('in' => implode(',', $storeIds)));
		} else if ($this->getParam('group')){
			$storeIds = Mage::app()->getGroup($this->getParam('group'))->getStoreIds();
			$this->_collection->addFieldToFilter('store_id', array('in' => implode(',', $storeIds)));
		}
		$this->_collection->load();
    }
}