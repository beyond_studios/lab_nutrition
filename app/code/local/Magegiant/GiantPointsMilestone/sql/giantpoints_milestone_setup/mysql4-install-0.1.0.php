<?php
/**
 * MageGiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageGiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsMilestone
 * @copyright   Copyright (c) 2014 MageGiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/** @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();
$installer->run("
DROP TABLE IF EXISTS {$this->getTable('milestone/rule')};
CREATE TABLE {$this->getTable('milestone/rule')} (
    `rule_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `behavior_rule_id` INT(11) UNSIGNED NOT NULL,
    `name` VARCHAR(255) NOT NULL DEFAULT '',
    `points_conditions` VARCHAR(255) DEFAULT NULL,
    `point_action` VARCHAR(25),
    `milestone_orders` INT(10) UNSIGNED NOT NULL,
    `milestone_revenue` INT(10) UNSIGNED NOT NULL,
    `milestone_membership` INT(10) UNSIGNED NOT NULL,
    `milestone_inactivity` INT(10) UNSIGNED NOT NULL,
    `milestone_referrals` INT(10) UNSIGNED NOT NULL,
    `customer_group_id` SMALLINT(5) UNSIGNED NOT NULL,
     PRIMARY KEY (`rule_id`),
     CONSTRAINT `FK_GIANTPOINTS_MILESTONE_RULE_ID` FOREIGN KEY (`behavior_rule_id`) REFERENCES {$this->getTable('giantpoints/rule')} (`behavior_rule_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB DEFAULT CHARSET = utf8;
");
$installer->run("
DROP TABLE IF EXISTS {$this->getTable('milestone/log')};
CREATE TABLE `{$this->getTable('milestone/log')}` (
        `log_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
        `rule_id` INT(10) UNSIGNED NOT NULL,
        `name` VARCHAR(255) NOT NULL DEFAULT '',
        `points_conditions` VARCHAR(255) DEFAULT NULL,
        `point_action` VARCHAR(25),
        `customer_id` INT(10) UNSIGNED NOT NULL,
        `executed_date` TIMESTAMP NULL DEFAULT NULL,
        PRIMARY KEY (`log_id`),
        CONSTRAINT `FK_CUSTOMER_ID` FOREIGN KEY (`customer_id`) REFERENCES `{$this->getTable('customer_entity')}` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
        CONSTRAINT `FK_RULE_ID` FOREIGN KEY (`rule_id`) REFERENCES `{$this->getTable('milestone/rule')}` (`rule_id`) ON DELETE CASCADE ON UPDATE CASCADE
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Contains rule execution records' AUTO_INCREMENT=1 ;
");
$installer->endSetup();

