<?php
/**
 * MageGiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageGiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsMilestone
 * @copyright   Copyright (c) 2014 MageGiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * GiantPointsMilestone Observer Model
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsMilestone
 * @author      MageGiant Developer
 */
class Magegiant_GiantPointsMilestone_Model_Observer
{
    /**
     * Observes the sales_order_place_after event.
     * Triggers any 'orders' milestones if they are set to be triggered upon order creation.
     *
     * @param $observer
     * @return $this
     */
    public function orderPlaceAfter($observer)
    {
//        Mage::log("PRUEBITAAAAA ....");
//        Mage::log("AAAAA");

        $event = $observer->getEvent();
        if (!$event) {
            return $this;
        }

        $order = $event->getOrder();
        if (!$order) {
            return $this;
        }

        $customerId = $order->getCustomerId();
        if (!$customerId) {
            return $this;
        }

        $store = $order->getStore();
        if (!$store) {
            return $this;
        }

        $storeId      = $store->getId();

        $orderTrigger = Mage::helper('milestone/config')->getOrdersTrigger($storeId);
        //if ($orderTrigger != Magegiant_GiantPointsMilestone_Model_System_Config_Source_Order_Status::ORDER_CREATE)
        //    return $this;

        $customer = Mage::getModel('customer/customer')->load($customerId);

        /*Proccess Mile Order*/
        $this->_processMilestoneOrder($customer);

//        Mage::log("BBBB");
    }



    public function salesOrderPlaceAfter($observer)
    {

        $event = $observer->getEvent();
        if (!$event) {
            return $this;
        }

        $invoice = $event->getInvoice();
        if (!$invoice) {
            return $this;
        }

        $order = $invoice->getOrder();
        if (!$order) {
            return $this;
        }

        $customerId = $order->getCustomerId();
        if (!$customerId) {
            return $this;
        }

        $store = $order->getStore();
        if (!$store) {
            return $this;
        }

        $storeId  = $store->getId();
        $customer = Mage::getModel('customer/customer')->load($customerId);
        $this->_processMilestoneRevenue($customer);
        $orderTrigger = Mage::helper('milestone/config')->getOrdersTrigger($storeId);
        if ($orderTrigger != Magegiant_GiantPointsMilestone_Model_System_Config_Source_Order_Status::ORDER_PAYMENT)
            return $this;
        $this->_processMilestoneOrder($customer);

    }

    /**
     * @param Varien_Event_Observer $observer
     * @return self
     */
    public function invoiceSaveCommitAfter($observer)
    {
        $event = $observer->getEvent();
        if (!$event) {
            return $this;
        }

        $invoice = $event->getInvoice();
        if (!$invoice) {
            return $this;
        }

        $order = $invoice->getOrder();
        if (!$order) {
            return $this;
        }

        $customerId = $order->getCustomerId();
        if (!$customerId) {
            return $this;
        }

        $store = $order->getStore();
        if (!$store) {
            return $this;
        }

        $storeId  = $store->getId();
        $customer = Mage::getModel('customer/customer')->load($customerId);
        $this->_processMilestoneRevenue($customer);
        $orderTrigger = Mage::helper('milestone/config')->getOrdersTrigger($storeId);
        if ($orderTrigger != Magegiant_GiantPointsMilestone_Model_System_Config_Source_Order_Status::ORDER_PAYMENT)
            return $this;
        $this->_processMilestoneOrder($customer);
    }

    /**
     * @param $observer
     */
    public function shipmentSaveCommitAfter($observer)
    {
        $event = $observer->getEvent();
        if (!$event) {
            return $this;
        }

        $shipment = $observer->getShipment();
        if (!$shipment || !($shipment instanceof Mage_Sales_Model_Order_Shipment)) {
            return $this;
        }

        $order = $shipment->getOrder();
        if (!$order) {
            return $this;
        }

        $customerId = $order->getCustomerId();
        if (!$customerId) {
            return $this;
        }

        $store = $order->getStore();
        if (!$store) {
            return $this;
        }

        $storeId      = $store->getId();
        $orderTrigger = Mage::helper('milestone/config')->getOrdersTrigger($storeId);
        if ($orderTrigger != Magegiant_GiantPointsMilestone_Model_System_Config_Source_Order_Status::ORDER_CREATE)
            return $this;
        $customer = Mage::getModel('customer/customer')->load($customerId);

        /*Proccess Milestone Order*/
        $this->_processMilestoneOrder($customer);
    }


    /**
     * Proccess mile stone rule
     *
     * @param $customer
     */
    protected function _processMilestoneOrder($customer)
    {
        $rules = Mage::getModel('giantpoints/rule')
            ->getAllRulesByCondition(Magegiant_GiantPointsMilestone_Model_Behavior_Rule_Action_Milestone_Orders::ACTION_CODE, $customer);

        foreach ($rules as $rule) {
            $mile_stone = Mage::getModel('milestone/rule')->load($rule->getId(), 'behavior_rule_id');

            if (!$mile_stone || !$mile_stone->getId())
                continue;
            $validation = Mage::getModel('milestone/validation_orders');

            if (!$validation->validate($customer->getId(), $rule, $mile_stone)) {
                continue;
            }
//            Mage::log('Paso validacion');                    
            //Mage::log($mile_stone->ruleIsExecuted($customer->getId()));

            if ($mile_stone->ruleIsExecuted($customer->getId())) {
                continue;
            }

            //Create a row in milestone rule log
            $mile_stone->setCustomerId($customer->getId())
                ->writeLog();
            if ($rule->getPointAction() == 'grant_points') {
                Mage::helper('milestone')->processEarnPoint('orders', $customer, $rule);

            } else if ($rule->getPointAction() == 'customergroup') {
                Mage::helper('milestone')->processChangeGroup($customer, $mile_stone);
            }
            $helperConfig = Mage::helper('milestone/config');

            //$this->_sendEmail($rule, $customer, $helperConfig->getOrderEmailTemplate());
        }
    }

    /**
     * Proccess mile stone rule
     *
     * @param $customer
     */
    protected function _processMilestoneRevenue($customer)
    {
        $rules = Mage::getModel('giantpoints/rule')
            ->getAllRulesByCondition(Magegiant_GiantPointsMilestone_Model_Behavior_Rule_Action_Milestone_Revenue::ACTION_CODE, $customer);
        foreach ($rules as $rule) {
            $mile_stone = Mage::getModel('milestone/rule')->load($rule->getId(), 'behavior_rule_id');
            if (!$mile_stone || !$mile_stone->getId())
                continue;
            $validation = Mage::getModel('milestone/validation_revenue');
            if (!$validation->validate($customer->getId(), $rule, $mile_stone)) {
                continue;
            }
            if ($mile_stone->ruleIsExecuted($customer->getId())) {
                continue;
            }
            //Create a row in milestone rule log
            $mile_stone->setCustomerId($customer->getId())
                ->writeLog();
            if ($rule->getPointAction() == 'grant_points') {
                Mage::helper('milestone')->processEarnPoint('revenue', $customer, $rule);
            } else if ($rule->getPointAction() == 'customergroup') {
                Mage::helper('milestone')->processChangeGroup($customer, $mile_stone);
            }
            $helperConfig = Mage::helper('milestone/config');
            $this->_sendEmail($rule, $customer, $helperConfig->getRevenueEmailTemplate());
        }
    }

    /**
     *
     * @param $observer
     * @return $this
     */
    public function referralsCommitAfter($observer)
    {
        $event = $observer->getEvent();
        if (!$event) {
            return $this;
        }

        $referral = $observer->getReferral();
        if (!$referral || !$referral->getId()) {
            return $this;
        }
        $customer = Mage::getModel('customer/customer')->load($referral->getReferralId());
        /*Proccess Milestone Order*/
        $this->_processMilestoneReferrals($customer);
    }

    /**
     * @param $customer
     */
    protected function _processMilestoneReferrals($customer)
    {
        $rules = Mage::getModel('giantpoints/rule')
            ->getAllRulesByCondition(Magegiant_GiantPointsRefer_Model_Rule_Action_Milestone_Referrals::ACTION_CODE, $customer);
        foreach ($rules as $rule) {
            $mile_stone = Mage::getModel('milestone/rule')->load($rule->getId(), 'behavior_rule_id');
            if (!$mile_stone || !$mile_stone->getId())
                continue;
            $validation = $mile_stone->getValidation();
            if (!$validation->validate($customer, $rule, $mile_stone)) {
                continue;
            }
            if ($mile_stone->ruleIsExecuted($customer->getId())) {
                continue;
            }
            //Create a row in milestone rule log
            $mile_stone->setCustomerId($customer->getId())
                ->writeLog();
            if ($rule->getPointAction() == 'grant_points') {
                Mage::helper('milestone')->processEarnPoint('referrals', $customer, $rule);
            } else if ($rule->getPointAction() == 'customergroup') {
                Mage::helper('milestone')->processChangeGroup($customer, $mile_stone);
            }
            $helperConfig = Mage::helper('milestone/config');

            $this->_sendEmail($rule, $customer, $helperConfig->getReferralsEmailTemplate());
        }


    }

    protected function _sendEmail($rule, $customer, $template)
    {
        $message = Mage::helper('milestone')->getMilestoneMessage($rule);
        Mage::helper('milestone')->sendMilestoneEmail($rule, $customer, $template, $message);
    }
}