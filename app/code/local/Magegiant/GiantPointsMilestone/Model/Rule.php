<?php
/**
 * MageGiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageGiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsMilestone
 * @copyright   Copyright (c) 2014 MageGiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * Giantpointsmilestone Model
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsMilestone
 * @author      MageGiant Developer
 */
class Magegiant_GiantPointsMilestone_Model_Rule extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('milestone/rule');
    }

    /**
     * Check rule ever executed or not
     *
     * @param $customer_id
     * @return bool
     */
    public function ruleIsExecuted($customer_id)
    {
        return Mage::getModel('milestone/log')->ruleIsExecuted($this->getId(), $customer_id);
    }

    /**
     * Write milestone data to log table
     */
    public function writeLog()
    {
        $currentTimestamp = Mage::helper('giantpoints')->getMageTime();
        try {
            Mage::getModel('milestone/log')
                ->setData($this->getData())
                ->setExecutedDate($currentTimestamp)
                ->save();
        } catch (Exception $e) {
            Mage::helper('giantpoints')->log($e->getMessage());
        }
    }

    public function getValidation()
    {
        $type = str_replace('giantpoints_customer_milestone_', '', $this->getPointsConditions());

        return Mage::getModel('milestone/validation_' . $type);
    }
}