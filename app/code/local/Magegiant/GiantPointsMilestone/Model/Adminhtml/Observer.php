<?php
/**
 * MageGiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageGiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsMilestone
 * @copyright   Copyright (c) 2014 MageGiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * GiantPointsMilestone Observer Model
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsMilestone
 * @author      MageGiant Developer
 */
class Magegiant_GiantPointsMilestone_Model_Adminhtml_Observer
{
    const BEHAVIOR_RULE_PREFIX = 'giantpoints_customer_';

    /**
     * process controller_action_predispatch event
     *
     * @return Magegiant_GiantPointsMilestone_Model_Observer
     */
    public function behaviorRuleSaveAfter($observer)
    {
        $behavior_rule = $observer->getRule();
        $rule_prefix   = self::BEHAVIOR_RULE_PREFIX;
        if (strpos($behavior_rule['points_conditions'], 'milestone') === false) {
            return $this;
        }
        $data = array();
        foreach ($behavior_rule as $key => $value) {
            $data[str_replace($rule_prefix, '', $key)] = $value;
        }
        $milestone = Mage::getModel('milestone/rule')->load($data['behavior_rule_id'], 'behavior_rule_id');
        try {
            $milestone->addData($data);
            $milestone->save();
        } catch (Exception $e) {
            Mage::helper('giantpoints')->log($e->getMessage());
        }

        return $this;
    }

    public function behaviorRuleAfterLoad($observer)
    {
        $behavior_rule = $observer->getRule();
        if ($behavior_rule && $behavior_rule->getId()) {
            $milestone = Mage::getModel('milestone/rule')->load($behavior_rule->getId(), 'behavior_rule_id');
            $data      = array();
            foreach ($milestone->getData() as $key => $value) {
                if ($key !== 'customer_group_id') {
                    $data[self::BEHAVIOR_RULE_PREFIX . $key] = $value;
                } else {
                    $data[$key] = $value;
                }

            }
            $behavior_rule->addData($data);
        }

        return $this;
    }
}