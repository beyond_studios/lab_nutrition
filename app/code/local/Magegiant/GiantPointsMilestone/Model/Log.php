<?php
/**
 * MageGiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageGiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsMilestone
 * @copyright   Copyright (c) 2014 MageGiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * Giantpointsmilestone Model
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsMilestone
 * @author      MageGiant Developer
 */
class Magegiant_GiantPointsMilestone_Model_Log extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('milestone/log');
    }

    /**
     * @param int $ruleId
     * @param int $customerId
     * @return int, The number of times a specific rule has been executed by a customer
     */
    public function ruleIsExecuted($ruleId, $customerId)
    {
        $executionCount = $this->getCollection()
            ->filterRuleLogsByCustomer($ruleId, $customerId)
            ->getSize();

        return $executionCount;
    }

    /**
     * @param int $ruleId
     * @param int $customerId
     * @return boolean. True if any execution records were found for the specified rule and customer. False otherwise.
     */
    public function isExecutedRule($ruleId, $customerId)
    {
        return !($this->getRuleExecutionCount($ruleId, $customerId) == 0);
    }
}