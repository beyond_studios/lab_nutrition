<?php

class Magegiant_GiantPointsMilestone_Model_Validation_Referrals extends Magegiant_GiantPointsMilestone_Model_Validation_Abstract
{
    public function validate($customer, $rule, $milestone)
    {
        $fromDate  = $rule->getFromDate();
        $toDate    = $rule->getToDate();
        $threshold = $milestone->getMilestoneReferrals();
        if (!$customer || !$customer->getId()) {
            return false;
        }
        $referralsBeforeStartDate = Mage::getModel('giantpointsrefer/invitation')->getCollection()
            ->addFieldToFilter('date', array("lt" => $fromDate));
        $referralsBeforeStartDate->getSelect()
            ->where('main_table.status = ?', Magegiant_GiantPointsRefer_Model_Invitation::INVITATION_ACCEPTED)
            ->where('main_table.referral_id = ?', $customer->getId());
        $referralsAfterStartDate = Mage::getModel('giantpointsrefer/invitation')->getCollection()
            ->addFieldToFilter('date', array("gteq" => $fromDate));
        $referralsAfterStartDate->getSelect()
            ->where('main_table.status = ?', Magegiant_GiantPointsRefer_Model_Invitation::INVITATION_ACCEPTED)
            ->where('main_table.referral_id = ?', $customer->getId());
        if (!empty($toDate)) {
            $referralsAfterStartDate->addFieldToFilter(
                "date", array("lt" => $toDate)
            );
        }
        $storeId = $customer->getStoreId();
        if ($this->_getHelper('config')->getReferralsTrigger($storeId) == Magegiant_GiantPointsMilestone_Model_System_Config_Source_Referral_Status::REFERRAL_ORDER) {
            $this->_joinWithOrders($referralsBeforeStartDate);
            $this->_joinWithOrders($referralsAfterStartDate);
            $countBeforeStartDate = $this->_getCount($referralsBeforeStartDate);
            $countAfterStartDate  = $this->_getCount($referralsAfterStartDate);

        } else {
            $countBeforeStartDate = $referralsBeforeStartDate->getSize();
            $countAfterStartDate  = $referralsAfterStartDate->getSize();
        }
        $countTotal = $countBeforeStartDate + $countAfterStartDate;

        return ($countBeforeStartDate < $threshold && $countTotal >= $threshold);
    }

    /**
     * @param $collection
     * @return mixed
     */
    protected function _joinWithOrders(&$collection)
    {
        $ordersTable = $this->_getOrdersTableName();
        $collection->getSelect()
            ->join(array('orders' => $ordersTable),
                'main_table.email = orders.customer_email',
                array("number_of_orders" => "COUNT(*)")
            )->group('email');

        return $collection;
    }

    /**
     * @return string. Name of the table for the sales/order model.
     */
    protected function _getOrdersTableName()
    {
        if (!isset($this->_ordersTable)) {
            $this->_ordersTable = Mage::getSingleton('core/resource')->getTableName('sales/order');
        }

        return $this->_ordersTable;
    }

    /**
     * @param $collection
     * @return int
     */
    protected function _getCount($collection)
    {
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $countSql   = "SELECT COUNT(*) FROM ({$collection->getSelectSql(true)}) AS collection";

        return (int)$connection->fetchOne($countSql);
    }
}
