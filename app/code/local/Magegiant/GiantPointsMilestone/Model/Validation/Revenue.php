<?php

class Magegiant_GiantPointsMilestone_Model_Validation_Revenue extends Magegiant_GiantPointsMilestone_Model_Validation_Abstract
{
    public function validate($customer_id, $rule, $milestone)
    {
        $invoices                     = $this->getInvoicesByCustomer($customer_id, $rule);
        $invoiceCollectionBeforeStart = $invoices['invoices_before'];
        $invoiceCollectionAfterStart  = $invoices['invoices_after'];
        $totalRevenueBeforeStart      = $this->_fetchRevenue($invoiceCollectionBeforeStart);
        $totalRevenueAfterStart       = $this->_fetchRevenue($invoiceCollectionAfterStart);
        $totalRevenue                 = $totalRevenueBeforeStart + $totalRevenueAfterStart;

        // Convert currency amounts to integers to circumvent any ugly floating-point headaches.
        $totalRevenueBeforeStart = (int)round($totalRevenueBeforeStart * 4, 0);
        $totalRevenue            = (int)round($totalRevenue * 4, 0);
        $threshold               = (int)round($milestone->getMilestoneRevenue() * 4, 0);
        return $totalRevenueBeforeStart < $threshold && $totalRevenue >= $threshold;

    }

    protected function _fetchRevenue($collection)
    {
        $collection->getSelect()
            ->reset(Zend_Db_Select::COLUMNS);

        $collection->getSelect()
            ->group('order_table.customer_id');

        $collection->addExpressionFieldToSelect('total_revenue', "SUM(main_table.base_grand_total)", array());

        return $collection->getFirstItem()->getData('total_revenue');
    }
}
