<?php

class Magegiant_GiantPointsMilestone_Model_Validation_Orders extends Magegiant_GiantPointsMilestone_Model_Validation_Abstract
{
    public function validate($customer_id, $rule, $milestone)
    {
        $orders                = $this->getOrdersByCustomer($customer_id, $rule);
        $ordersBeforeStartDate = $orders['orders_before'];
        $ordersAfterStartDate  = $orders['orders_after'];
        $this->_addCountingConstraints($ordersBeforeStartDate);
        $this->_addCountingConstraints($ordersAfterStartDate);
        $countBeforeStartDate = $ordersBeforeStartDate->getSize();
        $countAfterStartDate  = $ordersAfterStartDate->getSize();
        $countTotal           = $countBeforeStartDate + $countAfterStartDate;
        $threshold            = $milestone->getMilestoneOrders();

//        Mage::log('---start---');
//        Mage::log($countBeforeStartDate);
//        Mage::log($threshold);
//        Mage::log($countTotal);
//        Mage::log('---end---');

        // Condición solo para
        //return ($countBeforeStartDate < $threshold && $countTotal >= $threshold);
        //return ($countBeforeStartDate < $threshold);
        
        // Evalua a los usuarios existentes que no hallan hecho una compra desde una determinada fecha
        return ($countAfterStartDate <= $threshold);

    }

    /**
     * @param $collection
     * @return mixed
     */
    protected function _addCountingConstraints(&$collection)
    {
        $orderCountTrigger = $this->_getHelper('config')->getOrdersTrigger();
        switch ($orderCountTrigger) {
            case "payment":
                // Count everything that has an invoice
                $collection->getSelect()->join(
                    array("invoice" => $this->_getInvoiceTableName()),
                    "main_table.entity_id = invoice.order_id"
                );
                break;

            case "shipment":
                // Count everything that has a shipment
                $collection->getSelect()->join(
                    array("shipment" => $this->_getShipmentTableName()),
                    "main_table.entity_id = shipment.order_id"
                );
                break;

            case "create":
                // Notuing specific
            default:
                break;
        }

        // Make sure we're always looking at orders which are not canceled
        $collection->addFieldToFilter('main_table.state',
            array("nin" => array(
                Mage_Sales_Model_Order::STATE_CANCELED
            )));

        return $collection;
    }

    /**
     * Get the table name for the sales/invoice table
     *
     * @return string
     */
    protected function _getInvoiceTableName()
    {
        if (!isset($this->_invoiceTable)) {
            $this->_invoiceTable = Mage::getSingleton('core/resource')->getTableName('sales/invoice');
        }

        return $this->_invoiceTable;
    }

    /**
     * Get the table name for the sales/shipment table
     *
     * @return string
     */
    protected function _getShipmentTableName()
    {
        if (!isset($this->_shipmentTable)) {
            $this->_shipmentTable = Mage::getSingleton('core/resource')->getTableName('sales/shipment');
        }

        return $this->_shipmentTable;
    }
}
