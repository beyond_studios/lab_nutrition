<?php


class Magegiant_GiantPointsMilestone_Model_Validation_Abstract extends Varien_Object
{
    /**
     * get mile helper
     *
     * @return mixed
     */
    protected function _getHelper($type = null)
    {
        $helper = is_null($type) ? "data" : $type;

        return Mage::helper("milestone/{$helper}");
    }

    /**
     * get order by customer id, rule, milestone
     *
     * @param $customer_id
     * @param $rule
     * @return array
     */
    public function getOrdersByCustomer($customerId, $rule)
    {
        $fromDate              = $rule->getFromDate();
        $toDate                = $rule->getToDate();
        $storeIds              = $this->_getHelper()->getStoreIdsFromWebsites($rule->getWebsiteIds());
        $ordersBeforeStartDate = Mage::getModel('sales/order')->getCollection()
            ->addFieldToFilter('main_table.customer_id', $customerId)
            ->addFieldToFilter('main_table.store_id', array("in" => $storeIds))
            ->addFieldToFilter('main_table.created_at', array("lt" => $fromDate));
        $ordersAfterStartDate  = Mage::getModel('sales/order')->getCollection()
            ->addFieldToFilter('main_table.customer_id', $customerId)
            ->addFieldToFilter('main_table.store_id', array("in" => $storeIds))
            ->addFieldToFilter('main_table.created_at', array("gteq" => $fromDate));
        if (!empty($toDate)) {
            $ordersAfterStartDate->addFieldToFilter("main_table.created_at", array("lt" => $toDate));
        }
        $orders = array(
            'orders_before' => $ordersBeforeStartDate,
            'orders_after'  => $ordersAfterStartDate
        );

        return $orders;
    }

    /**
     * get order by customer id, rule, milestone
     *
     * @param $customer_id
     * @param $rule
     * @return array
     */
    public function getInvoicesByCustomer($customerId, $rule)
    {
        $fromDate                     = $rule->getFromDate();
        $toDate                       = $rule->getToDate();
        $storeIds                     = $this->_getHelper()->getStoreIdsFromWebsites($rule->getWebsiteIds());
        $invoiceCollectionBeforeStart = Mage::getResourceModel('sales/order_invoice_collection')
            ->addFieldToFilter('main_table.store_id', array('in' => $storeIds))
            ->addFieldToFilter('main_table.state', Mage_Sales_Model_Order_Invoice::STATE_PAID)
            ->addFieldToFilter('main_table.created_at', array('lt' => $fromDate));
        $invoiceCollectionBeforeStart->getSelect()->join(
            array('order_table' => Mage::getSingleton('core/resource')->getTableName('sales/order')),
            "main_table.order_id = order_table.entity_id",
            array('order_table.customer_id')
        );
        $invoiceCollectionBeforeStart->addFieldToFilter('order_table.customer_id', $customerId);

        $invoiceCollectionAfterStart = Mage::getResourceModel('sales/order_invoice_collection')
            ->addFieldToFilter('main_table.store_id', array('in' => $storeIds))
            ->addFieldToFilter('main_table.state', Mage_Sales_Model_Order_Invoice::STATE_PAID)
            ->addFieldToFilter('main_table.created_at', array('gteq' => $toDate));
        $invoiceCollectionAfterStart->getSelect()->join(
            array('order_table' => Mage::getSingleton('core/resource')->getTableName('sales/order')),
            "main_table.order_id = order_table.entity_id",
            array('order_table.customer_id')
        );
        $invoiceCollectionAfterStart->addFieldToFilter('order_table.customer_id', $customerId);
        if (!empty($toDate)) {
            $invoiceCollectionAfterStart->addFieldToFilter('main_table.created_at', array('lt' => $toDate));
        }
        $invoices = array(
            'invoices_before' => $invoiceCollectionBeforeStart,
            'invoices_after'  => $invoiceCollectionAfterStart
        );

        return $invoices;
    }

}
