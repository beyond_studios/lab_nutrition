<?php

class Magegiant_GiantPointsMilestone_Model_Validation_Inactivity extends Magegiant_GiantPointsMilestone_Model_Validation_Abstract
{
    /**
     * Valedate cusomer is reached membership length
     *
     * @param $customer_id
     * @param $rule
     * @param $milestone
     * @return bool
     */
    public function validate($rule, $milestone)
    {
        $customers = $this->getCustomersCollectionByRule($rule, $milestone);
        foreach ($customers as $customer) {
            if (!$customer->getIsActive()) {
                continue;
            }
            //Create a row in milestone rule log
            $milestone->setCustomerId($customer->getId())
                ->writeLog();
            if ($rule->getPointAction() == 'grant_points') {
                Mage::helper('milestone')->processEarnPoint('inactivity', $customer, $rule);
            } else if ($rule->getPointAction() == 'customergroup') {
                Mage::helper('milestone')->processChangeGroup($customer, $milestone);
            }
            $message      = Mage::helper('milestone')->getMilestoneMessage($rule);
            $helperConfig = Mage::helper('milestone/config');
            try {
                Mage::helper('milestone')->sendMilestoneEmail($rule, $customer, $helperConfig->getInactivityEmailTemplate(), $message);
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }

        return $this;

    }

    /**
     * @param $rule
     */
    public function getCustomersCollectionByRule($rule, $milestone)
    {
        $fromDate  = $rule->getFromDate();
        $toDate    = $rule->getToDate();
        $today     = Mage::helper('milestone')->getLocalMidnightInUtcTimestamp();
        $threshold = intval($milestone->getMilestoneInactivity());
        if ($today < strtotime($fromDate) ||
            (!empty($toDate) && $today > strtotime($toDate))
        ) {
            return new Varien_Data_Collection();
        }
        $targetDate               = array();
        $targetDate['today']      = Mage::helper('milestone')->getDateStringXDaysAgo($threshold - 1);
        $targetDate['rule_start'] = Mage::helper('milestone')->getDateStringXDaysAgo($threshold, $fromDate);
        $previouslyExecuted       = Mage::getModel('milestone/log')->getCollection()
            ->addFieldToFilter('rule_id', $milestone->getId())
            ->getSelect()->reset(Zend_Db_Select::COLUMNS)
            ->columns('customer_id');
        $customers                = Mage::getResourceModel('customer/customer_collection')
            ->addAttributeToSelect('*')
            ->addFieldToFilter('group_id', array('in' => explode(',', $rule->getCustomerGroupIds())))
            ->addFieldToFilter('website_id', array('in' => explode(',', $rule->getWebsiteIds())))
            ->addFieldToFilter('entity_id', array('nin' => $previouslyExecuted));
        $storeIds                 = $this->_getHelper()->getStoreIdsFromWebsites($rule->getWebsiteIds());
        $this->addLastLoginToSelect($customers, $storeIds);
        $customers->getSelect()->having("`last_login` < '{$targetDate['today']}'");
        $customers->getSelect()->having("`last_login` >= '{$targetDate['rule_start']}'");

        return $customers;
    }

    public function addLastLoginToSelect($customers, $storeIds = null)
    {
        if ($this->_logJoined) {
            return $this;
        }

        $logTable = $customers->getTable('log/customer');

        $customers->getSelect()->join(
            array('log' => $logTable),
            'e.entity_id = log.customer_id',
            array(
                "last_login" => "MAX(log.login_at)",
            )
        );

        if (!empty($storeIds)) {
            if (!is_array($storeIds)) {
                $storeIds = array($storeIds);
            }

            $customers->getSelect()->where("`log`.`store_id` IN('" . implode("','", $storeIds) . "')");
        }

        $customers->getSelect()->group('log.customer_id');

        $this->_logJoined = true;

        return $this;
    }
}
