<?php

class Magegiant_GiantPointsMilestone_Model_Behavior_Rule_Action_Milestone_Inactivity extends Magegiant_GiantPointsMilestone_Model_Behavior_Rule_Action_Abstract implements Magegiant_GiantPoints_Model_Rule_Action_Interface, Magegiant_GiantPointsMilestone_Model_Behavior_Rule_Action_Interface
{
    const ACTION_CODE = 'giantpoints_customer_milestone_inactivity';

    public function getConditionLabel()
    {
        return Mage::helper('giantpoints')->__('[Milestone] - Inactivity period');
    }

    public function getFieldLabel()
    {
        return Mage::helper('giantpoints')->__('Inactive Days');
    }

    public function getFieldComments()
    {
        return Mage::helper('giantpoints')->__('(days). Magento\'s Cron must be <a href="http://go.magegiant.com/cronjob" target="_blank">setup</a>.');
    }

    public function getConditionCode()
    {
        return self::ACTION_CODE;
    }
}