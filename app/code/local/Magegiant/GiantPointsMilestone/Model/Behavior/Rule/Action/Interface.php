<?php
/**
 * MageGiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageGiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPoints
 * @copyright   Copyright (c) 2014 MageGiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * Giantpoints Resource Model
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPoints
 * @author      MageGiant Developer
 */
interface Magegiant_GiantPointsMilestone_Model_Behavior_Rule_Action_Interface
{
    /**
     * get codition label for field
     *
     * @return String
     */
    public function getConditionLabel();

    /**
     * Add field for field which added in behavior rule response with condition
     *
     * @return mixed
     */

    public function getFieldLabel();

    /**
     * Add comment for field added
     *
     * @return mixed
     */
    public function getFieldComments();

    /**
     * Get condion code
     *
     * @return mixed
     */
    public function getConditionCode();

}
