<?php

class Magegiant_GiantPointsMilestone_Model_Behavior_Rule_Action_Abstract extends Magegiant_GiantPoints_Model_Rule_Action_Abstract
{

    public function getCustomerConditions()
    {

        return array(
            $this->getConditionCode() => $this->getConditionLabel(),
        );
    }

    public function visitAdminTriggers(&$fieldset)
    {
        /*Add Milestone Condition*/
        $element = $fieldset->addField($this->getConditionCode(), 'text', array(
            'name'               => $this->getConditionCode(),
            'required'           => $this->_isFieldRequired(),
            'class'              => ($this->_isFieldPositiveNumber() ? "validate-greater-than-zero " : "") .
                ($this->_isFieldInteger() ? "validate-digits " : ""),
            'label'              => $this->getFieldLabel(),
            'after_element_html' => "<p class='note'><span>{$this->getFieldComments()}</span></p>",
        ));

        return $this;
    }

    public function visitAdminActions(&$fieldset)
    {
        // Only if this condition supports the customer group action
        if (array_key_exists('customergroup', $this->getNewActions())) {
            $customergroupFieldId = 'customer_group_id';
            // Avoid duplicates,
            if ($fieldset->getElements()->searchById($customergroupFieldId) === null) {
                $customerGroups = Mage::getResourceModel('customer/group_collection')
                    ->addFieldToFilter('customer_group_id', array('neq' => 0));

                $fieldset->addField($customergroupFieldId, 'select', array(
                    'name'     => $customergroupFieldId,
                    'label'    => Mage::helper('giantpoints')->__("New Customer Group"),
                    'options'  => $customerGroups->load()->toOptionHash(),
                    'required' => true,
                ));
            }
        }

        return $this;
    }

    public function getNewActions()
    {
        return array('customergroup' => Mage::helper('giantpoints')->__('Change customer group'));
    }

    public function getAdminFormScripts()
    {
        $conditionArray = array(
            'elementId'  => "rule_{$this->getConditionCode()}",
            'isRequired' => $this->_isFieldRequired(),
        );

        $actionArray     = $this->getNewActions();
        $conditionObject = Mage::helper('core')->jsonEncode($conditionArray);
        $actionObject    = Mage::helper('core')->jsonEncode($actionArray);
        $script          = "
            	MagegiantGiantPoints = (typeof MagegiantGiantPoints === 'undefined') ? {} : MagegiantGiantPoints;
	            MagegiantGiantPoints.milestone = (typeof MagegiantGiantPoints.milestone === 'undefined') ? {} : MagegiantGiantPoints.milestone;
	            MagegiantGiantPoints.milestone.milestoneFields = (typeof MagegiantGiantPoints.milestone.milestoneFields === 'undefined') ? {} : MagegiantGiantPoints.milestone.milestoneFields;
	            MagegiantGiantPoints.milestone.milestoneActions = (typeof MagegiantGiantPoints.milestone.milestoneActions === 'undefined') ? {} : MagegiantGiantPoints.milestone.milestoneActions;

	            MagegiantGiantPoints.milestone.milestoneFields['{$this->getConditionCode()}'] = {$conditionObject};
	            MagegiantGiantPoints.milestone.milestoneActions['{$this->getConditionCode()}'] = {$actionObject};
        ";

        return array($script);
    }

    public function getAdminFormInitScripts()
    {
        return array();
    }

    /**
     * @return ture if the "validate-integer" class should be added to the field for validation.
     */
    protected function _isFieldInteger()
    {
        return true;
    }

    protected function _isFieldRequired()
    {
        return true;
    }

    protected function _isFieldPositiveNumber()
    {
        return true;
    }

}