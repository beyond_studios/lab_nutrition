<?php

class Magegiant_GiantPointsMilestone_Model_Behavior_Rule_Action_Milestone_Revenue extends Magegiant_GiantPointsMilestone_Model_Behavior_Rule_Action_Abstract implements Magegiant_GiantPoints_Model_Rule_Action_Interface, Magegiant_GiantPointsMilestone_Model_Behavior_Rule_Action_Interface
{
    const ACTION_CODE = 'giantpoints_customer_milestone_revenue';

    public function getConditionLabel()
    {
        return Mage::helper('giantpoints')->__('[Milestone] - Reach lifetime sales');
    }

    public function getFieldLabel()
    {
        return Mage::helper('giantpoints')->__('Lifetime sales');
    }

    public function getFieldComments()
    {
        return "<strong>[" . (string)Mage::app()->getStore()->getBaseCurrencyCode() . "]</strong>";
    }

    public function getConditionCode()
    {
        return self::ACTION_CODE;
    }
}