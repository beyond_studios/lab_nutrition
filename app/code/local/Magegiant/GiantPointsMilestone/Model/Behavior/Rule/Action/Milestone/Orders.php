<?php

class Magegiant_GiantPointsMilestone_Model_Behavior_Rule_Action_Milestone_Orders extends Magegiant_GiantPointsMilestone_Model_Behavior_Rule_Action_Abstract implements Magegiant_GiantPoints_Model_Rule_Action_Interface, Magegiant_GiantPointsMilestone_Model_Behavior_Rule_Action_Interface
{
    const ACTION_CODE = 'giantpoints_customer_milestone_orders';

    public function getConditionLabel()
    {
        return Mage::helper('milestone')->__('[Milestone] - Reach number of orders');
    }

    public function getFieldLabel()
    {
        return Mage::helper('milestone')->__('Number of Orders');
    }

    public function getConditionCode()
    {
        return self::ACTION_CODE;
    }

    public function getFieldComments()
    {
        return '';
    }
}