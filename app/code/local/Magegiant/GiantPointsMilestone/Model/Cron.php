<?php
/**
 * MageGiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageGiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsMilestone
 * @copyright   Copyright (c) 2014 MageGiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * GiantPointsMilestone Cron Model
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsMilestone
 * @author      MageGiant Developer
 */
class Magegiant_GiantPointsMilestone_Model_Cron
{
    /**
     * Daily cron check mileston rule
     */
    public function checkMilestoneRule()
    {
        $this->_processMembership();
        $this->_processInactivity();

        return $this;
    }

    /**
     * Process when customer reached milestone membership
     */
    protected function _processMembership()
    {
        $condition = Magegiant_GiantPointsMilestone_Model_Behavior_Rule_Action_Milestone_Membership::ACTION_CODE;
        $rules     = Mage::getModel('giantpoints/rule')->getAllRulesByCondition($condition);
        foreach ($rules as $rule) {
            $mile_stone = Mage::getModel('milestone/rule')->load($rule->getId(), 'behavior_rule_id');
            if (!$mile_stone || !$mile_stone->getId())
                continue;
            $mile_stone->getValidation()->validate($rule, $mile_stone);

        }

        return $this;
    }

    /**
     * process when custom reached inactivity time
     */
    protected function _processInactivity()
    {
        $condition = Magegiant_GiantPointsMilestone_Model_Behavior_Rule_Action_Milestone_Inactivity::ACTION_CODE;
        $rules     = Mage::getModel('giantpoints/rule')->getAllRulesByCondition($condition);
        foreach ($rules as $rule) {
            $mile_stone = Mage::getModel('milestone/rule')->load($rule->getId(), 'behavior_rule_id');
            if (!$mile_stone || !$mile_stone->getId())
                continue;
            $mile_stone->getValidation()->validate($rule, $mile_stone);

        }

        return $this;
    }


}