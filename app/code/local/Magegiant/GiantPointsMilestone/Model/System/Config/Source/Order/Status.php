<?php
/**
 * MageGiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageGiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPoints
 * @copyright   Copyright (c) 2014 MageGiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * Giantpoints Model
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPoints
 * @author      MageGiant Developer
 */
class Magegiant_GiantPointsMilestone_Model_System_Config_Source_Order_Status
{
    const ORDER_CREATE   = 'create';
    const ORDER_PAYMENT  = 'payment';
    const ORDER_SHIPMENT = 'shipment';

    public function toOptionArray()
    {
        return array(
            array(
                'value' => self::ORDER_CREATE,
                'label' => Mage::helper('giantpoints')->__('Order Is Created')
            ),
            array(
                'value' => self::ORDER_PAYMENT,
                'label' => Mage::helper('giantpoints')->__('Order Is Paid For')
            ),
            array(
                'value' => self::ORDER_SHIPMENT,
                'label' => Mage::helper('giantpoints')->__('Order Is Shipped')
            ),
        );
    }
}

