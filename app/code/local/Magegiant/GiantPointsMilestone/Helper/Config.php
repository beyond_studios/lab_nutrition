<?php
/**
 * MageGiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageGiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPoints
 * @copyright   Copyright (c) 2014 MageGiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * GiantPoints Helper
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPoints
 * @author      MageGiant Developer
 */
class Magegiant_GiantPointsMilestone_Helper_Config extends Mage_Core_Helper_Abstract
{
    const GIANTPOINTS_MILESTONE                                   = 'giantpoints/milestone/';
    const GIANTPOINTS_MILESTONE_GENERAL_EMAIL_TEMPLATE_DEFAULT    = 'giantpoints_milestone_general_notification';
    const GIANTPOINTS_MILESTONE_INACTIVITY_EMAIL_TEMPLATE_DEFAULT = 'giantpoints_milestone_inactivity_notification';
    protected $_locale;


    /*=============Begin register config==========*/
    public function getMilestoneConfig($code, $storeId = null)
    {
        $storeId = !is_null($storeId) ? $storeId : Mage::app()->getStore()->getId();

        return Mage::getStoreConfig(self::GIANTPOINTS_MILESTONE . $code, $storeId);
    }

    /**
     * get orders trigger config
     *
     *
     * @param null $store
     * @return mixed
     */
    public function getOrdersTrigger($store = null)
    {
        return $this->getMilestoneConfig('orders_trigger', $store);
    }

    /**
     * GEt referals trigger config
     *
     * @param null $store
     * @return mixed
     */
    public function getReferralsTrigger($store = null)
    {
        return $this->getMilestoneConfig('referrals_trigger', $store);
    }

    /**
     * @param null $store
     * @return mixed|string
     */
    public function getOrderEmailTemplate($store = null)
    {
        $template = $this->getMilestoneConfig('orders_email_template', $store);

        return $template && strpos($template, 'giantpoints_milestone') === false ? $template : $this->getGeneralEmailTemplateDefault();
    }

    /**
     * @param null $store
     * @return mixed|string
     */
    public function getRevenueEmailTemplate($store = null)
    {
        $template = $this->getMilestoneConfig('revenue_email_template', $store);

        return $template && strpos($template, 'giantpoints_milestone') === false ? $template : $this->getGeneralEmailTemplateDefault();
    }

    public function getReferralsEmailTemplate($store = null)
    {
        $template = $this->getMilestoneConfig('referrals_email_template', $store);

        return $template && strpos($template, 'giantpoints_milestone') === false ? $template : $this->getGeneralEmailTemplateDefault();
    }

    public function getMemberShipEmailTemplate($store = null)
    {
        $template = $this->getMilestoneConfig('membership_email_template', $store);

        return $template && strpos($template, 'giantpoints_milestone') === false ? $template : $this->getGeneralEmailTemplateDefault();
    }

    public function getInactivityEmailTemplate($store = null)
    {
        $template = $this->getMilestoneConfig('inactivity_email_template', $store);

        return $template && strpos($template, 'giantpoints_milestone') === false ? $template : $this->getInactiveEmailTemplateDefault();
    }

    public function getGeneralEmailTemplateDefault()
    {
        return self::GIANTPOINTS_MILESTONE_GENERAL_EMAIL_TEMPLATE_DEFAULT;
    }

    public function getInactiveEmailTemplateDefault()
    {
        return self::GIANTPOINTS_MILESTONE_INACTIVITY_EMAIL_TEMPLATE_DEFAULT;
    }
}