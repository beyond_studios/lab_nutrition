<?php
/**
 * MageGiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageGiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsMilestone
 * @copyright   Copyright (c) 2014 MageGiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * GiantPointsMilestone Helper
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsMilestone
 * @author      MageGiant Developer
 */
class Magegiant_GiantPointsMilestone_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_EMAIL_SENDER = 'giantpoints/email/sender';

    /**
     * get Store_Ids by Websites
     *
     * @param $websiteIds
     * @return array
     */
    public function getStoreIdsFromWebsites($websiteIds)
    {
        $websiteIds = is_array($websiteIds) ? $websiteIds : array($websiteIds);
        $storeIds   = array();
        foreach ($websiteIds as $websiteId) {
            $newStoreIds = Mage::app()->getWebsite($websiteId)->getStoreIds();
            $storeIds    = array_merge($storeIds, $newStoreIds);
        }

        return $storeIds;
    }

    /**
     * @param $type
     * @param $customer
     * @param $rule
     * @return $this
     */
    public function processEarnPoint($type, $customer, $rule)
    {
        $obj            = new Varien_Object(array(
            'point_amount'    => $rule->getPointAmount(),
            'onhold_duration' => $rule->getOnholdDuration()
        ));
        $additionalData = array(
            'customer'      => $customer,
            'action_object' => $obj,
        );
        $action_type    = 'milestone_' . $type;
        Mage::helper('giantpoints/action')->createTransaction(
            $action_type, $additionalData
        );

        return $this;
    }

    /**
     * @param $customer
     * @param $milestone
     */
    public function processChangeGroup($customer, $milestone)
    {
        try {
            $customer->setGroupId($milestone->getCustomerGroupId())
                ->save();
        } catch (Exception $e) {
            Mage::helper('giantpoints')->log($e->getMessage());
        }
    }

    public function sendMilestoneEmail($rule, $customer, $template, $message = null)
    {
        /* @var $translate Mage_Core_Model_Translate */
        $translate = Mage::getSingleton('core/translate');
        $translate->setTranslateInline(false);
        /* @var $email Mage_Core_Model_Email_Template */
        $email = Mage::getModel('core/email_template');

        $email->setDesignConfig(array(
                'area'  => 'frontend',
                'store' => $customer->getStoreId())
        );
        $mile_stone = Mage::getModel('milestone/rule')->load($rule->getId(), 'behavior_rule_id');
        $vars       = array(
            'logo_url'              => Mage::getDesign()->getSkinUrl(Mage::getStoreConfig('design/header/logo_src')),
            'logo_alt'              => Mage::getStoreConfig('design/header/logo_alt'),
            'customer_name'         => $customer->getName(),
            'customer_firstname'    => $customer->getFirstname(),
            'customer_email'        => $customer->getEmail(),
            'store_name'            => $customer->getStore()->getName(),
            'store_name'            => $customer->getStore()->getName(),
            'milestone_message'     => !empty($message) ? $message : "",
            'milestone_description' => $rule->getDescription(),
        );
        $additions  = array();
        if ($mile_stone->getPointsConditions() == Magegiant_GiantPointsMilestone_Model_Behavior_Rule_Action_Milestone_Inactivity::ACTION_CODE) {
            if ($mile_stone->getPointAction() == 'grant_points') {
                $additions = array(
                    'inactive_day' => $mile_stone->getMilestoneInactivity(),
                    'point_amount' => $rule->getPointAmount(),
                    'point_string' => Mage::helper('giantpoints')->addLabelForPoint($rule->getPointAmount(), $customer->getStoreId()),
                );
            } else if ($mile_stone->getPointAction() == 'customergroup') {
                $additions = array(
                    'inactive_day'      => $mile_stone->getMilestoneInactivity(),
                    'customer_group_id' => $rule->getCustomerGroupId(),
                );
            }
        }
        $vars = array_merge($vars, $additions);
        $email->sendTransactional($template, Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER, $customer->getStoreId()),
            $customer->getEmail(), $customer->getName(), $vars);
        $translate->setTranslateInline(true);

        return $email->getSentSuccess();
    }

    public function getMilestoneMessage($rule)
    {
        $message = '';
        if ($rule->getPointAction() == 'grant_points') {
            $message = $this->__('You have earned %s', Mage::helper('giantpoints')->addLabelForPoint($rule->getPointAmount()));
        } else if ($rule->getPointAction() == 'customergroup') {
            $message = $this->__('Your new group is %s', $this->getCustomerGroupName($rule->getCustomerGroupId()));
        }

        return $message;
    }

    public function getCustomerGroupName($groupId)
    {
        return Mage::getModel('customer/group')->load($groupId)->getCustomerGroupCode();
    }

    /**
     * @param null $localTimestamp
     * @return mixed
     */
    public function getLocalMidnightInUtcTimestamp($localTimestamp = null)
    {
        $localTimestamp = !empty($localTimestamp) ? $localTimestamp : $this->getLocalTimestamp();
        $midnight       = $this->getNormalizedDateString($localTimestamp);

        return $this->getUtcTimestamp($midnight);
    }

    /**
     * @param      $numberOfDaysAgo
     * @param null $startDate
     * @return mixed
     */
    public function getDateStringXDaysAgo($numberOfDaysAgo, $startDate = null)
    {
        $numberOfDaysAgo = max(0, $numberOfDaysAgo);
        if (empty($startDate)) {
            $startDate = $this->getLocalMidnightInUtcTimestamp();
        }

        if (!is_numeric($startDate)) {
            $startDate = strtotime($startDate);
        }

        $targetDate = strtotime("-{$numberOfDaysAgo} day", $startDate);

        return $this->getMySqlDateString($targetDate);
    }

    /**
     * Accepts any date string and returns date in format of "Y-m-d H:i:s" ideal for db queries.
     * Ignores timezone, so if you're using this for DB queries, you should pass in UTC time.
     *
     * @param int|string $dateString any date string parsable by php's strtotime() function. Will use UTC time if not specified
     * @return string db formatted date.
     */
    public function getMySqlDateString($dateString = null, $format = null)
    {
        $format = !empty ($format) ? $format : "Y-m-d H:i:s";

        if (empty($dateString)) {
            $timestamp = $this->getUtcTimestamp();
        } else if (is_numeric($dateString)) {
            $timestamp = $dateString;
        } else {
            $timestamp = strtotime($dateString);
        }

        return date($format, $timestamp);
    }

    /**
     * Accepts any date string and returns date in format of "Y-m-d" (reset to midnight) for db queries.
     * Ignores timezone, so if you're using this for DB queries, you should pass in UTC time.
     *
     * @param int|string $dateString any date string parsable by php's strtotime() function. Will use UTC time if not specified
     * @return string db formatted date.
     */
    public function getNormalizedDateString($dateString = null)
    {
        return $this->getMySqlDateString($dateString, "Y-m-d");
    }

    /**
     * Accepts a utc timestamp and returns a timestamp in local store timezone.
     * UTC time is what is stored in the DB.
     *
     * @see Mage_Core_Model_Date::timestamp()
     * @param int|string $input date in UTC/GMT timezone
     * @return int
     */
    public function getLocalTimestamp($utcTimestamp = null)
    {
        $utcTimestamp = empty($utcTimestamp) ? null : $utcTimestamp;

        return Mage::getModel('core/date')->timestamp($utcTimestamp);
    }

    /**
     * Accepts a local timestamp (in the store time-zone) and returns a UTC timestamp.
     * UTC time is what is stored in the DB.
     *
     * @see Mage_Core_Model_Date::gmtTimestamp()
     * @param int|string $localTimestamp date in current timezone
     * @return int
     */
    public function getUtcTimestamp($localTimestamp = null)
    {
        $localTimestamp = empty($localTimestamp) ? null : $localTimestamp;

        return Mage::getModel('core/date')->gmtTimestamp($localTimestamp);
    }

}