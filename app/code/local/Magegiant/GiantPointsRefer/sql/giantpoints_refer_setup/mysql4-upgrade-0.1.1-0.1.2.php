<?php

$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn($this->getTable('sales/order_item'), 'referral_earn_brian', 'int(11) NOT NULL default 0');

$installer->endSetup();