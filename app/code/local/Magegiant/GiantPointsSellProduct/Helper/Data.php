<?php
/**
 * MageGiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageGiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsSellProduct
 * @copyright   Copyright (c) 2014 MageGiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * GiantPointsSellProduct Helper
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsSellProduct
 * @author      MageGiant Developer
 */
class Magegiant_GiantPointsSellProduct_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_ENABLED = 'giantpoints/giantpointssell/is_enabled';

    public function isEnabled($store = null)
    {
        return Mage::getStoreConfig(self::XML_PATH_ENABLED, $store);
    }
}