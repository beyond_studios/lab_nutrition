<?php
/**
 * Magegiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the magegiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPointsSellProduct
 * @copyright   Copyright (c) 2014 Magegiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * Giantpointssellproduct Adminhtml Controller
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPointsSellProduct
 * @author      Magegiant Developer
 */
class Magegiant_GiantPointsSellProduct_Adminhtml_Giantpointssellproductadmin_SellproductController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('giantpoints/giantpointssell')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));

        return $this;
    }

    public function sellAction()
    {
        $this->_title($this->__('Reward Points'))
            ->_title($this->__('Sell Product By Points'));
        $this->loadLayout()->_setActiveMenu('giantpoints/giantpointssell');
        $this->_addContent($this->getLayout()->createBlock('giantpointssell/adminhtml_sellproducts_edit'))
            ->_addLeft($this->getLayout()->createBlock('giantpointssell/adminhtml_sellproducts_edit_tabs'));
        $this->renderLayout();
    }

    public function sellProductGridAction()
    {
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('giantpointssell/adminhtml_sellproducts_edit_tab_grid', 'admin.giantpointssell.sellproducts')->toHtml()
        );
    }

    public function saveSellAction()
    {
        $data = $this->getRequest()->getPost();
        if ($data) {
            try {
                foreach ($data['giantpoints_sell_product'] as $key => $value) {
                    if (substr_count($key, 'giant_') == 1 && $value != '') {
                        $product    = explode('giant_', $key);
                        $product_id = $product[1];
                        if ($value == 0) $value = '';
                        $attributesData = array('giantpoints_sell_product' => $value);

                        Mage::getSingleton('catalog/product_action')->updateAttributes(array($product_id), $attributesData, 0);

                    }

                }
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('giantpointssell')->__('The reward points has been saved successfully!'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                $this->_redirect('*/*/sell');

                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/sell');

                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('giantpointssell')->__('Unable to find product to save'));
        $this->_redirect('*/*/sell');
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('giantpoints/giantpointssell');
    }

}