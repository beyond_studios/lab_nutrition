<?php
/**
 * Magegiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the magegiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPointsSellProduct
 * @copyright   Copyright (c) 2014 Magegiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * GiantPointsSellProduct Index Controller
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPointsSellProduct
 * @author      Magegiant Developer
 */
class Magegiant_GiantPointsSellProduct_Block_Adminhtml_Sellproducts_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('products_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('giantpointssell')->__('Sell products by points'));
    }

    protected function _beforeToHtml()
    {

        $this->addTab('form_products', array(
            'label'   => Mage::helper('giantpointssell')->__('Products'),
            'title'   => Mage::helper('giantpointssell')->__('Products'),
            'content' => $this->getLayout()->createBlock('giantpointssell/adminhtml_sellproducts_edit_tab_grid')->toHtml(),
        ));


        return parent::_beforeToHtml();
    }
}