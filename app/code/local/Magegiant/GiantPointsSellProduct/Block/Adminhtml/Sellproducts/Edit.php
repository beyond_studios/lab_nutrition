<?php

/**
 * Magegiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the magegiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPointsSellProduct
 * @copyright   Copyright (c) 2014 Magegiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * GiantPointsSellProduct Index Controller
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPointsSellProduct
 * @author      Magegiant Developer
 */
class Magegiant_GiantPointsSellProduct_Block_Adminhtml_Sellproducts_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId   = 'product_id';
        $this->_blockGroup = 'giantpointssell';
        $this->_controller = 'adminhtml_sellproducts';

        $this->_updateButton('save', 'label', Mage::helper('giantpointssell')->__('Save'));
        $this->_removeButton('delete');
        $this->_removeButton('back');

    }

    public function getHeaderText()
    {
        return Mage::helper('giantpointssell')->__('Sell products by points');

    }
}