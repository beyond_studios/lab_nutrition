<?php
/**
 * Magegiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the magegiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPointsSellProduct
 * @copyright   Copyright (c) 2014 Magegiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * GiantPointsSellProduct Index Controller
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPointsSellProduct
 * @author      Magegiant Developer
 */
class Magegiant_GiantPointsSellProduct_Block_Adminhtml_Renderer_Sellproduct extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $id     = $row['entity_id'];
        $name   = 'giantpoints_sell_product[giant_' . $id . ']';
        $value  = $row['giantpoints_sell_product'];
        $result = $value . "  <input type='text' class='input-text validate-number validate-digits' name=" . $name . " value=" . $value . "></input>";

        return $result;
    }

}