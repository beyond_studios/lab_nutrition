<?php

/**
 * MageGiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magegiant.com license that is
 * available through the world-wide-web at this URL:
 * https://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @copyright   Copyright (c) 2014 Magegiant (http://magegiant.com/)
 * @license     https://magegiant.com/license-agreement/
 */
class Magegiant_GiantPointsSellProduct_Model_Tax_Sales_Total_Quote_Rewrite_Tax extends Mage_Tax_Model_Sales_Total_Quote_Tax
{
    protected function _calcUnitTaxAmount(Mage_Sales_Model_Quote_Item_Abstract $item, $rate)
    {
        $qty       = $item->getTotalQty();
        $inclTax   = $item->getIsPriceInclTax();
        $price     = $item->getTaxableAmount() + $item->getExtraTaxableAmount();
        $basePrice = $item->getBaseTaxableAmount() + $item->getBaseExtraTaxableAmount();
        $rateKey   = (string)$rate;
        $item->setTaxPercent($rate);

        $hiddenTax     = null;
        $baseHiddenTax = null;
        switch ($this->_config->getCalculationSequence($this->_store)) {
            case Mage_Tax_Model_Calculation::CALC_TAX_BEFORE_DISCOUNT_ON_EXCL:
            case Mage_Tax_Model_Calculation::CALC_TAX_BEFORE_DISCOUNT_ON_INCL:
                $unitTax     = $this->_calculator->calcTaxAmount($price, $rate, $inclTax);
                $baseUnitTax = $this->_calculator->calcTaxAmount($basePrice, $rate, $inclTax);
                break;
            case Mage_Tax_Model_Calculation::CALC_TAX_AFTER_DISCOUNT_ON_EXCL:
            case Mage_Tax_Model_Calculation::CALC_TAX_AFTER_DISCOUNT_ON_INCL:
                $discountAmount      = $item->getDiscountAmount() / $qty;
                $baseDiscountAmount  = $item->getBaseDiscountAmount() / $qty;
                $unitTax             = $this->_calculator->calcTaxAmount($price, $rate, $inclTax);
                $discountRate        = $price ? ($unitTax / $price) * 100 : 0;
                $unitTaxDiscount     = $this->_calculator->calcTaxAmount($discountAmount, $discountRate, $inclTax, false);
                $unitTax             = max($unitTax - $unitTaxDiscount, 0);
                $baseUnitTax         = $this->_calculator->calcTaxAmount($basePrice, $rate, $inclTax);
                $baseDiscountRate    = $basePrice ? ($baseUnitTax / $basePrice) * 100 : 0;
                $baseUnitTaxDiscount = $this->_calculator
                    ->calcTaxAmount($baseDiscountAmount, $baseDiscountRate, $inclTax, false);
                $baseUnitTax         = max($baseUnitTax - $baseUnitTaxDiscount, 0);

                if ($inclTax && $discountAmount > 0) {
                    $hiddenTax            = $this->_calculator->calcTaxAmount($discountAmount, $rate, $inclTax, false);
                    $baseHiddenTax        = $this->_calculator->calcTaxAmount($baseDiscountAmount, $rate, $inclTax, false);
                    $this->_hiddenTaxes[] = array(
                        'rate_key'   => $rateKey,
                        'qty'        => $qty,
                        'item'       => $item,
                        'value'      => $hiddenTax,
                        'base_value' => $baseHiddenTax,
                        'incl_tax'   => $inclTax,
                    );
                } elseif ($discountAmount > $price) { // case with 100% discount on price incl. tax
                    $hiddenTax            = $discountAmount - $price;
                    $baseHiddenTax        = $baseDiscountAmount - $basePrice;
                    $this->_hiddenTaxes[] = array(
                        'rate_key'   => $rateKey,
                        'qty'        => $qty,
                        'item'       => $item,
                        'value'      => $hiddenTax,
                        'base_value' => $baseHiddenTax,
                        'incl_tax'   => $inclTax,
                    );
                }
                break;
        }
        $item->setTaxAmount($this->_store->roundPrice(max(0, $qty * $unitTax)));
        $item->setBaseTaxAmount($this->_store->roundPrice(max(0, $qty * $baseUnitTax)));

        return $this;
    }

}