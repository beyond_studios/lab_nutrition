<?php
/**
 * MageGiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageGiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsSellProduct
 * @copyright   Copyright (c) 2014 MageGiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * GiantPointsSellProduct Observer Model
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsSellProduct
 * @author      MageGiant Developer
 */
class Magegiant_GiantPointsSellProduct_Model_Observer
{
    protected $_flag = array();

    /**
     * process controller_action_predispatch event
     *
     * @return Magegiant_GiantPointsSellProduct_Model_Observer
     */
    public function catalogProductGetFinalPrice($observer)
    {
        $event = $observer->getEvent();
        if (!$event)
            return $this;
        $product = $event->getProduct();
        if ($product->getData('giantpoints_sell_product') > 0) {
            $product->setSpecialPrice(1);
            $product->setFinalPrice(0);
        }

        return $this;

    }

    /**
     * With sell product in points, change final price to zero
     *
     * @param $observer
     * @return $this
     */
    public function catalogProductCollectionLoadAfter($observer)
    {
        $event = $observer->getEvent();
        if (!$event)
            return $this;
        $products = $event->getCollection();

        foreach ($products as $product) {
            $productData = Mage::getModel('catalog/product')->load($product->getId());
            $sellPoints  = $productData->getData('giantpoints_sell_product');
            if ($sellPoints > 0) {
                $product->setSpecialPrice(1);
                $product->setFinalPrice(0);
                $product->setData('giantpoints_sell_product', $sellPoints);
            }
        }

        return $this;
    }

    /**
     * Check sell product in points when add product to cart
     *
     * @param $observer
     * @return $this
     */
    public function collectTotalBefore($observer)
    {
        if (!$this->_isEnabled()) {
            return $this;
        }
        $event = $observer->getEvent();
        if (!$event) {
            return $this;
        }
        $quote   = $event->getQuote();
        $address = $this->getAddress($quote);
        $items   = $address->getAllItems();
        $session = Mage::getSingleton('checkout/session');
        if (!count($items)) {
            return $this;
        }
        foreach ($items as $item) {
            if ($item->getParentItemId()) {
                continue;
            }
            $product = $item->getProduct();
            if ($sellPoints = $product->getData('giantpoints_sell_product')) {
                $session->setData('is_used_point', true);
                if (!Mage::helper('giantpoints/customer')->getCustomer()) {
                    $quote->removeItem($item->getId())->save();
                    Mage::getSingleton('checkout/session')->getMessages(true);
                    Mage::getSingleton('checkout/session')->addError(Mage::helper('giantpoints')->__('For using points to checkout order, please login!'));
                } else {
                    $max_point = Mage::helper('giantpointsrule/conversion_spending')->getCustomerPoint($item->getId());
                    if ($sellPoints > $max_point) {
                        $quote->removeItem($item->getId())->save();
                        Mage::getSingleton('checkout/session')->getMessages(true);
                        Mage::getSingleton('checkout/session')->addError(Mage::helper('giantpoints')->__('Your balance has not enough point to spend!'));
                    }
                }
            }

        }

        return $this;
    }

    /**
     *
     * @param $observer
     */
    public function quoteAddressDiscountItemAfter($observer)
    {
        $event = $observer->getEvent();
        if (!$event) {
            return $this;
        }
        $item  = $event->getItem();
        $quote = $item->getQuote();
        if (!$item->getId() || !$quote->getCustomerId())
            return $this;
        $product    = $item->getProduct();
        $sellPoints = $product->getData('giantpoints_sell_product') * $item->getQty();
        if (!$sellPoints)
            return $this;
        $max_point = Mage::helper('giantpointsrule/conversion_spending')->getCustomerPoint($item->getId());
        if ($sellPoints > $max_point) {
            try {
                $quote->removeItem($item->getId())
                    ->save();
                Mage::getSingleton('checkout/session')->getMessages(true);
                Mage::getSingleton('checkout/session')->addError(Mage::helper('giantpoints')->__('Your balance has not enough point to spend!'));

            } catch (Exception $e) {
            }

            return $this;
        }
        $item->setGiantpointsSpent($item->getGiantpointsSent() + $sellPoints);
        $item->setSellInPoints($sellPoints);

        return $this;
    }

    /**
     * Add more GiantpointsSpent
     *
     * @param $observer
     * @return $this
     */
    public function quoteCollectSpendingAfter($observer)
    {
        if (!$this->_isEnabled()) {
            return $this;
        }
        $event = $observer->getEvent();
        if (!$event) {
            return $this;
        }
        $address = $event->getAddress();
        $items   = $address->getAllItems();
        if (!count($items)) {
            return $this;
        }
        $item_count  = 0;
        $pointsSpent = 0;
        foreach ($items as $item) {
            $item_count++;
            if ($item->getParentItemId()) {
                continue;
            }
            if ($item->getHasChildren() && $item->isChildrenCalculated()) {
                foreach ($item->getChildren() as $child) {
                    $pointsSpent += $child->getSellInPoints();
                }
            } else {
                $pointsSpent += $item->getSellInPoints();
            }

        }
        if ($pointsSpent) {
            $address->setGiantpointsSpent($address->getGiantpointsSpent() + $pointsSpent);
        }

        return $this;


    }

    /**
     * Add more GiantpointsSpent per item
     *
     * @param $observer
     * @return $this
     */
    public function pointItemSpent($observer)
    {
        if (!$this->_isEnabled()) {
            return $this;
        }
        $event = $observer->getEvent();
        if (!$event) {
            return $this;
        }
        $container = $event->getContainer();
        $quote     = Mage::getSingleton('checkout/session')->getQuote();
        $address   = $this->getAddress($quote);
        $items     = $address->getAllItems();
        if (!count($items)) {
            return $this;
        }
        $pointsSpent = $container->getPointItemSpent();
        foreach ($items as $item) {
            if ($item->getParentItemId()) {
                continue;
            }
            if ($item->getHasChildren() && $item->isChildrenCalculated()) {
                foreach ($item->getChildren() as $child) {
                    $pointsSpent += $child->getSellInPoints();
                }
            } else {
                $pointsSpent += $item->getSellInPoints();
            }

        }
        if ($pointsSpent) {
            $container->setPointItemSpent($pointsSpent);
        }

        return $this;
    }

    private function getAddress($quote)
    {
        if ($quote->isVirtual()) {
            $address = $quote->getBillingAddress();
        } else {
            $address = $quote->getShippingAddress();
        }

        return $address;
    }

    /**
     * fix bug 1.7 tax divided by 0
     *
     * @param $observer
     * @return $this
     */
    public function pageLoadBeforeFront($observer)
    {
        if (!$this->_isEnabled() || Mage::helper('giantpoints/version')->isRawVerAtLeast('1.8.0.0')) {
            return $this;
        }
        $event = $observer->getEvent();
        if (!$event) {
            return $this;
        }
        $node  = Mage::getConfig()->getNode('global/models/tax/rewrite');
        $dnode = Mage::getConfig()->getNode('global/models/tax/drewrite/sales_total_quote_tax');
        $node->appendChild($dnode);

        return $this;
    }

    /**
     *
     * @param string $key
     * @return boolean
     */
    public function getFlag($key)
    {
        if (isset($this->_flag[$key])) {
            return (boolean)$this->_flag[$key];
        }

        return false;
    }

    /**
     *
     * @param string  $key
     * @param boolean $value
     * @return Magegiant_GiantPointsRule_Model_Observer
     */
    public function setFlag($key, $value = true)
    {
        $this->_flag[$key] = (boolean)$value;

        return $this;
    }

    protected function _isEnabled($store = null)
    {
        return (bool)Mage::helper('giantpointssell')->isEnabled($store);
    }
}