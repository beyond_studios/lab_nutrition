<?php

class Magegiant_GiantPoints_Model_Observermambo
{

    public function applyPoints($observer)
    {

        $event = $observer->getEvent();
        $quote = $event->getOrder();
        $invoice = $quote->prepareInvoice();

        if (!$invoice->getData('order_id')) {
            throw new Exception($this->__('Not found order to create transaction.'));
        }
        $payment = Mage::getModel('sales/order_payment')
            ->getCollection()
            ->addFieldToFilter('parent_id', $invoice->getData('order_id', 0))
            ->getFirstItem()
        ;
        $paymentMethodNotAllowed = array('mcperu_webpoint', 'visanet_eticket', 'appvisa');
        if(in_array($payment->getData('method'), $paymentMethodNotAllowed)){
            return ;
        }



        $customer = Mage::getModel('customer/customer')->load($quote->getData("customer_id"));

        $expire_days = Mage::helper('giantpoints/config')->getEarningConfig("expire_days");
        $pointsToAdd = $quote->getData("giantpoints_earn");
        $incrementId = $quote->getData("increment_id");

        //Mage::log("Entro a applyPoints");

        //Fin Implementacion

        if (!$customer && !$customer->getId()) {
            throw new Exception($this->__('Not found customer to create transaction.'));
        }



        // Si la orden viene de los moviles y los puntos ganados de esa orden son mayor que el invoice,
        // actualizo los puntos del invoice con el de la orden.
        if($pointsToAdd > 0 && $quote->getOmniStatus() == 1 &&
            $pointsToAdd >= $invoice->getData("giantpoints_earn")){
//            Mage::log('--#Punto cambiado por moviles#--', null, 'loyalty_.log');
            $rate = Mage::getModel('giantpoints/rate');
            $websiteId = 1;
            $direction = Magegiant_GiantPoints_Model_Rate::MONEY_TO_POINT;
            $rate->getResource()->loadRateByCustomerWebsiteDirection($rate, $customer, $websiteId, $direction);
            $pointsToAdd = (string)$rate->exchange($quote['subtotal']);
            $invoice->setGiantpointsEarn($pointsToAdd);
        }

        $obj            = new Varien_Object(array(
            'point_amount'   => $pointsToAdd,
            'comment'        => Mage::helper('giantpoints')->__('Gana puntos por orden de compra #%s', $incrementId),
            'expiration_day' => (int)$expire_days,
        ));


        $additionalData = array(
            'customer'      => $customer,
            'action_object' => $invoice,
            'notice'        => null,
        );

        $transaction    = Mage::helper('giantpoints/action')->createTransaction(
            'order_invoiced', $additionalData
        );

        return $this;

    }

}
