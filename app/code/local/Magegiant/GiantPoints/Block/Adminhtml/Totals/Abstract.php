<?php
/**
 * Magegiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the magegiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPoints
 * @copyright   Copyright (c) 2014 Magegiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * Giantpoints Adminhtml Block
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPoints
 * @author      Magegiant Developer
 */
class Magegiant_GiantPoints_Block_Adminhtml_Totals_Abstract extends Mage_Adminhtml_Block_Template
{
    /**
     * @return Mage_Sales_Model_Order_Creditmemo
     */
    public function getCreditmemo()
    {
        return Mage::registry('current_creditmemo');
    }

    public function getSpendingHelper()
    {
        return Mage::helper('giantpoints/calculation_spending');
    }

    public function getEarningHelper()
    {
        return Mage::helper('giantpoints/calculation_earning');
    }

    /**
     * get max point can deduct from customer balance
     *
     * @return int
     */
    public function getMaxInviteeEarnedRefund()
    {
        return $this->getEarningHelper()->getMaxInviteeEarnedRefund();
    }

    /**
     * @return boolean
     */
    public function canRefundSpentPoints()
    {
        $order            = $this->getCreditmemo()->getOrder();
        $allowRefundSpent = Mage::helper('giantpoints/config')->allowRefundSpentPoint($order->getStoreId());
        if ($order->getCustomerIsGuest() || !$allowRefundSpent) {
            return false;
        }
        if ($this->getMaxSpentPointRefund()) {
            return true;
        }

        return false;
    }

    /**
     * @return boolean
     */
    public function canRefundRateEarnedPoints()
    {
        $order             = $this->getCreditmemo()->getOrder();
        $allowRefundEarned = Mage::helper('giantpoints/config')->allowCancelEarnedPoint($order->getStoreId());
        if ($order->getCustomerIsGuest() || !$allowRefundEarned) {
            return false;
        }
        if ($this->getMaxRateEarnedRefund()) {
            return true;
        }

        return false;
    }


    /**
     * get max point can deduct from customer balance
     *
     * @return int
     */
    public function getMaxRateEarnedRefund()
    {
        return $this->getEarningHelper()->getMaxRateEarnedRefund();
    }

    /**
     * check admin can refund points that earned by invitee
     *
     * @return boolean
     */
    public function canRefundInviteeEarnedPoints()
    {
        $order             = $this->getCreditmemo()->getOrder();
        $allowRefundEarned = Mage::helper('giantpoints/config')->allowCancelEarnedPoint($order->getStoreId());
        if ($order->getCustomerIsGuest() || !$allowRefundEarned) {
            return false;
        }
        if ($this->getMaxInviteeEarnedRefund()) {
            return true;
        }

        return false;
    }


    /**
     * check admin can refund points that earned by customer
     *
     * @return boolean
     */
    public function canRefundReferralEarnedPoints()
    {
        $order             = $this->getCreditmemo()->getOrder();
        $allowRefundEarned = Mage::helper('giantpoints/config')->allowCancelEarnedPoint($order->getStoreId());
        if ($order->getCustomerIsGuest() || !$allowRefundEarned) {
            return false;
        }
        if ($this->getMaxReferralEarnedRefund()) {
            return true;
        }

        return false;
    }

    /**
     *
     * @return int
     */
    public function getMaxReferralEarnedRefund()
    {
        return $this->getEarningHelper()->getMaxReferralEarnedRefund();
    }

    /**
     * max point that admin can refund to customer
     *
     * @return int
     */
    public function getMaxSpentPointRefund()
    {
        $this->getSpendingHelper()->getMaxSpentPointRefund();
    }
}