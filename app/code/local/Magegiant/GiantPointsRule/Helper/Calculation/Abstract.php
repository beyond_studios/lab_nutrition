<?php
/**
 * MageGiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageGiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsRule
 * @copyright   Copyright (c) 2014 MageGiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * GiantPointsRule Helper
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsRule
 * @author      MageGiant Developer
 */
class Magegiant_GiantPointsRule_Helper_Calculation_Abstract extends Magegiant_GiantPoints_Helper_Calculation_Abstract
{

    /**
     * returns an empty product if the product model could not be found
     *
     * @param   Mage_Sales_Model_Quote_Item||Magegiant_GiantPointsRule_Model_Catalog_Product $item                : the catalog item associated
     * @return Magegiant_GiantPointsRule_Model_Catalog_Product
     */
    protected function _parseProduct($item)
    {
        if ($item instanceof Magegiant_GiantPointsRule_Model_Catalog_Product) {
            $product = $item;
        } else if ($item instanceof Mage_Catalog_Model_Product) {
            $product = $this->_parseProduct(Magegiant_GiantPointsRule_Model_Catalog_Product::wrap($item));
        } else if ($this->hasGetProductFunc($item)) {
            $product = $item->getProduct();
            if (empty ($product)) {
                $product = Mage::getModel('giantpointsrule/catalog_product');
            }
        } else {
            $product = Mage::getModel('giantpointsrule/catalog_product');
        }

        return $product;
    }

    /**
     * @param $obj
     * @return bool
     */
    private function hasGetProductFunc($obj)
    {
        $ret = false;
        if ($this->isItem($obj) || $obj instanceof Varien_Object) { // params are function($rule)
            $ret = true;
        }

        return $ret;
    }

    /**
     * @param $obj
     * @return bool
     */
    public function isItem($obj)
    {
        $ret = false;
        if ($obj instanceof Mage_Sales_Model_Quote_Item || $obj instanceof Mage_Sales_Model_Quote_Item_Abstract || $obj instanceof Mage_Sales_Model_Quote_Address_Item || $obj instanceof Mage_Sales_Model_Order_Item || $obj instanceof Mage_Sales_Model_Order_Invoice_Item || $obj instanceof Mage_Sales_Model_Order_Creditmemo_Item || $obj instanceof Mage_Sales_Model_Order_Shipment_Item) { // params are function($rule)
            $ret = true;
        }

        return $ret;
    }

    /**
     * get All available salesrule
     *
     * @param null $quote
     * @param int  $type
     * @return array
     */
    public function getAllRewardSalesRule($quote = null, $type = 1)
    {
        if (is_null($quote)) {
            $quote = Mage::getModel('checkout/session')->getQuote();
        }
        $appliedRules = array();
        $customer     = Mage::helper('giantpoints/customer')->getCustomer();
        if ($customer && $quote && count($quote->getAllItems())) {
            $ruleCollection = Mage::getModel('giantpointsrule/salesrule')
                ->getCollection()
                ->addFieldToFilter('rule_type', $type)
                ->addAvailableFilter()
                ->addFilterByCustomerGroup($customer->getGroupId())
                ->addFilterByWebsiteId(Mage::app()->getWebsite()->getId())
                ->addFieldToFilter('coupon_type', array(
                    'eq' => Magegiant_GiantPointsRule_Model_Salesrule::COUPON_TYPE_NO_COUPON
                ))
                ->setOrder('sort_order', Varien_Data_Collection::SORT_ORDER_DESC);
            foreach ($ruleCollection as $rule) {
                if ($rule->checkRule($quote)) {
                    $appliedRules[] = $rule;
                    if ($rule->getStopRulesProcessing()) {
                        break;
                    }
                }
            }
        }

        return $appliedRules;
    }
}