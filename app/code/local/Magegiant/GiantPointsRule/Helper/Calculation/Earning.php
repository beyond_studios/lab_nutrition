<?php
/**
 * MageGiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageGiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsRule
 * @copyright   Copyright (c) 2014 MageGiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * GiantPointsRule Helper
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsRule
 * @author      MageGiant Developer
 */
class Magegiant_GiantPointsRule_Helper_Calculation_Earning extends Magegiant_GiantPointsRule_Helper_Calculation_Abstract
{

    /**
     * Calculate price using catalog price rule of product
     *
     * @param Mage_Catalog_Model_Product $product
     * @param float                      $price
     * @return float|null
     */
    public function calcPointsForCatalogRule($rule_id, $item, $isGrouped = false)
    {
        Varien_Profiler::start("Magegiant_GiantPointsRule:: Catalog points calculator");

        Varien_Profiler::start("Magegiant_GiantPointsRule:: Catalog points calculator (init)");

        // Load the rule and product model.
        $rule    = Mage::getModel('giantpointsrule/catalogrule')->load($rule_id);
        $product = $this->_parseProduct($item);


        // Get the store configuration
        $prices_include_tax = Mage::helper('tax')->priceIncludesTax();


        // Instantiate what the product cost will be evaluated to
        // If no rule needs it, then just skip this step.
        if ($rule->getSimpleAction() == 'by_profit') {
            if (!$product->getCost()) {
                $product = $product->load($product->getId());
            }
            $product_cost = (int)$product->getCost();
        } else {
            $product_cost = 0;
        }

        if ($this->isItem($item)) {
            $qty = ($item->getQty() > 0) ? $item->getQty() : 1;
            if ($prices_include_tax) {
                $price = $item->getBaseRowTotal();
                $price += $item->getBaseTaxAmount();
            } else {
                $price = $item->getBaseRowTotal();
            }
            $profit = $item->getBaseRowTotal() - ($product_cost * $qty);
        } else {
            $qty    = 1;
            $price  = $product->getFinalPrice();
            $profit = $product->getFinalPrice() - $product_cost;
        }

        // Set default price and profit values
        if ($profit < 0) {
            $profit = 0;
        }
        if ($price < 0) {
            $price = 0;
        }

        Varien_Profiler::stop("Magegiant_GiantPointsRule:: Catalog points calculator (init)");

        if ($rule->getId()) {
            if ($rule->getSimpleAction() == 'fixed') {
                // give a flat number of points if this rule's conditions are met
                // since this is a catalog rule, the points are relative to the quantity
                // if this is agroup product , points get updated when the qty is changes in
                // the product view page. So get the changes qty.
                if ($isGrouped) {
                    $qty = ($item->getQty() > -1) ? $item->getQty() : 1;
                }
                $earningPoint = $rule->getPointAmount() * $qty;
            } elseif ($rule->getSimpleAction() == 'by_price' || $rule->getSimpleAction() == 'by_profit') {
                if ($rule->getSimpleAction() == 'by_price') {
                    $value = $price;
                } elseif ($rule->getSimpleAction() == 'by_profit') {
                    $value = $profit;
                } else {
                    $value = 0;
                }

                // give a set qty of points per every given amount spent if this rule's conditions are met
                $earningPoint = Mage::helper('giantpoints/config')->getRoundingMethod($rule->getPointAmount() * $value / $rule->getMoneyStep());
                // group product , points get updated when the qty is changes.
                if ($isGrouped) {
                    $qty = ($item->getQty() > -1) ? $item->getQty() : 1;
                    if ($qty === '0.0000') //fix for ce 1.9
                        $qty = 1;
                    $earningPoint = Mage::helper('giantpoints/config')->getRoundingMethod($rule->getPointAmount() * $value * $qty / $rule->getMoneyStep());
                }
                if ($rule->getMaxPoints() > 0) {
                    if ($earningPoint > $rule->getMaxPoints()) {
                        $earningPoint = $rule->getMaxPoints();
                    }
                }
                if ($earningPoint < 0) {
                    $earningPoint = 0;
                }
            } else {
                // whatever the Points Action is set to is invalid
                // - this means no transfer of points
                Varien_Profiler::stop("Magegiant_GiantPointsRule:: Catalog points calculator");

                return null;
            }
            if ($rule->getMaxPoints() && $max_points_earned = $rule->getMaxPoints() * $qty) {
                if ($earningPoint < 0) {
                    if (-$earningPoint > $max_points_earned) $earningPoint = -$max_points_earned;
                } else {
                    if ($earningPoint > $max_points_earned) $earningPoint = $max_points_earned;
                }
            }

            Varien_Profiler::stop("Magegiant_GiantPointsRule:: Catalog points calculator");

            return $earningPoint;
        }
        Varien_Profiler::stop("Magegiant_GiantPointsRule:: Catalog points calculator");

        return 0;
    }

    /**
     * @param $item
     */
    public function calcPointsForSalesRule($rule, $quote)
    {

        if ($quote->isVirtual()) {
            $address = $quote->getBillingAddress();
        } else {
            $address = $quote->getShippingAddress();
        }
        $items    = $quote->getAllItems();
        $rowTotal = 0;
        $qtyTotal = 0;
        foreach ($items as $item) {
            if ($item->getParentItemId()) {
                continue;
            }
            if ($rule->getActions()->validate($item)) {
                $rowTotal += max(0, $item->getBaseRowTotal() - $item->getBaseDiscountAmount());
                $qtyTotal += $item->getQty();
            }
        }
        if (!$qtyTotal) {
            return 0;
        }
        $helperConfig = Mage::helper('giantpoints/config');
        if ($helperConfig->getEarningByShipping($quote->getStoreId())) {
            $rowTotal += $address->getBaseShippingAmount();
        }
        if ($rule->getSimpleAction() == 'fixed') {
            $earningPoint = $rule->getPointAmount();
        } else if ($rule->getSimpleAction() == 'by_price') {
            $earningPoint = Mage::helper('giantpoints/config')->getRoundingMethod($rule->getPointAmount() * $rowTotal / $rule->getMoneyStep());
        } else {
            $earningPoint = Mage::helper('giantpoints/config')->getRoundingMethod($rule->getPointAmount() * $qtyTotal / $rule->getQtyStep());
        }
        if ($rule->getMaxPoints() && $max_points_earned = $rule->getMaxPoints()) {
            if ($earningPoint < 0) {
                if (-$earningPoint > $max_points_earned) $earningPoint = -$max_points_earned;
            } else {
                if ($earningPoint > $max_points_earned) $earningPoint = $max_points_earned;
            }
        }

        return $earningPoint;
    }


    /**
     * @param $item
     * @return int
     */
    public function getEarnedPointsOnItem($item)
    {
        $earnedPoints = 0;
        if ($item->getHasChildren() && $item->isChildrenCalculated()) {
            foreach ($item->getChildren() as $child) {
                $earnedPoints += $child->getCatalogRuleEarn();
            }
        } else {
            $earnedPoints += $item->getCatalogRuleEarn();
        }
        if ($earnedPoints)
            return $earnedPoints;

        return 0;
    }
}