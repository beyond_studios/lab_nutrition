<?php
/**
 * MageGiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageGiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsRule
 * @copyright   Copyright (c) 2014 MageGiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * GiantPointsRule Helper
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsRule
 * @author      MageGiant Developer
 */
class Magegiant_GiantPointsRule_Helper_Calculation_Spending extends Magegiant_GiantPointsRule_Helper_Calculation_Abstract
{

    /**
     * Calculate price using catalog price rule of product
     *
     * @param Mage_Catalog_Model_Product $product
     * @param float                      $price
     * @return float|null
     */
    public function calcPointsForCatalogRule($rule_id, $item, $isGrouped = false)
    {
        Varien_Profiler::start("Magegiant_GiantPointsRule:: Catalog points calculator");

        Varien_Profiler::start("Magegiant_GiantPointsRule:: Catalog points calculator (init)");

        // Load the rule and product model.
        $rule    = Mage::getModel('giantpointsrule/catalogrule')->load($rule_id);
        $product = $this->_parseProduct($item);


        // Get the store configuration
        $prices_include_tax = Mage::helper('tax')->priceIncludesTax();

        if ($this->isItem($item)) {
            $qty = ($item->getQty() > 0) ? $item->getQty() : 1;
            if ($prices_include_tax) {
                $price = $item->getBaseRowTotal();
                $price += $item->getBaseTaxAmount();
            } else {
                $price = $item->getBaseRowTotal();
            }
        } else {
            $qty   = 1;
            $price = $product->getFinalPrice();
        }

        // Set default price and profit values
        if ($price < 0) {
            $price = 0;
        }

        Varien_Profiler::stop("Magegiant_GiantPointsRule:: Catalog points calculator (init)");

        if ($rule->getId()) {
            if ($rule->getSimpleAction() == 'fixed') {
                // give a flat number of points if this rule's conditions are met
                // since this is a catalog rule, the points are relative to the quantity
                // if this is agroup product , points get updated when the qty is changes in
                // the product view page. So get the changes qty.
                if ($isGrouped) {
                    $qty = ($item->getQty() > -1) ? $item->getQty() : 1;
                }
                $spendingPoint = $rule->getPointAmount() * $qty;
            } elseif ($rule->getSimpleAction() == 'by_price') {
                if ($rule->getSimpleAction() == 'by_price') {
                    $value = $price;
                } else {
                    $value = 0;
                }
                // give a set qty of points per every given amount spent if this rule's conditions are met
                $spendingPoint = round($rule->getPointAmount() * $value / $rule->getMoneyStep());
                // group product , points get updated when the qty is changes.
                if ($isGrouped) {
                    $qty = ($item->getQty() > -1) ? $item->getQty() : 1;
                    if ($qty === '0.0000') //fix for ce 1.9
                        $qty = 1;
                    $spendingPoint = round($rule->getPointAmount() * $value * $qty / $rule->getMoneyStep());
                }
                if ($rule->getMaxPoints() > 0) {
                    if ($spendingPoint > $rule->getMaxPoints()) {
                        $spendingPoint = $rule->getMaxPoints();
                    }
                }
                if ($spendingPoint < 0) {
                    $spendingPoint = 0;
                }
            } else {
                // whatever the Points Action is set to is invalid
                // - this means no transfer of points
                Varien_Profiler::stop("Magegiant_GiantPointsRule:: Catalog points calculator");

                return 0;
            }
            if ($rule->getMaxPoints() && $max_points_spent = $rule->getMaxPoints() * $qty) {
                if ($spendingPoint < 0) {
                    if (-$spendingPoint > $max_points_spent) $spendingPoint = -$max_points_spent;
                } else {
                    if ($spendingPoint > $max_points_spent) $spendingPoint = $max_points_spent;
                }
            }

            Varien_Profiler::stop("Magegiant_GiantPointsRule:: Catalog points calculator");

            return $spendingPoint;
        }
        Varien_Profiler::stop("Magegiant_GiantPointsRule:: Catalog points calculator");

        return 0;
    }

    /**
     * get catalog spending rules for a product
     *
     * @param mixed  $product
     * @param int    $customerGroupId
     * @param int    $websiteId
     * @param string $date
     * @return array
     */
    public function getProductSpendingRules($product, $websiteId = null, $customerGroupId = null, $date = null)
    {
        $product  = $this->_parseProduct($product);
        $cacheKey = "product_spending_collection:{$product->getId()}:$customerGroupId:$websiteId";
        if (!$this->hasCache($cacheKey)) {
            $rules = array();
            foreach ($product->getAvailableCatalogSpendingRules($websiteId, $customerGroupId, $date) as $ruleInfo) {
                $rules[] = $ruleInfo->getRule();
            }
            $this->saveCache($cacheKey, $rules);
        }

        return $this->getCache($cacheKey);
    }

    /**
     * get session catalog spending point
     *
     * @param type $item
     * @return int
     */
    public function getCatalogSpentPoints($item = null)
    {
        $session      = Mage::getSingleton('checkout/session');
        $catalogRules = $session->getCatalogRules();
        if (!is_array($catalogRules)) {
            return 0;
        }
        if ($item && $item->getId()) {
            if (isset($catalogRules[$item->getId()])) {
                $rule = $catalogRules[$item->getId()];

                return $rule->getPointUsed() * $rule->getItemQty();
            }

            return 0;
        }
        $points = 0;
        foreach ($catalogRules as $rule) {
            $rulePoints = $rule->getPointUsed() * $rule->getItemQty();
            $points += $rulePoints;
        }

        return $points;
    }

    /**
     * get discount for current item
     *
     * @param type $item
     * @return int
     */
    public function getItemDiscount($item = null)
    {
        $session      = Mage::getSingleton('checkout/session');
        $catalogRules = $session->getCatalogRules();
        if (!is_array($catalogRules)) {
            return 0;
        }
        if (is_null($item)) {
            $discount = 0;
            foreach ($catalogRules as $catalogPoints) {
                $discount += (isset($catalogPoints['base_point_discount']) && isset($catalogPoints['item_qty'])) ? $catalogPoints['base_point_discount'] * $catalogPoints['item_qty'] : 0;
            }

            return $discount;
        }
        if (!isset($catalogRules[$item->getId()])) {
            return 0;
        }
        $catalogPoints = $catalogRules[$item->getId()];
        if (isset($catalogPoints['base_point_discount'])) {
            return $catalogPoints['base_point_discount'];
        }

        return 0;
    }

    /**
     * get Catalog Rule discount (each step)
     *
     * @param type $rule
     * @return float
     */
    public function getCatalogRuleDiscount($rule, $productPrice)
    {
        $ruleDiscount = 0;
        switch ($rule->getDiscountStyle()) {
            case 'by_fixed':
                $ruleDiscount = $rule->getDiscountAmount();
                break;
            case 'to_fixed':
                $ruleDiscount = $productPrice - $rule->getDiscountAmount();
                break;
            case 'by_percent':
                $ruleDiscount = $productPrice * $rule->getDiscountAmount() / 100;
                break;
            case 'to_percent':
                $ruleDiscount = $productPrice * max(0, 100 - $rule->getDiscountAmount()) / 100;
                break;
        }
        if ($ruleDiscount < 0) {
            $ruleDiscount = 0;
        }

        return $ruleDiscount;
    }

    /**
     * calculate catalog rule discount
     *
     * @param type $rule
     * @param type $product
     * @param int  $points
     * @return float
     */
    public function getCatalogRuleDiscountByPoints($rule, $product, $points)
    {
        $stepDiscount = $this->getCatalogRuleDiscount($rule, $product->getPrice());
        $pointStep    = $this->getPointStep($rule, $product->getPrice());
        if ($pointStep == 0) {
            $productDiscount = $stepDiscount;
        } else {
            $points = round($points / $pointStep) * $pointStep;
            if ($timeUses = (int)$rule->getUsesPerProduct()) {
                $zMaxPoints = $timeUses * $pointStep;
                if ($points > $zMaxPoints) {
                    $points = $zMaxPoints;
                }
            }
            $productDiscount = $stepDiscount * $points / $pointStep;
        }
        $finalPrice = Mage::helper('tax')->getPrice($product, $product->getPrice());
        if ($productDiscount > $finalPrice) {
            return $finalPrice;
        }

        return $productDiscount;
    }

    /**
     * calculate catalog rule discount
     *
     * @param type $rule
     * @param type $product
     * @param int  $points
     * @return float
     */
    public function getSalesRuleDiscountByPoints($rule, $quote, &$points)
    {
        $spendingHelper = Mage::helper('giantpoints/calculation_spending');
        $baseTotal      = $spendingHelper->getQuoteBaseTotal($quote);
        foreach ($quote->getAllItems() as $item) {
            if ($item->getParentItemId()) {
                continue;
            }
            if (!$rule->getActions()->validate($item)) {
                $baseTotal -= $item->getBaseRowTotal() - $item->getQty() * $this->getItemDiscount($item);
            }
        }
        $availableTotal = $baseTotal - $this->getCheckedRuleDiscountWithout($rule->getId());
        if ($availableTotal <= 0) {
            $points   = 0;
            $discount = 0;
        } else if ($rule->getDiscountStyle() == 'cart_fixed') {
            if ($rule->getPointAmount() <= 0) {
                $points   = 0;
                $discount = min($rule->getDiscountAmount(), $availableTotal);
            } else {
                if ($maxPerOrder = $spendingHelper->getMaxPointsPerOrder($quote->getStoreId())) {
                    $maxPerOrder -= Mage::helper('giantpoints/calculation_spending')->getPointItemSpent();
                    $maxPerOrder -= $this->getCheckedRulePointWithout($rule->getId());
                    $points = min($points, max($maxPerOrder, 0));
                }
                if ($rule->getDiscountAmount() > 0) {
                    $points   = min($points, ceil($availableTotal / $rule->getDiscountAmount()) * $rule->getPointAmount());
                    $points   = floor($points / $rule->getPointAmount()) * $rule->getPointAmount();
                    $discount = $rule->getDiscountAmount() * $points / $rule->getPointAmount();
                } else {
                    $points   = 0;
                    $discount = 0;
                }
            }
        } else {
            if ($rule->getPointAmount() <= 0) {
                $points      = 0;
                $discountPer = min($rule->getDiscountAmount(), 100);
                $discount    = min($discountPer * $baseTotal / 100, $availableTotal);
            } else if ($rule->getDiscountAmount() <= 0) {
                $points   = 0;
                $discount = 0;
            } else {
                $maxPerOrder = $rule->getMaxPoints() ? $rule->getMaxPoints() : $spendingHelper->getMaxPointsPerOrder($quote->getStoreId());
                if ($maxPerOrder) {
                    $maxPerOrder -= Mage::helper('giantpoints/calculation_spending')->getPointItemSpent();
                    $maxPerOrder -= $this->getCheckedRulePointWithout($rule->getId());
                    $points = min($points, max($maxPerOrder, 0));
                }
                $points      = min($points, ceil(100 * ($availableTotal / $baseTotal) / $rule->getDiscountAmount()) * $rule->getPointAmount());
                $points      = floor($points / $rule->getPointAmount()) * $rule->getPointAmount();
                $discountPer = $rule->getDiscountAmount() * $points / $rule->getPointAmount();
                $discountPer = min($discountPer, 100 * $availableTotal / $baseTotal);
                $discount    = $discountPer * $baseTotal / 100;
            }
        }

        return $discount;
    }

    public function getPointStep($rule, $price)
    {
        $pointStep = 0;
        if ($rule->getSimpleAction() == 'fixed') {
            $pointStep = (int)$rule->getPointAmount();
        } else {
            if ($rule->getMoneyStep() > 0) {
                $pointStep = floor($price / $rule->getMoneyStep()) * $rule->getPointAmount();
            }
            if ($rule->getMaxPoints() && $pointStep > $rule->getMaxPoints()) {
                $pointStep = $rule->getMaxPoints();
            }
        }

        return $pointStep;
    }

    public function getCatalogRule($ruleId)
    {
        $cacheKey = "catalog_rule_model:$ruleId";
        if (!$this->hasCache($cacheKey)) {
            $this->saveCache($cacheKey, Mage::getModel('giantpointsrule/spending_catalog')->load($ruleId));
        }

        return $this->getCache($cacheKey);
    }

    /**
     * get spending rules for shopping cart
     *
     * @param int    $customerGroupId
     * @param int    $websiteId
     * @param string $date
     * @return Magegiant_GiantPointsRuleRule_Model_Mysql4_Spending_Sales_Collection
     */
    public function getShoppingCartRules($customerGroupId = null, $websiteId = null, $date = null)
    {
        if (is_null($customerGroupId)) {
            $customerGroupId = $this->getCustomerGroupId();
        }
        if (is_null($websiteId)) {
            $websiteId = $this->getWebsiteId();
        }
        $rules = Mage::getResourceModel('giantpointsrule/salesrule_collection')
            ->addFilterByType(Magegiant_GiantPointsRule_Model_RuleType::RULE_TYPE_SPENDING)
            ->addAvailableFilter($date)
            ->addFilterByCustomerGroup($customerGroupId)
            ->addFilterByWebsiteId($websiteId)
            ->addFilterByCoupon(Magegiant_GiantPointsRule_Model_Salesrule::COUPON_TYPE_NO_COUPON);
        foreach ($rules as $rule) {
            $rule->afterLoad();
        }

        return $rules;
    }

    /**
     * get spending rules depend on current quote
     *
     * @param type   $quote
     * @param int    $customerGroupId
     * @param int    $websiteId
     * @param string $date
     * @return array
     */
    public function getQuoteSpendingRules($quote = null, $customerGroupId = null, $websiteId = null, $date = null)
    {
        if (is_null($quote)) {
            $quote = Mage::helper('giantpoints/spending_point')->getQuote();
        }
        if ($quote->isVirtual()) {
            $address = $quote->getBillingAddress();
        } else {
            $address = $quote->getShippingAddress();
        }
        if (is_null($customerGroupId)) {
            $customerGroupId = $this->getCustomerGroupId();
        }
        if (is_null($websiteId)) {
            $websiteId = $this->getWebsiteId();
        }
        $cacheKey = "quote_spending_collection:{$address->getId()}:$customerGroupId:$websiteId";
        if (!$this->hasCache($cacheKey)) {
            $rules              = array();
            $session            = Mage::getSingleton('checkout/session');
            $rewardCheckedRules = $session->getRewardCheckedRules();
            if (is_array($rewardCheckedRules)) {
                $rewardCheckedRules = array_keys($rewardCheckedRules);
            } else {
                $rewardCheckedRules = array();
            }
            foreach ($this->getShoppingCartRules($customerGroupId, $websiteId, $date) as $rule) {
                /**
                 * end update
                 */
                if ($rule->validate($address)) {
                    $rules[] = $rule;
                    if ($rule->getStopRulesProcessing() && in_array($rule->getId(), $rewardCheckedRules)) {
                        break;
                    }
                }
            }
            $this->saveCache($cacheKey, $rules);
        }

        return $this->getCache($cacheKey);
    }

    /**
     * get checked rule discount (base currency) without a special rule
     *
     * @param type $ruleId
     * @return float
     */
    public function getCheckedRuleDiscountWithout($ruleId = null)
    {
        $session            = Mage::getSingleton('checkout/session');
        $rewardCheckedRules = $session->getRewardCheckedRules();
        if (!$rewardCheckedRules || !is_array($rewardCheckedRules)) {
            return 0;
        }
        $baseDiscount = 0;
        foreach ($rewardCheckedRules as $_ruleId => $ruleData) {
            if ($_ruleId != $ruleId) {
                $baseDiscount += isset($ruleData['base_discount']) ? $ruleData['base_discount'] : 0;
            }
        }

        return $baseDiscount;
    }

    /**
     * get checked rule points without a special rule
     *
     * @param type $ruleId
     * @return int
     */
    public function getCheckedRulePointWithout($ruleId = null)
    {
        $session            = Mage::getSingleton('checkout/session');
        $rewardCheckedRules = $session->getRewardCheckedRules();
        if (!$session->getData('use_point') || !$rewardCheckedRules || !is_array($rewardCheckedRules)) {
            return 0;
        }
        $points = 0;
        foreach ($rewardCheckedRules as $_ruleId => $ruleData) {
            if ($_ruleId != $ruleId) {
                $points += isset($ruleData['use_point']) ? $ruleData['use_point'] : 0;
            }
        }

        return $points;
    }

    /**
     * @return mixed
     */
    public function getCustomerPoint($item_id = null)
    {
        if ($this->hasCache('customer_point')) {
            return $this->getCache('customer_point');
        }
        $points = Mage::helper('giantpoints/customer')->getBalance();
        $points -= $this->getCatalogSpentPoints();
        if ($points < 0) {
            $points = 0;
        }
        $session = Mage::getSingleton('checkout/session');

        $shopingCartSpending = $session->getRewardSalesRules();
        if (is_array($shopingCartSpending) && !empty($shopingCartSpending))
            $points -= $shopingCartSpending['point_amount'];
        $points -= $this->getCheckedRulePointWithout();

        $catalogRules = $session->getCatalogRules();
        $pointTemp    = 0;
        if (isset($catalogRules[$item_id])) {
            $pointTemp = $catalogRules[$item_id]->getData('point_used') * $catalogRules[$item_id]->getData('item_qty');
        }

        $this->saveCache('customer_point', $points + $pointTemp);

        return $this->getCache('customer_point');
    }

}