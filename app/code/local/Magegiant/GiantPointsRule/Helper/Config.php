<?php
/**
 * MageGiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageGiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsRule
 * @copyright   Copyright (c) 2014 MageGiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * GiantPointsRule Helper
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsRule
 * @author      MageGiant Developer
 */
class Magegiant_GiantPointsRule_Helper_Config extends Mage_Core_Helper_Abstract
{
    const XML_PATH_RULE_ENABLED = 'giantpoints/giantpointsrule/is_enabled';
    const XML_PATH_RULE_CONFIG  = 'giantpoints/giantpointsrule/';

    /**
     * Check Giantpoints rule is enabled
     *
     * @param null $storeId
     * @return mixed
     */
    public function isEnabled($storeId = null)
    {
        if (!$storeId) {
            $storeId = Mage::app()->getStore()->getId();
        }

        return Mage::getStoreConfig(self::XML_PATH_RULE_ENABLED, $storeId);
    }

    /**
     * Get Giantpoints Rule configuration
     *
     * @param      $name
     * @param null $storeId
     * @return mixed
     */
    public function getRuleConfig($name, $storeId = null)
    {
        if (!$storeId) {
            $storeId = Mage::app()->getStore()->getId();
        }

        return Mage::getStoreConfig(self::XML_PATH_RULE_CONFIG . $name, $storeId);

    }

}