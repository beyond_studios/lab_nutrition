<?php
/**
 * MageGiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageGiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsRule
 * @copyright   Copyright (c) 2014 MageGiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/** @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();
/**
 * CatalogRule Begin
 */
$installer->getConnection()->dropTable($installer->getTable('giantpointsrule/catalogrule'));
$table = $installer->getConnection()
    ->newTable($installer->getTable('giantpointsrule/catalogrule'))
    ->addColumn('rule_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary'  => true,
    ), 'Rule Id')
    ->addColumn('rule_type', Varien_Db_Ddl_Table::TYPE_SMALLINT, 6, array(
        'unsigned' => true,
        'nullable' => false,
    ), 'Rule Type')
    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(), 'Name')
    ->addColumn('description', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(), 'Description')
    ->addColumn('from_date', Varien_Db_Ddl_Table::TYPE_DATE, null, array(), 'From Date')
    ->addColumn('to_date', Varien_Db_Ddl_Table::TYPE_DATE, null, array(), 'To Date')
    ->addColumn('is_active', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable' => false,
        'default'  => '0',
    ), 'Is Active')
    ->addColumn('rule_type', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable' => false,
        'default'  => 1,
    ), 'Rule Type')
    ->addColumn('conditions_serialized', Varien_Db_Ddl_Table::TYPE_TEXT, '2M', array(), 'Conditions Serialized')
    ->addColumn('stop_rules_processing', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable' => false,
        'default'  => '1',
    ), 'Stop Rules Processing')
    ->addColumn('sort_order', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
        'default'  => '0',
    ), 'Sort Order')
    ->addColumn('simple_action', Varien_Db_Ddl_Table::TYPE_VARCHAR, 32, array(
        'nullable' => false,
        'default'  => 'fixed',
    ), 'Simple Action')
    ->addColumn('discount_style', Varien_Db_Ddl_Table::TYPE_VARCHAR, 25, array(
        'nullable' => false,
        'default'  => 'by_fixed',
    ), 'Discount Style')
    ->addColumn('point_amount', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'nullable' => false,
        'default'  => 0,
    ), 'Point Amount')
    ->addColumn('discount_amount', Varien_Db_Ddl_Table::TYPE_DECIMAL, array(12, 4), array(
        'nullable' => false,
        'default'  => 0,
    ), 'Discount Amount')
    ->addColumn('uses_per_product', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'nullable' => false,
        'default'  => 0,
    ), 'Uses Per Product')
    ->addColumn('money_step', Varien_Db_Ddl_Table::TYPE_DECIMAL, array(12, 4), array(
        'nullable' => false,
        'default'  => 0,
    ), 'Money Step')
    ->addColumn('max_points', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'nullable' => false,
        'default'  => 0,
    ), 'Max Points')
    ->addIndex($installer->getIdxName('giantpointsrule/catalogrule', array('is_active', 'sort_order', 'to_date', 'from_date')),
        array('is_active', 'sort_order', 'to_date', 'from_date'))

    ->setComment('GiantPoints CatalogRule');
$installer->getConnection()->createTable($table);

/**
 * Create table 'giantpointsrule/catalogrule_product'
 */
$installer->getConnection()->dropTable($installer->getTable('giantpointsrule/catalogrule_product'));
$table = $installer->getConnection()
    ->newTable($installer->getTable('giantpointsrule/catalogrule_product'))
    ->addColumn('rule_product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary'  => true,
    ), 'Rule Product Id')
    ->addColumn('rule_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
        'default'  => '0',
    ), 'Rule Id')
    ->addColumn('from_time', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
        'default'  => '0',
    ), 'From Time')
    ->addColumn('to_time', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
        'default'  => '0',
    ), 'To time')
    ->addColumn('customer_group_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned' => true,
        'nullable' => false,
        'default'  => '0',
    ), 'Customer Group Id')
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
        'default'  => '0',
    ), 'Product Id')
    ->addColumn('rule_type', Varien_Db_Ddl_Table::TYPE_SMALLINT, 6, array(
        'unsigned' => true,
        'nullable' => false,
    ), 'Rule Type')
    ->addColumn('simple_action', Varien_Db_Ddl_Table::TYPE_VARCHAR, 32, array(
        'default' => 'fixed',
    ), 'Simple Action')
    ->addColumn('discount_style', Varien_Db_Ddl_Table::TYPE_VARCHAR, 32, array(
        'default' => 'by_fixed',
    ), 'Discount Style')
    ->addColumn('point_amount', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'nullable' => false,
        'default'  => '0',
    ), 'Point Amount')
    ->addColumn('discount_amount', Varien_Db_Ddl_Table::TYPE_DECIMAL, array(12, 4), array(
        'nullable' => false,
        'default'  => '0',
    ), 'Discount Amount')
    ->addColumn('action_stop', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable' => false,
        'default'  => '0',
    ), 'Action Stop')
    ->addColumn('sort_order', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
        'default'  => '0',
    ), 'Sort Order')
    ->addColumn('website_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned' => true,
        'nullable' => false,
    ), 'Website Id')
    ->addColumn('money_step', Varien_Db_Ddl_Table::TYPE_DECIMAL, array(12, 4), array(
        'nullable' => false,
        'default'  => '0',
    ), 'Point Amount')
    ->addColumn('max_points', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'unsigned' => true,
        'nullable' => false,
    ), 'Max Points')
    ->addColumn('uses_per_product', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'unsigned' => true,
        'nullable' => false,
    ), 'Uses Per Product')
    ->addIndex($installer->getIdxName('giantpointsrule/catalogrule_product', array('rule_id', 'from_time', 'to_time', 'website_id', 'customer_group_id', 'product_id', 'sort_order'), true),
        array('rule_id', 'from_time', 'to_time', 'website_id', 'customer_group_id', 'product_id', 'sort_order'), array('type' => 'unique'))

    ->addIndex($installer->getIdxName('giantpointsrule/catalogrule_product', array('rule_id')),
        array('rule_id'))
    ->addIndex($installer->getIdxName('giantpointsrule/catalogrule_product', array('customer_group_id')),
        array('customer_group_id'))
    ->addIndex($installer->getIdxName('giantpointsrule/catalogrule_product', array('website_id')),
        array('website_id'))
    ->addIndex($installer->getIdxName('giantpointsrule/catalogrule_product', array('from_time')),
        array('from_time'))
    ->addIndex($installer->getIdxName('giantpointsrule/catalogrule_product', array('to_time')),
        array('to_time'))
    ->addIndex($installer->getIdxName('giantpointsrule/catalogrule_product', array('product_id')),
        array('product_id'))

    ->addForeignKey($installer->getFkName('giantpointsrule/catalogrule_product', 'product_id', 'catalog/product', 'entity_id'),
        'product_id', $installer->getTable('catalog/product'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)

    ->addForeignKey($installer->getFkName('giantpointsrule/catalogrule_product', 'customer_group_id', 'customer/customer_group', 'customer_group_id'),
        'customer_group_id', $installer->getTable('customer/customer_group'), 'customer_group_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)

    ->addForeignKey($installer->getFkName('giantpointsrule/catalogrule_product', 'rule_id', 'giantpointsrule/catalogrule', 'rule_id'),
        'rule_id', $installer->getTable('giantpointsrule/catalogrule'), 'rule_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)

    ->addForeignKey($installer->getFkName('giantpointsrule/catalogrule_product', 'website_id', 'core/website', 'website_id'),
        'website_id', $installer->getTable('core/website'), 'website_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)

    ->setComment('GiantPoints CatalogRule Product');

$installer->getConnection()->createTable($table);

/**
 * Create table 'giantpointsrule/catalogrule_affected_product'
 */
$installer->getConnection()->dropTable($installer->getTable('giantpointsrule/catalogrule_affected_product'));
$table = $installer->getConnection()
    ->newTable($installer->getTable('giantpointsrule/catalogrule_affected_product'))
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
        'primary'  => true,
    ), 'Product Id')
    ->setComment('Giantpoints CatalogRule Affected Product');

$installer->getConnection()->createTable($table);

/**
 * Create table 'giantpointsrule/catalogrule_group_website'
 */
$installer->getConnection()->dropTable($installer->getTable('giantpointsrule/catalogrule_group_website'));
$table = $installer->getConnection()
    ->newTable($installer->getTable('giantpointsrule/catalogrule_group_website'))
    ->addColumn('rule_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
        'primary'  => true,
        'default'  => '0',
    ), 'Rule Id')
    ->addColumn('customer_group_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned' => true,
        'nullable' => false,
        'primary'  => true,
        'default'  => '0',
    ), 'Customer Group Id')
    ->addColumn('website_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned' => true,
        'nullable' => false,
        'primary'  => true,
        'default'  => '0',
    ), 'Website Id')
    ->addIndex($installer->getIdxName('giantpointsrule/catalogrule_group_website', array('rule_id')),
        array('rule_id'))
    ->addIndex($installer->getIdxName('giantpointsrule/catalogrule_group_website', array('customer_group_id')),
        array('customer_group_id'))
    ->addIndex($installer->getIdxName('giantpointsrule/catalogrule_group_website', array('website_id')),
        array('website_id'))

    ->addForeignKey($installer->getFkName('giantpointsrule/catalogrule_group_website', 'customer_group_id', 'customer/customer_group', 'customer_group_id'),
        'customer_group_id', $installer->getTable('customer/customer_group'), 'customer_group_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)

    ->addForeignKey($installer->getFkName('giantpointsrule/catalogrule_group_website', 'rule_id', 'giantpointsrule/catalogrule', 'rule_id'),
        'rule_id', $installer->getTable('giantpointsrule/catalogrule'), 'rule_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)

    ->addForeignKey($installer->getFkName('giantpointsrule/catalogrule_group_website', 'website_id', 'core/website', 'website_id'),
        'website_id', $installer->getTable('core/website'), 'website_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('GiantPoints CatalogRule Group Website');

$installer->getConnection()->createTable($table);

$installer->getConnection()->dropTable($installer->getTable('giantpointsrule/catalogrule_website'));
$table = $installer->getConnection()->newTable($installer->getTable('giantpointsrule/catalogrule_website'))
    ->addColumn('rule_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'unsigned' => true,
            'nullable' => false,
            'primary'  => true
        ),
        'Rule Id'
    )
    ->addColumn('website_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned' => true,
            'nullable' => false,
            'primary'  => true
        ),
        'Website Id'
    )
    ->addIndex(
        $installer->getIdxName('giantpointsrule/catalogrule_website', array('rule_id')),
        array('rule_id')
    )
    ->addIndex(
        $installer->getIdxName('giantpointsrule/catalogrule_website', array('website_id')),
        array('website_id')
    )
    ->addForeignKey($installer->getFkName('giantpointsrule/catalogrule_website', 'rule_id', 'giantpointsrule/catalogrule', 'rule_id'),
        'rule_id', $installer->getTable('giantpointsrule/catalogrule'), 'rule_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey($installer->getFkName('giantpointsrule/catalogrule_website', 'website_id', 'core/website', 'website_id'),
        'website_id', $installer->getTable('core/website'), 'website_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->setComment('GiantPoints Catalog Rules To Websites Relations');

$installer->getConnection()->createTable($table);

$installer->getConnection()->dropTable($installer->getTable('giantpointsrule/catalogrule_customer_group'));
$table = $installer->getConnection()->newTable($installer->getTable('giantpointsrule/catalogrule_customer_group'))
    ->addColumn('rule_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'unsigned' => true,
            'nullable' => false,
            'primary'  => true
        ),
        'Rule Id'
    )
    ->addColumn('customer_group_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned' => true,
            'nullable' => false,
            'primary'  => true
        ),
        'Customer Group Id'
    )
    ->addIndex(
        $installer->getIdxName('giantpointsrule/catalogrule_customer_group', array('rule_id')),
        array('rule_id')
    )
    ->addIndex(
        $installer->getIdxName('giantpointsrule/catalogrule_customer_group', array('customer_group_id')),
        array('customer_group_id')
    )
    ->addForeignKey($installer->getFkName('giantpointsrule/catalogrule_customer_group', 'rule_id', 'catalogrule/rule', 'rule_id'),
        'rule_id', $installer->getTable('giantpointsrule/catalogrule'), 'rule_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey(
        $installer->getFkName('giantpointsrule/catalogrule_customer_group', 'customer_group_id',
            'customer/customer_group', 'customer_group_id'
        ),
        'customer_group_id', $installer->getTable('customer/customer_group'), 'customer_group_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->setComment('GiantPoints CatalogRules To Customer Groups Relations');

$installer->getConnection()->createTable($table);

/*Catalog Rule End*/

/*ShoppingCart Rule Begin*/
$installer->getConnection()->dropTable($installer->getTable('giantpointsrule/salesrule'));
$installer->getConnection()->dropTable($installer->getTable('giantpointsrule/salesrule'));
$table = $installer->getConnection()
    ->newTable($installer->getTable('giantpointsrule/salesrule'))
    ->addColumn('rule_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary'  => true,
    ), 'Rule Id')
    ->addColumn('rule_type', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable' => false,
        'default'  => '1',
    ), 'Rule Type')
    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(), 'Name')
    ->addColumn('description', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(), 'Description')
    ->addColumn('from_date', Varien_Db_Ddl_Table::TYPE_DATE, null, array(), 'From Date')
    ->addColumn('to_date', Varien_Db_Ddl_Table::TYPE_DATE, null, array(), 'To Date')
    ->addColumn('customer_group_ids', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(), 'Customer Group Ids')
    ->addColumn('is_active', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable' => false,
        'default'  => '0',
    ), 'Is Active')
    ->addColumn('conditions_serialized', Varien_Db_Ddl_Table::TYPE_TEXT, '2M', array(), 'Conditions Serialized')
    ->addColumn('actions_serialized', Varien_Db_Ddl_Table::TYPE_TEXT, '2M', array(), 'Actions Serialized')
    ->addColumn('stop_rules_processing', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable' => false,
        'default'  => '1',
    ), 'Stop Rules Processing')
    ->addColumn('is_advanced', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned' => true,
        'nullable' => false,
        'default'  => '1',
    ), 'Is Advanced')
    ->addColumn('product_ids', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(), 'Product Ids')
    ->addColumn('sort_order', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
        'default'  => '0',
    ), 'Sort Order')
    ->addColumn('simple_action', Varien_Db_Ddl_Table::TYPE_VARCHAR, 32, array(
        'nullable' => false,
        'default'  => 'fixed',
    ), 'Simple Action')
    ->addColumn('point_amount', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'unsigned' => true,
        'nullable' => false,
    ), 'Point Amount')
    ->addColumn('money_step', Varien_Db_Ddl_Table::TYPE_DECIMAL, array(12, 4), array(
        'nullable' => false,
        'default'  => '0.0000',
    ), 'Money Step')
    ->addColumn('qty_step', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'unsigned' => true,
        'nullable' => false,
    ), 'Quantity Step')
    ->addColumn('max_points', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'unsigned' => true,
        'nullable' => false,
    ), 'Max Points')
    ->addColumn('discount_style', Varien_Db_Ddl_Table::TYPE_VARCHAR, 32, array(
        'nullable' => false,
        'default'  => 'by_fixed',
    ), 'Discount Style')
    ->addColumn('discount_amount', Varien_Db_Ddl_Table::TYPE_DECIMAL, array(12, 4), array(
        'nullable' => false,
        'default'  => '0.0000',
    ), 'Discount Amount')
    ->addColumn('uses_per_customer', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => false,
        'default'  => '0',
    ), 'Uses Per Customer')
    ->addColumn('uses_per_coupon', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => false,
        'default'  => '0',
    ), 'Uses Per Coupon')
    ->addColumn('coupon_type', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned' => true,
        'nullable' => false,
        'default'  => '1',
    ), 'Coupon Type')
    ->addColumn('website_ids', Varien_Db_Ddl_Table::TYPE_TEXT, 4000, array(), 'Website Ids')
    ->addColumn('static_blocks_ids', Varien_Db_Ddl_Table::TYPE_TEXT, 4000, array(), 'Static Block Ids')
    ->addIndex($installer->getIdxName('salesrule/rule', array('is_active', 'sort_order', 'to_date', 'from_date')),
        array('is_active', 'sort_order', 'to_date', 'from_date'))
    ->setComment('GiantPoints Salesrule');

$installer->getConnection()->createTable($table);

$installer->getConnection()->dropTable($installer->getTable('giantpointsrule/salesrule_coupon'));
$table = $installer->getConnection()
    ->newTable($installer->getTable('giantpointsrule/salesrule_coupon'))
    ->addColumn('coupon_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary'  => true,
    ), 'Coupon Id')
    ->addColumn('rule_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
    ), 'Rule Id')
    ->addColumn('code', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(), 'Code')
    ->addColumn('usage_limit', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
    ), 'Usage Limit')
    ->addColumn('usage_per_customer', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
    ), 'Usage Per Customer')
    ->addColumn('times_used', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
        'default'  => '0',
    ), 'Times Used')
    ->addColumn('expiration_date', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(), 'Expiration Date')
    ->addColumn('is_primary', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned' => true,
    ), 'Is Primary')
    ->addIndex($installer->getIdxName('giantpointsrule/salesrule_coupon', array('code'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
        array('code'), array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE))
    ->addIndex($installer->getIdxName('giantpointsrule/salesrule_coupon', array('rule_id', 'is_primary'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
        array('rule_id', 'is_primary'), array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE))
    ->addIndex($installer->getIdxName('giantpointsrule/salesrule_coupon', array('rule_id')),
        array('rule_id'))
    ->addForeignKey($installer->getFkName('giantpointsrule/salesrule_coupon', 'rule_id', 'giantpointsrule/salesrule', 'rule_id'),
        'rule_id', $installer->getTable('giantpointsrule/salesrule'), 'rule_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('GiantPoint Salesrule Coupon');
$installer->getConnection()->createTable($table);

/**
 * Create table 'salesrule/coupon_usage'
 */
$installer->getConnection()->dropTable($installer->getTable('giantpointsrule/salesrule_coupon_usage'));
$table = $installer->getConnection()
    ->newTable($installer->getTable('giantpointsrule/salesrule_coupon_usage'))
    ->addColumn('coupon_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
        'primary'  => true,
    ), 'Coupon Id')
    ->addColumn('customer_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
        'primary'  => true,
    ), 'Customer Id')
    ->addColumn('times_used', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
        'default'  => '0',
    ), 'Times Used')
    ->addIndex($installer->getIdxName('giantpointsrule/salesrule_coupon_usage', array('coupon_id')),
        array('coupon_id'))
    ->addIndex($installer->getIdxName('giantpointsrule/salesrule_coupon_usage', array('customer_id')),
        array('customer_id'))
    ->addForeignKey($installer->getFkName('giantpointsrule/salesrule_coupon_usage', 'coupon_id', 'giantpointsrule/salesrule_coupon', 'coupon_id'),
        'coupon_id', $installer->getTable('giantpointsrule/salesrule_coupon'), 'coupon_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey($installer->getFkName('giantpointsrule/salesrule_coupon_usage', 'customer_id', 'customer/entity', 'entity_id'),
        'customer_id', $installer->getTable('customer/entity'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('GiantPoints Salesrule Coupon Usage');
$installer->getConnection()->createTable($table);

/**
 * Create table 'salesrule/rule_customer'
 */
$installer->getConnection()->dropTable($installer->getTable('giantpointsrule/salesrule_customer'));
$table = $installer->getConnection()
    ->newTable($installer->getTable('giantpointsrule/salesrule_customer'))
    ->addColumn('rule_customer_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary'  => true,
    ), 'Rule Customer Id')
    ->addColumn('rule_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
        'default'  => '0',
    ), 'Rule Id')
    ->addColumn('customer_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
        'default'  => '0',
    ), 'Customer Id')
    ->addColumn('times_used', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned' => true,
        'nullable' => false,
        'default'  => '0',
    ), 'Times Used')
    ->addIndex($installer->getIdxName('giantpointsrule/salesrule_customer', array('rule_id', 'customer_id')),
        array('rule_id', 'customer_id'))
    ->addIndex($installer->getIdxName('giantpointsrule/salesrule_customer', array('customer_id', 'rule_id')),
        array('customer_id', 'rule_id'))
    ->addForeignKey($installer->getFkName('giantpointsrule/salesrule_customer', 'customer_id', 'customer/entity', 'entity_id'),
        'customer_id', $installer->getTable('customer/entity'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey($installer->getFkName('giantpointsrule/salesrule_customer', 'rule_id', 'giantpointsrule/salesrule', 'rule_id'),
        'rule_id', $installer->getTable('giantpointsrule/salesrule'), 'rule_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('GiantPoints Salesrule Customer');
$installer->getConnection()->createTable($table);
/*ShoppingCart Rule End*/

/*Sales Order Table*/
$installer->getConnection()->addColumn($this->getTable('sales/order'), 'salesrule_earning_ids', 'text NOT NULL default ""');
$installer->getConnection()->addColumn($this->getTable('sales/order'), 'salesrule_spending_ids', 'text NOT NULL default ""');
$installer->endSetup();

