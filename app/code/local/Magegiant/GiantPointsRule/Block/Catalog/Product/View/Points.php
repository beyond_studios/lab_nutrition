<?php

class Magegiant_GiantPointsRule_Block_Catalog_Product_View_Points extends Magegiant_GiantPointsRule_Block_Catalog_Product_View_Abstract
{
    protected $_isShowed = false;

    /**
     * @return array
     */
    public function getAvailableEarningRules()
    {
        try {
            return $this->getProduct()->getAvailableEarningRules();
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function printOptionsPrice()
    {
        if (!version_compare(Mage::getVersion(), '1.4.0.0', '>=')) {
            return false;
        }

        return (!$this->getProduct()->isComposite() && !$this->getProduct()->getHasOptions());
    }

    /**
     * Get JSON encripted configuration array which can be used for JS dynamic
     * price calculation depending on product options
     *
     * @return string
     */
    public function getJsonConfig()
    {

        $config = array();

        $_request = Mage::getSingleton('tax/calculation')->getRateRequest(false, false, false);
        $_request->setProductClassId($this->getProduct()->getTaxClassId());
        $defaultTax = Mage::getSingleton('tax/calculation')->getRate($_request);

        $_request = Mage::getSingleton('tax/calculation')->getRateRequest();
        $_request->setProductClassId($this->getProduct()->getTaxClassId());
        $currentTax = Mage::getSingleton('tax/calculation')->getRate($_request);

        $_regularPrice = $this->getProduct()->getPrice();
        $_finalPrice   = $this->getProduct()->getFinalPrice();
        $_priceInclTax = Mage::helper('tax')->getPrice($this->getProduct(), $_finalPrice, true);
        $_priceExclTax = Mage::helper('tax')->getPrice($this->getProduct(), $_finalPrice);

        $config = array('productId' => $this->getProduct()->getId(), 'priceFormat' => Mage::app()->getLocale()->getJsPriceFormat(), 'includeTax' => Mage::helper('tax')->priceIncludesTax() ? 'true' : 'false', 'showIncludeTax' => Mage::helper('tax')->displayPriceIncludingTax(), 'showBothPrices' => Mage::helper('tax')->displayBothPrices(), 'productPrice' => Mage::helper('core')->currency($_finalPrice, false, false), 'productOldPrice' => Mage::helper('core')->currency($_regularPrice, false, false), 'skipCalculate' => ($_priceExclTax != $_priceInclTax ? 0 : 1), 'defaultTax' => $defaultTax, 'currentTax' => $currentTax, 'idSuffix' => '_clone', 'oldPlusDisposition' => 0, 'plusDisposition' => 0, 'oldMinusDisposition' => 0, 'minusDisposition' => 0);

        $responseObject = new Varien_Object ();
        Mage::dispatchEvent('catalog_product_view_config', array('response_object' => $responseObject));
        if (is_array($responseObject->getAdditionalOptions())) {
            foreach ($responseObject->getAdditionalOptions() as $option => $value) {
                $config [$option] = $value;
            }
        }

        return Mage::helper('core')->jsonEncode($config);
    }

    protected function _toHtml()
    {
        if (!$this->_isShowed) {
            $this->_isShowed = true;

            return parent::_toHtml();
        } else {
            return "";
        }

    }
}