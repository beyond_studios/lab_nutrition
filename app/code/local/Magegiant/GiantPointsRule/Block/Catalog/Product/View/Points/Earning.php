<?php

class Magegiant_GiantPointsRule_Block_Catalog_Product_View_Points_Earning extends Magegiant_GiantPointsRule_Block_Catalog_Product_View_Abstract
{
    /**
     * @return mixed
     */
    public function isShow()
    {
        return Mage::helper('giantpoints/config')->showOnProductViewPage();
    }

    /**
     * get total earning poing by catalog rule
     *
     * @return int
     */
    public function getTotalEarning()
    {
        return $this->getProduct()->getTotalEarningByRule();
    }

    /**
     * @return bool
     */
    public function getShowEarningFrom()
    {
        $has = $this->getProduct()->isComposite() && $this->getProduct()->getHasOptions();

        return $has;
    }

    public function customerIsGuest()
    {
        return Mage::getModel('customer/session')->getCustomer()->getId() ? false : true;
    }
}