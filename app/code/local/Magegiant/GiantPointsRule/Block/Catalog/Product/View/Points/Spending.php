<?php

class Magegiant_GiantPointsRule_Block_Catalog_Product_View_Points_Spending extends Magegiant_GiantPointsRule_Block_Catalog_Product_View_Abstract
{
    /**
     * get spending calculation
     *
     */
    public function getCalculation()
    {
        return Mage::helper('giantpointsrule/calculation_spending');
    }

    /**
     * get spending rules
     *
     * @return array
     */
    public function getCatalogSpendingRules()
    {
        if (!Mage::helper('giantpointsrule/config')->isEnabled()) {
            return array();
        }
        $product = $this->getProduct();
        if (!$product) {
            return array();
        }

        return $this->getCalculation()->getProductSpendingRules($product);
    }

    public function getProduct()
    {
        if (!$this->hasData('product')) {
            $this->setData('product', Mage::registry('product'));
        }

        return $this->getData('product');
    }

    public function isShowRedeemRules()
    {
        if (!$this->getProduct()) {
            return false;
        }
        if ($this->getProduct()->isGrouped()) {
            return false;
        }
        if ($this->getProduct()->getFinalPrice() < 0.0001) {
            return false;
        }

        return true;
    }

    public function isShowRedeemRulesGrouped()
    {
        if (!Mage::helper('customer')->isLoggedIn()) {
            return false;
        }
        if (!$this->getProduct()) {
            return false;
        }
        if ($this->getProduct()->isGrouped()) {
            return true;
        }

        return false;
    }

    public function getPriceFormatJs()
    {
        $priceFormat = Mage::app()->getLocale()->getJsPriceFormat();

        return Mage::helper('core')->jsonEncode($priceFormat);
    }

    /**
     * get JSON string used for JS
     *
     * @param array $rules
     * @return string
     */
    public function getProductRulesJson($rules = null, $product = null, $productFinalPrice = null)
    {
        if (is_null($rules)) {
            $rules = $this->getCatalogSpendingRules();
        }
        if (is_null($product)) {
            $product = $this->getProduct();
        }
        if (is_null($productFinalPrice)) {
            $productFinalPrice = $product->getFinalPrice();
        }
        $result          = array();
        $isCustomerLogin = $this->getCustomer()->getId();
        $id              = $this->getRequest()->getParam('id');
        foreach ($rules as $rule) {
            $ruleOptions = array();
            if ($isCustomerLogin) {
                $minPoins = $this->getPointsForRule($rule);
                $this->getCalculation()->getCustomerPoint($id);
                $totalPoints = Mage::helper('giantpoints/customer')->getBalance();
                $minRedeem   = (int)Mage::helper('giantpoints/config')->getMinimumRedeemPoint();
                if ($this->getCalculation()->getCustomerPoint($id) < $minPoins || ($minRedeem && $totalPoints < $minRedeem)) {
                    $ruleOptions['optionType'] = 'needPoint';
                    $ruleOptions['needPoint']  = max($minPoins - $this->getCalculation()->getCustomerPoint($id), $minRedeem - $totalPoints);
                } else {
                    $price        = Mage::helper('tax')->getPrice($product, $productFinalPrice);
                    $sliderOption = array(
                        'minPoints' => $minPoins,
                        'pointStep' => $minPoins,
                        'pointUsed' => $this->getAppliedPointsRuleByItemId($rule->getId())
                    );
                    $ruleDiscount = $this->getRuleDiscount($rule);

                    $maxPriceSpend = $rule->getMaxPriceSpendedValue() > 0 ? $rule->getMaxPriceSpendedValue() : 0;
                    if ($rule->getMaxPriceSpendedType() == 'by_price') {
                        $ruleOptions['maxDiscount'] = $maxPriceSpend;
                    } elseif ($rule->getMaxPriceSpendedType() == 'by_percent') {
                        $ruleOptions['maxDiscount'] = $price * $maxPriceSpend / 100;
                    } else
                        $ruleOptions['maxDiscount'] = 0;
                    if ($ruleOptions['maxDiscount'] > 0 && $ruleOptions['maxDiscount'] < $price) $price = $ruleOptions['maxDiscount'];

                    if ($ruleDiscount < 0.0001) {
                        $ruleOptions['optionType']   = 'static';
                        $ruleOptions['stepDiscount'] = 0;
                    } else {
                        $ruleOptions['stepDiscount'] = Mage::app()->getStore()->convertPrice($ruleDiscount);
                        $ruleOptions['optionType']   = 'slider';
                        $maxPoints                   = $this->getCalculation()->getCustomerPoint();
                        $zMaxPoints                  = ceil($price / $ruleDiscount) * $sliderOption['pointStep'];
                        if ($maxPoints >= $zMaxPoints) {
                            $maxPoints = $zMaxPoints;
                        } else {
                            $maxPoints = floor($maxPoints / $sliderOption['pointStep']) * $sliderOption['pointStep'];
                        }
                        if ($timeUses = (int)$rule->getUsesPerProduct()) {
                            $zMaxPoints = $timeUses * $sliderOption['pointStep'];
                            if ($maxPoints > $zMaxPoints) {
                                $maxPoints = $zMaxPoints;
                            }
                        }
                        if ($maxPoints == $sliderOption['pointStep']) {
                            $ruleOptions['optionType'] = 'static';
                        } else {
                            $sliderOption['maxPoints'] = (int)$maxPoints;
                        }
                    }

                    $ruleOptions['sliderOption'] = $sliderOption;
                }
            } else {
                $ruleOptions['optionType'] = 'login';
            }
            $result[$rule->getId()] = $ruleOptions;
        }

        return Mage::helper('core')->jsonEncode($result);
    }

    public function getProductRulesJsonGrouped()
    {
        $product = $this->getProduct();
        if ($product->isGrouped()) {
            $result         = array();
            $groupedProduct = $product->getTypeInstance(true)->getAssociatedProducts($product);
            foreach ($groupedProduct as $productGroup) {
                $rule = $this->getCalculation()->getProductSpendingRules($productGroup);
                if (count($rule) > 0) {
                    $ruleJson                       = $this->getProductRulesJson($rule, $productGroup);
                    $result[$productGroup->getId()] = Mage::helper('core')->jsonDecode($ruleJson);
                }
            }
        }

        return Mage::helper('core')->jsonEncode($result);
    }

    /**
     * @param $rule
     * @return int
     */
    public function getPointsForRule($rule)
    {
        return (int)$this->getCalculation()->calcPointsForCatalogRule($rule->getId(), $this->getProduct());
    }

    public function getCustomer()
    {
        return Mage::getSingleton('customer/session')->getCustomer();
    }

    public function getRewardCatalogrule()
    {
        $request = Mage::app()->getRequest();
        $action  = $request->getRequestedRouteName() . '_' . $request->getRequestedControllerName() . '_' . $request->getRequestedActionName();
        if ($action == 'checkout_cart_configure' && $request->getParam('id')) {
            $session      = Mage::getSingleton('checkout/session');
            $catalogRules = $session->getCatalogRules();
            if (isset($catalogRules[$request->getParam('id')])) {
                $ruleItemData = $catalogRules[$request->getParam('id')];

                return new Varien_Object(array(
                    'reward_product_rule'  => isset($ruleItemData['rule_id']) ? $ruleItemData['rule_id'] : '',
                    'reward_product_point' => isset($ruleItemData['point_used']) ? $ruleItemData['point_used'] : '',
                ));
            }
        }

        return new Varien_Object;
    }

    /**
     * get Rule discount (each step)
     *
     * @param type $rule
     * @return float
     */
    public function getRuleDiscount($rule)
    {
        $price = $this->getProduct()->getPrice();

        return $this->getCalculation()->getCatalogRuleDiscount($rule, $price);
    }


    public function getAppliedPointsRuleByItemId($rule_id)
    {
        $catalogRules = Mage::getSingleton('checkout/session')->getCatalogRules();
        if (!is_array($catalogRules)) {
            return 0;
        }
        foreach ($catalogRules as $rule) {
            if ($rule->getRuleId() == $rule_id) {
                return $rule->getPointUsed();
            }
        }

        return 0;
    }

    /**
     * get reward points helper
     *
     */
    public function getPointHelper()
    {
        return Mage::helper('giantpoints');
    }


    /**
     * call method that defined from block helper
     *
     * @param string $method
     * @param array  $args
     * @return mixed
     */
    public function __call($method, $args)
    {
        $helper = Mage::helper('giantpoints/spending_point');
        if (method_exists($helper, $method)) {
            return call_user_func_array(array($helper, $method), $args);
            // return call_user_method_array($method, $helper, $args);
        }

        return parent::__call($method, $args);
    }
}