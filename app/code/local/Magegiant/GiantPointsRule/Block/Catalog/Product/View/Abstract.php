<?php

class Magegiant_GiantPointsRule_Block_Catalog_Product_View_Abstract extends Mage_Core_Block_Template
{
    protected $_product = null;

    /**
     * @return Magegiant_GiantPointsRule_Model_Catalog_Product|mixed
     */
    public function getProduct()
    {
        if ($this->_product == null) {

            $p = Mage::registry('product');
            if (empty ($p))
                $p = $this->getProduct();

            if ($p) {
                if ($p instanceof Magegiant_GiantPointsRule_Model_Catalog_Product) {
                    $this->_product = $p;
                } else {
                    $this->_product = Mage::getModel('giantpointsrule/catalog_product')->setStoreId($p->getStoreId())->load($p->getId());
                    $this->_product->addData($p->getData());
                }
            } else {
                $this->_product = Mage::getModel('giantpointsrule/catalog_product');
            }
            Mage::unregister('product');
            Mage::register('product', $this->_product);
        }

        return $this->_product;
    }

    /**
     * @return array
     */
    public function getAvailableEarningRules()
    {
        return $this->getProduct()->getAvailableEarningRules();
    }

    /**
     * @param $pointAmount
     * @return string
     */
    public function getPointsString($pointAmount)
    {
        return Mage::helper('giantpoints')->addLabelForPoint($pointAmount);
    }

}