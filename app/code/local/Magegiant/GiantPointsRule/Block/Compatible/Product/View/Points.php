<?php

class Magegiant_GiantPointsRule_Block_Compatible_Product_View_Points extends Magegiant_GiantPointsRule_Block_Catalog_Product_View_Points
{


    protected function _prepareLayout()
    {
        /*
         * For compatibility with MC 1.9+ and third party modules,
         *
         * Look for the product.info block & search for one of it's children aliased as "other".
         * If such a child doesn't exist, create it, then re-parent and prepend this block under "other".
         * If it does exist, make sure it's one that can output our block ('core/text_list').
         * If the product.info template doesn't output the "other" block, then we fall back on the default way this block was supposed to render.
         * */
        $productInfoBlock = $this->getLayout()->getBlock('product.info');
        if ($productInfoBlock) {
            $otherBlock = $productInfoBlock->getChild('other');
            if (!$otherBlock) {
                $otherBlock = $this->getLayout()->createBlock('core/text_list', 'other')->append($this);
                $productInfoBlock->append($otherBlock);

            } else if ($otherBlock instanceof Mage_Core_Block_Text_List) {
                $newBlock = $this->getLayout()->createBlock('core/text_list', 'rewards.integrated.product.view.points.output')->append($this);
                $otherBlock->insert($newBlock, '', false);
            }
        }

        return parent::_prepareLayout();
    }

    public function getEarningPointHtml()
    {
        $child = $this->getChild("giantpoints_catalogrule_earning");
        if (empty($child)) return "";

        $child->setProduct($this->getProduct());

        return $this->getChildHtml("giantpoints_catalogrule_earning");
    }

    protected function _toHtml()
    {
        if (Mage::helper('giantpoints/config')->showOnProductViewPage()) {
            return parent::_toHtml();
        } else {
            return "";
        }
    }

}
