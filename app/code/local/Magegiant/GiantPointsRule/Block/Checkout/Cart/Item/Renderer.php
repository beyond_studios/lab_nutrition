<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Checkout
 * @copyright   Copyright (c) 2008 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Shopping cart item render block
 *
 * @category    Mage
 * @package     Mage_Checkout
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Magegiant_GiantPointsRule_Block_Checkout_Cart_Item_Renderer extends Mage_Checkout_Block_Cart_Item_Renderer
{

    protected $earned_points = null;

    /**
     * get earning point per item in cart
     *
     * @return int
     */


    public function getRowTotalInclTax($_item)
    {
        $base_row_total          = $_item->getRowTotal();
        $tax_amount              = $_item->getTaxAmount();
        $base_row_total_incl_tax = $base_row_total + $tax_amount;

        return $base_row_total_incl_tax;
    }


    /**
     * @return array
     */
    public function getCatalogRules()
    {
        $rules = array();
        if ($this->getCheckoutSession()->getCatalogRules()) {
            $rules = $this->getCheckoutSession()->getCatalogRules();
        }

        return $rules;
    }


}
