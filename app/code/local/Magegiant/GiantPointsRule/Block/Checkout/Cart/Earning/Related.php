<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Checkout
 * @copyright   Copyright (c) 2008 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Shopping cart item render block
 *
 * @category    Mage
 * @package     Mage_Checkout
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Magegiant_GiantPointsRule_Block_Checkout_Cart_Earning_Related extends Magegiant_GiantPoints_Block_Abstract
{
    protected $_quote;
    protected $_appliedRules = array();

    /**
     *
     * @return array
     */
    public function getAppliedRules()
    {
        if (!empty($this->_appliedRules)) {
            $this->_quote = Mage::getModel('checkout/session')->getQuote();
            $customer     = Mage::helper('giantpoints/customer')->getCustomer();
            if ($customer && $this->_quote && count($this->_quote->getAllItems())) {
                $ruleCollection = Mage::getModel('giantpointsrule/salesrule')
                    ->getCollection()
                    ->addAvailableFilter()
                    ->addFilterByType(Magegiant_GiantPointsRule_Model_RuleType::RULE_TYPE_EARNING)//type earning
                    ->addFilterByCustomerGroup($customer->getGroupId())
                    ->addFilterByWebsiteId(Mage::app()->getWebsite()->getId())
                    ->setOrder('sort_order', Varien_Data_Collection::SORT_ORDER_DESC);
                foreach ($ruleCollection as $rule) {
                    if ($rule->checkRule($this->_quote)) {
                        $this->_appliedRules[] = $rule;
                        if ($rule->getStopRulesProcessing()) {
                            break;
                        }
                    }
                }
            }
        }

        return $this->_appliedRules;
    }

    /**
     * get static block
     *
     * @param $rule
     * @return array
     */
    public function getStaticBlocks($rule)
    {
        $blocksIds = explode(',', $rule->getStaticBlocksIds());
        $toReturn  = array();

        $processor = Mage::getModel('core/email_template_filter');

        foreach ($blocksIds as $blockId) {
            $toReturn[] = $processor->filter(Mage::getModel('cms/block')->load($blockId)->getContent());
        }

        return $toReturn;
    }


}