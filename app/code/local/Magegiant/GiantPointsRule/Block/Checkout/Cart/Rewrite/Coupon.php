<?php
/**
 * MageGiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magegiant.com license that is
 * available through the world-wide-web at this URL:
 * https://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @copyright   Copyright (c) 2014 Magegiant (http://magegiant.com/)
 * @license     https://magegiant.com/license-agreement/
 */ 
class Magegiant_GiantPointsRule_Block_Checkout_Cart_Rewrite_Coupon extends Mage_Checkout_Block_Cart_Coupon {

    /**
     * get current used coupon code
     *
     * @return string
     */
    public function getCouponCode()
    {
        $container = new Varien_Object(array(
            'coupon_code'   => '',
        ));
        Mage::dispatchEvent('giantpoints_rewrite_coupon_block_get_coupon_code', array(
            'block'     => $this,
            'container' => $container,
        ));
        if ($container->getCouponCode()) {
            return $container->getCouponCode();
        }
        return parent::getCouponCode();
    }

    protected function _toHtml()
    {
        $container = new Varien_Object(array(
            'html'          => '',
            'rewrite_core'  => false,
        ));
        Mage::dispatchEvent('giantpoints_rewrite_coupon_block_to_html', array(
            'block'     => $this,
            'container' => $container,
        ));
        if ($container->getRewriteCore() && $container->getHtml()) {
            return $container->getHtml();
        }
        if ($this->getChild('checkout.cart.giantpoints')) {
            return $container->getHtml()
            . $this->getChildHtml('checkout.cart.giantpoints')
            . parent::_toHtml();
        }
        return $container->getHtml() . parent::_toHtml();
    }
}