<?php
/**
 * Magegiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magegiant.com license that is
 * available through the world-wide-web at this URL:
 * https://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPoints
 * @copyright   Copyright (c) 2014 Magegiant (https://magegiant.com/)
 * @license     https://magegiant.com/license-agreement/
 */

/**
 * GiantPoints Rule Adminhtml Earning Catalog Edit Block
 *
 * @category     Magegiant
 * @package     Magegiant_GiantPointsRule
 * @author      Magegiant Developer
 */
class Magegiant_GiantPointsRule_Block_Adminhtml_Earning_Catalogrule_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    const PAGE_TABS_BLOCK_ID = 'rule_tabs';

    public function __construct()
    {
        parent::__construct();

        $this->_objectId   = 'id';
        $this->_blockGroup = 'giantpointsrule';
        $this->_controller = 'adminhtml_earning_catalogrule';

        $this->_updateButton('save', 'label', Mage::helper('giantpoints')->__('Save Rule'));
        $this->_updateButton('delete', 'label', Mage::helper('giantpoints')->__('Delete Rule'));

        $this->_addButton('save_apply', array(
            'class'   => 'save',
            'label'   => Mage::helper('giantpointsrule')->__('Save and Apply'),
            'onclick' => "$('rule_auto_apply').value=1; editForm.submit()",
        ));

        $this->_addButton('save_and_continue_edit', array(
            'class'   => 'save',
            'label'   => Mage::helper('giantpointsrule')->__('Save and Continue Edit'),
            'onclick' => 'saveAndContinueEdit(\'' . $this->_getSaveAndContinueUrl() . '\')',
        ), 10);

        $this->_formScripts[] = "
            function toggleSimpleAction(){
                if ($('rule_simple_action').value == 'fixed') {
                    $('rule_money_step').up(1).hide();
                    $('rule_max_points').up(1).hide();
                } else {
                    $('rule_money_step').up(1).show();
                    $('rule_max_points').up(1).show();
                }
            }
            function saveAndContinueEdit(urlTemplate){
                var urlTemplateSyntax = /(^|.|\\r|\\n)({{(\\w+)}})/;
                var template = new Template(urlTemplate, urlTemplateSyntax);
                var url = template.evaluate({tab_id:" . self::PAGE_TABS_BLOCK_ID . "JsTabs.activeTab.id});
                editForm.submit(url);
            }
            Event.observe(window, 'load', function(){toggleSimpleAction();});
            function saveAndApply(){
                editForm.submit($('edit_form').action+'apply/1/');
            }
            
        ";
    }

    protected function _getSaveAndContinueUrl()
    {
        return $this->getUrl('*/*/save', array(
            '_current'   => true,
            'back'       => 'edit',
            'tab'        => '{{tab_id}}',
            'active_tab' => null
        ));
    }

    /**
     * get text to show in header when edit an item
     *
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('catalogrule_data') && Mage::registry('catalogrule_data')->getId()) {
            return Mage::helper('giantpointsrule')->__("Edit Rule '%s'", $this->htmlEscape(Mage::registry('catalogrule_data')->getName()));
        } else {
            return Mage::helper('giantpointsrule')->__('Add Rule');
        }
    }

    /**
     * Overwrite Back Url
     *
     * @return string
     */
    public function getBackUrl()
    {
        if ($this->getRequest()->getParam('type')) {
            $typeId = $this->getRequest()->getParam('type');
        }
        if ($this->_getCatalogRule()->getRuleType()) {
            $typeId = $this->_getCatalogRule()->getRuleType();
        }

        return $this->getUrl('*/*/', array('type' => $typeId));
    }

    protected function _getCatalogRule()
    {
        return Mage::registry('catalogrule_data');
    }
}
