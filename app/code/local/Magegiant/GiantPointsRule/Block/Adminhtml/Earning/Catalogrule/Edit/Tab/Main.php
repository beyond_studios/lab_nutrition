<?php

/**
 * Magegiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magegiant.com license that is
 * available through the world-wide-web at this URL:
 * https://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPoints
 * @copyright   Copyright (c) 2014 Magegiant (https://magegiant.com/)
 * @license     https://magegiant.com/license-agreement/
 */

/**
 * GiantPoints Rule Earning Catalog Edit Tab Form Block
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPointsRule
 * @author      Magegiant Developer
 */
class Magegiant_GiantPointsRule_Block_Adminhtml_Earning_Catalogrule_Edit_Tab_Main extends Mage_Adminhtml_Block_Widget_Form
{

    /**
     * prepare tab form's information
     *
     * @return Magegiant_GiantPoints_Block_Adminhtml_Catalogrule_Edit_Tab_Form
     */
    protected function _prepareForm()
    {
        if (Mage::getSingleton('adminhtml/session')->getFormData()) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData();
            Mage::getSingleton('adminhtml/session')->setFormData(null);
        } else if (Mage::registry('catalogrule_data')) {
            $data = Mage::registry('catalogrule_data')->getData();
        }
        if (!is_null(Mage::app()->getRequest()->getParam('group_id'))) {
            $data['customer_group_ids'] = Mage::app()->getRequest()->getParam('group_id');
        }
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('rule_');
        $this->setForm($form);
        $fieldset = $form->addFieldset('general_fieldset', array('legend' => Mage::helper('giantpoints')->__('General information')));

        $fieldset->addField('auto_apply', 'hidden', array(
            'name' => 'auto_apply',
        ));
        if (Mage::registry('catalogrule_data')->getId()) {
            $fieldset->addField('rule_id', 'hidden', array(
                'name' => 'rule_id',
            ));
        }
        $fieldset->addField('rule_type', 'hidden', array(
            'name' => 'rule_type',
        ));
        $fieldset->addField('name', 'text', array(
            'label'    => Mage::helper('giantpointsrule')->__('Rule Name'),
            'title'    => Mage::helper('giantpointsrule')->__('Rule Name'),
            'class'    => 'required-entry',
            'required' => true,
            'name'     => 'name',
        ));

        $fieldset->addField('description', 'editor', array(
            'name'  => 'description',
            'label' => Mage::helper('giantpointsrule')->__('Description'),
            'title' => Mage::helper('giantpointsrule')->__('Description'),
            'style' => 'width:276px;height:100px;',
            'note'  => Mage::helper('giantpointsrule')->__('Rule description shown on Reward Information page'),
        ));

        $fieldset->addField('is_active', 'select', array(
            'label'  => Mage::helper('giantpointsrule')->__('Status'),
            'title'  => Mage::helper('giantpointsrule')->__('Status'),
            'name'   => 'is_active',
            'values' => array(
                array(
                    'value' => '0',
                    'label' => Mage::helper('giantpointsrule')->__('Inactive'),
                ),
                array(
                    'value' => '1',
                    'label' => Mage::helper('giantpointsrule')->__('Active'),
                )
            ),
            'value'  => 0
        ));

        if (!Mage::app()->isSingleStoreMode()) {
            $fieldset->addField('website_ids', 'multiselect', array(
                'name'     => 'website_ids[]',
                'label'    => Mage::helper('giantpointsrule')->__('Websites'),
                'title'    => Mage::helper('giantpointsrule')->__('Websites'),
                'required' => true,
                'values'   => Mage::getSingleton('adminhtml/system_config_source_website')->toOptionArray(),
                'after_element_html' => Mage::helper('giantpoints')->addSelectAll('rule_website_ids')
            ));
        } else {
            $fieldset->addField('website_ids', 'hidden', array(
                'name'               => 'website_ids[]',
                'value'              => Mage::app()->getStore(true)->getWebsiteId(),

            ));
            $data['website_ids'] = Mage::app()->getStore(true)->getWebsiteId();
        }

        $fieldset->addField('customer_group_ids', 'multiselect', array(
            'label'              => Mage::helper('giantpointsrule')->__('Customer groups'),
            'title'              => Mage::helper('giantpointsrule')->__('Customer groups'),
            'name'               => 'customer_group_ids',
            'required'           => true,
            'values'             => Mage::getResourceModel('customer/group_collection')
                    // ->addFieldToFilter('customer_group_id', array('gt'=> 0))
                    ->load()
                    ->toOptionArray(),
            'after_element_html' => Mage::helper('giantpoints')->addSelectAll('rule_customer_group_ids')
        ));
        $dateFormatIso = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
        $fieldset->addField('from_date', 'date', array(
            'name'         => 'from_date',
            'label'        => Mage::helper('giantpointsrule')->__('Valid from'),
            'title'        => Mage::helper('giantpointsrule')->__('From date'),
            'image'        => $this->getSkinUrl('images/grid-cal.gif'),
            'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
            'format'       => $dateFormatIso,
        ));

        $fieldset->addField('to_date', 'date', array(
            'name'         => 'to_date',
            'label'        => Mage::helper('giantpointsrule')->__('Valid to'),
            'title'        => Mage::helper('giantpointsrule')->__('To date'),
            'image'        => $this->getSkinUrl('images/grid-cal.gif'),
            'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
            'format'       => $dateFormatIso,
        ));

        $fieldset->addField('sort_order', 'text', array(
            'name'  => 'sort_order',
            'label' => Mage::helper('giantpointsrule')->__('Priority'),
            'title' => Mage::helper('giantpointsrule')->__('Priority'),
            'note'  => Mage::helper('giantpointsrule')->__('Rule with the highest priority will be applied first.')
        ));


        $data['rule_type'] = Magegiant_GiantPointsRule_Model_RuleType::RULE_TYPE_EARNING;
        $form->setValues($data);

        return parent::_prepareForm();
    }

}
