<?php
/**
 * Magegiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magegiant.com license that is
 * available through the world-wide-web at this URL:
 * https://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPoints
 * @copyright   Copyright (c) 2014 Magegiant (https://magegiant.com/)
 * @license     https://magegiant.com/license-agreement/
 */

/**
 * GiantPoints Rule Earning Catalog Edit Actions Tab Block
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPoints
 * @author      Magegiant Developer
 */
class Magegiant_GiantPointsRule_Block_Adminhtml_Earning_Catalogrule_Edit_Tab_Actions extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare tab form's information
     *
     * @return Magegiant_GiantPoints_Block_Adminhtml_Catalogrule_Edit_Tab_Actions
     */
    protected function _prepareForm()
    {
        if (Mage::getSingleton('adminhtml/session')->getFormData()) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData();
            Mage::getSingleton('adminhtml/session')->setFormData(null);
        } elseif (Mage::registry('catalogrule_data')) {
            $data = Mage::registry('catalogrule_data')->getData();
        }

        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('rule_');
        $this->setForm($form);
        $fieldset = $form->addFieldset('actions_fieldset', array('legend' => Mage::helper('giantpointsrule')->__('Earning Point Action')));

        $fieldset->addField('simple_action', 'select', array(
            'label'    => Mage::helper('giantpointsrule')->__('Action'),
            'title'    => Mage::helper('giantpoints')->__('Action'),
            'name'     => 'simple_action',
            'options'  => Mage::getModel('giantpointsrule/catalogrule_simple_action_options_earning')->getOptionArray(),
            'onchange' => 'toggleSimpleAction()',
            'note'     => Mage::helper('giantpointsrule')->__('select the type to earn points')
        ));

        $fieldset->addField('point_amount', 'text', array(
            'label'    => Mage::helper('giantpointsrule')->__('Points (X)'),
            'title'    => Mage::helper('giantpointsrule')->__('Points (X)'),
            'class'    => 'required-entry',
            'required' => true,
            'name'     => 'point_amount',
        ));

        $fieldset->addField('money_step', 'text', array(
            'label'              => Mage::helper('giantpointsrule')->__('Money Step (Y)'),
            'title'              => Mage::helper('giantpointsrule')->__('Money Step (Y)'),
            'name'               => 'money_step',
            'after_element_html' => '<strong>[' . Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE) . ']</strong>',
        ));

        $fieldset->addField('max_points', 'text', array(
            'label' => Mage::helper('giantpointsrule')->__('Max points earned by this rule'),
            'title' => Mage::helper('giantpointsrule')->__('Max points earned by this rule'),
            'name'  => 'max_points',
            'note'  => Mage::helper('giantpointsrule')->__('Set the maximum number of Discount Amounts.
If empty or zero, there is no limitation.')
        ));

        $fieldset->addField('stop_rules_processing', 'select', array(
            'label'   => Mage::helper('giantpointsrule')->__('Stop further rules processing'),
            'title'   => Mage::helper('giantpointsrule')->__('Stop further rules processing'),
            'name'    => 'stop_rules_processing',
            'options' => array(
                '1' => Mage::helper('giantpointsrule')->__('Yes'),
                '0' => Mage::helper('giantpointsrule')->__('No'),
            ),
        ));
        $form->setValues($data);

        return parent::_prepareForm();
    }
}
