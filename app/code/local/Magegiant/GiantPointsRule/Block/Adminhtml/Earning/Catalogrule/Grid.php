<?php
/**
 * Magegiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magegiant.com license that is
 * available through the world-wide-web at this URL:
 * https://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPoints
 * @copyright   Copyright (c) 2014 Magegiant (https://magegiant.com/)
 * @license     https://magegiant.com/license-agreement/
 */

/**
 * GiantPoints Earning Catalog Grid Block
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPoints
 * @author      Magegiant Developer
 */
class Magegiant_GiantPointsRule_Block_Adminhtml_Earning_Catalogrule_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('catalogEarningRuleGrid');
        $this->setDefaultSort('sort_order');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * prepare collection for block to display
     *
     * @return Magegiant_GiantPoints_Block_Adminhtml_Catalogrule_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('giantpointsrule/catalogrule')
            ->getResourceCollection()
            ->addFieldToFilter('rule_type', Magegiant_GiantPointsRule_Model_RuleType::RULE_TYPE_EARNING);
        $collection->addWebsitesToResult();
        $this->setCollection($collection);

        parent::_prepareCollection();

        return $this;
    }

    /**
     * prepare columns for this grid
     *
     * @return Magegiant_GiantPoints_Block_Adminhtml_Catalogrule_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('rule_id', array(
            'header' => Mage::helper('giantpoints')->__('ID'),
            'align'  => 'right',
            'width'  => '50px',
            'index'  => 'rule_id',
        ));

        $this->addColumn('name', array(
            'header' => Mage::helper('giantpoints')->__('Rule Name'),
            'align'  => 'left',
            'index'  => 'name',
        ));

        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('website_ids', array(
                'header'                    => Mage::helper('giantpoints')->__('Website'),
                'align'                     => 'left',
                'width'                     => '200px',
                'type'                      => 'options',
                'options'                   => Mage::getSingleton('adminhtml/system_store')->getWebsiteOptionHash(),
                'index'                     => 'website_ids',
                'filter_condition_callback' => array($this, 'filterCallback'),
                'sortable'                  => false,
            ));
        }

        $this->addColumn('customer_group_ids', array(
            'header'                    => Mage::helper('giantpoints')->__('Customer Groups'),
            'align'                     => 'left',
            'index'                     => 'customer_group_ids',
            'type'                      => 'options',
            'width'                     => '200px',
            'sortable'                  => false,
            'options'                   => Mage::getResourceModel('customer/group_collection')
                    ->load()
                    ->toOptionHash(),
            'filter_condition_callback' => array($this, 'filterCallback'),
        ));

        $this->addColumn('from_date', array(
            'header' => Mage::helper('giantpoints')->__('Created on'),
            'align'  => 'left',
            'index'  => 'from_date',
            'format' => 'dd/MM/yyyy',
            'type'   => 'date',
        ));

        $this->addColumn('to_date', array(
            'header' => Mage::helper('giantpoints')->__('Expired on'),
            'align'  => 'left',
            'index'  => 'to_date',
            'format' => 'dd/MM/yyyy',
            'type'   => 'date',
        ));

        $this->addColumn('is_active', array(
            'header'  => Mage::helper('giantpoints')->__('Status'),
            'align'   => 'left',
            'width'   => '80px',
            'index'   => 'is_active',
            'type'    => 'options',
            'options' => array(
                '1' => Mage::helper('giantpoints')->__('Active'),
                '0' => Mage::helper('giantpoints')->__('Inactive'),
            ),
        ));

        $this->addColumn('sort_order', array(
            'header' => Mage::helper('giantpoints')->__('Priority'),
            'align'  => 'left',
            'width'  => '60px',
            'index'  => 'sort_order',
        ));

        $this->addColumn('action', array(
            'header'    => Mage::helper('giantpoints')->__('Action'),
            'width'     => '70px',
            'type'      => 'action',
            'getter'    => 'getId',
            'actions'   => array(
                array(
                    'caption' => Mage::helper('giantpoints')->__('Edit'),
                    'url'     => array('base' => '*/*/edit'),
                    'field'   => 'id'
                )
            ),
            'filter'    => false,
            'sortable'  => false,
            'index'     => 'stores',
            'is_system' => true,
        ));

        return parent::_prepareColumns();
    }

    /**
     * get url for each row in grid
     *
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array(
                'type' => Magegiant_GiantPointsRule_Model_RuleType::RULE_TYPE_EARNING,
                'id'   => $row->getId()
            )
        );
    }

    /**
     * Callback filter for Website/ Customer group
     *
     * @param type $collection
     * @param type $column
     * @return type
     */
    public function filterCallback($collection, $column)
    {
        $value = $column->getFilter()->getValue();
        if (is_null(@$value))
            return;
        else
            $collection->addFieldToFilter($column->getIndex(), array('finset' => $value));
    }
}
