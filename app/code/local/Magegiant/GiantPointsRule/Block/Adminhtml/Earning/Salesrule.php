<?php
/**
 * Magegiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the magegiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPoints
 * @copyright   Copyright (c) 2014 Magegiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * Giantpoints Block
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPoints
 * @author      Magegiant Developer
 */
class Magegiant_GiantPointsRule_Block_Adminhtml_Earning_Salesrule extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller     = 'adminhtml_earning_salesrule';
        $this->_blockGroup     = 'giantpointsrule';
        $this->_headerText     = Mage::helper('giantpointsrule')->__('Shopping Cart Earning Rules');
        $this->_addButtonLabel = Mage::helper('giantpointsrule')->__('Add Rule');
        parent::__construct();
    }

    public function getCreateUrl()
    {
        return $this->getUrl('*/*/new', array('type' => Magegiant_GiantPointsRule_Model_RuleType::RULE_TYPE_EARNING));
    }
}