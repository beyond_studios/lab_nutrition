<?php
/**
 * Magegiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magegiant.com license that is
 * available through the world-wide-web at this URL:
 * https://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPoints
 * @copyright   Copyright (c) 2014 Magegiant (https://magegiant.com/)
 * @license     https://magegiant.com/license-agreement/
 */

/**
 * GiantPointsRule Earning Catalog Edit Tabs Block
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPointsRule
 * @author      Magegiant Developer
 */
class Magegiant_GiantPointsRule_Block_Adminhtml_Earning_Catalogrule_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('rule_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('giantpointsrule')->__('Catalog Earning Rule Information'));
    }

    /**
     * prepare before render block to html
     *
     * @return Magegiant_GiantPoints_Block_Adminhtml_Catalogrule_Edit_Tabs
     */
    protected function _beforeToHtml()
    {
        $this->addTab('general', array(
            'label'   => Mage::helper('giantpointsrule')->__('General Information'),
            'title'   => Mage::helper('giantpointsrule')->__('General Information'),
            'content' => $this->getLayout()->createBlock('giantpointsrule/adminhtml_earning_catalogrule_edit_tab_main')->toHtml(),
        ));

        $this->addTab('condition', array(
            'label'   => Mage::helper('giantpointsrule')->__('Conditions'),
            'title'   => Mage::helper('giantpointsrule')->__('Conditions'),
            'content' => $this->getLayout()->createBlock('giantpointsrule/adminhtml_earning_catalogrule_edit_tab_conditions')->toHtml(),
        ));

        $this->addTab('actions', array(
            'label'   => Mage::helper('giantpointsrule')->__('Actions'),
            'title'   => Mage::helper('giantpointsrule')->__('Actions'),
            'content' => $this->getLayout()->createBlock('giantpointsrule/adminhtml_earning_catalogrule_edit_tab_actions')->toHtml(),
        ));
        $this->_updateActiveTab();

        return parent::_beforeToHtml();
    }

    protected function _updateActiveTab()
    {
        $tabId = $this->getRequest()->getParam('tab');
        if ($tabId) {
            $tabId = preg_replace("#{$this->getId()}_#", '', $tabId);
            if ($tabId) {
                $this->setActiveTab($tabId);
            }
        }
    }
}
