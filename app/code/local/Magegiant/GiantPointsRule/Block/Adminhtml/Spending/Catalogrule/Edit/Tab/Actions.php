<?php
/**
 * Magegiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magegiant.com license that is
 * available through the world-wide-web at this URL:
 * https://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPoints
 * @copyright   Copyright (c) 2014 Magegiant (https://magegiant.com/)
 * @license     https://magegiant.com/license-agreement/
 */

/**
 * GiantPoints Rule Earning Catalog Edit Actions Tab Block
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPoints
 * @author      Magegiant Developer
 */
class Magegiant_GiantPointsRule_Block_Adminhtml_Spending_Catalogrule_Edit_Tab_Actions extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare tab form's information
     *
     * @return Magegiant_GiantPoints_Block_Adminhtml_Catalogrule_Edit_Tab_Actions
     */
    protected function _prepareForm()
    {
        if (Mage::getSingleton('adminhtml/session')->getFormData()) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData();
            Mage::getSingleton('adminhtml/session')->setFormData(null);
        } elseif (Mage::registry('catalogrule_data')) {
            $data = Mage::registry('catalogrule_data')->getData();
        }

        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('rule_');
        $this->setForm($form);
        $fieldset = $form->addFieldset('actions_fieldset', array('legend' => Mage::helper('giantpointsrule')->__('Update prices using the following information')));

        $fieldset->addField('discount_style', 'select', array(
            'label'   => Mage::helper('giantpointsrule')->__('Discount Style'),
            'title'   => Mage::helper('giantpointsrule')->__('Discount Style'),
            'name'    => 'discount_style',
            'options' => array(
                'by_fixed'   => Mage::helper('giantpointsrule')->__('By a fixed amount'),
                'to_fixed'   => Mage::helper('giantpointsrule')->__('To a fixed amount'),
                'by_percent' => Mage::helper('giantpointsrule')->__('By a percentage of the original price'),
                'to_percent' => Mage::helper('giantpointsrule')->__('To a percentage of the original price'),
            ),
        ));

        $fieldset->addField('discount_amount', 'text', array(
            'label'    => Mage::helper('giantpointsrule')->__('Discount amount'),
            'title'    => Mage::helper('giantpointsrule')->__('Discount amount'),
            'class'    => 'required-entry',
            'required' => true,
            'name'     => 'discount_amount',
            'note'     => Mage::helper('giantpointsrule')->__('Discount received for every X points (in tab Conditions)')
        ));
        $fieldset->addField('uses_per_product', 'text', array(
            'label' => Mage::helper('giantpointsrule')->__('Uses Allowed Per Product'),
            'title' => Mage::helper('giantpointsrule')->__('Uses Allowed Per Product'),
            'name'  => 'uses_per_product',
			'note'=> Mage::helper('giantpointsrule')->__('Fill 0 for unlimited')
        ));

        $fieldset->addField('stop_rules_processing', 'select', array(
            'label'   => Mage::helper('giantpointsrule')->__('Stop further rules processing'),
            'title'   => Mage::helper('giantpointsrule')->__('Stop further rules processing'),
            'name'    => 'stop_rules_processing',
            'options' => array(
                '1' => Mage::helper('giantpointsrule')->__('Yes'),
                '0' => Mage::helper('giantpointsrule')->__('No'),
            ),
        ));
        $form->setValues($data);

        return parent::_prepareForm();
    }
}
