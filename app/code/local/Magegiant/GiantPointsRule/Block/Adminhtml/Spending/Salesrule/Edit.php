<?php
/**
 * Magegiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the magegiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPoints
 * @copyright   Copyright (c) 2014 Magegiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * Giantpoints Block
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPoints
 * @author      Magegiant Developer
 */
class Magegiant_GiantPointsRule_Block_Adminhtml_Spending_Salesrule_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    const PAGE_TABS_BLOCK_ID = 'rule_tabs';

    public function __construct()
    {
        $this->_objectId   = 'id';
        $this->_blockGroup = 'giantpointsrule';
        $this->_controller = 'adminhtml_spending_salesrule';

        parent::__construct();

        $this->_updateButton('save', 'label', Mage::helper('giantpointsrule')->__('Save Rule'));
        $this->_updateButton('delete', 'label', Mage::helper('giantpointsrule')->__('Delete Rule'));
        $this->_addButton(
            'save_and_continue',
            array(
                'label'   => Mage::helper('giantpointsrule')->__('Save And Continue Edit'),
                'onclick' => 'saveAndContinueEdit(\'' . $this->_getSaveAndContinueUrl() . '\')',
                'class'   => 'save'
            ),
            10
        );
        $this->_formScripts[] = "
            function saveAndApply(){
                editForm.submit($('edit_form').action+'apply/1/');
            }
            function saveAndContinueEdit(urlTemplate){
                var urlTemplateSyntax = /(^|.|\\r|\\n)({{(\\w+)}})/;
                var template = new Template(urlTemplate, urlTemplateSyntax);
                var url = template.evaluate({tab_id:" . self::PAGE_TABS_BLOCK_ID . "JsTabs.activeTab.id});
                editForm.submit(url);
            }
        ";
    }

    protected function _getSaveAndContinueUrl()
    {
        return $this->getUrl('*/*/save', array(
            '_current'   => true,
            'back'       => 'edit',
            'tab'        => '{{tab_id}}',
            'active_tab' => null
        ));
    }

    public function getHeaderText()
    {
        $rule = Mage::registry('salesrule_data');
        if ($rule->getRuleId()) {
            return Mage::helper('giantpointsrule')->__("Edit Rule '%s'", $this->escapeHtml($rule->getName()));
        } else {
            return Mage::helper('giantpointsrule')->__('New Rule');
        }
    }

    /**
     * Overwrite Back Url
     *
     * @return string
     */
    public function getBackUrl()
    {
        if ($this->getRequest()->getParam('type')) {
            $typeId = $this->getRequest()->getParam('type');
        }
        if ($this->_getSalesRule()->getRuleType()) {
            $typeId = $this->_getSalesRule()->getRuleType();
        }

        return $this->getUrl('*/*/', array('type' => $typeId));
    }

    protected function _getSalesRule()
    {
        return Mage::registry('salesrule_data');
    }
}