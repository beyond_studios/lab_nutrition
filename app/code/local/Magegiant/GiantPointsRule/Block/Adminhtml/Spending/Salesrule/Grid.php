<?php
/**
 * Magegiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the magegiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPoints
 * @copyright   Copyright (c) 2014 Magegiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * Giantpoints Block
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPoints
 * @author      Magegiant Developer
 */
class Magegiant_GiantPointsRule_Block_Adminhtml_Spending_Salesrule_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('rule_id');
        $this->setDefaultSort('name');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('giantpointsrule/salesrule')->getCollection()
            ->addFieldToFilter('rule_type', Magegiant_GiantPointsRule_Model_RuleType::RULE_TYPE_SPENDING);
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $helper = Mage::helper('giantpointsrule');
        $this->addColumn(
            'rule_id',
            array(
                'header' => $helper->__('ID'),
                'align'  => 'right',
                'width'  => '50px',
                'index'  => 'rule_id',
                'type'   => 'number',
            )
        );

        $this->addColumn(
            'name',
            array(
                'header' => $helper->__('Rule Name'),
                'align'  => 'left',
                'index'  => 'name',
            )
        );

        $this->addColumn(
            'from_date',
            array(
                'header' => $helper->__('Date Start'),
                'align'  => 'left',
                'index'  => 'from_date',
                'type'   => 'date',
            )
        );

        $this->addColumn(
            'to_date',
            array(
                'header' => $helper->__('Date Expire'),
                'align'  => 'left',
                'index'  => 'to_date',
                'type'   => 'date',
            )
        );

        $this->addColumn(
            'priority',
            array(
                'header' => $helper->__('Priority'),
                'align'  => 'left',
                'index'  => 'priority',
                'type'   => 'number',
            )
        );

        $this->addColumn(
            'is_active',
            array(
                'header'  => $helper->__('Status'),
                'align'   => 'left',
                'width'   => '100px',
                'index'   => 'is_active',
                'type'    => 'options',
                'options' => array(
                    1 => 'Active',
                    0 => 'Inactive',
                ),
            )
        );

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $rule_type = $this->getRequest()->getParam('type');
        $this->setMassactionIdField('rule_id');
        $this->getMassactionBlock()->setFormFieldName('rule');

        $this->getMassactionBlock()->addItem(
            'delete',
            array(
                'label' => Mage::helper('giantpointsrule')->__('Delete'),
                'url'   => $this->getUrl('*/*/massDelete', array('type' => $rule_type)),
            )
        );

        $this->getMassactionBlock()->addItem(
            'activate',
            array(
                'label' => Mage::helper('giantpointsrule')->__('Activate'),
                'url'   => $this->getUrl('*/*/massActivate', array('type' => $rule_type)),
            )
        );

        $this->getMassactionBlock()->addItem(
            'deactivate',
            array(
                'label' => Mage::helper('giantpointsrule')->__('Inactivate'),
                'url'   => $this->getUrl('*/*/massDeactivate', array('type' => $rule_type)),
            )
        );

        return $this;
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array(
                'type' => Magegiant_GiantPointsRule_Model_RuleType::RULE_TYPE_SPENDING,
                'id'   => $row->getId()
            )
        );
    }
}