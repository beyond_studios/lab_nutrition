<?php
/**
 * Magegiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the magegiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPoints
 * @copyright   Copyright (c) 2014 Magegiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * Giantpoints Block
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPoints
 * @author      Magegiant Developer
 */
class Magegiant_GiantPointsRule_Block_Adminhtml_Spending_Salesrule_Edit_Tab_Actions extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $model = Mage::registry('salesrule_data');
        $form  = new Varien_Data_Form();
        $form->setHtmlIdPrefix('rule_');

        $helper   = Mage::helper('giantpoints');
        $fieldset = $form->addFieldset('points_action_fieldset', array('legend' => Mage::helper('giantpointsrule')->__('Update prices using the following information')));

        $fieldset->addField('discount_style', 'select', array(
            'label'   => Mage::helper('giantpointsrule')->__('Action'),
            'title'   => Mage::helper('giantpointsrule')->__('Action'),
            'name'    => 'discount_style',
            'options' => array(
                'cart_fixed' => Mage::helper('giantpointsrule')->__('Give a fixed discount amount for the whole cart'),
                'by_percent' => Mage::helper('giantpointsrule')->__('Give a percent discount amount for the whole cart'),
            ),
        ));

        $fieldset->addField('discount_amount', 'text', array(
            'label'    => Mage::helper('giantpointsrule')->__('Discount Amount'),
            'title'    => Mage::helper('giantpointsrule')->__('Discount Amount'),
            'name'     => 'discount_amount',
            'required' => true,
            'note'     => Mage::helper('giantpointsrule')->__('Discount received for every X points in tab Conditions')

        ));
        $fieldset->addField(
            'stop_rules_processing',
            'select',
            array(
                'label'   => $helper->__('Stop further rules processing'),
                'title'   => $helper->__('Stop further rules processing'),
                'name'    => 'stop_rules_processing',
                'options' => array(
                    '1' => $helper->__('Yes'),
                    '0' => $helper->__('No'),
                ),
            )
        );
        $renderer = Mage::getBlockSingleton('adminhtml/widget_form_renderer_fieldset')
            ->setTemplate('promo/fieldset.phtml')
            ->setNewChildUrl($this->getUrl('adminhtml/promo_quote/newActionHtml/form/rule_actions_fieldset'));

        $fieldset = $form->addFieldset('actions_fieldset', array('legend' => Mage::helper('giantpointsrule')->__('Apply the rule only to cart items matching the following conditions (leave blank for all items)')))->setRenderer($renderer);

        $fieldset->addField('actions', 'text', array(
            'label' => Mage::helper('giantpointsrule')->__('Apply To'),
            'title' => Mage::helper('giantpointsrule')->__('Apply To'),
            'name'  => 'actions',
        ))->setRule($model)->setRenderer(Mage::getBlockSingleton('rule/actions'));
        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }
}