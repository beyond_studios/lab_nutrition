<?php
/**
 * Magegiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magegiant.com license that is
 * available through the world-wide-web at this URL:
 * https://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPoints
 * @copyright   Copyright (c) 2014 Magegiant (https://magegiant.com/)
 * @license     https://magegiant.com/license-agreement/
 */

/**
 * GiantPoints Earning Catalog Edit Tab Conditions Block
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPoints
 * @author      Magegiant Developer
 */
class Magegiant_GiantPointsRule_Block_Adminhtml_Spending_Catalogrule_Edit_Tab_Conditions extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare tab form's information
     *
     * @return Magegiant_GiantPoints_Block_Adminhtml_Catalogrule_Edit_Tab_Conditions
     */
    protected function _prepareForm()
    {
        if (Mage::getSingleton('adminhtml/session')->getFormData()) {
            $data  = Mage::getSingleton('adminhtml/session')->getFormData();
            $model = Mage::getModel('giantpointsrule/catalogrule')
                ->load($data['rule_id'])
                ->setData($data);
            Mage::getSingleton('adminhtml/session')->setFormData(null);
        } elseif (Mage::registry('catalogrule_data')) {
            $model = Mage::registry('catalogrule_data');
            $data  = $model->getData();
        }

        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('rule_');

        $renderer = Mage::getBlockSingleton('adminhtml/widget_form_renderer_fieldset')
            ->setTemplate('promo/fieldset.phtml')
            ->setNewChildUrl($this->getUrl('adminhtml/promo_quote/newConditionHtml/form/rule_conditions_fieldset'));

        $fieldset = $form->addFieldset('conditions_fieldset', array('legend' => Mage::helper('giantpoints')->__('Apply the rule only if the following conditions are met (leave blank for all products)')))->setRenderer($renderer);

        $fieldset->addField('conditions', 'text', array(
            'name'     => 'conditions',
            'label'    => Mage::helper('giantpointsrule')->__('Conditions'),
            'title'    => Mage::helper('giantpointsrule')->__('Conditions'),
            'required' => true,
        ))->setRule($model)->setRenderer(Mage::getBlockSingleton('rule/conditions'));

        $fieldset = $form->addFieldset('actions_fieldset', array('legend' => Mage::helper('giantpointsrule')->__('Spending Point Action')));

        $fieldset->addField('simple_action', 'select', array(
            'label'    => Mage::helper('giantpointsrule')->__('Action'),
            'title'    => Mage::helper('giantpoints')->__('Action'),
            'name'     => 'simple_action',
            'options'  => Mage::getModel('giantpointsrule/catalogrule_simple_action_options_spending')->getOptionArray(),
            'onchange' => 'toggleSimpleAction()',
            'note'     => Mage::helper('giantpointsrule')->__('select the type to spend points')
        ));

        $fieldset->addField('point_amount', 'text', array(
            'label'    => Mage::helper('giantpointsrule')->__('Points (X)'),
            'title'    => Mage::helper('giantpointsrule')->__('Points (X)'),
            'class'    => 'required-entry',
            'required' => true,
            'name'     => 'point_amount',
        ));

        $fieldset->addField('money_step', 'text', array(
            'label'              => Mage::helper('giantpointsrule')->__('Money Step (Y)'),
            'title'              => Mage::helper('giantpointsrule')->__('Money Step (Y)'),
            'name'               => 'money_step',
            'after_element_html' => '<strong>[' . Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE) . ']</strong>',
        ));

        $fieldset->addField('max_points', 'text', array(
            'label' => Mage::helper('giantpointsrule')->__('Maximum Total of Points To Transfer (0 for unlimited)'),
            'title' => Mage::helper('giantpointsrule')->__('Maximum Total of Points To Transfer (0 for unlimited)'),
            'name'  => 'max_points',
        ));

        $form->setValues($data);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}