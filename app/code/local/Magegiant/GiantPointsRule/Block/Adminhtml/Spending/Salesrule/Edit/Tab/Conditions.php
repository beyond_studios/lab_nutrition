<?php
/**
 * Magegiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the magegiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPoints
 * @copyright   Copyright (c) 2014 Magegiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * Giantpoints Block
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPoints
 * @author      Magegiant Developer
 */
class Magegiant_GiantPointsRule_Block_Adminhtml_Spending_Salesrule_Edit_Tab_Conditions extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $model = Mage::registry('salesrule_data');
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('rule_');
        $helper = Mage::helper('giantpoints');

        $renderer = Mage::getBlockSingleton('adminhtml/widget_form_renderer_fieldset')
            ->setTemplate('promo/fieldset.phtml')
            ->setNewChildUrl($this->getUrl('adminhtml/promo_quote/newConditionHtml/form/rule_conditions_fieldset'));

        $fieldset = $form
            ->addFieldset(
                'conditions_fieldset',
                array(
                     'legend' => $helper->__(
                         'Apply the rule only if the following conditions are met (leave blank for all products)'
                     ),
                )
            )
            ->setRenderer($renderer)
        ;

        $fieldset
            ->addField(
                'conditions',
                'text',
                array(
                     'name'     => 'conditions',
                     'label'    => $helper->__('Conditions'),
                     'title'    => $helper->__('Conditions'),
                     'required' => true,
                )
            )
            ->setRule($model)
            ->setRenderer(Mage::getBlockSingleton('rule/conditions'))
        ;

        $fieldset = $form->addFieldset('action_fieldset', array('legend' => $helper->__('Spending Point Action')));
        $fieldset->addField('simple_action', 'select', array(
            'label'    => Mage::helper('giantpointsrule')->__('Action'),
            'title'    => Mage::helper('giantpointsrule')->__('Action'),
            'name'     => 'simple_action',
            'options'  => Mage::getModel('giantpointsrule/salesrule_simple_action_options_spending')->getOptionArray(),
//            'onchange' => 'toggleSimpleAction()',
            'note'     => Mage::helper('giantpointsrule')->__('select the type to spend points')
        ));

        $fieldset->addField('point_amount', 'text', array(
            'label'    => Mage::helper('giantpointsrule')->__('Points (X)'),
            'title'    => Mage::helper('giantpointsrule')->__('Points (X)'),
            'class'    => 'required-entry',
            'required' => true,
            'name'     => 'point_amount',
        ));


        $fieldset->addField('max_points', 'text', array(
            'label' => Mage::helper('giantpointsrule')->__('Maximum Redeemable Points'),
            'title' => Mage::helper('giantpointsrule')->__('Maximum Redeemable Points'),
            'name'  => 'max_points',
        ));
        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }
}