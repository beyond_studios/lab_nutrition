<?php
/**
 * Magegiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the magegiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPointsRule
 * @copyright   Copyright (c) 2014 Magegiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * GiantPointsRule Index Controller
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPointsRule
 * @author      Magegiant Developer
 */
class Magegiant_GiantPointsRule_Checkout_SpendingController extends Mage_Core_Controller_Front_Action
{
    /**
     * index action
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Remove current catalogrule spending points
     */
    public function removeCatalogRuleAction()
    {
        $item_id = $this->getRequest()->getParam('item_id');
        if (!is_null($item_id)) {
            $session      = Mage::getSingleton('checkout/session');
            $catalogRules = Mage::getSingleton('checkout/session')->getCatalogRules();
            if (is_array($catalogRules) && !empty($catalogRules) && array_key_exists($item_id, $catalogRules)) {
                $quote     = $this->_getQuote();
                $quoteItem = $quote->getItemById($item_id);
                $product   = $quoteItem->getProduct();
                $quoteItem->setCustomPrice($product->getFinalPrice());
                $quoteItem->setOriginalCustomPrice($product->getFinalPrice());
                $product->setIsSuperMode(true);
                $quote->collectTotals()->save();
                unset($catalogRules[$item_id]);
                $session->setCatalogRules($catalogRules);
                $session->addSuccess(Mage::helper('giantpointsrule')->__('All requested reward redemptions were removed from the product'));
            }

        }
        $this->_redirect('checkout/cart/');
    }

    public function checkboxRuleAction()
    {
        if (!$this->_isEnabled()) {
            $this->norouteAction();

            return;
        }
        $session = Mage::getSingleton('checkout/session');
        $session->setData('is_used_point', true);
        $rewardCheckedRules = $session->getRewardCheckedRules();
        if (!is_array($rewardCheckedRules)) $rewardCheckedRules = array();
        if ($ruleId = $this->getRequest()->getParam('rule_id')) {
            if ($this->getRequest()->getParam('is_used') == 'true') {
                $rewardCheckedRules[$ruleId] = array(
                    'rule_id'      => $ruleId,
                    'point_amount' => null,
                );
            } elseif (isset($rewardCheckedRules[$ruleId])) {
                unset($rewardCheckedRules[$ruleId]);
            }
            $session->setRewardCheckedRules($rewardCheckedRules);
        }
        $cart   = Mage::getSingleton('checkout/cart');
        $result = array();
        if ($cart->getQuote()->getItemsCount()) {
            $cart->init();
            $cart->save();
            $result = array(
                'success'  => true,
                'messages' => array(),
                'blocks'   => array(),
            );
            $session->getQuote()->collectTotals()->save();
            $result['blocks'] = $this->getUpdater()->getBlocks();
        } else {
            $result['reload'] = true;
        }
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    /**
     * Checkout onepage apply spend point
     */
    public function checkboxRuleOnepageAction()
    {
        if (!$this->_isEnabled()) {
            $this->norouteAction();

            return;
        }
        $session = Mage::getSingleton('checkout/session');
        $session->setData('is_used_point', true);
        $rewardCheckedRules = $session->getRewardCheckedRules();
        if (!is_array($rewardCheckedRules)) $rewardCheckedRules = array();
        if ($ruleId = $this->getRequest()->getParam('rule_id')) {
            if ($this->getRequest()->getParam('is_used') == 'true') {
                $rewardCheckedRules[$ruleId] = array(
                    'rule_id'      => $ruleId,
                    'point_amount' => null,
                );
            } elseif (isset($rewardCheckedRules[$ruleId])) {
                unset($rewardCheckedRules[$ruleId]);
            }
            $session->setRewardCheckedRules($rewardCheckedRules);
        }
        $result = array();
        $session->getQuote()->collectTotals()->save();
        $layout               = Mage::app()->getLayout();
        $fullTargetActionName = 'checkout_onepage_paymentmethod';
        $result['blocks']     = $this->getUpdater()->getBlocks($layout, $fullTargetActionName);
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    public function checkboxRuleGiantOnestepAction()
    {
        if (!$this->_isEnabled()) {
            $this->norouteAction();

            return;
        }
        $session = Mage::getSingleton('checkout/session');
        $session->setData('is_used_point', true);
        $rewardCheckedRules = $session->getRewardCheckedRules();
        if (!is_array($rewardCheckedRules)) $rewardCheckedRules = array();
        if ($ruleId = $this->getRequest()->getParam('rule_id')) {
            if ($this->getRequest()->getParam('is_used') == 'true') {
                $rewardCheckedRules[$ruleId] = array(
                    'rule_id'      => $ruleId,
                    'point_amount' => null,
                );
            } elseif (isset($rewardCheckedRules[$ruleId])) {
                unset($rewardCheckedRules[$ruleId]);
            }
            $session->setRewardCheckedRules($rewardCheckedRules);
        }
        $result = array();
        $session->getQuote()->collectTotals()->save();
        $layout               = Mage::app()->getLayout();
        $fullTargetActionName = 'onestepcheckout_index_index';
        $result['blocks']     = $this->getUpdater()->getBlocks($layout, $fullTargetActionName);
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    private function _isEnabled($store = null)
    {
        return (Mage::helper('giantpoints/config')->isEnabled($store) && Mage::helper('giantpointsrule/config')->isEnabled($store));
    }

    /**
     * @return Magegiant_GiantPoints_Model_Updater
     */
    public function getUpdater()
    {
        return Mage::getSingleton('giantpoints/updater');
    }

    protected function _getQuote()
    {
        return Mage::getSingleton('checkout/session')->getQuote();
    }
}