<?php
/**
 * Magegiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the magegiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPoints
 * @copyright   Copyright (c) 2014 Magegiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * GiantPoints Rule Controller
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPoints
 * @author      Magegiant Developer
 */
class Magegiant_GiantPointsRule_Adminhtml_Giantpointsruleadmin_SystemController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Reset all transaction
     */
    public function reinstallAction()
    {
        try {

            $coreResourceTable = Mage::getSingleton('core/resource')->getTableName('core/resource');
            $write = Mage::getSingleton('core/resource')->getConnection('core_write');
            $write->exec("delete from `{$coreResourceTable}` WHERE `code`='giantpoints_rule_setup'");
            echo 'success';
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return $this;
    }

}