<?php
/**
 * Magegiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magegiant.com license that is
 * available through the world-wide-web at this URL:
 * https://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPoints
 * @copyright   Copyright (c) 2014 Magegiant (https://magegiant.com/)
 * @license     https://magegiant.com/license-agreement/
 */

/**
 * GiantPoints Earning Catalog Adminhtml Controller
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPointsRule
 * @author      Magegiant Developer
 */
class Magegiant_GiantPointsRule_Adminhtml_Giantpointsruleadmin_CatalogruleController extends Mage_Adminhtml_Controller_Action
{
    /**
     * init layout and set active for current menu
     *
     * @return Magegiant_GiantPoints_Adminhtml_Earning_CatalogruleController
     */
    protected $_dirtyRulesNoticeMessage;

    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('giantpoints')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Rules Manager'), Mage::helper('adminhtml')->__('Rule Manager'));

        return $this;
    }

    /**
     * index action
     */
    public function indexAction()
    {
        $dirtyRules = Mage::getModel('giantpointsrule/flag')->loadSelf();
        if ($dirtyRules->getState()) {
            Mage::getSingleton('adminhtml/session')->addNotice($this->getDirtyRulesNoticeMessage());
        }
        $this->_initAction();
        $ruleType = $this->getRequest()->getParam('type');
        if ($ruleType == Magegiant_GiantPointsRule_Model_RuleType::RULE_TYPE_EARNING) {
            $this->_title($this->__('Reward Points'))
                ->_title($this->__('Catalog Earning Rule'));
            $this->_addContent($this->getLayout()->createBlock('giantpointsrule/adminhtml_earning_catalogrule'));
        } else {
            $this->_title($this->__('Reward Points'))
                ->_title($this->__('Catalog Spending Rule'));
            $this->_addContent($this->getLayout()->createBlock('giantpointsrule/adminhtml_spending_catalogrule'));
        }
        $this->renderLayout();
    }

    /**
     * view and edit item action, if item is new then view blank
     */
    public function editAction()
    {
        $id       = $this->getRequest()->getParam('id');
        $model    = Mage::getModel('giantpointsrule/catalogrule')->load($id);
        $ruleType = $this->getRequest()->getParam('type');
        if ($model->getId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }
            $model->getConditions()->setJsFormObject('rule_conditions_fieldset');
            Mage::register('catalogrule_data', $model);

            if ($model->getId()) {
                $this->_title($model->getTitle());
            } else {
                $this->_title($this->__('New rule'));
            }

            $this->loadLayout();
            $this->_setActiveMenu('giantpoints');

            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Rule Manager'), Mage::helper('adminhtml')->__('Rule Manager'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Rule News'), Mage::helper('adminhtml')->__('Rule News'));
            $this->getLayout()->getBlock('head')
                ->setCanLoadExtJs(true)
                ->setCanLoadRulesJs(true);
            if ($ruleType == 1) {
                $this->_title($this->__('Reward Points'))
                    ->_title($this->__('Catalog Earning Rule'));
                $this->_addContent($this->getLayout()->createBlock('giantpointsrule/adminhtml_earning_catalogrule_edit'))
                    ->_addLeft($this->getLayout()->createBlock('giantpointsrule/adminhtml_earning_catalogrule_edit_tabs'));
            } else {
                $this->_title($this->__('Reward Points'))
                    ->_title($this->__('Catalog Spending Rule'));
                $this->_addContent($this->getLayout()->createBlock('giantpointsrule/adminhtml_spending_catalogrule_edit'))
                    ->_addLeft($this->getLayout()->createBlock('giantpointsrule/adminhtml_spending_catalogrule_edit_tabs'));

            }
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('giantpoints')->__('The item does not exist'));
            $this->_redirect('*/*/', array('type' => $this->getRequest()->getParam('type')));
        }
    }

    /**
     * new action is create new item
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * save action is save item
     */
    public function saveAction()
    {
        if ($this->getRequest()->getPost()) {
            try {
                $model = Mage::getModel('giantpointsrule/catalogrule');
                Mage::dispatchEvent(
                    'adminhtml_controller_giantpointsrule_catalogrule_prepare_save',
                    array('request' => $this->getRequest())
                );
                $data = $this->getRequest()->getPost();
                $data = $this->_filterDates($data, array('from_date', 'to_date'));
                if ($id = $this->getRequest()->getParam('rule_id')) {
                    $model->load($id);
                    if ($id != $model->getId()) {
                        Mage::throwException(Mage::helper('giantpointsrule')->__('Wrong rule specified.'));
                    }
                }

                $validateResult = $model->validateData(new Varien_Object($data));
                if ($validateResult !== true) {
                    foreach ($validateResult as $errorMessage) {
                        $this->_getSession()->addError($errorMessage);
                    }
                    $this->_getSession()->setPageData($data);
                    $this->_redirect('*/*/edit', array(
                        'type' => $model->getRuleType(),
                        'id'   => $model->getId()));

                    return;
                }
                $data['conditions'] = $data['rule']['conditions'];
                unset($data['rule']);

                $autoApply = false;
                if (!empty($data['auto_apply'])) {
                    $autoApply = true;
                    unset($data['auto_apply']);
                }
                $model->loadPost($data);

                Mage::getSingleton('adminhtml/session')->setPageData($model->getData());

                try {
                    $model->save();
                } catch (Exception $e) {
                    Mage::helper('giantpoints')->log($e->getMessage());
                }

                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('giantpointsrule')->__('The rule has been saved.')
                );
                Mage::getSingleton('adminhtml/session')->setPageData(false);
                if ($autoApply) {
                    $this->getRequest()->setParam('rule_id', $model->getId());
                    $this->_forward('applyRules');
                } else {
                    Mage::getModel('giantpointsrule/flag')->loadSelf()
                        ->setState(1)
                        ->save();
                    if ($this->getRequest()->getParam('back')) {
                        $this->_redirect('*/*/edit', array(
                                'type'     => $model->getRuleType(),
                                'id'       => $model->getId(),
                                '_current' => true
                            )
                        );

                        return;
                    }
                    $this->_redirect('*/*/', array('type' => $model->getRuleType()));
                }

                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(
                    Mage::helper('giantpointsrule')->__('An error occurred while saving the rule data. Please review the log and try again.')
                );
                Mage::logException($e);
                Mage::getSingleton('adminhtml/session')->setPageData($data);
                $this->_redirect('*/*/edit', array(
                    'type' => $this->getRequest()->getParam('type'),
                    'id'   => $this->getRequest()->getParam('rule_id'),
                ));

                return;
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     * Apply all active catalog price rules
     */
    public function applyRulesAction()
    {
        $errorMessage = Mage::helper('catalogrule')->__('Unable to apply rules.');
        try {
            Mage::getModel('giantpointsrule/catalogrule')->applyAll(); //Apply all giant points catalog rule
            Mage::getModel('giantpointsrule/flag')->loadSelf()
                ->setState(0)
                ->save();
            $this->_getSession()->addSuccess(Mage::helper('giantpointsrule')->__('The rules have been applied.'));
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($errorMessage . ' ' . $e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()->addError($errorMessage);
            Mage::logException($e);
        }
        $this->_redirect('*/*', array('type' => $this->getRequest()->getParam('type', 1)));
    }

    /**
     * delete action is delete item
     */
    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('giantpointsrule/catalogrule');
                $model->setId($this->getRequest()->getParam('id'))
                    ->delete();
                Mage::getModel('giantpointsrule/flag')->loadSelf()
                    ->setState(1)
                    ->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Rule was successfully deleted'));
                $this->_redirect('*/*/', array('type' => 1));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array(
                    'id'   => $this->getRequest()->getParam('id'),
                    'type' => $this->getRequest()->getParam('type', 1)
                ));
            }
        }
        $this->_redirect('*/*', array('type' => $this->getRequest()->getParam('type', 1)));
    }

    public function setDirtyRulesNoticeMessage($dirtyRulesNoticeMessage)
    {
        $this->_dirtyRulesNoticeMessage = $dirtyRulesNoticeMessage;
    }

    public function getDirtyRulesNoticeMessage()
    {
        $defaultMessage = Mage::helper('giantpointsrule')->__('There are rules that have been changed but were not applied. Please, click Apply Rules in order to see immediate effect in the catalog.');

        return $this->_dirtyRulesNoticeMessage ? $this->_dirtyRulesNoticeMessage : $defaultMessage;
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('giantpoints');
    }
}
