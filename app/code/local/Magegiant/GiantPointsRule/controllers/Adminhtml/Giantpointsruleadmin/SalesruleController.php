<?php
/**
 * Magegiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the magegiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPoints
 * @copyright   Copyright (c) 2014 Magegiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * GiantPoints Rule Controller
 *
 * @category    Magegiant
 * @package     Magegiant_GiantPoints
 * @author      Magegiant Developer
 */
class Magegiant_GiantPointsRule_Adminhtml_Giantpointsruleadmin_SalesruleController extends Mage_Adminhtml_Controller_Action
{

    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('giantpoints')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Rules Manager'), Mage::helper('adminhtml')->__('Rule Manager'));

        return $this;
    }

    public function indexAction()
    {
        $this->_initAction();
        $ruleType = $this->getRequest()->getParam('type', 1);
        if ($ruleType == Magegiant_GiantPointsRule_Model_RuleType::RULE_TYPE_EARNING) {
            $this->_title($this->__('Reward Points'))
                ->_title($this->__('Shopping Cart Earning Rule'));
            $this->_addContent($this->getLayout()->createBlock('giantpointsrule/adminhtml_earning_salesrule'));
        } else {
            $this->_title($this->__('Reward Points'))
                ->_title($this->__('Shopping Cart Spending Rule'));
            $this->_addContent($this->getLayout()->createBlock('giantpointsrule/adminhtml_spending_salesrule'));
        }
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $id    = $this->getRequest()->getParam('id');
        $model = Mage::getModel('giantpointsrule/salesrule');

        if ($id) {
            $model->load($id);
            if (!$model->getRuleId()) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('giantpointsrule')->__('This rule no longer exists')
                );

                return $this->_redirect('*/*', array('type' => $this->getRequest()->getParam('type')));
            }
        }

        $model->getConditions()->setJsFormObject('rule_conditions_fieldset');
        Mage::register('salesrule_data', $model);
        $this
            ->loadLayout()
            ->_setActiveMenu('giantpoints');
        $type  = $this->getRequest()->getParam('type') == 1 ? 'earning' : 'spending';
        $block = $this->getLayout()->createBlock('giantpointsrule/adminhtml_' . $type . '_salesrule_edit')
            ->setData('action', $this->getUrl('*/giantpointsrule_salesrule/save'));
        if ($type == 'earning') {
            $this->_title($this->__('Reward Points'))
                ->_title($this->__('Shopping Cart Earning Rule'));
        } else {
            $this->_title($this->__('Reward Points'))
                ->_title($this->__('Shopping Cart Spending Rule'));
        }
        $this->getLayout()->getBlock('head')
            ->setCanLoadExtJs(true)
            ->setCanLoadRulesJs(true)
            ->addItem('js', 'tiny_mce/tiny_mce.js')
            ->addItem('js', 'mage/adminhtml/wysiwyg/tiny_mce/setup.js')
            ->addJs('mage/adminhtml/browser.js')
            ->addJs('prototype/window.js')
            ->addJs('lib/flex.js')
            ->addJs('mage/adminhtml/flexuploader.js');
        $this
            ->_addContent($block)
            ->_addLeft($this->getLayout()->createBlock('giantpointsrule/adminhtml_' . $type . '_salesrule_edit_tabs'))
            ->renderLayout();
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            try {
                $redirectBack = $this->getRequest()->getParam('back', false);
                $data         = $this->_filterDates($data, array('from_date', 'to_date'));
                if (isset($data['from_date']) && $data['from_date'] instanceof Zend_Date) {
                    $data['from_date'] = $data['from_date']->toString(VARIEN_DATE::DATE_INTERNAL_FORMAT);
                }
                if (isset($data['to_date']) && $data['to_date'] instanceof Zend_Date) {
                    $data['to_date'] = $data['to_date']->toString(VARIEN_DATE::DATE_INTERNAL_FORMAT);
                }
                if (!empty($data['from_date']) && !empty($data['to_date'])) {
                    $fromDate = new Zend_Date($data['from_date'], VARIEN_DATE::DATE_INTERNAL_FORMAT);
                    $toDate   = new Zend_Date($data['to_date'], VARIEN_DATE::DATE_INTERNAL_FORMAT);

                    if ($fromDate->compare($toDate) === 1) {
                        throw new Exception($this->__("'To Date' must be equal or more than 'From Date'"));
                    }
                }

                $model = Mage::getModel('giantpointsrule/salesrule');
                if (isset($data['rule'])) {
                    $rules = $data['rule'];
                    if (isset($rules['conditions'])) {
                        $data['conditions'] = $rules['conditions'];
                    }
                    if (isset($rules['actions'])) {
                        $data['actions'] = $rules['actions'];
                    }
                    unset($data['rule']);
                }
                $model->loadPost($data);

                if ($this->getRequest()->getParam('_save_as_flag')) {
                    $model->setId(null);
                }

                $model->save();

                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('giantpoints')->__('Rule was successfully saved')
                );
                if ($redirectBack) {
                    $this->_redirect(
                        '*/*/edit', array(
                            'id'       => $model->getId(),
                            'type'     => $model->getRuleType(),
                            '_current' => true
                        )
                    );

                    return;
                }

                return $this->_redirect('*/*/', array('type' => $model->getRuleType()));

            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setPageData($data);
                $this->_redirect('*/*/edit', array(
                    'id'   => $this->getRequest()->getParam('rule_id'),
                    'type' => $this->getRequest()->getParam('type')
                ));

                return;
            }
        }
        $this->_redirect('*/*/');

        return;
    }

    public function relatedGridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('giantpoints/adminhtml_rule_edit_tab_related')->toHtml()
        );
    }

    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                $salesRule = Mage::getModel('giantpointsrule/salesrule')->load($id);
                $type      = $salesRule->getRuleType();
                $salesRule->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('giantpoints')->__('Rule was successfully deleted')
                );

                return $this->_redirect('*/*/', array('type' => $type));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());

                return $this->_redirect('*/*/edit', array('id' => $id, 'type' => $type));
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('giantpoints')->__('Unable to find a page to delete')
        );

        return $this->_redirect('*/*/');
    }

    public function massDeleteAction()
    {
        try {
            $rulesToDelete = $this->getRequest()->getParam('rule');
            foreach ($rulesToDelete as $ruleToDelete) {
                Mage::getModel('giantpointsrule/salesrule')->load($ruleToDelete)->delete();
            }
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Successfully deleted'));
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('advancednewsletter')->__('Unable to find item to save')
            );
        }
        $this->_redirect('*/*/index', array('type' => $this->getRequest()->getParam('type')));
    }

    public function massActivateAction()
    {
        try {
            $rulesToDelete = $this->getRequest()->getParam('rule');
            foreach ($rulesToDelete as $ruleToDelete) {
                Mage::getModel('giantpointsrule/salesrule')->load($ruleToDelete)->setData('is_active', 1)->save();
            }
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Successfully deleted'));
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('giantpointsrule')->__('Unable to find item to save')
            );
        }
        $this->_redirect('*/*/index', array('type' => $this->getRequest()->getParam('type')));
    }

    public function massDeactivateAction()
    {
        try {
            $rulesToDelete = $this->getRequest()->getParam('rule');
            foreach ($rulesToDelete as $ruleToDelete) {
                Mage::getModel('giantpointsrule/salesrule')->load($ruleToDelete)->setData('is_active', 0)->save();
            }
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Successfully deleted'));
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('giantpointsrule')->__('Unable to find item to save')
            );
        }
        $this->_redirect('*/*/index', array('type' => $this->getRequest()->getParam('type')));
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('giantpoints');
    }
}