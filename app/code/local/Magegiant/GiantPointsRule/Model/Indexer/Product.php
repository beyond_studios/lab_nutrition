<?php

/**
 * GiantPointsRule Status Model
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsRule
 * @author      MageGiant Developer
 */
class Magegiant_GiantPointsRule_Model_Indexer_Product extends Mage_Index_Model_Indexer_Abstract
{
    /**
     * Data key for matching result to be saved in
     */
    protected $_matchedEntities = array(
        Mage_Catalog_Model_Product::ENTITY => array(
            Mage_Index_Model_Event::TYPE_SAVE,
            Mage_Index_Model_Event::TYPE_MASS_ACTION
        )
    );
    // var to protect multiple runs
    protected $_registered = false;
    protected $_processed = false;
    protected $_categoryId = 0;
    protected $_productIds = array();

    /**
     * not sure why this is required.
     * _registerEvent is only called if this function is included.
     *
     * @param Mage_Index_Model_Event $event
     * @return bool
     */
    public function matchEvent(Mage_Index_Model_Event $event)
    {
        return Mage::getModel('catalog/category_indexer_product')->matchEvent($event);
    }

    public function getName()
    {
        return Mage::helper('giantpointsrule')->__('Reward Points Product');
    }

    public function getDescription()
    {
        return Mage::helper('giantpointsrule')->__('Refresh Reward Points Product');
    }

    protected function _registerEvent(Mage_Index_Model_Event $event)
    {
        // if event was already registered once, then no need to register again.
        if ($this->_registered)
            return $this;

        $entity = $event->getEntity();
        switch ($entity) {
            case Mage_Catalog_Model_Product::ENTITY:
                $this->_registerGiantProductEvent($event);
                break;
            case Mage_Catalog_Model_Category::ENTITY:
                $this->_registerCategoryEvent($event);
                break;

            case Mage_Catalog_Model_Convert_Adapter_Product::ENTITY:
                $event->addNewData('giantpoints_indexer_reindex_all', true);
                break;

            case Mage_Core_Model_Store::ENTITY:
            case Mage_Core_Model_Store_Group::ENTITY:
                $this->_changeProcessStatus($event);
                break;
        }
        $this->_registered = true;

        return $this;
    }

    protected function _registerGiantProductEvent(Mage_Index_Model_Event $event)
    {
        $eventType = $event->getType();
        if ($eventType == Mage_Index_Model_Event::TYPE_SAVE) {
            $product = $event->getDataObject();
            /**
             * Check if product categories data was changed
             */
            if ($product->getIsChangedCategories() || $product->dataHasChangedFor('status')
                || $product->dataHasChangedFor('visibility') || $product->getIsChangedWebsites()
            ) {
                $this->_productIds = $event->getDataObject()->getData('product_ids');
                $this->_changeProcessStatus($event);
            }

        } else if ($eventType == Mage_Index_Model_Event::TYPE_MASS_ACTION) {
            /* @var $actionObject Varien_Object */
            $actionObject = $event->getDataObject();
            $attributes   = array('status', 'visibility');
            $rebuildIndex = false;

            // check if attributes changed
            $attrData = $actionObject->getAttributesData();
            if (is_array($attrData)) {
                foreach ($attributes as $attributeCode) {
                    if (array_key_exists($attributeCode, $attrData)) {
                        $rebuildIndex = true;
                        break;
                    }
                }
            }

            // check changed websites
            if ($actionObject->getWebsiteIds()) {
                $rebuildIndex = true;
            }

            // register affected products
            if ($rebuildIndex) {
                $this->_changeProcessStatus($event);
            }
        }
    }


    protected function _changeProcessStatus($event)
    {
        $process           = $event->getProcess();
        $process->changeStatus(Mage_Index_Model_Process::STATUS_REQUIRE_REINDEX);
    }

    /**
     * Register event data during product save process
     *
     * @param Mage_Index_Model_Event $event
     */
    protected function _registerProductEvent(Mage_Index_Model_Event $event)
    {
        $eventType = $event->getType();
        if ($eventType == Mage_Index_Model_Event::TYPE_SAVE) {
            $this->reindexAll();
        }
    }

    /**
     * Register event data during category save process
     *
     * @param Mage_Index_Model_Event $event
     */
    protected function _registerCategoryEvent(Mage_Index_Model_Event $event)
    {
        $category = $event->getDataObject();
        /**
         * Check if product categories data was changed
         * Check if category has another affected category ids (category move result)
         */
        if ($category->getIsChangedProductList() || $category->getAffectedCategoryIds()) {
            $this->_categoryId = $event->getDataObject()->getData('entity_id');
            $this->_changeProcessStatus($event);
        }
    }

    protected function _processEvent(Mage_Index_Model_Event $event)
    {
        // process index event
        if (!$this->_processed) {
            $this->_processed = true;
        }
    }

    public function reindexAll()
    {
        // reindex all data
        Mage::getModel('giantpointsrule/catalogrule')->applyAll();
    }

}
