<?php

class Magegiant_GiantPointsRule_Model_Catalog_Product extends Mage_Catalog_Model_Product
{
    public function getCatalogPointsForRule($rule)
    {
        if ($rule instanceof Magegiant_GiantPointsRule_Model_Catalogrule) {
            $rule_id = $rule->getId();
        } else {
            $rule_id = $rule;
        }

        // calculate the proper points quantity based on rule and item
        $points = Mage::helper('giantpointsrule/calculation_earning')->calcPointsForCatalogRule($rule_id, $this, true);

        return $points;
    }

    /**
     * Loads in a salesrule and returns a points salesrule
     *
     * @param Mage_Catalog_Model_Product $product
     */
    public static function wrap(Mage_Catalog_Model_Product $product)
    {
        $rewards_product = Mage::getModel('giantpointsrule/catalog_product')
            ->setData($product->getData())->setId($product->getId());

        return $rewards_product;
    }

    public function getTotalEarningByRule()
    {
        $total_earned = 0;
        foreach ($this->getAvailableEarningRules() as $rule) {
            $total_earned += $rule->getPointAmount();
        }

        return $total_earned;
    }

    public function getEarnablePoints()
    {
        if (!$this->hasData("earnable_points"))
            $this->setData("earnable_points", $this->getTotalEarningByRule());

        return $this->getData("earnable_points");
    }

    public function getCatalogRuleCollection()
    {
        if (!$this->hasData("all_catalog_rules"))
            $this->setData("all_catalog_rules", $this->getAvailableEarningRules());

        return $this->getData("all_catalog_rules");
    }

    /**
     * Calculates all the points being earned from distribution rules.
     *
     * @return array
     */
    public function getAvailableEarningRules()
    {
        if (!$this->getId()) {
            return array();
        }
        $ruleIds = $this->getCatalogRewardsRuleIdsForProduct();
        $rules   = array();
        if (count($ruleIds)) {
            foreach ($ruleIds as $ruleId) {
                $pointsEarned = Mage::helper('giantpointsrule/calculation_earning')->calcPointsForCatalogRule($ruleId, $this);
                if ($this->isGrouped()) {
                    $associated_prod = $this->getTypeInstance(true)->getAssociatedProducts($this);
                    $amount          = 0;
                    foreach ($associated_prod as $item) {
                        $itemEarned = Mage::helper('giantpointsrule/calculation_earning')->calcPointsForCatalogRule($ruleId, $item, true);
                        $amount     = $itemEarned + $amount;
                    }
                    $pointsEarned = $amount;
                }

                if ($pointsEarned == 0) {
                    continue;
                }
                $rules[] = new Varien_Object(array(
                    'rule'         => Mage::getModel('giantpointsrule/catalogrule')->load($ruleId),
                    'point_amount' => $pointsEarned,
                ));
            }
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function getAvailableCatalogSpendingRules($websiteId = null, $customerGroupId = null, $date = null)
    {
        if (!$this->getId()) {
            return array();
        }
        $ruleIds = $this->getCatalogRewardsRuleIdsForProduct(2,$websiteId,$customerGroupId,$date);
        $rules   = array();
        if (count($ruleIds)) {
            foreach ($ruleIds as $ruleId) {
                $pointsSpent = Mage::helper('giantpointsrule/calculation_spending')->calcPointsForCatalogRule($ruleId, $this);
                if ($this->isGrouped()) {
                    $associated_prod = $this->getTypeInstance(true)->getAssociatedProducts($this);
                    $amount          = 0;
                    foreach ($associated_prod as $item) {
                        $itemEarned = Mage::helper('giantpointsrule/calculation_spending')->calcPointsForCatalogRule($ruleId, $item, true);
                        $amount     = $itemEarned + $amount;
                    }
                    $pointsSpent = $amount;
                }

                if ($pointsSpent == 0) {
                    continue;
                }
                $rules[] = new Varien_Object(array(
                    'rule'         => Mage::getModel('giantpointsrule/catalogrule')->load($ruleId),
                    'point_amount' => $pointsSpent,
                ));
            }
        }

        return $rules;
    }

    /**
     * Gets a list of all rule ID's that are associated with the given product id.
     *
     * @see THIS GETS ALL RULES!!!!!!
     *
     * @param integer $wId website id
     * @param integer $gId group id
     * @return  array(int)                          : An array of rule ID's that are associated with the item
     */
    public function getCatalogRewardsRuleIdsForProduct($type = 1, $wId = null, $gId = null, $date = null)
    {
        // look up all rule objects associated with this item
        $now = ($date == null) ? Mage::helper('giantpoints')->getMageDate() : $date;
        $wId = ($wId == null) ? Mage::app()->getStore()->getWebsiteId() : $wId;

        $gId       = ($gId == null) ? Mage::getSingleton('customer/session')->getCustomerGroupId() : $gId;
        $productId = $this->getId();
        $rule_ids  = array();
        $rule_data = Mage::getResourceModel('giantpointsrule/catalogrule')->getRulesFromProduct($now, $wId, $gId, $productId, $type);
        if ($rule_data) {
            foreach ($rule_data as $rule) {
                $rule        = (array)$rule;
                $rule_ids [] = (int)$rule['rule_id'];
                if ($rule['action_stop']) {
                    break;
                }
            }
        }

        // For older versions of Magento we can use getRuleProductsForDateRange

        $rule_ids = array_unique($rule_ids);

        return $rule_ids;
    }
}
