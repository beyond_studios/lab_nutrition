<?php

/**
 * GiantPointsRule Status Model
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsRule
 * @author      MageGiant Developer
 */
class Magegiant_GiantPointsRule_Model_Resource_Catalogrule extends Magegiant_GiantPointsRule_Model_Rule_Resource_Abstract
{
    /**
     * Store number of seconds in a day
     */
    const SECONDS_IN_DAY = 86400;

    /**
     * Number of products in range for insert
     */
    const RANGE_PRODUCT_STEP = 1000000;

    /**
     * Store associated with rule entities information map
     *
     * @var array
     */
    protected $_associatedEntitiesMap = array(
        'website'        => array(
            'associations_table' => 'giantpointsrule/catalogrule_website',
            'rule_id_field'      => 'rule_id',
            'entity_id_field'    => 'website_id'
        ),
        'customer_group' => array(
            'associations_table' => 'giantpointsrule/catalogrule_customer_group',
            'rule_id_field'      => 'rule_id',
            'entity_id_field'    => 'customer_group_id'
        )
    );

    /**
     * Factory instance
     *
     * @var Mage_Core_Model_Factory
     */
    protected $_factory;

    /**
     * App instance
     *
     * @var Mage_Core_Model_App
     */
    protected $_app;

    /**
     * Constructor with parameters
     * Array of arguments with keys
     *  - 'factory' Mage_Core_Model_Factory
     *
     * @param array $args
     */
    public function __construct(array $args = array())
    {
        $this->_factory = !empty($args['factory']) ? $args['factory'] : Mage::getSingleton('giantpointsrule/factory');
        $this->_app     = !empty($args['app']) ? $args['app'] : Mage::app();
        parent::__construct();
    }

    /**
     * Initialize main table and table id field
     */
    protected function _construct()
    {
        $this->_init('giantpointsrule/catalogrule', 'rule_id');
    }

    /**
     * Add customer group ids and website ids to rule data after load
     *
     * @param Mage_Core_Model_Abstract $object
     *
     * @return Mage_CatalogRule_Model_Resource_Rule
     */
    protected function _afterLoad(Mage_Core_Model_Abstract $object)
    {
        $object->setData('customer_group_ids', (array)$this->getCustomerGroupIds($object->getId()));
        $object->setData('website_ids', (array)$this->getWebsiteIds($object->getId()));

        return parent::_afterLoad($object);
    }

    /**
     * Bind catalog rule to customer group(s) and website(s).
     * Update products which are matched for rule.
     *
     * @param Mage_Core_Model_Abstract $object
     *
     * @return Mage_CatalogRule_Model_Resource_Rule
     */
    protected function _afterSave(Mage_Core_Model_Abstract $object)
    {

        if ($object->hasWebsiteIds()) {
            $websiteIds = $object->getWebsiteIds();
            if (!is_array($websiteIds)) {
                $websiteIds = explode(',', (string)$websiteIds);
            }
            $this->bindRuleToEntity($object->getId(), $websiteIds, 'website');
        }

        if ($object->hasCustomerGroupIds()) {
            $customerGroupIds = $object->getCustomerGroupIds();
            if (!is_array($customerGroupIds)) {
                $customerGroupIds = explode(',', (string)$customerGroupIds);
            }
            $this->bindRuleToEntity($object->getId(), $customerGroupIds, 'customer_group');
        }

        parent::_afterSave($object);

        return $this;
    }

    /**
     * Deletes records in catalogrule/product_data by rule ID and product IDs
     *
     * @param int   $ruleId
     * @param array $productIds
     */
    public function cleanProductData($ruleId, array $productIds = array())
    {
        /** @var $write Varien_Db_Adapter_Interface */
        $write = $this->_getWriteAdapter();

        $conditions = array('rule_id = ?' => $ruleId);

        if (count($productIds) > 0) {
            $conditions['product_id IN (?)'] = $productIds;
        }

        $write->delete($this->getTable('giantpointsrule/catalogrule_product'), $conditions);
    }

    /**
     * Return whether the product fits the rule
     *
     * @param Magegiant_GiantPointsRule_Model_Catalogrule $rule
     * @param Varien_Object                               $product
     * @param array                                       $websiteIds
     * @return bool
     */
    public function validateProduct(Magegiant_GiantPointsRule_Model_Catalogrule $rule, Varien_Object $product, $websiteIds = array())
    {
        /** @var $helper Mage_Catalog_Helper_Product_Flat */
        $helper = $this->_factory->getHelper('catalog/product_flat');
        if ($helper->isEnabled() && $helper->isBuiltAllStores()) {
            /** @var $store Mage_Core_Model_Store */
            foreach ($this->_app->getStores(false) as $store) {
                if (count($websiteIds) == 0 || in_array($store->getWebsiteId(), $websiteIds)) {
                    /** @var $selectByStore Varien_Db_Select */
                    $selectByStore = $rule->getProductFlatSelect($store->getId());
                    $selectByStore->where('p.entity_id = ?', $product->getId());
                    $selectByStore->limit(1);
                    if ($this->_getReadAdapter()->fetchOne($selectByStore)) {
                        return true;
                    }
                }
            }

            return false;
        } else {
            return $rule->getConditions()->validate($product);
        }
    }

    /**
     * Inserts rule data into catalogrule/rule_product table
     *
     * @param Magegiant_GiantPointsRule_Model_Catalogrule $rule
     * @param array                                       $websiteIds
     * @param array                                       $productIds
     */
    public function insertRuleData(Magegiant_GiantPointsRule_Model_Catalogrule $rule, array $websiteIds, array $productIds = array())
    {
        /** @var $write Varien_Db_Adapter_Interface */
        $write = $this->_getWriteAdapter();

        $customerGroupIds = $rule->getCustomerGroupIds();

        $fromTime = (int)strtotime($rule->getFromDate());
        $toTime   = (int)strtotime($rule->getToDate());
        $toTime   = $toTime ? ($toTime + self::SECONDS_IN_DAY - 1) : 0;

        /** @var Mage_Core_Model_Date $coreDate */
        $coreDate  = $this->_factory->getModel('core/date');
        $timestamp = $coreDate->gmtTimestamp('Today');
        if ($fromTime > $timestamp
            || ($toTime && $toTime < $timestamp)
        ) {
            return;
        }
        $sortOrder     = (int)$rule->getSortOrder();
        $simple_action = $rule->getSimpleAction();
        $pointAmount   = (int)$rule->getPointAmount();
        $moneyStep     = (float)$rule->getMoneyStep();
        $max_points    = (int)$rule->getMaxPoints();
        $actionStop    = (int)$rule->getStopRulesProcessing();
        //Spending Rule
        $discountStyle  = $rule->getDiscountStyle();
        $discountAmount = (float)$rule->getDiscountAmount();
        $perProduct     = (int)$rule->getUsesPerProduct();
        /** @var $helper Mage_Catalog_Helper_Product_Flat */
        $helper = $this->_factory->getHelper('catalog/product_flat');
        if ($helper->isEnabled() && $helper->isBuiltAllStores()) {
            /** @var $store Mage_Core_Model_Store */
            foreach ($this->_app->getStores(false) as $store) {
                if (in_array($store->getWebsiteId(), $websiteIds)) {
                    /** @var $selectByStore Varien_Db_Select */
                    $selectByStore = $rule->getProductFlatSelect($store->getId())
                        ->joinLeft(array('cg' => $this->getTable('customer/customer_group')),
                            $write->quoteInto('cg.customer_group_id IN (?)', $customerGroupIds),
                            array('cg.customer_group_id'))
                        ->reset(Varien_Db_Select::COLUMNS)
                        ->columns(array(
                            new Zend_Db_Expr($store->getWebsiteId()),
                            'cg.customer_group_id',
                            'p.entity_id',
                            new Zend_Db_Expr($rule->getId()),
                            new Zend_Db_Expr($fromTime),
                            new Zend_Db_Expr($toTime),
                            new Zend_Db_Expr("'" . $simple_action . "'"),
                            new Zend_Db_Expr($pointAmount),
                            new Zend_Db_Expr($actionStop),
                            new Zend_Db_Expr($sortOrder),
                            new Zend_Db_Expr("'" . $moneyStep . "'"),
                            new Zend_Db_Expr($max_points),
                        ));

                    if (count($productIds) > 0) {
                        $selectByStore->where('p.entity_id IN (?)', array_keys($productIds));
                    }

                    $selects = $write->selectsByRange('entity_id', $selectByStore, self::RANGE_PRODUCT_STEP);
                    foreach ($selects as $select) {
                        $write->query(
                            $write->insertFromSelect(
                                $select, $this->getTable('giantpointsrule/catalogrule_product'), array(
                                    'website_id',
                                    'customer_group_id',
                                    'product_id',
                                    'rule_id',
                                    'rule_type',
                                    'from_time',
                                    'to_time',
                                    'simple_action',
                                    'point_amount',
                                    'action_stop',
                                    'sort_order',
                                    'money_step',
                                    'max_points',
                                    'discount_style',
                                    'discount_amount',
                                    'uses_per_product',
                                ), Varien_Db_Adapter_Interface::INSERT_IGNORE
                            )
                        );
                    }
                }
            }
        } else {
            if (count($productIds) == 0) {
                Varien_Profiler::start('__MATCH_PRODUCTS__');
                $productIds = $rule->getMatchingProductIds();
                Varien_Profiler::stop('__MATCH_PRODUCTS__');
            }
            $rows = array();
            foreach ($productIds as $productId => $validationByWebsite) {
                foreach ($websiteIds as $websiteId) {
                    foreach ($customerGroupIds as $customerGroupId) {
                        if (empty($validationByWebsite[$websiteId])) {
                            continue;
                        }
                        $rows[] = array(
                            'rule_id'           => $rule->getId(),
                            'rule_type'         => $rule->getRuleType(),
                            'from_time'         => $fromTime,
                            'to_time'           => $toTime,
                            'website_id'        => $websiteId,
                            'customer_group_id' => $customerGroupId,
                            'product_id'        => $productId,
                            'simple_action'     => $simple_action,
                            'point_amount'      => $pointAmount,
                            'action_stop'       => $actionStop,
                            'sort_order'        => $sortOrder,
                            'money_step'        => $moneyStep,
                            'max_points'        => $max_points,
                            'discount_style'    => $discountStyle,
                            'discount_amount'   => $discountAmount,
                            'uses_per_product'  => $perProduct,
                        );

                        if (count($rows) == 1000) {
                            $write->insertMultiple($this->getTable('giantpointsrule/catalogrule_product'), $rows);
                            $rows = array();
                        }
                    }
                }
            }

            if (!empty($rows)) {
                $write->insertMultiple($this->getTable('giantpointsrule/catalogrule_product'), $rows);
            }
        }
    }

    /**
     * Update products which are matched for rule
     *
     * @param Magegiant_GiantPointsRule_Model_Catalogrule $rule
     *
     * @throws Exception
     * @return Mage_CatalogRule_Model_Resource_Rule
     */
    public function updateRuleProductData(Magegiant_GiantPointsRule_Model_Catalogrule $rule)
    {
        $ruleId = $rule->getId();
        $write  = $this->_getWriteAdapter();
        $write->beginTransaction();
        if ($rule->getProductsFilter()) {
            $this->cleanProductData($ruleId, $rule->getProductsFilter());
        } else {
            $this->cleanProductData($ruleId);
        }

        if (!$rule->getIsActive()) {
            $write->commit();

            return $this;
        }

        $websiteIds = $rule->getWebsiteIds();
        if (!is_array($websiteIds)) {
            $websiteIds = explode(',', $websiteIds);
        }
        if (empty($websiteIds)) {
            return $this;
        }

        try {
            $this->insertRuleData($rule, $websiteIds);
            $write->commit();
        } catch (Exception $e) {
            $write->rollback();
            throw $e;
        }

        return $this;
    }

    /**
     * Get all product ids matched for rule
     *
     * @param int $ruleId
     *
     * @return array
     */
    public function getRuleProductIds($ruleId)
    {
        $read   = $this->_getReadAdapter();
        $select = $read->select()->from($this->getTable('giantpointsrule/catalogrule_product'), 'product_id')
            ->where('rule_id=?', $ruleId);

        return $read->fetchCol($select);
    }

    public function getRulesFromProduct($date, $websiteId, $customerGroupId, $productId, $type)
    {
        $adapter = $this->_getReadAdapter();
        if (is_string($date)) {
            $date = strtotime($date);
        }
        $select = $adapter->select()
            ->from($this->getTable('giantpointsrule/catalogrule_product'))
            ->where('website_id = ?', $websiteId)
            ->where('customer_group_id = ?', $customerGroupId)
            ->where('product_id = ?', $productId)
            ->where('rule_type = ?', $type)
            ->where('from_time = 0 or from_time < ?', $date)
            ->where('to_time = 0 or to_time > ?', $date);

        return $adapter->fetchAll($select);
    }


    /**
     * Get ids of matched rules for specific product
     *
     * @param int $productId
     * @return array
     */
    public function getProductRuleIds($productId)
    {
        $read   = $this->_getReadAdapter();
        $select = $read->select()->from($this->getTable('giantpointsrule/catalogrule_product'), 'rule_id');
        $select->where('product_id = ?', $productId);

        return array_flip($read->fetchCol($select));
    }

    /**
     * Is product has been matched the rule
     *
     * @param int                        $ruleId
     * @param Mage_Catalog_Model_Product $product
     * @return bool
     */
    protected function _isProductMatchedRule($ruleId, $product)
    {
        $rules = $product->getMatchedRules();

        return isset($rules[$ruleId]);
    }
}
