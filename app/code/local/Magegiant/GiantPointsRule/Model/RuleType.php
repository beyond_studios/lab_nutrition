<?php


/**
 * GiantPointsRule Status Model
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsRule
 * @author      MageGiant Developer
 */
class Magegiant_GiantPointsRule_Model_RuleType extends Varien_Object
{
    const RULE_TYPE_EARNING  = 1;
    const RULE_TYPE_SPENDING = 2;

    /**
     * get model option as array
     *
     * @return array
     */
    static public function getOptionArray()
    {
        return array(
            self::RULE_TYPE_EARNING  => Mage::helper('giantpointsrule')->__('Earning'),
            self::RULE_TYPE_SPENDING => Mage::helper('giantpointsrule')->__('Spending')
        );
    }

    /**
     * get model option hash as array
     *
     * @return array
     */
    static public function getOptionHash()
    {
        $options = array();
        foreach (self::getOptionArray() as $value => $label) {
            $options[] = array(
                'value' => $value,
                'label' => $label
            );
        }

        return $options;
    }

    public function toOptionArray()
    {
        return self::getOptionHash();
    }

}