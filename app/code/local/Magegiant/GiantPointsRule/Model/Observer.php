<?php
/**
 * MageGiant
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageGiant.com license that is
 * available through the world-wide-web at this URL:
 * http://magegiant.com/license-agreement/
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsRule
 * @copyright   Copyright (c) 2014 MageGiant (http://magegiant.com/)
 * @license     http://magegiant.com/license-agreement/
 */

/**
 * GiantPointsRule Observer Model
 *
 * @category    MageGiant
 * @package     MageGiant_GiantPointsRule
 * @author      MageGiant Developer
 */
class Magegiant_GiantPointsRule_Model_Observer
{
    protected $_product;
    protected $_totalEarning = 0;

    protected $_flag = array();

    /**
     *
     * @param string $key
     * @return boolean
     */
    public function getFlag($key)
    {
        if (isset($this->_flag[$key])) {
            return (boolean)$this->_flag[$key];
        }

        return false;
    }

    /**
     *
     * @param string  $key
     * @param boolean $value
     * @return Magegiant_GiantPointsRule_Model_Observer
     */
    public function setFlag($key, $value = true)
    {
        $this->_flag[$key] = (boolean)$value;

        return $this;
    }

    public function checkoutCartCouponPost($observer)
    {
        if (!Mage::helper('giantpoints/config')->isEnabled() || !Mage::helper('giantpointsrule/config')->isEnabled())
            return $this;
        $action     = $observer->getEvent()->getControllerAction();
        $couponCode = (string)$action->getRequest()->getParam('coupon_code');
        if (!$couponCode)
            return $this;

        $couponModel = Mage::getModel('giantpointsrule/salesrule_coupon')->loadByCode($couponCode);
        if (!$couponModel || !$couponModel->getId()) {
            return $this;
        }
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $rule  = Mage::getModel('giantpointsrule/salesrule')->load($couponModel->getRuleId());
        if (!$rule || !$rule->getId() || !$rule->checkRule($quote))
            return $this;
        $this->_processSalesRuleCoupon($action, $rule, $couponCode);
        $action->setFlag('', Mage_Core_Controller_Varien_Action::FLAG_NO_DISPATCH, true);
        $action->getResponse()->setRedirect(Mage::getUrl('checkout/cart'));
    }

    protected function _processSalesRuleCoupon($action, $rule, $couponCode)
    {
        $currentRuleIds = array();
        if ($rule->getRuleType() != Magegiant_GiantPointsRule_Model_RuleType::RULE_TYPE_SPENDING) {
            $key = 'salesrule_earning_ids';
        } else {
            $key = 'salesrule_spending_ids';
            if ($rule->getCouponType() == Magegiant_GiantPointsRule_Model_Salesrule::COUPON_TYPE_NO_COUPON || $rule->getSimpleAction() != 'fixed')
                return;
            $session = $this->getCheckoutSession();
            $session->setData('is_used_point', true);
            $rewardCheckedRules = $session->getRewardCheckedRules();
            if (!is_array($rewardCheckedRules)) $rewardCheckedRules = array();
            if ($ruleId = $rule->getId()) {
                $rewardCheckedRules[$ruleId] = array(
                    'rule_id'      => $ruleId,
                    'point_amount' => null,
                );
                $session->setRewardCheckedRules($rewardCheckedRules);
            }
        }
        if (Mage::getSingleton('checkout/session')->getData($key) != '')
            $currentRuleIds = explode(',', Mage::getSingleton('checkout/session')->getData($key));
        if (!Mage::getSingleton('checkout/session')->getData('coupon_code'))
            Mage::getSingleton('checkout/session')->setData('coupon_code', $couponCode);
        if ($action->getRequest()->getParam('remove') == 1) {
            if (Mage::getSingleton('checkout/session')->getData('coupon_code'))
                Mage::getSingleton('checkout/session')->setData('coupon_code', '');
            if (in_array($rule->getId(), $currentRuleIds)) {
                $this->_clearRuleData($rule->getId());
                Mage::getSingleton('checkout/session')->getMessages(true);
                Mage::getSingleton('checkout/session')->addSuccess(Mage::helper('giantpointsrule')->__('Coupon code was canceled.'));
            }
        } else {
            if (!in_array($rule->getId(), $currentRuleIds))
                $currentRuleIds[] = $rule->getId();
            Mage::getSingleton('checkout/session')->setData($key, implode(',', $currentRuleIds));
            Mage::getSingleton('checkout/session')->getMessages(true);
            Mage::getSingleton('checkout/session')->addSuccess(Mage::helper('giantpointsrule')->__('Coupon code "%s" was applied.', $couponCode));
        }
    }

    /**
     *
     */
    protected function _clearRuleData($ruleId = null)
    {
        $session = $this->getCheckoutSession();
        $session->setData('salesrule_earning_ids', '');
        $session->setData('salesrule_spending_ids', '');
        $session->setData('coupon_code', '');
        if ($ruleId) {
            $rewardCheckedRules = $session->getRewardCheckedRules();
            if (!is_array($rewardCheckedRules)) $rewardCheckedRules = array();
            if (isset($rewardCheckedRules[$ruleId])) {
                unset($rewardCheckedRules[$ruleId]);
            }
            $session->setRewardCheckedRules($rewardCheckedRules);
        }

        return;
    }

    /**
     * @param $observer
     */
    public function addGiantCouponCode($observer)
    {
        $coupon = $observer->getContainer();
        if (Mage::getSingleton('checkout/session')->getData('coupon_code')) {
            $coupon->setCouponCode(Mage::getSingleton('checkout/session')->getData('coupon_code'));
        }

        return;
    }

    /**
     *
     * @return Magegiant_GiantPointsRule_Model_Observer
     */
    public function addPointsToCatalogProductView($observer)
    {
        if (!$this->isEnabled())
            return $this;
        $container            = $observer->getContainer();
        $currentPoints        = $container->getData('earning_points');
        $pointsForCatalogRule = $this->_getProduct($container->getProduct())->getTotalEarningByRule();
        $container->setData('earning_points', $currentPoints + $pointsForCatalogRule);

        return $this;
    }

    /**
     * @param $observer
     * @return $this
     */
    public function addPointsToCatalogProductList($observer)
    {
        if (!$this->isEnabled())
            return $this;
        $container            = $observer->getContainer();
        $product              = $this->_getProduct($container->getProduct());
        $key                  = 'points_' . $product->getId();
        $currentPoints        = $container->getData($key);
        $pointsForCatalogRule = $product->getTotalEarningByRule();
        $container->setData($key, $currentPoints + $pointsForCatalogRule);

        return $this;
    }

    /**
     * Add catalog earning points to checkout
     *
     * @param $observer
     * @return $this
     */
    public function addPointsToCheckoutCart($observer)
    {
        if (!$this->isEnabled())
            return $this;
        $quote          = Mage::helper('giantpoints/calculation_earning')->getQuote();
        $container      = $observer->getContainer();
        $current_amount = $container->getData('point_amount') ? $container->getData('point_amount') : 0;
        $earningInfo    = $container->getData('info') ? $container->getData('info') : array();
        $additionInfo   = array();
        /*Catalogrule Earning*/
        $earnByCatalogRule = $this->_getCatalogRuleEarn($quote);
        if (is_array($earnByCatalogRule) && count($earnByCatalogRule)) {
            foreach ($earnByCatalogRule as $rule) {
                $additionInfo[] = new Varien_Object(array(
                    'point_amount' => $rule['point_amount'],
                    'name'         => $rule['name'],
                    'description'  => $rule['description'],
                ));
                $current_amount += $rule['point_amount'];
            }

        }

        /*Salesrule Earning*/
        $salesRules = Mage::helper('giantpointsrule/calculation_earning')->getAllRewardSalesRule($quote);
        //Salesrule is coupon
        if ($salesRuleIds = Mage::getSingleton('checkout/session')->getData('salesrule_earning_ids')) {
            $rules = Mage::getResourceModel('giantpointsrule/salesrule_collection')
                ->getRulesByIds($salesRuleIds);
            foreach ($rules as $rule) {
                $salesRules[] = $rule;
            }
        }
        $earnBySalesRule = $this->_getSalesRuleEarn($salesRules, $quote);
        if (is_array($earnBySalesRule) && count($earnBySalesRule)) {
            foreach ($earnBySalesRule as $rule) {
                $additionInfo[] = new Varien_Object(array(
                    'point_amount' => $rule['point_amount'],
                    'name'         => $rule['name'],
                    'description'  => $rule['description'],
                ));
                $current_amount += $rule['point_amount'];
            }
        }
        $earningInfo = array_merge($earningInfo, $additionInfo);
        $container->setData('point_amount', $current_amount);
        $container->setData('info', $earningInfo);

        return $this;
    }

    /**
     * get current product
     *
     * @param $product
     * @return mixed
     */
    protected function _getProduct($product)
    {
        if ($product) {
            if ($product instanceof Magegiant_GiantPointsRule_Model_Catalog_Product) {
                return $product;
            } else {
                return Mage::getModel('giantpointsrule/catalog_product')->wrap($product);

            }
        } else {
            return Mage::getModel('giantpointsrule/catalog_product');
        }

    }

    /**
     * @param $observer
     * @return $this
     */
    public function quoteAddressItemEarningPoint($observer)
    {
        if (!$this->isEnabled()) {
            return $this;
        }
        $item     = $observer->getItem();
        $product  = Mage::getModel('giantpointsrule/catalog_product')->wrap($item->getProduct());
        $itemEarn = $product->getTotalEarningByRule();
        $item->setCatalogRuleEarn($itemEarn);
        $item->setGiantpointsEarn($item->getGiantpointsEarn() + $itemEarn);
        $catalogRules = $product->getAvailableEarningRules();
        if (count($catalogRules) == 1) {
            $rule = $catalogRules[0]->getRule();
            $item->setCatalogRuleName($rule->getName());
            $item->setCatalogRuleDescription($rule->getDescription());
        } else {
            $cnt = 0;
            foreach ($catalogRules as $rule) {
                $cnt++;
                if ($cnt != count($catalogRules)) {
                    $item->setCatalogRuleName($item->getCatalogRuleName() . $rule->getName() . ',');
                    $item->setCatalogRuleDescription($item->getCatalogRuleDescription() . $rule->getDescription() . ',');
                } else {
                    $item->setCatalogRuleName($item->getCatalogRuleName() . $rule->getName());
                    $item->setCatalogRuleDescription($item->getCatalogRuleDescription() . $rule->getDescription());
                }
            }
        }

        return $this;
    }

    /**
     * Update Catalogrule Earning to quote
     *
     * @param $observer
     * @return $this
     */
    public function collectEarningTotalPointsAfter($observer)
    {
        if (!$this->isEnabled()) {
            return $this;
        }
        if (!Mage::getSingleton('customer/session')->isLoggedIn())
            return $this;
        $quote         = $observer->getQuote();
        $address       = $this->getAddress($quote);
        $currentEarned = $address->getGiantpointsEarn();
        /*Catalog Earning Rule*/
        $catalogRuleEarning = $this->_getCatalogRuleEarn($quote);
        foreach ($catalogRuleEarning as $rule) {
            $currentEarned += $rule['point_amount'];
        }
        /*Set GiantpointsEarn*/
        $address->setGiantpointsEarn($currentEarned);

        return $this;
    }

    public function calculationEarningTotalPoints($observer)
    {
        $event = $observer->getEvent();
        if (!$event || !$this->isEnabled()) {
            return $this;
        }
        if (!Mage::getSingleton('customer/session')->isLoggedIn())
            return $this;
        $quote         = $event->getQuote();
        $container     = $event->getContainer();
        $address       = $this->getAddress($quote);
        $currentEarned = $container->getTotalEarnPoints();
        /*Shopping Cart Earning Rule*/
        $salesRules = Mage::helper('giantpointsrule/calculation_earning')->getAllRewardSalesRule($quote);
        //Is coupon
        if ($salesRuleIds = Mage::getSingleton('checkout/session')->getData('salesrule_earning_ids')) {
            $rules = Mage::getResourceModel('giantpointsrule/salesrule_collection')
                ->getRulesByIds($salesRuleIds);
            foreach ($rules as $rule) {
                $salesRules[] = $rule;
            }
            $address->setSalesruleEarningIds($salesRuleIds);
        }
        $salesruleEarning = $this->_getSalesRuleEarn($salesRules, $quote);
        foreach ($salesruleEarning as $rule) {
            $currentEarned += $rule['point_amount'];
        }
        /*Set GiantpointsEarn*/
        $container->setTotalEarnPoints($currentEarned);

        return $this;
    }

    /**
     * @param $address
     * @return $this|int
     */
    protected function _getCatalogRuleEarn($quote)
    {
        $earningPoints = array();
        $address       = $this->getAddress($quote);
        $items         = $address->getAllItems();
        if (!count($items)) {
            return $earningPoints;
        }
        foreach ($items as $item) {
            if ($item->getParentItemId()) {
                continue;
            }
            if ($item->getHasChildren() && $item->isChildrenCalculated()) {
                foreach ($item->getChildren() as $child) {
                    if($child->getCatalogRuleEarn()>0){
                        $earningPoints[] = array(
                            'point_amount' => $child->getCatalogRuleEarn(),
                            'name'         => $child->getName(),
                            'description'  => $child->getCatalogRuleDescription(),
                        );
                    }
                }
            } else {
                if($item->getCatalogRuleEarn()>0){
                    $earningPoints[] = array(
                        'point_amount' => $item->getCatalogRuleEarn(),
                        'name'         => $item->getName(),
                        'description'  => $item->getCatalogRuleDescription(),
                    );
                }
            }

        }

        return $earningPoints;
    }

    private function getAddress($quote)
    {
        if ($quote->isVirtual()) {
            $address = $quote->getBillingAddress();
        } else {
            $address = $quote->getShippingAddress();
        }

        return $address;
    }

    /**
     * get earning points by sales rule
     *
     * @param $address
     * @return $this|int
     */
    protected function _getSalesRuleEarn($rules, $quote)
    {
        $earningPoints = array();
        foreach ($rules as $rule) {
            $points = Mage::helper('giantpointsrule/calculation_earning')->calcPointsForSalesRule($rule, $quote);
            if ($points) {
                $earningPoints[] = array(
                    'point_amount' => $points,
                    'name'         => $rule->getName(),
                    'description'  => $rule->getDescription()
                );
            }
        }

        return $earningPoints;
    }

    /**
     * @param $observer
     * @return $this
     */
    public function salesOrderAfterPlace($observer)
    {
        if (!$this->isEnabled()) {
            return $this;
        }
        $order = $observer->getEvent()->getOrder();

        if (!$order) {
            return $this;
        }
        /*Clear Session Data*/
        $this->_clearRuleData();
        // Lookup rule ids
        $earningRuleIds  = explode(',', $order->getSalesruleEarningIds());
        $spendingRuleIds = explode(',', $order->getSalesruleSpendingIds());
        $ruleIds         = array_merge($earningRuleIds, $spendingRuleIds);
        $ruleIds         = array_unique($ruleIds);

        $ruleCustomer = null;
        $customerId   = $order->getCustomerId();

        // use each rule (and apply to customer, if applicable)
        foreach ($ruleIds as $ruleId) {
            if (!$ruleId) {
                continue;
            }
            $rule = Mage::getModel('giantpointsrule/salesrule');
            $rule->load($ruleId);
            if ($rule->getId()) {
                $rule->setTimesUsed($rule->getTimesUsed() + 1);
                $rule->save();

                if ($customerId) {
                    $ruleCustomer = Mage::getModel('giantpointsrule/salesrule_customer');
                    $ruleCustomer->loadByCustomerRule($customerId, $ruleId);
                    if ($ruleCustomer->getId()) {
                        $ruleCustomer->setTimesUsed($ruleCustomer->getTimesUsed() + 1);
                    } else {
                        $ruleCustomer
                            ->setCustomerId($customerId)
                            ->setRuleId($ruleId)
                            ->setTimesUsed(1);
                    }
                    $ruleCustomer->save();
                }
            }
            $coupon = Mage::getModel('giantpointsrule/salesrule_coupon');
            /** @var Mage_SalesRule_Model_Coupon */
            $coupon->load($rule->getCouponCode(), 'code');
            if ($coupon->getId()) {
                $coupon->setTimesUsed($coupon->getTimesUsed() + 1);
                $coupon->save();
                if ($customerId) {
                    $couponUsage = Mage::getResourceModel('giantpointsrule/salesrule_coupon_usage');
                    $couponUsage->updateCustomerCouponTimesUsed($customerId, $coupon->getId());
                }
            }
        }
    }

    /**
     * Process spending catalogrule points when product add to cart after
     *
     * @param $observer
     */
    public function checkoutCartAddProductAfter($observer)
    {
        if (!$this->isEnabled()) {
            return $this;
        }
        $quoteItem = $observer['quote_item'];
        $product   = $observer['product'];
        if ($quoteItem->getParentItem()) {
            $quoteItem = $quoteItem->getParentItem();
        }
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        if (!$quoteItem->getId()) {
            try {
                if (!$quote->getId()) {
                    $quote->save();
                }
                $quoteItem->setQuoteId($quote->getId())->save();
            } catch (Exception $e) {
                Mage::logException($e);
            }
        }
        if (!$quoteItem->getId()) {
            return $this;
        }
        if ($this->getFlag('added_for_item_' . $quoteItem->getId())) {
            return $this;
        }
        if (!$quote->getCustomerId())
            return $this;
        $this->setFlag('added_for_item_' . $quoteItem->getId());
        $this->_updatePointsToQuote($quoteItem, $product, $quote);

    }

    /**
     *
     */
    public function checkoutCartUpdateItemsAfter($observer)
    {
        if (!$this->isEnabled()) {
            return $this;
        }
        $event = $observer->getEvent();
        if (!$event) {
            return $this;
        }
        $cart      = $event->getCart();
        $quote     = $cart->getQuote();
        $cartItems = $quote->getAllVisibleItems();
        foreach ($cartItems as $item) {
            $product = $item->getProduct();
            if ($item->getParentItem()) {
                $item = $item->getParentItem();
            }
            $this->_updatePointsToQuote($item, $product, $quote, false);
        }

        return $this;

    }

    protected function _updatePointsToQuote($quoteItem, $product, $quote, $isAdd = true)
    {

        $session      = Mage::getSingleton('checkout/session');
        $catalogRules = $session->getCatalogRules();
        $request      = Mage::app()->getRequest();
        $ruleId       = null;
        if ($isAdd) {
            $ruleId    = $request->getParam('reward_product_rule');
            $pointUsed = $request->getParam('reward_product_point');
        } else {
            if (is_array($catalogRules) && array_key_exists($quoteItem->getId(), $catalogRules)) {
                $ruleItem  = $catalogRules[$quoteItem->getId()];
                $ruleId    = $ruleItem->getRuleId();
                $pointUsed = (int)$ruleItem->getPointUsed() / $ruleItem->getItemQty();
            }
        }
        if (is_null($ruleId))
            return $this;
        $rule = Mage::getModel('giantpointsrule/catalogrule')->load($ruleId);
        if (!$rule || !$rule->getId()) {
            return $this;
        }
        $qty = $quoteItem->getQty() > 0 ? $quoteItem->getQty() : 1;
        if ($rule->getUsesPerProduct() && $qty > $rule->getUsesPerProduct()) {
            $qty = $rule->getUsesPerProduct();
        }
        $pointUsed *= $qty;
        $max_point = Mage::helper('giantpointsrule/calculation_spending')->getCustomerPoint($quoteItem->getId());
        if ($pointUsed > $max_point)
            $pointUsed = $max_point;
        $min_point = Mage::helper('giantpoints/config')->getMinimumRedeemPoint($quote->getStoreId());
        if ($min_point && $pointUsed < $min_point) { //if not enough minimum point to redeem
            return $this;
        }
        $base_discount_amount = Mage::helper('giantpointsrule/calculation_spending')->getCatalogRuleDiscountByPoints($rule, $product, $pointUsed);
        $discount_amount      = Mage::app()->getStore($quoteItem->getStoreId())->convertPrice($base_discount_amount);
        if (!is_array($catalogRules)) {
            $catalogRules = array();
        }
        if (!$product->isGrouped()) {
            if ($ruleId) {
                $catalogRules[$quoteItem->getId()] = new Varien_Object(
                    array(
                        'item_id'              => $quoteItem->getId(),
                        'item_qty'             => $qty,
                        'rule_id'              => $ruleId,
                        'name'                 => $rule->getName(),
                        'point_used'           => $pointUsed,
                        'base_discount_amount' => $base_discount_amount,
                        'discount_amount'      => $discount_amount,
                        'type'                 => 'catalog_spend'
                    )
                );

            } elseif (isset($catalogRules[$quoteItem->getId()])) {
                unset($catalogRules[$quoteItem->getId()]);
            }

        }
        $session->setCatalogRules($catalogRules);
        if (!empty($catalogRules)) {
            $session->setData('is_used_point', true);
        }

        return $this;
    }

    /**
     *
     * @param $observer
     */
    public function quoteAddressDiscountItemAfter($observer)
    {
        if (!$this->isEnabled()) {
            return $this;
        }
        $event = $observer->getEvent();
        if (!$event) {
            return $this;
        }
        $item = $event->getItem();
        if (!$item->getId())
            return $this;
        $catalogRules = $this->getCheckoutSession()->getCatalogRules();
        if (is_array($catalogRules) && array_key_exists($item->getId(), $catalogRules)) {
            $rule = $catalogRules[$item->getId()];
            $item->setGiantpointsSpent($item->getGiantpointsSpent() + $rule->getPointUsed());
            $item->setCatalogRuleSpent($rule->getPointUsed());
            //set discount by catalog rule
            $item->setBaseDiscountAmount($item->getBaseDiscountAmount() + $rule->getBaseDiscountAmount());
            $item->setDiscountAmount($item->getDiscountAmount() + $rule->getDiscountAmount());
            $item->setCatalogRuleBaseDiscount($rule->getBaseDiscountAmount());
            $item->setCatalogRuleDiscount($rule->getDiscountAmount());
        }

        return $this;
    }

    public function quoteCollectSpendingAfter($observer)
    {
        if (!$this->isEnabled()) {
            return $this;
        }
        $event = $observer->getEvent();
        if (!$event) {
            return $this;
        }
        $address      = $event->getAddress();
        $quote        = $address->getQuote();
        $items        = $address->getAllItems();
        $helperConfig = Mage::helper('giantpoints/config');
        if (!count($items)) {
            return $this;
        }
        $salesRuleIds = Mage::getSingleton('checkout/session')->getData('salesrule_spending_ids');
        $address->setSalesruleSpendingIds($salesRuleIds);
        $item_count              = 0;
        $catalogRuleSpent        = 0;
        $catalogRuleBaseDiscount = 0;
        foreach ($items as $item) {
            $item_count++;
            if ($item->getParentItemId()) {
                continue;
            }
            if ($item->getHasChildren() && $item->isChildrenCalculated()) {
                foreach ($item->getChildren() as $child) {
                    $catalogRuleSpent += $child->getCatalogRuleSpent();
                    $catalogRuleBaseDiscount += $child->getCatalogRuleBaseDiscount();
                }
            } else {
                $catalogRuleSpent += $item->getCatalogRuleSpent();
                $catalogRuleBaseDiscount += $item->getCatalogRuleBaseDiscount();
            }

        }
        $catalogRuleDiscount = Mage::app()->getStore($quote->getStoreId())->convertPrice($catalogRuleBaseDiscount);
        if ($catalogRuleSpent) {
            $address->setGiantpointsSpent($address->getGiantpointsSpent() + $catalogRuleSpent);
            $address->setBaseDiscountAmount($address->getBaseDiscountAmount() - $catalogRuleBaseDiscount);
            $address->setDiscountAmount($address->getDiscountAmount() - $catalogRuleDiscount);
            $address->setBaseGrandTotal($address->getBaseGrandTotal() - $catalogRuleBaseDiscount);
            $address->setGrandTotal($address->getGrandTotal() - $catalogRuleDiscount);
            if ($address->getDiscountDescription() && strpos($address->getDiscountDescription(), $helperConfig->getDiscountLabel($quote->getStoreId())) === false) {
                $address->setDiscountDescription($address->getDiscountDescription() . ', ' . $helperConfig->getDiscountLabel($quote->getStoreId()));
            } else {
                $address->setDiscountDescription($helperConfig->getDiscountLabel());
            }
        }

        return $this;


    }

    public function conversionSpendingRuleMaxPoints($observer)
    {
        $event = $observer->getEvent();
        if (!$event || !$this->isEnabled()) {
            return $this;
        }
        $rule      = $observer['rule'];
        $quote     = $observer['quote'];
        $container = $observer['container'];

        $helperCore = Mage::helper('giantpoints/calculation_spending');
        $helperRule = Mage::helper('giantpointsrule/calculation_spending');
        $baseTotal  = $helperCore->getQuoteBaseTotal($quote) - $helperCore->getCheckedRuleDiscount();
        foreach ($quote->getAllItems() as $item) {
            if ($item->getParentItemId()) {
                continue;
            }
            if (!$rule->getActions()->validate($item)) {
                $baseTotal -= $item->getBaseRowTotal() - $item->getQty() * $helperRule->getItemDiscount($item);
            }
        }
        if ($rule->getDiscountStyle() == 'cart_fixed') {
            if ($rule->getDiscountAmount()) {
                $maxPoints = ceil($baseTotal / $rule->getDiscountAmount()) * $rule->getPointAmount();
            }
        } else {
            if ($rule->getDiscountAmount()) {
                $percent   = 100;
                $maxPoints = ceil($percent / $rule->getDiscountAmount()) * $rule->getPointAmount();
            }
        }
        $maxPerOrder = $rule->getMaxPoints() ? $rule->getMaxPoints() : $helperCore->getMaxPointsPerOrder($quote->getStoreId());
        if ($maxPerOrder) {
            $maxPerOrder -= $helperCore->getPointItemSpent();
            $maxPerOrder -= $helperCore->getCheckedRulePoint();
            if ($maxPerOrder > 0 && $maxPoints) {
                $maxPoints = min($maxPoints, $maxPerOrder);
                $maxPoints = floor($maxPoints / $rule->getPointAmount()) * $rule->getPointAmount();
            }
        }
        $container->setRuleMaxPoints($maxPoints);

        return $this;
    }

    private function getCheckoutSession()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * Check module is enabled
     *
     * @param $store
     * @return mixed
     */
    private function isEnabled($store = null)
    {
        return (Mage::helper('giantpoints/config')->isEnabled($store) && Mage::helper('giantpointsrule/config')->isEnabled($store));
    }

    /**
     * Append Giant Spending Rules to checkout cart
     *
     * @param $observer
     * @return $this
     */
    public function blockSpendGetRules($observer)
    {
        if (!$this->isEnabled()) {
            return $this;
        }
        $event = $observer->getEvent();
        if (!$event) {
            return $this;
        }
        $container      = $event->getContainer();
        $currentRules   = $container->getSpendingRules();
        $availableRules = Mage::helper('giantpointsrule/calculation_spending')->getQuoteSpendingRules();
        $container->setSpendingRules(array_merge($currentRules, $availableRules));

        return $this;
    }

    /**
     * @param $observer
     * @return $this
     */
    public function getQuoteSpendingSaleRule($observer)
    {
        $event = $observer->getEvent();
        if (!$event) {
            return $this;
        }
        $container = $event->getContainer();
        $ruleId    = $event->getRuleId();
        $container->setQuoteRuleModel(
            Mage::getModel('giantpointsrule/salesrule')->load($ruleId)
        );

        return $this;
    }

    public function calcSpendingQuoteRuleDiscount($observer)
    {
        if (!$this->isEnabled()) {
            return $this;
        }
        $event = $observer->getEvent();
        if (!$event) {
            return $this;
        }
        $rule      = $observer->getRule();
        $quote     = $observer->getQuote();
        $container = $observer->getContainer();
        $points    = $container->getPoints();
        $discount  = $container->getQuoteRuleDiscount() + Mage::helper('giantpointsrule/calculation_spending')
                ->getSalesRuleDiscountByPoints($rule, $quote, $points);
        $container->setPoints($points);
        $container->setQuoteRuleDiscount($discount);

        return $this;

    }

    public function updaterGetMapBefore($observer)
    {
        if (!$this->isEnabled()) {
            return $this;
        }
        $event = $observer->getEvent();
        if (!$event) {
            return $this;
        }
        $container  = $event->getContainer();
        $currentMap = $container->getMap();
        $appendMap  = array(
            'checkboxRule'             => array(
                'review_cart' => 'checkout.cart.totals',
            ),
            'checkboxRuleOnepage'      => array(
                'review_payment' => 'root',
            ),
            'checkboxRuleGiantOnestep' => array(
                'review_payment' => 'onestepcheckout.onestep.form.payment.methods',
                'review_cart'    => 'onestepcheckout.onestep.form.review.cart',
            ),

        );
        $container->setMap(array_merge($currentMap, $appendMap));

        return $this;
    }
}