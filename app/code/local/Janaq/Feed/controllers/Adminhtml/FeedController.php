<?php
class Janaq_Feed_Adminhtml_FeedController extends Mage_Adminhtml_Controller_Action
{
    
    public function indexAction(){
                
        $this->_redirect('*/*/');
    }

    public function generateAction(){

        

        try {
            Mage::getModel('janaq_feed/observer')->generateProductFeed();

            $this->_getSession()->addSuccess(
                            $this->__('Se ha generado correctamente.'));

        } catch (Exception $e) {
            $this->_getSession()->addError($this->__('Error: ' . $e->toString()));
        }
        
        Mage::app()->getResponse()->setRedirect(Mage::helper('adminhtml')->getUrl('adminhtml/system_config/edit/section/jnqadmin_config', array('_secure' => true)));
    }

    

    protected function _isAllowed()
    {
        return true;
    }
}
