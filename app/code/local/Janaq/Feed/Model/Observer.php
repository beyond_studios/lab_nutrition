<?php


class Janaq_Feed_Model_Observer {


  protected $_list = array();
  protected $page = 1;
  protected $count = 0;
  protected $limit = 10;

  public function generateProductFeed() {


    $getData= array(930,931,932);

    $io = new Varien_Io_File();
    $path = Mage::getBaseDir('media') . DS . 'feed' . DS;
    $name = 'janaq_feed_products';
    $file = $path . DS . $name . '.csv';
    $io->setAllowCreateFolders(true);
    $io->open(array('path' => $path));
    $io->streamOpen($file, 'w+');
    $io->streamLock(true);

    $head = array(
        'SKU',
        'Nombre',
        'Descripción',
        'Precio',
        'Url Producto',
        'Url Imagen',
    );
    $io->streamWriteCsv($head);
    do {
            $offset     =   ($this->page - 1) * $this->limit;
            $productCollection = Mage::getModel('catalog/product')->getCollection()
                ->addFieldToFilter('status', array('in'=> 1))
                ->addFieldToFilter('visibility', array('in'=> array(2,3,4)));

            $productCollection->getSelect()->limit($this->limit,$offset);


            foreach ($productCollection as $product) {
                $_product = Mage::getModel('catalog/product')->load($product->getId());
                $media = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
                $_product_data = array(
                    'Sku' => $_product->getSku(),
                    'name'  => $_product->getName(),
                    'description' => $_product->getDescription(),
                    'price' => $_product->getFinalPrice(),
                    'product_url'  => $_product->getProductUrl(),
                    'product_image'  => $media.'catalog/product'. $_product->getImage(),
                    
                );
                $io->streamWriteCsv($_product_data);

            }

            $this->page++;
            $results = $productCollection->load();

    } while ($results->count());
    
          
    //$productCollection->load();

    
  }

  public function saveCsv(){

  }
    
    public function getCommentText()
    {
        
        return Mage::getBaseUrl( Mage_Core_Model_Store::URL_TYPE_WEB, true ) . 'media/feed/janaq_feed_products.csv';
    }

  
}
