<?php

class Janaq_Feed_Block_Feed extends Mage_Adminhtml_Block_System_Config_Form_Field
{

    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $this->setElement($element);
        //$url = $this->getUrl('janaq_feed/feed/generate'); //
        //$url = $this->getUrl('janaq_feed/adminhtml_controller/generate');
        $url =Mage::helper('adminhtml')->getUrl('adminhtml/feed/generate', array('_secure' => true));

        $html = $this->getLayout()->createBlock('adminhtml/widget_button')
                    ->setType('button')
                    ->setClass('scalable')
                    ->setLabel('Run Now !')
                    ->setOnClick("setLocation('$url')")
                    ->toHtml();

        return $html;
    }
}