<?php
$installer = $this;
$installer->startSetup();

$installer->addAttribute("order", "ruc_pim", array("type"=>"varchar"));
$installer->addAttribute("quote", "ruc_pim", array("type"=>"varchar"));

$installer->addAttribute("order", "razon_social_pim", array("type"=>"varchar"));
$installer->addAttribute("quote", "razon_social_pim", array("type"=>"varchar"));


$installer->endSetup();