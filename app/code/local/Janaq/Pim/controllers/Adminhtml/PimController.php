<?php
class Janaq_Pim_Adminhtml_PimController extends Mage_Adminhtml_Controller_Action
{
  public function indexAction()
  {
    $this->_title($this->__('PIM'))->_title($this->__('Integrador List'));

      $this->loadLayout();
      $this->_setActiveMenu('janaq_pim/pim');
      $this->_addBreadcrumb(Mage::helper('adminhtml')->__('PIM'), Mage::helper('adminhtml')->__('Integrador List'));
      $this->_addContent($this->getLayout()->createBlock('janaq_pim/adminhtml_integradoradmin'));
      $this->renderLayout();
  }
  public function gridAction()
  {
      $this->loadLayout();
      $this->getResponse()->setBody($this->getLayout()->createBlock('janaq_pim/adminhtml_integradoradmin_grid')->toHtml());
  }

  public function editAction()
  {


     $id  = $this->getRequest()->getParam('id');
     $model = Mage::getModel('janaq_pim/integrador');

     if ($id) {
         $model->load($id);

         if (!$model->getId()) {
             Mage::getSingleton('adminhtml/session')->addError(Mage::helper('janaq_pim')->__('No existe'));
             $this->_redirect('*/*');

             return;
         }
     }
     if (!$model->getId()){

     }

     Mage::register('pim', $model);

     $this->loadLayout()
     ->_setActiveMenu('janaq_pim/integradoradmin')
         ->_addBreadcrumb($id ? $this->__('Integrador') : Mage::helper('adminhtml')
             ->__('Detalles'), $id ? Mage::helper('adminhtml')
             ->__('Detalles') : Mage::helper('adminhtml')->__('Detalles'))
         ->_addContent($this->getLayout()->createBlock('janaq_pim/adminhtml_pim_edit')->setData('action', $this->getUrl('*/*/save')))
         ->renderLayout();
  }

  public function exportInchooCsvAction()
  {
      $fileName = 'order_pim.csv';
      $grid = $this->getLayout()->createBlock('janaq_pim/adminhtml_integradoradmin_grid');
      $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
  }

  public function exportInchooExcelAction()
  {
      $fileName = 'order_pim.xml';
      $grid = $this->getLayout()->createBlock('janaq_pim/adminhtml_integradoradmin_grid');
      $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
  }

  public function massStatusAction() {
      $integrador_ids = $this->getRequest()->getParam('integrador');
      if (!is_array($integrador_ids)) {
          Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
      } else {
          try {
              $i = 0;
              foreach ($integrador_ids as $id) {
                  $model = Mage::getModel('janaq_pim/integrador')->load($id);
                  if ($model->getStatus() !='completado') {
                      $model->setStatus($this->getRequest()->getParam('status'))
                          ->setIsMassupdate(true)
                          ->save();
                          $i++;
                  }
              }
              $this->_getSession()->addSuccess(
                      $this->__('Total of %d record(s) were successfully updated', $i)
              );
          } catch (Exception $e) {
              $this->_getSession()->addError($e->getMessage());
          }
      }
      $this->_redirect('*/*/index');
  }
  public function executecronAction(){

    $cron = Mage::getModel('janaq_pim/cron')->run();
    $this->_getSession()->addSuccess(
                        $this->__('Se a ejecutado el envio de items.')
                );
    $this->_redirect('*/*/index');
  }
  public function sendpimAction() {
      $integrador_ids = $this->getRequest()->getParam('order_ids');
      $this->_redirect('*/sales_order/index');
      if (!is_array($integrador_ids)) {
          Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
      } else {
          try {
              $i = 0;
              foreach ($integrador_ids as $id) {
                  $order = Mage::getModel('sales/order')->load($id);
                  if ($order) {
                      $params['order'] = $order;
                      Mage::dispatchEvent('janaq_pim_order_save', $params);
                      $i++;
                  }
              }
              $this->_getSession()->addSuccess(
                      $this->__('Se agregaron %d registros al integrador PIM.', $i)
              );
          } catch (Exception $e) {
              $this->_getSession()->addError($e->getMessage());
          }
      }


  }

  public function importstockOldAction(){
    $helper = Mage::Helper('janaq_pim');
      if ($helper->getStatus()) {
          $pim = Mage::getModel('janaq_pim/service');
          $almacen_id = $helper->getStoreDefault();
          $products_pim = $pim->getProductByStore($almacen_id);
          //Mage::log($products_pim);
          $count_update = 0;
          $error_sku = '';
          foreach ($products_pim as $item) {
              $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $item['sku']);
              if ($product && $product->getId()) {
                $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product->getId());
                if ($stockItem->getId() > 0 and $stockItem->getManageStock()) {
                  $qty = $item['stock'];
                  $stockItem->setQty($qty);
                  $stockItem->setIsInStock((int)($qty > 0));
                  $stockItem->save();
                  $count_update++;
                  //Mage::log('save product: ' .$item['sku'] . ' stock: ' . $item['stock']);
                }
              }else{
                //Mage::log('error '.$item['sku']);
                $error_sku .= $item['sku'] . '; ';
              }
          }
          $this->_getSession()->addSuccess($this->__('Se actualizaron %d productos.',$count_update));
          if ($error_sku != '') {
            $this->_getSession()->addError($this->__('Los siguientes códigos no existen en Magento: ' . $error_sku));
          }
          

      }else{
          $this->_getSession()->addError(
                      $this->__('Activar el modulo PIM de las configuraciones.')
              );
      }

      $this->_redirect('*/*/index');
  }

  public function importstockAction(){
    $helper = Mage::Helper('janaq_pim');
      if ($helper->getStatus()) {
          $pim = Mage::getModel('janaq_pim/service');
          $almacen_id = $helper->getStoreDefault();
          $products_pim = $pim->getProductByStore($almacen_id);
          //Mage::log($products_pim);
          $count_update = 0;
          $error_sku = '';
          $collection_simple = $this->_getCollectionProductSimple();
          $page_number = 1;
          $pages = $collection_simple->getLastPageNumber();
          do {
              $collection_simple->setCurPage($page_number);
              foreach ($collection_simple as $product) {
                 //Mage::log($product->getTypeId() .' - '. $product->getSku());
                try {

                   $key = array_search($product->getSku(), array_column($products_pim, 'sku'));
                   if ($key) {
                     $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product->getId());
                    if ($stockItem->getId() > 0 && $stockItem->getManageStock()) {
                      $qty = $products_pim[$key]['stock'];
                      $stockItem->setQty($qty);
                      $stockItem->setIsInStock((int)($qty > 0));
                      $stockItem->save();
                      $count_update++;
                    }
                   }else{
                      //Mage::log('error '.$products_pim[$key]['sku']);
                      $error_sku .= $product->getSku() . '; ';
                   }
                  
                } catch (Exception $e) {
                    Mage::log($e->getMessage());
                }
                 
              }
              $page_number++;
              $collection_simple->clear();
              $collection_simple = $this->_getCollectionProductSimple();

          } while ($page_number <= $pages);

          $this->_getSession()->addSuccess($this->__('Se actualizaron %d productos.',$count_update));
          if ($error_sku != '') {
            $this->_getSession()->addError($this->__('Los siguientes códigos no existen en Odoo: ' . $error_sku));
          }
          

      }else{
          $this->_getSession()->addError(
                      $this->__('Activar el modulo PIM de las configuraciones.')
              );
      }

      $this->_redirect('*/*/index');
  }

  protected function _getCollectionProductSimple(){
      $max_page_default = 10;
      $collection = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToFilter('type_id', array('eq' => 'simple'))
            ->addAttributeToFilter('status', array('eq' => '1'))
            ->addAttributeToSelect('*')
            ->setPageSize($max_page_default);

      return $collection;
  }

  public function generateOrderAction(){

      $increment_id= $this->getRequest()->getParam('increment_id', false);
      //Mage::log($increment_id);
      $result = array();
      $model = Mage::getModel('janaq_pim/integrador');

      $model->load($increment_id,'order_id');
      if ($model->getId()) {
          //Mage::log($model->getStatus());
          if ($model->getStatus() == 'completado') {
              $result['error'] = false;
              $result['message'] = 'La orden ya ha sido enviado en PIM';
          }elseif ($model->getStatus() == 'pendiente') {
              $result['error'] = false;
              $result['message'] = 'La orden ya esta en espera de envió a PIM';
          }
      }else{
          //Mage::log("SE CREARA ESTE REGISTRO");
          $date = Mage::getModel('core/date')->date('Y-m-d H:i:s');
          $model->setId(null);
          $model->setOrderId($increment_id);
          $model->setTypeTask('Order');
          $model->setStatus('pendiente');
          $model->setCreatedAt($date);
          $model->setUpdatedAt($date);
          try {
              $model->save();
              $result['error'] = false;
              $result['message'] = 'La orden se ha agregado a la lista de envios para PIM';
          } catch (Exception $e) {
               $result['error'] = false;
              $result['message'] = $e->getMessage();
          }
      }
      //$result = array('error'=> false,'message'=>'exito order '. $increment_id);
      //Mage::Log($result);
      
      $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
  }

  protected function _isAllowed()
  {
      return Mage::getSingleton('admin/session')->isAllowed('janaq_pim/integradoradmin');
  }

}
