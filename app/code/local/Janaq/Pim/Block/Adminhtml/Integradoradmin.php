<?php

class Janaq_Pim_Block_Adminhtml_Integradoradmin extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'janaq_pim';
        $this->_controller = 'adminhtml_integradoradmin';
        $this->_headerText = Mage::helper('adminhtml')->__('PIM - Integrador');

    	parent::__construct();
    	$this->_removeButton('add');
    	$this->_addButton('execute_cron', array(
            'label'   => Mage::helper('catalog')->__('Envio manual de pendientes'),
            'onclick' => "setLocation('{$this->getUrl('*/pim/executecron')}')",
            'class'   => 'add'
        ));

        $this->_addButton('import_stock_store_default', array(
            'label'   => Mage::helper('catalog')->__('Importar Stock de Tienda Por Default'),
            'onclick' => "(function () { var r = confirm('".$this->__('Esta seguro que quiere actualizar el stock de todos los productos de la Tienda por Default.')."');if (r == true){setLocation('{$this->getUrl('*/pim/importstock')}');}
            

                            }());",
            'class'   => 'add',
        ));
    }
}
