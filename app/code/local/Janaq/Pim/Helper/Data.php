<?php
/**
 * @category   Janaq
 * @package    Janaq_Urbaner
 * @author     contacto@janaq.com

 */
class Janaq_Pim_Helper_Data extends Mage_Core_Helper_Abstract
{
    // AUTH SERVICES
    const STATUS = "janaq_pim/configuration/enable";
    const STATUS_CART = "janaq_pim/configuration/enable_stock_cart";
    const URL = "janaq_pim/configuration/url";
    const USER = "janaq_pim/configuration/user";
    const PASSWORD = "janaq_pim/configuration/password";
    const STORE_DEFAULT = "janaq_pim/configuration/store_default";
    const VALIDATE_PRODUCT = "janaq_pim/configuration/enable_validate_product_admin";
    const VALIDATE_PRODUCT_BUNDLE = "janaq_pim/configuration/enable_validate_product_bundle_admin";
    public function getStatus()
    {
        return Mage::getStoreConfig(self::STATUS, Mage::app()->getStore());
    }
    public function getStatusCart()
    {
        return Mage::getStoreConfig(self::STATUS_CART, Mage::app()->getStore());
    }
    public function getValidateProductAdmin()
    {
        return Mage::getStoreConfig(self::VALIDATE_PRODUCT, Mage::app()->getStore());
    }
    public function getValidateProductBundleAdmin()
    {
        return Mage::getStoreConfig(self::VALIDATE_PRODUCT_BUNDLE, Mage::app()->getStore());
    }
    public function getStoreDefault()
    {
        return Mage::getStoreConfig(self::STORE_DEFAULT, Mage::app()->getStore());
    }
    public function getUrl()
    {
        return Mage::getStoreConfig(self::URL, Mage::app()->getStore());
    }
    public function getUser()
    {
        return Mage::getStoreConfig(self::USER, Mage::app()->getStore());
    }
    public function getPassword()
    {
        return Mage::getStoreConfig(self::PASSWORD, Mage::app()->getStore());
    }

}
