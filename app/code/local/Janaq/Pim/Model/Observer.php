<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 15/12/2016
 * Time: 12:12 PM
 */

class Janaq_Pim_Model_Observer extends Mage_Payment_Model_Method_Abstract
{

    public function saveOrder($observer){

        $helper = Mage::Helper('janaq_pim');

        if ($helper->getStatus()) {
          $date = Mage::getModel('core/date')->date('Y-m-d H:i:s');

          $model = Mage::getSingleton('janaq_pim/integrador');
          $increment_id = $observer['order']->getIncrementId();
          $modelUpdate = Mage::getModel('janaq_pim/integrador');
          $modelUpdate->load($increment_id,'order_id');
          if ($increment_id && !$modelUpdate->getId()) {
              $model->setOrderId($increment_id);
              $model->setTypeTask('Order');
              $model->setStatus('pendiente');
              $model->setCreatedAt($date);
              $model->setUpdatedAt($date);
          }else{
              $model->setId($modelUpdate->getId());
              //$model->setStatus('pendiente');
              $model->setUpdateAt($date);
          }
          try {
              $model->save();
          } catch (Exception $e) {
              Mage::log('Error al guardar registro de PIM (Tareas): '. $e->getMessage());
          }

        }
        return $this;

    }

    public function addProductCart(Varien_Event_Observer $observer)
	  {
      $helper = Mage::Helper('janaq_pim');
      if ($helper->getStatus() && $helper->getStatusCart()) {
        $quoteItem = $observer->getEvent()->getQuoteItem();
        $product = $observer->getEvent()->getProduct();
        $sku = $product->getSku();
        $items = explode('-', $sku);
        //Mage::log('sku add : '.$sku);
        $store_default = $helper->getStoreDefault();
        $model = Mage::getModel('janaq_pim/service');
        $quote = Mage::getSingleton('checkout/session')->getQuote();
    		$cartItems = $quote->getAllVisibleItems();
        /*foreach ($cartItems as $item) {

          $productSku = $item->getSku();
          $params  = array('sku' => $productSku ,'almacen_id' => $store_default );
          $_product_pim = $model->getProductPim($params);
          if ($_product_pim && $_product_pim['status']) {
             $existencia = $_product_pim['success'][0]['stock'];
          }else{
            $existencia = 0;
          }
          Mage::log($item->getProductType());
          Mage::log($item->getName());
          Mage::log('-----------------');
          if ($item->getProductType() != 'bundle') {
                $cant = $item->getQty();
                if ((int)$existencia < (int)$cant) {
                      //Mage::throwException('Esta cantidad del producto "'.$item->getName().'" no esta disponible.');
                }
          }
  		    
        }*/
        if ($product->getTypeId() == 'bundle') {
          $bundled_items = array();
          $optionCollection = $product->getTypeInstance()->getOptionsCollection();
          $selectionCollection = $product->getTypeInstance()->getSelectionsCollection($product->getTypeInstance()->getOptionsIds());
          $options = $optionCollection->appendSelections($selectionCollection);
          foreach($options as $option) {
              $_selections = $option->getSelections();
              foreach($_selections as $selection) {
                  //Mage::log($selection->getQty());
                  //$bundled_items[$option->getId()] = $selection->getSku();
                  //$simple_bundle = Mage::getModel('catalog/product')->loadByAttribute('sku', $selection->getSku());
                  
                  $productSku = $selection->getSku();
                  $qty= $selection->getQty();
                  $qty = ($qty == 0 ) ? 1 : $qty ;
                  $params  = array('sku' => $productSku ,'almacen_id' => $store_default );
                  $_product_pim = $model->getProductPim($params);
                  if ($_product_pim && $_product_pim['status']) {
                     $existencia = $_product_pim['success'][0]['stock'];
                  }else{
                    $existencia = 0;
                  }
                  if((int)$qty > (int)$existencia){
                    $quoteItem->getQuote()->removeItem($selection->getId());
                    Mage::throwException('El producto "'.$selection->getName().'" no esta disponible');
                  }

              }
          }
      }else{

        $productSku = $sku;
        $qty= $product->getQty();
        $qty = ($qty == 0 ) ? 1 : $qty ;
        $params  = array('sku' => $productSku ,'almacen_id' => $store_default );
        $_product_pim = $model->getProductPim($params);
        if ($_product_pim && $_product_pim['status']) {
           $existencia = $_product_pim['success'][0]['stock'];
        }else{
          $existencia = 0;
        }
        if((int)$qty > (int)$existencia){
          $quoteItem->getQuote()->removeItem($quoteItem->getId());
          Mage::throwException('El producto "'.$quoteItem->getName().'" no esta disponible.');
        }
      }
      //Mage::throwException('El producto "'.$quoteItem->getName().'" esta fuera de stock.');

        /*if ($existencia <= 0 ) {
          Mage::throwException('El producto se encuentra fuera de stock.');
        }
        if ($existencia < $product->getQty()) {
            Mage::throwException('Esta cantidad no esta disponible');
        }*/
      }
  	}
    public function updateProductCart(Varien_Event_Observer $observer){
      $helper = Mage::Helper('janaq_pim');

      if ($helper->getStatus() && $helper->getStatusCart()) {
        $data = $observer->getEvent()->getInfo();
        //Mage::log($data);
        foreach ($data as $itemId => $itemInfo) {
            //$item = $this->getQuote()->getItemById($itemId);
            $item = Mage::getSingleton('checkout/session')->getQuote()->getItemById($itemId);
            if (!$item) {
                continue;
            }
            $store_default = $helper->getStoreDefault();
            $model = Mage::getModel('janaq_pim/service');
            if ($item->getProductType() == 'bundle') {
                $items = Mage::getSingleton('checkout/cart')->getQuote()->getAllItems();
                foreach ($items as $value) {
                  if ($value->getParentItemId() == $itemId) {
                      $params  = array('sku' => $value->getSku() ,'almacen_id' => $store_default );
                      $_product_pim = $model->getProductPim($params);
                      if ($_product_pim && $_product_pim['status']) {
                         $existencia = $_product_pim['success'][0]['stock'];
                      }else{
                        $existencia = 0;
                      }
                      if ((int)$existencia < (int)$itemInfo['qty'] || (int)$existencia == 0) {
                        Mage::throwException('El producto "'.$value->getName().'" se encuentra fuera de stock');
                      }
                  }
                }
            }else{
                $params  = array('sku' => $item->getSku() ,'almacen_id' => $store_default );
                $_product_pim = $model->getProductPim($params);
                if ($_product_pim && $_product_pim['status']) {
                   $existencia = $_product_pim['success'][0]['stock'];
                }else{
                  $existencia = 0;
                }
                if ((int)$existencia < (int)$itemInfo['qty'] || (int)$existencia == 0) {
                  Mage::throwException('El producto "'.$item->getName().'" se encuentra fuera de stock.');
                }
            }
            
        }
      }

    }

    public function saveProduct(Varien_Event_Observer $observer){
        $helper = Mage::Helper('janaq_pim');
        //Mage::log('Save Product observer');
        $product = $observer->getProduct();
        if ($helper->getStatus()  && ($product->getTypeId() == 'simple' || $product->getTypeId() == 'bundle' )) {
          //Mage::log($product->getStatus());
          if ($product->getSku() == '' || $product->getStatus() == '2') {
            return true;
          }
          $pim = Mage::getModel('janaq_pim/service');
          $pim_status = true;
          if ($product->getTypeId() == 'bundle' && $helper->getValidateProductBundleAdmin() ) {

              $pim_status = $pim->getValidateProductBundlePim($product->getSku());
          }elseif ($product->getTypeId() == 'simple' &&  $helper->getValidateProductAdmin() ){
              $pim_status = $pim->getValidateProductPim($product->getSku()); 
          }
          if (!$pim_status) {
            try {
                $reflectionClass = new ReflectionClass('Mage_Catalog_Model_Product');
                $reflectionProperty = $reflectionClass->getProperty('_dataSaveAllowed');
                $reflectionProperty->setAccessible(true);
                $reflectionProperty->setValue($product, false);
                Mage::getSingleton('adminhtml/session')->addError('El sku "'.$product->getSku().'" no existe en PIM.');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError('Ocurrió un error al verificar con PIM');
                
            }
            
          }
          
        }
    }

    public function saveFactura(Varien_Event_Observer $observer){
        $params = Mage::app()->getRequest()->getParams();

        $order = $observer->getEvent()->getOrder();
        //Mage::log($params);
        $params['is_ruc'] = (isset($params['is_ruc']))? '1':'0' ;
        if ($params['is_ruc']) {
          $order->setData('ruc_pim', $params['ruc_pim']);
          $order->setData('razon_social_pim', $params['razon_social_pim']);
        }
    }
    public function getSalesOrderViewInfo(Varien_Event_Observer $observer) {
        $block = $observer->getBlock();
        
        if (($block->getNameInLayout() == 'order_info') && ($child = $block->getChild('pim.order.info.custom.block'))) {
            $transport = $observer->getTransport();
            if ($transport) {
                $html = $transport->getHtml();
                $html .= $child->toHtml();
                $transport->setHtml($html);
            }
        }
    }
  }
