<?php

class Janaq_Pim_Model_Cron
{

    /* -----------------------------------------------------------------------------------------
            - Proceso de Enviar pedido a PIM Laravel
    */
    public function run()
    {
        $date = Mage::getModel('core/date')->date('Y-m-d H:i:s');
        $Collection = Mage::getResourceModel('janaq_pim/integrador_collection');
        $Collection->addFieldToFilter('status', array('in' => array('pendiente')));
        //ORDER
        //Mage::log('cron_pim');
        //$parameters = array();
        foreach ($Collection as  $tareas) {
            $model = Mage::getModel('janaq_pim/integrador');
            $model->load($tareas->getId());
            if ($tareas->getTypeTask() == 'Order') {
            	try{
                    $incrementId = $model->getOrderId();
                    $order = Mage::getModel('sales/order')->loadByIncrementId($incrementId);

                    $billing_address = $order->getBillingAddress();
                    $billing_postcode = $billing_address->getPostcode();
                    $billing_postcode = explode('-',$billing_postcode);
                    $shipping_address = $order->getShippingAddress();
                    $shipping_postcode = $shipping_address->getPostcode();
                    $shipping_postcode = explode('-',$shipping_postcode);
                    $orderItems = $order->getAllItems();
                    $helper = Mage::Helper('janaq_pim');
                    $almacen_id = false;
                    if ($order->getShippingMethod() == 'storepickup_storepickup') {

                        $_store = Mage::getModel('storelocator/storelocator')->load($order->getStoreStorepickup());
                        if ($_store && $_store->getId()) {
                          $almacen_id = $_store->getAlmacenId();
                        }
                    }
                    if ($order->getShippingMethod() == 'urbaner_urbaner') {

                        $_store = Mage::getModel('storelocator/storelocator')->load($order->getUrbanerAlmacenId());
                        //if ($_store && $_store->getId() && $order->getUrbanerOrderType() == '4') {
                        if ($_store && $_store->getId()) {
                          $almacen_id = $_store->getAlmacenId();
                        }
                    }
                    if (!$almacen_id) {
                      $almacen_id = $helper->getStoreDefault();
                    }
                    $payemnt_method_id = '0';
                    switch ($order->getPayment()->getMethod()) {
                      case 'mercadopago_custom':
                        $payemnt_method_id = Mage::getStoreConfig('payment/mercadopago_custom/odoo_payment_method_id', Mage::app()->getStore());
                        break;
                      case 'mercadopago_customticket':
                        $payemnt_method_id = Mage::getStoreConfig('payment/mercadopago_customticket/odoo_payment_method_id', Mage::app()->getStore());
                        break;
                      case 'ondelivery':
                        $payemnt_method_id = Mage::getStoreConfig('payment/ondelivery/odoo_payment_method_id', Mage::app()->getStore());
                        break;
                      
                      default:
                        $payemnt_method_id = '0';
                        break;
                    }
                    $items = array();
                    $_item = array();
                    
                    $discount_product_total = 0.0000;
                    foreach ($orderItems as $item) {
                      
                      $data = $item->getData();
                      //Mage::log($data['product_type'] .' - '. $data['name'] . ' - '.$data['price'] . ' - '.$data['qty_ordered']);
                      if ($data['product_type'] == 'configurable') {
                        $row_total = $data['row_total'];
                        $_price = $data['price'];
                        foreach ($orderItems as $simple) {
                            $discount_product = "0.0000";
                            if ($simple['product_type'] == 'simple' && $simple['parent_item_id'] == $data['item_id']) {
                                $product = Mage::getModel('catalog/product')->load($simple["product_id"]);
                                $price = $product->getPrice();
                                if ((double)$product->getPrice() != (double)$_price ) {
                                    $discount_product = (double)$product->getPrice() - (double)$_price;
                                    $discount_product_total += $discount_product;
                                    $discount_product = ($discount_product  * 100)/(double)$product->getPrice();
                                }
                                
                                $_item['item_id'] = $simple['item_id'];
                                $_item['magento_order_id'] = $simple['order_id'];
                                $_item['parent_item_id'] = $simple['parent_item_id'];
                                $_item['product_id'] = $simple['product_id'];
                                $_item['product_type'] = $simple['product_type'];
                                $_item['name'] = $simple['name'];
                                $_item['sku'] = $simple['sku'];
                                $_item['qty'] = $simple['qty_ordered'];
                                $_item['price'] = $price;
                                $_item['discount'] = number_format( $discount_product,4,'.','');
                                $_item['row_total'] = $row_total;
                                $items[] = $_item;
                            }
                        }
                        
                      }elseif ($data['product_type'] == 'bundle') {
                          $data = $item->getData();
                          $product = Mage::getModel('catalog/product')->load($data["product_id"]);
                          $discount_product = "0.0000";
                          //Mage::log($product->getPrice());
                          if ((double)$product->getPrice() != (double)$data['price'] ) {
                              $discount_product = (double)$product->getPrice() - (double)$data['price'];
                              $discount_product_total += $discount_product;
                              $discount_product = ($discount_product  * 100)/(double)$product->getPrice();
                          }
                          $_item['item_id'] = $data['item_id'];
                          $_item['magento_order_id'] = $data['order_id'];
                          $_item['parent_item_id'] = $data['parent_item_id'];
                          $_item['product_id'] = $data['product_id'];
                          $_item['product_type'] = $data['product_type'];
                          $_item['name'] = $data['name'];
                          $_item['sku'] = $data['sku'];
                          $_item['qty'] = $data['qty_ordered'];
                          $_item['price'] = $product->getPrice();
                          $_item['discount'] = number_format( $discount_product,4,'.','');
                          $_item['row_total'] = $data['row_total'];
                          $items[] = $_item;
                          foreach ($orderItems as $simple) {
                            $discount_product = "0.0000";
                            if ($simple['product_type'] == 'simple' && $simple['parent_item_id'] == $data['item_id']) {
                                
                                $_item['item_id'] = $simple['item_id'];
                                $_item['magento_order_id'] = $simple['order_id'];
                                $_item['parent_item_id'] = $simple['parent_item_id'];
                                $_item['product_id'] = $simple['product_id'];
                                $_item['product_type'] = $simple['product_type'];
                                $_item['name'] = $simple['name'];
                                $_item['sku'] = $simple['sku'];
                                $_item['qty'] = $simple['qty_ordered'];
                                $_item['price'] = $simple['price'];
                                $_item['discount'] = $discount_product;
                                $_item['row_total'] = $simple['row_total'];
                                $items[] = $_item;
                            }
                        }

                      }else{
                          if ($data['product_type'] == 'simple' && $data['parent_item_id'] == NULL ) {
                            $product = Mage::getModel('catalog/product')->load($data["product_id"]);
                            if ((double)$product->getPrice() != (double)$data["price"]) {
                                $discount_product = (double)$product->getPrice() - (double)$data["price"];
                                $discount_product_total += $discount_product;
                                $discount_product = ($discount_product  * 100)/(double)$product->getPrice();

                            }
                            $_item['item_id'] = $data['item_id'];
                            $_item['magento_order_id'] = $data['order_id'];
                            $_item['parent_item_id'] = $data['parent_item_id'];
                            $_item['product_id'] = $data['product_id'];
                            $_item['product_type'] = $data['product_type'];
                            $_item['name'] = $data['name'];
                            $_item['sku'] = $data['sku'];
                            $_item['qty'] = $data['qty_ordered'];
                            $_item['price'] = $product->getPrice();
                            $_item['row_total'] = $data['row_total'];
                            $_item['discount'] =  number_format( $discount_product,4,'.','');
                            $items[] = $_item;
                            $_item = array();

                          }
                      }
                      
                    }
                    //factura 
                    $name = $billing_address->getFirstname();
                    $lastname = $billing_address->getLastname();
                    $type_document = $billing_address->getDoctype();
                    $document = $billing_address->getFax();
                    if ($order->getRucPim() && $order->getRazonSocialPim()) {
                      $type_document = "6";
                      $document = $order->getRucPim();
                      $name = $order->getRazonSocialPim();
                      $lastname = "-";
                    }
                    $params = array(
                      'entity_id' => $order->getId(),
                      'source_order'=>'Magento',
                      'increment_id' => $order->getIncrementId(),
                      'status' => $order->getStatus(),
                      'status_pim' => 'pending',
                      'shipping_method' => $order->getShippingMethod(),
                      'almacen_id' => $almacen_id,
                      'customer_id' => $order->getCustomerId(),
                      'customer_email' => $order->getCustomerEmail(),
                      'customer_firstname' => $order->getCustomerFirstname(),
                      'customer_lastname' => $order->getCustomerLastname(),
                      'shipping_amount' => $order->getShippingAmount(),
                      'subtotal' => $order->getSubtotal(),
                      'grand_total' => $order->getGrandTotal(),
                      //'discount_amount' => number_format( (double)$order->getDiscountAmount() -  (double)$discount_product_total , 4 ,'.',''),
                      'discount_amount' => $order->getDiscountAmount(),
                      'billing_firstname' => $name,
                      'billing_lastname' => $lastname,
                      'billing_type_document' => $type_document,
                      'billing_document' => $document,
                      'billing_region' => $billing_address->getRegion(),
                      'billing_province' => $billing_postcode[0],
                      'billing_district' => $billing_postcode[1],
                      'billing_address' => $billing_address->getData('street'),
                      'billing_reference' => $billing_address->getCompany(),
                      'billing_phone' => $billing_address->getTelephone(),
                      'shipping_firstname' => $shipping_address->getFirstname(),
                      'shipping_lastname' => $shipping_address->getLastname(),
                      'shipping_type_document' => $shipping_address->getDoctype(),
                      'shipping_document' => $shipping_address->getFax(),
                      'shipping_region' =>  $shipping_address->getRegion(),
                      'shipping_province' => $shipping_postcode[0],
                      'shipping_district' => $shipping_postcode[1],
                      'shipping_address' => $shipping_address->getData('street'),
                      'shipping_reference' => $shipping_address->getCompany(),
                      'shipping_phone' => $shipping_address->getTelephone(),
                      'payment_method' => $order->getPayment()->getMethod(),
                      'payment_method_id' => $payemnt_method_id,
                      'onestepcheckout_order_comment' => $order->getOnestepcheckoutOrderComment(),
                      'cupon_code' => $order->getCouponCode(),
                      'order_created_at' => $order->getCreatedAt(),
                      'order_updated_at' => $order->getUpdatedAt(),
                      'order_item' =>$items
                    );
                    $pim = Mage::getModel('janaq_pim/service');
                    //Mage::log($params);
                    $response_array = $pim->setOrder($params);
                    //Mage::log($response_array);
                    if ($response_array && $response_array['status']) {

                        $model->setStatus('completado');
                        $model->setDataSent(json_encode($params));
                        $model->setUpdatedAt($date);
                        $model->setResponse(json_encode($response_array));
                        $model->save();
                        //var_dump($xmls->Table->NUMERO_GUIA);

                    }else{
                        $model->setDataSent(json_encode($params));
                        $model->setResponse(json_encode($response_array));
                        $model->setUpdatedAt($date);
                        $model->save();
                    }
                } catch (Exception $e){
                        $model->setResponse($e->getMessage());
                        $model->setDataSent(json_encode($params));
                        $model->setUpdatedAt($date);
                        $model->save();
                        Mage::log('Error al actualizar estado de la Tarea Order');
                }
            }

    	}
      $this->invoice();
	}

  public function invoice(){
    $helper = Mage::Helper('janaq_pim');
    if ($helper->getStatus()) {
      $pim = Mage::getModel('janaq_pim/service');
      $order_pim = $pim->getOrderInvoiced();
      //Mage::log($order_pim);
      $status = 1; // EN DESPACHO.
      $new_status = 'in_dispatch'; // EN DESPACHO.
      foreach ($order_pim as $_order) {
        if ($_order['status_pim'] == 'invoiced_invoiced') {

            $order = Mage::getModel('sales/order')->load($_order['entity_id']);
            if ($order->getId()) {
                //Mage::log($order->getIncrementId());
                $order->setStatusLogistics($status);
                $order->save();
                $res = $pim->orderChangeStatusPim($_order['increment_id'],$new_status);
                //Mage::log($res);
            }
        }
      }
    }
  }

}
