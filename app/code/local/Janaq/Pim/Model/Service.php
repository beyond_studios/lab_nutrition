<?php
/**
 * Created by PhpStorm.
 * User: HOME
 * Date: 11/12/2016
 * Time: 21:35
 */
class Janaq_Pim_Model_Service extends Mage_Core_Model_Abstract
{
    public function Login($token = false)
    {
        $cacheId = 'janaq_pim_login';
    		if (false !== ($data = Mage::app()->getCache()->load($cacheId))) {
    		    $data = unserialize($data);
            //Mage::log('SI CACHE');
    		} else {
            //Mage::log('NO CACHE');
    		    $email = Mage::helper('janaq_pim')->getUser();
    	    	$pwd = Mage::helper('janaq_pim')->getPassword();
    	    	$api_url = Mage::helper('janaq_pim')->getUrl() . "login";
    	    	$parameters = array(
    	    		'email' => $email,
    	    		'password' => $pwd
    	    	);
      	    $parameters = json_encode($parameters);
      			$httpHeader = array('Content-Type: application/json;');
      			$curl = curl_init($api_url);
      			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
      			curl_setopt($curl, CURLOPT_HTTPHEADER,$httpHeader);
      			curl_setopt($curl, CURLOPT_POSTFIELDS, $parameters);
      			curl_setopt($curl, CURLOPT_POST, true);
      			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

      			$response = curl_exec($curl);
      			if ($response) {
      			    $data = json_decode($response,true);
      			    $cacheLifetime = 864000;
      		    	Mage::app()->getCache()->save(serialize($data), $cacheId, array("COLLECTION_DATA"), $cacheLifetime);
      	    }
		  }
      if ($token) {
        return $data['success']['token'];
      }else{
        return $data;
      }
    }

    public function setOrder($parameters){
      $data_response = "";
      $helper = Mage::helper('janaq_pim');
      $api_url = $helper->getUrl() . "order-register";
      $data_response = array();
      //Mage::log($parameters);
      try {
        $token = $this->login(true);
        $parameters = json_encode($parameters);
        $httpHeader = array('Content-Type: application/json;','Authorization: Bearer '. $token );
        $curl = curl_init($api_url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_HTTPHEADER,$httpHeader);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $parameters);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($curl);
        //Mage::log($response);
        if ($response) {
                $data_response = json_decode($response,true);
        }
        return $data_response;
      } catch (\Exception $e) {
          return false;
      }

    }

    public function getProductPim($parameters){
      $data_response = "";
      $helper = Mage::helper('janaq_pim');
      $api_url = $helper->getUrl() . "product-detail";
      $data_response = array();
      //Mage::log($parameters);
      try {
        $token = $this->login(true);
        $parameters = json_encode($parameters);
        $httpHeader = array('Content-Type: application/json;','Authorization: Bearer '. $token );
        $curl = curl_init($api_url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_HTTPHEADER,$httpHeader);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $parameters);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($curl);
        //Mage::log($response);
        if ($response) {
                $data_response = json_decode($response,true);
        }
        return $data_response;
      } catch (\Exception $e) {
          return false;
      }

    }

    public function getValidateProductPim($sku){
      $helper = Mage::helper('janaq_pim');
      $api_url = $helper->getUrl() . "product-detail-all";
      $data_response = array();
      try {
        $token = $this->login(true);
        $parameters = json_encode(array("sku"=>$sku));
        $httpHeader = array('Content-Type: application/json;','Authorization: Bearer '. $token );
        $curl = curl_init($api_url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_HTTPHEADER,$httpHeader);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $parameters);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($curl);
        //Mage::log($response);
        if ($response) {
                $data_response = json_decode($response,true);
                return $data_response['status'];
        }else{
            return false;
        }
      } catch (\Exception $e) {
          return false;
      }
    }

    public function getValidateProductBundlePim($sku){
      $helper = Mage::helper('janaq_pim');
      $api_url = $helper->getUrl() . "product-detail-bundle";
      $data_response = array();
      try {
        $token = $this->login(true);
        $parameters = json_encode(array("sku"=>$sku));
        $httpHeader = array('Content-Type: application/json;','Authorization: Bearer '. $token );
        $curl = curl_init($api_url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_HTTPHEADER,$httpHeader);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $parameters);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($curl);
        //Mage::log($response);
        if ($response) {
                $data_response = json_decode($response,true);
                return $data_response['status'];
        }else{
            return false;
        }
      } catch (\Exception $e) {
          return false;
      }
    }

    public function getProductByStore($almacen_id){
      $helper = Mage::helper('janaq_pim');
      $api_url = $helper->getUrl() . "product-list-store";
      $data_response = array();
      try {
        $token = $this->login(true);
        $parameters = json_encode(array("almacen_id"=>$almacen_id));
        $httpHeader = array('Content-Type: application/json;','Authorization: Bearer '. $token );
        $curl = curl_init($api_url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_HTTPHEADER,$httpHeader);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $parameters);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($curl);
        //Mage::log($response);
        if ($response) {
                $data_response = json_decode($response,true);
                if ($data_response['status'] == true) {
                  return $data_response['success'];
                }else{
                  return array();
                }
                
        }else{
            return array();
        }
      } catch (\Exception $e) {

          return array();
      }
    }

    public function orderChangeStatusPim($increment_id,$new_status){
        $helper = Mage::helper('janaq_pim');
        $api_url = $helper->getUrl() . "order-change-status";
        $data_response = array();
        try {
          $token = $this->login(true);
          $parameters = json_encode(array(array("increment_id"=>$increment_id,"status_pim"=> $new_status )));
          $httpHeader = array('Content-Type: application/json;','Authorization: Bearer '. $token );
          $curl = curl_init($api_url);
          curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
          curl_setopt($curl, CURLOPT_HTTPHEADER,$httpHeader);
          curl_setopt($curl, CURLOPT_POSTFIELDS, $parameters);
          curl_setopt($curl, CURLOPT_POST, true);
          curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
          $response = curl_exec($curl);
          //Mage::log($response);
          if ($response) {
                  $data_response = json_decode($response,true);
                  return $data_response['status'];
          }else{
              return false;
          }
        } catch (\Exception $e) {
            return false;
        }
    }

    public function getOrderInvoiced(){
      $helper = Mage::helper('janaq_pim');
      $api_url = $helper->getUrl() . "order-list-by-status";
      $data_response = array();
      try {
        $token = $this->login(true);
        $parameters = json_encode(array('status_pim' => 'invoiced_invoiced'));
        $httpHeader = array('Content-Type: application/json;','Authorization: Bearer '. $token );
        $curl = curl_init($api_url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_HTTPHEADER,$httpHeader);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $parameters);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($curl);
        //Mage::log($response);
        if ($response) {
                $data_response = json_decode($response,true);
                if ($data_response['success']) {
                  return $data_response['success'];
                }else{
                  return array();
                }
                
        }else{
            return array();
        }
      } catch (\Exception $e) {

          return array();
      }
    }

}
