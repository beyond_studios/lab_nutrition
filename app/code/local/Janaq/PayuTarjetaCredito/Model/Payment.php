<?php


require_once(Mage::getBaseDir('lib') . '/PayU/PayU.php');

/**
 * Janaq Payment PayuTarjetaCredito Model
 */
class Janaq_PayuTarjetaCredito_Model_Payment extends Mage_Payment_Model_Method_Cc
{

    /**
     * Payment method code
     *
     * @var string
     */
    protected $_code  = 'payutarjetacredito';

    /**
     * Cash On Delivery payment block paths
     *
     * @var string
     */
    protected $_formBlockType = 'payutarjetacredito/form';
    protected $_infoBlockType = 'payutarjetacredito/info';

    protected $_canAuthorize    = true;
    protected $_canSaveCc       = false;
    /**
     * Get instructions text from config
     *
     * @return string
     */
    public function getInstructions()
    {
        return trim($this->getConfigData('instructions'));
    }

    /*public function getOrderPlaceRedirectUrl()
    {
        return Mage::getUrl('payutarjetacredito/creditcard/payment', array('_secure' => true));
    }*/

    /**
     * Stores custom data for payment method in the database.
     *
     * @return Mage_Payment_Model_Method_Abstract
     */
    /*public function assignData($data)
    {
        parent::assignData($data);

        if (!($data instanceof Varien_Object)) {
            $data = new Varien_Object($data);
        }
        $info = $this->getInfoInstance();
        $info->setMoneyAmount(null);
        $info->save();
        return $this;
    }*/

    public function assignData($data)
    {
        if (!($data instanceof Varien_Object)) {
            $data = new Varien_Object($data);
        }
        $info = $this->getInfoInstance();
        $info->setCcType($data->getCcType())
            ->setCcOwner($data->getCcOwner())
            ->setCcLast4(substr($data->getCcNumber(), -4))
            ->setCcNumber($data->getCcNumber())
            ->setCcCid($data->getCcCid())
            ->setCcExpMonth($data->getCcExpMonth())
            ->setCcExpYear($data->getCcExpYear())
            ->setCcSsIssue($data->getCcSsIssue())
            ->setCcSsStartMonth($data->getCcSsStartMonth())
            ->setCcSsStartYear($data->getCcSsStartYear())
            ->setAdditionalInformation('installments',$data->getCcInstallments())
            ->setAdditionalInformation('device',$data->getCcDeviceSessionId())
            ->setAdditionalInformation('dni',$data->getCcDni());
        return $this;
    }

    public function getVerificationRegEx(){

        return array_merge(parent::getVerificationRegEx(), array(
            'DC' => '/^[0-9]{3}$/' // Diners Club CCV
       ));
    }

    public function OtherCcType($type)
    {
        return in_array($type, array('OT', 'DC'));
    }

    /**
     * Validates custom data for payment method.
     *
     * @return Mage_Payment_Model_Method_Abstract
     */
    public function validate()
    {
        //Mage::log('validate');
        parent::validate();

        $info = $this->getInfoInstance();
        $ccNumber = $info->getCcNumber();

        $availableTypes = explode(',',$this->getConfigData('cctypes'));

        if(!in_array($info->getCcType(), $availableTypes)){
            Mage::throwException($this->_getHelper()->__('Credit card type is not allowed for this payment method.'));
        }

        // validate credit card number against Luhn algorithm
        if(!$this->validateCcNum($info->getCcNumber())){
            Mage::throwException($this->_getHelper()->__('Invalid Credit Card Number'));
        }

        if($info->getCcType()=='DC' && !preg_match('/^3(?:0[0-5]|[68][0-9])[0-9]{11}$/', $ccNumber)){
            Mage::throwException($this->_getHelper()->__('El número de tarjeta de crédito no coincide con el tipo de tarjeta de crédito.'));
        }

        // now we retrieve our CCV regex pattern and validate against it
        $verificationRegex = $this->getVerificationRegEx();
        if(!preg_match($verificationRegex[$info->getCcType()], $info->getCcCid()))
        {
            Mage::throwException($this->_getHelper()->__('Introduzca un número de verificación de tarjeta de crédito válido.'));
        }
//        $money = $info->getMoneyAmount();
//
//        if(empty($money)){
//            $errorCode = 'invalid_data';
//            $errorMsg = $this->_getHelper()->__('Money Amount is a required field');
//        }
//
//        if($errorMsg){
//            Mage::throwException($errorMsg);
//        }
        return $this;
    }

    public function authorize(Varien_Object $payment, $amount)
    {
        Mage::log('Authorize');
        $errorMsg = false;
        $order = $payment->getOrder();
        $result = $this->callApi($payment,$amount,'authorize');
        if($result === false) {
            $errorCode = 'Invalid Data';
            $errorMsg = $this->_getHelper()->__('Error Processing the request');
        } else {
            //Mage::log($result, null, $this->getCode().'.log');
            // if invalid status throw exception

            if($result['status']){
                $order_comment = '';
                foreach($result as $key=>$value){
                    $order_comment .= "<br/>$key: $value";
                }

                if ($result['state'] == 'APPROVED') {
                    $payment->setTransactionId($result['transactionId']);

                    $payment->setTransactionAdditionalInfo(Mage_Sales_Model_Order_Payment_Transaction::RAW_DETAILS,$result); //use this in case you want to add some extra information
                    $payment->setIsTransactionClosed(0);
                    $payment->getOrder()->setStatus('confirmado_tarjeta_credito');
                    $params['order'] = $order;
                    Mage::dispatchEvent('order_servientrega_integrador_save', $params);
                    Mage::dispatchEvent('order_urbaner_integrador_save', $params);
                    Mage::dispatchEvent('order_alldaydelivery_integrador_save', $params);
                    Mage::dispatchEvent('janaq_pim_order_save', $params);

                    $payment->getOrder()->addStatusToHistory('confirmado_tarjeta_credito', $order_comment);
                } elseif ($result['state'] == 'PENDING') {
                   $payment->setTransactionId($result['transactionId']);

                    $payment->setTransactionAdditionalInfo(Mage_Sales_Model_Order_Payment_Transaction::RAW_DETAILS,$result); //use this in case you want to add some extra information
                    $payment->setIsTransactionClosed(0);
                    $payment->getOrder()->setStatus('pending_tarjeta_credito');
                    $payment->getOrder()->addStatusToHistory('pending_tarjeta_credito', $order_comment);

                } else {

                   Mage::throwException('Transaccion Declinada : '.$result['state']);

                }
            }else{
                Mage::throwException('Transaccion no aprobada : '.$result['message']);
            }
            // Add the comment and save the order
        }
        return $this;
    }
    private function callApi(Varien_Object $payment, $amount,$type){
        $response = array();
        $order = $payment->getOrder();
        $params = $this->getFormFields($payment,$order);

        $url = $this->getPayuUrl();
        $payu_response = $this->sendPostData($url,$params);
        if ($payu_response->code == "SUCCESS") {

                //TODO: Realizar una accion en el caso de que el estado de la transaccion este aprobado.
                $response["status"] = true;
                $response['orderId'] = $payu_response->transactionResponse->orderId;
                $response["message"] = $payu_response->code;
                $response["transactionId"] = $payu_response->transactionResponse->transactionId;
                $response["state"] = $payu_response->transactionResponse->state;
                $response["trazabilityCode"] = $payu_response->transactionResponse->trazabilityCode;
                $response["authorizationCode"] = $payu_response->transactionResponse->authorizationCode;
                $response["operationDate"] = $payu_response->transactionResponse->operationDate;
        } else {
            //TODO
            $response["status"] = false;
            $response["message"] = $payu_response->code;
        }
        //Mage::log($payu_response);
        return $response;
        //return array('status'=>1,'transaction_id' => time() , 'fraud' => rand(0,1));
    }

    public function getFormFields($payment,$orderPayment)
    {
        //Mage::log($payment->getCcOwner());

        $merchant_id = Mage::getStoreConfig( 'payment/payutarjetacredito/merchant_id' );
        $secure_key = Mage::getStoreConfig( 'payment/payutarjetacredito/secure_key' );
        $api_login = Mage::getStoreConfig( 'payment/payutarjetacredito/api_login' );
        $account_id = Mage::getStoreConfig( 'payment/payutarjetacredito/account_id' );
        $gateway_url = Mage::getStoreConfig( 'payment/payutarjetacredito/payu_url' );
        $transaction_mode = Mage::getStoreConfig( 'payment/payutarjetacredito/transaction_mode' );

        $checkout = Mage::getSingleton('checkout/session');

        //$orderIncrementId = $checkout->getLastRealOrderId();
        $orderIncrementId = $orderPayment->getIncrementId();
        $order = Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);

        $currency   = $order->getOrderCurrencyCode();

        $BAddress = $order->getBillingAddress();

        $paymentAmount = number_format($order->getGrandTotal(),2,'.','');
        $tax = number_format($order->getTaxAmount(),2,'.','');

        $taxReturnBase = number_format(($paymentAmount - $tax),2,'.','');
        if($tax == 0) $taxReturnBase = 0;


        $ProductName = '';
        $items = $order->getAllItems();
        if ($items)
        {
            foreach($items as $item)
            {
                if ($item->getParentItem()) continue;
                $ProductName .= $item->getName() . '; ';
            }
        }
        $ProductName = rtrim($ProductName, '; ');
        //Limitar nombres de productos para evitar error en la descripcion
        if (strlen($ProductName) >= 255) {
          $ProductName = (substr($ProductName, 0, 250))."...";
        }

        $signature = md5($secure_key . '~' . $merchant_id . '~' . $orderIncrementId . '~' . $paymentAmount . '~' . $currency );

        $test = "false";
        $language   = 'es';
        $command    = 'SUBMIT_TRANSACTION';
        $ip_address = Mage::helper('core/http')->getRemoteAddr();
        $cookie = Mage::getModel('core/cookie')->get('frontend');
        $types = Mage::getSingleton('payment/config')->getCcTypes();
        if (isset($types[$payment->getCcType()])) {
        $type = $types[$payment->getCcType()];
        $method_payment = ($payment->getCcType() == 'AE') ? 'AMEX' : $type;
        $method_payment = ($payment->getCcType() == 'DC') ? 'DINERS' : $method_payment;
        }else{
            Mage::throwException('Tipo de tarjeta de crédito no válido');
        }

        //Mage::log('Methodo de pago: '.$method_payment);

        $year  = $payment->getCcExpYear();
        $month  = $payment->getCcExpMonth();
        $month  = sprintf('%02d', $month);

        $expiration_date = $year.'/'.$month;
        $user_agent =Mage::helper('core/http')->getHttpUserAgent();

        if($transaction_mode == 'test') $test = "true";



        $params = array(
                'language'  =>$language,
                'command'   =>$command,
                'merchant'  =>array(
                            'apiKey'    =>$secure_key,
                            'apiLogin'  =>$api_login,
                    ),
                'transaction'   => array(
                            'order' =>  array(
                                    'accountId' => $account_id,
                                    'referenceCode' => $orderIncrementId,
                                    'description'       =>  $ProductName,
                                    'language'  => $language,
                                    'signature' => $signature,
                                    'notifyUrl' => Mage::getUrl('payutarjetacredito/creditcard/notify'),
                                    'additionalValues' => array(
                                            'TX_VALUE' => array(
                                                    'value' =>$paymentAmount,
                                                    'currency' => $currency,
                                                ),
                                        ),


                                    'buyer' => array(
                                            'merchantBuyerId' => '1',
                                            'fullName' => $order->getShippingAddress()->getFirstname().' '.$order->getShippingAddress()->getLastname(),
                                            'emailAddress' => $order->getShippingAddress()->getEmail(),
                                            'contactPhone' => $order->getShippingAddress()->getTelephone(),
                                            'dniNumber' =>  $order->getShippingAddress()->getFax(),
                                            'shippingAddress' => array(
                                                    'street1' => $order->getShippingAddress()->getStreet1(),
                                                    'street2' => $order->getShippingAddress()->getStreet2(),
                                                    'city'     =>   $order->getShippingAddress()->getCity(),
                                                    'state'     => $order->getShippingAddress()->getRegion(),
                                                    'country'   => 'PE',// $order->getShippingAddress()->getCountry(),
                                                    'postalCode'    => "000000",
                                                    'phone' => $order->getShippingAddress()->getTelephone(),
                                                ),
                                        ),
                                    'shippingAddress' => array(
                                                'street1' => $order->getShippingAddress()->getStreet1(),
                                                'street2' => $order->getShippingAddress()->getStreet2(),
                                                'city'     =>   $order->getShippingAddress()->getCity(),
                                                'state'     => $order->getShippingAddress()->getRegion(),
                                                'country'   => 'PE',// $order->getShippingAddress()->getCountry(),
                                                'postalCode'    => "000000",
                                                'phone' => $order->getShippingAddress()->getTelephone(),
                                            ),
                                ),
                            'payer' => array(
                                    'merchantPayerId' => '1',
                                    'fullName' => $order->getBillingAddress()->getFirstname().' '.$order->getBillingAddress()->getLastname(),
                                    'emailAddress' =>   $order->getBillingAddress()->getEmail(),
                                    'contactPhone' =>   $order->getBillingAddress()->getTelephone(),
                                    //'dniNumber' =>  $order->getBillingAddress()->getFax(),
                                    'dniNumber' => $payment->getAdditionalInformation('dni'),
                                    'billingAddress' => array(
                                            'street1' => $order->getBillingAddress()->getStreet1(),
                                            'street2' =>  $order->getBillingAddress()->getStreet2(),
                                            'city'  =>  $order->getBillingAddress()->getCity(),
                                            'state' =>  $order->getBillingAddress()->getRegion(),
                                            'country' => 'PE',
                                            'postalCode' => "000000",
                                            'phone' =>  $order->getBillingAddress()->getTelephone(),
                                        ),
                                ),
                            'creditCard' => array(
                                    'number' => $payment->getCcNumber(),
                                    'securityCode'  => $payment->getCcCid(),
                                    'expirationDate' => $expiration_date,
                                    'name' =>$payment->getCcOwner(),
                                ),
                            'extraParameters' => array(
                                    'INSTALLMENTS_NUMBER' =>$payment->getAdditionalInformation('installments'),
                                ),
                            'type' => "AUTHORIZATION_AND_CAPTURE",
                            'paymentMethod' => strtoupper($method_payment),//type method
                            'paymentCountry'    =>'PE',
                            'deviceSessionId'   => $payment->getAdditionalInformation('device'),
                            'ipAddress'     => Mage::helper('core/http')->getRemoteAddr(),
                            'cookie'    => Mage::getModel('core/cookie')->get()['frontend'],
                            'userAgent' =>$user_agent,

                    ),
                'test' => $test,
            );


        Mage::log('DATOS ENVIO '.$method_payment.':',null,$this->getCode().'.log');
        //Mage::log($params);
        return $params;




    }

    protected function sendPostData($url, $params){



            //open connection
            /*Mage::log($url);
            Mage::log($fields);
            $ch = curl_init();
            Mage::log($ch);
            $headers = array(
                'Content-Type: application/json',
                'Accept: application/json',
                'Content-Length: length'
            );
            //set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt($ch, CURLOPT_POST,1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$fields);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION ,1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER ,1); // RETURN THE CONTENTS OF THE CALL
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120); // Timeout on connect (2 minutes)
            //execute post
            $result = curl_exec($ch);
            curl_close($ch);
            return $result;*/
            $merchant_id = Mage::getStoreConfig('payment/payutarjetacredito/merchant_id');
            PayU::$apiKey = $params['merchant']['apiKey']; //Ingrese aquí su propio apiKey.
            PayU::$apiLogin =  $params['merchant']['apiLogin']; //Ingrese aquí su propio apiLogin.
            PayU::$merchantId = $merchant_id; //Ingrese aquí su Id de Comercio.
            PayU::$language = SupportedLanguages::ES; //Seleccione el idioma.
            PayU::$isTest = $params['test']; //Dejarlo True cuando sean pruebas.

            Environment::setPaymentsCustomUrl($url);

            //Environment::setReportsCustomUrl("https://stg.api.payulatam.com/reports-api/4.0/service.cgi");

            $parameters = array(
                //Ingrese aquí el identificador de la cuenta.
                PayUParameters::ACCOUNT_ID => $params['transaction']['order']['accountId'],
                //Ingrese aquí el código de referencia.
                PayUParameters::REFERENCE_CODE => $params['transaction']['order']['referenceCode'],
                //Ingrese aquí la descripción.
                PayUParameters::DESCRIPTION => $params['transaction']['order']['description'],

                // -- Valores --
                //Ingrese aquí el valor.
                PayUParameters::VALUE => $params['transaction']['order']['additionalValues']['TX_VALUE']['value'],
                //Ingrese aquí la moneda.
                PayUParameters::CURRENCY => $params['transaction']['order']['additionalValues']['TX_VALUE']['currency'],

                // -- Comprador
                //Ingrese aquí el nombre del comprador.
                PayUParameters::BUYER_NAME => $params['transaction']['order']['buyer']['fullName'],
                //Ingrese aquí el email del comprador.
                PayUParameters::BUYER_EMAIL => $params['transaction']['order']['buyer']['emailAddress'],
                //Ingrese aquí el teléfono de contacto del comprador.
                PayUParameters::BUYER_CONTACT_PHONE => $params['transaction']['order']['buyer']['contactPhone'],
                //Ingrese aquí el documento de contacto del comprador.
                //PayUParameters::BUYER_DNI => $params['transaction']['order']['buyer']['dniNumber'],
                PayUParameters::BUYER_DNI => $params['transaction']['order']['buyer']['dniNumber'],
                //Ingrese aquí la dirección del comprador.
                PayUParameters::BUYER_STREET => $params['transaction']['order']['buyer']['shippingAddress']['street1'],
                PayUParameters::BUYER_STREET_2 => $params['transaction']['order']['buyer']['shippingAddress']['street2'],
                PayUParameters::BUYER_CITY => $params['transaction']['order']['buyer']['shippingAddress']['city'],
                PayUParameters::BUYER_STATE => $params['transaction']['order']['buyer']['shippingAddress']['state'],
                PayUParameters::BUYER_COUNTRY => "PE",
                PayUParameters::BUYER_POSTAL_CODE => "000000",
                PayUParameters::BUYER_PHONE => $params['transaction']['order']['buyer']['shippingAddress']['phone'],

                // -- pagador --
                //Ingrese aquí el nombre del pagador.
                PayUParameters::PAYER_NAME => $params['transaction']['creditCard']['name'],
                //Ingrese aquí el email del pagador.
                PayUParameters::PAYER_EMAIL => $params['transaction']['payer']['emailAddress'],
                //Ingrese aquí el teléfono de contacto del pagador.
                PayUParameters::PAYER_CONTACT_PHONE => $params['transaction']['payer']['contactPhone'],
                //Ingrese aquí el documento de contacto del pagador.
                PayUParameters::PAYER_DNI => $params['transaction']['payer']['dniNumber'],
                //PayUParameters::PAYER_DNI => '98787676',
                //OPCIONAL fecha de nacimiento del pagador YYYY-MM-DD, importante para autorización de pagos en México.
                //PayUParameters::PAYER_BIRTHDATE => '1980-06-22',

                //Ingrese aquí la dirección del pagador.
                PayUParameters::PAYER_STREET => $params['transaction']['payer']['billingAddress']['street1'],
                PayUParameters::PAYER_STREET_2 => $params['transaction']['payer']['billingAddress']['street2'],
                PayUParameters::PAYER_CITY => $params['transaction']['payer']['billingAddress']['city'],
                PayUParameters::PAYER_STATE => $params['transaction']['payer']['billingAddress']['state'],
                PayUParameters::PAYER_COUNTRY => "PE",
                PayUParameters::PAYER_POSTAL_CODE => "00000",
                PayUParameters::PAYER_PHONE => $params['transaction']['payer']['billingAddress']['phone'],

                // -- Datos de la tarjeta de crédito --
                //Ingrese aquí el número de la tarjeta de crédito
                PayUParameters::CREDIT_CARD_NUMBER => $params['transaction']['creditCard']['number'],
                //Ingrese aquí la fecha de vencimiento de la tarjeta de crédito
                PayUParameters::CREDIT_CARD_EXPIRATION_DATE => $params['transaction']['creditCard']['expirationDate'],
                //Ingrese aquí el código de seguridad de la tarjeta de crédito
                PayUParameters::CREDIT_CARD_SECURITY_CODE=> $params['transaction']['creditCard']['securityCode'],
                //Ingrese aquí el nombre de la tarjeta de crédito
                //VISA||MASTERCARD
                PayUParameters::PAYMENT_METHOD => $params['transaction']['paymentMethod'],

                //Ingrese aquí el número de cuotas.
                PayUParameters::INSTALLMENTS_NUMBER => $params['transaction']['extraParameters']['INSTALLMENTS_NUMBER'],
                //Ingrese aquí el nombre del pais.
                PayUParameters::COUNTRY => PayUCountries::PE,

                //Session id del device.
                PayUParameters::DEVICE_SESSION_ID => $params['transaction']['deviceSessionId'],
                //IP del pagadador
                PayUParameters::IP_ADDRESS => $params['transaction']['ipAddress'],
                //Cookie de la sesión actual.
                PayUParameters::PAYER_COOKIE=>$params['transaction']['cookie'],
                //Cookie de la sesión actual.
                PayUParameters::USER_AGENT=>$params['transaction']['userAgent'],
                PayUParameters::NOTIFY_URL=>$params['transaction']['order']['notifyUrl'],
            );

        //Mage::log('Payu data');
        $parameters_log = $parameters;

        $parameters_log['creditCardNumber'] = substr($parameters_log['creditCardNumber'],0,-5).'*****';
        $parameters_log['creditCardSecurityCode'] = '***';
        Mage::log($parameters_log,null,$this->getCode().'.log');
        $response = false;

        $response = PayUPayments::doAuthorizationAndCapture($parameters);



        /*if($response) {
            $response->transactionResponse->orderId;
            $response->transactionResponse->transactionId;
            $response->transactionResponse->state;
            if($response->transactionResponse->state=="PENDING") {
                $response->transactionResponse->pendingReason;
            }
            $response->transactionResponse->paymentNetworkResponseCode;
            $response->transactionResponse->paymentNetworkResponseErrorMessage;
            $response->transactionResponse->trazabilityCode;
            $response->transactionResponse->authorizationCode;
            $response->transactionResponse->responseCode;
            $response->transactionResponse->responseMessage;


        }*/
        Mage::log($response,null,$this->getCode().'.log');
        return $response;
    }

    public function getPayuUrl()
    {
        return Mage::getStoreConfig( 'payment/payutarjetacredito/payu_url');
    }

}
