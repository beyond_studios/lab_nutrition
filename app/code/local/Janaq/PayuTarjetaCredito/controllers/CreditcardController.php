<?php
class Janaq_PayuTarjetaCredito_CreditcardController extends Mage_Core_Controller_Front_Action {

	public function paymentAction() {
			$this->_redirect('checkout/onepage/success');
	}

	public function notifyAction()
	{
		$merchant_id = Mage::getStoreConfig( 'payment/payutarjetacredito/merchant_id' );
		$secure_key = Mage::getStoreConfig( 'payment/payutarjetacredito/secure_key' );

		$params = array();
		$reference_sale = $_POST['reference_sale'];
		$state_pol = $_POST['state_pol'];
		$transaction_id = $_POST['transaction_id'];
		$signature = $_POST['sign'];
		$currency = $_POST['currency'];
		$value = $_POST['value'];
		$payment_method_name = $_POST['payment_method_name'];

		$split = explode('.', $value);
		$decimals = $split[1];
		if ($decimals % 10 == 0) {
			$value = number_format($value, 1, '.', '');
		}

		$return_message = $_POST['response_message_pol'];

		$hash_signature = md5($secure_key . '~' . $merchant_id . '~' . $reference_sale . '~' . $value . '~' . $currency . '~' . $state_pol );
		$order = Mage::getModel('sales/order')->loadByIncrementId($reference_sale);
		if ($order->getId()) {
			if(($state_pol == 4 || $state_pol == 7) && $hash_signature == $signature){

				$order->getPayment()->setTransactionId($transaction_id);

				$order_comment = $return_message;
				foreach($_POST as $key=>$value){
					$order_comment .= "<br/>$key: $value";
				}

				if($state_pol == 4){
					$order->getPayment()->registerCaptureNotification( $_POST['value'] );
					if 	($payment_method_name == 'PAGOEFECTIVO'){
						$order->addStatusToHistory('confirmado_pagoefectivo', $order_comment);
					}elseif ($payment_method_name == 'BCP') {
						$order->addStatusToHistory('confirmado_pago_banco', $order_comment);
					}else {
						$order->addStatusToHistory('confirmado_tarjeta_credito', $order_comment);
					}
					$params['order'] = $order;
					Mage::dispatchEvent('order_servientrega_integrador_save', $params);
					Mage::dispatchEvent('order_urbaner_integrador_save', $params);
					Mage::dispatchEvent('order_alldaydelivery_integrador_save', $params);

				}elseif($state_pol == 7){
					//$order->addStatusToHistory('pending', $order_comment);

					if 	($payment_method_name == 'PAGOEFECTIVO'){
						$order->addStatusToHistory('pending_pagoefectivo', $order_comment);
					}elseif ($payment_method_name == 'BCP') {
						$order->addStatusToHistory('pending_pago_banco', $order_comment);
					}else {
						$order->addStatusToHistory('pending_tarjeta_credito', $order_comment);
					}
				}

				//$order->addStatusToHistory($order->getStatus(), $order_comment);

				$order->save();

			} else {
					$order->cancel();
	        		$order->addStatusToHistory($order->getStatus(), $return_message);
					$order->save();
			}
		}


		exit;

	}

}
