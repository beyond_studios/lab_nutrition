<?php
/**
 * @category Janaq
 * @package Janaq_CartPopup
 * @author Janaq <https://www.janaq.com/>
 */
class Janaq_CartPopup_Block_Cartpopup extends Mage_Checkout_Block_Cart_Crosssell
{
    protected $_helper;
    protected $_cartItems = array();
    protected $_itemsArray = array();
    protected $quoteSubtotal = 0;

    public function __construct()
    {
        parent::__construct();

        $this->_helper = Mage::helper('jnqcartpopup');
        $this->_itemsArray = $this->_helper->getItemsArray();
        if ($this->_itemsArray) {
            $quote = $this->getQuote();

            $this->quoteSubtotal = $quote->getSubtotal();

            foreach ($quote->getAllItems() as $item) {
                $this->_cartItems[] = $item->getProductId();
            }
        }
    }

    public function getCartItems()
    {
        $cart = Mage::getModel('checkout/cart')->getQuote();
        $skus_allow = [];
        foreach ($cart->getAllItems() as $item) {     

            $pop = $item->getProduct();

            $skus_allow[$pop->getSku()] = $pop->getSku();
            if(!$pop->canConfigure()){
                //get also parent
                $sku = $pop->getId();
                $parentIds = Mage::getResourceSingleton('catalog/product_type_configurable')
                  ->getParentIdsByChild($sku);
                foreach($parentIds as $id){
                    $product_parent = Mage::getModel('catalog/product')->load($id);
                    $p_sku = $product_parent->getSku();
                    $skus_allow[$p_sku] = $p_sku;
                }
            }

        }

        $for_items = [];

        foreach($this->_itemsArray as $item){
            if(count($item['in_skus'])<=0){
                //if empty add to for_items
                $for_items[] = $item;
            }else{
                $skus = $item['in_skus'];
                foreach(explode(',',$skus) as $sku){
                    if(array_key_exists($sku, $skus_allow)){
                        $for_items[] = $item;
                    }

                }
            }
        }

        $this->_itemsArray = $for_items;
        $collection = array();
        if ($this->_itemsArray) {
            $skus = array_column($this->_itemsArray, 'sku');

            $collection = Mage::getModel('catalog/product')
                ->getCollection()
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('sku', array('in' => $skus))
                ->addAttributeToFilter('status', 1);

            //Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);
            Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);

            $collection->load();
        }
        return $collection;
    }

    public function getCrosssellSkus()
    {
        $crosssell_skus = $this->getData('crosssell_skus');
        if (is_null($crosssell_skus)) {
            $crosssell_skus = array();
            foreach ($this->getItems() as $_item) {
                $crosssell_skus[] = $_item->getSku();
            }
            $this->setData('crosssell_skus', $crosssell_skus);
        }
        return $crosssell_skus;
    }

    public function getConfigItems()
    {
        $items = array();
        foreach ($this->_itemsArray as $item) {
            $items[$item['sku']] = $item;
        }
        return $items;
    }

    /**
     * Checking product exist in Quote Items Array
     *
     * @param int $productId
     * @return bool
     */
    public function hasProductId($productId)
    {
        return in_array($productId, $this->_cartItems);
    }

    public function getCartPopupShowForAll($product, $item)
    {
        $valid = $this->quoteSubtotal >= $item['subtotal'] && !$this->hasProductId($product->getId());
        if ($valid && $item['show_forall'] == 'all') {
            return true;
        } else {
            $crosssell_skus = $this->getCrosssellSkus();
            if ($crosssell_skus) {
                $_sku = "";
                if($product->getTypeId() == "simple") {
                    if ($product->isVisibleInSiteVisibility()) {
                        $_sku = $product->getSku();
                    }
                    else {
                        $parentIds = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($product->getId());
                        if(isset($parentIds[0])){
                            $parent = Mage::getModel('catalog/product')->load($parentIds[0]);
                            $_sku = $parent->getSku();
                        }
                    }
                }
                else {
                    $_sku = $product->getSku();
                }
                return $valid && in_array($_sku, $crosssell_skus);
            }
            return false;
        }
    }
}