<?php
class Janaq_CartPopup_Block_Config_Cartpopup 
    extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
    protected $_itemRenderer;
    protected $_textareaRenderer;

    public function _prepareToRender()
    {
        $this->addColumn('sku', array(
            'label' => Mage::helper('adminhtml')->__('Sku'),
            'style' => 'width:200px',
            'class' => 'required-entry',

        ));
        $this->addColumn('image', array(
            'label' => Mage::helper('adminhtml')->__('Image'),
            'style' => 'width:220px'
        ));
         $this->addColumn('in_skus', array(
            'label' => Mage::helper('adminhtml')->__('Mostrar si skus estan'),
            'style' => 'width:140px'
        ));
        $this->addColumn('ctdad', array(
            'label' => Mage::helper('adminhtml')->__('Qty'),
            'style' => 'width:60px',
            'class' => 'required-entry validate-number',
        ));
        $this->addColumn('show_forall', array(
            'label' => Mage::helper('adminhtml')->__('Mostrar para'),
            'style' => 'width:100px',
            'renderer' => $this->_getRenderer(),
        ));
        $this->addColumn('subtotal', array(
            'label' => Mage::helper('adminhtml')->__('Subtotal'),
            'class' => 'required-entry validate-zero-or-greater',
            'style' => 'width:60px',
        ));

        $this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper('adminhtml')->__('Add');
    }

    protected function _getRenderer()
    {
        if (!$this->_itemRenderer) {
            $this->_itemRenderer = $this->getLayout()->createBlock(
                'jnqcartpopup/config_adminhtml_form_field_producttypes', '',
                array('is_render_to_js_template' => true)
            );
        }
        return $this->_itemRenderer;
    }

    protected function _getTextareaRenderer()
    {
         if (!$this->_textareaRenderer) {

            $this->_textareaRenderer = $this->getLayout()->createBlock(
                'jnqcartpopup/config_adminhtml_form_field_textarea', '',
                 array('is_render_to_js_template' => true)
            );

        }
        return $this->_textareaRenderer;
    }
 
    protected function _prepareArrayRow(Varien_Object $row)
    {
        $row->setData(
            'option_extra_attr_' . $this->_getRenderer()
                ->calcOptionHash($row->getData('show_forall')),
            'selected="selected"'
        );
    }
}
