<?php

class Janaq_CartPopup_Model_Source_Producttype
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'all', 'label'=>Mage::helper('adminhtml')->__('All')),
            array('value' => 'simple', 'label'=>Mage::helper('adminhtml')->__('Simple')),
        );
    }

    public function toArray()
    {
        return array(
            'all' => Mage::helper('adminhtml')->__('All'),
            'simple' => Mage::helper('adminhtml')->__('Simple'),
        );
    }

}
