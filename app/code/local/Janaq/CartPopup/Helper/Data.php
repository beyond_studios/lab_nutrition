<?php

class Janaq_CartPopup_Helper_Data extends Mage_Core_Helper_Abstract
{
	const CART_POPUP_STATUS = "jnqadmin_config/cartpopup/enabled";
    const CART_POPUP_SKU = "jnqadmin_config/cartpopup/sku";
	const CART_POPUP_IN_SKUS = "jnqadmin_config/cartpopup/in_skus";
    const CART_POPUP_QTY = "jnqadmin_config/cartpopup/qty";
    const CART_POPUP_SHOW_FORALL = "jnqadmin_config/cartpopup/show_forall";
	const CART_POPUP_FROM_DATE = "jnqadmin_config/cartpopup/from_date";
	const CART_POPUP_FROM_TIME = "jnqadmin_config/cartpopup/from_time";
    const CART_POPUP_TO_DATE = "jnqadmin_config/cartpopup/to_date";
    const CART_POPUP_TO_TIME = "jnqadmin_config/cartpopup/to_time";
    const CART_POPUP_CSS = "jnqadmin_config/cartpopup/css";
    const CART_POPUP_POPUPIMAGE = "jnqadmin_config/cartpopup/popupimage_banner";
    const CART_POPUP_SUBTOTAL = "jnqadmin_config/cartpopup/subtotal";
    const CART_POPUP_BUTTONTEXT = "jnqadmin_config/cartpopup/button_text";

    protected $_items;

	public function isEnable()
	{
		return (bool)Mage::getStoreConfig(self::CART_POPUP_STATUS, Mage::app()->getStore());
	}
    public function getCartPopupStatus()
    {
        if ($this->isEnable()) {
            $to_date = str_replace('/', '-', $this->getCartPopupToDate());
            $to_time = explode(',',$this->getCartPopupToTime());

            $from_date = str_replace('/', '-', $this->getCartPopupFromDate());
            $from_time = explode(',',$this->getCartPopupFromTime());

            $to_hour =  $to_time[0]; $to_min = $to_time[1]; $to_sug = $to_time[2];
            $from_hour =  $from_time[0]; $from_min = $from_time[1]; $from_sug = $from_time[2];

            $to_date = $to_date.' '.$to_hour.':'.$to_min.':'.$to_sug;
            $to_strtotime = strtotime($to_date);

            $from_date = $from_date.' '.$from_hour.':'.$from_min.':'.$from_sug;
            $from_strtotime = strtotime($from_date);

            $date_today = Mage::getModel('core/date')->date('Y-m-d H:i:s');

            $today_strtotime = strtotime($date_today);

            if ($from_strtotime < $today_strtotime && $to_strtotime > $today_strtotime) {
                return true;
            } else{
                return false;
            }
            
        } else {
            return false;
        }
    }
    
    public function getCartPopupCss()
    {
        return '<style type="text/css">'.Mage::getStoreConfig(self::CART_POPUP_CSS).'</style>';
    }
    public function getCartPopupSku()
    {
 		return Mage::getStoreConfig(self::CART_POPUP_SKU);
    }
    public function getCartPopupQty()
    {
 		return Mage::getStoreConfig(self::CART_POPUP_QTY);
    }
    public function getCartPopupButtonText()
    {
        return Mage::getStoreConfig(self::CART_POPUP_BUTTONTEXT, Mage::app()->getStore());
    }
    public function getCartPopupImagePopup()
    {
        $uploadDir = Mage::getBaseDir('media') . DIRECTORY_SEPARATOR . 'cartpopup' . DIRECTORY_SEPARATOR;
        $image_url = Mage::getStoreConfig(self::CART_POPUP_POPUPIMAGE);
        if (file_exists($uploadDir . $image_url)) {
            return Mage::getBaseUrl('media') .'cartpopup/'. $image_url;
        }
        else {
            return false;
        }
    }
    public function getCartPopupSubtotal()
    {
 		return Mage::getStoreConfig(self::CART_POPUP_SUBTOTAL);
    }
    public function getItemsArray()
    {
        if ($this->getCartPopupStatus()) {
            if (!$this->_items) {
                $this->_items = unserialize(Mage::getStoreConfig('jnqadmin_config/cartpopup/items_array'));
            }
            return $this->_items;            
        }
        return false;
    }
    public function getCartPopupToDate()
    {
 		return Mage::getStoreConfig(self::CART_POPUP_TO_DATE, Mage::app()->getStore());
    }
    public function getCartPopupToTime()
    {
        return Mage::getStoreConfig(self::CART_POPUP_TO_TIME, Mage::app()->getStore());
    }
    public function getCartPopupFromDate()
    {
        return Mage::getStoreConfig(self::CART_POPUP_FROM_DATE, Mage::app()->getStore());
    }
    public function getCartPopupFromTime()
    {
        return Mage::getStoreConfig(self::CART_POPUP_FROM_TIME, Mage::app()->getStore());
    }

}