<?php
/**
 * @category   Janaq
 * @package    Janaq_Motoboy
 * @author     contacto@janaq.com
 
 */
class Janaq_Motoboy_Helper_Data extends Mage_Core_Helper_Abstract
{



    // AUTH SERVICES
    const TIME_FROM = "carriers/motoboy/time_from";
    const TIME_TO = "carriers/motoboy/time_to";
    const DISABLED_DAYS = "carriers/motoboy/disabled_days";
    const HOLIDAYS = "carriers/motoboy/holidays";
    const TERMS = "carriers/motoboy/terms";
    const URL_DISTRICTS = "carriers/motoboy/url_districts";
 



    public function getTimeFrom()
    {
        return Mage::getStoreConfig(self::TIME_FROM, Mage::app()->getStore());
    }
    public function getTimeTo()
    {
        return Mage::getStoreConfig(self::TIME_TO, Mage::app()->getStore());
    }
    public function getDisabledDays()
    {
        return Mage::getStoreConfig(self::DISABLED_DAYS, Mage::app()->getStore());
    }
    public function getHolidays()
    {
        return Mage::getStoreConfig(self::HOLIDAYS, Mage::app()->getStore());
    }
    public function getTerms()
    {
        return Mage::getStoreConfig(self::TERMS, Mage::app()->getStore());
    }
    public function getUrlDistricts()
    {
        return Mage::getStoreConfig(self::URL_DISTRICTS, Mage::app()->getStore());
    }
    


   

}