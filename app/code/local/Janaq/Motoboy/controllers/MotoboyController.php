<?php
/**
 * Created by PhpStorm.
 * User: HOME
 * Date: 12/12/2016
 * Time: 11:13
 */
class Janaq_Motoboy_MotoboyController extends Mage_Adminhtml_Controller_Action
{

    public function exportmotoboyAction()
    {
        $fileName   = 'motoboy.csv';

        $gridBlock  = $this->getLayout()->createBlock('motoboy/carrier_grid');
        $website    = Mage::app()->getWebsite($this->getRequest()->getParam('website'));

        $gridBlock->setWebsiteId($website->getId());
        $content    = $gridBlock->getCsvFile();
        $this->_prepareDownloadResponse($fileName, $content);

    }

    protected function _isAllowed()
    {
        return true;
    }

}