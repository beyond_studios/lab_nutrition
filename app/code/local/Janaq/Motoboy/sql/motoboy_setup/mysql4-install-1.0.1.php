<?php

$installer = $this;
/** @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

/**
 * Create table 'motoboy/tablerate'
 */

$installer->addAttribute("order", "motoboy_addresses", array("type"=>"text"));
$installer->addAttribute("quote", "motoboy_addresses", array("type"=>"text"));

$installer->addAttribute("order", "motoboy_lat_lng", array("type"=>"varchar"));
$installer->addAttribute("quote", "motoboy_lat_lng", array("type"=>"varchar"));

$table = $installer->getConnection()
    ->newTable($installer->getTable('motoboy/motoboyrate'))
    ->addColumn('pk', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Primary key')
    ->addColumn('website_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => false,
        'default'   => '0',
        ), 'Website Id')
    ->addColumn('dest_country_id', Varien_Db_Ddl_Table::TYPE_TEXT, 4, array(
        'nullable'  => false,
        'default'   => '0',
        ), 'Destination coutry ISO/2 or ISO/3 code')
    ->addColumn('dest_region_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => false,
        'default'   => '0',
        ), 'Destination Region Id')
    ->addColumn('dest_zip', Varien_Db_Ddl_Table::TYPE_TEXT, 50, array(
        'nullable'  => false,
        'default'   => '*',
        ), 'Destination Post Code (Zip)')
    ->addColumn('weight_base', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4', array(
        'nullable'  => false,
        'default'   => '0.0000',
        ), 'Rate Weight Base')
    ->addColumn('price', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4', array(
        'nullable'  => false,
        'default'   => '0.0000',
        ), 'Price')
    ->addColumn('volume', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4', array(
        'nullable'  => false,
        'default'   => '0.0000',
    ), 'Volume')
    ->addColumn('delivery_time',Varien_Db_Ddl_Table::TYPE_TEXT,10,array(
        'nullable' => false,
        'default'  =>'0'
        ),'Delivery time hours')
    ->addIndex($installer->getIdxName('motoboy/motoboyrate', array('website_id', 'dest_country_id', 'dest_region_id', 'dest_zip'), Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE),
        array('website_id', 'dest_country_id', 'dest_region_id', 'dest_zip'), array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE))
    ->setComment('Motoboy Rate');
$installer->getConnection()->createTable($table);


$installer->endSetup();