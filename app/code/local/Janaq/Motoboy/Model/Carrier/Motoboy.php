<?php
/**
 * Created by PhpStorm.
 * User: HOME
 * Date: 12/12/2016
 * Time: 22:52
 */

class Janaq_Motoboy_Model_Carrier_Motoboy
    extends Mage_Shipping_Model_Carrier_Abstract
    implements Mage_Shipping_Model_Carrier_Interface
{
    protected $_code = 'motoboy';

    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        if (!Mage::getStoreConfig('carriers/'.$this->_code.'/active')) {
            return false;
        }
        $storeId = Mage::app()->getStore()->getStoreId();
        $freeQty = 0;
        $volumeTotal = 0;
        //Mage::log($coordenadas_mapa);
        if ($request->getAllItems()) {
            $freePackageValue = 0;
            foreach ($request->getAllItems() as $item) {
                if ($item->getProduct()->isVirtual() || $item->getParentItem()) {
                    continue;
                }

                if ($item->getHasChildren() && $item->isShipSeparately()) {
                    foreach ($item->getChildren() as $child) {
                        if ($child->getFreeShipping() && !$child->getProduct()->isVirtual()) {
                            $freeShipping = is_numeric($child->getFreeShipping()) ? $child->getFreeShipping() : 0;
                            $freeQty += $item->getQty() * ($child->getQty() - $freeShipping);
                        }
                    }
                } elseif ($item->getFreeShipping()) {
                    $freeShipping = is_numeric($item->getFreeShipping()) ? $item->getFreeShipping() : 0;
                    $freeQty += $item->getQty() - $freeShipping;
                    $freePackageValue += $item->getBaseRowTotal();
                }
                if ($item->getHasChildren()) {
                    foreach ($item->getChildren() as $child) {
                        //Mage::log($child->getProduct()->getId());
                        //Mage::log($child->getProduct()->getManufacturerProdOrder());
                        $high = (double)Mage::getResourceModel('catalog/product')->getAttributeRawValue($child->getProduct()->getId(), 'high', $storeId);
                        $long = (double)Mage::getResourceModel('catalog/product')->getAttributeRawValue($child->getProduct()->getId(), 'long', $storeId);
                        $width = (double)Mage::getResourceModel('catalog/product')->getAttributeRawValue($child->getProduct()->getId(), 'width', $storeId);
                        $high = ($high == '') ? 0 : $high ;
                        $long = ($long == '') ? 0 : $long ;
                        $width = ($width == '') ? 0 : $width ;
                        $volume = $high*$long*$width;
                        $volumeTotal += $volume;
                    }
                }else{
                    //simple
                    $high = (double)Mage::getResourceModel('catalog/product')->getAttributeRawValue($item->getProduct()->getId(), 'high', $storeId);
                    $long = (double)Mage::getResourceModel('catalog/product')->getAttributeRawValue($item->getProduct()->getId(), 'long', $storeId);
                    $width = (double)Mage::getResourceModel('catalog/product')->getAttributeRawValue($item->getProduct()->getId(), 'width', $storeId);
                    $high = ($high == '') ? 0 : $high ;
                    $long = ($long == '') ? 0 : $long ;
                    $width = ($width == '') ? 0 : $width ;
                    $volume = $high*$long*$width;
                    $volumeTotal += $volume;
                }
            }
            $volumeTotal = number_format($volumeTotal, 6, '.', '');
            //Mage::log('volumen total: '.$volumeTotal);

            $oldValue = $request->getPackageValue();
            $request->setPackageValue($oldValue - $freePackageValue);
        }

        $oldWeight = $request->getPackageWeight();
        $oldQty = $request->getPackageQty();

        //$request->setPackageWeight($request->getFreeMethodWeight());
        $request->setPackageQty($oldQty - $freeQty);
        $request->setVolume($volumeTotal);

        $result = $this->_getModel('shipping/rate_result');
        $rate = $this->getRate($request);
        // send value Volume ..
        $request->setPackageWeight($oldWeight);
        $request->setPackageQty($oldQty);

        $coordenadas_mapa = $this->getCordenadasMap($request->getDestPostcode(),$rate['id_mapa_motoboy']);
        $request->setCoordinates($coordenadas_mapa);
        $destStreet = str_replace("\n", " ", $request->getDestStreet());

        //Mage::log($rate['id_mapa_motoboy']);
        //Mage::log($request->getIdMapaMotoboy());
        if (!empty($rate) && $rate['price'] >= 0) {
            $method = $this->_getModel('shipping/rate_result_method');

            $method->setCarrier($this->_code);
            $method->setCarrierTitle($this->getConfigData('title'));

            $method->setMethod($this->_code);
            $method->setMethodTitle($this->getConfigData('name'));

            if ($request->getFreeShipping() === true || ($request->getPackageQty() == $freeQty)) {
                $shippingPrice = 0;
            } else {
                $shippingPrice = $this->getFinalPriceWithHandlingFee($rate['price']);
            }

            $method->setPrice($shippingPrice);
            $method->setCost(0);
            $method->setDeliveryTime($this->CalculoTiempoEntrega($rate['delivery_time']));
            $method->setVolume($volumeTotal);
            $method->setCoordinates($coordenadas_mapa);
            //Mage::log('Address before :'.$request->getDestStreet());
            $method->setAddressShipping($destStreet);

            $result->append($method);
        } elseif (empty($rate) && $request->getFreeShipping() === true) {
            /**
             * was applied promotion rule for whole cart
             * other shipping methods could be switched off at all
             * we must show table rate method with 0$ price, if grand_total more, than min table condition_value
             * free setPackageWeight() has already was taken into account
             */
            $request->setPackageValue($freePackageValue);
            $request->setPackageQty($freeQty);

            $rate = $this->getRate($request);
            if (!empty($rate) && $rate['price'] >= 0) {
                $method = $this->_getModel('shipping/rate_result_method');

                $method->setCarrier($this->_code);
                $method->setCarrierTitle($this->getConfigData('title'));

                $method->setMethod($this->_code);
                $method->setMethodTitle($this->getConfigData('name'));

                $method->setPrice(0);
                $method->setCost(0);
                $method->setDeliveryTime($this->CalculoTiempoEntrega($rate['delivery_time']));
                $method->setVolume($volumeTotal);
                $method->setAddressShipping($destStreet);
                $method->setCoordinates($coordenadas_mapa);
                $result->append($method);
            }
        }else {
            $error = $this->_getModel('shipping/rate_result_error');
            $error->setCarrier($this->_code);
            $error->setCarrierTitle($this->getConfigData('title'));
            $error->setErrorMessage($this->getConfigData('specificerrmsg'));
            $result->append($error);
        }
        return $result;

    }
    protected function _getModel($modelName)
    {
        return Mage::getModel($modelName);
    }
    public function getRate(Mage_Shipping_Model_Rate_Request $request)
    {
        return Mage::getResourceModel('motoboy/motoboyrate')->getRate($request);
    }

    public function getAllowedMethods()
    {
        return array('standar' => $this->getConfigData('name'));
    }
    public function getCordenadasMap($postcode,$id_mapa,$static = false){

        $array_distrito = array();
        $array_distrito['nombre'] = '';
        $array_distrito['center'] = array();
        $array_distrito['coordenadas'] = array();
        $motoboy = Mage::helper('motoboy');

        if (!$static) {
            $url_districts = $motoboy->getUrlDistricts();

            $cache = Mage::app()->getCache();
            $body = $cache->load("MOTOMAPS_districts");
            
            

            if ($body === false) {
                $client = new \Zend_Http_Client($url_districts);
                $client->setParameterPost(array());
                $response = $client->request('GET');
                if ($response->isSuccessful()) {
                    $body = json_decode($response->getBody());
                    $cacheLifetime = $this->getData('cachelifetime');
                    $cache->save($response->getBody(), "MOTOMAPS_districts", array("MOTOMAPS"), $cacheLifetime);
                }
            }else{
                $body = json_decode($body);
            }
            foreach ($body->distritos as $district) {
                if (trim((int)$id_mapa) == trim((int)$district->coordenadas->distrito_id)) {
                    $array_distrito['nombre'] = (string)$postcode;
                    $coordinate = json_decode((string)$district->coordenadas->valor,true);
                    $array_distrito['center'] = array((string)$coordinate[0]['lng'],(string)$coordinate[0]['lat']);
                    $array_distrito['coordenadas'] = (array)$coordinate;
                    break;
                }

            }

        }else{

            $xml = simplexml_load_file(Mage::getBaseUrl().'distritos.xml');
            foreach ($xml->Document[0]->Folder[0]->Placemark as $placemark) 
            {
                if (trim((string)$placemark->name) == trim($postcode)) {
                    $array_distrito['nombre'] = (string)$placemark->name;
                    $array_distrito['center'] = explode(",",(string)$placemark->LineString[0]->center);
                    $array_distrito['coordenadas'] = $this->getLatLong((string)$placemark->LineString[0]->coordinates);
                    break;
                }
                
            } 
        }
    
    //Mage::log($array_distrito);
    
    return $array_distrito;

    }

    protected function getLatLong($text){
        $text = str_replace(' ', '', $text);
        $text = mberegi_replace("[\n|\r|\n\r]", '', $text);
        $aux = explode(",0", $text);
        array_pop($aux);
        $response[] = array();$i=0;
       foreach ($aux as $value) {
            $aux_ = explode(",", $value);
            $response[$i]['lat'] = $aux_[1];
            $response[$i]['lng'] = $aux_[0];
            $i++;
        } 

        return $response;
    }

    protected function CalculoTiempoEntrega($time_delivery){
        $response = '';
        $motoboy = Mage::helper('motoboy');
        $time_from = explode(",", $motoboy->getTimeFrom());
        $time_from = implode(":", $time_from);

        $time_to = explode(",", $motoboy->getTimeTo());
        $time_to = implode(":", $time_to);

        $holidays = explode(",", $motoboy->getHolidays());
        $disabled_days = explode(",", $motoboy->getDisabledDays());

        $server_hours = Mage::getModel('core/date')->date('H:i:s');
        $current_date = Mage::getModel('core/date')->date('d/m');
        $server_week_day = Mage::getModel('core/date')->date('w');
        $str_server_hours = strtotime($server_hours);
        $str_time_from = strtotime($time_from);
        $str_time_to = strtotime($time_to);

        if ($str_server_hours > $str_time_to) {
            $response = 'El pedido sera entregado al siguiente día hábil.';
        } elseif ($str_server_hours < $str_time_from){
            $response = 'En un máximo de <b>'.$time_delivery .'</b> horas a partir del horario hábil';
        } else {
            $response = 'En un máximo de <b>'.$time_delivery .'</b> horas.';
        }
        if (in_array($server_week_day, $disabled_days)) {
            $response = 'El pedido sera entregado al siguiente día hábil.';
        }
        if (in_array($current_date, $holidays)) {
            $response = 'El pedido sera entregado al siguiente día hábil.';
        }
        return $response;
    }

}