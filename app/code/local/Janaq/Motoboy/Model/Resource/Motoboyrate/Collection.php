<?php
/**
 * Created by PhpStorm.
 * User: HOME
 * Date: 11/12/2016
 * Time: 21:38
 */

class Janaq_Motoboy_Model_Resource_Motoboyrate_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    protected $_shipTable;

    protected $_countryTable;

    protected $_regionTable;

    public function _construct()
    {

        parent::_construct();
        $this->_init('motoboy/motoboyrate');
        $this->_shipTable       = $this->getMainTable();
        $this->_countryTable    = $this->getTable('directory/country');
        $this->_regionTable     = $this->getTable('directory/country_region');

    }
    public function setWebsiteFilter($websiteId)
    {
        return $this->addFieldToFilter('website_id', $websiteId);
    }
    public function _initSelect()
    {
        parent::_initSelect();

        $this->_select
            ->joinLeft(
                array('country_table' => $this->_countryTable),
                'country_table.country_id = main_table.dest_country_id',
                array('dest_country' => 'iso3_code'))
            ->joinLeft(
                array('region_table' => $this->_regionTable),
                'region_table.region_id = main_table.dest_region_id',
                array('dest_region' => 'code'));

        $this->addOrder('dest_country', self::SORT_ORDER_ASC);
        $this->addOrder('dest_region', self::SORT_ORDER_ASC);
        $this->addOrder('dest_zip', self::SORT_ORDER_ASC);
    }
    public function setCountryFilter($countryId)
    {
        return $this->addFieldToFilter('dest_country_id', $countryId);
    }

}