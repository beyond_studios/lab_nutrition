<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 15/12/2016
 * Time: 12:12 PM
 */

class Janaq_Motoboy_Model_Observer extends Mage_Payment_Model_Method_Abstract
{

    public function afterSave(Varien_Event_Observer $event)
    {
        $params = Mage::app()->getRequest()->getParams();
        
        $shipping_method = $params['shipping_method'];

        if ($shipping_method == 'motoboy_motoboy' ) {

            $address = $params["motoboy"]["address"];
            $lat_lng = $params["motoboy"]["lat_lng"];
            $order = $event->getOrder();
            $order->setData('motoboy_addresses',$address);
            $order->setData('motoboy_lat_lng', $lat_lng);

        }
        //Mage::log($params);
        

    }


   
}