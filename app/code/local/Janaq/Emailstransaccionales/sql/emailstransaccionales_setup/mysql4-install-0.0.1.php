<?php

$installer = $this;
/** @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

$installer->addAttribute("order", "email_flag", array("type"=>"int", "default"=>"0"));
$installer->addAttribute("quote", "email_flag", array("type"=>"int", "default"=>"0"));

$installer->endSetup();