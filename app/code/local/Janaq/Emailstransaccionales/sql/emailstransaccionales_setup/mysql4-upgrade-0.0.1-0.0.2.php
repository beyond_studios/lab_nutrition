<?php

$installer = $this;
/** @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();

$installer->run("

DROP TABLE IF EXISTS {$this->getTable('emailstransaccionales')};
CREATE TABLE {$this->getTable('emailstransaccionales')} (
  `emailstransaccionales_id` int(10) unsigned NOT NULL auto_increment PRIMARY KEY,
  `entity_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NULL,
  `email_status` smallint(6) NOT NULL default '0',
  `created_at` datetime NULL,
  `updated_at` datetime NULL,
  FOREIGN KEY (`entity_id`) REFERENCES sales_flat_order(`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

");

$installer->endSetup();