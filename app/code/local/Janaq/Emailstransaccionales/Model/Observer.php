<?php

class Janaq_Emailstransaccionales_Model_Observer extends Mage_Core_Model_Observer
{
    public function afterSave($observer)
    {
    	Mage::log('entra a guardar despues de orden');
        $order = $observer->getEvent()->getOrder();
        $order->setData('email_flag', 1);
        
        $items = $order->getAllVisibleItems();
        foreach ($items as $item) {
        	$email_transactional = Mage::getModel('emailstransaccionales/emailstransaccionales');
        	$email_transactional->setEntityId($order->getId());
        	$email_transactional->setCreatedAt($order->getData('created_at'));
        	$email_transactional->setUpdatedAt($order->getData('created_at'));
        	$email_transactional->setItemId($item->getData('product_id'));
        	//$email_transactional->setEmailStatus(1);
        	$email_transactional->save();
        }
    }
}