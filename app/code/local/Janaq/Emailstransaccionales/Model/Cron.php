<?php

class Janaq_Emailstransaccionales_Model_Cron
{
	/*Enviar emails*/
	public function run()
	{
		Mage::log("inicia cron janaq emails");
        //Emails para comentar
		$orders = Mage::getResourceModel('sales/order_collection')
         ->addFieldToFilter(
            'main_table.status', 
            array(
                'complete',
                'confirmado_pagoefectivo',
                'confirmado_pago_banco',
                'confirmado_tarjeta_credito')
            );
        //$orders->addFieldToFilter('email_flag', 1);
        foreach ($orders as $order) {
            if ($order->hasShipments()) {
                $this->getProductsHtml($order);
            }
        }

        //Emails para volver a comprar
        $_orders = Mage::getResourceModel('sales/order_collection')
         ->addFieldToFilter(
            'main_table.status',
            array(
                'complete',
                'confirmado_pagoefectivo',
                'confirmado_pago_banco',
                'confirmado_tarjeta_credito')
            );
        //$_orders->addFieldToFilter('email_flag', 2);
        $_orders->getSelect()->joinLeft(
            array('sfo'=>'emailstransaccionales'),'main_table.entity_id = sfo.entity_id and sfo.email_status = 0');
        //Mage::log($_orders->getSelect()->__toString());

        foreach ($_orders as $_order) {
            if ($_order->hasShipments()) {
                //$this->getRelProductsHtml($_order);
            }
    	}

	}

	function getProductsHtml($order)
	{
        $now = Mage::getModel('core/date')->date('Y-m-d');
        $days = Mage::getStoreConfig('emailstransaccionales_config/after_order/days');

        $items = $order->getAllItems();
        $sended_email = false;

        /** @var $order Mage_Sales_Model_Order */
        foreach($order->getShipmentsCollection() as $shipment) {
            /** @var $shipment Mage_Sales_Model_Order_Shipment */
            //$shipment_date = $shipment->getCreatedAt();
            $shipment_date = $order->getServientregaShippingDate();
            $send_date = date("Y-m-d", strtotime($shipment_date . " + ".$days." days"));

            if (strtotime($now) == strtotime($send_date)) {
                $orderedProductIds = array();
                foreach($items as $item){
                    $orderedProductIds[] = $item->getData('product_id');
                }
                $_collection = Mage::getModel('catalog/product')->getCollection();
                $_collection->addAttributeToSelect('*');
                $_collection->addIdFilter($orderedProductIds);
                $_collection->addUrlRewrite();
                //$_collection->addAttributeToFilter('type_id', array('configurable', 'bundle'));

                $html = "";
                $html .= "<table width='100%' cellpadding='10' cellspacing='0'>";
                foreach ($_collection as $product) {
                    if ($product->isVisibleInSiteVisibility()) {
                        $html .= "<tr><td align='center' style='padding:0px;'>";
                        $html .= "<img alt='".$product->getName()."' src='".Mage::helper('catalog/image')->init($product, 'small_image')->resize(100,100)."' width='100' height='100' border='0'>";
                        $html .= "</td>";
                        $html .= "<td style='padding-left:20px;'>";
                        $html .= "<h2>";
                        $html .= "<a href='".$product->getProductUrl()."#valoration-form' style='color: #888;font-size: 16px;font-weight: 500;text-transform: uppercase;text-decoration: none;'>".$product->getName()."</a>";
                        $html .= "</h2>";
                        $html .= "<div style='font-size:13px;'>".$product->getData('features')."</div>";
                        $html .= "<div style='margin-top: 15px;'><a style='border-radius:2px;background:#f92525;color:#fff;display:inline-block;font-size:17px;line-height:normal;padding:7px 30px;text-decoration: none;' href='".$product->getProductUrl()."#valoration-form'>Valorar Producto</a></div>";
                        $html .= "</td></tr>";
                    }
                }
                $html .= "</table>";
                Mage::log($html);

                Mage::log("Enviar correo para dejar comentario");
                $sended_email = $this->sendEmail($order, $html, 1);
                if ($sended_email) {
                    $model_order = Mage::getModel('sales/order')->load($order->getEntityId());
                    $model_order->setEmailFlag(2);
                    $model_order->save();
                }
            }

        }
	}

    function getRelProductsHtml($order)
    {
        $now = Mage::getModel('core/date')->date('Y-m-d');
        $days = Mage::getStoreConfig('emailstransaccionales_config/after_order/days');

        $items = $order->getAllItems();

        /** @var $order Mage_Sales_Model_Order */
        foreach($order->getShipmentsCollection() as $shipment) {
            $buy = false;
            /** @var $shipment Mage_Sales_Model_Order_Shipment */
            $shipment_date = $order->getServientregaShippingDate();
            $send_date = date("Y-m-d", strtotime($shipment_date . " + ".$days." days"));

            $orderedProductIds = array();
            $relatedProductIds = array();
            $dias_consumo = 0;
            foreach($items as $item){
                $orderedProductIds[] = $item->getData('product_id');
            }
            $_collection = Mage::getModel('catalog/product')->getCollection();
            $_collection->addAttributeToSelect('*');
            $_collection->addIdFilter($orderedProductIds);
            $_collection->addUrlRewrite();
            //$_collection->addAttributeToFilter('type_id', array('configurable', 'bundle'));

            $htmlArray = array();
            $i = 0;
            foreach ($_collection as $product) {
                    
                if ($product->isVisibleInSiteVisibility()) {
                    $htmlArray[$i] = "";
                    $htmlArray[$i] .= "<table width='100%' cellpadding='5' cellspacing='0'>";
                    
                    $htmlArray[$i] .= "<tr><td align='center' style='padding:0px;'>";
                    $htmlArray[$i] .= "<img alt='".$product->getName()."' href='".Mage::helper('catalog/image')->init($product, 'small_image')->resize(100,100)."' width='100' height='100' border='0'>";
                    $htmlArray[$i] .= "</td>";
                    $htmlArray[$i] .= "<td>";
                    $htmlArray[$i] .= "<h2>";
                    $htmlArray[$i] .= "<a href='".$product->getProductUrl()."' style='color: #888;font-size: 14px;'>".$product->getName()."</a>";
                    $htmlArray[$i] .= "</h2>";
                    $htmlArray[$i] .= "<div>".$product->getShortDescription()."</div>";
                    $htmlArray[$i] .= "</td></tr>";

                    if ($product->getConsumptionTime()) {
                        $dias_consumo = $product->getConsumptionTime();
                        $dias_envio = date("Y-m-d", strtotime($shipment_date . " + ".$dias_consumo." days"));
                        if (strtotime($now) == strtotime($dias_envio)) {
                            Mage::log("se enviará el email para comprar");
                            $buy = true;
                            $relatedProductIds = $product->getRelatedProductIds();

                            if (count($relatedProductIds) > 0) {
                                $htmlArray[$i] .= "<tr><td width='100%' colspan='2'>";
                                $htmlArray[$i] .= "<table width='100%' cellpadding='0' cellspacing='0'>";
                                $htmlArray[$i] .= "<tr height='35'><td>&nbsp;</td></tr>";
                                $htmlArray[$i] .= "<tr height='50'><td width='100%' align='center'>";
                                $htmlArray[$i] .= "<h2 style='line-height: 19px;font-size: 25px;font-weight: normal;'>Te recomendamos también</h2>";
                                $htmlArray[$i] .= "</td></tr>";
                                $htmlArray[$i] .= "<tr height='20'><td>&nbsp;</td></tr>";
                                $htmlArray[$i] .= "<table width='100%' cellpadding='5' cellspacing='0'>";
                                $i = 0;
                                $model = Mage::getModel('catalog/product');
                                foreach ($relatedProductIds as $id) {
                                    $related = $model->load($id);
                                    $htmlArray[$i] .= "<tr><td align='center'>";
                                    $htmlArray[$i] .= "<img alt='".$related->getName()."' href='".Mage::helper('catalog/image')->init($related, 'small_image')->resize(100,100)."' width='100' height='100' border='0'>";
                                    $htmlArray[$i] .= "</td>";
                                    $htmlArray[$i] .= "<td>";
                                    $htmlArray[$i] .= "<h2>";
                                    $htmlArray[$i] .= "<a href='".$related->getProductUrl()."' style='color: #888;font-size: 14px;'>".$related->getName()."</a>";
                                    $htmlArray[$i] .= "</h2>";
                                    $htmlArray[$i] .= "<div>".$related->getShortDescription()."</div>";
                                    $htmlArray[$i] .= "</td></tr>";
                                }
                                $htmlArray[$i] .= "<td></tr>";
                                $htmlArray[$i] .= "</table>";
                            }
                            //$this->sendEmail($order, $html, 2);
                        }
                        else{
                            $buy = false;
                        }
                    }
                    else{
                        $dias_consumo = -1;
                        Mage::log("El producto ". $producto->getName() . " no tiene tiempo consumo");
                    }
                    $htmlArray[$i] .= "</table>";
                    $i++;
                    Mage::log($htmlArray);
                    Mage::log("---------");
                }
            }

            foreach ($htmlArray as $value) {
                //$this->sendEmail($order, $value, 2);
                Mage::log("value: ".$value);
            }

        }
    }

	protected function sendEmail($model, $productsHtml, $type)
	{
        $from_email = "";
        if ($type == 1) {
            $subject = Mage::getStoreConfig('emailstransaccionales_config/after_order/subject');
            $from_email = Mage::getStoreConfig('emailstransaccionales_config/after_order/sender');
        }
        else if ($type == 2) {
            $subject = Mage::getStoreConfig('emailstransaccionales_config/after_outofstock/subject');
            $from_email = Mage::getStoreConfig('emailstransaccionales_config/after_outofstock/sender');
        }

		$email = $model->getCustomerEmail();
      	$fName = $model->getName();
      	$from_name = Mage::getStoreConfig('trans_email/ident_general/name'); //fetch sender name Admin
        try {
            $customerEmailId = $email;
            $customerFName = $fName;
            if ($type == 1) {
                Mage::log('email 1');
                $emailTemplate = Mage::getModel('core/email_template')
                    ->loadDefault('emailstransaccionales_comment');
            }
            elseif ($type == 2) {
                Mage::log('email 2');
                $emailTemplate = Mage::getModel('core/email_template')
                    ->loadDefault('emailstransaccionales_outofstock');
            }
            $emailTemplateVariables = array();
            $emailTemplateVariables['order'] = $model;
            $emailTemplateVariables['subject'] = $subject;
            $emailTemplateVariables['productsHtml'] = $productsHtml;

            $emailTemplate->setSenderEmail($from_email);
            $emailTemplate->setSenderName($from_name);
            $emailTemplate->setType('html');
            $emailTemplate->send($customerEmailId, $customerFName, $emailTemplateVariables);
            return true;
        } catch (Exception $e) {
            $errorMessage = $e->getMessage();
            return $errorMessage;
        }
	}
}