<?php
class Janaq_Emailstransaccionales_Model_Mysql4_Emailstransaccionales_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    protected function _construct()
    {
    	parent::_construct();
        $this->_init('emailstransaccionales/emailstransaccionales');
    }
}