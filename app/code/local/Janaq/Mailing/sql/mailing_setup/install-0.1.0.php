<?php

$installer = $this;
$installer->startSetup();

$tableName = $installer->getTable('mailing/mailing');
$tableNameLog = $installer->getTable('mailing/log');
try {

    $installer->getConnection()->beginTransaction();

    if ($installer->getConnection()->isTableExists($tableName) != true) {

        $table = $installer->getConnection()
            ->newTable($tableName)
            ->addColumn('mailing_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
                array(
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary' => true,
                ),
                'Mailing Id'
            )
            ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
                array(
                    'unsigned' => true,
                    'nullable' => false,
                    'default' => '0',
                ),
                'Customer Id'
            )
//            ->addColumn('customer_group_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
//                array(
//                    'unsigned' => true,
//                    'nullable' => false,
//                    'default' => '0',
//                ),
//                'Customer group Id'
//            )
            ->addColumn('mail', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50,
                array(
                    'nullable' => false
                ),
                'Mail Customer'
            )
//            ->addColumn('point', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
//                array(
//                    'nullable' => false,
//                    'default' => '0',
//                ),
//                'Point Customer'
//            )
            ->addColumn('template', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50,
                array(
                    'nullable' => false
                ),
                'Template mail'
            )
            ->addColumn('type', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
                'unsigned'  => true,
                'nullable'  => false,
            ), 'Mail Type')
            ->addColumn('params', Varien_Db_Ddl_Table::TYPE_VARCHAR, 250,
                array(),
                'Data template'
            )
            ->addColumn('send', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
                'unsigned'  => true,
                'nullable'  => false,
                'default'   => 0,
            ), 'Send mail: 0 => pendiente; 1 => enviado; 2 => no enviado')
            ->addColumn('update', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null,
                array(
                    'nullable' => false,
                ),
                'Last Update')
            ->addColumn('date_send', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null,
                array(
                    'nullable' => false,
                ),
                'Date send email')
            ->addColumn('dispatched', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null,
                array(
                    'nullable' => false,
                ),
                'Date dispatch email')
            ->addColumn('is_locked', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
                'unsigned'  => true,
                'nullable'  => false,
                'default'   => 0,
            ), 'Is Locked row: 0 => unlock ; 1 => lock')
            ->addColumn('active', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
                'unsigned'  => true,
                'nullable'  => false,
                'default'   => '1',
            ), 'Is Active')
            ->addColumn('create', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null,
                array(
                    'nullable' => false,
                ),
                'Date create row')
            ->addIndex($installer->getIdxName('mailing/mailing', array('entity_id')),
                array('entity_id'))
//            ->addIndex($installer->getIdxName('mailing/mailing', array('customer_group_id')),
//                array('customer_group_id'))
            ->addForeignKey(
                $installer->getFkName(
                    'mailing/mailing',
                    'entity_id',
                    'customer/entity',
                    'entity_id'
                ),
                'entity_id', $installer->getTable('customer/entity'), 'entity_id',
                Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
//            ->addForeignKey(
//                $installer->getFkName(
//                    'mailing/mailing',
//                    'customer_group_id',
//                    'core/website',
//                    'customer_group_id'
//                ),
//                'customer_group_id', $installer->getTable('customer/customer_group'), 'customer_group_id',
//                Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
            ->setComment('Table Mailing');

        $installer->getConnection()->createTable($table);
    }

    if ($installer->getConnection()->isTableExists($tableNameLog) != true) {

        $table = $installer->getConnection()
            ->newTable($tableNameLog)
            ->addColumn('mailing_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
                array(
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary' => true,
                ),
                'Mailing Id'
            )
            ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
                array(
                    'unsigned' => true,
                    'nullable' => false,
                    'default' => '0',
                ),
                'Customer Id'
            )
//            ->addColumn('customer_group_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
//                array(
//                    'unsigned' => true,
//                    'nullable' => false,
//                    'default' => '0',
//                ),
//                'Customer group Id'
//            )
            ->addColumn('mail', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50,
                array(
                    'nullable' => false
                ),
                'Mail Customer'
            )
//            ->addColumn('point', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
//                array(
//                    'nullable' => false,
//                    'default' => '0',
//                ),
//                'Point Customer'
//            )
            ->addColumn('template', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50,
                array(
                    'nullable' => false
                ),
                'Template mail'
            )
            ->addColumn('type', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
                'unsigned'  => true,
                'nullable'  => false,
            ), 'Mail Type')
            ->addColumn('params', Varien_Db_Ddl_Table::TYPE_VARCHAR, 250,
                array(),
                'Data template'
            )
            ->addColumn('send', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
                'unsigned'  => true,
                'nullable'  => false,
                'default'   => 0,
            ), 'Send mail: 0 => pendiente; 1 => enviado; 2 => no enviado')
            ->addColumn('update', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null,
                array(
                    'nullable' => false,
                ),
                'Last Update')
            ->addColumn('date_send', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null,
                array(
                    'nullable' => false,
                ),
                'Date send email')
            ->addColumn('dispatched', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null,
                array(
                    'nullable' => false,
                ),
                'Date dispatch email')
            ->addColumn('is_locked', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
                'unsigned'  => true,
                'nullable'  => false,
                'default'   => 0,
            ), 'Is Locked row: 0 => unlock ; 1 => lock')
            ->addColumn('active', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
                'unsigned'  => true,
                'nullable'  => false,
                'default'   => '1',
            ), 'Is Active')
            ->addColumn('create', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null,
                array(
                    'nullable' => false,
                ),
                'Date create row')
            ->addIndex($installer->getIdxName('mailing/log', array('entity_id')),
                array('entity_id'))
//            ->addIndex($installer->getIdxName('mailing/log', array('customer_group_id')),
//                array('customer_group_id'))
            ->addForeignKey(
                $installer->getFkName(
                    'mailing/log',
                    'entity_id',
                    'customer/entity',
                    'entity_id'
                ),
                'entity_id', $installer->getTable('customer/entity'), 'entity_id',
                Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
//            ->addForeignKey(
//                $installer->getFkName(
//                    'mailing/log',
//                    'customer_group_id',
//                    'core/website',
//                    'customer_group_id'
//                ),
//                'customer_group_id', $installer->getTable('customer/customer_group'), 'customer_group_id',
//                Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
            ->setComment('Table Mailing');

        $installer->getConnection()->createTable($table);
    }

    $installer->getConnection()->commit();

    $installer->endSetup();

} catch (Exception $exc) {
    $installer->getConnection()->rollback();
    echo $exc->getMessage();
    exit;
}
