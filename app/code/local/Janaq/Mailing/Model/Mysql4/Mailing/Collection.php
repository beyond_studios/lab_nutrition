<?php

class Janaq_Mailing_Model_Mysql4_Mailing_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct() 
    {
        $this->_init('mailing/mailing');
        parent::_construct();
    }

    /**
     * betweenMinutes
     *
     * @param int $min
     * @return $this
     *
     * Muestra solo los resultados dentro de un rango de minutos
     */
    public function betweenMinutes($min = 0)
    {
        $curZendDate = new Zend_Date();
        $curZendDate->sub($min, Zend_Date::MINUTE);
        $curDateTimeBefore = $curZendDate->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
        $curZendDate = new Zend_Date();
        $curZendDate->add($min, Zend_Date::MINUTE);
        $curDateTimeAfter = $curZendDate->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
        $this->getSelect()->where('date_send >= ?', $curDateTimeBefore)->where('date_send <= ?', $curDateTimeAfter);
        return $this;
    }

    /**
     * typeMail
     * @param $type
     * @return $this
     *
     * type
     * -----
     * 1 => Loyalty
     *
     */
    public function typeMail($type)
    {
        $this->getSelect()->where('type = ?', $type);
        return $this;
    }

    /**
     * flagSend
     *
     * @param int $flag
     * @return $this
     *
     * flag
     * ----
     * 0 => pendiente
     * 1 => enviado
     * 2 => fail
     */
    public function flagSend($flag = 0)
    {
        $this->getSelect()->where('send = ?', $flag);
        return $this;
    }

    /**
     * flagActive
     *
     * @param int $flag
     * @return $this
     *
     * flag
     * ----
     * 0 => registro deshabilitado
     * 1 => registro habilitado
     */
    public function flagActive($flag = 1)
    {
        $this->getSelect()->where('active = ?', $flag);
        return $this;
    }

    /**
     * lock transaction
     *
     * @param int $state
     * @return $this
     */
    public function lock($state = 1)
    {
        $onlyWhere = preg_replace('/.*(where.*)/i', '$1', $this->getSelect()->assemble());
        $lockQuery
            = "UPDATE `" . $this->getMainTable() . "` SET is_locked=" . intval($state) . " $onlyWhere";
        $this->getResource()->getReadConnection()->raw_query($lockQuery);
//        $this->_isLocked = $state;
        //Mage::log($lockQuery);
        return $this;
    }

    public function addNotLockedFilter()
    {
        $this->getSelect()->where('COALESCE(is_locked,0) < 1');

        return $this;
    }
}