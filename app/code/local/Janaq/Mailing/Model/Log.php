<?php

class Janaq_Mailing_Model_Log extends Mage_Core_Model_Abstract
{
    public function __construct() 
    {
        $this->_init('mailing/log');
        parent::__construct();
    }

    public function insertUpdateRegistryData($data)
    {
        try {
            if(!empty($data)){
                $this->setEntityId($data['entity_id']);
                $this->setMail($data['mail']);
                $this->setTemplate($data['template']);
                if(!empty($data['params'])){
                    $this->setParams($data['params']);
                }
                $this->setType($data['type']);
                $this->setSend($data['send']);
                if(!empty($data['date_send'])){
                    $this->setDateSend($data['date_send']);
                }
                if(!empty($data['dispatched'])){
                    $this->setDispatched($data['dispatched']);
                }
                $this->setIsLocked($data['is_locked']);
                $this->setActive($data['active']);
                $this->setCreate(date('Y-m-d H:i:s'));
                $this->setUpdate(date('Y-m-d H:i:s'));
            }else{
                throw new Exception('Error en procesar la petición: insuficiente data');
            }
        } catch (Exception $exc) {
            Mage::logException($exc);
        }
        return $this;
    }
}