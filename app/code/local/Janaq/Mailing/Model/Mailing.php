<?php

class Janaq_Mailing_Model_Mailing extends Mage_Core_Model_Abstract
{
    public function __construct() 
    {
        $this->_init('mailing/mailing');
        parent::__construct();
    }
    
    public function insertUpdateRegistryData($data, $update = 0)
    {
        try {
            if(!empty($data)){
                $this->setEntityId($data['entity_id']);
                $this->setMail($data['mail']);
                $this->setTemplate($data['template']);
                if(!empty($data['params'])){
                    $json = Mage::helper('core')->jsonEncode($data['params']);
                    $this->setParams($json);
                }
                $this->setType($data['type']);
                $this->setSend($data['send']);
                if(!empty($data['date_send'])){
                    $this->setDateSend($data['date_send']);
                }
                if(!empty($data['dispatched'])){
                    $this->setDispatched($data['dispatched']);
                }
                $this->setIsLocked($data['is_locked']);
                $this->setActive($data['active']);
                if($update==0){
                    $this->setCreate(date('Y-m-d H:i:s'));
                }
                $this->setUpdate(date('Y-m-d H:i:s'));

            }else{
                throw new Exception('Error en procesar la petición: insuficiente data');
            }
        } catch (Exception $exc) {
            Mage::logException($exc);
        }
        return $this;
    }
    
    protected function _beforeSave()
    {
//        parent::_beforeSave();
//        if($this->getResource()->checkDuplicate($this)){
//           throw new Exception('User Email Already Exists');
//        }
//        //$this->getResource()   returns the object of the resource model, where can put in the sql operations
//        return $this;
    }
}