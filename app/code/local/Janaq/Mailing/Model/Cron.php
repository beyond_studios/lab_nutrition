<?php

class Janaq_Mailing_Model_Cron
{
    const BETWEEN_MINUTES = 10;
    const TYPE_LOYALTY = 1;

    /**
     * @name run
     * 
     * Ejecuta los procesos de expirar puntos, cambiar grupo y enviar correos.
     */
    public function run()
    {
        $time_start = microtime(true);

//        Mage::log('############# run cron mailing #############');
        $this->iterateMail();

        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);
        Mage::log("Tiempo total de ejecución del script Mailing $execution_time  segundos".PHP_EOL,null,"giantpoints.log");
    }

    /**
     * listMail
     *
     * @return mixed
     *
     * Listado de registros mails
     */
    public function listMail()
    {
        $listMails = Mage::getResourceModel('mailing/mailing_collection')
            ->betweenMinutes(self::BETWEEN_MINUTES)
            ->typeMail(self::TYPE_LOYALTY)
            ->flagSend()
            ->flagActive()
            ->addNotLockedFilter()
        ;
        return $listMails;
    }
    
    public function objectMailingLog()
    {
        $model = Mage::getModel('mailing/log');
        return $model;
    }

    /**
     * iterateMail
     *
     * Itera el listado de correos
     */
    public function iterateMail()
    {
        $listMails = $this->listMail();
//        Mage::log('Correo');
        if ($listMails->getSize()) {
            $emailsData = $listMails->getData();
            $listMails->lock();
//            Mage::log('lock');
            foreach ($listMails as $mail) {
                try {
                    $data = array(
                        'template' => $mail->getData('template'),
                        'mail' => $mail->getData('mail'),
                        'params' => !empty($mail->getData('params'))?json_decode($mail->getData('params')):array(),
                    );
                    $send = $this->sendMail($data);
//                    Mage::log("Envio de correo: $send");
                    $dataLog = $mail->getData();
                    $dataLog['send'] = $send;
                    $dataLog['dispatched'] =  date('Y-m-d H:i:s');
                    $dataLog['update'] =  date('Y-m-d H:i:s');
                    $this->saveLog($dataLog);
//                    Mage::log('Save Log');
                    $mail->delete();
//                    Mage::log('Delete Correo');
                }catch (Exception $e) {
                    $mail->insertUpdateRegistryData(array('is_locked'=>0), 1);
                    $mail->save();
                    Mage::helper('mailing')->log('Exception: ' . $e->getMessage() . ' in ' . __CLASS__ . ' on line ' . __LINE__);
                }
            }
        }
    }

    /**
     * sendMail
     *
     * @param $data
     * @return int
     *
     * Envía un correo.
     *
     */
    public function sendMail($data)
    {
        $template   = $data['template'];
        $emailTo = $data['mail'];
        $url_base = str_replace('index.php/','',Mage::getBaseUrl());

        $emailTemplateVariables = (array)$data['params'];
        $emailTemplateVariables['url_base'] = $url_base;

        $senderName = Mage::getStoreConfig(Mage_Core_Model_Store::XML_PATH_STORE_STORE_NAME);
        $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');
        $sender = array(
            'name' => $senderName,
            'email' => 'no_reply@bembos.com.pe'//$senderEmail
        );
        $send = 0; // pendiente
        try {
            $mailTemplate = Mage::getModel('core/email_template');
            $mailTemplate->setDesignConfig(array('area' => 'frontend'))
                ->setReplyTo($senderEmail)
                ->sendTransactional(
                    strtolower($template),
                    $sender,
                    $emailTo,
                    $emailTemplateVariables['customer_name'],
                    $emailTemplateVariables
                );
            $send = 1; //enviado
            if (!$mailTemplate->getSentSuccess()) {
                $send = 2;//fallido
                throw new Exception();
            }
        } catch (Exception $exc) {
            Mage::log($exc->getMessage());
        }
        return $send;
    }

    /**
     * saveLog
     *
     * @param $dataLog
     * @throws Exception
     *
     * Guarda los correos enviados en la tabla mailing_log
     */
    public function saveLog($dataLog)
    {
        $log = $this->objectMailingLog();
        $log->insertUpdateRegistryData($dataLog);
        $log->save();
    }
}
