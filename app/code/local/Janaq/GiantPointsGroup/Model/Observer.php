<?php

class Janaq_GiantPointsGroup_Model_Observer
{
    /**
     * process status customer
     *
     * @param $observer
     * @return $this
     */
    public function statusCustomer($observer)
    {
        $event = $observer->getEvent();
        $customerId = $event->getCustomerId();
        
        $cron = new Janaq_GiantPointsGroup_Model_Cron();
        $cron->operations($cron->getGileoPoints(), Janaq_GiantPointsGroup_Model_Cron::GILEO_RANGE_UP_POINTS, 
                Janaq_GiantPointsGroup_Model_Cron::GILEO_RANGE_DOWN_POINTS, Janaq_GiantPointsGroup_Model_Cron::GROUP_GENERAL, 
                Janaq_GiantPointsGroup_Model_Cron::GROUP_GILEO, $customerId, Janaq_GiantPointsGroup_Model_Cron::TEMPLATE_GILEO);
        $cron->operations($cron->getEstarPoints(), Janaq_GiantPointsGroup_Model_Cron::ESTASBEMBOS_RANGE_UP_POINTS, 
                Janaq_GiantPointsGroup_Model_Cron::ESTASBEMBOS_RANGE_DOWN_POINTS, Janaq_GiantPointsGroup_Model_Cron::GROUP_GILEO,
                Janaq_GiantPointsGroup_Model_Cron::GROUP_ESTASBEMBOS, $customerId, Janaq_GiantPointsGroup_Model_Cron::TEMPLATE_ESTASBEMBOS);

        $cron->saveLastPoint();
        $cron->saveMassMails(date('Y-m-d H:i:s'));
        $cron->emptyMails();
        Mage::log('Estado actualizado');
    }

}