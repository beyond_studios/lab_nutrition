<?php

class Janaq_GiantPointsGroup_Model_Cron
{
    /*const GILEO_POINTS = 100;*/
    const GILEO_RANGE_UP_POINTS = 50;
    const GILEO_RANGE_DOWN_POINTS = 150;
    /*const ESTASBEMBOS_POINTS = 400;*/
    const ESTASBEMBOS_RANGE_UP_POINTS = 350;
    const ESTASBEMBOS_RANGE_DOWN_POINTS = 450;
    const DOWN_GROUP = 1;
    const EQUAL_GROUP = 2;
    const UP_GROUP = 3;
    const GROUP_GENERAL = 1;
    const GROUP_GILEO = 4;
    const GROUP_ESTASBEMBOS = 5;
    const TEMPLATE_GILEO = 'GILEO';
    const TEMPLATE_ESTASBEMBOS = 'ESTASBEMBOS';
    
    const EXPIRE_DAY_POINTS = 90;
    
    const LIMIT_SELECT_CUSTOMER = 1000;

    const HOUR_SEND_EMAIL_DOWN = '11:00:00';
    
    protected $_groups = array();
    protected $_emailsToSend = array();


    /**
     * @name run
     * 
     * Ejecuta los procesos de expirar puntos, cambiar grupo y enviar correos.
     */
    public function run()
    {
        $time_start = microtime(true);

//        Mage::log('Cron de clasificación y envío de emails');
//        Mage::log('###########Expire Points##############');
        $this->expirePoints();
        
//        Mage::log('###########Changer Group##############');
        $this->changeGroupByPoints();

//        Mage::log('###########Send Mass Mail##############');
        //$this->sendMassMails();
        $date = $this->calcDaySendEmailDown();
        $this->saveMassMails($date);
        $this->emptyMails();

        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);
        Mage::log("Tiempo total de ejecución del script  $execution_time  segundos".PHP_EOL,null,'giantpoints.log');

    }

    /**
     * @name getGileoPoints
     *
     * Retorna el valor de puntos para llegar a "Gileos"
     *
     * @return puntosGileos
     */
    public function getGileoPoints()
    {
        return Mage::getStoreConfig("loyalty/gileo/puntaje_gileos");
    }

    /**
     * @name getGileoPoints
     *
     * Retorna el valor de puntos para llegar a "Estar"
     *
     * @return puntosEstar
     */
    public function getEstarPoints()
    {
        return Mage::getStoreConfig("loyalty/relacion/puntaje_relacion");
    }
    
    /**
     * @name changeGroupByPoints
     * 
     * Itera a los usuarios que tienen puntos.
     */
    public function changeGroupByPoints()
    {
//        Mage::log('Cron points group');
        
        $countVal = $this->getCountAllCustomerGiantPoints();
        $limitSelectCustomer = self::LIMIT_SELECT_CUSTOMER;
        $pageTotal = (int)($countVal/$limitSelectCustomer);
        $modRow = $countVal%$limitSelectCustomer;
        if($modRow!==0) $pageTotal++;
        try {
            for ($page=1;$page<=$pageTotal;$page++) {
                $rowsCurrent = $page*$limitSelectCustomer;
//                Mage::log("###### {$rowsCurrent} de {$countVal} ######");
                $customers = $this->listAllCustomerGiantPoints($page, $limitSelectCustomer);
                if(!empty($customers)){
                    foreach ($customers as $value) {
                        $customerId = $value['entity_id'];
                        $this->operations($this->getGileoPoints(), self::GILEO_RANGE_UP_POINTS,
                                self::GILEO_RANGE_DOWN_POINTS, self::GROUP_GENERAL, 
                                self::GROUP_GILEO, $customerId, self::TEMPLATE_GILEO);
                        $this->operations($this->getEstarPoints(), self::ESTASBEMBOS_RANGE_UP_POINTS,
                                self::ESTASBEMBOS_RANGE_DOWN_POINTS, self::GROUP_GILEO, 
                                self::GROUP_ESTASBEMBOS, $customerId, self::TEMPLATE_ESTASBEMBOS);

                        $this->saveLastPoint();
                    }
                }
            }
        } catch (Exception $exc) {
            Mage::log($exc->getMessage());
        }
    }
    
    /**
     * @name listAllCustomerId
     * 
     * @param type $page
     * @param type $rows
     * @return type
     * 
     * Lista todo los usuarios de Bembos.
     */
    public function listAllCustomerId($page = null, $rows = null)
    {
        $customers = Mage::getModel('customer/customer')->getCollection();
        
        if(!is_null($page)){
            $customers = $customers->setPageSize($rows)
            ->setCurPage($page);
        }
        
        $user = array(
            '181470',
            '972',
            '181468',
            '181469',
            '1046'
        );
//        $user = array(
//            '224259',
//            '177896',
//        );
//        
//        $customers->addFieldtoFilter('entity_id', array('in'=>$user));
        
        $customers->getSelect()->reset(Zend_Db_Select::COLUMNS)->columns('entity_id');

        return $customers->getData();
    }
    
    /**
     * @name getCountAllCustomer
     * @return type
     * 
     * Devuelve la cantidad total de usuarios de Bembos.
     */
    public function getCountAllCustomer()
    {
        $customers = Mage::getModel('customer/customer')->getCollection();
        $customers->getSelect()->reset(Zend_Db_Select::COLUMNS)->columns('count(entity_id) as total_customer');
        $row = $customers->getFirstItem();
        return $row->getData()['total_customer'];
    }
    
    /**
     * @name listAllCustomerGiantPoints
     * 
     * @param type $page
     * @param type $rows
     * @return type
     * 
     * Lista todo los usuarios de Bembos que tienen puntos loyalty.
     * 
     */
    public function listAllCustomerGiantPoints($page = null, $rows = null)
    {
        $customers = Mage::getModel('giantpoints/customer')->getCollection();
//        $customers->addFieldtoFilter('point_balance', array('gt' => 0));
        if(!is_null($page)){
            $customers = $customers->setPageSize($rows)
            ->setCurPage($page);
        }
        $customers->getSelect()->reset(Zend_Db_Select::COLUMNS)->columns('customer_id as entity_id');
        return $customers->getData();
    }
    
    /**
     * @name getCountAllCustomerGiantPoints
     * @return type
     * 
     * Devuelve la cantidad total de usuarios de Bembos que tienen puntos.
     */
    public function getCountAllCustomerGiantPoints()
    {
        $customers = Mage::getModel('giantpoints/customer')->getCollection();
//        $customers->addFieldtoFilter('point_balance', array('gt' => 0));
        $customers->getSelect()->reset(Zend_Db_Select::COLUMNS)->columns('count(customer_id) as total_customer');
        $row = $customers->getFirstItem();
        return $row->getData()['total_customer'];
    }
    
    /**
     * @name getGiantpointsCustomerObject
     * 
     * @param type $customerId
     * @return type
     * 
     * Devuelve los datos de la tabla gianpoints/customer.
     */
    public function getGiantpointsCustomerObject($customerId)
    {
        $customerGiant = Mage::getModel("giantpoints/customer");
        $customer = $customerGiant->getCustomerById($customerId);
        return $customer;
    }
    
    /**
     * @name getCurrentPoints
     * 
     * @param type $customerId
     * @return type
     * 
     * Retorna el puntaje loyalty actual de un usuario.
     */
    public function getCurrentPoints($customerId)
    {
        $customer = $this->getGiantpointsCustomerObject($customerId);
        return $customer->getPointBalance();
    }
    
    /**
     * @name getCustomerObject
     * 
     * @param type $customerId
     * @return type
     * 
     * Retorna el modelo de la tabla Customer.
     */
    public function getCustomerObject($customerId)
    {
        $customer = Mage::getModel('customer/customer')->load($customerId);
        return $customer;
    }
    
    /**
     * @name getGroup
     * 
     * @param type $customerId
     * @return type
     * 
     * Retorna el modelo de la tabla Group.
     */
    public function getGroup($customerId)
    {
        $customer = $this->getCustomerObject($customerId);
        return $customer->getGroupId();
    }
    
    /**
     * @name insertDuplicateGiantPointsGroupCustomer
     * 
     * @param type $data
     * @return type
     * 
     * Inserta datos, si el registro no existe en la tabla giantpointsgroup/customer .
     */
    public function insertDuplicateGiantPointsGroupCustomer($data)
    {
        $table = Mage::getSingleton('core/resource')->getTableName('giantpointsgroup/customer');
        $adapter = Mage::getSingleton('core/resource')->getConnection('core_write');
        return $adapter->insertOnDuplicate($table, $data, array('last_update', 'point', 'active'));
        
    }
    
    /**
     * @name getGiantpointsGroupCustomerObject
     * 
     * @param type $customerId
     * @return type
     * 
     * Retorna el modelo de la tabla giantpointsgroup/customer .
     */
    public function getGiantpointsGroupCustomerObject($customerId)
    {
        $groupCustomerModel = Mage::getModel('giantpointsgroup/customer');
        if($groupCustomerModel !== false){
            $groupCustomerModel = $groupCustomerModel->load($customerId, 'customer_id');
        }
        return $groupCustomerModel;
    }
    
    
    /**
     * @name lastPoint
     * 
     * @param type $customerId
     * @return type
     * 
     * Devuelve el último puntaje loyalty antes de hacer una transacción.
     */
    public function lastPoint($customerId)
    {
        $groupCustomer = $this->getGiantpointsGroupCustomerObject($customerId);
        $point = $groupCustomer->getPoint();
        return empty($point)?0:$point;
    }
    
    /**
     * @name savePointGroup
     * 
     * Recorre la cola de grupos y guarda el último puntaje loyalty del usuario.
     */
    public function savePointGroup()
    {
        $groups = $this->getPointGroups();
        if(!empty($groups)){
            $group = $groups[0][1];
            $customerId = $groups[0][0];
            $currentPoint = $groups[0][3];
            if($groups[0][2] == self::DOWN_GROUP){
                foreach ($groups as $value) {
                    $group = $value <= $group ? $value : $group;
                }
                $this->saveGroup($customerId, $group);
                $data = array('point'=>$currentPoint, 'active'=>1);
                $this->saveLastPointGroup($customerId, $data);
            }elseif($groups[0][2] == self::UP_GROUP){
                foreach ($groups as $value) {
                    $group = $value >= $group ? $value : $group;
                }
                $this->saveGroup($customerId, $group);
                $data = array('point'=>$currentPoint, 'active'=>1);
                $this->saveLastPointGroup($customerId, $data);
            }
        }
        $this->emptyPointGroups();
    }
    
    /**
     * @name saveLastPoint
     * 
     * Guarda el último puntaje loyalty de un usuario.
     */
    public function saveLastPoint()
    {
        $groups = $this->getPointGroups();
        if(!empty($groups)){
            $group = $groups[0][1];
            $customerId = $groups[0][0];
            $currentPoint = $groups[0][3];
            $data = array('point'=>$currentPoint, 'active'=>1);
            $this->saveLastPointGroup($customerId, $data);
        }
        $this->emptyPointGroups();
    }
    
    /**
     * @name saveLastPointGroup
     * 
     * @param type $customerId
     * @param type $data
     * @return boolean
     * 
     * Guarda los puntajes loyalty en la tabla giantpointsgroup/customer .
     */
    public function saveLastPointGroup($customerId, $data) {
        $customer = Mage::getModel('customer/customer')->load($customerId);
        $groupCustomer = $this->getGiantpointsGroupCustomerObject($customerId);
        $groupCustomer->InsertUpdateRegistryData($customer, $data);
        $groupCustomer->save();
        return true;
    }

    /**
     * @name emptyPointGroups
     * 
     * Limpia la cola de grupos .
     */
    public function emptyPointGroups()
    {
        $this->_groups = array();
    }

    /**
     * @name setPointGroups
     * 
     * @param type $customerId
     * @param type $group
     * @param type $downUp
     * @param type $point
     * 
     * Agrega datos a la cola de grupo.
     */
    public function setPointGroups($customerId, $group, $downUp, $point)
    {
        $this->_groups[] = array($customerId, $group, $downUp, $point);
    }
    
    /**
     * @name getPointGroups
     * 
     * @return type
     * 
     * Obtiene la cola de grupos.
     */
    public function getPointGroups()
    {
        return $this->_groups;
    }


    /**
     * @name downUp
     * 
     * @param type $customerId
     * @return type
     * 
     * Evalúa si el usuario esta ganando o perdiendo puntos.
     */
    public function downUp($customerId)
    {
        $lastPoint = $this->lastPoint($customerId);
        $currentPoint = $this->getCurrentPoints($customerId);
        if($currentPoint>$lastPoint){
            return self::UP_GROUP;
        }elseif($currentPoint==$lastPoint){
            return self::EQUAL_GROUP;
        }else{
            return self::DOWN_GROUP;
        }
    }

    /**
     * @name operations
     * 
     * @param type $point
     * @param type $min
     * @param type $max
     * @param type $groupBefore
     * @param type $groupAfter
     * @param type $customerId
     * @param type $template
     * 
     * Evalúa si el usuario debe bajar, permanecer o subir de grupo .
     */
    public function operations($point, $min, $max, $groupBefore, $groupAfter, $customerId, $template)
    {
        // Separar el proceso en:
        //    - Actualizar el Group ID
        //    - Crear una cola de correos para procesarla después.


        $currentPoint = $this->getCurrentPoints($customerId);
        $pointGroup = $this->downUp($customerId);
        $currentGroup = $this->getGroup($customerId);
        $dataGroupCustomer = array('point'=>$currentPoint, 'active'=>1);
        if($currentPoint < $point){
            if($pointGroup == self::UP_GROUP /*|| $pointGroup == self::EQUAL_GROUP*/){
//                Mage::log('UP - 1');
                if($currentPoint>=$min){
//                    Mage::log('UP - 1 - a');
//                    $this->sendMail("FALTA_POCO_{$template}", $customerId, $currentGroup, ($point-$currentPoint));
                    $this->setMail("FALTA_POCO_{$template}", $customerId, $currentGroup, ($point-$currentPoint));
                }
            }  elseif($pointGroup == self::DOWN_GROUP) {
//                Mage::log('DOWN - 1');
                if(($currentGroup != $groupBefore) && ((int)$groupBefore<(int)$currentGroup)){
//                    Mage::log('DOWN - 1 - a');
                    $this->saveGroup($customerId, $groupBefore);
//                    $this->sendMail("BAJASTE_{$template}", $customerId, $groupBefore, 0);
                    $this->setMail("BAJASTE_{$template}", $customerId, $groupBefore, 0);
                } elseif($currentPoint>=$min) {
//                    Mage::log('DOWN - 1 - b');
////                    $this->sendMail("NO_RETROCEDAS_{$template}", $customerId, $currentGroup, 0);
//                    $this->setMail("NO_RETROCEDAS_{$template}", $customerId, $currentGroup, 0);
                }
            }
        }elseif($currentPoint >= $point){
            if($pointGroup == self::UP_GROUP /*|| $pointGroup == self::EQUAL_GROUP*/){
//                Mage::log('UP - 2');
                if(($currentGroup != $groupAfter) && ((int)$groupAfter>(int)$currentGroup)){
//                    Mage::log('UP - 2 -a ');
                    $this->saveGroup($customerId, $groupAfter);
//                    $this->sendMail("BIENVENIDO_GRUPO_{$template}", $customerId, $groupAfter, 0);
                    $this->setMail("BIENVENIDO_GRUPO_{$template}", $customerId, $groupAfter, 0);
                } 
            } elseif($pointGroup == self::DOWN_GROUP) {
//                Mage::log('DOWN- 2');
                if($currentPoint <=$max){
//                    Mage::log('DOWN- 2 - a');
//                    $this->sendMail("NO_RETROCEDAS_{$template}", $customerId, $currentGroup, ($currentPoint-$point));
                    $this->setMail("NO_RETROCEDAS_{$template}", $customerId, $currentGroup, ($currentPoint-$point));
                }
            }
        }
        $this->setPointGroups($customerId, null, null, $currentPoint);
    }
    
    /**
     * @name getGroupObject
     * 
     * @param type $groupId
     * @return type
     * 
     * Devuelve el modelo de la tabla customer/group .
     */
    public function getGroupObject($groupId)
    {
        $group = Mage::getModel('customer/group')->load($groupId);
        return $group;
    }

    /**
     * @name sendMail
     * 
     * @param type $template
     * @param type $customerId
     * @param type $groupId
     * @param type $points
     * @throws Exception
     * 
     * Envio de correo
     */
    public function sendMail($template, $customerId, $groupId, $points = 0)
    {
        $customer = $this->getCustomerObject($customerId);
        $groupObject =  $this->getGroupObject($groupId);
        $groupName = $groupObject->getCustomerGroupCode();
        $emailTo = $customer->getEmail();
        $customerName   = $customer->getName();
        $url_base = str_replace('index.php/','',Mage::getBaseUrl());
        $emailTemplateVariables = array(
            'customer_name' => $customerName,
            'group_name' => $groupName,
            'url_base' => $url_base,
            'puntos' => $points,
        );
//        Mage::log('--envio de correo--');
//        Mage::log(print_r($emailTemplateVariables,true));
        $senderName = Mage::getStoreConfig(Mage_Core_Model_Store::XML_PATH_STORE_STORE_NAME);
        $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');
        $sender = array(
            'name' => $senderName,
            'email' => $senderEmail
        );
        try {
            $mailTemplate = Mage::getModel('core/email_template');                
            $mailTemplate->setDesignConfig(array('area' => 'frontend'))
                ->setReplyTo($senderEmail)
                ->sendTransactional(
                    strtolower($template),
                    $sender,
                    $emailTo,
                    $customerName,
                    $emailTemplateVariables
                );
            if (!$mailTemplate->getSentSuccess()) {
                throw new Exception();
            }
        } catch (Exception $exc) {
            Mage::log($exc->getMessage());
        }
    }
    
    /**
 * @name sendMassMails
 *
 * Itera y envía la cola de correo .
 */
    public function sendMassMails()
    {
        $mails = $this->getMails();
        $countMails = count($mails);
        if($countMails>0){
            for ($i = 0; $i<= $countMails-1; $i++){
                $val = $mails[$i];
                $this->sendMail($val[0], $val[1], $val[2], $val[3]);
            }
        }
//        $this->emptyMails();
    }

    /**
     * @name saveMassMails
     *
     * Itera y guarda el correo en la tabla de colas.
     */
    public function saveMassMails($date)
    {
        $mails = $this->getMails();
        $countMails = count($mails);
        if($countMails>0){
            for ($i = 0; $i<= $countMails-1; $i++){
                $val = $mails[$i];
//                $this->sendMail($val[0], $val[1], $val[2], $val[3]);
                //$template, $customerId, $groupId, $points
                $customer = $this->getCustomerObject($val[1]);
                $groupObject =  $this->getGroupObject($val[2]);
                $groupName = $groupObject->getCustomerGroupCode() == 'General' ? 'Tranqui' : $groupObject->getCustomerGroupCode();
                $moreData = array(
                    'customer_name' => $customer->getFirstName(),
                    //'customer_name' => $customer->getName(),
                    'customer_group_id' => $val[2],
                    'group_name' => $groupName,
                    'puntos' => $val[3],
                );
                $data = array(
                    'entity_id' => $val[1],
                    'mail' => $customer->getEmail(),//
                    'template' => $val[0],
                    'params' => $moreData,
                    'type' => 1, // tipo loyalty
                    'send' => 0,
                    'is_locked' => 0,
                    'date_send' => $date,
                    'active' => 1,
                );
                $this->saveMailing($data);
//                Mage::log('Correo guardado');
            }
        }
//        $this->emptyMails();
    }
    
    /**
     * @name setMail
     * 
     * @param type $template
     * @param type $customerId
     * @param type $groupId
     * @param type $points
     * 
     * Agrega un correo a la cola de correos.
     */
    public function setMail($template, $customerId, $groupId, $points = 0)
    {
         $this->_emailsToSend[] = array($template, $customerId, $groupId, $points);
    }
    
    /**
     * @name getMails
     * @return type
     * 
     * Obtiene la cola de correo.
     */
    public function getMails()
    {
        return $this->_emailsToSend;
    }
    
    /**
     * @name emptyMails
     * 
     * Limpia la cola de correo.
     */
    public function emptyMails()
    {
        $this->_emailsToSend = array();
    }
    
    /**
     * @name saveGroup
     * 
     * @param type $customerId
     * @param type $groupId
     * @return boolean
     * 
     * Setea un grupo a un usuario.
     */
    public function saveGroup($customerId, $groupId)
    {
        $customer = $this->getCustomerObject($customerId);
        $customer->setGroupId($groupId);
        $customer->save();
        return true;
    }
    
    /**
     * @name expirePoints
     * 
     * Expira los puntos loyalty menor que lo ultimos 90 días.
     */
    public function expirePoints()
    {
//        Mage::log('Enter Expire Points');
        $expiredTransactions = Mage::getResourceModel('giantpoints/transaction_collection')
            ->addAvailableBalanceFilter()
            ->addNotLockedFilter()
            ->addLastDayFilter(self::EXPIRE_DAY_POINTS)
            ;
        echo 'entro expire'.PHP_EOL;
        if ($expiredTransactions->getSize()) {
            $expTrans = $expiredTransactions->getData();
            $expiredTransactions->lock();
            foreach ($expiredTransactions as $tran) {
                echo 'entro a expirar'.PHP_EOL;
                try {
                    $rewardCustomer = Mage::getModel('giantpoints/customer')->load($tran->getRewardId());
                    $customer       = Mage::getModel('customer/customer')->load($tran->getCustomerId());
                    $tran->setData('reward_customer', $rewardCustomer);

                    //No necesita customer expireTransactionLastDay --- check ----
                    //$tran->setData('customer', $customer);

                    $tran->expireTransactionLastDay();
                    echo 'expirando ...'.PHP_EOL;
                } catch (Exception $e) {
                    Mage::helper('giantpoints')->log('Exception: ' . $e->getMessage() . ' in ' . __CLASS__ . ' on line ' . __LINE__);
                }
            }
        }
    }


    /**
     * saveMailing
     *
     * @param $data
     * @throws Exception
     *
     * Guarda los datos de los correos en la tabla "mainling"
     */
    public function saveMailing($data)
    {
        $modelMailing = Mage::getModel('mailing/mailing');
        $modelMailing->InsertUpdateRegistryData($data);
        $modelMailing->save();
    }

    /**
     * calcDaySendEmailDown
     *
     * Devuelve el día que se enviará el correo.
     *
     */
    public function calcDaySendEmailDown()
    {
        $date = new Zend_Date();
        $date->setTime(self::HOUR_SEND_EMAIL_DOWN);
        $now = new Zend_Date();
        if($now->compare($date)!=1){ //0 = equal, 1 = later, -1 = earlier
            return $date->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
        }
        $date->addDay(1);
        return $date->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);;
    }
}
