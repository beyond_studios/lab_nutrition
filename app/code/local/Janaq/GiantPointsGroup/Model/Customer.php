<?php

class Janaq_GiantPointsGroup_Model_Customer extends Mage_Core_Model_Abstract
{
    public function __construct() 
    {
        $this->_init('giantpointsgroup/customer');
        parent::__construct();
    }
    
    public function InsertUpdateRegistryData(Mage_Customer_Model_Customer $customer, $data)    
    {
        try {
            if(!empty($data)){
                $this->setCustomerId($customer->getId());
                $this->setWebsiteId($customer->getWebsiteId());
                $this->setPoint($data['point']);
                $this->setLastUpdate(date('Y-m-d h:i:s'));
                $this->setActive($data['active']);
            }else{
                throw new Exception('Error en procesar la petición: insuficiente data');
            }
        } catch (Exception $exc) {
            Mage::logException($exc);
        }
        return $this;
    }
    
    public function getInfoLoyalty($customer, $websiteId = 1)
    {
        $categoryId = '0';
        if ($customer->getGroupId() == Mage::getStoreConfig('loyalty/friendzone/groupid_friendzone')) { //general
            $categoryId = Mage::getStoreConfig('loyalty/friendzone/categoryid_friendzone');
        }elseif ($customer->getGroupId() == Mage::getStoreConfig('loyalty/gileo/groupid_gileos')) { //Gileos
            $categoryId = Mage::getStoreConfig('loyalty/gileo/categoryid_gileos');
        }elseif ($customer->getGroupId() == Mage::getStoreConfig('loyalty/relacion/groupid_relacion')) { //Estar con Bembos
            $categoryId = Mage::getStoreConfig('loyalty/relacion/categoryid_relacion');
        }

        $rewardData = Mage::getModel('giantpoints/customer')->getAccountByCustomer($customer)->getData();
        $dataGroup = array(
            'category_id' => $categoryId,
            'my_points' => empty($rewardData['point_balance'])?'0':(string)$rewardData['point_balance'],
            'url_share_fb' => Mage::getUrl('beneficios') . '?cod=' . Mage::helper('giantpoints/crypt')->encrypt($customer->getId()),
        );
        return $dataGroup;
    }

    public function getMultiplierPoints($groupId, $websiteId = 1)
    {
        $direction = Magegiant_GiantPoints_Model_Rate::MONEY_TO_POINT;
        $rate = Mage::getModel('giantpoints/rate');
        $rate->getResource()->loadRateByCustomerWebsiteDirection($rate, $groupId, $websiteId, $direction);
        return $rate->getPoints();
    }

    public function getPriceShipping()
    {
        $product = Mage::getModel('catalog/product')->load(470);
        $item = Mage::getModel('sales/quote_item')->setProduct($product)->setQty(1);
        $store = Mage::getModel('core/store')->load(1);

        $request = Mage::getModel('shipping/rate_request')
            ->setAllItems(array($item))
            ->setDestCountryId('PE')
            ->setPackageValue($product->getFinalPrice())
            ->setPackageValueWithDiscount($product->getFinalPrice())
            ->setPackageWeight($product->getWeight())
            ->setPackageQty(1)
            ->setPackagePhysicalValue($product->getFinalPrice())
            ->setFreeMethodWeight(0)
            ->setStoreId($store->getId())
            ->setWebsiteId($store->getWebsiteId())
            ->setFreeShipping(0)
            ->setBaseCurrency($store->getBaseCurrency())
            ->setBaseSubtotalInclTax($product->getFinalPrice());

        $model = Mage::getModel('shipping/shipping')->collectRates($request);

        $priceShipping = '3.9';
        foreach($model->getResult()->getAllRates() as $rate) {
            $priceShipping  = (string)$rate->getPrice();
        }
        return $priceShipping;
    }
}