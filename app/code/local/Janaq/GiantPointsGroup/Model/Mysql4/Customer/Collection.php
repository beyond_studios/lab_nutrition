<?php

class Janaq_GiantPointsGroup_Model_Mysql4_Customer_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct() 
    {
        $this->_init('giantpointsgroup/customer');
        parent::_construct();
    }
}