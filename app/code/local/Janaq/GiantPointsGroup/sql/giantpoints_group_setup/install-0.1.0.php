<?php

$installer = $this;
$installer->startSetup();

$tableName = $installer->getTable('giantpointsgroup/customer');
try {
    
    if ($installer->getConnection()->isTableExists($tableName) != true) {

        $table = $installer->getConnection()
            ->newTable($tableName)
            ->addColumn('groupcustomer_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
                array(
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary' => true,
                ),
                'Group customer Id'
            )
            ->addColumn('customer_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
                array(
                    'unsigned' => true,
                    'nullable' => false,
                    'default' => '0',
                ),
                'Customer Id'
            )
            ->addColumn('website_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null,
                array(
                    'unsigned' => true,
                    'nullable' => false,
                    'default' => '0',
                ),
                'Website Id'
            )
            ->addColumn('point', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
                array(),
                'Point Customer'
            )
            ->addColumn('last_update', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null,
                array(
                    'nullable' => false,
                ),
                'Last Update')
            ->addColumn('is_active', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
                'unsigned'  => true,
                'nullable'  => false,
                'default'   => '1',
            ), 'Is Active')
            ->addIndex($installer->getIdxName('giantpointsgroup/customer', array('customer_id')),
                array('customer_id'))
            ->addIndex($installer->getIdxName('giantpointsgroup/customer', array('website_id')),
                array('website_id'))
            ->addForeignKey(
                $installer->getFkName(
                    'giantpointsgroup/customer',
                    'customer_id',
                    'customer/entity',
                    'entity_id'
                ),
                'customer_id', $installer->getTable('customer/entity'), 'entity_id',
                Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
            ->addForeignKey(
                $installer->getFkName(
                    'giantpointsgroup/customer',
                    'website_id',
                    'core/website',
                    'website_id'
                ),
                'website_id', $installer->getTable('core/website'), 'website_id',
                Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
            ->setComment('Table Group Customer Giant');

        $installer->getConnection()->createTable($table);
    }


    $installer->endSetup();

} catch (Exception $exc) {
    echo $exc->getMessage();
    exit;
}
