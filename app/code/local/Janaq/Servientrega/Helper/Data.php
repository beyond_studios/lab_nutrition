<?php
/**
 * @category   Janaq
 * @package    Janaq_Servientrega
 * @author     contacto@janaq.com
 
 */
class Janaq_Servientrega_Helper_Data extends Mage_Core_Helper_Abstract
{

	// URLS SERVICIOS
    const URL_REGISTRAR_GUIAS = "servientrega_integrador/section_config/servientrega_registrargias_url";

    // AUTH SERVICES
    const URL_CLIENT = "servientrega_integrador/section_config_general/servientrega_client_id";
    const URL_BASE_SERVIENTREGA = "servientrega_integrador/section_config_general/servientrega_basehost_url";
    const URL_BASE_SERVIENTREGA_PRODUCTION = "servientrega_integrador/section_config_general/servientrega_basehost_url_production";
    const URL_BASE_SERVIENTREGA_MODE = "servientrega_integrador/section_config_general/servientrega_url_mode";
    const USER_SERVIENTREGA = "servientrega_integrador/section_config_general/servientrega_userauth_url";
    const PASS_SERVIENTREGA = "servientrega_integrador/section_config_general/servientrega_userpassword_url";
    const CCOSTOS_SERVIENTREGA = "servientrega_integrador/section_config_general/servientrega_ccostos_servientrega_id";

    const URL_CARRIER_TRAKING = "servientrega_integrador/section_config_general/servientrega_carrier_racking_url";

    const URL_REPORT = "servientrega_integrador/section_config_general/servientrega_report_url";


    /*CAMPOS CONFIGURABLES*/

	const DOCUMENTO_REMITENTE = "servientrega_integrador/section_config/servientrega_documento_remitente";
	const NOMBRE_REMITENTE = "servientrega_integrador/section_config/servientrega_nombre_remitente";
	const APELLIDO_REMITENTE = "servientrega_integrador/section_config/servientrega_apellido_remitente";
	const TELEFONO_REMITENTE = "servientrega_integrador/section_config/servientrega_telefono_remitente";
	const DIRECCION_REMITENTE = "servientrega_integrador/section_config/servientrega_direccion_remitente";
	const CONTACTO_REMITENTE = "servientrega_integrador/section_config/servientrega_contacto_remitente";
    const ID_CIUDAD_REMITENTE = "servientrega_integrador/section_config/servientrega_id_ciudad_remitente";

    // Fechas

    const DATE_HOLIDAYS = "servientrega_integrador/section_date/servientrega_holidays";
    const DATE_DISABLED_DAYS = "servientrega_integrador/section_date/servientrega_disabled_days";


    public function getUrlBaseServientrega()
    {
        if (Mage::getStoreConfig(self::URL_BASE_SERVIENTREGA_MODE, Mage::app()->getStore()) == 'production') {
            return Mage::getStoreConfig(self::URL_BASE_SERVIENTREGA_PRODUCTION, Mage::app()->getStore());
        }else {
            return Mage::getStoreConfig(self::URL_BASE_SERVIENTREGA, Mage::app()->getStore());
        }
        
    }
    public function getClientServientrega()
    {
        return Mage::getStoreConfig(self::URL_CLIENT, Mage::app()->getStore());
    }
    public function getUserSlervientrega()
    {
        return Mage::getStoreConfig(self::USER_SERVIENTREGA, Mage::app()->getStore());
    }
    public function getPasswordServientrega()
    {
        return Mage::getStoreConfig(self::PASS_SERVIENTREGA, Mage::app()->getStore());
    }
    public function getCcostosServientrega()
    {
        return Mage::getStoreConfig(self::CCOSTOS_SERVIENTREGA, Mage::app()->getStore());
    }

    /* SERVICIOS*/

    public function getUrlRegistrarGuias()
    {
        return Mage::getStoreConfig(self::URL_REGISTRAR_GUIAS, Mage::app()->getStore());
    }

    public function getDocumentoRemitente()
    {
        return Mage::getStoreConfig(self::DOCUMENTO_REMITENTE, Mage::app()->getStore());
    }
    /* DATOS GENERALES */
    public function getNombreRemitente()
    {
        return Mage::getStoreConfig(self::NOMBRE_REMITENTE, Mage::app()->getStore());
    }
    public function getApellidoRemitente()
    {
        return Mage::getStoreConfig(self::APELLIDO_REMITENTE, Mage::app()->getStore());
    }
    public function getTelefonoRemitente()
    {
        return Mage::getStoreConfig(self::TELEFONO_REMITENTE, Mage::app()->getStore());
    }
    public function getDireccionRemitente()
    {
        return Mage::getStoreConfig(self::DIRECCION_REMITENTE, Mage::app()->getStore());
    }
    public function getContactoRemitente()
    {
        return Mage::getStoreConfig(self::CONTACTO_REMITENTE, Mage::app()->getStore());
    }
    public function getIdCiudadRemitente()
    {
        return Mage::getStoreConfig(self::ID_CIUDAD_REMITENTE, Mage::app()->getStore());
    }
    public function getUrlCarrierTracking()
    {
        return Mage::getStoreConfig(self::URL_CARRIER_TRAKING, Mage::app()->getStore());
    }
    public function getTrakingServientrega($number_traking = '')
    {
        //$number_traking = '8000553736';
        $url = $this->getUrlCarrierTracking()."?Accion=Consultar&hr=http://www.servientrega.com.pe&reqint_id=".$number_traking;
        $data = file_get_contents($url);
 
        if ( preg_match_all('|<table>(.*?)</table>|is' , $data , $cap ) )
        {
            return $cap[1];
        }else {
            return  false;
        }
    }
    public function getUrlTrakingServientrega($number_traking = '')
    {
        //$number_traking = '8000553736';
        $url = $this->getUrlCarrierTracking()."?Accion=Consultar&hr=http://www.servientrega.com.pe&reqint_id=".$number_traking;

        return $link = '<a href="'.$url.'">'.$this->__('VER DETALLES').'</a>';

    }


    // Calendario

    public function getHoliday()
    {
        return Mage::getStoreConfig(self::DATE_HOLIDAYS, Mage::app()->getStore());
    }
    public function getDisabledDay()
    {
        return Mage::getStoreConfig(self::DATE_DISABLED_DAYS, Mage::app()->getStore());
    }

    public function getUrlReport()
    {
        return Mage::getStoreConfig(self::URL_REPORT, Mage::app()->getStore());
    }

}