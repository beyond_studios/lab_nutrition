<?php
class Janaq_Servientrega_Block_Adminhtml_Renderer_Action extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

 public function render(Varien_Object $row) {
 $value = $row->getData($this->getColumn()->getIndex());
 //Mage::log($value);

 return '<a target="_blank" href="'.$this->getUrlReport($value).'">Generar</a>';	
 }

 public function getUrlReport($code){

 		$helper = Mage::helper('servientrega');
 		if ($helper->getUrlReport()) {
        	return $helper->getUrlReport().$code;
 		}else{
 			return '#';
 		}
    }
}
?>