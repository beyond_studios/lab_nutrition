<?php
 
class Janaq_Servientrega_Block_Adminhtml_Integradoradmin extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'servientrega';
        $this->_controller = 'adminhtml_integradoradmin';
        $this->_headerText = Mage::helper('adminhtml')->__('Servientrega - Integrador');
        
    	parent::__construct();
    	$this->_removeButton('add');
    }
}