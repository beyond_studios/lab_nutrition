<?php
 
class Janaq_Servientrega_Block_Adminhtml_Servientrega extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'servientrega';
        $this->_controller = 'adminhtml_servientrega';
        $this->_headerText = Mage::helper('adminhtml')->__('Sistema - Ubigeos');
        
    	parent::__construct();
    	//$this->_removeButton('add');
    }
}