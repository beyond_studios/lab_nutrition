<?php
 
class Janaq_Servientrega_Block_Adminhtml_GenerationGuides_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('janaq_servientrega_generationguides_grid');
        $this->setDefaultDir('DESC');
        $this->setDefaultSort('order_id');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }
 
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('servientrega/integrador_collection');

        $collection->addFieldToFilter('status','completado'); 
        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }
 
    protected function _prepareColumns()
    {
        $this->addColumn('servientrega_id', array(
            'header' => 'ID',
            'index'  => 'servientrega_id',
            
        ));
        $this->addColumn('order_id', array(
            'header' => 'ID Orden',
            'index'  => 'order_id',
            
        ));
 
 
        
        $this->addColumn('numero_guia', array(
            'header'   => 'Numero guia',
            'index'    => 'numero_guia',
            'sortable' => false
        ));
        $this->addColumn('created_at', array(
            'header'   => 'Fecha de creación',
            'index'    => 'created_at',
            'sortable' => false
        ));
        
        $this->addColumn('status', array(
            'header'   => 'Status',
            'index'    => 'status',
            'sortable' => false
        ));
        $this->addColumn('action',
            array(
                'header'    => Mage::helper('adminhtml')->__('Generar Guias'),
                'width'     => '50px',
                'index'     => 'numero_guia',
                'renderer'  => 'servientrega/adminhtml_renderer_action',
                'filter'    => false,
                'sortable'  => false
        ));


 
        //$this->addExportType('*/*/exportInchooCsv', 'CSV');
        //$this->addExportType('*/*/exportInchooExcel', 'Excel XML');
 
        return parent::_prepareColumns();
    }
 
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
}