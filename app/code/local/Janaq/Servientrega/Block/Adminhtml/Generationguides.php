<?php
 
class Janaq_Servientrega_Block_Adminhtml_GenerationGuides extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'servientrega';
        $this->_controller = 'adminhtml_generationguides';
        $this->_headerText = Mage::helper('adminhtml')->__('Servientrega - Generación de Guias');
        
    	parent::__construct();
    	$this->_removeButton('add');
    }
}