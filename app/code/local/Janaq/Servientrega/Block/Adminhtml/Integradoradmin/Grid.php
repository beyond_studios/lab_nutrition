<?php
 
class Janaq_Servientrega_Block_Adminhtml_Integradoradmin_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('janaq_servientrega_integrador_grid');
        $this->setDefaultDir('DESC');
        $this->setDefaultSort('order_id');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }
 
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('servientrega/integrador_collection');
        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }
 
    protected function _prepareColumns()
    {
        $this->addColumn('servientrega_id', array(
            'header' => 'ID',
            'index'  => 'servientrega_id',
            
        ));
        $this->addColumn('order_id', array(
            'header' => 'OrderId',
            'index'  => 'order_id',
            
        ));
 
        $this->addColumn('data_sent', array(
            'header' => 'DataSent',
            'index'  => 'data_sent',
            
        ));
 
        $this->addColumn('type_task', array(
            'header'   => 'TypeTask',
            'index'    => 'type_task',
            
        ));
        $this->addColumn('eror', array(
            'header'   => 'Error',
            'index'    => 'error',
            'sortable' => false
        ));
        $this->addColumn('numero_guia', array(
            'header'   => 'Numero guia',
            'index'    => 'numero_guia',
            'sortable' => false
        ));
        $this->addColumn('created_at', array(
            'header'   => 'CreatedAt',
            'index'    => 'created_at',
            'sortable' => false
        ));
        $this->addColumn('update_at', array(
            'header'   => 'UpdatedAt',
            'index'    => 'update_at',
            'sortable' => false
        ));
        $this->addColumn('status', array(
            'header'   => 'Status',
            'index'    => 'status',
            'sortable' => false
        ));


 
        //$this->addExportType('*/*/exportInchooCsv', 'CSV');
        //$this->addExportType('*/*/exportInchooExcel', 'Excel XML');
 
        return parent::_prepareColumns();
    }
 
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    protected function _prepareMassaction(){
        $this->setMassactionIdField('integrador_id');
        $this->getMassactionBlock()->setFormFieldName('integrador');

        $statuses = array('pendiente'=>'pendiente','inhabilitado'=>'inhabilitado');

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
            'label'=> Mage::helper('adminhtml')->__('Cambiar de estado'),
            'url'   => $this->getUrl('*/*/massStatus', array('_current'=>true)),
            'additional' => array(
                'visibility' => array(
                    'name'  => 'status',
                    'type'  => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('adminhtml')->__('Status'),
                    'values'=> $statuses
                ))
        ));

        //$this->getMassactionBlock()->addItem('delete', array(
        //    'label'     => Mage::helper('adminhtml')->__('Delete'),
        //    'url'       => $this->getUrl('*/*/massDelete'),
        //    'confirm'   => Mage::helper('adminhtml')->__('Are you sure?')
        //));

        return $this;
    }
}