<?php
class Janaq_Servientrega_Block_Adminhtml_Servientrega_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Init class
     */
    public function __construct()
    {  
        parent::__construct();
        $this->setId('servientrega_form_detall');
        $this->setTitle('UBIGEOS');
    }  
     
    /**
     * Setup form fields for inserts/updates
     *
     * return Mage_Adminhtml_Block_Widget_Form
     */
    
    protected function _prepareForm()
    {  
        $model = Mage::registry('servientrega');
        $form = new Varien_Data_Form(array(
            'id'        => 'edit_form',
            'action'    => $this->getUrl('*/*/save', array('ubigeo_id' => $this->getRequest()->getParam('ubigeo_id'))),
            'method'    => 'post'
        ));
     
        $fieldset = $form->addFieldset('base_fieldset', array(
            'legend'    => 'Detalle',
            'class'     => 'fieldset-wide',
        ));
      
     
        $fieldset->addField('ubigeo_id', 'hidden', array(
            'name'      => 'ubigeo_id',
            'label'     => 'ID',
            'onchange' => "",
            'title'     => 'ID',
        ));

        $fieldset->addField('ubigeo_code', 'text', array(
            'name'      => 'ubigeo_code',
            'label'     => 'Código Ubigeo',
            'onchange' => "",
            'class'     => 'required-entry',
            'title'     => 'ID',
            'note'      => 'Código proporcionado por Servientrega "No modificar"',
        ));
        $fieldset->addField('id_ciudad', 'text', array(
            'name'      => 'id_ciudad',
            'label'     => 'Id Ciudad',
            'onchange' => "",
            'class'     => 'required-entry',
            'title'     => 'ID',
            'note'      => 'Id Ciudad proporcionado por Servientrega "No modificar"',
        ));

        $fieldset->addField('nombre_distrito', 'text', array(
            'name'      => 'nombre_distrito',
            'label'     => 'Distrito',
            'onchange' => "",
            'class'     => 'required-entry',
            'title'     => 'ID',
        ));

        $fieldset->addField('nombre_departamento', 'text', array(
            'name'      => 'nombre_departamento',
            'label'     => 'Departamento',
            'onchange' => "",
            'class'     => 'required-entry',
            'title'     => 'ID',
        ));
        
        $status = array(array('value'=>'ACTIVO','label' => 'ACTIVO'),array('value' => 'INACTIVO','label' => 'INACTIVO'));
        $fieldset->addField('estado_ciudad', 'select', array(
          'label'     => 'Estado',
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'estado_ciudad',
          'onclick' => "",
          'onchange' => "",
          'value'  => '0',
          'values' => $status,
          'disabled' => false,
          'readonly' => false,
          //'after_element_html' => '<small>Comments</small>',
          'tabindex' => 1
        ));
        

    

        
     
        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);  
     
        return parent::_prepareForm();
    }  
}