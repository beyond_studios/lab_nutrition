<?php

class Janaq_Servientrega_Adminhtml_Servientrega_IntegradoradminController extends Mage_Adminhtml_Controller_Action
{

    public function indexAction()
    {
    	$this->_title($this->__('Servientrega'))->_title($this->__('Integrador List'));

        $this->loadLayout();
        $this->_setActiveMenu('servientrega/servientrega');
        $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Servientrega'), Mage::helper('adminhtml')->__('Integrador List'));
        $this->_addContent($this->getLayout()->createBlock('servientrega/adminhtml_integradoradmin'));
        $this->renderLayout();
    }
    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody($this->getLayout()->createBlock('servientrega/adminhtml_integradoradmin_grid')->toHtml());
    }

    public function exportInchooCsvAction()
    {
        $fileName = 'ServientregaAdmin.csv';
        $grid = $this->getLayout()->createBlock('servientrega/adminhtml_integradoradmin_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }
 
    public function exportInchooExcelAction()
    {
        $fileName = 'ServientregaAdmin.xml';
        $grid = $this->getLayout()->createBlock('servientrega/adminhtml_integradoradmin_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    public function massStatusAction() {
        $integrador_ids = $this->getRequest()->getParam('integrador');
        if (!is_array($integrador_ids)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                $i = 0;
                foreach ($integrador_ids as $id) {
                    $model = Mage::getModel('servientrega/integrador')->load($id);
                    if ($model->getStatus() !='completado') {
                        $model->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                            $i++;
                    }
                    
                }
                $this->_getSession()->addSuccess(
                        $this->__('Total of %d record(s) were successfully updated', $i)
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('servientrega/integradoradmin');
    }

}