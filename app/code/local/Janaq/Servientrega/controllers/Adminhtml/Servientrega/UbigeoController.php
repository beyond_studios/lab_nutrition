<?php

class Janaq_Servientrega_Adminhtml_Servientrega_UbigeoController extends Mage_Adminhtml_Controller_Action
{
	public function indexAction(){
		$this->_title($this->__('System'))->_title($this->__('Ubigeo Servientrega'));

        $this->loadLayout();
        $this->_setActiveMenu('system/district');
        $this->_addBreadcrumb(Mage::helper('adminhtml')->__('System'), Mage::helper('adminhtml')->__('Distritos'));
        $this->_addContent($this->getLayout()->createBlock('servientrega/adminhtml_servientrega'));
        //$this->_addContent($this->getLayout()->createBlock('core/text')->setText($this->htmlScript()));
        $this->renderLayout();
	}
	public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody($this->getLayout()->createBlock('servientrega/adminhtml_servientrega_grid')->toHtml());
    }

    public function editAction()
    {


        $id  = $this->getRequest()->getParam('ubigeo_id');
        $model = Mage::getModel('servientrega/servientregaubigeo');

        if ($id) {
            $model->load($id);

            if (!$model->getId()) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('No existe'));
                $this->_redirect('*/*');

                return;
            }
        }
        if (!$model->getId()){
            
        }
        
        Mage::register('servientrega', $model);

        $this->loadLayout()
        ->_setActiveMenu('servientrega/ubigeo')
            ->_addBreadcrumb($id ? $this->__('Editar Ubigeo') : Mage::helper('adminhtml')
                ->__('Nuevo Ubigeo'), $id ? Mage::helper('adminhtml')
                ->__('Editar Ubigeo') : Mage::helper('adminhtml')->__('Nuevo Ubigeo'))
            ->_addContent($this->getLayout()->createBlock('servientrega/adminhtml_servientrega_edit')->setData('action', $this->getUrl('*/*/save')))
            ->renderLayout();
    }
    public function newAction() {
        $this->_forward('edit');
    }

    public function saveAction(){
        if ($data = $this->getRequest()->getPost()) {
           try {
           		$id = ($this->getRequest()->getParam('ubigeo_id')) ? $this->getRequest()->getParam('ubigeo_id') : null ;
                $model = Mage::getModel('servientrega/servientregaubigeo');
                   $model->setData($data)
                    ->setId($id);
                    $model->save();
           } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('servientrega')->__('Error al guardar registro.'));
           		
           }
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('servientrega')->__('Registro guardado.'));
               
                //$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            
            
        }
        $this->_redirect('*/*/');
    }
    public function massDeleteAction() {
        $ids = $this->getRequest()->getParam('servientrega');
        if (!is_array($ids)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                $i = 0;
                foreach ($ids as $id) {
                    $model = Mage::getModel('servientrega/servientregaubigeo')->load($id);
                    if ($model->getId()) {
                        $model->delete();
                        $i++;
                    }
                    
                    
                }
                $this->_getSession()->addSuccess(
                        $this->__('Un total de %d registro(s) fueron eliminados', $i)
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function deleteAction() {
        if ($this->getRequest()->getParam('ubigeo_id') > 0) {
            try {
                $model = Mage::getModel('servientrega/servientregaubigeo');

                $model->setId($this->getRequest()->getParam('ubigeo_id'))
                        ->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('El registro fue eliminado.'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('ubigeo_id' => $this->getRequest()->getParam('ubigeo_id')));
            }
        }
        $this->_redirect('*/*/');
    }

    protected function _isAllowed() {
        return Mage::getSingleton('admin/session')->isAllowed('servientrega/ubigeo');
    }

}