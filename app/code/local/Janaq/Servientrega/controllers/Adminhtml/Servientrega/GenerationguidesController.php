<?php

class Janaq_Servientrega_Adminhtml_Servientrega_GenerationguidesController extends Mage_Adminhtml_Controller_Action
{

    public function indexAction()
    {
    	$this->_title($this->__('Servientrega'))->_title($this->__('Generación de Guias'));

        $this->loadLayout();
        $this->_setActiveMenu('servientrega/generation_of_guides');
        $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Servientrega'), Mage::helper('adminhtml')->__('Generación de Guias'));
        $this->_addContent($this->getLayout()->createBlock('servientrega/adminhtml_generationguides'));
        $this->renderLayout();
    }
    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody($this->getLayout()->createBlock('servientrega/adminhtml_generationguides_grid')->toHtml());
    }

    /*public function exportInchooCsvAction()
    {
        $fileName = 'ServientregaAdmin.csv';
        $grid = $this->getLayout()->createBlock('servientrega/adminhtml_integradoradmin_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }
 
    public function exportInchooExcelAction()
    {
        $fileName = 'ServientregaAdmin.xml';
        $grid = $this->getLayout()->createBlock('servientrega/adminhtml_integradoradmin_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }*/
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('servientrega/generation_of_guides');
    }

}