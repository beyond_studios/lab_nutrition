<?php

class Janaq_Servientrega_Model_ObserverFieldsCheckout extends Mage_Payment_Model_Method_Abstract
{

    public function saveCustomDataFront($event)
    {

        $params = Mage::app()->getRequest()->getParams();
        $date = str_replace('/', '-', $params["sitewards"]["delivery_date"]);
        $shipping_method = $params['shipping_method'];
        if ($shipping_method == 'tablerate_bestway' ) {
            $newdate = date('Y-m-d', strtotime($date));
            $newdate = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s',strtotime($newdate));
            $quote = $event->getEvent()->getOrder();

            $quote->setData('servientrega_status', "En Almacén");
            $quote->setData('shipping_date_servientrega', $newdate);
        }
        return $this;

    }
    
    
}
