<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 15/12/2016
 * Time: 12:12 PM
 */

class Janaq_Servientrega_Model_Observer extends Mage_Payment_Model_Method_Abstract
{

    public function afterSave(Varien_Event_Observer $event)
    {
        $params = Mage::app()->getRequest()->getParams();
        $shipping_method = $params['shipping_method'];

        if ($shipping_method == 'servientrega_servientrega' ) {
            $date = str_replace('/', '-', $params["sitewards"]["delivery_date"]);
            $order = $event->getOrder();
            $new_date = date('Y-m-d', strtotime($date));
            $new_date = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s',strtotime($new_date));
            $order->setData('servientrega_status', "En Almacén");
            $order->setData('servientrega_shipping_date', $new_date);

        }

    }

    public function validateServientrega(Varien_Event_Observer $observer){

        $params = Mage::app()->getRequest()->getParams();
        //validacion DNI 8 digitos
        //Mage::log('Validacion DNI');
        
        $order = $observer->getEvent()->getOrder();
        $shipping_method = $order->getShippingMethod();
        if ($shipping_method == "servientrega_servientrega") {
        
            $billing_address = $order->getBillingAddress();
            $shipping_address = $order->getShippingAddress();

            $dni_billing = '';$region_billing = ''; $postcode_billing = '';
            $dni_shipping = '';$region_shipping = ''; $postcode_shipping = '';

            $postcode_billing = $billing_address->getPostcode();
            $postcode_shipping = $shipping_address->getPostcode();
            $dni_billing = $billing_address->getFax();
            $dni_shipping = $shipping_address->getFax();
            $region_billing = $billing_address->getRegion();
            $region_shipping = $shipping_address->getRegion();

            $postcode_b = explode(" - ",$postcode_billing);
            $postcode_s = explode(" - ",$postcode_shipping);

            $helper = Mage::helper('provincedropdown');


            $response_billing = $helper->validateUbigeoServientrega($region_billing,trim($postcode_b[1]));

            if (!$response_billing) {
                Mage::throwException('El ubigeo en la dirección de facturación no está disponible');
            }

            $response_shipping = $helper->validateUbigeoServientrega($region_shipping,trim($postcode_s[1]));

            if (!$response_shipping) {
                Mage::throwException('El ubigeo en la dirección de envío no está disponible');
            }

        }
    }

    public function saveOrderServientregaSlin($observer){

        Mage::log('Entra a guardar orden para Servientrega');
        $date = Mage::getModel('core/date')->date('Y-m-d H:i:s');
        $model = Mage::getSingleton('servientrega/integrador');
        //$increment_id = $observer->getEvent()->getOrder()->getIncrementId();
        $increment_id = $observer['order']->getIncrementId();
        $shipping_method = $observer['order']->getShippingMethod();
        //Mage::log($observer['order']->getOrderId());
        //$increment_id = $observer['order']->getOrderId();
        //Mage::log('Shipping Name:'.$shipping_method);
        if ($shipping_method == 'servientrega_servientrega') {
            $modelUpdate = Mage::getModel('servientrega/integrador');
            $modelUpdate->load($increment_id,'order_id');
            if ($increment_id && !$modelUpdate->getId()) {
                $model->setOrderId($increment_id);
                $model->setTypeTask('Order');
                $model->setStatus('pendiente');
                $model->setCreatedAt($date);
                $model->setUpdateAt($date);
            }else{
                $model->setId($modelUpdate->getId());
                //$model->setStatus('pendiente');
                $model->setUpdateAt($date);
            }
            try {
                $model->save();
            } catch (Exception $e) {
                Mage::log('Error al guardar registro de Servientrega (Tareas)');
            }
        }
        
        
        return $this;

    }
}