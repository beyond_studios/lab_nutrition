<?php 

class Janaq_Servientrega_Model_Funciones extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('servientrega/funciones');
    }
    
    public function registrarGuias($orderObj) {

    	$urlBase = Mage::helper("servientrega")->getUrlBaseServientrega();
        $urlregisterGuias = Mage::helper("servientrega")->getUrlRegistrarGuias();

        $cliente = Mage::helper("servientrega")->getClientServientrega();
        $usuario = Mage::helper("servientrega")->getUserSlervientrega();
        $contrasena = Mage::helper("servientrega")->getPasswordServientrega();
        


        $data = array(
            'RegistrarGuias' => array(
                'cliente' => (string)$cliente,
	            'usuario' => (string)$usuario,
	            'contrasena' =>(string)$contrasena,
                'xml' => (string)'<OPERACION><GUIAS>'.
                	'<NUMERO_GUIA>'.$orderObj['NUMERO_GUIA'].'</NUMERO_GUIA>'.
                	'<ID_TIPO_GUIA>'.$orderObj['ID_TIPO_GUIA'].'</ID_TIPO_GUIA>'.
                	'<ID_CLIENTE>'.(string)$cliente.'</ID_CLIENTE>'.
                	'<ID_PAIS_ORIGEN>'.$orderObj['ID_PAIS_ORIGEN'].'</ID_PAIS_ORIGEN>'.
                	'<ID_CIUDAD_ORIGEN>'.$orderObj['ID_CIUDAD_ORIGEN'].'</ID_CIUDAD_ORIGEN>'.
                	'<ID_PAIS_DESTINO>'.$orderObj['ID_PAIS_DESTINO'].'</ID_PAIS_DESTINO>'.
                	'<ID_CIUDAD_DESTINO>'.$orderObj['ID_CIUDAD_DESTINO'].'</ID_CIUDAD_DESTINO>'.
                	'<ID_TIEMPO_ENTREGA>'.$orderObj['ID_TIEMPO_ENTREGA'].'</ID_TIEMPO_ENTREGA>'.
                	'<ID_MEDTRANS>'.$orderObj['ID_MEDTRANS'].'</ID_MEDTRANS>'.
                    '<ID_TIPO_ENVIO>'.$orderObj['ID_TIPO_ENVIO'].'</ID_TIPO_ENVIO>'.
                	'<FECHA_ENVIO>'.$orderObj['FECHA_ENVIO'].'</FECHA_ENVIO>'.
                	'<ID_REMITE>'.$orderObj['ID_REMITE'].'</ID_REMITE>'. //opcional
                	'<ID_DESTINA>'.$orderObj['ID_DESTINA'].'</ID_DESTINA>'. //opcional
                	'<NOMBRE_REMITE_GUIA>'.$orderObj['NOMBRE_REMITE_GUIA'].'</NOMBRE_REMITE_GUIA>'.
                	'<APELLIDO_REMITE_GUIA>'.$orderObj['APELLIDO_REMITE_GUIA'].'</APELLIDO_REMITE_GUIA>'.
                	'<NOMBRE_DESTINA_GUIA>'.$orderObj['NOMBRE_DESTINA_GUIA'].'</NOMBRE_DESTINA_GUIA>'.
                	'<APELLIDO_DESTINA_GUIA>'.$orderObj['APELLIDO_DESTINA_GUIA'].'</APELLIDO_DESTINA_GUIA>'.
                	'<TELEFONO_REMITE_GUIA>'.$orderObj['TELEFONO_REMITE_GUIA'].'</TELEFONO_REMITE_GUIA>'.  // opcional
                	'<TELEFONO_DESTINA_GUIA>'.$orderObj['TELEFONO_DESTINA_GUIA'].'</TELEFONO_DESTINA_GUIA>'. //opcional
                	'<DIRECCION_REMITE_GUIA>'.$orderObj['DIRECCION_REMITE_GUIA'].'</DIRECCION_REMITE_GUIA>'.
                	'<DIRECCION_DESTINA_GUIA>'.$orderObj['DIRECCION_DESTINA_GUIA'].'</DIRECCION_DESTINA_GUIA>'.
                	'<OBSERVACIONES>'.$orderObj['OBSERVACIONES'].'</OBSERVACIONES>'. //opcional
                	'<CONTACTO>'.$orderObj['CONTACTO'].'</CONTACTO>'.
                	'<PESO_TOTAL_ENVIO>'.$orderObj['PESO_TOTAL_ENVIO'].'</PESO_TOTAL_ENVIO>'.
                	'<NUMERO_PIEZAS>'.$orderObj['NUMERO_PIEZAS'].'</NUMERO_PIEZAS>'.
                	'<FECHA_ENTREGA>'.$orderObj['FECHA_ENTREGA'].'</FECHA_ENTREGA>'.
                	'<ID_CENTRO_COSTOS>'.$orderObj['ID_CENTRO_COSTOS'].'</ID_CENTRO_COSTOS>'.  //opcional
                	'<ID_CCOSTOS_SERVIENT>'.$orderObj['ID_CCOSTOS_SERVIENT'].'</ID_CCOSTOS_SERVIENT>'.
                	'<VALOR_DECLARADO_GUIA>'.$orderObj['VALOR_DECLARADO_GUIA'].'</VALOR_DECLARADO_GUIA>'.//opcional
                	'<VALOR_RECAUDO_GUIA>'.$orderObj['VALOR_RECAUDO_GUIA'].'</VALOR_RECAUDO_GUIA>'.//opcional
                	'<MEDIO_PAGO_RECAUDO>'.$orderObj['MEDIO_PAGO_RECAUDO'].'</MEDIO_PAGO_RECAUDO>'.//opcional
                	'<VOLUMEN>'.$orderObj['VOLUMEN'].'</VOLUMEN>'.
                '</GUIAS></OPERACION>'  
            )
        );
        Mage::log($data);

        $response = $this->executeService($urlBase, $urlregisterGuias, $data);
        return $response->RegistrarGuiasResult;

    }

    public function executeService($url, $funcion, $data)
    {
        Mage::log("servientrega SERVICIO: ".$url." - FUNCION:".$funcion);
        try {

            $client = new SoapClient($url);
            $response = $client->__soapCall(
                $funcion,
                $data
            );
            Mage::log("servientrega RETURN: ");
            Mage::log($response);

            return $response;
        } catch(Exception $e) {
            Mage::log($e->getMessage());
            return false;
        }
    }
}