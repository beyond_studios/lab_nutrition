<?php

class Janaq_Servientrega_Model_Cron
{

    /* -----------------------------------------------------------------------------------------
            - Proceso de Enviar pedido a servientrega
    */
    public function run()
    {
    	$date = Mage::getModel('core/date')->date('Y-m-d H:i:s');
        $Collection = Mage::getResourceModel('servientrega/integrador_collection');
        $Collection->addFieldToFilter('status', array('in' => array('pendiente')));
        //ORDER
        foreach ($Collection as  $tareas) {
            $model = Mage::getModel('servientrega/integrador');
            $model->load($tareas->getServientregaId());
            if ($tareas->getTypeTask() == 'Order') {
            	try{
                    $incrementId = $model->getOrderId();
                    $order = Mage::getModel('sales/order')->loadByIncrementId($incrementId);
                    $customer_id = $order->getCustomerId();
                    $customerData = Mage::getModel('customer/customer')->load($customer_id);
                    $billingAddress = $order->getBillingAddress();
                    $shippingAddress = $order->getShippingAddress();
                    $orderItems = $order->getAllItems();
                    $emailCustomer = $order->getCustomerEmail();
                    //distrito 
                    $id_destina = $shippingAddress->getFax();
                    $postcode =$shippingAddress->getPostcode();
                    $region_id=$shippingAddress->getRegionId();
                    $region = Mage::getModel('directory/region')->load($region_id);
                    
                    $modelPostcode = Mage::getModel('provincedropdown/province');
                    $modelPostcode->load(185);
                    $postcodeaereo= $modelPostcode->getDefaultName();

                    $pos = strrpos($postcode, '-', 0);
                    $district = trim(substr($postcode, $pos+1)); 
                    Mage::log('Distrito : '.$district);
                    $collectionUbigeoServientrega = Mage::getResourceModel('servientrega/servientregaubigeo_collection');
                    //$collectionUbigeoServientrega->setOrder('nombre_departamento', 'DESC');
                    $collectionUbigeoServientrega->addFieldToFilter('estado_ciudad', array('like' => 'ACTIVO'));
                    $collectionUbigeoServientrega->addFieldToFilter('nombre_departamento', array('like' => $region->getName()));
                    $collectionUbigeoServientrega->addFieldToFilter('nombre_distrito', array('like' => $district));

                    $id_ciudad_destino ='';

                    foreach ($collectionUbigeoServientrega as $district) {

                        $id_ciudad_destino = $district->getIdCiudad();
                    }
                    //
                    $numero_guia = '0';//numero de guia 
                    $id_tipo_guia = '16'; //clientes numeracion homologada
                    //ID_CLIENTE
                    $id_pais_origen = '6'; 
                    $id_pais_destino = '6'; //$shippingAddress->getCountryId();

                    //id_ciudad_origen
                    $id_tiempo_entrega ='1';
                    
                    if ($postcodeaereo == $postcode) {// si es aereo 2 terreste 1
                        $id_medtrans = '2';
                    }else{
                        $id_medtrans = '1';
                    }
                    // 

                    $id_tipo_envio = '2'; // // 2 Mercancias, 1 docuentos unitarios
                    
                    $fecha_envio =Mage::getModel('core/date')->date('m/d/Y', strtotime($order->getCreatedAt())); // calcular de la orden

                    $id_destina = $shippingAddress->getFax(); // identificaion del destinatario opcional
                    
                    $nombre_destina_guia = $shippingAddress->getFirstname();

                    $apellido_destina_guia = $shippingAddress->getLastname();
                    $telefono_destina_guia = $shippingAddress->getTelephone();// opcional 
                    $direccion_destina_guia = $shippingAddress->getData('street');
                    $observaciones = $shippingAddress->getCompany();//
                    //PRODUCTOS
                    $storeId = Mage::app()->getStore()->getStoreId();
                    $peso_total_envio = 0;
                    $numero_piezas = 0;
                    $volumen = 0;
                    $orderItemsOrdered = $this->getOrderItemsOrdered($orderItems);
                    foreach ($orderItemsOrdered as $item) {
                            //Mage::log($item);
                            if (isset($item["child"])) {
                                foreach($item["child"] as $child) {
                                    if ($child["product_type"] == "simple") {
                                        
                                        $product_obj = Mage::getModel('catalog/product')->load($child["product_id"]);
                                        $peso_total_envio +=($child["weight"] * $child["qty_ordered"]);
                                        $numero_piezas +=$child["qty_ordered"];
                                        $high = (string)Mage::getResourceModel('catalog/product')->getAttributeRawValue($child["product_id"], 'high', $storeId);
                                        $long = (string)Mage::getResourceModel('catalog/product')->getAttributeRawValue($child["product_id"], 'long', $storeId);
                                        $width = (string)Mage::getResourceModel('catalog/product')->getAttributeRawValue($child["product_id"], 'width', $storeId);
                                        $volumen += $high*$long*$width;
                                            
                                    }
                                }
                            } else {
                                $product_obj = Mage::getModel('catalog/product')->load($item["product_id"]);
                                $peso_total_envio +=($item["weight"] * $item["qty_ordered"]);
                                $numero_piezas += $item["qty_ordered"];
                                $high = (string)Mage::getResourceModel('catalog/product')->getAttributeRawValue($item["product_id"], 'high', $storeId);
                                $long = (string)Mage::getResourceModel('catalog/product')->getAttributeRawValue($item["product_id"], 'long', $storeId);
                                $width = (string)Mage::getResourceModel('catalog/product')->getAttributeRawValue($item["product_id"], 'width', $storeId);
                                $volumen += $high*$long*$width;

                                            

                                
                            }
                    }
                    $peso_total_envio = ($peso_total_envio < 1) ? 1 : $peso_total_envio;
                    $peso_total_envio = number_format($peso_total_envio, 3, '.', '');
                    $volumen = number_format($volumen, 3, '.', '');

                    $weight = 0;
                    
                     // unidad en kilogramos , documentos 1

                
                    $fecha_entrega =Mage::getModel('core/date')->date('m/d/Y', strtotime($order->getServientregaShippingDate()));// Fecha seleccionada por el cliente en la web
                    $id_centro_costos = ''; //opcional

                    $valor_declarado_guia = number_format($order->getGrandTotal(),2,'.','');// costo total del envio

                    $valor_recaudo_guia = '0.00';
                    $medio_pago_recaudo = '0';

                    /*configuraciones en servientrega */
                    $documento_remitente=Mage::helper("servientrega")->getDocumentoRemitente();
    		        $nombre_remitente=Mage::helper("servientrega")->getNombreRemitente();
    		        $apellido_remitente=Mage::helper("servientrega")->getApellidoRemitente();
    		        $telefono_remitente=Mage::helper("servientrega")->getTelefonoRemitente();
    		        $direccion_remitente=Mage::helper("servientrega")->getDireccionRemitente();
    		        $contacto_remitente=Mage::helper("servientrega")->getContactoRemitente();
    		        $id_ciudad_remitente=Mage::helper("servientrega")->getIdCiudadRemitente();
    		        $id_ccostos_servient = Mage::helper("servientrega")->getCcostosServientrega();

                    /*fin configuraciones */
                    
                    $data = array(
                        'NUMERO_GUIA'=>$numero_guia,
                        'ID_TIPO_GUIA'=>$id_tipo_guia,
                        'ID_CLIENTE'=>'', // configuracion servientrega
                        'ID_PAIS_ORIGEN'=>$id_pais_origen,
                        'ID_CIUDAD_ORIGEN'=>$id_ciudad_remitente,//configuracion servientrega
                        'ID_PAIS_DESTINO'=>$id_pais_destino,
                        'ID_CIUDAD_DESTINO'=>$id_ciudad_destino,
                        'ID_TIEMPO_ENTREGA'=>$id_tiempo_entrega,
                        'ID_MEDTRANS'=>$id_medtrans,
                        'ID_TIPO_ENVIO'=>$id_tipo_envio,
                        'FECHA_ENVIO'=>$fecha_envio,
                        //'ID_REMITE'=>$documento_remitente,
                        'ID_REMITE' => $incrementId,
                        'ID_DESTINA'=>$id_destina,//opcional
                        'NOMBRE_REMITE_GUIA'=>$nombre_remitente,//configuracion servientrega
                        'APELLIDO_REMITE_GUIA'=>$apellido_remitente,//configuracion servientrega
                        'NOMBRE_DESTINA_GUIA'=>$nombre_destina_guia,
                        'APELLIDO_DESTINA_GUIA'=>$apellido_destina_guia,
                        'TELEFONO_REMITE_GUIA'=>$telefono_remitente,//configuracion servientrega
                        'TELEFONO_DESTINA_GUIA'=>$telefono_destina_guia,
                        'DIRECCION_REMITE_GUIA'=>$direccion_remitente,//configuracion servientrega
                        'DIRECCION_DESTINA_GUIA'=>$direccion_destina_guia,
                        'OBSERVACIONES'=>$observaciones,
                        'CONTACTO'=>$contacto_remitente,//configuracion servientrega
                        'PESO_TOTAL_ENVIO'=>$peso_total_envio,
                        'NUMERO_PIEZAS'=>(int)$numero_piezas,
                        'FECHA_ENTREGA'=>$fecha_entrega,
                        'ID_CENTRO_COSTOS'=>$id_centro_costos,
                        'ID_CCOSTOS_SERVIENT'=>$id_ccostos_servient,//configuracion servientrega
                        'VALOR_DECLARADO_GUIA'=> $valor_declarado_guia,
                        'VALOR_RECAUDO_GUIA'=>$valor_recaudo_guia,
                        'MEDIO_PAGO_RECAUDO'=>$medio_pago_recaudo,
                        'VOLUMEN'=>$volumen,
                        );
                    //var_dump($data);

                    $funciones = new Janaq_Servientrega_Model_Funciones();

                    $responseOrder = $funciones->registrarGuias($data);

                    $model->setDataSent(json_encode($data));

                    //Mage::log($responseOrder);


                    $response = explode("|", $responseOrder);
                    Mage::log($response);
                    $xmls = $response[1];

                    $xmls = new SimpleXMLElement($xmls);

                    //Mage::log($xmls);

                    $id_estado = $xmls->Table->ID_ESTADO;

                     
                    if ($id_estado == '0' ) {

                        $model->setError(json_encode($xmls->Table));
                        $model->setStatus('completado');
                        $model->setUpdateAt($date);
                        $model->setNumeroGuia($xmls->Table->NUMERO_GUIA);
                        $model->save();
                        $this->generateShipment($order,$xmls->Table->NUMERO_GUIA,'Servientrega');
                        //var_dump($xmls->Table->NUMERO_GUIA);
                        
                    }else{
                        $model->setError(json_encode($xmls->Table));
                        $model->setUpdateAt($date);
                        $model->save();
                    }
                } catch (Exception $e){
                    $model->setError($e->getMessage());
                        $model->setUpdateAt($date);
                        $model->save();
                        Mage::log('Error al actulizar estado de la Tarea Order');
                }
            }

    	}
	}
	public function getOrderItemsOrdered($orderItems)
    {
        $items_final = array();
        foreach ($orderItems as $item) {
            $data = $item->getData();
            $product_id = $data["item_id"];
            $obj = $data;

            if (is_null($data["parent_item_id"])) { // es composite
                $items_final[$product_id] = $obj;
            } else {
                $parent_item_id = $data["parent_item_id"];
                $items_final[$parent_item_id]["child"][] = $obj;
            }
        }
        return $items_final;
    }
    protected function generateShipment($order,$number_traking,$title_traking){
    	$itemQty =  $order->getItemsCollection()->count();
		$shipment = Mage::getModel('sales/service_order', $order)->prepareShipment($itemQty);
		$shipment = new Mage_Sales_Model_Order_Shipment_Api();
		$shipmentId = $shipment->create( $order->getIncrementId(), array(), 'Shipment created through ShipMailInvoice', true, true);
		//add tracking info
		$shipment_collection = Mage::getResourceModel('sales/order_shipment_collection');
		$shipment_collection->addAttributeToFilter('order_id', $order->getId());
		foreach($shipment_collection as $sc)
		{
		    $shipment = Mage::getModel('sales/order_shipment');
		    $shipment->load($sc->getId());
		    Mage::log($sc->getId());
		    if($shipment->getId() != '')
		    { 
		        try
		        {
		             Mage::getModel('sales/order_shipment_track')
		             ->setShipment($shipment)
		             ->setData('title', $title_traking)
		             ->setData('number', $number_traking)
		             ->setData('carrier_code', 'custom')
		             ->setData('order_id', $shipment->getData('order_id'))
		             ->save();

		        }catch (Exception $e)
		        {
		            Mage::log('order id '.$orderId.' no found');
		        }
		    }
		}
		// change order status to complete
		$order->addStatusToHistory(Mage_Sales_Model_Order::STATE_COMPLETE);
		$order->setData('state', Mage_Sales_Model_Order::STATE_COMPLETE);
		$order->save();
	}
}