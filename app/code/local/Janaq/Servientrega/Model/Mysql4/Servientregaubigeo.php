<?php
class Janaq_Servientrega_Model_Mysql4_Servientregaubigeo extends Mage_Core_Model_Mysql4_Abstract
{
  public function _construct()
  {
    $this->_init('servientrega/servientregaubigeo', 'ubigeo_id');
  }
}