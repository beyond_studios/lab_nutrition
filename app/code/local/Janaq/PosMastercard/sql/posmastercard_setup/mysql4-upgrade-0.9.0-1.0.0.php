<?php

$installer = $this;
$installer->startSetup();

$installer->addAttribute("order", "posmastercard_cardtypes", array("type"=>"varchar"));
$installer->addAttribute("quote", "posmastercard_cardtypes", array("type"=>"varchar"));

$installer->endSetup();

?>