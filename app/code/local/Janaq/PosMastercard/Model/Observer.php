<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 15/12/2016
 * Time: 12:12 PM
 */

class Janaq_PosMastercard_Model_Observer extends Mage_Payment_Model_Method_Abstract
{
    public function afterSave(Varien_Event_Observer $event)
    {
        $params = Mage::app()->getRequest()->getParams();
        $payment_method = $params['payment']['method'];

        if ($payment_method == 'posmastercard') {
            $cardtypes = $params["posmastercard_cardtypes"];
            $order = $event->getOrder();
            $order->setData('posmastercard_cardtypes',$cardtypes);
        }
        //Mage::log($params);
    }
}