<?php

class Janaq_PosMastercard_Model_Payment extends Mage_Payment_Model_Method_Abstract
{

    /**
     * Payment method code
     *
     * @var string
     */
    protected $_code  = 'posmastercard';

    /**
     * Cash On Delivery payment block paths
     *
     * @var string
     */
    protected $_formBlockType = 'posmastercard/form';
    protected $_infoBlockType = 'posmastercard/info';

    /**
     * Get instructions text from config
     *
     * @return string
     */
    public function getInstructions()
    {
        return trim($this->getConfigData('instructions'));
    }
    
    /**
     * Per client requirements we must reset MoneyAmount
     *
     * @return Mage_Payment_Model_Method_Abstract
     */
    public function assignData($data)
    {
        parent::assignData($data);
        
        if (!($data instanceof Varien_Object)) {
            $data = new Varien_Object($data);
        }
        $info = $this->getInfoInstance();
        $info->setMoneyAmount(null);
        $info->save();
        return $this;
    }
 
    /**
     * Validates custom data for payment method.
     *
     * @return Mage_Payment_Model_Method_Abstract
     */
    public function validate()
    {
        parent::validate();
 
        $info = $this->getInfoInstance();
//        $money = $info->getMoneyAmount();
//
//        if(empty($money)){
//            $errorCode = 'invalid_data';
//            $errorMsg = $this->_getHelper()->__('Money Amount is a required field');
//        }
//
//        if($errorMsg){
//            Mage::throwException($errorMsg);
//        }
        return $this;
    }

}
