<?php
/**
* @author janaq
*/
class Janaq_LabTv_Block_Tv extends Mage_Core_Block_Template
{
	protected $username = "labnutrition";
	protected $apikey = "AIzaSyD_5rx7ntF9Mn49DMHRlrG1uOyXhTtRJN0";
	protected $apiUrl = "https://www.googleapis.com/youtube/v3/";

    public function __construct()
	{
		parent::__construct();
		// Block caching setup
		$this->addData(array(
			'cache_lifetime'=> 604800, // (seconds) data lifetime in the cache
			'cache_tags' => array(
				Mage_Core_Model_Store::CACHE_TAG,
				Mage_Cms_Model_Block::CACHE_TAG,
			),
			'cache_key' => 'JNQ_LABTV_VIDEOS',
		));
	}

    protected function _prepareLayout()
    {
        return parent::_prepareLayout();        
    }

    public function getTitle()
	{
		return '<span class="text-dark-red">+</span>'.$this->__('Lab TV');
	}

    protected function getChannelInfo()
    {
    	$keyinfo = file_get_contents($this->apiUrl."channels?part=snippet&forUsername=" . $this->username . "&key=" . $this->apikey);
		return json_decode($keyinfo);
    }

    public function getVideoListHeader()
    {
    	//channel header
    	$html = '';
    	$keyinfo = $this->getChannelInfo();
		$userid = $keyinfo->items[0]->id;
		$channeltitle = $keyinfo->items[0]->snippet->title;
		$channeldescription = $keyinfo->items[0]->snippet->description;
		$channelthumbnail = $keyinfo->items[0]->snippet->thumbnails->default->url; // default, medium or high
		$html .= "<div style='width:100%;'>".
		"<a href='https://www.youtube.com/user/" . $username . "' target='_blank'>".
		"<img src='" . $channelthumbnail . "' style='border:none;float:left;margin-right:10px;' alt='" . $channeltitle . "' title='" . $channeltitle . "' /></a>".
		"<div style='width:100%;text-align:center;'><h1><a href='https://www.youtube.com/user/" .
		$username . "' target='_blank'>" . $channeltitle . "</a></h1>" . $channeldescription . "</div>"
		. "</div>";
		return $html;
    }
    
	public function getVideoList() {
		try
		{
		    $keyinfo = $this->getChannelInfo();
			$userid = $keyinfo->items[0]->id;
			$videoinfo = file_get_contents($this->apiUrl."search?order=date&part=snippet&channelId=".$userid."&maxResults=20"."&key=".$this->apikey);
			$videoinfo = json_decode($videoinfo);
			$videoCount = $videoinfo->pageInfo->totalResults;
		
			return $videoinfo->items;
		}
		catch (Exception $ex) {
			Mage::logException($e);
		}
	}

	public function getVideosUrl()
	{
		$url = "https://www.youtube.com/user/labnutrition";
		return $url;
	}
}
?>