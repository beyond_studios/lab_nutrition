<?php

/**
 * Janaq PosVisa Form Block
 */
class Janaq_PosVisa_Block_Form extends Mage_Payment_Block_Form_Cc
{
    /**
     * Instructions text
     *
     * @var string
     */
    protected $_instructions;

    /**
     * Block construction. Set block template.
     */
        protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('posvisa/form.phtml');
    }

    /**
     * Get instructions text from config
     *
     * @return string
     */
    public function getInstructions()
    {
        if (is_null($this->_instructions)) {
            $this->_instructions = $this->getMethod()->getInstructions();
        }
        return $this->_instructions;
    }
}