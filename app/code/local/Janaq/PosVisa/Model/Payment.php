<?php

/**
 * Janaq Payment PosVisa Model
 */
class Janaq_PosVisa_Model_Payment extends Mage_Payment_Model_Method_Abstract
{

    /**
     * Payment method code
     *
     * @var string
     */
    protected $_code  = 'posvisa';

    /**
     * Cash On Delivery payment block paths
     *
     * @var string
     */
    protected $_formBlockType = 'posvisa/form';
    protected $_infoBlockType = 'posvisa/info';

    /**
     * Get instructions text from config
     *
     * @return string
     */
    public function getInstructions()
    {
        return trim($this->getConfigData('instructions'));
    }
    
    /**
     * Stores custom data for payment method in the database.
     *
     * @return Mage_Payment_Model_Method_Abstract
     */
    public function assignData($data)
    {
        parent::assignData($data);
        
        if (!($data instanceof Varien_Object)) {
            $data = new Varien_Object($data);
        }
        $info = $this->getInfoInstance();
        $info->setMoneyAmount(null);
        $info->save();
        return $this;
    }
 
    /**
     * Validates custom data for payment method.
     *
     * @return Mage_Payment_Model_Method_Abstract
     */
    public function validate()
    {
        parent::validate();
 
        $info = $this->getInfoInstance();
//        $money = $info->getMoneyAmount();
//
//        if(empty($money)){
//            $errorCode = 'invalid_data';
//            $errorMsg = $this->_getHelper()->__('Money Amount is a required field');
//        }
//
//        if($errorMsg){
//            Mage::throwException($errorMsg);
//        }
        return $this;
    }

}
