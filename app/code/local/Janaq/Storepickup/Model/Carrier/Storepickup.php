<?php
class Janaq_Storepickup_Model_Carrier_Storepickup extends Mage_Shipping_Model_Carrier_Abstract
 implements Mage_Shipping_Model_Carrier_Interface {
	
	protected $_code = 'storepickup';
	
	public function getFormBlock(){
		return 'storepickup/storepickup';
	}

	public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        if (!Mage::getStoreConfig('carriers/'.$this->_code.'/active')) {
            return false;
        }

        if (!$this->checkAvailableDayTime()) {
            return false;
        }

        $freeQty = 0;
        $skus_notallowed = Mage::helper('storepickup')->getSkusNotAllowed();
        $skus = array();
        if ($request->getAllItems()) {
            $freePackageValue = 0;
            foreach ($request->getAllItems() as $item) {
                if ($item->getProduct()->isVirtual() || $item->getParentItem()) {
                    continue;
                }

                if (in_array($item->getProduct()->getSku(), $skus_notallowed)) {
                    $skus[] = $item->getProduct()->getSku();
                }

                if ($item->getHasChildren() && $item->isShipSeparately()) {
                    foreach ($item->getChildren() as $child) {
                        if ($child->getFreeShipping() && !$child->getProduct()->isVirtual()) {
                            $freeShipping = is_numeric($child->getFreeShipping()) ? $child->getFreeShipping() : 0;
                            $freeQty += $item->getQty() * ($child->getQty() - $freeShipping);
                        }
                    }
                } elseif ($item->getFreeShipping()) {
                    $freeShipping = is_numeric($item->getFreeShipping()) ? $item->getFreeShipping() : 0;
                    $freeQty += $item->getQty() - $freeShipping;
                    $freePackageValue += $item->getBaseRowTotal();
                }
            }
            $oldValue = $request->getPackageValue();
            $request->setPackageValue($oldValue - $freePackageValue);
        }

        if ($skus) {
            return false;
        }

        $oldWeight = $request->getPackageWeight();
        $oldQty = $request->getPackageQty();

        //$request->setPackageWeight($request->getFreeMethodWeight());
        $request->setPackageQty($oldQty - $freeQty);

        $result = $this->_getModel('shipping/rate_result');
        $rate = $this->getRate($request);

        $request->setPackageWeight($oldWeight);
        $request->setPackageQty($oldQty);
        
        if (!empty($rate) && $rate['price'] >= 0) {
            $delivery_time = $rate['delivery_time'];

            $method = $this->_getModel('shipping/rate_result_method');

            $method->setCarrier($this->_code);
            $method->setCarrierTitle($this->getConfigData('title'));

            $method->setMethod($this->_code);
            $method->setMethodTitle($this->getConfigData('name'));

            $new_shippingPrice = 0;

            if ($request->getFreeShipping() === true || ($request->getPackageQty() == $freeQty)) {
                $shippingPrice = 0;
            } else {
                $shippingPrice = $this->getFinalPriceWithHandlingFee($rate['price']);
                $priceAditional = 0;
                $weight_base = $oldWeight - $rate['weight_base'];
                if ($weight_base > 0){
                    $priceAditional = ceil($weight_base) * $rate['unit_cost'] ;
                }
                
                $new_shippingPrice = ($shippingPrice + $priceAditional);
            }
            
            $method->setPrice($new_shippingPrice);
            $method->setCost(0);
            $method->setDeliveryTime($delivery_time);

            $result->append($method);
        }
        elseif (empty($rate) && $request->getFreeShipping() === true) {
            /**
             * was applied promotion rule for whole cart
             * other shipping methods could be switched off at all
             * we must show table rate method with 0$ price, if grand_total more, than min table condition_value
             * free setPackageWeight() has already was taken into account
             */
            $request->setPackageValue($freePackageValue);
            $request->setPackageQty($freeQty);
            $rate = $this->getRate($request);
            if (!empty($rate) && $rate['price'] >= 0) {
                $method = $this->_getModel('shipping/rate_result_method');

                $method->setCarrier($this->_code);
                $method->setCarrierTitle($this->getConfigData('title'));

                $method->setMethod($this->_code);
                $method->setMethodTitle($this->getConfigData('name'));

                $method->setPrice(0);
                $method->setCost(0);
                $method->setDeliveryTime($delivery_time);

                $result->append($method);
            }
        }
        else {
            $error = $this->_getModel('shipping/rate_result_error');
            $error->setCarrier($this->_code);
            $error->setCarrierTitle($this->getConfigData('title'));
            $error->setErrorMessage($this->getConfigData('specificerrmsg'));
            $result->append($error);
        }
        
        return $result;
    }

    protected function checkAvailableDayTime(){
        $storepickup = Mage::helper('storepickup');
        $modelDate = Mage::getModel('core/date');

        $time_from = explode(",", $storepickup->getTimeFrom());
        $time_from = implode(":", $time_from);
        $time_to = explode(",", $storepickup->getTimeTo());
        $time_to = implode(":", $time_to);

        $disabled_days = explode(",", $storepickup->getDisabledDays());
        $holidays = explode(",", $storepickup->getHolidays());

        $server_hours = $modelDate->date('H:i:s');
        $current_date = $modelDate->date('d/m');
        $server_week_day = $modelDate->date('w');
        $str_server_hours = strtotime($server_hours);
        $str_time_to = strtotime($time_to);
        $str_time_from = strtotime($time_from);

        if (in_array($server_week_day, $disabled_days)) {
            return false;
        }
        if (in_array($current_date, $holidays)) {
            return false;
        }

        if ($str_server_hours > $str_time_to) {
            return false;
        } elseif ($str_server_hours < $str_time_from){
            return false;
        } else {
            return true;
        }

        return true;
    }

    protected function _getModel($modelName)
    {
        return Mage::getModel($modelName);
    }
    public function getRate(Mage_Shipping_Model_Rate_Request $request)
    {
        return Mage::getResourceModel('storepickup/storepickuprate')->getRate($request);
    }

	public function getAllowedMethods()
	{
		return array('excellence'=>$this->getConfigData('name'));
	}
}