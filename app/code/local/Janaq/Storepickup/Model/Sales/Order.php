<?php
class Janaq_Storepickup_Model_Sales_Order extends Mage_Sales_Model_Order{
    /**
     * Order states
     */
    const STATE_APPROVED_PAYMENT    = 'approved_payment';
    const STATE_REJECTED_PAYMENT    = 'rejected_payment';
    const STATE_CHARGEBACK_PAYMENT  = 'chargeback_payment';
    const STATE_REFUNDED_PAYMENT    = 'refunded_payment';

	public function getShippingDescription(){

		$desc = parent::getShippingDescription();
		/*$orderId = $this->getId();
        $shipping_method = $this->getShippingMethod();

        if ($shipping_method == 'storepickup_storepickup') {
        	$store_id = $this->getStoreStorepickup();
			$storeCollection = Mage::getModel('storelocator/storelocator')->load($store_id);

			$storeName = $storeCollection->getName();
			$storeDepartamento = $storeCollection->getState();
			if ($storeName && $storeDepartamento) {
				$desc .= ', Tienda: '.$storeName. ', Ubicación: '.$storeDepartamento;
			}
        }*/

		return $desc;
	}

    public function queueNewOrderEmail($forceMode = false)
    {
        $storeId = $this->getStore()->getId();

        if (!Mage::helper('sales')->canSendNewOrderEmail($storeId)) {
            return $this;
        }

        $payment_methods = array("bankdepositbcp", "bankdepositbbva", "paymentstore");

        // ---- Implementación para envio mails diferentes a tarjetas ----
        $payment = $this->getPayment()->getMethod();

        if (array_search($payment, $payment_methods) !== false) {
            return $this;
        }

        // ---- Fin de la implementación ----

        // Get the destination email addresses to send copies to
        $copyTo = $this->_getEmails(self::XML_PATH_EMAIL_COPY_TO);
        $copyMethod = Mage::getStoreConfig(self::XML_PATH_EMAIL_COPY_METHOD, $storeId);

        // Start store emulation process
        /** @var $appEmulation Mage_Core_Model_App_Emulation */
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);

        try {
            // Retrieve specified view block from appropriate design package (depends on emulated store)
            $paymentBlock = Mage::helper('payment')->getInfoBlock($this->getPayment())
                ->setIsSecureMode(true);
            $paymentBlock->getMethod()->setStore($storeId);
            $paymentBlockHtml = $paymentBlock->toHtml();
        } catch (Exception $exception) {
            // Stop store emulation process
            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
            throw $exception;
        }

        // Stop store emulation process
        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);

        // Retrieve corresponding email template id and customer name
        if ($this->getCustomerIsGuest()) {
            $templateId = Mage::getStoreConfig(self::XML_PATH_EMAIL_GUEST_TEMPLATE, $storeId);
            $customerName = $this->getBillingAddress()->getName();
        } else {
            $templateId = Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE, $storeId);
            $customerName = $this->getCustomerName();
        }

        /** @var $mailer Mage_Core_Model_Email_Template_Mailer */
        $mailer = Mage::getModel('core/email_template_mailer');
        /** @var $emailInfo Mage_Core_Model_Email_Info */
        $emailInfo = Mage::getModel('core/email_info');
        $emailInfo->addTo($this->getCustomerEmail(), $customerName);
        if ($copyTo && $copyMethod == 'bcc') {
            // Add bcc to customer email
            foreach ($copyTo as $email) {
                $emailInfo->addBcc($email);
            }
        }
        $mailer->addEmailInfo($emailInfo);

        // Email copies are sent as separated emails if their copy method is 'copy'
        if ($copyTo && $copyMethod == 'copy') {
            foreach ($copyTo as $email) {
                $emailInfo = Mage::getModel('core/email_info');
                $emailInfo->addTo($email);
                $mailer->addEmailInfo($emailInfo);
            }
        }

        //Loyalty Points
        $customer = Mage::getModel('customer/customer')->load($this->getCustomerId());
        $websiteId = 1;
        $direction = Magegiant_GiantPoints_Model_Rate::MONEY_TO_POINT;
        $rate = Mage::getModel('giantpoints/rate');
        $rate->getResource()->loadRateByCustomerWebsiteDirection($rate, $customer, $websiteId, $direction);

        $pointsLoyalty = $this->getGiantpointsEarn();

        // Set all required params and send emails
        $mailer->setSender(Mage::getStoreConfig(self::XML_PATH_EMAIL_IDENTITY, $storeId));
        $mailer->setStoreId($storeId);
        $mailer->setTemplateId($templateId);

        $storepickup = false;
        $express = false;
        $anothershippingmethod = false;
        $ondeliveryship = false;
        if ($this->getShippingMethod() == "storepickup_storepickup") {
            $storepickup = true;
        } else if ($this->getShippingMethod() == "urbaner_urbaner" && $this->getUrbanerOrderType() == 1) {
            $express = true;
        } else if ($this->getShippingMethod() == "ondeliveryship_ondeliveryship") {
            $ondeliveryship = true;
        } else {
            $anothershippingmethod = true;
        }

        $mailer->setTemplateParams(array(
            'order'        => $this,
            'billing'      => $this->getBillingAddress(),
            'payment_html' => $paymentBlockHtml,
            'giantpoint_earn' => (string)$pointsLoyalty,
            'storepickup' => $storepickup,
            'express'      => $express,
            'anothershippingmethod' => $anothershippingmethod,
            'ondeliveryship' => $ondeliveryship
        ));

        /** @var $emailQueue Mage_Core_Model_Email_Queue */
        $emailQueue = Mage::getModel('core/email_queue');
        $emailQueue->setEntityId($this->getId())
            ->setEntityType(self::ENTITY)
            ->setEventType(self::EMAIL_EVENT_NAME_NEW_ORDER)
            ->setIsForceCheck(!$forceMode);

        $mailer->setQueue($emailQueue)->send();

        $this->setEmailSent(true);
        $this->_getResource()->saveAttribute($this, 'email_sent');

        return $this;
    }

	public function queueOrderUpdateEmail($notifyCustomer = true, $comment = '', $forceMode = false)
    {
        $storeId = $this->getStore()->getId();

        if (!Mage::helper('sales')->canSendOrderCommentEmail($storeId)) {
            return $this;
        }
        // Get the destination email addresses to send copies to
        $copyTo = $this->_getEmails(self::XML_PATH_UPDATE_EMAIL_COPY_TO);
        $copyMethod = Mage::getStoreConfig(self::XML_PATH_UPDATE_EMAIL_COPY_METHOD, $storeId);
        // Check if at least one recipient is found
        if (!$notifyCustomer && !$copyTo) {
            return $this;
        }

        // Retrieve corresponding email template id and customer name
        if ($this->getCustomerIsGuest()) {
            $templateId = Mage::getStoreConfig(self::XML_PATH_UPDATE_EMAIL_GUEST_TEMPLATE, $storeId);
            $customerName = $this->getBillingAddress()->getName();
        }
        else {
            $templateId = Mage::getStoreConfig(self::XML_PATH_UPDATE_EMAIL_TEMPLATE, $storeId);
            $customerName = $this->getCustomerName();
        }

        /** @var $mailer Mage_Core_Model_Email_Template_Mailer */
        $mailer = Mage::getModel('core/email_template_mailer');
        if ($notifyCustomer) {
            /** @var $emailInfo Mage_Core_Model_Email_Info */
            $emailInfo = Mage::getModel('core/email_info');
            $emailInfo->addTo($this->getCustomerEmail(), $customerName);
            if ($copyTo && $copyMethod == 'bcc') {
                // Add bcc to customer email
                foreach ($copyTo as $email) {
                    $emailInfo->addBcc($email);
                }
            }
            $mailer->addEmailInfo($emailInfo);
        }

        // Email copies are sent as separated emails if their copy method is
        // 'copy' or a customer should not be notified
        if ($copyTo && ($copyMethod == 'copy' || !$notifyCustomer)) {
            foreach ($copyTo as $email) {
                $emailInfo = Mage::getModel('core/email_info');
                $emailInfo->addTo($email);
                $mailer->addEmailInfo($emailInfo);
            }
        }

        $urbaner_tracking = "";
        if ($this->getShippingMethod() == "urbaner_urbaner") {
            $urbaner = Mage::getModel('urbaner/integrador')->load($this->getIncrementId(),'order_id');
            if ($urbaner && $urbaner->getUrbanerTracking() && $urbaner->getUrbanerLatLng() && $urbaner->getUrbanerAddresses()) {
                $urbaner_tracking = $urbaner->getUrbanerTracking();
            }
        }

        // Set all required params and send emails
        $mailer->setSender(Mage::getStoreConfig(self::XML_PATH_UPDATE_EMAIL_IDENTITY, $storeId));
        $mailer->setStoreId($storeId);
        $mailer->setTemplateId($templateId);
        $mailer->setTemplateParams(array(
                'order'   => $this,
                'comment' => $comment,
                'billing' => $this->getBillingAddress(),
                'urbaner_tracking' => $urbaner_tracking
            )
        );

        /** @var $emailQueue Mage_Core_Model_Email_Queue */
        $emailQueue = Mage::getModel('core/email_queue');
        $emailQueue->setEntityId($this->getId())
            ->setEntityType(self::ENTITY)
            ->setEventType(self::EMAIL_EVENT_NAME_UPDATE_ORDER)
            ->setIsForceCheck(!$forceMode);
        $mailer->setQueue($emailQueue)->send();

        return $this;
    }
}
