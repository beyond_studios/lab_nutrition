<?php
$installer = $this;
$installer->startSetup();

$installer->addAttribute("order", "name_storepickup", array("type"=>"varchar"));
$installer->addAttribute("quote", "name_storepickup", array("type"=>"varchar"));

$installer->addAttribute("order", "dni_storepickup", array("type"=>"varchar"));
$installer->addAttribute("quote", "dni_storepickup", array("type"=>"varchar"));

$installer->endSetup();