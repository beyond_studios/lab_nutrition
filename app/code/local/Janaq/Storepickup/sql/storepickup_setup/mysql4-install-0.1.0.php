<?php
$installer = $this;
$installer->startSetup();

$installer->addAttribute("order", "store_storepickup", array("type"=>"varchar"));
$installer->addAttribute("quote", "store_storepickup", array("type"=>"varchar"));

$installer->endSetup();