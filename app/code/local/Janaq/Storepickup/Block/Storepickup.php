<?php 

class Janaq_Storepickup_Block_Storepickup extends Mage_Core_Block_Template
{
	public function __construct()
	{
		parent::__construct();
       // $this->setTemplate('storepickup/liststores.phtml');		
	}

    function getCollectionStoreLocation() {
    	$storeId = Mage::app()->getStore()->getStoreId();
		$collections = Mage::getModel('storelocator/storelocator')->getCollection()
			->setStoreId($storeId)
			->addFieldToSelect('*')
			->addFieldToFilter('store_pickup', 1)
			->addFieldToFilter('status', 1);
		if (Mage::helper('storelocator')->getConfig('sort_store')) {
			$collections->setOrder('name', 'ASC');
		} else {
			$collections->setOrder('sort', 'DESC');
		}
		$_stores = array();
		foreach ($collections as $key => $store) {
			if ($this->getWorkingTime($store)) {
				$_stores[] = $store;
			}
		}
		return $_stores;
    }

    protected function getWorkingTime($store) {
		$modelDate = Mage::getModel('core/date');

		$server_hours = $modelDate->date('H:i:s');
		$day_name = strtolower($modelDate->date('l'));
		$str_server_hours = strtotime($server_hours);

		$status = $store->getData($day_name . '_status');
		$open = $store->getData($day_name . '_open');
		$close = $store->getData($day_name . '_close');

		$openFormated = strtotime($open);
		$closeFormated = strtotime($close);

		if ($status == 2) {
			//Closed
			return false;
		} else {
			if ($str_server_hours > $closeFormated || $str_server_hours < $openFormated) {
	            return false;
	        } else {
	            return true;
	        }
		}
		return false;
	}
    
}