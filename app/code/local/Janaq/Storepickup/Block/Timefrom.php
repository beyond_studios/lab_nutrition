<?php

class Janaq_Storepickup_Block_Timefrom extends Mage_Adminhtml_Block_System_Config_Form_Field
{

    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $time = new Varien_Data_Form_Element_Time();
        $now = Mage::getModel('core/date')->timestamp(time());

        $format = 'H:I:s ';

        $data = array(
            'name'      => $element->getName(),
            'html_id'   => $element->getId(),
        );

        $time->setData($data);
        $time->setValue($element->getValue(), $format);
        $time->setFormat($format);
		
		// The line below leaves the required field
        //$date->setClass($element->getFieldConfig()->validate->asArray());
        $time->setForm($element->getForm());

        return $time->getElementHtml();
    }
}