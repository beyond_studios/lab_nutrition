<?php 

/**
 * PACKT Storepickup Data Helper
 *
 * @category   PACKT
 * @package    MST_Storepickup
 */
class Janaq_Storepickup_Helper_Data extends Mage_Core_Helper_Abstract
{
    const TIME_FROM = "carriers/storepickup/time_from";
    const TIME_TO = "carriers/storepickup/time_to";
    const DISABLED_DAYS = "carriers/storepickup/disabled_days";
    const HOLIDAYS = "carriers/storepickup/holidays";
    const SKUSNOTALLOWED = "carriers/storepickup/skus_notallowed";
    const TERMS = "carriers/storepickup/terms";
    const DELIVERY_TIME = "carriers/storepickup/delivery_time";

    public function getTimeFrom()
    {
        return Mage::getStoreConfig(self::TIME_FROM, Mage::app()->getStore());
    }
    public function getTimeTo()
    {
        return Mage::getStoreConfig(self::TIME_TO, Mage::app()->getStore());
    }
    public function getDisabledDays()
    {
        return Mage::getStoreConfig(self::DISABLED_DAYS, Mage::app()->getStore());
    }
    public function getHolidays()
    {
        return Mage::getStoreConfig(self::HOLIDAYS, Mage::app()->getStore());
    }
    public function getSkusNotAllowed()
    {
        $skus = Mage::getStoreConfig(self::SKUSNOTALLOWED, Mage::app()->getStore());
        $skus = array_map("trim", explode(",", $skus));
        return $skus;
    }
    public function getTerms()
    {
        return Mage::getStoreConfig(self::TERMS, Mage::app()->getStore());
    }
    public function getDeliveryTime()
    {
        return Mage::getStoreConfig(self::DELIVERY_TIME, Mage::app()->getStore());
    }
}