<?php
class Janaq_FitnessCalculator_Adminhtml_FitnesscalculatorController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {  
        // Let's call our initAction method which will set some basic params for each action
        $this->_initAction()
            ->renderLayout();
    }  
     
    public function newAction()
    {  
        // We just forward the new action to a blank edit form
        $this->_forward('edit');
    }  
     
    public function editAction()
    {  
        $this->_initAction();
     
        // Get id if available
        $id  = $this->getRequest()->getParam('fitnesscalculator_id');
        $model = Mage::getModel('fitnesscalculator/fitnesscalculator');
        if ($id) {
            // Load record
            $model->load($id);
     
            // Check if record is loaded
            if (!$model->getFitnesscalculatorId()) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('This fitness calculator register no longer exists.'));
                $this->_redirect('*/*/');
     
                return;
            }
        }  
     
        $this->_title($model->getFitnesscalculatorId() ? $model->getEmail() : $this->__('New Fitness Calculator'));
     
        $data = Mage::getSingleton('adminhtml/session')->getFitnesscalculatorData(true);
        if (!empty($data)) {
            $model->setData($data);
        }
     
        Mage::register('fitnesscalculator_fitnesscalculator', $model);
     
        $this->_initAction()
            ->_addBreadcrumb($id ? $this->__('Edit Fitness') : $this->__('New Fitness'), $id ? $this->__('Edit Fitness') : $this->__('New Fitness'))
            ->_addContent($this->getLayout()->createBlock('fitnesscalculator/adminhtml_fitnesscalculator_edit')->setData('action', $this->getUrl('*/*/save')))
            ->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody($this->getLayout()->createBlock('fitnesscalculator/adminhtml_fitnesscalculator_grid')->toHtml());
    }
     
    public function saveAction()
    {
        if ($postData = $this->getRequest()->getPost()) {
            $model = Mage::getSingleton('fitnesscalculator/fitnesscalculator');
            $model->setData($postData);

            try {
                if($model->getFitnesscalculatorId()){
                    $model->save();

                    Mage::getSingleton('adminhtml/session')->addSuccess($this->__('El registro ha sido guardado.'));
                    $this->_redirect('*/*/');
     
                    return;
                }
                else{
                    Mage::getSingleton('adminhtml/session')->addError($this->__('El registro no puede ser guardado.'));
                    $this->_redirect('*/*/');
                    return;
                }
            }  
            catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('An error occurred while saving this fitness.'));
            }
 
            Mage::getSingleton('adminhtml/session')->setBookData($postData);
            $this->_redirectReferer();
        }
    }
     
    public function messageAction()
    {
        $data = Mage::getModel('fitnesscalculator/fitnesscalculator')->load($this->getRequest()->getParam('fitnesscalculator_id'));
        echo $data->getContent();
    }


    public function massStatusAction() {
        $fitnessIds = $this->getRequest()->getParam('fitnesscalculator');
        if (!is_array($fitnessIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                foreach ($fitnessIds as $fitnessId) {
                    $fitness = Mage::getSingleton('fitnesscalculator/fitnesscalculator')->load($fitnessId);
                    $fitness->setIsMassupdate(true)->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated', count($fitnessIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function massDeleteAction()
    {
        $fitnessIds = $this->getRequest()->getParam('fitnesscalculator');
        if(!is_array($fitnessIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(es).'));
        } else {
            try {
                $fitnessModel = Mage::getModel('fitnesscalculator/fitnesscalculator');
                foreach ($fitnessIds as $fitnessId) {
                    $fitnessModel->load($fitnessId)->delete();
                }
                    Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('fitnesscalculator')->__('Total of %d record(s) were deleted.', count($fitnessIds)
                )
            );
            } 
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
         
        $this->_redirect('*/*/index');
    }

    public function exportCsvAction()
    {
        $fileName   = "fitnesscalculator.csv";
        $gridBlock  = $this->getLayout()->createBlock('fitnesscalculator/adminhtml_fitnesscalculator_grid');

        $content    = $gridBlock->getCsvFile();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Initialize action
     *
     * Here, we set the breadcrumbs and the active menu
     *
     * @return Mage_Adminhtml_Controller_Action
     */
    protected function _initAction()
    {
        $this->loadLayout()
            // Make the active menu match the menu config nodes (without 'children' inbetween)
            ->_setActiveMenu('customer/fitnesscalculator_fitnesscalculator')
            ->_title($this->__('Customer'))->_title($this->__('Fitness Calculator'))
            ->_addBreadcrumb($this->__('Customer'), $this->__('Customer'))
            ->_addBreadcrumb($this->__('Fitness Calculator'), $this->__('Fitness Calculator'));
        return $this;
    }
     
    /**
     * Check currently called action by permissions for current user
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('customer/fitnesscalculator');
    }
}