<?php
class Janaq_FitnessCalculator_AjaxController extends Mage_Core_Controller_Front_Action
{
	public function calculatorAction()
	{
		if (!Mage::helper('fitnesscalculator')->getFitnessCalcultarorEnable()) {
			return false;
		}
		$error = false;
		$result = array();
		$post = $this->getRequest()->getParams();
		try {
			$gender = trim($post['genero']);
			$edad = trim($post['edad']);
			$estatura = trim($post['estatura']);
			$peso = trim($post['peso']);
			$objetivo = trim($post['objetivo_id']);
			$email = trim($post['email']);

			if (!Zend_Validate::is($gender, 'NotEmpty') && ($gender != "M" || $gender != "F")) {
			    $error = true;
			    $result['mensaje'] = "Genero no ingresado.";
			}
			if (!Zend_Validate::is($edad, 'Int')) {
			    $error = true;
			    $result['mensaje'] = "Edad no ingresada.";
			}
			if (!Zend_Validate::is($estatura, 'Digits')) {
			    $error = true;
			    $result['mensaje'] = "Estatura no ingresada.";
			}
			if (!Zend_Validate::is($peso, 'Digits')) {
			    $error = true;
			    $result['mensaje'] = "Peso no ingresado.";
			}
			if (!Zend_Validate::is($objetivo, 'NotEmpty') || !Zend_Validate::is($objetivo, 'Int')) {
			    $error = true;
			    $result['mensaje'] = "Objetivo no ingresado.";
			}
			if (!Zend_Validate::is($email, 'EmailAddress')) {
			    $error = true;
			    $result['mensaje'] = "Email no ingresado.";
			}

			if ($error) {
				$result['error'] = true;
				$this->getResponse()->setBody(json_encode($result));
				return;
			}
			else {
				$_collection = Mage::getModel('fitnesscalculator/fitnesscalculator')->getCollection()
							->addFieldToFilter('email',$email);
				if (count($_collection) == 0) {
					//Guarda datos
					$model = Mage::getModel('fitnesscalculator/fitnesscalculator')->setData($post);

					$model->save();
				}
				else {
					//Actualiza datos
					$model = Mage::getModel('fitnesscalculator/fitnesscalculator')->load($email,'email');
					$model->addData($post);
					$model->save();
				}
				$result['error'] = false;
				$result['mensaje'] = "Consulta exitosa.";
				$result['html'] = $this->getProductsbyObj($objetivo);
				$this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
				$this->getResponse()->setBody(json_encode($result));
				return;
			}
		} catch (Exception $e) {
			Mage::log($e->getMessage());
			$result['error'] = true;
			$result['mensaje'] = "Error Fitness";
			$this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
			$this->getResponse()->setBody(json_encode($result));
		}
	}

	private function getProductsbyObj($obj_id)
	{
		$html = '';
		$objetivo = Mage::getModel('catalog/category')->load($obj_id);
		if ($objetivo) {
			$collection = $objetivo->getProductCollection()
						->addAttributeToSelect('*') // add all attributes - optional
 						->addAttributeToFilter('status', 1) // enabled
 						->addAttributeToFilter('visibility', 4); //visibility in catalog,search
 			$collection->getSelect()->order('position','desc');
 			$collection->getSelect()->limit(10);
			if (count($collection) > 0) {
				$productBlock = $this->getLayout()->createBlock('catalog/product_price');
				$jnqadmin = Mage::helper('jnqadmin');
				$html = '<div class="row">';
				$html .= '<ul class="owl-carousel owl-theme owl-grid" id="fitness-products">';

				foreach ($collection as $product) {
					$image = Mage::helper('catalog/image')->init($product, 'small_image')->resize(200,200);
					$marca = $jnqadmin->getManufacturerProduct($product);
					
					$html .= '<li class="item slider-item col-md-4">';
					$html .= '<div class="product-image-block">';
					$html .= '<a href="'. $product->getProductUrl() .'" title="'.$product->getName().'">';
					$html .= '<img alt="'.$product->getName().'" src="'.$image.'" width="200" height="200">';
					$html .= '</a>';
					$html .= '</div>';
					$html .= '<h2 class="product-name">';
					$html .= '<a href="'. $product->getProductUrl() .'">'.$product->getName();
					$html .= $marca;
					$html .= '</a>';
					$html .= '</h2>';
					$html .= $productBlock->getPriceHtml($product, true);
					$html .= '</li>';
				}
				$html .= '</ul>';
				$html .= '<div class="owl-loading"><i class="fa fa-spinner fa-spin"></i></div>';
				$html .= '</div>';
			}
		}
		return $html;
	}

	public function objetivosAction()
	{
		$cats = Mage::helper('fitnesscalculator')->getObjetivos();
		$this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
		$this->getResponse()->setBody(json_encode($cats));
	}
}