<?php
/**
* 
*/
class Janaq_FitnessCalculator_Helper_Data extends Mage_Core_Helper_Abstract
{
	const FITNESS_CALCULATOR_ENABLE = "jnqadmin_config/fitnesspromos/calculator_enable";
    const FITNESS_PROMO_ENABLE = "jnqadmin_config/fitnesspromos/promo_enable";
    const FITNESS_PROMO_SKU = "jnqadmin_config/fitnesspromos/sku";
	const FITNESS_PROMO_TITLE = "jnqadmin_config/fitnesspromos/title";
	const FITNESS_PROMO_DESCRIPTION = "jnqadmin_config/fitnesspromos/description";
	const FITNESS_PROMO_SHORTDESCRIPTION = "jnqadmin_config/fitnesspromos/short_description";
	const FITNESS_PROMO_BANNER = "jnqadmin_config/fitnesspromos/promo_banner";
    const FITNESS_PROMO_IMAGE = "jnqadmin_config/fitnesspromos/promo_image";
	const FITNESS_PROMO_MOBILEBANNER = "jnqadmin_config/fitnesspromos/promo_mobile_banner";
	const FITNESS_CALCULATOR_BANNER = "jnqadmin_config/fitnesspromos/fitness_banner";
	const FITNESS_CALCULATOR_MOBILEBANNER = "jnqadmin_config/fitnesspromos/fitness_mobile_banner";
	
	public function getFitnessCalcultarorEnable()
    {
        return Mage::getStoreConfig(self::FITNESS_CALCULATOR_ENABLE);
    }
    public function getFitnessPromoEnable()
    {
        return Mage::getStoreConfig(self::FITNESS_PROMO_ENABLE);
    }
    public function getFitnessPromoSKU()
    {
        return Mage::getStoreConfig(self::FITNESS_PROMO_SKU);
    }
    public function getFitnessPromoTitle()
    {
        return Mage::getStoreConfig(self::FITNESS_PROMO_TITLE);
    }
    public function getFitnessPromoDescription()
    {
        return Mage::getStoreConfig(self::FITNESS_PROMO_DESCRIPTION);
    }
    public function getFitnessPromoShortDescription()
    {
        return Mage::getStoreConfig(self::FITNESS_PROMO_SHORTDESCRIPTION);
    }
    public function getFitnessPromoBanner()
    {
        $uploadDir = Mage::getBaseDir('media') . DIRECTORY_SEPARATOR . 'fitnesspromos' . DIRECTORY_SEPARATOR;
        $image_url = Mage::getStoreConfig(self::FITNESS_PROMO_BANNER);
        if ($image_url && file_exists($uploadDir . $image_url)) {
            return Mage::getBaseUrl('media') .'fitnesspromos/'. $image_url;
        }
        return '';
    }
    public function getFitnessPromoImage()
    {
        $uploadDir = Mage::getBaseDir('media') . DIRECTORY_SEPARATOR . 'fitnesspromos' . DIRECTORY_SEPARATOR;
        $image_url = Mage::getStoreConfig(self::FITNESS_PROMO_IMAGE);
        if ($image_url && file_exists($uploadDir . $image_url)) {
            return Mage::getBaseUrl('media') .'fitnesspromos/'. $image_url;
        }
        else {
            return false;
        }
    }
    public function getFitnessMobilePromoBanner()
    {
        return Mage::getBaseUrl('media') .'fitnesspromos/'. Mage::getStoreConfig(self::FITNESS_PROMO_MOBILEBANNER);
    }
    public function getFitnessCalculatorBanner()
    {
        return Mage::getBaseUrl('media') .'fitnesspromos/'. Mage::getStoreConfig(self::FITNESS_CALCULATOR_BANNER);
    }
    public function getFitnessCalculatorMobileBanner()
    {
        return Mage::getBaseUrl('media') .'fitnesspromos/'. Mage::getStoreConfig(self::FITNESS_CALCULATOR_MOBILEBANNER);
    }

    public function getObjetivos()
    {
        $cacheId = 'janaq_objetivos';
        $cats = array();

        if (false !== ($data = Mage::app()->getCache()->load($cacheId))) {
            $data = unserialize($data);
        } else {
            $category_parent = 8;
            $_category = Mage::getModel('catalog/category')->load($category_parent);    
            $_categories = $_category
                    ->getCollection()
                    ->addAttributeToSelect(array('url_key', 'name'))
                    ->addIdFilter($_category->getChildren());
            foreach ($_categories as $key => $category) {
                $cats[$key]['id'] = $category->getId();
                $cats[$key]['name'] = $category->getName();
            }
            $data = $cats;
            //$data = json_decode($cats, true);
            $cacheLifetime = 864000;
            Mage::app()->getCache()->save(serialize($data), $cacheId, array("COLLECTION_DATA"), $cacheLifetime);
        }
        return $data;
    }

    public function getArrayObjetivos()
    {
        $options = array();
        $objetivos = $this->getObjetivos();
        foreach ($objetivos as $value => $label) {
           $options[$value] = $label['name'];
        }
        return $options;
    }
}