<?php
/**
 * @author janaq
 */
class Janaq_FitnessCalculator_Block_Fitnesscalculator extends Mage_Catalog_Block_Product_Abstract
{
	protected $_helper;

	public function __construct()
	{
		parent::__construct();
		$this->_helper = Mage::helper('fitnesscalculator');
	}

	public function getProduct()
	{
		if ($this->_helper->getFitnessPromoEnable()) {
            $sku = $this->_helper->getFitnessPromoSKU();
            return Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
        }
        return false;
	}

	public function getProductTitle()
	{
		if ($this->_helper->getFitnessPromoTitle()) {
			return $this->_helper->getFitnessPromoTitle();
		}
		else {
			return $this->getProduct()->getName();
		}
	}

	public function getProductShortDescription()
	{
		if ($this->_helper->getFitnessPromoShortDescription()) {
			return $this->_helper->getFitnessPromoShortDescription();
		}
		else {
			return $this->getProduct()->getShortDescription();
		}
	}

	public function getProductDescription()
	{
		if ($this->_helper->getFitnessPromoDescription()) {
			return $this->_helper->getFitnessPromoDescription();
		}
		else {
			return $this->getProduct()->getFeatures();
		}
	}

	public function getProductImage()
	{
		if ($this->_helper->getFitnessPromoImage()) {
			return $this->_helper->getFitnessPromoImage();
		}
		else {
			$image = $this->helper('catalog/image')->init($this->getProduct(), 'small_image')->resize(400,400);
			return $image;
		}
	}
}