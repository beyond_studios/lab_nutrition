<?php
class Janaq_FitnessCalculator_Block_Adminhtml_Fitnesscalculator extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        // The blockGroup must match the first half of how we call the block, and controller matches the second half
        // ie. fitnesscalculator/adminhtml_fitnesscalculator
        $this->_blockGroup = 'fitnesscalculator';
        $this->_controller = 'adminhtml_fitnesscalculator';
        $this->_headerText = $this->__('Fitness Calculator');
        
        parent::__construct();
        
        $this->_removeButton('add');
    }
}