<?php
class Janaq_FitnessCalculator_Block_Adminhtml_Fitnesscalculator_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Init class
     */
    public function __construct()
    {  
        $this->_blockGroup = 'fitnesscalculator';
        $this->_controller = 'adminhtml_fitnesscalculator';
     
        parent::__construct();
     
        $this->_updateButton('save', 'label', $this->__('Save Fitness'));
        $this->_updateButton('delete', 'label', $this->__('Delete Fitness'));
    }  
     
    /**
     * Get Header text
     *
     * @return string
     */
    public function getHeaderText()
    {  
        if (Mage::registry('fitnesscalculator_fitnesscalculator')->getFitnesscalculatorId()) {
            return $this->__('Edit Fitness');
        }  
        else {
            return $this->__('New Fitness');
        }  
    }  
}