<?php
class Janaq_FitnessCalculator_Block_Adminhtml_Fitnesscalculator_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
         
        // Set some defaults for our grid
        $this->setDefaultSort('fitnesscalculator_id');
        $this->setId('fitnesscalculator_grid');
        $this->setDefaultDir('desc');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }
    
    protected function _getCollectionClass()
    {
        // This is the model we are using for the grid
        return 'fitnesscalculator/fitnesscalculator_collection';
    }
     
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel($this->_getCollectionClass());
        $this->setCollection($collection);
        
        return parent::_prepareCollection();
    }
     
    protected function _prepareColumns()
    {
        $helper = Mage::helper('sales');
        $objetivos = Mage::helper('fitnesscalculator')->getArrayObjetivos();
        // Add the columns that should appear in the grid

        $this->addColumn('fitnesscalculator_id', array(
            'header'    => $helper->__('ID'),
            'align'  =>'right',
            'width'  => '30px',
            'index'  => 'fitnesscalculator_id',
            'type'  => 'text'
        ));

        $this->addColumn('email', array(
            'header'    => $helper->__('Email'),
            'width'  => '80px',
            'index'  => 'email',
            'type'  => 'text',
        ));

        $this->addColumn('genero', array(
            'header'    => $helper->__('Gender'),
            'width'  => '80px',
            'index'  => 'genero',
            'type'  => 'text',
        ));

        $this->addColumn('edad', array(
            'header'    => $helper->__('Edad'),
            'width'  => '80px',
            'index'  => 'edad',
            'type'  => 'text',
        ));

        $this->addColumn('estatura', array(
            'header'    => $helper->__('Estatura'),
            'align'  =>'left',
            'index'  => 'estatura',
            'width'  => '200px',
            'type'  => 'text'
        ));

        $this->addColumn('peso', array(
            'header'    => $helper->__('Weight'),
            'width'  => '100px',
            'index'  => 'peso',
            'type'  => 'text',
        ));

        $this->addColumn('objetivo_id', array(
            'header'    => $helper->__('Objetivo'),
            'align'  => 'left',
            'width'  => '80px',
            'index'  => 'objetivo_id',
            'type'      => 'options',
            'options'    => $objetivos,
        ));

        $fitnesscalculatorId = $this->getRequest()->getParam('fitnesscalculator',0);
        $this->addColumn('action',
            array(
                'header'    =>  $helper->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getFitnesscalculatorId',
                'actions'   => array(
                    array(
                        'caption'   => $helper->__('View'),
                        'url'       => array('base'=> '*/*/edit/'.$fitnesscalculatorId),
                        'field'     => 'fitnesscalculator_id'
                    )),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'fitnesscalculator',
                'is_system' => true,
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
         
        return parent::_prepareColumns();
    }

    protected function _prepareMassaction(){
        $this->setMassactionIdField('fitnesscalculator_id');
        $this->getMassactionBlock()->setFormFieldName('fitnesscalculator');

        $this->getMassactionBlock()->addItem('delete', array(
            'label'     => Mage::helper('fitnesscalculator')->__('Delete'),
            'url'       => $this->getUrl('*/*/massDelete'),
            'confirm'   => Mage::helper('fitnesscalculator')->__('Are you sure?')
        ));
        $statuses_[0] = array('label'=>'', 'value'=>'');
        $objetivos = Mage::helper('fitnesscalculator')->getArrayObjetivos();
        $statuses = $statuses_ + $objetivos;

        $this->getMassactionBlock()->addItem('objetivo_id', array(
            'label'=> Mage::helper('fitnesscalculator')->__('Cambiar Objetivo'),
            'url'   => $this->getUrl('*/*/massStatus', array('_current'=>true)),
            'additional' => array(
                'visibility' => array(
                    'name'  => 'objetivo_id',
                    'type'  => 'select',
                    'class' => 'required-entry',
                    'label' => 'Objetivo',
                    'values'=> $statuses
                ))
        ));

        return $this;
    }
     
    public function getRowUrl($row)
    {
        // This is where our row data will link to
        return $this->getUrl('*/*/edit', array('fitnesscalculator_id' => $row->getFitnesscalculatorId()));
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
}