<?php
class Janaq_FitnessCalculator_Block_Adminhtml_Fitnesscalculator_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Init class
     */
    public function __construct()
    {  
        parent::__construct();
     
        $this->setId('fitnesscalculator_fitnesscalculator_form');
        $this->setTitle($this->__('Fitness Calculator Information'));
    }  
     
    /**
     * Setup form fields for inserts/updates
     *
     * return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $model = Mage::registry('fitnesscalculator_fitnesscalculator');
        $helper = Mage::helper('sales');
        $objetivos = Mage::helper('fitnesscalculator')->getArrayObjetivos();

        $style = 'background:transparent;border: 0;padding: 4px 0;';
     
        $form = new Varien_Data_Form(array(
            'id'        => 'edit_form',
            'action'    => $this->getUrl('*/*/save', array(
                'fitnesscalculator_id' => $this->getRequest()->getParam('fitnesscalculator_id'))),
            'method'    => 'post'
        ));

        $fieldset = $form->addFieldset('base_fieldset', array(
            'legend'    => $helper->__('Fitness Calculator Information'),
            'class'     => 'fieldset-wide',
        ));
     
        if ($model->getFitnesscalculatorId()) {
            $fieldset->addField('fitnesscalculator_id', 'hidden', array(
                'name' => 'fitnesscalculator_id',
            ));
        }

        $fieldset->addField('email', 'text', array(
            'name'      => 'email',
            'label'     => $helper->__('Email'),
            'title'     => $helper->__('Email'),
            'required'  => false,
            'readonly'  => true,
            'style'     => $style,
        ));

        $fieldset->addField('genero', 'label', array(
            'name'      => 'genero',
            'label'     => $helper->__('Gender'),
            'title'     => $helper->__('Gender'),
            'required'  => false,
        ));

        $fieldset->addField('edad', 'text', array(
            'name'      => 'edad',
            'label'     => $helper->__('Edad'),
            'title'     => $helper->__('Edad'),
            'required'  => true,
            'readonly'  => true,
            'style'     => $style,
        ));

        $fieldset->addField('estatura', 'text', array(
            'name'      => 'estatura',
            'label'     => $helper->__('Estatura'),
            'title'     => $helper->__('Estatura'),
            'required'  => true,
            'readonly'  => true,
            'style'     => $style,
        ));

        $fieldset->addField('peso', 'text', array(
            'name'      => 'peso',
            'label'     => $helper->__('Weight'),
            'title'     => $helper->__('Weight'),
            'required'  => true,
            'readonly'  => true,
            'style'     => $style,
        ));
     
        $fieldset->addField('objetivo_id', 'select', array(
            'name'      => 'objetivo_id',
            'label'     => $helper->__('Objetivo'),
            'title'     => $helper->__('Objetivo'),
            'required'  => true,
            'values'    => $objetivos,
        ));
     
        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);
     
        return parent::_prepareForm();
    }  
}