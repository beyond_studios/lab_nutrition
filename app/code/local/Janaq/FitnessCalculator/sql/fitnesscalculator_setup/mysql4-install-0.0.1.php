<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

/** Create table 'janaq_videotutorial_video' */
$table = $installer->getConnection()
    ->newTable($installer->getTable('customer_fitnesscalculator'))
    ->addColumn('fitnesscalculator_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'ID')
    ->addColumn('genero', Varien_Db_Ddl_Table::TYPE_CHAR, 1, array(
        'nullable'  => false,
        ), 'Sexo')
    ->addColumn('edad', Varien_Db_Ddl_Table::TYPE_VARCHAR, 100, array(
        'nullable'  => false,
        ), 'Edad')
    ->addColumn('estatura', Varien_Db_Ddl_Table::TYPE_VARCHAR, 100, array(
        'nullable' => false,
        'unsigned'  => true,
        'nullable'  => false,
        ), 'Estatura')
    ->addColumn('peso', Varien_Db_Ddl_Table::TYPE_VARCHAR, 100, array(
        'nullable'  => false,
        ), 'Peso')
    ->addColumn('objetivo_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 100, array(
        'nullable'  => false,
        ), 'Objetivo Id')
    ->addColumn('email', Varien_Db_Ddl_Table::TYPE_VARCHAR, 250, array(
        'nullable'  => false,
        ), 'Email');
$installer->getConnection()->createTable($table);

$installer->endSetup();