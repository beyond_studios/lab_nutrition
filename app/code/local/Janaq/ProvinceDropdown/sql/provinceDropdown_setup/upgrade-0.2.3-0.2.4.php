<?php
$installer = $this;
$installer->startSetup();

$installer->run("
	ALTER TABLE `{$this->getTable('directory_country_region_postcode')}` ADD COLUMN `region_id` VARCHAR(6) CHARACTER SET utf8 DEFAULT NULL ;
	ALTER TABLE `{$this->getTable('directory_country_region_postcode')}` ADD COLUMN `status` INT(10) DEFAULT 1 ;
    ");
$installer->endSetup();