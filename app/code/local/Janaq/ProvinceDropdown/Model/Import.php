<?php 

class Janaq_Provincedropdown_Model_Import extends Mage_Core_Model_Abstract
{
	protected $_importErrorsAjax = array();

	protected $_importErrors        = array();

	protected $_updated = 0;

	protected $_failed = array();

    public function uploadAndImport($FILES)
    {
    	if (empty($FILES)) {
            return array('error'=> true,'message'=>'Archivo Incorrecto');
        }

        $csvFile = $FILES;
        $this->_importUniqueHash    = array();
        $this->_importErrors        = array();
        $this->_importedRows        = 0;

        $io     = new Varien_Io_File();
        $info   = pathinfo($csvFile);
        $io->open(array('path' => $info['dirname']));
        $io->streamOpen($info['basename'], 'r');

        $headers = $io->streamReadCsv();

        if ($headers === false || count($headers) < 2) {
            $io->streamClose();
            
            return array('error'=> true,'message'=>'Formato de archivo de tarifas no válido');
        }

        try {

        	$rowNumber  = 1;
            $importData = array();

            while (false !== ($csvLine = $io->streamReadCsv())) {
                $rowNumber ++;
                if (empty($csvLine)) {
                    continue;
                }

                $row = $this->_getImportRow($csvLine, $rowNumber);

                if ($row !== false) {
                    $importData[] = $row;
                }

                if (count($importData) == 5000) {
                    $this->_saveImportData($importData);
                    $importData = array();
                }
            }
            $this->_saveImportData($importData);

            $io->streamClose();
        	
        } catch (Mage_Core_Exception $e) {
        	$adapter->rollback();
            $io->streamClose();
            return array('error'=> true,'message'=>$e->getMessage());
        } catch (Exception $e) {
        	$adapter->rollback();
            $io->streamClose();
            Mage::logException($e);
            
            return array('error'=> true,'message'=>'Se ha producido un error al importar');
        }

        if ($this->_importErrors) {
            
            return array('error'=> true,'message'=>Mage::helper('shipping')->__('El archivo no se ha importado. Consulte los siguientes errores: </br>%s', implode("</br>", $this->_importErrors)));
        }
        $message = 'No existen registros para importar';
        $failed = false;
        if ($this->_updated > 0) {
        	$message = 'Se actualizaron exitosamente '.$this->_updated. ' distritos';
        }
        if ($this->_failed) {
        	$failed = 'Los siguentes distritos no se encontraron : '.implode(", ", $this->_failed);
        }
    	return array('error'=> false ,'message'=>$message,'failed'=>$failed);

    }
    protected function _getImportRow($row, $rowNumber = 0)
    {
    	if (count($row) < 2) {
            $this->_importErrors[] = Mage::helper('shipping')->__('La columna tiene formato inválido #%s', $rowNumber);
            return false;
        }
        foreach ($row as $k => $v) {
            $row[$k] = trim($v);
        }
        //Validate code
        $code = $row[0];
        //validate name
        $name = $row[1];

        if (!$code) {
            $this->_importErrors[] = Mage::helper('shipping')->__('El codigo "%s" tiene formato inválido en la columna  #%s.', $row[0], $rowNumber);
        }

        if (!$name) {
            $this->_importErrors[] = Mage::helper('shipping')->__('El nombre de distrito "%s" no se ha completado en la columna  #%s.', $row[0], $rowNumber);
        }

        return array(
            $code,
            $name
        );
    }

    protected function _saveImportData(array $data)
    {
        $collection = Mage::getModel('provincedropdown/province')->getCollection();
        foreach( $collection as $item )
        {
            $item->delete();
        }

        foreach ($data as $row) {
            $model = Mage::getModel('provincedropdown/province');
            $model->setRegionCode($row[0])
                ->setCode((string)$row[1])
                ->setDefaultName((string)$row[1]);

            $model->save();
            $this->_updated++;
        }

        return $this;
    }
}