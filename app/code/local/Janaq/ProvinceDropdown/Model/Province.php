<?php

class Janaq_ProvinceDropdown_Model_Province extends Mage_Core_Model_Abstract
{
  public function _construct()
  {
    parent::_construct();
    $this->_init('provincedropdown/province');
  }
}