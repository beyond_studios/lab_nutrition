<?php
class Janaq_ProvinceDropdown_Model_Resource_Province extends Mage_Core_Model_Mysql4_Abstract
{
  public function _construct()
  {
    $this->_init('provincedropdown/province', 'postcode_id');
  }
}