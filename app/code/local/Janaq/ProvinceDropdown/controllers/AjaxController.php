<?php
class Janaq_ProvinceDropdown_AjaxController extends Mage_Core_Controller_Front_Action {
    public function enderecoAction() {
       $region = $this->getRequest()->getParam('region', false);
       $type = $this->getRequest()->getParam('type', false);
       $Province_default = $this->getRequest()->getParam('province_default', false);
       $helper          = Mage::helper('provincedropdown');
       if ($type == 'shipping') {
       	$address   = Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress();
       } else {
       	 $address  = Mage::getSingleton('checkout/session')->getQuote()->getBillingAddress();
       }
      
      $defaultCity     = $address->getCity();
      $defaultPostCode = $address->getPostcode();
      $citiesOptions   = $helper->getUaeCitiesAsDropdown($Province_default,$region);
      echo $citiesOptions;
       
    }

    public function distritosAction()
    {
        $departamento  = $this->getRequest()->getParam('departamento', false);
        $response = Mage::helper('provincedropdown')->getCitiesAsDropdown($departamento);
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
    }
}