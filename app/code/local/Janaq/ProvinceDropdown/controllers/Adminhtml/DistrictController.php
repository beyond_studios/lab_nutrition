<?php

class Janaq_ProvinceDropdown_Adminhtml_DistrictController extends Mage_Adminhtml_Controller_Action
{
	public function indexAction(){
		$this->_title($this->__('System'))->_title($this->__('Distritos'));

        $this->loadLayout();
        $this->_setActiveMenu('system/district');
        $this->_addBreadcrumb(Mage::helper('adminhtml')->__('System'), Mage::helper('adminhtml')->__('Distritos'));
        $this->_addContent($this->getLayout()->createBlock('provincedropdown/adminhtml_district'));
        //$this->_addContent($this->getLayout()->createBlock('core/text')->setText($this->htmlScript()));
        $this->renderLayout();
	}
	public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody($this->getLayout()->createBlock('provincedropdown/adminhtml_district_grid')->toHtml());
    }

    public function editAction()
    {


        $id  = $this->getRequest()->getParam('id');
        $model = Mage::getModel('provincedropdown/province');

        if ($id) {
            $model->load($id);

            if (!$model->getId()) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('salesrule')->__('No existe'));
                $this->_redirect('*/*');

                return;
            }
        }
        if (!$model->getId()){
            
        }
        
        Mage::register('provincedropdown', $model);

        $this->loadLayout()
        ->_setActiveMenu('system/distict')
            ->_addBreadcrumb($id ? $this->__('Editar Distrito') : Mage::helper('adminhtml')
                ->__('Nuevo Distrito'), $id ? Mage::helper('adminhtml')
                ->__('Editar Distrito') : Mage::helper('adminhtml')->__('Nuevo Distrito'))
            ->_addContent($this->getLayout()->createBlock('provincedropdown/adminhtml_district_edit')->setData('action', $this->getUrl('*/*/save')))
            ->renderLayout();
    }
    public function newAction() {
        $this->_forward('edit');
    }

    public function editimportAction(){

        $this->loadLayout()
        ->_setActiveMenu('system/config')
            ->_addBreadcrumb($this->__('Importar Distritos'), Mage::helper('adminhtml')->__('Importar Distritos'))
            
            ->_addContent($this->getLayout()->createBlock('provincedropdown/adminhtml_district_import_edit'))
            ->renderLayout();

    }
    public function saveimportAction(){

        if ($data = $this->getRequest()->getPost())
        {
            $result = array();
            if ($_FILES['file_districts']['name']){
                if ($_FILES['file_districts']['type'] == 'application/vnd.ms-excel' || $_FILES['file_districts']['type'] == 'text/csv' ){
                    $result = Mage::getModel('provincedropdown/import')->uploadAndImport($_FILES['file_districts']['tmp_name']);
                }else{
                    $result['error'] = true;
                    $result['message'] ='Formato de archivo Incorrecto.';
                }
                if (isset($result['failed'])) {
                    if ($result['failed']) {
                        Mage::getSingleton('core/session')->addError($result['failed']);
                    }
                }
                if (!$result['error']) {
                    Mage::getSingleton('core/session')->addSuccess($result['message']);
                }else{
                    Mage::getSingleton('core/session')->addError($result['message']);
                }
            }else{
                Mage::getSingleton('core/session')->addError('Por favor carge un archivo .csv');
            }
            
        }

        $this->_redirect('*/*/editimport');
    }

    public function saveAction(){
        if ($data = $this->getRequest()->getPost()) {
            $id = $this->getRequest()->getParam('id');
            //$data = $data['send_data'];
            //$date = Mage::getModel('core/date')->date('Y-m-d H:i:s');
            $district = explode('-', $data['code']);
            

            //$validate_servientrega = Mage::helper('provincedropdown')->validateUbigeoServientrega(trim($data['region_code']),trim($district[1]),true);
            $validate_servientrega = true;

            if ($validate_servientrega) {
                $region = Mage::getModel('directory/region')->load($data['region_id']);
                $state_name = $region->getName(); 

                   $model = Mage::getModel('provincedropdown/province');
                   $model->setRegionCode($state_name)
                    ->setCode($data['code'])
                    ->setRegionId($data['region_id'])
                    ->setDefaultName($data['code'])
                    ->setStatus($data['status'])
                    ->setId($id);

                    $model->save();

                    Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('provincedropdown')->__('Registro guardado.'));
                
            }else{
                
                    Mage::getSingleton('adminhtml/session')->addError(Mage::helper('provincedropdown')->__('Error : Esta Privincia - Distrito no existe en SERVIENTREGA'));
                
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
            
        }
        $this->_redirect('*/*/');
    }
    public function massDeleteAction() {
        $ids = $this->getRequest()->getParam('district');
        if (!is_array($ids)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                $i = 0;
                foreach ($ids as $id) {
                    $model = Mage::getModel('provincedropdown/province')->load($id);
                    if ($model->getId()) {
                        $model->delete();
                        $i++;
                    }
                    
                    
                }
                $this->_getSession()->addSuccess(
                        $this->__('Un total de %d registro(s) fueron eliminados', $i)
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function massUpdateAction() {
        $ids = $this->getRequest()->getParam('district');
        if (!is_array($ids)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                $i = 0;
                foreach ($ids as $id) {
                    $model = Mage::getModel('provincedropdown/province')->load($id);

                    

                    if ($model->getId()) {

                        $region = Mage::getModel('directory/region')->load($model['region_code'],'code');

                        if ($region->getId()) {
                            $model->setRegionId($region->getRegionId());
                            $model->save();
                            $i++;
                        }
                        
                    }
                    
                    
                }
                $this->_getSession()->addSuccess(
                        $this->__('Un total de %d registro(s) fueron eliminados', $i)
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function deleteAction() {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('provincedropdown/province');

                $model->setId($this->getRequest()->getParam('id'))
                        ->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('El registro fue eliminado.'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    protected function _isAllowed() {
        return Mage::getSingleton('admin/session')->isAllowed('system');
    }

}