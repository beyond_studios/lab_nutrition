<?php
/**
 * @category   Janaq
 * @package    Janaq_ProvinceDropdown
 * @author     contacto@janaq.com

 */
class Janaq_ProvinceDropdown_Helper_Data extends Mage_Core_Helper_Abstract
{
    protected $_provinceJson;
    public function getProvinceJson(){

        if (!$this->_provinceJson) {

            $cacheKey = 'DIRECTORY_PROVINCES_JSON_STORE' . (string)Mage::app()->getStore()->getId();

            if (Mage::app()->useCache('config')) {
                $json = Mage::app()->loadCache($cacheKey);
            }
            if (empty($json)) {
                $regionsIds = array();

                $collection = Mage::getResourceModel('provincedropdown/province_collection');
                $collection->addFieldToFilter('status','1');
                $collection->getSelect()->order('code','DESC');

                $districts= array();
                foreach ($collection as $district) {
                    if (!$district->getId()) {
                        continue;
                    }
                    if (!$district->getRegionCode()) {
                        continue;
                    }
                    if (!$district->getRegionId()) {
                        continue;
                    }
                    $districts[$district->getRegionId()][$district->getCode()] = array(
                        'code' => $district->getCode(),
                        'name' => $this->__($district->getCode())
                    );
                }
                $json = Mage::helper('core')->jsonEncode($districts);
                    if (Mage::app()->useCache('config')) {
                        Mage::app()->saveCache($json, $cacheKey, array('config'));
                    }
            }

            $this->_provinceJson = $json;

        }
        return $this->_provinceJson;

    }
    public function getUaeCities($region)
    {
        $model = Mage::getResourceModel('provincedropdown/province_collection')->addFieldToFilter('region_code',$region);

        $model->getSelect()->order('default_name','DESC');

        $province = array();
        foreach ($model->getData() as  $provinces) {
            $province[]=$provinces['code'];
        }
        return $model->getData();
    }

    public function getUaeCitiesAsDropdown($selectedCity = '', $region = '')
    {
        $cities = $this->getUaeCities($region);
        $options = '';
        foreach($cities as $city){
            $isSelected = $selectedCity == $city['default_name'] ? ' selected="selected"' : null;
            $district = explode('-', $city['code']);
            if ($this->validateUbigeoServientrega($region,trim($district[1]), true)) {
                $options .= '<option value="' . $city['code'] . '"' . $isSelected . '>' . $city['default_name'] . '</option>';
            }

        }
        return $options;
    }

    public function getCitiesAsDropdown($region)
    {
        $cities = $this->getUaeCities($region);
        $_provincias = array();
        foreach ($cities as $value) {
            $_provincias[$value['code']] = $value['default_name'];
        }
        return $_provincias;
    }

    public function validateUbigeoServientrega($region,$district,$tipo=false){
        $collectionUbigeoServientrega = Mage::getResourceModel('servientrega/servientregaubigeo_collection');
        //$collectionUbigeoServientrega->setOrder('nombre_departamento', 'DESC');
        if (!$tipo) {
            $collectionUbigeoServientrega->addFieldToFilter('estado_ciudad', array('like' => 'ACTIVO'));
        }
        $collectionUbigeoServientrega->addFieldToFilter('nombre_departamento', array('like' => $region));
        $collectionUbigeoServientrega->addFieldToFilter('nombre_distrito', array('like' => $district));

        $status = false;

        foreach ($collectionUbigeoServientrega as $dis) {
            $status = true;
        }
        //Mage::log($status);
        return $status;
    }

    public function getStatus(){

        $status = array(array('value'=>'1','label' => 'Habilitado'),array('value' => '0','label' => 'Deshabilitado'));

        return $status;
    }

    public function getRegionPerArray($country = 'PE')
    {


        $collection = Mage::getResourceModel('directory/region_collection')
            ->addCountryFilter($country)
            ->load()
            ->toOptionArray();
        $i = 0;
        foreach ($collection as $values)
        {
              //Mage::log($values);
            if ($i  != 0) {
                $resArr[$i]['value']=$values['label'];
            }else {
                $resArr[$i]['value']='';
            }

              $resArr[$i]['label']=$values['label'];
              $i=$i+1;
        }
        return $resArr;
    }

    public function getRegionPerArray2($country = 'PE')
    {


        $collection = Mage::getResourceModel('directory/region_collection')
            ->addCountryFilter($country)
            ->load()
            ->toOptionArray();
        $i = 0;
        foreach ($collection as $values)
        {
              //Mage::log($values);
            if ($i  != 0) {
                $resArr[$i]['value']=$values['value'];
            }else {
                $resArr[$i]['value']='';
            }

          $resArr[$i]['label']=$values['label'];
          $i=$i+1;
        }
        return $resArr;
    }

}
