<?php
 
class Janaq_ProvinceDropdown_Block_Adminhtml_District extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'provincedropdown';
        $this->_controller = 'adminhtml_district';
        $this->_headerText = Mage::helper('adminhtml')->__('Sistema - Distritos');

        $this->_addButton('edit_import', array(
            'label'     => Mage::helper('sales')->__('Importar Distritos'),
            'onclick'   => "location.href='".$this->getUrl('*/district/editimport')."'",
            'class'     => '',
        ));
        
    	parent::__construct();
    	//$this->_removeButton('add');
    }
}