<?php
class janaq_ProvinceDropdown_Block_Adminhtml_District_Import_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Init class
     */
    public function __construct()
    {  
        parent::__construct();
        $this->setId('provincedropdown_importform');
        $this->setTitle('Importar Distritos');
    }
     
    /**
     * Setup form fields for inserts/updates
     *
     * return Mage_Adminhtml_Block_Widget_Form
     */
    
    protected function _prepareForm()
    {  
        $form = new Varien_Data_Form(array(
            'id'        => 'edit_form',
            'action'    => $this->getUrl('*/*/saveimport', array('integrador_id' => $this->getRequest()->getParam('integrador_id'))),
            'method'    => 'post',
            'enctype' => 'multipart/form-data'
        ));
     
        $fieldset = $form->addFieldset('base_fieldset', array(
            'legend'    => 'Detalle',
            'class'     => 'fieldset-wide',
        ));

        $fieldset->addField('file_districts', 'file', array(
          'label'     => Mage::helper('adminhtml')->__('Cargar Archivo'),
          'value'  => 'Upload',
          'name'      => 'file_districts',
          'disabled' => false,
          'readonly' => true,
          'tabindex' => 1
        ));
     
        $form->setUseContainer(true);
        $this->setForm($form);  
     
        return parent::_prepareForm();
    }  
}