<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 03/11/2016
 * Time: 03:11 PM
 */
class Janaq_ProvinceDropdown_Block_Adminhtml_District_Import_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Init class
     */
    public function __construct()
    {
        $this->_blockGroup = 'provincedropdown';
        $this->_controller = 'adminhtml_district_import';

        parent::__construct();

        $this->_updateButton('save', 'label', $this->__('Importar Distritos'));
        $this->_removeButton('delete');
        $this->_removeButton('reset');
        $this->_removeButton('back');
        $this->_addButton('back_', array(
            'label'   => Mage::helper('catalog')->__('Volver atrás'),
            'onclick' => "setLocation('{$this->getUrl('*/district/index')}')",
            'class'   => 'back'
        ));
        
        //$this->_removeButton('save');
    }

    /**
     * Get Header text
     *
     * @return string
     */
    public function getHeaderText()
    {
        return $this->__('Importar Distritos');
    }
}