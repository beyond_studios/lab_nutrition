<?php
class Janaq_ProvinceDropdown_Block_Adminhtml_District_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Init class
     */
    public function __construct()
    {  
        parent::__construct();
        $this->setId('provincedropdown_form_detall');
        $this->setTitle('DISTRITOS');
    }  
     
    /**
     * Setup form fields for inserts/updates
     *
     * return Mage_Adminhtml_Block_Widget_Form
     */
    
    protected function _prepareForm()
    {  
        $model = Mage::registry('provincedropdown');
        $form = new Varien_Data_Form(array(
            'id'        => 'edit_form',
            'action'    => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
            'method'    => 'post'
        ));
     
        $fieldset = $form->addFieldset('base_fieldset', array(
            'legend'    => 'Detalle',
            'class'     => 'fieldset-wide',
        ));
      
     
        $fieldset->addField('postcode_id', 'hidden', array(
            'name'      => 'postcode_id',
            'label'     => 'ID',
            'onchange' => "",
            'title'     => 'ID',
        ));
        $fieldset->addField('region_code', 'hidden', array(
            'name'      => 'region_code',
            'label'     => 'region_code',
            'onchange' => "",
            'title'     => 'region_code',
        ));
        $region = Mage::helper('provincedropdown')->getRegionPerArray2('PE');
        $fieldset->addField('region_id', 'select', array(
          'label'     => 'Departamento',
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'region_id',
          'onclick' => "",
          'onchange' => "",
          'value'  => '0',
          'values' => $region,
          'disabled' => false,
          'readonly' => false,
          //'after_element_html' => '<small>Comments</small>',
          'tabindex' => 1
        ));
        
        $fieldset->addField('code', 'text', array(
            'name'      => 'code',
            'label'     => 'Provincia - Distrito',
            'onchange' => "",
            'class'     => 'required-entry',
            'title'     => 'ID',
        ));

        $status = Mage::helper('provincedropdown')->getStatus();
        $fieldset->addField('status', 'select', array(
          'label'     => 'Estado',
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'status',
          'onclick' => "",
          'onchange' => "",
          'value'  => '0',
          'values' => $status,
          'disabled' => false,
          'readonly' => false,
          //'after_element_html' => '<small>Comments</small>',
          'tabindex' => 1
        ));
        

    

        
     
        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);  
     
        return parent::_prepareForm();
    }  
}