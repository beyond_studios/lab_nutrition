<?php
 
class Janaq_ProvinceDropdown_Block_Adminhtml_District_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('provincedropdown');
        $this->setDefaultDir('DESC');
        $this->setDefaultSort('integrador_id');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }
 
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('provincedropdown/province_collection');
        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }
 
    protected function _prepareColumns()
    {
        $this->addColumn('postcode_id', array(
            'header' => 'ID',
            'index'  => 'postcode_id',
            'width' => '100px'
            
        ));
        $this->addColumn('region_code', array(
            'header' => 'Departamento',
            'index'  => 'region_code',
        ));
 
        $this->addColumn('code', array(
            'header' => 'Provincia - Distrito',
            'index'  => 'code',
            
        ));

        $status = array( 0 => 'deshabilitado',1 => 'Habilitado' );
        $this->addColumn('status', array(
            'header'    => 'Estado',
            'align'  => 'left',
            'width'  => '100px',
            'index'  => 'status',
            'type'      => 'options',
            'options'    => $status,
        ));

        $this->addColumn('action',
            array(
                'header'    => Mage::helper('adminhtml')->__('Ver'),
                'width'     => '50px',
                'type'      => 'action',
                'getter'     => 'getId',
                'actions'   => array(
                    array(
                        'caption' => Mage::helper('adminhtml')->__('Ver'),
                        'url'     => array(
                            'base'=>'*/*/edit',
                            'params'=>array('id'=>$this->getRequest()->getParam('id'))
                        ),
                        'field'   => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
            ));

        //$this->addExportType('*/*/exportInchooCsv', 'CSV');
        //$this->addExportType('*/*/exportInchooExcel', 'Excel XML');
 
        return parent::_prepareColumns();
    }
 
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    protected function _prepareMassaction(){
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('district');

        

        $this->getMassactionBlock()->addItem('delete', array(
            'label'     => Mage::helper('adminhtml')->__('Delete'),
            'url'       => $this->getUrl('*/*/massDelete'),
            'confirm'   => Mage::helper('adminhtml')->__('Are you sure?')
        ));

        $this->getMassactionBlock()->addItem('update', array(
            'label'     => Mage::helper('adminhtml')->__('Actualizar'),
            'url'       => $this->getUrl('*/*/massUpdate'),
            'confirm'   => Mage::helper('adminhtml')->__('Are you sure?')
        ));

        return $this;
    }
}