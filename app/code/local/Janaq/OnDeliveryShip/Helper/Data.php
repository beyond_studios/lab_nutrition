<?php
/**
 * @category   Janaq
 * @package    Janaq_OnDeliveryShip
 * @author     contacto@janaq.com
 
 */
class Janaq_OnDeliveryShip_Helper_Data extends Mage_Core_Helper_Abstract
{

    // AUTH SERVICES
    const TIME_FROM = "carriers/ondeliveryship/time_from";
    const TIME_TO = "carriers/ondeliveryship/time_to";
    const DISABLED_DAYS = "carriers/ondeliveryship/disabled_days";
    const HOLIDAYS = "carriers/ondeliveryship/holidays";
    const TERMS = "carriers/ondeliveryship/terms";
    const DELIVERY_TIME = "carriers/ondeliveryship/delivery_time";

    public function getTimeFrom()
    {
        return Mage::getStoreConfig(self::TIME_FROM, Mage::app()->getStore());
    }
    public function getTimeTo()
    {
        return Mage::getStoreConfig(self::TIME_TO, Mage::app()->getStore());
    }
    public function getDisabledDays()
    {
        return Mage::getStoreConfig(self::DISABLED_DAYS, Mage::app()->getStore());
    }
    public function getHolidays()
    {
        return Mage::getStoreConfig(self::HOLIDAYS, Mage::app()->getStore());
    }
    public function getTerms()
    {
        return Mage::getStoreConfig(self::TERMS, Mage::app()->getStore());
    }
    public function getDeliveryTime()
    {
        return Mage::getStoreConfig(self::DELIVERY_TIME, Mage::app()->getStore());
    }

}