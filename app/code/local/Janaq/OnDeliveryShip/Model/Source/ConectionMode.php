<?php 

class Janaq_OnDeliveryShip_Model_Source_ConectionMode
{
    public function toOptionArray()
    {
        $options =  array();
        $options[] = array(
                   'value' => 'test',
                   'label' => 'Test'
         );
         $options[] = array(
                   'value' => 'production',
                   'label' => 'Production'
         );

        return $options;
    }
}