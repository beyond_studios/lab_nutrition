<?php
/**
 * @category   Janaq
 * @package    Janaq_AllDayDelivery
 * @author     contacto@janaq.com

 */
class Janaq_AllDayDelivery_Helper_Data extends Mage_Core_Helper_Abstract
{
    // List of Values
    const TIME_FROM = "carriers/alldaydelivery/time_from";
    const TIME_TO = "carriers/alldaydelivery/time_to";
    const DISABLED_DAYS = "carriers/alldaydelivery/disabled_days";
    const HOLIDAYS = "carriers/alldaydelivery/holidays";
    const TERMS = "carriers/alldaydelivery/terms";
    const DELIVERY_TIME = "carriers/alldaydelivery/delivery_time";
    const ALLDAY_ORDER_TYPE = "carriers/alldaydelivery/order_types";

    //General Config
    public function getHolidays()
    {
        return Mage::getStoreConfig(self::HOLIDAYS, Mage::app()->getStore());
    }
    public function getTimeFrom()
    {
        return Mage::getStoreConfig(self::TIME_FROM, Mage::app()->getStore());
    }
    public function getTimeTo()
    {
        return Mage::getStoreConfig(self::TIME_TO, Mage::app()->getStore());
    }
    private function getDate($format = "Y-m-d")
    {
      return Mage::getModel('core/date')->date($format);
    }
    public function getValidateClose(){
        $to_date =  $this->getDate('Y-m-d');
        $to_time = explode(',',$this->getTimeTo());

        $from_date =  $this->getDate('Y-m-d');
        $from_time = explode(',',$this->getTimeFrom());

        $to_hour =  $to_time[0]; $to_min = $to_time[1]; $to_sug = $to_time[2];
        $from_hour =  $from_time[0]; $from_min = $from_time[1]; $from_sug = $from_time[2];

        $to_date = $to_date.' '.$to_hour.':'.$to_min.':'.$to_sug;
        $to_strtotime = strtotime($to_date);

        $from_date = $from_date.' '.$from_hour.':'.$from_min.':'.$from_sug;
        $from_strtotime = strtotime($from_date);

        $date_today = $this->getDate('Y-m-d H:i:s');

        $today_strtotime = strtotime($date_today);

        if ($from_strtotime < $today_strtotime && $to_strtotime > $today_strtotime) {
            return true;
        } else {
            return false;
        }
    }
    public function getHours(){
      $hours = array();
      $from_date =  $this->getDate('H:i');

      $to_time = explode(',',$this->getTimeTo());
      $from_time = explode(',',$this->getTimeFrom());
      $week = $this->getDate('w');
      if ($week == '0') { // domingo
         $to_time[0] = '11';
         $from_time[0] = '19';
      }

      $to_hour = $to_time[0];
      $from_hour = $from_time[0];

      $time = explode(':',$from_date);

      for ($i= $from_hour; $i <= $to_hour ; $i++) {
        $aditional = 0;
        if ( (int)$time[0] == $i ) {
          $aditional = 1 ;
        }
        if (($time[0] + $aditional) < $i ) {
          $aux = sprintf("%02d", $i);
          $hours[$aux] = $aux .':00';
        }
      }

      if (!count($hours)) {
        $hours = array('' => 'Se encuentra fuera de horario.' );
      }
       return $hours;
    }
    public function getDisabledDays()
    {
        return Mage::getStoreConfig(self::DISABLED_DAYS, Mage::app()->getStore());
    }
    public function getTerms()
    {
        return Mage::getStoreConfig(self::TERMS, Mage::app()->getStore());
    }

    public function getDeliveryTime()
    {
        return Mage::getStoreConfig(self::DELIVERY_TIME, Mage::app()->getStore());
    }

    public function isHolidays() {
      $holidays = explode(',', $this->getHolidays());
      if (count($holidays)) {
        $date = $this->getDate('Y-m-d');
        $next_date = date('Y-m-d',strtotime ( '+1 day' , strtotime ( $date ) ) );
        $_date = explode('-',$date);
        $_next_date = explode('-',$next_date);
        $year = $this->getDate('Y');
        if ( $this->searchHoliday((int)$_next_date[2].'/'.(int)$_next_date[1]) ) {
          return false; //(3) true suguiente dia feriado.
        }
        foreach ($holidays as $value) {
          $holiday = explode('/', $value);
          $next_holiday = $year.'-'.$holiday[1].'-'.$holiday[0];
          $next_holiday = date('Y-m-d',strtotime ( '+1 day' , strtotime ( $next_holiday ) ) );
          $_next_holiday = explode('-', $next_holiday);

          if ((int)$holiday[0] == (int)$_date[2] && (int)$holiday[1] == (int)$_date[1] ) {
              if ($this->searchHoliday((int)$_next_holiday[2].'/'.(int)$_next_holiday[1]) && (int)$_next_holiday[2] == (int)$_next_date[2] && (int)$_next_holiday[1] == (int)$_next_date[1]) {
                return true; //(2) doble feriado.
              } else {
                return true; //(1) dia feriado.
              }
          }
        }
      }
      return false; //(4) dia no feriado.
    }

    private function searchHoliday($holiday){
      $holiday = explode('/',$holiday);
      $holiday_1 = str_pad($holiday[0],2,'0',STR_PAD_LEFT);
      $holiday_2 = str_pad($holiday[1],2,'0',STR_PAD_LEFT);
      $holiday = $holiday_1.'/'.$holiday_2;
      $holidays = explode(',',$this->getHolidays());
      if (in_array($holiday, $holidays) || in_array($holiday, $holidays)) {
        return true;
      } else {
        return false;
      }
    }

    public function getOrderTypes() {
      $types = explode(",", Mage::getStoreConfig(self::ALLDAY_ORDER_TYPE, Mage::app()->getStore()));
      $types_text = unserialize(Mage::getStoreConfig("carriers/urbaner/ordertypes_text", Mage::app()->getStore()));

      $order_types = array();
      if ($types_text) {
        $types_with_text = array();

        foreach ($types_text as $key => $value) {
          $t_key = explode("-", $value['ordertype_id'])[0];
          $types_with_text[$t_key] = $value['text'];
        }
        foreach ($types as $key => $order_type) {
          $order_key = explode("-", $order_type);

            $order_types[$order_key[0]]['id'] = $key;
            $order_types[$order_key[0]]['name'] = $order_key[1];
            $order_types[$order_key[0]]['text'] = $types_with_text[$order_key[0]];
        }
      } else {
        foreach ($types as $order_type) {
          $order_key = explode("-", $order_type);
          $order_types[$order_key[0]]['id'] = $order_key[0];
          $order_types[$order_key[0]]['name'] = $order_key[1];
          $order_types[$order_key[0]]['text'] = $order_key[1];
        }
      }

      return $order_types;
    }

}
