<?php 


class Janaq_AllDayDelivery_Model_Days 
{
	public function toArray()
    {
        return array(
            -1 => '',
            0 => Mage::helper('adminhtml')->__('Domingo'),
            1 => Mage::helper('adminhtml')->__('Lunes'),
            2 => Mage::helper('adminhtml')->__('Martes'),
            3 => Mage::helper('adminhtml')->__('Miercoles'),
            4 => Mage::helper('adminhtml')->__('Jueves'),
            5 => Mage::helper('adminhtml')->__('Viernes'),
            6 => Mage::helper('adminhtml')->__('Sábado'),
        );
    }
    public function toOptionArray()
    {
        return array(
            array('value' => -1, 'label' => ''),
            array('value' => 0, 'label' => Mage::helper('adminhtml')->__('Domingo')),
            array('value' => 1, 'label' => Mage::helper('adminhtml')->__('Lunes')),
            array('value' => 2, 'label' => Mage::helper('adminhtml')->__('Martes')),
            array('value' => 3, 'label' => Mage::helper('adminhtml')->__('Miercoles')),
            array('value' => 4, 'label' => Mage::helper('adminhtml')->__('Jueves')),
            array('value' => 5, 'label' => Mage::helper('adminhtml')->__('Viernes')),
            array('value' => 6, 'label' => Mage::helper('adminhtml')->__('Sabado')),
        );
    }
}