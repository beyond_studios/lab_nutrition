<?php
/**
 * Created by PhpStorm.
 * User: HOME
 * Date: 12/12/2016
 * Time: 22:52
 */

class Janaq_AllDayDelivery_Model_Carrier_Alldaydelivery
    extends Mage_Shipping_Model_Carrier_Abstract
    implements Mage_Shipping_Model_Carrier_Interface
{
    protected $_code = 'alldaydelivery';

    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        if (!Mage::getStoreConfig('carriers/'.$this->_code.'/active')) {
            return false;
        }
        /** @var Janaq_AllDayDelivery_Helper_Data */
        $allday_helper = Mage::helper('alldaydelivery');
        if (!$allday_helper->getValidateClose() || $allday_helper->isHolidays()) {
          return false;
        }

        $freeQty = 0;
        $maxWeight = Mage::helper('urbaner')->getMaxWeight();

        if ($request->getAllItems()) {
            $freePackageValue = 0;
            foreach ($request->getAllItems() as $item) {
                if ($item->getProduct()->isVirtual() || $item->getParentItem()) {
                    continue;
                }
                $isShipSeparately = $item->isShipSeparately();

                if ($item->getFreeShipping() && !$isShipSeparately) {
                    $freeShipping = is_numeric($item->getFreeShipping()) ? $item->getFreeShipping() : 0;
                    $freeQty += $item->getQty() - $freeShipping;
                    $freePackageValue += $item->getBaseRowTotal();
                }
            }

            $oldValue = $request->getPackageValue();
            $request->setPackageValue($oldValue - $freePackageValue);
        }

        $oldWeight = $request->getPackageWeight();
        $oldQty = $request->getPackageQty();
        //Don't show alldaydelivery method if package weight is upper than maxWeight
        if ($oldWeight > $maxWeight) {
            return false;
        }

        $request->setPackageQty($oldQty - $freeQty);

        $result = $this->_getModel('shipping/rate_result');
        $rate = $this->getRate($request);

        $request->setPackageWeight($oldWeight);
        $request->setPackageQty($oldQty);

        $coordenadas_mapa = array();
        $coordenadas_mapa['nombre'] = $request->getDestPostcode();
        $request->setCoordinates($coordenadas_mapa);
        $destStreet = str_replace("\n", " ", $request->getDestStreet());

        if (!empty($rate) && $rate['price'] >= 0) {
            $method = $this->_getModel('shipping/rate_result_method');

            $method->setCarrier($this->_code);
            $method->setCarrierTitle($this->getConfigData('title'));

            $method->setMethod($this->_code);
            $method->setMethodTitle($this->getConfigData('name'));

            if ($request->getFreeShipping() === true || ($request->getPackageQty() == $freeQty)) {
                $shippingPrice = 0;
            } else {
                //$shippingPrice = $this->getFinalPriceWithHandlingFee($rate['price']);

                /*URBANER PRICE API*/
                $shippingPrice = $this->_getPriceUrbaner($rate);
                /**/

                $priceAditional = 0;
                $weight_base = $oldWeight - $rate['weight_base'];
                if ($weight_base > 0){
                    $priceAditional = ceil($weight_base);
                }
                $shippingPrice = $shippingPrice + $priceAditional;
            }

            $method->setPrice($shippingPrice);
            $method->setCost(0);
            $method->setDeliveryTime($rate['delivery_time']);
            $method->setCoordinates($coordenadas_mapa);
            $method->setAddressShipping($destStreet);

            $result->append($method);
        } elseif (empty($rate) && $request->getFreeShipping() === true) {
            /**
             * was applied promotion rule for whole cart
             * other shipping methods could be switched off at all
             * we must show table rate method with 0$ price, if grand_total more, than min table condition_value
             * free setPackageWeight() has already was taken into account
             */
            $request->setPackageValue($freePackageValue);
            $request->setPackageQty($freeQty);
            $rate = $this->getRate($request);
            if (!empty($rate) && $rate['price'] >= 0) {
                $method = $this->_getModel('shipping/rate_result_method');

                $method->setCarrier($this->_code);
                $method->setCarrierTitle($this->getConfigData('title'));

                $method->setMethod($this->_code);
                $method->setMethodTitle($this->getConfigData('name'));

                $method->setPrice(0);
                $method->setCost(0);
                $method->setDeliveryTime($rate['delivery_time']);

                $method->setAddressShipping($destStreet);
                $method->setCoordinates($coordenadas_mapa);

                $result->append($method);
            }
        } else {
            $error = $this->_getModel('shipping/rate_result_error');
            $error->setCarrier($this->_code);
            $error->setCarrierTitle($this->getConfigData('title'));
            $error->setErrorMessage($this->getConfigData('specificerrmsg'));
            $result->append($error);
        }

        return $result;
    }
    protected function _getPriceUrbaner($rate){
      $lat_lng = Mage::app()->getRequest()->getParam('lat_lng');
      $vehiculo = Mage::app()->getRequest()->getParam('tipo_vehiculo');
      $urbaner_post = Mage::app()->getRequest()->getParam('urbaner');
      $urb_type = Mage::app()->getRequest()->getParam('urb_type');
      $almacen_id = Mage::app()->getRequest()->getParam('almacen_id');
      if ($lat_lng && $vehiculo) {
          $urbaner = Mage::getModel('urbaner/service');
          $price = $urbaner->getPriceShipping($lat_lng,$vehiculo,$urb_type,$almacen_id);
          $rate['price'] = $price;
      }elseif ($urbaner_post) {
          $urbaner = Mage::getModel('urbaner/service');
          $price = $urbaner->getPriceShipping($urbaner_post['lat_lng'],$urbaner_post['vehicletypes'],$urbaner_post['ordertypes'],$urbaner_post['almacen_id']);
          $rate['price'] = $price;
      }
      return $rate['price'];
    }
    protected function _getModel($modelName)
    {
        return Mage::getModel($modelName);
    }
    public function getRate(Mage_Shipping_Model_Rate_Request $request)
    {
        return Mage::getResourceModel('alldaydelivery/alldaydeliveryrate')->getRate($request);
    }

    public function getAllowedMethods()
    {
        return array('alldaydelivery' => $this->getConfigData('name'));

        /*return array(
            'express'   =>  'delivery Express',
            'sameday'   =>  'Same Day delivery',
            'nextday'   =>  'Next Day delivery'
        );*/
    }
}
