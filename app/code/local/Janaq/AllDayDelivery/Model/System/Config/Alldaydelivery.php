<?php
/**
 * Created by PhpStorm.
 * User: HOME
 * Date: 11/12/2016
 * Time: 23:27
 */

class Janaq_AllDayDelivery_Model_System_Config_Alldaydelivery extends Mage_Core_Model_Config_Data
{
    public function _afterSave()
    {
        Mage::getResourceModel('alldaydelivery/alldaydeliveryrate')->uploadAndImport($this);
    }
}