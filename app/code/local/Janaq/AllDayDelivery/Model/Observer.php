<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 10/05/2019
 * Time: 12:12 PM
 */

class Janaq_AllDayDelivery_Model_Observer extends Mage_Payment_Model_Method_Abstract
{
    public function afterSave(Varien_Event_Observer $event)
    {
        $params = Mage::app()->getRequest()->getParams();
        $shipping_method = $params['shipping_method'];

        if ($shipping_method == 'alldaydelivery_alldaydelivery') {
            $address = $params["alldaydelivery"]["address"];
            $lat_lng = $params["alldaydelivery"]["lat_lng"];
            $vehicletypes = $params["alldaydelivery"]["vehicletypes"];
            $ordertypes = $params["alldaydelivery"]["ordertypes"];
            $order_almacen_id = $params["alldaydelivery"]["almacen_id"];
            $hours = $params["alldaydelivery"]["hours"];

            $order = $event->getOrder();
            $order->setData('urbaner_addresses',$address);
            $order->setData('urbaner_lat_lng', $lat_lng);
            $order->setData('urbaner_vehicle', $vehicletypes);
            $order->setData('urbaner_order_type', $ordertypes);
            $order->setData('urbaner_order_hours', $hours);
            $order->setData('urbaner_almacen_id', $order_almacen_id);

            //$params['order'] = $order;
            //Mage::dispatchEvent('order_alldaydelivery_integrador_save', $params);
        }
    }

    public function saveOrderAllDayDelivery($observer){
        Mage::log('guarda AllDayDelivery ' , null, 'alldaydelivery.log');
        //Mage::log('Entra a guardar orden para AllDayDelivery');
        $shipping_method = $observer['order']->getShippingMethod();

        if ($shipping_method == 'alldaydelivery_alldaydelivery') {
            $date = Mage::getModel('core/date')->date('Y-m-d H:i:s');
            $modelUpdate = Mage::getModel('urbaner/integrador');
            $increment_id = $observer['order']->getIncrementId();
            $modelUpdate->load($increment_id,'order_id');
            if ($increment_id && !$modelUpdate->getId()) {
                $model = Mage::getSingleton('urbaner/integrador');
                $model->setOrderId($increment_id);
                $model->setTypeTask('Order');
                $model->setStatus('pendiente');
                $model->setCreatedAt($date);
                $model->setUpdateAt($date);
            } else {
                $model->setId($modelUpdate->getId());
                $model->setUpdateAt($date);
            }
            try {
                $model->save();
            } catch (Exception $e) {
                Mage::log('Error al guardar registro de Urbaner (Tareas)');
            }
        }
        return $this;
    }
}
