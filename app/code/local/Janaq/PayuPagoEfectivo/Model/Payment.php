<?php
require_once(Mage::getBaseDir('lib') . '/PayU/PayU.php');
/**
 * Janaq Payment PayuPagoEfectivo Model
 */
class Janaq_PayuPagoEfectivo_Model_Payment extends Mage_Payment_Model_Method_Abstract
{

    /**
     * Payment method code
     *
     * @var string
     */
    protected $_code  = 'payupagoefectivo';

    /**
     * Cash On Delivery payment block paths
     *
     * @var string
     */
    protected $_formBlockType = 'payupagoefectivo/form';
    protected $_infoBlockType = 'payupagoefectivo/info';

    protected $_canAuthorize    = true;
    protected $_canSaveCc       = false;
    /**
     * Get instructions text from config
     *
     * @return string
     */
    public function getInstructions()
    {
        return trim($this->getConfigData('instructions'));
    }
    
    /**
     * Stores custom data for payment method in the database.
     *
     * @return Mage_Payment_Model_Method_Abstract
     */
    public function assignData($data)
    {
        parent::assignData($data);
        
        if (!($data instanceof Varien_Object)) {
            $data = new Varien_Object($data);
        }
        $info = $this->getInfoInstance();
        $info->setMoneyAmount(null);
        $info->save();
        return $this;
    }
 
    /**
     * Validates custom data for payment method.
     *
     * @return Mage_Payment_Model_Method_Abstract
     */
    public function validate()
    {
        parent::validate();
 
        $info = $this->getInfoInstance();
//        $money = $info->getMoneyAmount();
//
//        if(empty($money)){
//            $errorCode = 'invalid_data';
//            $errorMsg = $this->_getHelper()->__('Money Amount is a required field');
//        }
//
//        if($errorMsg){
//            Mage::throwException($errorMsg);
//        }
        return $this;
    }
    public function authorize(Varien_Object $payment, $amount)
    {
        $errorMsg = false;
        $order = $payment->getOrder();
        $result = $this->callApi($payment,$amount,'authorize');
        if($result === false) {
            $errorCode = 'Invalid Data';
            $errorMsg = $this->_getHelper()->__('Error Processing the request');
        } else {
            //Mage::log($result, null, $this->getCode().'.log');
            if($result['status']){
                $order_comment = '';
                foreach($result as $key=>$value){
                    $order_comment .= "<br/>$key: $value";
                }
                $payment->setTransactionId($result['transactionId']);
                    
                $payment->setTransactionAdditionalInfo(Mage_Sales_Model_Order_Payment_Transaction::RAW_DETAILS,$result); //use this in case you want to add some extra information
                $payment->setIsTransactionClosed(0);
                $payment->getOrder()->setStatus('pending_pagoefectivo');
                $payment->getOrder()->addStatusToHistory('pending_pagoefectivo', $order_comment);

            }else{
                Mage::throwException('Transaccion no generada  : '.$result['message']);
            }
        }

        return $this;
    }
    private function callApi(Varien_Object $payment, $amount,$type){
        $response = array();
        $payu_response = array();
        $order = $payment->getOrder();
        $url = $this->getPayuUrl();
        $payu_response = $this->addDataPayu($url,$payment,$order);

        if ($payu_response->code == "SUCCESS") {
            
                //TODO: Realizar una accion en el caso de que el estado de la transaccion este aprobado.
                $response["status"] = true;
                $response['orderId'] = $payu_response->transactionResponse->orderId;
                $response["message"] = $payu_response->code;
                $response["transactionId"] = $payu_response->transactionResponse->transactionId;
                $response["state"] = $payu_response->transactionResponse->state;
                if ($payu_response->transactionResponse->state == "PENDING") {
                    $response["pendingReason"] = $payu_response->transactionResponse->pendingReason;
                    $response["responseCode"] = $payu_response->transactionResponse->responseCode;
                    $response["trazabilityCode"] = $payu_response->transactionResponse->trazabilityCode;
                    $response["authorizationCode"] = $payu_response->transactionResponse->authorizationCode;
                    $response["operationDate"] = $payu_response->transactionResponse->operationDate;
                    $response["URL_PAYMENT_RECEIPT_HTML"] = $payu_response->transactionResponse->extraParameters->URL_PAYMENT_RECEIPT_HTML;
                    $response["URL_PAYMENT_RECEIPT_PDF"] = $payu_response->transactionResponse->extraParameters->URL_PAYMENT_RECEIPT_PDF;
                    $response["REFERENCE"] = $payu_response->transactionResponse->extraParameters->REFERENCE;
                    $response["BAR_CODE"] = $payu_response->transactionResponse->extraParameters->BAR_CODE;
                    $response["additionalInfo"] = $payu_response->transactionResponse->additionalInfo;
                    $response["BAR_CODE"] = $payu_response->transactionResponse->extraParameters->BAR_CODE;
                    $response["EXPIRATION_DATE"] = $payu_response->transactionResponse->extraParameters->EXPIRATION_DATE;
                    
                }elseif ($payu_response->transactionResponse->state == "ERROR"){
                    $response["status"] = false;
                    $response["message"] = $payu_response->transactionResponse->responseMessage;
                }
                $response["responseCode"] = $payu_response->transactionResponse->responseCode;
        } else {
            //TODO
            $response["status"] = false;
            $response["message"] = $payu_response->code;
        }
        //Mage::log($payu_response);
        return $response;
        //return array('status'=>1,'transaction_id' => time() , 'fraud' => rand(0,1));
    }

    public function addDataPayu($url,$payment,$orderPayment){
          
            $merchant_id = Mage::getStoreConfig( 'payment/payupagoefectivo/merchant_id' );
            $secure_key = Mage::getStoreConfig( 'payment/payupagoefectivo/secure_key' );
            $api_login = Mage::getStoreConfig( 'payment/payupagoefectivo/api_login' );
            $account_id = Mage::getStoreConfig( 'payment/payupagoefectivo/account_id' );
            $gateway_url = Mage::getStoreConfig( 'payment/payupagoefectivo/payu_url' );
            $transaction_mode = Mage::getStoreConfig( 'payment/payupagoefectivo/transaction_mode' );
            
            $merchant_id = Mage::getStoreConfig('payment/payupagoefectivo/merchant_id');

            $test = "false";
            if($transaction_mode == 'test') {
               $test = "true"; 
            }

            //$orderIncrementId = $checkout->getLastRealOrderId();
            $orderIncrementId = $orderPayment->getIncrementId();
            $order = Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);

            $currency   = $order->getOrderCurrencyCode();

            $BAddress = $order->getBillingAddress();

            $paymentAmount = number_format($order->getGrandTotal(),2,'.','');
            $tax = number_format($order->getTaxAmount(),2,'.','');

            $taxReturnBase = number_format(($paymentAmount - $tax),2,'.','');
            if($tax == 0) $taxReturnBase = 0;


            $ProductName = '';
            $items = $order->getAllItems();
            if ($items)
            {
                foreach($items as $item)
                {
                    if ($item->getParentItem()) continue;
                    $ProductName .= $item->getName() . '; ';
                }
            }
            $ProductName = rtrim($ProductName, '; ');
            //Limitar nombres de productos para evitar error en la descripcion
            if (strlen($ProductName) >= 255) {
              $ProductName = (substr($ProductName, 0, 250))."...";
            }

            $signature = md5($secure_key . '~' . $merchant_id . '~' . $orderIncrementId . '~' . $paymentAmount . '~' . $currency );

            //$test = 0;
            $language   = 'es';

            $fecha = Mage::getModel('core/date')->date('Y-m-d H:i:s');
            //Mage::log($fecha);
            $fechaExpire = strtotime('+2 day',strtotime($fecha));
            //Mage::log($fechaExpire);

            PayU::$apiKey = $secure_key; //Ingrese aquí su propio apiKey.
            PayU::$apiLogin =  $api_login; //Ingrese aquí su propio apiLogin.
            PayU::$merchantId = $merchant_id; //Ingrese aquí su Id de Comercio.
            PayU::$language = SupportedLanguages::ES; //Seleccione el idioma.
            PayU::$isTest = $test; //Dejarlo True cuando sean pruebas.

            Environment::setPaymentsCustomUrl($url);

            //Environment::setReportsCustomUrl("https://stg.api.payulatam.com/reports-api/4.0/service.cgi");

            /*$parameters = array(
                //Ingrese aquí el identificador de la cuenta.
                PayUParameters::ACCOUNT_ID => $account_id,
                //Ingrese aquí el código de referencia.
                PayUParameters::REFERENCE_CODE => $orderIncrementId,
                //Ingrese aquí la descripción.
                PayUParameters::DESCRIPTION => $ProductName,
                
                // -- Valores --
                //Ingrese aquí el valor.        
                PayUParameters::VALUE => $paymentAmount,
                //Ingrese aquí la moneda.
                PayUParameters::CURRENCY => "PEN",
                
                //Ingrese aquí el email del comprador.
                PayUParameters::BUYER_EMAIL => $order->getShippingAddress()->getEmail(),
                //Ingrese aquí el nombre del pagador.
                PayUParameters::PAYER_NAME => $order->getBillingAddress()->getFirstname().' '.$order->getBillingAddress()->getLastname(),
                //Ingrese aquí el documento de contacto del pagador.
                PayUParameters::PAYER_DNI=> $order->getBillingAddress()->getFax(),
                
                //Ingrese aquí el nombre del método de pago
                PayUParameters::PAYMENT_METHOD => "BCP",
               
                //Ingrese aquí el nombre del pais.
                PayUParameters::COUNTRY => PayUCountries::PE,
                
                //Ingrese aquí la fecha de expiración.
                PayUParameters::EXPIRATION_DATE => Mage::getModel('core/date')->date('Y-m-d\TH:i:s',$fechaExpire),
                //IP del pagadador
                PayUParameters::IP_ADDRESS => Mage::helper('core/http')->getRemoteAddr(),
               
            );
                Mage::log($parameters);
            $response = PayUPayments::doAuthorizationAndCapture($parameters);*/

           $parameters = array(
                   "language"   => "es",
                   "command"    => "SUBMIT_TRANSACTION",
                   "merchant"   => array(
                      "apiKey"  => $secure_key,
                      "apiLogin"    => $api_login,
                   ),
                   "transaction"    => array(
                      "order"   => array(
                         "accountId"    => $account_id,
                         "referenceCode"    => $orderIncrementId,
                         "description"  => $ProductName,
                         "language" => "es",
                         "signature"    => $signature,
                         "notifyUrl"    => Mage::getUrl('payupagoefectivo/pagoefectivo/notify'),
                         "additionalValues" => array(
                            "TX_VALUE"  => array(
                               "value"  => $paymentAmount,
                               "currency"   => "PEN"
                            )
                         ),
                         "buyer"    => array(
                            "emailAddress"  => $order->getShippingAddress()->getEmail(),
                            'fullName' => $order->getShippingAddress()->getFirstname().' '.$order->getShippingAddress()->getLastname(),
                         )
                      ),
                      "payer" => array(
                        'fullName' => $order->getBillingAddress()->getFirstname().' '.$order->getBillingAddress()->getLastname(),
                       ),
                      "type"    => "AUTHORIZATION_AND_CAPTURE",
                      "paymentMethod"   => "PAGOEFECTIVO",
                      "expirationDate"  => Mage::getModel('core/date')->date('Y-m-d\TH:i:s',$fechaExpire),
                      "paymentCountry"  => "PE",
                      "ipAddress"   => Mage::helper('core/http')->getRemoteAddr(),
                   ),
                   "test" => $test
                );
           $parameters = json_encode($parameters);
           $httpHeader = array(
            'Content-Type: application/json; charset=UTF-8',
            'Content-Length:' . strlen($parameters),
            'Accept: application/json');
            
            //Mage:: log('URL : '. $url);
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($curl, CURLOPT_POSTFIELDS, $parameters);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_HTTPHEADER,$httpHeader);
            
            Mage::log('Send Data : ', null, $this->getCode().'.log');
            Mage::log($parameters, null, $this->getCode().'.log');
            $response = curl_exec($curl);
            if ($response) {
                $response = json_decode($response);
            }
            Mage::log('Response Data : ', null, $this->getCode().'.log');

            Mage::log($response, null, $this->getCode().'.log');
            if ($response->code == "ERROR") {
                Mage::throwException('Transaccion no generada  : '.$response->error );
            }
            /*if($response){
                $response->transactionResponse->orderId;
                $response->transactionResponse->transactionId;
                $response->transactionResponse->state;
                if($response->transactionResponse->state=="PENDING"){
                    $response->transactionResponse->pendingReason;
                    $response->transactionResponse->extraParameters->URL_PAYMENT_RECEIPT_HTML;
                    $response->transactionResponse->extraParameters->REFERENCE;
                    $response->transactionResponse->extraParameters->EXPIRATION_DATE;      
                }
                $response->transactionResponse->responseCode;         
            }*/  

            //Mage::log('res');
            
    
            //Mage::log($response); 

        return $response;
    }

    public function getPayuUrl()
    {
        return Mage::getStoreConfig( 'payment/payupagoefectivo/payu_url');
    }

}
