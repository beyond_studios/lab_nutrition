<?php

class Janaq_LastFeed_Block_Feed extends Mage_Core_Block_Abstract implements Mage_Widget_Block_Interface {

    public function __construct()
    {
        parent::__construct();
        // Block caching setup
        $this->addData(array(
            'cache_lifetime'=> 604800, // (seconds) data lifetime in the cache
            'cache_tags' => array(
                Mage_Core_Model_Store::CACHE_TAG,
                Mage_Cms_Model_Block::CACHE_TAG,
            ),
            'cache_key' => 'JNQ_LABBLOG',
        ));
    }

    /**
     * @return string
     */
    protected function _toHtml() {
        $html = '';
        $url = $this->getData('url');
        $lastn = $this->getData('lastn');

        if (empty($url) || empty($lastn)) {
            return $html;
        }

        $html = $this->getFeed($url, $lastn);
        return $html;
    }

    function getFeed($feed_url, $n) {
        try {
        $content = file_get_contents($feed_url);
        if ($content) {
            $invalid_characters = '/[^\x9\xa\x20-\xD7FF\xE000-\xFFFD]/';
            $content = preg_replace($invalid_characters, '', $content);
            $x = new SimpleXmlElement($content);
            $helper = Mage::helper('jnqadmin');

            $result = "";
            $placeholder = Mage::getModel('core/design_package')->getSkinUrl("images/blog/post-bg.png");

            $i = 0;
            foreach($x->channel->item as $entry) {
                if ($i>=$n) break;

                $dom = new domDocument;
                //$dom->loadHTML($entry->description);
                @$dom->loadHTML($entry->description);
                $dom->preserveWhiteSpace = false;
                $imgs  = $dom->getElementsByTagName("img");

                if ($imgs->item(0)) {
                    $img = $imgs->item(0)->getAttribute("src");
                    $img = utf8_decode($img);
                    $img = $helper->resizeImage($img, 350, 250, "feed");
                } else {
                    $img = "";
                }

                //$item = substr(preg_replace('/<[^>]*>/', ' ',$entry->description),0,121)."...";
                $item = $helper->text_truncate(preg_replace('/<[^>]*>/', ' ',$entry->description),90," ","...");
                //$titulo = substr($entry->title,0,55)."...";
                $titulo = $helper->text_truncate($entry->title, 50, " ", "...");

                $fecha = strftime("%Y-%m-%d", strtotime($entry->pubDate));

                $cat = $entry->category;

                $result .= "<li class='blog-item col-xs-6 col-sm-6 col-md-4 item'>
                          <div class='blog-item-content clearfix'>
                            <div class='blog-item-img'>
                                <a href='$entry->link'><img class='owl-lazy' alt='$entry->title' height='222' width='335' data-src='$img' src='$placeholder' /></a>
                                <h3 class='blog-category font-dinbek_bold'>$cat</h3>
                            </div>
                            <div class='blog-item-description'>
                                <h3 class='blog-item-title text-center no-margin'><a href='$entry->link'>$titulo</a></h3>
                                <p class='blog-item-shortdescription no-display'>$item</p>
                            </div>
                            <div class='blog-item-action no-display'>
                                <div class='blog-item-date'>$fecha</div>
                                <div class='blog-item-button-content'>
                                    <a href='$entry->link'>LEER MÁS</a>
                                </div>
                            </div>
                        </div>
                    </li>";

                $i++;
            }
            return $result;
        }
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }
}