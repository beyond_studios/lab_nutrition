<?php
/**
 * @category   Janaq
 * @package    Janaq_Urbaner
 * @author     contacto@janaq.com

 */
class Janaq_Urbaner_Helper_Data extends Mage_Core_Helper_Abstract
{
    // AUTH SERVICES
    const EMAIL_URBANER = "carriers/urbaner/auth_email";
    const PASS_URBANER = "carriers/urbaner/auth_password";

    const TIME_FROM = "carriers/urbaner/time_from";
    const TIME_TO = "carriers/urbaner/time_to";
    const DISABLED_DAYS = "carriers/urbaner/disabled_days";
    const HOLIDAYS = "carriers/urbaner/holidays";
    const TERMS = "carriers/urbaner/terms";

    // List of Values
    const URBANER_ORDER_TYPE = "carriers/urbaner/order_types";
    const URBANER_ORDER_TYPE_TEXT = "carriers/urbaner/ordertypes_text";
    const URBANER_VEHICLE_TYPE = "carriers/urbaner/vehicle_types";

    const URBANER_MAX_WEIGHT = "carriers/urbaner/delivery_time";
    const DELIVERY_TIME = "carriers/urbaner/delivery_time";
    const STATUS_CONTRA_ENTREGA = "carriers/urbaner/active_contra_entrega";
    const DISCOUNT_AMOUNT = "carriers/urbaner/discount_amount";
    const ID_STORE_DEFAULT = "carriers/urbaner/id_store_default";

    //info contact end point
    const URBANER_LAT_LNG = "carriers/urbaner/end_latlon";
    const URBANER_CONTACT_PERSON = "carriers/urbaner/contact_person";
    const URBANER_CONTACT_PHONE = "carriers/urbaner/contact_phone";
    const URBANER_CONTACT_ADDRESS = "carriers/urbaner/contact_address";
    const URBANER_CONTACT_REFERENCE = "carriers/urbaner/contact_reference";
    const URBANER_CONTACT_EMAIL = "carriers/urbaner/contact_email";

    const URL_CARRIER_TRAKING = "carriers/urbaner/carrier_tracking_url";

    // Mapas Values
    const URBANER_M_URL = "carriers/urbaner/url_mambapas";
    const URBANER_M_USER = "carriers/urbaner/user_mambapas";
    const URBANER_M_PASSWORD = "carriers/urbaner/password_mambapas";

    public $_urbaner_type_orders = array('1'=>'Express','2'=>'Express Programado','3'=>'Next Day','4'=>'Express Contraentrega.');

    //Auth
    public function getAuthUser()
    {
        return Mage::getStoreConfig(self::EMAIL_URBANER, Mage::app()->getStore());
    }
    public function getAuthPassword()
    {
        return Mage::getStoreConfig(self::PASS_URBANER, Mage::app()->getStore());
    }
    public function getHolidays()
    {
        return Mage::getStoreConfig(self::HOLIDAYS, Mage::app()->getStore());
    }
    //General Config
    public function getTimeFrom()
    {
        return Mage::getStoreConfig(self::TIME_FROM, Mage::app()->getStore());
    }
    public function getTimeTo()
    {
        return Mage::getStoreConfig(self::TIME_TO, Mage::app()->getStore());
    }
    private function getDate($format = "Y-m-d")
    {
      return Mage::getModel('core/date')->date($format);
    }
    public function getValidateClose(){
        $from_date =  $this->getDate();
        $from_time = explode(',',$this->getTimeFrom());
        $to_date =  $this->getDate();
        $to_time = explode(',',$this->getTimeTo());

        $to_hour =  $to_time[0]; $to_min = $to_time[1]; $to_sug = $to_time[2];
        $from_hour =  $from_time[0]; $from_min = $from_time[1]; $from_sug = $from_time[2];

        $from_date = $from_date.' '.$from_hour.':'.$from_min.':'.$from_sug;
        $from_strtotime = strtotime($from_date);

        $to_date = $to_date.' '.$to_hour.':'.$to_min.':'.$to_sug;
        $to_strtotime = strtotime($to_date);

        $date_today = $this->getDate('Y-m-d H:i:s');

        $today_strtotime = strtotime($date_today);

        if ($from_strtotime < $today_strtotime && $to_strtotime > $today_strtotime) {
            return true;
        } else {
            return false;
        }
    }
    public function getHours(){
      $hours = array();
      $from_date =  $this->getDate('H:i');

      $to_time = explode(',',$this->getTimeTo());
      $from_time = explode(',',$this->getTimeFrom());
      $week = $this->getDate('w');
      if ($week == '0') { // domingo
         $to_time[0] = '11';
         $from_time[0] = '19';
      }

      $to_hour = $to_time[0];
      $from_hour = $from_time[0];

      $time = explode(':',$from_date);

      for ($i= $from_hour; $i <= $to_hour ; $i++) {
        $aditional = 0;
        if ( (int)$time[0] == $i ) {
          $aditional = 1 ;
        }
        if (($time[0] + $aditional) < $i ) {
          $aux = sprintf("%02d", $i);
          $hours[$aux] = $aux .':00';
        }

      }

      if (!count($hours)) {
        $hours = array('' => 'Se encuentra fuera de horario.' );
      }
      //$hours = array('10' => $from_date,'11' =>'11:00' );
       return $hours;
    }
    public function getDisabledDays()
    {
        return Mage::getStoreConfig(self::DISABLED_DAYS, Mage::app()->getStore());
    }
    public function getTerms()
    {
        return Mage::getStoreConfig(self::TERMS, Mage::app()->getStore());
    }
    public function getMaxWeight()
    {
    	return Mage::getStoreConfig(self::URBANER_MAX_WEIGHT, Mage::app()->getStore());
    }

    public function getMapaUrl()
    {
        return Mage::getStoreConfig(self::URBANER_M_URL, Mage::app()->getStore());
    }

    public function getMapaUser()
    {
        return Mage::getStoreConfig(self::URBANER_M_USER, Mage::app()->getStore());
    }

    public function getMapaPassword()
    {
        return Mage::getStoreConfig(self::URBANER_M_PASSWORD, Mage::app()->getStore());
    }

    // List of Values from Urbaner
    private function getUrbanerService()
    {
    	return Mage::getModel('urbaner/service')->executeLogin();
    }
    public function getOrderTypesOriginal()
    {
    	$order_types = explode(",", Mage::getStoreConfig(self::URBANER_ORDER_TYPE));

      $_types = array();
      foreach ($order_types as $order_type) {
        $order_type = explode("-", $order_type);
        $_types[$order_type[0]] = $order_type[1];
      }
      return $_types;
    }
    public function getVehicleTypes()
    {
      $vehicles = explode(",", Mage::getStoreConfig(self::URBANER_VEHICLE_TYPE));

    	$_types = array();
      foreach ($vehicles as $vehicle) {
        $vehicle = explode("-", $vehicle);
        $_types[$vehicle[0]] = $vehicle[1];
      }
      //$_types = array( '2' => 'Moto','1' => 'Bicicleta', '3' => 'Auto' );
    	return $_types;
    }
    public function getOrderTypes_OLD(){
      //$types = array('1' =>'Express','2'=>'Express Programado','3'=> 'Next Day');
      $types = $this->_urbaner_type_orders;
      $_types = array();
      $week = $this->getDate('w');
      /*if ($week == '0') { //domingo 0
        //$types = array('3'=> 'Next Day');
        $_types['1'] = $types['1'];
        $_types['3'] = $types['3'];

      }elseif ($week == '6') {
        //$types = array('1' =>'Express','2'=>'Express Programado');
        //$types = array('1' =>'Express');
        $_types['1'] = $types['1'];

      }else{
        $_types['1'] = $types['1'];
        $_types['3'] = $types['3'];

      }*/
      $_types['1'] = $types['1'];
      $_types['3'] = $types['3'];
      //Mage::log($this->getValidateClose());
      $_is_holiday = $this->isHolidays();
      if (!$this->getValidateClose() || $_is_holiday == '1' ) {
         $_types = array();
         $_types['3'] = $types['3'];
      }
      if ($_is_holiday == '3') {
        $_types = array();
        $_types['1'] = $types['1'];
      }
      
      if ($this->getStatusContraEntrega()) {
        $_types['4'] = $types['4'];
      }
      
      return $_types;
    }
    public function getOrderTypes() {
      $types = explode(",", Mage::getStoreConfig(self::URBANER_ORDER_TYPE, Mage::app()->getStore()));
      $types_text = unserialize(Mage::getStoreConfig(self::URBANER_ORDER_TYPE_TEXT, Mage::app()->getStore()));
      //$is_holiday = $this->isHolidays();

      $order_types = array();
      if ($types_text) {
        $types_with_text = array();

        foreach ($types_text as $key => $value) {
          $t_key = explode("-", $value['ordertype_id'])[0];
          $types_with_text[$t_key] = $value['text'];
        }
        foreach ($types as $key => $order_type) {
          $order_key = explode("-", $order_type);

          $order_types[$order_key[0]]['id'] = $key;
          $order_types[$order_key[0]]['name'] = $order_key[1];
          $order_types[$order_key[0]]['text'] = $types_with_text[$order_key[0]];
        }
      } else {
        foreach ($types as $order_type) {
          $order_key = explode("-", $order_type);
          $order_types[$order_key[0]]['id'] = $order_key[0];
          $order_types[$order_key[0]]['name'] = $order_key[1];
          $order_types[$order_key[0]]['text'] = $order_key[1];
        }
      }
      /*if ($is_holiday == 1 ) {
        $order_types = array_intersect(array(3), $order_types);
      }
      elseif ($is_holiday == 3) {
        $order_types = array_intersect(array(1), $order_types);
      }*/

      return $order_types;
    }

    public function token(){
        $model = $this->getUrbanerService();
        return $model->auth_token;
    }

    public function getEndLatLng(){
        return Mage::getStoreConfig(self::URBANER_LAT_LNG, Mage::app()->getStore());
    }
    public function getContactPerson(){
        return Mage::getStoreConfig(self::URBANER_CONTACT_PERSON, Mage::app()->getStore());
    }
    public function getContactPhone(){
        return Mage::getStoreConfig(self::URBANER_CONTACT_PHONE, Mage::app()->getStore());
    }
    public function getContactAddress(){
        return Mage::getStoreConfig(self::URBANER_CONTACT_ADDRESS, Mage::app()->getStore());
    }
    public function getContactReference(){
        return Mage::getStoreConfig(self::URBANER_CONTACT_REFERENCE, Mage::app()->getStore());
    }
    public function getContactEmail(){
        return Mage::getStoreConfig(self::URBANER_CONTACT_EMAIL, Mage::app()->getStore());
    }

    public function getDeliveryTime()
    {
        return Mage::getStoreConfig(self::DELIVERY_TIME, Mage::app()->getStore());
    }

    public function getStatusContraEntrega()
    {
        return Mage::getStoreConfig(self::STATUS_CONTRA_ENTREGA, Mage::app()->getStore());
    }
    public function getDiscountAmount()
    {
        return Mage::getStoreConfig(self::DISCOUNT_AMOUNT, Mage::app()->getStore());
    }
    public function getIdStoreDefault()
    {
        return Mage::getStoreConfig(self::ID_STORE_DEFAULT, Mage::app()->getStore());
    }

    public function getNameStoreDefault()
    {
        $default_id = $this->getIdStoreDefault();
        $store_locator = Mage::getModel('storelocator/storelocator')->load($default_id);
        if ($store_locator && $store_locator->getId()) {
          return $store_locator->getName();
        }else{
          return '';
        }
    }

    //Tracking
    public function getUrlCarrierTracking()
    {
        return Mage::getStoreConfig(self::URL_CARRIER_TRAKING, Mage::app()->getStore());
    }
    public function getUrlTrakingUrbaner($number_traking)
    {
        $url = $this->getUrlCarrierTracking()."/tracking?data=".$number_traking;
        return $link = '<a href="'.$url.'">'.$this->__('VER DETALLES').'</a>';
    }


    public function getCoordinates(){
        /*$urbaner = Mage::getModel('urbaner/service');
        $mapas = $urbaner->getMapas();
        return $mapas;*/
        $data = [
          "data" => [[
              "id" => 214,
               "name"=>"Lima",
               "description"=>"Provincia de Lima",
               "store_code"=>"01",
               "coordinate_center"=>"-11.991961917513626,-76.98494921855271",
               "zoom_map"=>"14",
               "polygon"=>"[[-12.002300594175413,-77.13598484292419],[-12.020643998410199,-77.13821644082458],[-12.035922218187942,-77.14476103082092],[-12.03716038752492,-77.13271254792602],[-12.05015032856006,-77.14212173715026],[-12.060390743624234,-77.14894527688415],[-12.064264774125958,-77.15478683475203],[-12.07027905200461,-77.1641474508474],[-12.072990881642534,-77.17033740252054],[-12.074133964732253,-77.16507909938485],[-12.073062847006032,-77.16117145612628],[-12.067227524381982,-77.15747604265601],[-12.066552116435124,-77.15283179774673],[-12.066963918764454,-77.14843565717132],[-12.068039323137569,-77.14153165116699],[-12.06825964935456,-77.13132852807433],[-12.083472211820984,-77.09924930825622],[-12.094886311648272,-77.07967991128356],[-12.11066382372763,-77.05358738198669],[-12.116482246289461,-77.04749340310485],[-12.12213270835444,-77.04122776284606],[-12.129139673462621,-77.03479046121032],[-12.133461242765689,-77.02835315957458],[-12.139419019519714,-77.0271515299359],[-12.147348529106075,-77.02380413308526],[-12.15578124047724,-77.02337497964288],[-12.165923215503486,-77.02566022172357],[-12.169856142122333,-77.03352445855523],[-12.175540429263542,-77.03277344003106],[-12.188586561594128,-77.0074104715863],[-12.194970437688191,-77.00288491442586],[-12.2110854359701,-77.018443738369],[-12.222509791028491,-76.9927414704838],[-12.235962529189786,-76.96382457509429],[-12.236990067946248,-76.95259684815795],[-12.24394157718858,-76.93936551108749],[-12.247828785567798,-76.93359004452617],[-12.248612511138836,-76.92541131868751],[-12.243469761082796,-76.9098478008786],[-12.241001886992011,-76.9113830302635],[-12.238408169716894,-76.91205995276363],[-12.229907308936301,-76.9096157897988],[-12.22764770635589,-76.91463017955215],[-12.225637115665135,-76.91841947033794],[-12.223102229213739,-76.91966602697761],[-12.214341348765295,-76.92318910851867],[-12.161277044512543,-76.904338543562],[-12.138233768211071,-76.91262925162704],[-12.133146567754537,-76.92212158933074],[-12.163920023708274,-76.95552582040222],[-12.148481297642661,-76.9539379526654],[-12.144579521261957,-76.96445221200378],[-12.137489050089364,-76.97316402688415],[-12.119195602361657,-76.9605469156781],[-12.118993964699039,-76.9511960094789],[-12.116190859216422,-76.93927018262542],[-12.113773519255238,-76.93533124864507],[-12.099565103318687,-76.9437090184611],[-12.096182477051004,-76.93912635431718],[-12.093226420910478,-76.93264033275125],[-12.093860498113036,-76.92172822614282],[-12.086735982543034,-76.91973090196393],[-12.086754516765037,-76.91204553400178],[-12.09165936223904,-76.8965031367004],[-12.072148718983197,-76.8892633203152],[-12.072074415474988,-76.90264069014825],[-12.06490648453393,-76.9379197399993],[-12.057195570469455,-76.93752271263594],[-12.053597445501795,-76.93845606094396],[-12.045466502235206,-76.93664282722074],[-12.029729954620803,-76.92226594518434],[-12.029315887703694,-76.91617148286923],[-12.029836791312439,-76.90398082778927],[-12.026915901665426,-76.89359261716731],[-12.029468747485383,-76.88757907434132],[-12.023829281499918,-76.88104515275188],[-12.024302902276041,-76.86042420898707],[-12.011650349709978,-76.83806507292235],[-12.029421943761045,-76.76160059789663],[-12.00387565000007,-76.72084168927722],[-11.967554075444987,-76.72292296261446],[-11.966436994727442,-76.74029288776575],[-11.974388071018634,-76.75766281291703],[-11.983236948422007,-76.80270234583679],[-11.975033036212647,-76.84099350130424],[-11.97757623224806,-76.8810871012297],[-11.985178023394694,-76.89869306077429],[-11.9983102328543,-76.91187873986235],[-12.001220636567595,-76.91490425652387],[-12.003134055692573,-76.91825163826718],[-12.006625041320193,-76.91904554192098],[-12.009493254190257,-76.92383054237439],[-12.009090650012816,-76.92807904059572],[-12.008419739041942,-76.93292573185647],[-12.010015538893223,-76.93721452364218],[-12.010971906472971,-76.94038477383947],[-12.012841240203418,-76.94370522774159],[-12.011619407674676,-76.94513478471737],[-12.011342026170983,-76.94705786815189],[-12.012077017083532,-76.94844676898839],[-12.013651515576234,-76.94987858516913],[-12.013042871387098,-76.95067251903754],[-12.009711460641446,-76.95096171417794],[-12.00798560683369,-76.95233452176035],[-12.008140498353773,-76.95419183064485],[-12.006306277653598,-76.95650280709094],[-12.005602065547954,-76.95821515583032],[-12.005583284873701,-76.95855359759946],[-12.005525015029132,-76.9593105302356],[-12.005550698537887,-76.95989311928577],[-12.005762101548688,-76.96042261384957],[-12.007121507832137,-76.96095320780131],[-12.010286294534136,-76.96207340430306],[-12.011835559799577,-76.96284887440402],[-12.014892088372077,-76.96470022201538],[-12.016499555722865,-76.9675794175451],[-12.01674091169104,-76.96865766556908],[-12.017842751405185,-76.96967154057671],[-12.018722076499886,-76.97585499922252],[-12.022497622671823,-76.97627707290428],[-12.022497622671823,-76.97628243732231],[-12.024634003137482,-76.9771748644481],[-12.025445835450949,-76.97850224621493],[-12.025904730551682,-76.9797300745526],[-12.026008352260686,-76.98046499982263],[-12.025955557390708,-76.98113689318086],[-12.026155502709862,-76.98188142620745],[-12.026649261745703,-76.98275470526676],[-12.026671391022706,-76.98355712581213],[-12.027177356311354,-76.98606318913187],[-12.02750379180152,-76.98949417451183],[-12.027494441069189,-76.99296807523604],[-12.028105337727634,-76.99940089255358],[-12.015569330239819,-76.99732798503499],[-12.013274949000786,-76.99477227989371],[-12.010659001474455,-76.99837268449153],[-12.01377792126811,-77.00056775824885],[-12.013328914119464,-77.0013466256463],[-12.012110834684847,-77.0016427163759],[-12.011777652724636,-77.00177515044601],[-12.01169632478199,-77.00178856149108],[-12.011066688265819,-77.00204068913848],[-12.01005926677795,-77.00293654694946],[-12.0092617220942,-77.00305456414611],[-12.007939471757435,-77.00367683663757],[-12.00311215352701,-76.9993423868695],[-11.952357120061807,-76.96312183633239],[-11.94441791279546,-76.95390040173919],[-11.936016610930109,-76.94510812058837],[-11.927401168216674,-76.9298409868756],[-11.92382422262453,-76.92023867860229],[-11.913864686984883,-76.91338295236022],[-11.899016558861662,-76.90154368176849],[-11.891222771390506,-76.88970441117675],[-11.882017967072457,-76.90756792321594],[-11.881380154730458,-76.91787833466918],[-11.87200702828313,-76.92269558205993],[-11.87635897083741,-76.9459771563092],[-11.887430125252012,-76.96445221200378],[-11.89663737111288,-76.98698276772888],[-11.91709096914294,-76.98632830872924],[-11.927149235141417,-76.9915800739804],[-11.93653533063896,-77.00404161706359],[-11.964504491494873,-77.01131576791198],[-11.973586674149047,-77.03324550881774],[-11.984361748911773,-77.0568489481488],[-11.949428653366397,-77.04978400960357],[-11.919531649816786,-77.03543687358291],[-11.895511415306368,-77.01556036367805],[-11.885264311594005,-77.01128693416507],[-11.87770461079557,-77.01937312379272],[-11.876353720288927,-77.03957552209289],[-11.86239646001053,-77.05405945077331],[-11.856488557772353,-77.0722126413861],[-11.864954877614405,-77.10503215089233],[-11.86804524634811,-77.13819498315246],[-11.867822569467005,-77.1588412870446],[-11.862807424670375,-77.16196388125212],[-11.857834187216403,-77.16495772942693],[-11.854833034824646,-77.16647611118646],[-11.851369843822498,-77.16674994796307],[-11.825939162036006,-77.15565121851654],[-11.800464108281888,-77.13863017156513],[-11.791649294527225,-77.14770701832862],[-11.787463190600194,-77.15256187950985],[-11.78508654252907,-77.15500003893652],[-11.781439035858842,-77.15853253964127],[-11.782110778408942,-77.15916403419072],[-11.781564195692766,-77.15972042688776],[-11.780838626674928,-77.16039869442142],[-11.781666601948142,-77.16206100131643],[-11.785360070157289,-77.16463459658229],[-11.79466878732999,-77.17560754509435],[-11.79526918934543,-77.18073062717707],[-11.798226010448426,-77.18036049804908],[-11.800510666858832,-77.17810209377461],[-11.802643467896026,-77.17624603656844],[-11.805732811248255,-77.17682545657993],[-11.810103332703696,-77.17721175754235],[-11.81384369053694,-77.17558103732557],[-11.82422613520493,-77.1793149237314],[-11.841895876714593,-77.18111837401779],[-11.855785498288228,-77.16653118029029],[-11.870178385316505,-77.1577804733792],[-11.91329780770291,-77.1420037199536],[-11.929152444477952,-77.13860267892272]]",
               "tipo_servicio"=>"todos",
               "tipo_zona"=>"blajnca",
               "rango_tiempo"=>"",
               "rango_fechas"=>"",
          ]],
          "status"=>true
      ];
      
      return $data;
    }
    public function isHolidays() {
      $holidays = explode(',', $this->getHolidays());
      if (count($holidays)) {
        $date = $this->getDate('Y-m-d');
        $next_date = date('Y-m-d',strtotime ( '+1 day' , strtotime ( $date ) ) );
        $_date = explode('-',$date);
        $_next_date = explode('-',$next_date);
        $year = $this->getDate('Y');
        if ( $this->searchHoliday((int)$_next_date[2].'/'.(int)$_next_date[1]) ) {
          return false; //(3) true suguiente dia feriado.
        }
        foreach ($holidays as $value) {
          $holiday = explode('/', $value);
          $next_holiday = $year.'-'.$holiday[1].'-'.$holiday[0];
          $next_holiday = date('Y-m-d',strtotime ( '+1 day' , strtotime ( $next_holiday ) ) );
          $_next_holiday = explode('-', $next_holiday);

          if ((int)$holiday[0] == (int)$_date[2] && (int)$holiday[1] == (int)$_date[1] ) {
              if ($this->searchHoliday((int)$_next_holiday[2].'/'.(int)$_next_holiday[1]) && (int)$_next_holiday[2] == (int)$_next_date[2] && (int)$_next_holiday[1] == (int)$_next_date[1]) {
                return true; //(2) doble feriado.
              } else {
                return true; //(1) dia feriado.
              }
          }
        }
      }
      return false; //(4) dia no feriado.
    }

    private function searchHoliday($holiday){
      $_holiday = explode('/',$holiday);
      $_holiday_1 = str_pad($_holiday[0],2,'0',STR_PAD_LEFT);
      $_holiday_2 = str_pad($_holiday[1],2,'0',STR_PAD_LEFT);
      $_holiday = $_holiday_1.'/'.$_holiday_2;
      $holidays = explode(',',$this->getHolidays());
      //Mage::log($holiday);Mage::log($_holiday);
      if (in_array($holiday,$holidays) || in_array($_holiday,$holidays)) {
        return true;
      } else {
        return false;
      }
    }

    public function getStoreUponDelivery($validate_stock = false) {
      $storeId = Mage::app()->getStore()->getStoreId();
      $collection = Mage::getModel('storelocator/storelocator')->getCollection()
          ->setStoreId($storeId)
          ->addFieldToSelect(array('name', 'almacen_id_navasoft', 'phone', 'email', 'latitude', 'longtitude', 'address','radio_delivery','storelocator_id'))
          ->addFieldToFilter('upon_delivery', 1);

      $array_store = array();
      $items = $this->getItemsCart();
      
      foreach ($collection->getData() as $store) {
        if ($validate_stock) {
            if (!$this->validateStock($store['almacen_id_navasoft'], $items)) {
              continue;
            }
        }
        $_store['id'] = $store['storelocator_id'];
        $_store['name'] = $store['name'];
        $_store['almacen_id'] = $store['almacen_id_navasoft'];
        $_store['radio_delivery'] = $store['radio_delivery'];
        $_store['latitude'] = $store['latitude'];
        $_store['longtitude'] = $store['longtitude'];
        $_store['phone'] = $store['phone'];
        $_store['email'] = $store['email'];
        $_store['address'] = $store['address'];
        $array_store[] = $_store;
      }

      return $array_store;
    }

    public function getItemsCart(){
      $_items = Mage::getSingleton('checkout/cart')->getQuote()->getAllItems();
      $items = array();
      foreach ($_items as $item) {
          if ($item->getProductType() == 'simple') {
              $items[] = array('product_id'=>$item->getProductId(),'name'=>$item->getName(),'sku' => $item->getSku(),'type' =>$item->getProductType(),'qty'=>$item->getQty());
          } 
          
      }
      return $items;
    }

    public function validateStock($almacen_id,$items){
      $helper = Mage::Helper('janaq_navasoft');
      if ($helper->getStatus() && $helper->getStatusCart()) {
          $model = Mage::getModel('janaq_navasoft/generate');
          foreach ($items as $_item) {
              $params  = array('items' => $_item['sku'] ,'almacen_id' => $almacen_id );
              $_product_navasoft = $model->getStock($params);
              if ($_product_navasoft && $_product_navasoft['status']) {
                 $existencia = $_product_navasoft['stock'];
              }else{
                $existencia = 0;
              }
              if ((int)$existencia < (int)$_item['qty'] || (int)$existencia == 0) {
                return false;
              }
          }
      }
      return true;
    }

    public function validateStockPim($almacen_id,$items){
      $helper = Mage::Helper('janaq_pim');

      if ($helper->getStatus() && $helper->getStatusCart()) {
          $model = Mage::getModel('janaq_pim/service');
          foreach ($items as $_item) {
              $params  = array('sku' => $_item['sku'] ,'almacen_id' => $almacen_id );
              $_product_pim = $model->getProductPim($params);
              if ($_product_pim && $_product_pim['status']) {
                 $existencia = $_product_pim['success'][0]['stock'];
              }else{
                $existencia = 0;
              }
              if ((int)$existencia < (int)$_item['qty'] || (int)$existencia == 0) {
                return false;
              }
          }
      }
      return true;
    }

}
