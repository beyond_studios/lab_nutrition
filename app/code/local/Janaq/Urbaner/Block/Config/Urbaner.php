<?php
class Janaq_Urbaner_Block_Config_Urbaner 
    extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
    protected $_itemRenderer;

    public function _prepareToRender()
    {
        $this->addColumn('ordertype_id', array(
            'label' => Mage::helper('adminhtml')->__('Order Type Id'),
            'style' => 'width:100px',
            'renderer' => $this->_getRenderer(),
        ));
        $this->addColumn('text', array(
            'label' => Mage::helper('adminhtml')->__('Text'),
            'style' => 'width:300px',
        ));

        $this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper('adminhtml')->__('Add');
    }

    protected function _getRenderer()
    {
        if (!$this->_itemRenderer) {
            $this->_itemRenderer = $this->getLayout()->createBlock(
                'urbaner/config_adminhtml_form_field_ordertypesid', '',
                array('is_render_to_js_template' => true)
            );
        }
        return $this->_itemRenderer;
    }
 
    protected function _prepareArrayRow(Varien_Object $row)
    {
        $row->setData(
            'option_extra_attr_' . $this->_getRenderer()
                ->calcOptionHash($row->getData('ordertype_id')),
            'selected="selected"'
        );
    }
}
