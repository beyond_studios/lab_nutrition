<?php
/**
 * Created by PhpStorm.
 * User: HOME
 * Date: 12/12/2016
 * Time: 0:25
 */

class Janaq_Urbaner_Block_Field_Export extends Mage_Adminhtml_Block_System_Config_Form_Field
{

    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $buttonBlock = $this->getLayout()->createBlock('adminhtml/widget_button');

        $params = array(
            'website' => $buttonBlock->getRequest()->getParam('website')
        );

        $data = array(
            'label'     => Mage::helper('adminhtml')->__('Export CSV Urbaner'),
            'onclick'   => 'setLocation(\''.Mage::helper('adminhtml')->getUrl("*/urbaner/exporturbaner", $params) . '\')',
            'class'     => '',
        );

        $html = $buttonBlock->setData($data)->toHtml();

        return $html;
    }
}