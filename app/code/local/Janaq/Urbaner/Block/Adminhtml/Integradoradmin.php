<?php

class Janaq_Urbaner_Block_Adminhtml_Integradoradmin extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'urbaner';
        $this->_controller = 'adminhtml_integradoradmin';
        $this->_headerText = Mage::helper('adminhtml')->__('Urbaner - Integrador');

    	parent::__construct();
    	$this->_removeButton('add');
    	$this->_addButton('execute_cron', array(
            'label'   => Mage::helper('catalog')->__('Envio Manual de Pendientes'),
            'onclick' => "setLocation('{$this->getUrl('*/*/executecron')}')",
            'class'   => 'add'
        ));
    }
}
