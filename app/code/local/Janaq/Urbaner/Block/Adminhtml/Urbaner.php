<?php

class Janaq_Urbaner_Block_Adminhtml_Urbaner extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'urbaner';
        $this->_controller = 'adminhtml_urbaner';
        $this->_headerText = Mage::helper('adminhtml')->__('Sistema - Ubigeos');

    	parent::__construct();
    	//$this->_removeButton('add');
    }
}
