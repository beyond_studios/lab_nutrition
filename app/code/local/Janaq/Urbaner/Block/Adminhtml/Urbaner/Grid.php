<?php

class Janaq_Urbaner_Block_Adminhtml_Urbaner_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('urbaner');
        $this->setDefaultDir('DESC');
        $this->setDefaultSort('ubigeo_id');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('urbaner/servientregaubigeo_collection');
        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }

    protected function _prepareColumns()
    {
        $this->addColumn('ubigeo_id', array(
            'header' => 'ID',
            'index'  => 'ubigeo_id',
            'width' => '100px'

        ));
        $this->addColumn('ubigeo_code', array(
            'header' => 'CODIGO UBIGEO',
            'index'  => 'ubigeo_code',
        ));

        $this->addColumn('id_ciudad', array(
            'header' => 'Id Ciudad',
            'index'  => 'id_ciudad',

        ));

        $this->addColumn('nombre_distrito', array(
            'header' => 'Distrito',
            'index'  => 'nombre_distrito',

        ));

        $this->addColumn('nombre_departamento', array(
            'header' => 'Departamento',
            'index'  => 'nombre_departamento',

        ));

        $status = array('ACTIVO' => 'ACTIVO','INACTIVO' => 'INACTIVO' );
        $this->addColumn('estado_ciudad', array(
            'header'    => 'Estado',
            'align'  => 'left',
            'width'  => '100px',
            'index'  => 'estado_ciudad',
            'type'      => 'options',
            'options'    => $status,
        ));

        $this->addColumn('action',
            array(
                'header'    => Mage::helper('adminhtml')->__('Ver'),
                'width'     => '50px',
                'type'      => 'action',
                'getter'     => 'getId',
                'actions'   => array(
                    array(
                        'caption' => Mage::helper('adminhtml')->__('Ver'),
                        'url'     => array(
                            'base'=>'*/*/edit',
                            'params'=>array('ubigeo_id'=>$this->getRequest()->getParam('ubigeo_id'))
                        ),
                        'field'   => 'ubigeo_id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
            ));

        //$this->addExportType('*/*/exportInchooCsv', 'CSV');
        //$this->addExportType('*/*/exportInchooExcel', 'Excel XML');

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('ubigeo_id' => $row->getId()));
    }

    protected function _prepareMassaction(){
        $this->setMassactionIdField('ubigeo_id');
        $this->getMassactionBlock()->setFormFieldName('servientrega');



        $this->getMassactionBlock()->addItem('delete', array(
            'label'     => Mage::helper('adminhtml')->__('Delete'),
            'url'       => $this->getUrl('*/*/massDelete'),
            'confirm'   => Mage::helper('adminhtml')->__('Are you sure?')
        ));

        return $this;
    }
}
