<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 03/11/2016
 * Time: 03:11 PM
 */
class Janaq_Urbaner_Block_Adminhtml_Urbaner_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Init class
     */
    public function __construct()
    {
        $this->_blockGroup = 'urbaner';
        $this->_controller = 'adminhtml_urbaner';

        parent::__construct();

        //$this->_updateButton('save', 'label', $this->__('Save'));
        //$this->_removeButton('delete');
        $this->_removeButton('reset');
        $this->_removeButton('save');
    }

    /**
     * Get Header text
     *
     * @return string
     */
    public function getHeaderText()
    {

            return $this->__('Detalle Urbaner');

    }
}
