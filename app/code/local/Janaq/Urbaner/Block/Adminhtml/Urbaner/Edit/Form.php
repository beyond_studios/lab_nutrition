<?php
class Janaq_Urbaner_Block_Adminhtml_Urbaner_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Init class
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('urbaner_form_detall');
        $this->setTitle('Detalle Urbaner');
    }

    /**
     * Setup form fields for inserts/updates
     *
     * return Mage_Adminhtml_Block_Widget_Form
     */

    protected function _prepareForm()
    {
        $model = Mage::registry('urbaner');
        $form = new Varien_Data_Form(array(
            'id'        => 'edit_form',
            'action'    => $this->getUrl('*/*/save', array('urbaner_id' => $this->getRequest()->getParam('ubigeo_id'))),
            'method'    => 'post'
        ));

        $fieldset = $form->addFieldset('base_fieldset', array(
            'legend'    => 'Detalles',
            'class'     => 'fieldset-wide',
        ));

        $fieldset->addField('order_id', 'label', array(
            'name'      => 'order_id',
            'label'     => 'IncrementID',
            'onchange' => "",
            'title'     => 'ID',
        ));
        $fieldset->addField('data_sent', 'label', array(
            'name'      => 'data_sent',
            'label'     => 'Datos de Envio',
            'onchange' => "",
            'title'     => 'ID',
        ));


        $fieldset->addField('response', 'label', array(
            'name'      => 'response',
            'label'     => 'Datos de Respuesta',
            'onchange' => "",
            'title'     => 'ID',
        ));

        $fieldset->addField('status', 'label', array(
            'name'      => 'status',
            'label'     => 'Estado Integrador',
            'onchange' => "",
            'title'     => 'ID',
        ));

        $fieldset->addField('tracking_id', 'label', array(
            'name'      => 'tracking_id',
            'label'     => 'Tracking ID',
            'onchange' => "",
            'title'     => 'ID',
        ));

        $fieldset->addField('urbaner_tracking', 'label', array(
            'name'      => 'urbaner_tracking',
            'label'     => 'Tracking',
            'onchange' => "",
            'title'     => 'ID',
        ));



        $fieldset->addField('urbaner_status', 'label', array(
            'name'      => 'urbaner_status',
            'label'     => 'Estado Urbaner',
            'onchange' => "",
            'title'     => 'ID',
        ));

        $fieldset->addField('urbaner_code', 'label', array(
            'name'      => 'urbaner_code',
            'label'     => 'Código Urbaner',
            'onchange' => "",
            'title'     => 'ID',
        ));

        $fieldset->addField('created_at', 'label', array(
            'name'      => 'created_at',
            'label'     => 'Fecha creación',
            'onchange' => "",
            'title'     => 'ID',
        ));

        $fieldset->addField('update_at', 'label', array(
            'name'      => 'update_at',
            'label'     => 'Fecha Actualización',
            'onchange' => "",
            'title'     => 'ID',
        ));

        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
