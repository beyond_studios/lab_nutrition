<?php

class Janaq_Urbaner_Block_Adminhtml_Integradoradmin_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('janaq_urbaner_integrador_grid');
        $this->setDefaultDir('DESC');
        $this->setDefaultSort('order_id');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('urbaner/integrador_collection');
        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }

    protected function _prepareColumns()
    {
        $this->addColumn('urbaner_id', array(
            'header' => 'ID',
            'index'  => 'urbaner_id',

        ));
        $this->addColumn('order_id', array(
            'header' => 'OrderId',
            'index'  => 'order_id',

        ));

        $this->addColumn('urbaner_status', array(
            'header'   => 'Estado Urbaner',
            'index'    => 'urbaner_status',
            'sortable' => false
        ));
        $this->addColumn('urbaner_code', array(
            'header'   => 'Código Urbaner',
            'index'    => 'urbaner_code',
            'sortable' => false
        ));
        $this->addColumn('response', array(
            'header'   => 'Datos Respuesta',
            'index'    => 'response',
            'sortable' => false
        ));
        $this->addColumn('created_at', array(
            'header'   => 'CreatedAt',
            'index'    => 'created_at',
            'sortable' => false
        ));
        $this->addColumn('update_at', array(
            'header'   => 'UpdatedAt',
            'index'    => 'update_at',
            'sortable' => false
        ));
        $this->addColumn('status', array(
            'header'   => 'Estado de envío',
            'index'    => 'status',
            'sortable' => false
        ));



        //$this->addExportType('*/*/exportInchooCsv', 'CSV');
        //$this->addExportType('*/*/exportInchooExcel', 'Excel XML');

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('urbaner_id' => $row->getId()));
    }

    protected function _prepareMassaction(){
        $this->setMassactionIdField('integrador_id');
        $this->getMassactionBlock()->setFormFieldName('integrador');

        $statuses = array('pendiente'=>'pendiente','inhabilitado'=>'inhabilitado');

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
            'label'=> Mage::helper('adminhtml')->__('Cambiar de estado'),
            'url'   => $this->getUrl('*/*/massStatus', array('_current'=>true)),
            'additional' => array(
                'visibility' => array(
                    'name'  => 'status',
                    'type'  => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('adminhtml')->__('Status'),
                    'values'=> $statuses
                ))
        ));

        //$this->getMassactionBlock()->addItem('delete', array(
        //    'label'     => Mage::helper('adminhtml')->__('Delete'),
        //    'url'       => $this->getUrl('*/*/massDelete'),
        //    'confirm'   => Mage::helper('adminhtml')->__('Are you sure?')
        //));

        return $this;
    }
}
