<?php

class Janaq_Urbaner_Adminhtml_Urbaner_IntegradoradminController extends Mage_Adminhtml_Controller_Action
{

    public function indexAction()
    {
    	$this->_title($this->__('Urbaner'))->_title($this->__('Integrador List'));

        $this->loadLayout();
        $this->_setActiveMenu('urbaner/urbaner');
        $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Urbaner'), Mage::helper('adminhtml')->__('Integrador List'));
        $this->_addContent($this->getLayout()->createBlock('urbaner/adminhtml_integradoradmin'));
        $this->renderLayout();
    }
    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody($this->getLayout()->createBlock('urbaner/adminhtml_integradoradmin_grid')->toHtml());
    }
    public function editAction()
    {


       $id  = $this->getRequest()->getParam('urbaner_id');
       $model = Mage::getModel('urbaner/integrador');

       if ($id) {
           $model->load($id);

           if (!$model->getId()) {
               Mage::getSingleton('adminhtml/session')->addError(Mage::helper('urbaner')->__('No existe'));
               $this->_redirect('*/*');

               return;
           }
       }
       if (!$model->getId()){

       }

       Mage::register('urbaner', $model);

       $this->loadLayout()
       ->_setActiveMenu('urbaner/integradoradmin')
           ->_addBreadcrumb($id ? $this->__('Integrador') : Mage::helper('adminhtml')
               ->__('Detalles'), $id ? Mage::helper('adminhtml')
               ->__('Detalles') : Mage::helper('adminhtml')->__('Detalle Urbaner'))
           ->_addContent($this->getLayout()->createBlock('urbaner/adminhtml_urbaner_edit')->setData('action', $this->getUrl('*/*/save')))
           ->renderLayout();
    }

    public function exportInchooCsvAction()
    {
        $fileName = 'ServientregaAdmin.csv';
        $grid = $this->getLayout()->createBlock('urbaner/adminhtml_integradoradmin_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    public function exportInchooExcelAction()
    {
        $fileName = 'ServientregaAdmin.xml';
        $grid = $this->getLayout()->createBlock('urbaner/adminhtml_integradoradmin_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    public function massStatusAction() {
        $integrador_ids = $this->getRequest()->getParam('integrador');
        if (!is_array($integrador_ids)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                $i = 0;
                foreach ($integrador_ids as $id) {
                    $model = Mage::getModel('urbaner/integrador')->load($id);
                    if ($model->getStatus() !='completado') {
                        $model->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                            $i++;
                    }

                }
                $this->_getSession()->addSuccess(
                        $this->__('Total of %d record(s) were successfully updated', $i)
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function executecronAction(){

      $cron = Mage::getModel('urbaner/cron')->run();
      $this->_getSession()->addSuccess(
                        $this->__('Se a ejecutado el envio de items.')
                );
      $this->_redirect('*/*/index');
    }

    public function generateOrderAction(){

        $increment_id= $this->getRequest()->getParam('increment_id', false);
        //Mage::log($increment_id);
        $result = array();
        $model = Mage::getModel('urbaner/integrador');

        $model->load($increment_id,'order_id');
        if ($model->getId()) {
            //Mage::log($model->getStatus());
            if ($model->getStatus() == 'completado') {
                $result['error'] = false;
                $result['message'] = 'La orden ya ha sido enviado en Urbaner';
            }elseif ($model->getStatus() == 'pendiente') {
                $result['error'] = false;
                $result['message'] = 'La orden ya esta en espera de envió a Urbaner';
            }
        }else{
            //Mage::log("SE CREARA ESTE REGISTRO");
            $date = Mage::getModel('core/date')->date('Y-m-d H:i:s');
            $model->setId(null);
            $model->setOrderId($increment_id);
            $model->setTypeTask('Order');
            $model->setStatus('pendiente');
            $model->setCreatedAt($date);
            $model->setUpdateAt($date);
            try {
                $model->save();
                $result['error'] = false;
                $result['message'] = 'La orden se ha agregado a la lista de envios para Urbaner';
            } catch (Exception $e) {
                 $result['error'] = false;
                $result['message'] = $e->getMessage();
            }
        }
        //$result = array('error'=> false,'message'=>'exito order '. $increment_id);
        //Mage::Log($result);
        
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    public function calculatepriceAction(){

        $lat_lng = $this->getRequest()->getParam('lat_lng', false);
        $vehiculo = $this->getRequest()->getParam('vehiculo', false);
        $urb_type = $this->getRequest()->getParam('urb_type', false);
        $almacen_id = $this->getRequest()->getParam('almacen_id', false);
        $result = array();
        $urbaner = Mage::getModel('urbaner/service');

        $price = $urbaner->getPriceShipping($lat_lng,$vehiculo,$urb_type,$almacen_id);
        if (is_numeric($price)) {
            $result['error'] = false;
            $result['price'] = Mage::helper('core')->currency($price, true, false);
            $result['only_price'] = $price;
        }else{
          $result['error'] = true;
          $result['price'] = $price;
        }
        

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));

    }

    public function changestoreadminAction(){


        $order_id = $this->getRequest()->getParam('order_id', false);
        $lat_lng = $this->getRequest()->getParam('lat_lng', false);
        $vehiculo = $this->getRequest()->getParam('vehiculo', false);
        $urb_type = $this->getRequest()->getParam('urb_type', false);
        $almacen_id = $this->getRequest()->getParam('almacen_id', false);
        $order = Mage::getModel('sales/order')->load($order_id);
        $result = array('error' =>true);
        if ($order) {

          $urbaner = Mage::getModel('urbaner/service');
          $price = $urbaner->getPriceShipping($lat_lng,$vehiculo,$urb_type,$almacen_id);
          if ($price) {

            $order->setShippingAmount($price);
            $order->setBaseShippingAmount($price);
            $subtotal = $order->getSubtotal();
            $basesubtotal = $order->getBaseSubtotal();
            $order->setGrandTotal($subtotal + $price);
            $order->setBaseGrandTotal($basesubtotal+ $price);
            
            $order->setUrbanerAlmacenId($almacen_id);

            $order->save();

            

            $result['error'] = false;

          }else{
            $result['error'] = true;
          }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));

          
        }
        
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('urbaner/integradoradmin');
    }

}
