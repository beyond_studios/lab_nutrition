<?php

$installer = $this;
$installer->startSetup();

// Add new Attribute group
/*$groupName = 'Default';
$entityTypeId = $installer->getEntityTypeId('catalog_product');
$attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
$installer->addAttributeGroup($entityTypeId, $attributeSetId, $groupName, 20);

$installer->addAttribute('catalog_product', 'high', array(
            'group'           => $groupName,
            'label'           => 'Alto (m)',
            'input'           => 'text',
            'type'            => 'varchar',
            'backend'         => '',
            'frontend'        => '',
            'required'        => 1,
            'visible_on_front'=> 1,
            'filterable'      => 0,
            'searchable'      => 0,
            'comparable'      => 0,
            'user_defined'    => 1,
            'default'         => '0',
            'unique'          => false,
            'apply_to'        => '',
            'is_configurable' => 0,
            'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'note'            => '',
));

$installer->addAttribute('catalog_product', 'long', array(
            'group'           => $groupName,
            'label'           => 'Largo (m)',
            'input'           => 'text',
            'type'            => 'varchar',
            'backend'         => '',
            'frontend'        => '',
            'required'        => 1,
            'visible_on_front'=> 1,
            'filterable'      => 0,
            'searchable'      => 0,
            'comparable'      => 0,
            'user_defined'    => 1,
            'default'         => '0',
            'unique'          => false,
            'apply_to'        => '',
            'is_configurable' => 0,
            'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'note'            => '',
));

$installer->addAttribute('catalog_product', 'width', array(
            'group'           => $groupName,
            'label'           => 'Profundidad (m)',
            'input'           => 'text',
            'type'            => 'varchar',
            'backend'         => '',
            'frontend'        => '',
            'required'        => 1,
            'visible_on_front'=> 1,
            'filterable'      => 0,
            'searchable'      => 0,
            'comparable'      => 0,
            'user_defined'    => 1,
            'default'         => '0',
            'unique'          => false,
            'apply_to'        => '',
            'is_configurable' => 0,
            'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'note'            => '',
));
*/
$installer->endSetup();


?>
