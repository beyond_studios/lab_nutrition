<?php

$installer = $this;
$installer->startSetup();

    $installer->getConnection()
        ->addColumn($installer->getTable('urbaner/integrador'),'tracking_id', array(
            'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
            'nullable'  => true,
            'after'     => "status",
            'comment'   => 'Id Order in Urbaner'
    ));
    $installer->getConnection()
        ->addColumn($installer->getTable('urbaner/integrador'),'urbaner_code', array(
            'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
            'nullable'  => true,
            'after'     => "status",
            'comment'   => 'Code Urbaner'
    ));
    $installer->getConnection()
        ->addColumn($installer->getTable('urbaner/integrador'),'urbaner_status', array(
            'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
            'nullable'  => true,
            'after'     => "status",
            'comment'   => 'Status Urbaner'
    ));
    $installer->getConnection()
        ->addColumn($installer->getTable('urbaner/integrador'),'urbaner_tracking', array(
            'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
            'nullable'  => true,
            'after'     => "status",
            'comment'   => 'Tracking Urbaner'
    ));
$installer->endSetup();
