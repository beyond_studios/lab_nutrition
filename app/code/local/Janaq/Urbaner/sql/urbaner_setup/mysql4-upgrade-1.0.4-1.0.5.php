<?php

$installer = $this;
$installer->startSetup();

$installer->addAttribute("order", "urbaner_order_type", array("type"=>"varchar"));
$installer->addAttribute("quote", "urbaner_order_type", array("type"=>"varchar"));

$installer->addAttribute("order", "urbaner_order_hours", array("type"=>"varchar"));
$installer->addAttribute("quote", "urbaner_order_hours", array("type"=>"varchar"));

$installer->endSetup();


?>
