<?php

$installer = $this;
$installer->startSetup();

$installer->addAttribute("order", "urbaner_vehicle", array("type"=>"varchar"));
$installer->addAttribute("quote", "urbaner_vehicle", array("type"=>"varchar"));


$table = $installer->getConnection()
    ->newTable($installer->getTable('urbaner/integrador'))
    ->addColumn('urbaner_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'ID')
    ->addColumn('order_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 0, array(
        'nullable'  => true,
        ), 'ID ORDER')
    ->addColumn('data_sent', Varien_Db_Ddl_Table::TYPE_VARCHAR, 0, array(
        'nullable'  => true,
        ), 'ID CUSTOMER')
    ->addColumn('type_task', Varien_Db_Ddl_Table::TYPE_VARCHAR, 0, array(
        'nullable'  => false,
        ), 'Type Task')
    ->addColumn('response', Varien_Db_Ddl_Table::TYPE_VARCHAR, 0, array(
        'nullable'  => true,
      ), 'Respuesta Web service')
    ->addColumn('numero_guia', Varien_Db_Ddl_Table::TYPE_TEXT, 0, array(
        'nullable'  => true,
      ), 'Número de la guia')
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, 0, array(
        'nullable'  => false,
      ), 'Fecha Creación')
    ->addColumn('update_at', Varien_Db_Ddl_Table::TYPE_DATETIME, 0, array(
        'nullable'  => false,
      ), 'Fecha Actualización')
    ->addColumn('status', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        'nullable'  => false,
        ), 'Estado');
$installer->getConnection()->createTable($table);

$installer->endSetup();


?>
