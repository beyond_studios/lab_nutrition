<?php 
class Janaq_Urbaner_Model_System_Config_Source_Ordertypes
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
    	$model = Mage::getModel('urbaner/service')->executeLogin();
    	$order_types = $model->client->order_types;
    	$_types = array();
    	foreach ($order_types as $type) {
    		$_types[] = array(
    			'value' => ($type->id . '-'. Mage::helper('adminhtml')->__($type->name)),
    			'label' => Mage::helper('adminhtml')->__($type->name)
    		);
    	}
    	return $_types;
    }
}

?>