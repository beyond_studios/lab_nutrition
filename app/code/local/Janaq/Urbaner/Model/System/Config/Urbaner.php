<?php
/**
 * Created by PhpStorm.
 * User: HOME
 * Date: 11/12/2016
 * Time: 23:27
 */

class Janaq_Urbaner_Model_System_Config_Urbaner extends Mage_Core_Model_Config_Data
{
    public function _afterSave()
    {
        Mage::getResourceModel('urbaner/urbanerrate')->uploadAndImport($this);
    }
}