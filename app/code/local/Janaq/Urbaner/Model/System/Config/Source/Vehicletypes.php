<?php 
class Janaq_Urbaner_Model_System_Config_Source_Vehicletypes
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $model = Mage::getModel('urbaner/service')->executeLogin();
    	$vehicle_types = $model->client->vehicle_types;
    	$_types = array();
    	foreach ($vehicle_types as $type) {
    		$_types[] = array(
    			'value' => $type->id . '-'. $type->name,
    			'label' => $type->name
    		);
    	}
    	return $_types;
    }

    public function toArray()
    {
        $model = Mage::getModel('urbaner/service')->executeLogin();
    	$vehicle_types = $model->client->vehicle_types;
    	$_types = array();
    	foreach ($vehicle_types as $type) {
    		$_types[$type->id] = $type->name;
    	}
    	return $_types;
    }
}

?>