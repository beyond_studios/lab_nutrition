<?php
/**
 * Created by PhpStorm.
 * User: HOME
 * Date: 11/12/2016
 * Time: 21:35
 */
class Janaq_Urbaner_Model_Service extends Mage_Core_Model_Abstract
{
    public function executeLogin()
    {
        $cacheId = 'urbaner_login';
		if (false !== ($data = Mage::app()->getCache()->load($cacheId))) {
		    $data = unserialize($data);
		} else {
		    $email = Mage::helper('urbaner')->getAuthUser();
	    	$pwd = Mage::helper('urbaner')->getAuthPassword();
	    	$api_url = Mage::helper('urbaner')->getUrlCarrierTracking() . "client/authenticate/";
	    	$parameters = array(
	    		'email' => $email,
	    		'password' => $pwd
	    	);

	        $parameters = json_encode($parameters);
			$httpHeader = array('Content-Type: application/json;');

			$curl = curl_init($api_url);
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
			curl_setopt($curl, CURLOPT_HTTPHEADER,$httpHeader);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $parameters);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

			$response = curl_exec($curl);
			if ($response) {
			    $data = json_decode($response);
			    $cacheLifetime = 3600;
		    	Mage::app()->getCache()->save(serialize($data), $cacheId, array("URBANER"), $cacheLifetime);
			}
		}

		return $data;
    }

    public function getPriceShipping($lat_lng,$vehiculo,$urb_type_order,$almacen_id = false){

      $helper = Mage::helper('urbaner');
    	$api_url = $helper->getUrlCarrierTracking() . "cli/price/";
      $end_point = $helper->getEndLatLng();
      $token = $helper->token();
      $is_return = false;
      if ($urb_type_order == '4') {
        $is_return = true;
        $urb_type_order = '1';
      }
      if ($almacen_id) {
        $storelocator = Mage::getModel('storelocator/storelocator')->load($almacen_id);
        if ($storelocator && $storelocator->getStorelocatorId()) {
          $end_point = $storelocator->getLatitude() .','. $storelocator->getLongtitude();
        }
      }
    	$parameters = array(
    		'destinations' => array(
          array('latlon' => $end_point),
		      array('latlon' => $lat_lng)
    		),
    		//'package_type_id' => $vehiculo,
        'vehicle_type_id' => $vehiculo, //Campo requerido (Actualizacion)
        'is_return' => $is_return
    	);
      if ($is_return) {
        $parameters['destinations']['latlon'] = $end_point;
      }
      //Mage::log($parameters);
      $parameters = json_encode($parameters);
  		$httpHeader = array('Content-Type: application/json;','Authorization: token '. $token );
  		$curl = curl_init($api_url);
  		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
  		curl_setopt($curl, CURLOPT_HTTPHEADER,$httpHeader);
  		curl_setopt($curl, CURLOPT_POSTFIELDS, $parameters);
  		curl_setopt($curl, CURLOPT_POST, true);
  		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
      $data = array();
	    $response = curl_exec($curl);
      if ($response) {
        $data = json_decode($response,true);
      }
      //Mage::log($data);
      $default = false;
      $type = array('1' => 'EXPRESS' , '3' =>'NEXTDAY');
      if ($urb_type_order) {
        //Mage::log($urb_type_order);
        $type_key = array_search($type[$urb_type_order], array_column($data['prices'], 'order_type'));
        if ($type_key !== false) {
            $price = $data['prices'][$type_key]['price'];
        }else{
          $default = true;
        }
      }else{
        $default = true;
      }
      if ($default) {
        $price = $data['prices'][0]['price'];
      }
      //Mage::log($price);
      $price = $price - $helper->getDiscountAmount();
      if((float)$price < 0 ){
        $price = 0;
      }
      return $price;
    }

    public function setOrderUrbaner($parameters){
      $data_response = "";
      $api_url = Mage::helper('urbaner')->getUrlCarrierTracking() . "cli/order/";
      $helper = Mage::helper('urbaner');
      $token = $helper->token();

      //Mage::log($parameters);
      $parameters = json_encode($parameters);
      $httpHeader = array('Content-Type: application/json;','Authorization: token '. $token );
      $curl = curl_init($api_url);
      curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
      curl_setopt($curl, CURLOPT_HTTPHEADER,$httpHeader);
      curl_setopt($curl, CURLOPT_POSTFIELDS, $parameters);
      curl_setopt($curl, CURLOPT_POST, true);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
          $data = array();
      $response = curl_exec($curl);
      //Mage::log($response);
      if ($response) {
              $data_response = json_decode($response,true);
      }
      return $response;

    }


    public function getMapas(){
      $urbaner = Mage::helper('urbaner');

      $urlMambapas = $urbaner->getMapaUrl();
      $userMambapas = $urbaner->getMapaUser();
      $passMambapas = $urbaner->getMapaPassword();
      $cachelifetime = 86000;


      $cache = Mage::app()->getCache();
      $body = $cache->load("URBANER_zonareparto");


      if ($body === false) {
      	  //Mage::log('Sin Cache Mapas.');
          $client = new \Zend_Http_Client($urlMambapas."mapas");
          $client->setParameterPost(array());
          $client->setAuth($userMambapas, $passMambapas, \Zend_Http_Client::AUTH_BASIC);
          $response = $client->request('GET');
          if ($response->isSuccessful()) {
          	$data = $response->getBody();
              $body = json_decode($data,true);
              //$cacheLifetime = $this->getData('cachelifetime');
              $cache->save($response->getBody(), "URBANER_zonareparto", array("URBANER"), $cachelifetime);
          }
      } else {
          $body = json_decode($body,true);
      }
      return $body;
	}

	public function getDistricts(){

    	$urbaner = Mage::helper('urbaner');

		$urlMambapas = $urbaner->getMapaUrl();
        $userMambapas = $urbaner->getMapaUser();
        $passMambapas = $urbaner->getMapaPassword();
        $cachelifetime = 86000;

        $cache = Mage::app()->getCache();
        $body = $cache->load("URBANER_districts");

        if ($body === false) {
            $client = new \Zend_Http_Client($urlMambapas."mapas/distritos-peru");
            $client->setParameterPost(array());
            $client->setAuth($userMambapas, $passMambapas, \Zend_Http_Client::AUTH_BASIC);
            $response = $client->request('GET');
            if ($response->isSuccessful()) {
                $body = json_decode($response->getBody());

                //$cacheLifetime = $this->getData('cachelifetime');
                $cache->save($response->getBody(), "URBANER_districts", array("URBANER"), $cachelifetime);
            }
        } else {
            $body = json_decode($body,true);
        }


        return $body;


    }

}
