<?php

class Janaq_Urbaner_Model_Cron
{

    /* -----------------------------------------------------------------------------------------
            - Proceso de Enviar pedido a urbaner
    */
    public function run()
    {
      $date = Mage::getModel('core/date')->date('Y-m-d H:i:s');
    	//Mage::log($date);


        $Collection = Mage::getResourceModel('urbaner/integrador_collection');
        $Collection->addFieldToFilter('status', array('in' => array('pendiente')));
        //ORDER
        //$parameters = array();
        foreach ($Collection as  $tareas) {
            $model = Mage::getModel('urbaner/integrador');
            $model->load($tareas->getUrbanerId());
            $validate_close = Mage::helper('urbaner')->getValidateClose();
            if ($tareas->getTypeTask() == 'Order') {
            	try{
                    $incrementId = $model->getOrderId();
                    $order = Mage::getModel('sales/order')->loadByIncrementId($incrementId);
                    $customer_id = $order->getCustomerId();
                    $shippingAddress = $order->getShippingAddress();

                    $customer_email = $order->getCustomerEmail();
                    //distrito
                    $dni = $shippingAddress->getFax();
                    $provincia_distrito = $shippingAddress->getPostcode();
                    $region = $shippingAddress->getRegion();
                    $latlon = $order->getUrbanerLatLng();

                    //
                    $tipo_vehiculo = $order->getUrbanerVehicle(); // // 2 Mercancias, 1 docuentos unitarios
                    $fecha_envio =Mage::getModel('core/date')->date('m/d/Y', strtotime($order->getCreatedAt())); // calcular de la orden
                    $nombre_contacto = $shippingAddress->getFirstname().' '.$shippingAddress->getLastname();
                    $telefono = $shippingAddress->getTelephone();
                    $address = $order->getUrbanerAddresses() .', '. $provincia_distrito;// opcional
                    // unidad en kilogramos , documentos 1
                    $fecha_entrega =Mage::getModel('core/date')->date('m/d/Y', strtotime($order->getServientregaShippingDate()));// Fecha seleccionada por el cliente en la web
                    $id_centro_costos = ''; //opcional
                    $valor_declarado_guia = number_format($order->getGrandTotal(),2,'.','');// costo total del envio
                    $valor_recaudo_guia = '0.00';
                    $medio_pago_recaudo = '0';
                    /*configuraciones en servientrega */
                    $description = "";
                    $memo = "Orden #" . $order->getIncrementId();
                    $items = $order->getAllItems();
                    $ProductName = 'Productos: ';
                    if ($items)
                    {
                        foreach($items as $item)
                        {
                        	if ($item->getParentItem()) continue;
            				          $ProductName .= $item->getName() . '; ';
                        }
                    }
		                $description = rtrim($ProductName, '; ');

                    $urbaner = Mage::getModel('urbaner/service');
                    $helper = Mage::helper('urbaner');
                    $person_end = $helper->getContactPerson();
                    $coordenadas_urbaner = explode(',',$latlon);
                    $order_almacen_id = $order->getUrbanerAlmacenId();
                    $storelocator = Mage::getModel('storelocator/storelocator')->load($order_almacen_id);
                    if ($storelocator && $storelocator->getStorelocatorId()) {
                       $end_point = $storelocator->getLatitude() .','. $storelocator->getLongtitude();
                       $person_end = ($storelocator->getContactPerson()) ? $storelocator->getContactPerson(): $helper->getContactPerson();
                       $phone_end = $storelocator->getPhone();
                       $address_end = $storelocator->getAddress();
                       $reference_end = '';
                       $email_end = $storelocator->getEmail();
                    }else{
                      $end_point = $helper->getEndLatLng();
                      $phone_end = $helper->getContactPhone();
                      $address_end = $helper->getContactAddress();
                      $reference_end = $helper->getContactReference();
                      $email_end = $helper->getContactEmail();
                    }
                    
                    /*$urbener_shipping = $urbaner->getPriceShipping($coordenadas_urbaner[0],$coordenadas_urbaner[1],$tipo_vehiculo,false);
                    //Mage::log($urbener_shipping);
                    $duration = $urbener_shipping['duration'];
                    if ($duration) {
                        $duration = ceil(($duration/3600)) + 5;
                        $minutes = 0;
                    }else{
                      $duration = 6;
                      $minutes = 0;
                    }
                    //$duration = 13;
                    */
                    $date_add = false;

                    $is_return = false;
                    $order_type = $order->getUrbanerOrderType();
                    if ($order_type == '4') {
                        $is_return = true;
                    }
                    if ($order_type == '2') {
                      $order_type = '1';
                      $hour = $order->getUrbanerOrderHours();
                      if ($hour) {
                        $date_add = Mage::getModel('core/date')->date('Y-m-d '.$hour.':00:00');
                        $date_add = date('Y-m-d H:i:s',strtotime ( '+5 hour' , strtotime ( $date_add ) ) );
                      }else{
                        $duration = '6';
                        $date_add = Mage::getModel('core/date')->date('Y-m-d H:i:s',strtotime('+'.$duration.' hour') );
                      }
                    }
                    //contra entrega
                    if ($order_type =='4') {
                      $order_type = '1';
                    }
                    
                    $parameters = array(
                      "type" => $order_type,
                  		"destinations" =>array(
                        array(
                          "contact_person" => $person_end,
                          "phone" => $phone_end,
                          "address" => $address_end,
                          "latlon" => $end_point,
                          "interior" => "",
                          "special_instructions" => $reference_end,
                          "email" => $email_end
                        ),
                        array(
                          "contact_person" => $nombre_contacto,
                          "phone" => $telefono,
                          "address" => $address,
                          "latlon" => $latlon,
                          "interior" => "",
                          "special_instructions" => $order->getOnestepcheckoutOrderComment() ? $order->getOnestepcheckoutOrderComment() : '',
                          "email" => $customer_email
                        )
                      ),
                      "payment"  => array(
                        "backend" =>  "credit"
                      ),
                      "description" => $description,
                      "vehicle_id" => $tipo_vehiculo,
                      "memo" => $memo,
                      "is_return" => $is_return,
                      "has_extended_search_time" => "true"
                      //"coupon" => $data['cupon']
                    );
                    if ($is_return) {
                      $array_is_return = array(
                        "contact_person" => $person_end,
                        "phone" => $phone_end,
                        "address" => $address_end,
                        "latlon" => $end_point,
                        "interior" => "",
                        "special_instructions" => $reference_end,
                        "email" => $email_end
                      );
                      array_push($parameters['destinations'], $array_is_return);
                    }
                    if ($date_add) {
                      $parameters['programmed_date'] = $date_add;
                    }
                    $response = $urbaner->setOrderUrbaner($parameters);
                    $response_array = array();
                    $response_array = json_decode($response,true);

                    if (array_key_exists('code',$response_array) && array_key_exists('status',$response_array) ) {
                        $model->setStatus('completado');
                        $model->setDataSent(json_encode($parameters));
                        $model->setUpdateAt($date);
                        $model->setResponse($response);
                        $model->setUrbanerStatus($response_array['status']);
                        $model->setTrackingId($response_array['id']);
                        $model->setUrbanerTracking($response_array['tracking']);
                        $model->setUrbanerCode($response_array['code']);
                        $model->save();
                        //var_dump($xmls->Table->NUMERO_GUIA);

                    } else {
                        $model->setDataSent(json_encode($parameters));
                        $model->setResponse($response);
                        $model->setUpdateAt($date);
                        $model->save();
                    }
                } catch (Exception $e){
                        $model->setResponse($e->getMessage());
                        $model->setDataSent(json_encode($parameters));
                        $model->setResponse($response);
                        $model->setUpdateAt($date);
                        $model->save();
                        Mage::log('Error al actulizar estado de la Tarea Order');
                }
            }

    	}
	}
}
