<?php
/**
 * Created by PhpStorm.
 * User: HOME
 * Date: 12/12/2016
 * Time: 22:52
 */

class Janaq_Urbaner_Model_Carrier_Urbaner
    extends Mage_Shipping_Model_Carrier_Abstract
    implements Mage_Shipping_Model_Carrier_Interface
{
    protected $_code = 'urbaner';

    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        if (!Mage::getStoreConfig('carriers/'.$this->_code.'/active')) {
            return false;
        }
        /** @var Janaq_Urbaner_Helper_Data */
        $helper_urbaner = Mage::helper('urbaner');
        if (!$helper_urbaner->getValidateClose() || $helper_urbaner->isHolidays()) {
          return false;
        }

        $freeQty = 0;
        $itemsWeight = 0;
        $volumeTotal = 0;
        $maxWeight = $helper_urbaner->getMaxWeight();

        $storeId = Mage::app()->getStore()->getStoreId();
        if ($request->getAllItems()) {
            $freePackageValue = 0;
            foreach ($request->getAllItems() as $item) {
                if ($item->getProduct()->isVirtual() || $item->getParentItem()) {
                    continue;
                }
                $isShipSeparately = $item->isShipSeparately();

                /*if ($item->getHasChildren()) {

            		foreach ($item->getChildren() as $child) {
                        if ($isShipSeparately && $child->getFreeShipping() && !($child->getProduct()->isVirtual())) {
                            $freeShipping = is_numeric($child->getFreeShipping()) ? $child->getFreeShipping() : 0;
                            $freeQty += $item->getQty() * ($child->getQty() - $freeShipping);
                        }

                        $high = (double)Mage::getResourceModel('catalog/product')->getAttributeRawValue($child->getProduct()->getId(), 'high', $storeId);
                        $long = (double)Mage::getResourceModel('catalog/product')->getAttributeRawValue($child->getProduct()->getId(), 'long', $storeId);
                        $width = (double)Mage::getResourceModel('catalog/product')->getAttributeRawValue($child->getProduct()->getId(), 'width', $storeId);
                        $high = ($high == '') ? 0 : $high;
                        $long = ($long == '') ? 0 : $long;
                        $width = ($width == '') ? 0 : $width;
                        $volume = $high * $long * $width;
                        $volumeTotal += $volume;
                    }
                } else {
                	//simple
                    $high = (double)Mage::getResourceModel('catalog/product')->getAttributeRawValue($item->getProduct()->getId(), 'high', $storeId);
                    $long = (double)Mage::getResourceModel('catalog/product')->getAttributeRawValue($item->getProduct()->getId(), 'long', $storeId);
                    $width = (double)Mage::getResourceModel('catalog/product')->getAttributeRawValue($item->getProduct()->getId(), 'width', $storeId);
                    $high = ($high == '') ? 0 : $high;
                    $long = ($long == '') ? 0 : $long;
                    $width = ($width == '') ? 0 : $width;
                    $volume = $high * $long * $width;
                    $volumeTotal += $volume;
                }*/

                if ($item->getFreeShipping() && !$isShipSeparately) {
                    $freeShipping = is_numeric($item->getFreeShipping()) ? $item->getFreeShipping() : 0;
                    $freeQty += $item->getQty() - $freeShipping;
                    $freePackageValue += $item->getBaseRowTotal();
                }
                //$itemsWeight += ($item->getWeight() * $item->getQty());
            }
            $volumeTotal = number_format($volumeTotal, 6, '.', '');

            $oldValue = $request->getPackageValue();
            $request->setPackageValue($oldValue - $freePackageValue);
        }

        $oldWeight = $request->getPackageWeight();
        $oldQty = $request->getPackageQty();

        //Don't show urbaner method if package weight is upper than maxWeight
        if ($oldWeight > $maxWeight) {
            return false;
        }

        $request->setPackageQty($oldQty - $freeQty);
        $request->setVolume($volumeTotal);

        $result = $this->_getModel('shipping/rate_result');
        $rate = $this->getRate($request);

        $request->setPackageWeight($oldWeight);
        $request->setPackageQty($oldQty);

        if (!empty($rate['weight_base'])) {
          $price = 15;
          $rate['price'] = $price;
        }
        //$coordenadas_mapa = $this->getCordenadasMap($request->getDestPostcode(),$rate['id_mapa_urbaner']);
        $coordenadas_mapa = array();
        $coordenadas_mapa['nombre'] = $request->getDestPostcode();
        $coordenadas_mapa['coordenadas'] = array();
        $coordenadas_mapa['center'] = false;
        $request->setCoordinates($coordenadas_mapa);
        $destStreet = str_replace("\n", " ", $request->getDestStreet());

        if (!empty($rate) && $rate['price'] >= 0) {
            $method = $this->_getModel('shipping/rate_result_method');

            $method->setCarrier($this->_code);
            $method->setCarrierTitle($this->getConfigData('title'));

            $method->setMethod($this->_code);
            $method->setMethodTitle($this->getConfigData('name'));

            if ($request->getFreeShipping() === true || ($request->getPackageQty() == $freeQty)) {
                $shippingPrice = 0;
            } else {
              //$shippingPrice = $this->getFinalPriceWithHandlingFee($rate['price']);

                /*URBNER API PRICE*/
                  $shippingPrice = $this->_getPriceUrbaner($rate);
                /**/

                $priceAditional = 0;
                $weight_base = $oldWeight - $rate['weight_base'];
                if ($weight_base > 0){
                    $priceAditional = ceil($weight_base);
                }
                $shippingPrice = $shippingPrice + $priceAditional;
            }

            $method->setPrice($shippingPrice);
            $method->setCost(0);
            $method->setDeliveryTime($rate['delivery_time']);
            $method->setVolume($volumeTotal);
            $method->setCoordinates($coordenadas_mapa);
            $method->setAddressShipping($destStreet);

            $result->append($method);
        } elseif (empty($rate) && $request->getFreeShipping() === true) {
            /**
             * was applied promotion rule for whole cart
             * other shipping methods could be switched off at all
             * we must show table rate method with 0$ price, if grand_total more, than min table condition_value
             * free setPackageWeight() has already was taken into account
             */
            $request->setPackageValue($freePackageValue);
            $request->setPackageQty($freeQty);
            $rate = $this->getRate($request);
            if (!empty($rate) && $rate['price'] >= 0) {
                $method = $this->_getModel('shipping/rate_result_method');

                $method->setCarrier($this->_code);
                $method->setCarrierTitle($this->getConfigData('title'));

                $method->setMethod($this->_code);
                $method->setMethodTitle($this->getConfigData('name'));

                $method->setPrice(0);
                $method->setCost(0);
                $method->setDeliveryTime($rate['delivery_time']);

                $method->setVolume($volumeTotal);
                $method->setAddressShipping($destStreet);
                $method->setCoordinates($coordenadas_mapa);

                $result->append($method);
            }
        } else {
            $error = $this->_getModel('shipping/rate_result_error');
            $error->setCarrier($this->_code);
            $error->setCarrierTitle($this->getConfigData('title'));
            $error->setErrorMessage($this->getConfigData('specificerrmsg'));
            $result->append($error);
        }

        return $result;
    }
    protected function _getPriceUrbaner($rate){
      $lat_lng = Mage::app()->getRequest()->getParam('lat_lng');
      $vehiculo = Mage::app()->getRequest()->getParam('tipo_vehiculo');
      $urbaner_post = Mage::app()->getRequest()->getParam('urbaner');
      $urb_type = Mage::app()->getRequest()->getParam('urb_type');
      $almacen_id = Mage::app()->getRequest()->getParam('almacen_id');
      if ($lat_lng && $vehiculo) {
          $urbaner = Mage::getModel('urbaner/service');
          $price = $urbaner->getPriceShipping($lat_lng,$vehiculo,$urb_type,$almacen_id);
          $rate['price'] = $price;
      }elseif ($urbaner_post) {
          $urbaner = Mage::getModel('urbaner/service');
          $price = $urbaner->getPriceShipping($urbaner_post['lat_lng'],$urbaner_post['vehicletypes'],$urbaner_post['ordertypes'],$urbaner_post['almacen_id']);
          $rate['price'] = $price;
      }
      return $rate['price'];
    }
    protected function _getModel($modelName)
    {
        return Mage::getModel($modelName);
    }
    public function getRate(Mage_Shipping_Model_Rate_Request $request)
    {
        return Mage::getResourceModel('urbaner/urbanerrate')->getRate($request);
    }

    public function getAllowedMethods()
    {
        return array('urbaner' => $this->getConfigData('name'));

        /*return array(
            'express'   =>  'delivery Express',
            'sameday'   =>  'Same Day delivery',
            'nextday'   =>  'Next Day delivery'
        );*/
    }

    public function getCordenadasMap($postcode, $id_mapa, $static = false){

        $array_distrito = array();
        $array_distrito['nombre'] = '';
        $array_distrito['center'] = array();
        $array_distrito['coordenadas'] = array();
        $urbaner = Mage::helper('urbaner');

        if (!$static) {
            $url_districts = $urbaner->getUrlDistricts();

            $cache = Mage::app()->getCache();
            $body = $cache->load("URBANERMAPS_districts");

            if ($body === false) {
                $client = new \Zend_Http_Client($url_districts);
                $client->setParameterPost(array());
                $response = $client->request('GET');
                if ($response->isSuccessful()) {
                    $body = json_decode($response->getBody());
                    $cacheLifetime = $this->getData('cachelifetime');
                    $cache->save($response->getBody(), "URBANERMAPS_districts", array("URBANERMAPS"), $cacheLifetime);
                }
            } else {
                $body = json_decode($body);
            }
            foreach ($body->distritos as $district) {
                if (trim((int)$id_mapa) == trim((int)$district->coordenadas->distrito_id)) {
                    $array_distrito['nombre'] = (string)$postcode;
                    $coordinate = json_decode((string)$district->coordenadas->valor,true);
                    $array_distrito['center'] = array((string)$coordinate[0]['lng'],(string)$coordinate[0]['lat']);
                    $array_distrito['coordenadas'] = (array)$coordinate;
                    break;
                }
            }

        } else {

            $xml = simplexml_load_file(Mage::getBaseUrl().'distritos.xml');
            foreach ($xml->Document[0]->Folder[0]->Placemark as $placemark) {
                if (trim((string)$placemark->name) == trim($postcode)) {
                    $array_distrito['nombre'] = (string)$placemark->name;
                    $array_distrito['center'] = explode(",",(string)$placemark->LineString[0]->center);
                    $array_distrito['coordenadas'] = $this->getLatLong((string)$placemark->LineString[0]->coordinates);
                    break;
                }
            }
        }
        return $array_distrito;
    }

    protected function getLatLong($text) {
        $text = str_replace(' ', '', $text);
        $text = mberegi_replace("[\n|\r|\n\r]", '', $text);
        $aux = explode(",0", $text);
        array_pop($aux);
        $response[] = array(); $i=0;
        foreach ($aux as $value) {
            $aux_ = explode(",", $value);
            $response[$i]['lat'] = $aux_[1];
            $response[$i]['lng'] = $aux_[0];
            $i++;
        }
        return $response;
    }
}
