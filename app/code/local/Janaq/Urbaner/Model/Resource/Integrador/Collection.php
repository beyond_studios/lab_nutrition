<?php
class Janaq_Urbaner_Model_Resource_Integrador_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('urbaner/integrador');
    }
}
