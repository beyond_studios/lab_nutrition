<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 15/12/2016
 * Time: 12:12 PM
 */

class Janaq_Urbaner_Model_Observer extends Mage_Payment_Model_Method_Abstract
{

    public function afterSave(Varien_Event_Observer $event)
    {
        $params = Mage::app()->getRequest()->getParams();
        $shipping_method = $params['shipping_method'];

        if ($shipping_method == 'urbaner_urbaner') {
            $address = $params["urbaner"]["address"];
            $lat_lng = $params["urbaner"]["lat_lng"];
            $vehicletypes = $params["urbaner"]["vehicletypes"];
            $ordertypes = $params["urbaner"]["ordertypes"];
            $order_almacen_id = $params["urbaner"]["almacen_id"];
            //Mage::log('ordertypes'.$ordertypes);
            $hours = $params["urbaner"]["hours"];
            $order = $event->getOrder();
            $order->setData('urbaner_addresses',$address);
            $order->setData('urbaner_lat_lng', $lat_lng);
            $order->setData('urbaner_vehicle', $vehicletypes);
            $order->setData('urbaner_order_type', $ordertypes);
            $order->setData('urbaner_order_hours', $hours);
            $order->setData('urbaner_almacen_id', $order_almacen_id);
            
            //$params['order'] = $order;
            //Mage::dispatchEvent('order_urbaner_integrador_save', $params);
        }
    }

    public function saveOrderUrbaner($observer){
        Mage::log('guarda Urbaner ' ,null,'urbaner.log');
        //Mage::log('Entra a guardar orden para Urbaner');
        $shipping_method = $observer['order']->getShippingMethod();

        if ($shipping_method == 'urbaner_urbaner') {
            $date = Mage::getModel('core/date')->date('Y-m-d H:i:s');
            $model = Mage::getSingleton('urbaner/integrador');
            $increment_id = $observer['order']->getIncrementId();
            $modelUpdate = Mage::getModel('urbaner/integrador');
            $modelUpdate->load($increment_id,'order_id');
            if ($increment_id && !$modelUpdate->getId()) {
                $model->setOrderId($increment_id);
                $model->setTypeTask('Order');
                $model->setStatus('pendiente');
                $model->setCreatedAt($date);
                $model->setUpdateAt($date);
            }else{
                $model->setId($modelUpdate->getId());
                //$model->setStatus('pendiente');
                $model->setUpdateAt($date);
            }
            try {
                $model->save();
            } catch (Exception $e) {
                Mage::log('Error al guardar registro de Urbaner (Tareas)');
            }
        }
        return $this;
    }
}
