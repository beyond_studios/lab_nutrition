<?php
/* @var $this Janaq_ScheduledProduct_Model_Mysql4_Setup */
$this->startSetup();
// For performance reasons we should add this fields to main entity table
// Status column adding to product entity table
$this->getConnection()->addColumn(
    $this->getTable('catalog/product'),
    Janaq_ScheduledProduct_Model_Attribute_Backend_Datetime::ATTRIBUTE_PROMO_STATUS,
    'INT DEFAULT 0'
);
// Status attribute information adding to the product entity
$this->addAttribute(
    'catalog_product',
    Janaq_ScheduledProduct_Model_Attribute_Backend_Datetime::ATTRIBUTE_PROMO_STATUS,
    array(
        'type'      => 'static',
        'input'     => 'select',
        'label'     => 'Status',
        'global'    => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'backend'   => '',
        'visible'   => 1,
        'required'  => 1,
        'position'  => 5,
        'group'     => 'Schedule Settings',
        'source' => 'eav/entity_attribute_source_boolean',
    )
);
$this->endSetup();