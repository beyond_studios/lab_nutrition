<?php
class Janaq_Logistics_Adminhtml_LogisticsController extends Mage_Adminhtml_Controller_Action
{
    public function massStatusAction() {
        $orderIds = $this->getRequest()->getPost('order_ids', array());
        $status = $this->getRequest()->getPost('status');
        $countOrder = 0;
        $countNonOrder = 0;
        foreach ($orderIds as $orderId) {
            $order = Mage::getModel('sales/order')->load($orderId);
            if ($order->getId()) {
                $order->setStatusLogistics($status);
                $order->save();
                $countOrder++;
                /*if ($status == '1') {
                  $params['order'] = $order;
                  Mage::dispatchEvent('janaq_pim_order_save', $params);
                }*/
                /*$shipping_method = $order->getShippingMethod();
                if ($shipping_method != "motoboy_motoboy" && ($status == "2" || $status == "3")) {
                    $countNonOrder++;
                }
                else {
                    $order->setStatusLogistics($status);
                    $order->save();
                    $countOrder++;
                }*/

            } else {
                $countNonOrder++;
            }
        }
        if ($countNonOrder) {
            if ($countOrder) {
                $this->_getSession()->addError(Mage::helper('sales')->__('%s order(s) cannot be updated', $countNonOrder));
            } else {
                $this->_getSession()->addError(Mage::helper('sales')->__('The order(s) cannot be updated'));
            }
        }
        if ($countOrder) {
            $this->_getSession()->addSuccess(Mage::helper('sales')->__('%s order(s) have been updated.', $countOrder));
        }
        $this->_redirect('*/sales_order/index');


    }
    public function saveStatusAction(){
        $order_id = $this->getRequest()->getPost('order_id');
        $status = $this->getRequest()->getPost('state_logistics');
        $order = Mage::getModel('sales/order')->load($order_id);
            if ($order->getId()) {
                //$shippingMethods = array("motoboy_motoboy","urbaner_urbaner");
                $order->setStatusLogistics($status);
                $order->save();
                $this->_getSession()->addSuccess(
                            $this->__('El estado se ha actualizado corectamente'));
                /*if ($status == '1') {
                  $params['order'] = $order;
                  Mage::dispatchEvent('janaq_pim_order_save', $params);
                }*/
                /*$shipping_method = $order->getShippingMethod();
                if ((in_array($shipping_method, $shippingMethods)) && ($status == "2" || $status == "3")) {
                    $order->setStatusLogistics($status);
                    $order->save();
                    $this->_getSession()->addSuccess($this->__('El estado se ha actualizado corectamente'));
                }
                else {
                    $this->_getSession()->addError($this->__('El estado seleccionado solo se utiliza con pedido hecho con Motoboy o Urbaner'));
                }*/

            } else {
                $this->_getSession()->addError($this->__('Error al actualizar estado'));
            }
        $this->_redirect('*/sales_order/index');

    }

    public function saveLogisticAction(){
        $order_id = $this->getRequest()->getPost('order_id');
        $order = Mage::getModel('sales/order')->load($order_id);
        $status = $this->getRequest()->getPost('state_logistics');
        $assessor = $this->getRequest()->getPost('logistics_assessor');
        if ($order->getId()) {
            $order->setStatusLogistics($status);
            $order->setLogisticsAssessor($assessor);
            $order->save();
            $this->_getSession()->addSuccess($this->__('Los datos de logística se actualizaron correctamente'));
        } else {
            $this->_getSession()->addError($this->__('Error al actualizar datos de logística'));
        }
        $this->_redirect('*/sales_order/index');
    }

    /**
     *  Export order grid to Excel XLS format
     */
    public function exportExcelrotulosAction()
    {
        $time = time();
        $fileName   = 'orderstablarotulos' . $time . '.xls';
        $grid       = $this->getLayout()->createBlock('janaq_logistics/rotulosordergrid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('sales');
    }
}
