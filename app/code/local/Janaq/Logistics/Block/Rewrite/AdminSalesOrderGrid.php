<?php

class Janaq_Logistics_Block_Rewrite_AdminSalesOrderGrid extends Mage_Adminhtml_Block_Sales_Order_Grid
{
	protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel($this->_getCollectionClass());
        //
        $this->setCollection($collection);
        return Mage_Adminhtml_Block_Widget_Grid::_prepareCollection();
        //return parent::_prepareCollection();
        //return $this;
        
    }
    /*protected function _addColumnFilterToCollection($column)
    {
    	
        if ($this->getCollection()) {
            $field = ( $column->getFilterIndex() ) ? $column->getFilterIndex() : $column->getIndex();
            $field = 'main_table.'.$field;
            if ($column->getFilterConditionCallback()) {
                call_user_func($column->getFilterConditionCallback(), $this->getCollection(), $column);
            } else {
                $cond = $column->getFilter()->getCondition();
                if ($field && isset($cond)) {
                    $this->getCollection()->addFieldToFilter($field , $cond);
                }
            }
        }
        return $this;
    }*/
	protected function _prepareColumns()
    {
        $shipMethods = Mage::helper('janaq_logistics')->getActiveCarriers();

        $this->addColumn('real_order_id', array(
            'header'=> Mage::helper('sales')->__('Order #'),
            'width' => '80px',
            'type'  => 'text',
            'index' => 'increment_id',
            'filter_index' => 'main_table.increment_id',

        ));

        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id', array(
                'header'    => Mage::helper('sales')->__('Purchased From (Store)'),
                'index'     => 'store_id',
                'type'      => 'store',
                'store_view'=> true,
                'display_deleted' => true,
            ));
        }

        $this->addColumn('method', array(
            'header' => Mage::helper('sales')->__('Payment Method'),
            'index' => 'method',
            'filter_index' => 'method',
            'width' => '70px',
            'column_css_class'=>'no-display',
            'header_css_class'=>'no-display',
        ));

        $this->addColumn('created_at', array(
            'header' => Mage::helper('sales')->__('Purchased On'),
            'index' => 'created_at',
            'type' => 'datetime',
            'width' => '100px',
            'filter_index' => 'main_table.created_at',

        ));

        $this->addColumn('b_fax', array(
            'header' => Mage::helper('sales')->__('DNI'),
            'index' => 'b_fax',
            'width' => '70px',
            'filter_index' => 'b_address.fax',
        ));

        $this->addColumn('billing_name', array(
            'header' => Mage::helper('sales')->__('Bill to Name'),
            'index' => 'billing_name',
        ));

        $this->addColumn('shipping_name', array(
            'header' => Mage::helper('sales')->__('Ship to Name'),
            'index' => 'shipping_name',
        ));
        $this->addColumn('b_region', array(
            'header' => Mage::helper('sales')->__('Departamento'),
            'index' => 'b_region',
            'width' => '100px',
            'filter_index' => 'b_address.region',
        ));

        $this->addColumn('provincia', array(
            'header' => Mage::helper('sales')->__('Provincia'),
            'index' => 'provincia',
            'width' => '200px',
            'filter_index' => 'b_address.postcode',
        ));

        $this->addColumn('distrito', array(
            'header' => Mage::helper('sales')->__('Distrito'),
            'index' => 'distrito',
            'width' => '200px',
            'filter_index' => 'b_address.postcode',
        ));

        $this->addColumn('base_grand_total', array(
            'header' => Mage::helper('sales')->__('G.T. (Base)'),
            'index' => 'base_grand_total',
            'type'  => 'currency',
            'currency' => 'base_currency_code',
            'filter_index' => 'main_table.base_grand_total',
        ));

        $this->addColumn('grand_total', array(
            'header' => Mage::helper('sales')->__('G.T. (Purchased)'),
            'index' => 'grand_total',
            'type'  => 'currency',
            'currency' => 'order_currency_code',
            'filter_index' => 'main_table.grand_total',
        ));


        $this->addColumn('status_logistics', array(
            'header' => Mage::helper('sales')->__('State Logistics'),
            'index' => 'status_logistics',
            'width' => '70px',
            'filter_index' => 'sales_order.status_logistics',
            'type'      => 'options',
            'options'    => Mage::getSingleton('janaq_logistics/status')->getOptionArray(),
        ));

        $this->addColumn('status', array(
            'header' => Mage::helper('sales')->__('Status'),
            'index' => 'status',
            'type'  => 'options',
            'width' => '70px',
            'filter_index' => 'main_table.status',
            'options' => Mage::getSingleton('sales/order_config')->getStatuses(),
        ));

        $this->addColumn('shipping_description', array(
            'header' => Mage::helper('sales')->__('Método de envio'),
            'index' => 'shipping_method',
            'width' => '70px',
            'type'      => 'options',
            'filter_index' => 'sales_order.shipping_method',
            'options' => $shipMethods
        ));

        if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/view')) {
            $this->addColumn('action',
                array(
                    'header'    => Mage::helper('sales')->__('Action'),
                    'width'     => '50px',
                    'type'      => 'action',
                    'getter'     => 'getId',
                    'actions'   => array(
                        array(
                            'caption' => Mage::helper('sales')->__('View'),
                            'url'     => array('base'=>'*/sales_order/view'),
                            'field'   => 'order_id',
                            'data-column' => 'action',
                        )
                    ),
                    'filter'    => false,
                    'sortable'  => false,
                    'index'     => 'stores',
                    'is_system' => true,
            ));
        }
        $this->addRssList('rss/order/new', Mage::helper('sales')->__('New Order RSS'));

        if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/jnqexport')) {
            $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
            $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel XML'));
            $this->addExportType('*/logistics/exportExcelrotulos', Mage::helper('sales')->__('Tabla Rótulos'));
        }
        return Mage_Adminhtml_Block_Widget_Grid::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('order_ids');
        $this->getMassactionBlock()->setUseSelectAll(false);

        $statuses = Mage::getSingleton('janaq_logistics/status')->getOptionArray();
        
        $this->getMassactionBlock()->addItem('status', array(
            'label'=> Mage::helper('sales')->__('Change status logistics'),
            'url'   => $this->getUrl('*/logistics/massStatus', array('_current'=>true)),
            'additional' => array(
                'visibility' => array(
                    'name'  => 'status',
                    'type'  => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('sales')->__('Status'),
                    'values'=> $statuses
                ))
        ));

        if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/cancel')) {
            $this->getMassactionBlock()->addItem('cancel_order', array(
                 'label'=> Mage::helper('sales')->__('Cancel'),
                 'url'  => $this->getUrl('*/sales_order/massCancel'),
            ));
        }

        if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/hold')) {
            $this->getMassactionBlock()->addItem('hold_order', array(
                 'label'=> Mage::helper('sales')->__('Hold'),
                 'url'  => $this->getUrl('*/sales_order/massHold'),
            ));
        }

        if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/unhold')) {
            $this->getMassactionBlock()->addItem('unhold_order', array(
                 'label'=> Mage::helper('sales')->__('Unhold'),
                 'url'  => $this->getUrl('*/sales_order/massUnhold'),
            ));
        }

        $this->getMassactionBlock()->addItem('pdfinvoices_order', array(
             'label'=> Mage::helper('sales')->__('Print Invoices'),
             'url'  => $this->getUrl('*/sales_order/pdfinvoices'),
        ));

        $this->getMassactionBlock()->addItem('pdfshipments_order', array(
             'label'=> Mage::helper('sales')->__('Print Packingslips'),
             'url'  => $this->getUrl('*/sales_order/pdfshipments'),
        ));

        $this->getMassactionBlock()->addItem('pdfcreditmemos_order', array(
             'label'=> Mage::helper('sales')->__('Print Credit Memos'),
             'url'  => $this->getUrl('*/sales_order/pdfcreditmemos'),
        ));

        $this->getMassactionBlock()->addItem('pdfdocs_order', array(
             'label'=> Mage::helper('sales')->__('Print All'),
             'url'  => $this->getUrl('*/sales_order/pdfdocs'),
        ));

        $this->getMassactionBlock()->addItem('print_shipping_label', array(
             'label'=> Mage::helper('sales')->__('Print Shipping Labels'),
             'url'  => $this->getUrl('*/sales_order_shipment/massPrintShippingLabel'),
        ));

        $pim = Mage::helper('janaq_pim');

        if ($pim->getStatus()) {
            $this->getMassactionBlock()->addItem('send_pim', array(
                 'label'=> Mage::helper('sales')->__('Enviar a PIM'),
                 'url'  => $this->getUrl('*/pim/sendpim'),
            ));
        }

        $navasoft = Mage::helper('janaq_navasoft');

        if ($navasoft->getStatus()) {
            $this->getMassactionBlock()->addItem('send_navasoft', array(
                 'label'=> Mage::helper('sales')->__('Enviar a NAVASOFT'),
                 'url'  => $this->getUrl('*/navasoft/sendnavasoft'),
            ));
        }

        

        return $this;
    }

}

