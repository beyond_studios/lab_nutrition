<?php

class Janaq_Logistics_Block_Rotulosordergrid extends Mage_Adminhtml_Block_Sales_Order_Grid
{
	protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel($this->_getCollectionClass());
        $this->setCollection($collection);
        return Mage_Adminhtml_Block_Widget_Grid::_prepareCollection();
        
    }
	protected function _prepareColumns()
    {
        /*$payments = Mage::getSingleton('payment/config')->getActiveMethods();
        $methods = array();
        foreach ($payments as $paymentCode=>$paymentModel)
        {
            $paymentTitle = Mage::getStoreConfig('payment/'.$paymentCode.'/title');
            $methods[$paymentCode] = $paymentTitle;
        }*/
        
        $this->addColumn('tipo_envio', array(
            'header' => Mage::helper('sales')->__('Tipo de Envío'),
            'index' => 'tipo_envio',
            'width' => '70px',
        ));

        $this->addColumn('method', array(
            'header' => Mage::helper('sales')->__('Payment Method'),
            'index' => 'method',
            'filter_index' => 'method',
            //'type'  => 'options',
            'width' => '70px',
            //'options' => $methods,
        ));

        $this->addColumn('real_order_id', array(
            'header'=> Mage::helper('sales')->__('Order #'),
            'width' => '80px',
            'type'  => 'text',
            'index' => 'increment_id',
            'filter_index' => 'main_table.increment_id',

        ));

        $this->addColumn('created_at', array(
            'header' => Mage::helper('sales')->__('Purchased On'),
            'index' => 'created_at',
            'type' => 'datetime',
            'width' => '100px',
            'filter_index' => 'main_table.created_at',
        ));

        $this->addColumn('b_fax', array(
            'header' => Mage::helper('sales')->__('DNI'),
            'index' => 'b_fax',
            'width' => '70px',
        ));

        $this->addColumn('billing_name', array(
            'header' => Mage::helper('sales')->__('Nombre del Cliente (Factura)'),
            'index' => 'billing_name',
            'width' => '150px',
        ));

        $this->addColumn('b_telephone', array(
            'header' => Mage::helper('sales')->__('TELEFONO'),
            'index' => 'b_telephone',
            'width' => '70px',
            'filter_index' => 'b_address.telephone',
        ));

        $this->addColumn('street', array(
            'header' => Mage::helper('sales')->__('Dirección de Envío'),
            'index' => 'street',
            'width' => '150px',
            'filter_index' => 'b_address.street',
        ));

        $this->addColumn('company', array(
            'header' => Mage::helper('sales')->__('REFERENCIA'),
            'index' => 'company',
            'width' => '150px',
            'filter_index' => 'b_address.company',
        ));
        $this->addColumn('region', array(
            'header' => Mage::helper('sales')->__('DEPARTAMENTO'),
            'index' => 'region',
            'width' => '100px',
            'filter_index' => 'b_address.region',
        ));

        $this->addColumn('provincia', array(
            'header' => Mage::helper('sales')->__('Provincia'),
            'index' => 'provincia',
            'width' => '200px',
            'filter_index' => 'b_address.postcode',
        ));

        $this->addColumn('distrito', array(
            'header' => Mage::helper('sales')->__('Distrito'),
            'index' => 'distrito',
            'width' => '200px',
            'filter_index' => 'b_address.postcode',
        ));

        $this->addColumn('base_grand_total', array(
            'header' => Mage::helper('sales')->__('Total (Base)'),
            'index' => 'base_grand_total',
            'type'  => 'currency',
            'currency' => 'base_currency_code',
            'filter_index' => 'main_table.base_grand_total',

        ));

        $this->addColumn('shipping_description', array(
            'header' => Mage::helper('sales')->__('Método de envio'),
            'index' => 'shipping_description',
            'width' => '70px',
            'filter_index' => 'sales_order.shipping_method',
        ));

        $this->addColumn('status_logistics', array(
            'header' => Mage::helper('sales')->__('State Logistics'),
            'index' => 'status_logistics',
            'width' => '70px',
            'filter_index' => 'sales_order.status_logistics',
            'type'      => 'options',
            'options'    => Mage::getSingleton('janaq_logistics/status')->getOptionArray(),
        ));

        $this->addColumn('shipping_name', array(
            'header' => Mage::helper('sales')->__('Nombre del Cliente (Envío)'),
            'index' => 'shipping_name',
            'width' => '150px',
        ));

        $this->addColumn('fax', array(
            'header' => Mage::helper('sales')->__('DNI'),
            'index' => 'fax',
            'width' => '70px',
        ));

        $this->addColumn('telephone', array(
            'header' => Mage::helper('sales')->__('TELEFONO'),
            'index' => 'telephone',
            'width' => '70px',
            'filter_index' => 's_address.telephone',
        ));
        
        return Mage_Adminhtml_Block_Widget_Grid::_prepareColumns();
    }

}