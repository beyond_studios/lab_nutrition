<?php
class Janaq_Logistics_Model_Assessor extends Varien_Object
{

	/**
	get model option as array
	 *
	 * @return array
	 */
	static public function getOptionArray(){
		$assessors = array_map("trim", explode(",", Mage::getStoreConfig('jnqadmin_config/assessors/assessor')));
		//$assessors = array_combine($assessors, $assessors);
		return $assessors;
	}
	
}