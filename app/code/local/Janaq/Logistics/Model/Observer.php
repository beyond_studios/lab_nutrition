<?php
class Janaq_Logistics_Model_Observer {

  
    public function getSalesOrderViewInfo(Varien_Event_Observer $observer) {
        $block = $observer->getBlock();
        
        if (($block->getNameInLayout() == 'order_info') && ($child = $block->getChild('logistics.order.info.custom.block'))) {
            $transport = $observer->getTransport();
            if ($transport) {
                $html = $transport->getHtml();
                $html .= $child->toHtml();
                $transport->setHtml($html);
            }
        }
    }

    public function onSalesOrderGridCollectionLoadBefore(Varien_Event_Observer $observer) //sales_order_grid_collection_load_before
    {

        if (Mage::app()->getRequest()->getControllerName() == 'customer') {
            // Strange fatal error at Customer -> Orders in EE
            return;
        }
        /** @var Mage_Sales_Model_Resource_Order_Collection $collection */
        $collection = $observer->getOrderGridCollection();
        $select = $collection->getSelect();

        if (strpos((string)$select, '`sales_flat_order`') === false) {
            $select->joinLeft(
                array('sales_order'=>$collection->getTable('sales/order')),
                'sales_order.entity_id=main_table.entity_id',
                array('status_logistics'=>'status_logistics','logistics_assessor'=>'logistics_assessor','shipping_description'=>'shipping_description','shipping_method'=>'shipping_method')
                
            );

            $select->join(
                array('payment'=>'sales_flat_order_payment'),'main_table.entity_id=payment.parent_id',
                new Zend_Db_Expr("(CASE WHEN `payment`.`method` = 'ondelivery' THEN 'Efectivo' WHEN `payment`.`method` = 'payupagoefectivo' THEN 'PagoEfectivo' WHEN `payment`.`method` = 'payutarjetacredito' THEN 'Tarjeta' ELSE method END) AS method")
            );
            $select->join(
                array('s_address'=>'sales_flat_order_address'), 'main_table.entity_id = s_address.parent_id',
                array('s_address.fax','s_address.telephone','s_address.street','s_address.company','s_address.region','s_address.postcode')
            );
            $select->join(
                array('b_address'=>'sales_flat_order_address'), 'main_table.entity_id = b_address.parent_id',
                array('b_fax'=>'b_address.fax','b_telephone'=>'b_address.telephone','b_street'=>'b_address.street','b_company'=>'b_address.company','b_region'=>'b_address.region','b_postcode'=>'b_address.postcode')
            );
            $select
            ->columns(
                new Zend_Db_Expr("SUBSTRING_INDEX(`s_address`.`postcode`, '-', 1) AS provincia, SUBSTRING_INDEX(`s_address`.`postcode`, '-', -1) AS distrito, 
                    (CASE WHEN `sales_order`.`shipping_method` = 'olva_olva' THEN 'Regular'
                    WHEN `sales_order`.`shipping_method` = 'motoboy_motoboy' THEN 'Express'
                    ELSE `sales_order`.`shipping_description` END) AS tipo_envio")
            );
            $select->where("s_address.address_type='shipping' AND b_address.address_type='billing'");

        }
    }
    
}