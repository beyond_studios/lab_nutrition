<?php
class Janaq_Logistics_Model_Status extends Varien_Object
{
	const STATUS_PENDING        = 0;
	const STATUS_IN_OFFICE	    = 1;
	const STATUS_DELIVERED	    = 2;
	const STATUS_REJECTED	    = 3;


	/**
	get model option as array
	 *
	 * @return array
	 */
	static public function getOptionArray(){
		return array(
			self::STATUS_PENDING	=> Mage::helper('sales')->__('Shipping Pending'),
			self::STATUS_IN_OFFICE	=> Mage::helper('sales')->__('In Office'),
			self::STATUS_DELIVERED	=> Mage::helper('sales')->__('Delivered'),
			self::STATUS_REJECTED	=> Mage::helper('sales')->__('Rechazado - No encontrado')
		);
	}

	public function getOptionString($option){
		
		switch ($option) {
			case '0':
				$string = Mage::helper('sales')->__('Shipping Pending');
				break;
			case '1':
				$string = Mage::helper('sales')->__('In Office');
				break;
			default:
				$string = Mage::helper('sales')->__('Shipping Pending');
				break;
		}
		return $string;
		
	}

	
}