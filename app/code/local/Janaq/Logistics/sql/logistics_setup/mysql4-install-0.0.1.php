<?php
$installer = $this;
$installer->startSetup();

$installer->addAttribute("order", "status_logistics", array("type"=>"int","default"=>0));
$installer->addAttribute("quote", "status_logistics", array("type"=>"int","default"=>0));

$installer->endSetup();