<?php
$installer = $this;
$installer->startSetup();

$installer->addAttribute("order", "mastercard_cardtype", array("type"=>"varchar"));
$installer->addAttribute("quote", "mastercard_cardtype", array("type"=>"varchar"));

$installer->endSetup();