<?php
$installer = $this;
$installer->startSetup();

/*$installer->run("
ALTER TABLE {$installer->getTable('sales/order')} CHANGE `status_logistics` `logistics_status` INT(11) NULL DEFAULT 0;
ALTER TABLE {$installer->getTable('sales/quote')} CHANGE `status_logistics` `logistics_status` INT(11) NULL DEFAULT 0;
");*/

$installer->addAttribute("order", "logistics_assessor", array("type"=>"varchar"));
$installer->addAttribute("quote", "logistics_assessor", array("type"=>"varchar"));

$installer->endSetup();