<?php
/**
 *
 * @category    Janaq
 * @package     Janaq_Logistics
 * @version     1.0.0
 * @author      Janaq Team
 * @copyright   Copyright (c) 2019 Janaq. (https://janaq.com)
 *
 */
class Janaq_Logistics_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function getAllCarriers()
    {
        $methods = Mage::getSingleton('shipping/config')->getAllCarriers();

        $shipMethods = array();
        foreach ($methods as $_code => $_method)
        {
            $shippingTitle = Mage::getStoreConfig("carriers/$_code/title");
            $shippingName = Mage::getStoreConfig("carriers/$_code/name");
            $shipMethod = ($shippingName) ? $shippingTitle. " - ".$shippingName : $shippingTitle."";

            $_code = $_code . '_'. $_code;

            $shipMethods = array_merge($shipMethods, array($_code=>$shipMethod));
        }
        return $shipMethods;
    }

    public function getActiveCarriers()
    {
        $methods = Mage::getSingleton('shipping/config')->getActiveCarriers();

        $shipMethods = array();
        foreach ($methods as $_code => $_method)
        {
            $shippingTitle = Mage::getStoreConfig("carriers/$_code/title");
            $shippingName = Mage::getStoreConfig("carriers/$_code/name");
            $shipMethod = ($shippingName) ? $shippingTitle. " - ".$shippingName : $shippingTitle."";

            $_code = $_code . '_'. $_code;

            $shipMethods = array_merge($shipMethods, array($_code=>$shipMethod));
        }
        return $shipMethods;
    }
}