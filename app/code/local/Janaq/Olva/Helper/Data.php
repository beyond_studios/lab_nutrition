<?php
/**
 * @category   Janaq
 * @package    Janaq_Olva
 * @author     contacto@janaq.com
 
 */
class Janaq_Olva_Helper_Data extends Mage_Core_Helper_Abstract
{
    // AUTH SERVICES
    const TIME_FROM = "carriers/olva/time_from";
    const TIME_TO = "carriers/olva/time_to";
    const DISABLED_DAYS = "carriers/olva/disabled_days";
    const HOLIDAYS = "carriers/olva/holidays";
    const TERMS = "carriers/olva/terms";

    public function getTimeFrom()
    {
        return Mage::getStoreConfig(self::TIME_FROM, Mage::app()->getStore());
    }
    public function getTimeTo()
    {
        return Mage::getStoreConfig(self::TIME_TO, Mage::app()->getStore());
    }
    public function getDisabledDays()
    {
        return Mage::getStoreConfig(self::DISABLED_DAYS, Mage::app()->getStore());
    }
    public function getHolidays()
    {
        return Mage::getStoreConfig(self::HOLIDAYS, Mage::app()->getStore());
    }
    public function getTerms()
    {
        return Mage::getStoreConfig(self::TERMS, Mage::app()->getStore());
    }

}