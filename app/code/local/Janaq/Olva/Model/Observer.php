<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 15/12/2016
 * Time: 12:12 PM
 */

class Janaq_Olva_Model_Observer extends Mage_Payment_Model_Method_Abstract
{

    public function afterSave(Varien_Event_Observer $event)
    {
        $params = Mage::app()->getRequest()->getParams();
        
        $shipping_method = $params['shipping_method'];

        if ($shipping_method == 'olva_olva' ) {

            $shipping_date = $params["olva"]["shipping_date"];
            $new_date = date('Y-m-d', strtotime($shipping_date));
            $new_date = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s',strtotime($new_date));
            $order = $event->getOrder();
            $order->setData('olva_shipping_date',$new_date);

        }
        //Mage::log($params);
    }  
}