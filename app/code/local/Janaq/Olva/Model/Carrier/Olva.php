<?php
/**
 * Created by PhpStorm.
 * User: HOME
 * Date: 12/12/2016
 * Time: 22:52
 */

class Janaq_Olva_Model_Carrier_Olva
    extends Mage_Shipping_Model_Carrier_Abstract
    implements Mage_Shipping_Model_Carrier_Interface
{
    protected $_code = 'olva';

    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        if (!Mage::getStoreConfig('carriers/'.$this->_code.'/active')) {
            return false;
        }

        $freeQty = 0;
        if ($request->getAllItems()) {
            $freePackageValue = 0;
            foreach ($request->getAllItems() as $item) {
                if ($item->getProduct()->isVirtual() || $item->getParentItem()) {
                    continue;
                }

                if ($item->getHasChildren() && $item->isShipSeparately()) {
                    foreach ($item->getChildren() as $child) {
                        if ($child->getFreeShipping() && !$child->getProduct()->isVirtual()) {
                            $freeShipping = is_numeric($child->getFreeShipping()) ? $child->getFreeShipping() : 0;
                            $freeQty += $item->getQty() * ($child->getQty() - $freeShipping);
                        }
                    }
                } elseif ($item->getFreeShipping()) {
                    $freeShipping = is_numeric($item->getFreeShipping()) ? $item->getFreeShipping() : 0;
                    $freeQty += $item->getQty() - $freeShipping;
                    $freePackageValue += $item->getBaseRowTotal();
                }
            }
            $oldValue = $request->getPackageValue();
            $request->setPackageValue($oldValue - $freePackageValue);
        }

        $oldWeight = $request->getPackageWeight();
        $oldQty = $request->getPackageQty();

        //$request->setPackageWeight($request->getFreeMethodWeight());
        $request->setPackageQty($oldQty - $freeQty);

        $result = $this->_getModel('shipping/rate_result');
        $rate = $this->getRate($request);

        $hour = Mage::getModel('core/date')->date('H');

        //$request->setConditionName($rate['weight_base']);

        $request->setPackageWeight($oldWeight);
        $request->setPackageQty($oldQty);
        
        if (!empty($rate) && $rate['price'] >= 0) {
            $delivery_time = $rate['delivery_time'];
            if ($rate['dest_region_id'] == 498 && $hour >= 13) {
                $delivery_time = $delivery_time + 1;
            }

            $method = $this->_getModel('shipping/rate_result_method');

            $method->setCarrier($this->_code);
            $method->setCarrierTitle($this->getConfigData('title'));

            $method->setMethod($this->_code);
            $method->setMethodTitle($this->getConfigData('name'));

            $new_shippingPrice = 0;

            if ($request->getFreeShipping() === true || ($request->getPackageQty() == $freeQty)) {
                $shippingPrice = 0;
            } else {
                $shippingPrice = $this->getFinalPriceWithHandlingFee($rate['price']);
                $priceAditional = 0;
                $weight_base = $oldWeight - $rate['weight_base'];
                if ($weight_base > 0){
                    $priceAditional = ceil($weight_base) * $rate['unit_cost'] ;
                }
                
                $igv = 0.18;
                $new_shippingPrice = ($shippingPrice + $priceAditional);
                $new_shippingPrice = $new_shippingPrice + ($new_shippingPrice * $igv);
            }
            
            $method->setPrice($new_shippingPrice);
            $method->setCost(0);
            $method->setDeliveryTime($delivery_time);

            $result->append($method);
        }
        elseif (empty($rate) && $request->getFreeShipping() === true) {
            /**
             * was applied promotion rule for whole cart
             * other shipping methods could be switched off at all
             * we must show table rate method with 0$ price, if grand_total more, than min table condition_value
             * free setPackageWeight() has already was taken into account
             */
            $request->setPackageValue($freePackageValue);
            $request->setPackageQty($freeQty);
            $rate = $this->getRate($request);
            if (!empty($rate) && $rate['price'] >= 0) {
                $method = $this->_getModel('shipping/rate_result_method');

                $method->setCarrier($this->_code);
                $method->setCarrierTitle($this->getConfigData('title'));

                $method->setMethod($this->_code);
                $method->setMethodTitle($this->getConfigData('name'));

                $method->setPrice(0);
                $method->setCost(0);
                $method->setDeliveryTime($delivery_time);

                $result->append($method);
            }
        }
        else {
            $error = $this->_getModel('shipping/rate_result_error');
            $error->setCarrier($this->_code);
            $error->setCarrierTitle($this->getConfigData('title'));
            $error->setErrorMessage($this->getConfigData('specificerrmsg'));
            $result->append($error);
        }
        
        return $result;

    }
    protected function _getModel($modelName)
    {
        return Mage::getModel($modelName);
    }
    public function getRate(Mage_Shipping_Model_Rate_Request $request)
    {
        return Mage::getResourceModel('olva/olvarate')->getRate($request);
    }

    public function getAllowedMethods()
    {
        return array('olva' => $this->getConfigData('name'));
    }

}