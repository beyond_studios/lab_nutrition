<?php
/**
 * Created by PhpStorm.
 * User: HOME
 * Date: 12/12/2016
 * Time: 11:29
 */

class Janaq_Olva_Block_Carrier_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    protected $_websiteId;

    protected $_conditionName;

    public function __construct()
    {
        parent::__construct();
        $this->setId('shippingOlvaGrid');
        $this->_exportPageSize = 10000;
    }
    public function setWebsiteId($websiteId)
    {
        $this->_websiteId = Mage::app()->getWebsite($websiteId)->getId();
        return $this;
    }
    public function getWebsiteId()
    {
        if (is_null($this->_websiteId)) {
            $this->_websiteId = Mage::app()->getWebsite()->getId();
        }
        return $this->_websiteId;
    }

    protected function _prepareCollection()
    {
        /** @var $collection Mage_Shipping_Model_Mysql4_Carrier_Tablerate_Collection */
        $collection = Mage::getResourceModel('olva/olvarate_collection');
        $collection->setWebsiteFilter($this->getWebsiteId());
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }
    protected function _prepareColumns()
    {
        $this->addColumn('dest_country', array(
            'header'    => $this->__('PAIS'),
            'index'     => 'dest_country',
            'default'   => '*',
        ));

        $this->addColumn('dest_region', array(
            'header'    => $this->__('ESTADO/DEPARTAMENTO'),
            'index'     => 'dest_region',
            'default'   => '*',
        ));

        $this->addColumn('dest_zip', array(
            'header'    => $this->__('PROVINCIA / DISTRITO'),
            'index'     => 'dest_zip',
            'default'   => '*',
        ));

        $this->addColumn('weight_base', array(
            'header'    => $this->__('PESO MAX.'),
            'index'     => 'weight_base',
            'default'   => '*',
        ));

        $this->addColumn('price', array(
            'header'    => $this->__('PRECIO DE ENVIO'),
            'index'     => 'price',
        ));

        $this->addColumn('unit_cost', array(
            'header'    => $this->__('Unit Cost'),
            'index'     => 'unit_cost',
        ));

        $this->addColumn('delivery_time', array(
            'header'    => $this->__('TIEMPO DE ENVIO EN HORAS'),
            'index'     => 'delivery_time',
        ));

        return parent::_prepareColumns();
    }
}