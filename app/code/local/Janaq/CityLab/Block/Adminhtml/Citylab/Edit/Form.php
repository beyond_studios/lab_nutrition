<?php
class Janaq_CityLab_Block_Adminhtml_Citylab_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Init class
     */
    public function __construct()
    {  
        parent::__construct();
     
        $this->setId('citylab_citylab_form');
        $this->setTitle($this->__('City Lab Information'));
    }
    
    /**
     * Setup form fields for inserts/updates
     *
     * return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $model = Mage::registry('citylab_citylab');
        $helper = Mage::helper('sales');

        $style = 'background:transparent;border: 0;padding: 4px 0;';
     
        $form = new Varien_Data_Form(array(
            'id'        => 'edit_form',
            'action'    => $this->getUrl('*/*/save', array(
                'citylab_id' => $this->getRequest()->getParam('citylab_id'))),
            'method'    => 'post'
        ));

        $fieldset = $form->addFieldset('base_fieldset', array(
            'legend'    => $helper->__('City Lab Information'),
            'class'     => 'fieldset-wide',
        ));
     
        if ($model->getCitylabId()) {
            $fieldset->addField('citylab_id', 'hidden', array(
                'name' => 'citylab_id',
            ));
        }

        $fieldset->addField('name', 'label', array(
            'name'      => 'name',
            'label'     => $helper->__('Name'),
            'title'     => $helper->__('Name'),
            'required'  => false,
        ));

        $fieldset->addField('email', 'text', array(
            'name'      => 'email',
            'label'     => $helper->__('Email'),
            'title'     => $helper->__('Email'),
            'required'  => false,
            'readonly'  => true,
            'style'     => $style,
        ));

        $fieldset->addField('birthday', 'text', array(
            'name'      => 'birthday',
            'label'     => $helper->__('Cumpleaños'),
            'title'     => $helper->__('Cumpleaños'),
            'required'  => true,
            'readonly'  => true,
            'style'     => $style,
        ));

        $fieldset->addField('phone', 'text', array(
            'name'      => 'phone',
            'label'     => $helper->__('Telephone'),
            'title'     => $helper->__('Telephone'),
            'required'  => true,
            'readonly'  => true,
            'style'     => $style,
        ));

        $fieldset->addField('dni', 'text', array(
            'name'      => 'dni',
            'label'     => $helper->__('DNI'),
            'title'     => $helper->__('DNI'),
            'required'  => true,
            'readonly'  => true,
            'style'     => $style,
        ));
     
        $fieldset->addField('share_post', 'text', array(
            'name'      => 'share_post',
            'label'     => $helper->__('Publicar Post'),
            'title'     => $helper->__('Publicar Post'),
            'required'  => true,
            'readonly'  => true,
            'style'     => $style,
        ));

        $fieldset->addField('fb_id', 'text', array(
            'name'      => 'fb_id',
            'label'     => $helper->__('Facebook Id'),
            'title'     => $helper->__('Facebook Id'),
            'required'  => true,
            'readonly'  => true,
            'style'     => $style,
        ));

        $fieldset->addField('registered_date', 'text', array(
            'name'      => 'registered_date',
            'label'     => $helper->__('Created At'),
            'title'     => $helper->__('Created At'),
            'required'  => true,
            'readonly'  => true,
            'style'     => $style,
        ));
     
        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);
     
        return parent::_prepareForm();
    }  
}