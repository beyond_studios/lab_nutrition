<?php
class Janaq_CityLab_Block_Adminhtml_Citylab_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Init class
     */
    public function __construct()
    {  
        $this->_blockGroup = 'citylab';
        $this->_controller = 'adminhtml_citylab';
     
        parent::__construct();
     
        //$this->_updateButton('save', 'label', $this->__('Save Citylab'));
        $this->_removeButton('save');
        $this->_updateButton('delete', 'label', $this->__('Delete Citylab'));
    }  
     
    /**
     * Get Header text
     *
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('citylab_citylab')->getCitylabId()) {
            return $this->__('Edit Citylab');
        }  
        else {
            return $this->__('New Citylab');
        }  
    }  
}