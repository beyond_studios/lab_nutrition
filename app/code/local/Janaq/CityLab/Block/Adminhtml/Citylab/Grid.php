<?php
class Janaq_CityLab_Block_Adminhtml_Citylab_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
         
        // Set some defaults for our grid
        $this->setDefaultSort('citylab_id');
        $this->setId('citylab_grid');
        $this->setDefaultDir('desc');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }
    
    protected function _getCollectionClass()
    {
        // This is the model we are using for the grid
        return 'citylab/citylab_collection';
    }
     
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel($this->_getCollectionClass());
        $this->setCollection($collection);
        
        return parent::_prepareCollection();
    }
    
    protected function _prepareColumns()
    {
        $helper = Mage::helper('sales');
        // Add the columns that should appear in the grid

        $this->addColumn('citylab_id', array(
            'header'    => $helper->__('ID'),
            'align'  =>'right',
            'width'  => '30px',
            'index'  => 'citylab_id',
            'type'  => 'text'
        ));

        $this->addColumn('name', array(
            'header'    => $helper->__('Name'),
            'width'  => '120px',
            'index'  => 'name',
            'type'  => 'text',
        ));

        /*$this->addColumn('email', array(
            'header'    => $helper->__('Email'),
            'width'  => '80px',
            'index'  => 'email',
            'type'  => 'text',
        ));*/

        $this->addColumn('birthday', array(
            'header'    => $helper->__('Birthday'),
            'width'  => '80px',
            'index'  => 'birthday',
            'type'  => 'text',
        ));

        $this->addColumn('phone', array(
            'header'    => $helper->__('Phone'),
            'width'  => '80px',
            'index'  => 'phone',
            'type'  => 'text',
        ));

        $this->addColumn('dni', array(
            'header'    => $helper->__('DNI'),
            'align'  =>'left',
            'index'  => 'dni',
            'width'  => '80px',
            'type'  => 'text'
        ));

        /*$this->addColumn('share_post', array(
            'header'    => $helper->__('Publicar Post'),
            'width'  => '20px',
            'index'  => 'share_post',
            'type'  => 'text',
        ));*/

        /*$this->addColumn('fb_id', array(
            'header'    => $helper->__('Facebook Id'),
            'align'  => 'left',
            'width'  => '80px',
            'index'  => 'fb_id',
            'type'      => 'text',
        ));*/

        $citylabId = $this->getRequest()->getParam('citylab', 0);
        $this->addColumn('action',
            array(
                'header'    =>  $helper->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getCitylabId',
                'actions'   => array(
                    array(
                        'caption'   => $helper->__('View'),
                        'url'       => array('base'=> '*/*/edit/'.$citylabId),
                        'field'     => 'citylab_id'
                    )),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'citylab',
                'is_system' => true,
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
         
        return parent::_prepareColumns();
    }

    protected function _prepareMassaction(){
        $this->setMassactionIdField('citylab_id');
        $this->getMassactionBlock()->setFormFieldName('citylab');

        $this->getMassactionBlock()->addItem('delete', array(
            'label'     => Mage::helper('citylab')->__('Delete'),
            'url'       => $this->getUrl('*/*/massDelete'),
            'confirm'   => Mage::helper('citylab')->__('Are you sure?')
        ));

        return $this;
    }
     
    public function getRowUrl($row)
    {
        // This is where our row data will link to
        return $this->getUrl('*/*/edit', array('citylab_id' => $row->getCitylabId()));
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
}