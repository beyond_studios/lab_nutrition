<?php
class Janaq_CityLab_Block_Adminhtml_Citylab extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        // The blockGroup must match the first half of how we call the block, and controller matches the second half
        // ie. citylab/adminhtml_citylab
        $this->_blockGroup = 'citylab';
        $this->_controller = 'adminhtml_citylab';
        $this->_headerText = $this->__('City Lab');
        
        parent::__construct();
        
        $this->_removeButton('add');
    }
}