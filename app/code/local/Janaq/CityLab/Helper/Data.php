<?php
/**
 *
 * @category    Janaq
 * @package     Janaq_CityLab
 * @version     1.0.0
 * @author      Janaq Team
 * @copyright   Copyright (c) 2019 Janaq. (https://janaq.com)
 *
 */
class Janaq_CityLab_Helper_Data extends Mage_Core_Helper_Abstract
{
	const XML_PATH_URL = 'jnqadmin_config/citylab/url';
	
	public function getUrl($store = null){
		return Mage::getStoreConfig(self::XML_PATH_URL, $store);
	}
	
}