<?php
/**
* @author janaq
*/
class Janaq_CityLab_AjaxController extends Mage_Core_Controller_Front_Action
{
	public function savecitylabAction()
	{
		$params = json_decode($this->getRequest()->getRawBody());
		$response['success'] = false;
		$citylab_url = Mage::getStoreConfig('jnqadmin_config/citylab/url');
		$this->getResponse()->clearHeaders()
					->setHeader('Access-Control-Allow-Origin',$citylab_url,true)
					->setHeader('Access-Control-Allow-Credentials','true',true)
					->setHeader('Access-Control-Allow-Headers','origin, x-requested-with, content-type',true)
					->setHeader('Access-Control-Allow-Methods','PUT, GET, POST, DELETE, OPTIONS',true)
					->setHeader('Content-type','application/json',true);
		
		if ('POST' == $this->getRequest()->getMethod()) {
			$error = false;
			if (Zend_Validate::is($params->validarfbid, 'NotEmpty') && Zend_Validate::is($params->fbid, 'NotEmpty')) {
			    $resource = Mage::getSingleton('core/resource');
				$readConnection = $resource->getConnection('core_read');

			    $query = 'SELECT * FROM `citylab` WHERE fb_id='.$params->fbid;

			    $results = $readConnection->fetchAll($query);
			    if (count($results)) {
			    	$response['success'] = true;
				    //Mage::log($results);
			    }
			} else {
				if (!Zend_Validate::is($params->dni, 'NotEmpty') && !Zend_Validate::is($params->dni, 'Digits') && strlen($params->dni) != 8) {
			    $error = true;
				}
				if (!Zend_Validate::is($params->phone, 'NotEmpty') && !Zend_Validate::is($params->phone, 'Digits') && (strlen($params->phone) != 6 || strlen($params->phone) != 9)) {
				    $error = true;
				}
				$validator = new Zend_Validate_Date('DD-MM-YYYY');
				if (!Zend_Validate::is($params->birthday, 'NotEmpty') && !$validator->isValid($params->birthday)) {
				    $error = true;
				}
				if (!Zend_Validate::is($params->name, 'NotEmpty')) {
				    $error = true;
				}
				if (!Zend_Validate::is($params->email, 'NotEmpty')) { //EmailAddress
				    $error = true;
				}
				if (!Zend_Validate::is($params->terms, 'NotEmpty')) {
				    $error = true;
				}
				if (!Zend_Validate::is($params->fbid, 'NotEmpty')) {
				    $error = true;
				}
				if (!$error) {
					$birthday = strtotime(str_replace("/", "-", $params->birthday));
					$params->birthday = Mage::getModel('core/date')->gmtDate('Y-m-d', $birthday);
					$today = Mage::getModel("core/date")->date('Y-m-d H:i:s');
					$resource = Mage::getSingleton('core/resource');

					$writeConnection = $resource->getConnection('core_write');

					$query = "INSERT INTO `citylab` (`name`,`email`,`birthday`,`phone`,`dni`,`share_post`,`fb_id`,`registered_date`) VALUES ('$params->name','$params->email','$params->birthday','$params->phone','$params->dni','$params->sharepost','$params->fbid','$today')";

					$writeConnection->query($query);
					$response['success'] = true;
				}
			}

			$this->getResponse()->setBody(json_encode($response));
	   	}
	}
}
