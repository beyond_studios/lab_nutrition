<?php
class Janaq_CityLab_Adminhtml_CitylabController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {  
        // Let's call our initAction method which will set some basic params for each action
        $this->_initAction()
            ->renderLayout();
    }  
     
    public function newAction()
    {  
        // We just forward the new action to a blank edit form
        $this->_forward('edit');
    }  
     
    public function editAction()
    {  
        $this->_initAction();
     
        // Get id if available
        $id  = $this->getRequest()->getParam('citylab_id');
        $model = Mage::getModel('citylab/citylab');
        if ($id) {
            // Load record
            $model->load($id);
     
            // Check if record is loaded
            if (!$model->getCitylabId()) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('This city lab register no longer exists.'));
                $this->_redirect('*/*/');
     
                return;
            }
        }  
     
        $this->_title($model->getCitylabId() ? $model->getEmail() : $this->__('New City Lab'));
     
        $data = Mage::getSingleton('adminhtml/session')->getCitylabData(true);
        if (!empty($data)) {
            $model->setData($data);
        }
     
        Mage::register('citylab_citylab', $model);
     
        $this->_initAction()
            ->_addBreadcrumb($id ? $this->__('Edit City Lab') : $this->__('New City Lab'), $id ? $this->__('Edit City Lab') : $this->__('New City Lab'))
            ->_addContent($this->getLayout()->createBlock('citylab/adminhtml_citylab_edit')->setData('action', $this->getUrl('*/*/save')))
            ->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody($this->getLayout()->createBlock('citylab/adminhtml_citylab_grid')->toHtml());
    }
     
    public function saveAction()
    {
        if ($postData = $this->getRequest()->getPost()) {
            $model = Mage::getSingleton('citylab/citylab');
            $model->setData($postData);

            try {
                if($model->getCitylabId()){
                    $model->save();

                    Mage::getSingleton('adminhtml/session')->addSuccess($this->__('El registro ha sido guardado.'));
                    $this->_redirect('*/*/');
     
                    return;
                }
                else{
                    Mage::getSingleton('adminhtml/session')->addError($this->__('El registro no puede ser guardado.'));
                    $this->_redirect('*/*/');
                    return;
                }
            }  
            catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('An error occurred while saving this city lab.'));
            }
 
            Mage::getSingleton('adminhtml/session')->setBookData($postData);
            $this->_redirectReferer();
        }
    }
     
    public function messageAction()
    {
        $data = Mage::getModel('citylab/citylab')->load($this->getRequest()->getParam('citylab_id'));
        echo $data->getContent();
    }


    public function massStatusAction() {
        $citylabIds = $this->getRequest()->getParam('citylab');
        if (!is_array($citylabIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                foreach ($citylabIds as $citylabId) {
                    $citylab = Mage::getSingleton('citylab/citylab')->load($citylabId);
                    $citylab->setIsMassupdate(true)->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated', count($citylabIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function massDeleteAction()
    {
        $citylabIds = $this->getRequest()->getParam('citylab');
        if(!is_array($citylabIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(es).'));
        } else {
            try {
                $citylabModel = Mage::getModel('citylab/citylab');
                foreach ($citylabIds as $citylabId) {
                    $citylabModel->load($citylabId)->delete();
                }
                    Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('citylab')->__('Total of %d record(s) were deleted.', count($citylabIds)
                )
            );
            } 
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
         
        $this->_redirect('*/*/index');
    }

    public function exportCsvAction()
    {
        $fileName   = "citylab.csv";
        $gridBlock  = $this->getLayout()->createBlock('citylab/adminhtml_citylab_grid');

        $content    = $gridBlock->getCsvFile();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Initialize action
     *
     * Here, we set the breadcrumbs and the active menu
     *
     * @return Mage_Adminhtml_Controller_Action
     */
    protected function _initAction()
    {
        $this->loadLayout()
            // Make the active menu match the menu config nodes (without 'children' inbetween)
            ->_setActiveMenu('customer/citylab_citylab')
            ->_title($this->__('Customer'))->_title($this->__('City Lab'))
            ->_addBreadcrumb($this->__('Customer'), $this->__('Customer'))
            ->_addBreadcrumb($this->__('City Lab'), $this->__('City Lab'));
        return $this;
    }
     
    /**
     * Check currently called action by permissions for current user
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('customer/citylab');
    }
}