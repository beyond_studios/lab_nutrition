<?php

class Janaq_AdminJanaq_Helper_Data extends Mage_Core_Helper_Abstract
{
    const CUSTOM_PROMO_ENABLE = "jnqadmin_config/custompromos/enable";

	const SALEBANNER_ENABLE = "jnqadmin_config/sale_banner/enable";
	const SALEBANNER_SKU = "jnqadmin_config/sale_banner/sku";
	const SALEBANNER_URL = "jnqadmin_config/sale_banner/url";
	const SALEBANNER_SHOWPOPUP = "jnqadmin_config/sale_banner/show_popup";
	const SALEBANNER_POPUPIMAGE = "jnqadmin_config/sale_banner/popup_image";
	const SALEBANNER_BANNERIMAGE = "jnqadmin_config/sale_banner/banner_image";

	const XML_PATH_ENABLED       = 'timer/general/enabled';
	const XML_PATH_TIMER_HEADING = 'timer/general/timer_heading';
	const XML_PATH_TIMER_CAPTION = 'timer/general/timer_catption';
	const XML_PATH_TITLE 		 = 'timer/general/title';

	const REDIRECT_PRODUCT_ENABLE = "jnqadmin_config/redirect_product/enable";
	
	public function conf($code, $store = null){
		return Mage::getStoreConfig($code, $store);
	}
	
	public function isTimerEnabled($store = null){
		return $this->conf(self::XML_PATH_ENABLED, $store);
	}
	
	public function isTimerHeading($store = null){
		return $this->conf(self::XML_PATH_TIMER_HEADING, $store);
	}
	
	public function isTimerCaption($store = null){
		return $this->conf(self::XML_PATH_TIMER_CAPTION, $store);
	}
	
	public function getTimerTitle($store = null){
		return $this->conf(self::XML_PATH_TITLE, $store);
	}
	
	public function isShowTitle($currentpage = null){
		if($this->isTimerHeading() == 'showall'){
			return true;
		} else if($this->isTimerHeading() == $currentpage){
			return true;
		} else if($this->isTimerHeading() != 'hideall'){
			return false;
		} else {
			return false;
		}
	}
	
	public function isShowCaption($currentpage = null){
		if ($this->isTimerCaption() == 'showall'){
			return true;
		} else if($this->isTimerCaption() == $currentpage) {
			return true;
		} else if($this->isTimerCaption() != 'hideall'){
				return false;
		}else {
			return false;
		}
	}

	public function redirectProductEnabled()
	{
		return (bool)Mage::getStoreConfig(self::REDIRECT_PRODUCT_ENABLE);
	}

	public function isEnabled()
	{
		return (bool)Mage::getStoreConfig(self::SALEBANNER_ENABLE);
	}
    public function getSaleBannerUrl()
    {
    	return Mage::getStoreConfig(self::SALEBANNER_URL);
    }
    public function saleBannerPopupEnable()
    {
        return (bool)Mage::getStoreConfig(self::SALEBANNER_SHOWPOPUP);
    }
    public function saleBannerPopupImage()
    {
        return Mage::getStoreConfig(self::SALEBANNER_POPUPIMAGE);
    }
    public function saleBannerBannerImage()
    {
        return Mage::getStoreConfig(self::SALEBANNER_BANNERIMAGE);
    }

    public function isCustomPromosEnable()
    {
        return Mage::getStoreConfig(self::CUSTOM_PROMO_ENABLE);
    }

    public function getProductLabels($_product) {
		$finalPrice = $_product->getFinalPrice();
		$price =  $_product->getPrice();
		if($finalPrice < $price){
			echo '<div class="sale-label">'.$this->__('Sale').'</div>';
		}

		$now = date("Y-m-d");
		$newsFrom = substr($_product->getData('news_from_date'),0,10);
		$newsTo = substr($_product->getData('news_to_date'),0,10);		
		if(($now >= $newsFrom) && ($now <= $newsTo)){
			echo '<div class="new-label">'.$this->__('New').'</div>';
		}
    }
	
	public function text_truncate($string, $limit, $break=".", $pad="…") {
		// return with no change if string is shorter than $limit
		if(strlen($string) <= $limit)
		return $string;
		// is $break present between $limit and the end of the string?
		if(false !== ($breakpoint = strpos($string, $break, $limit))) {
			if($breakpoint < strlen($string) - 1) {
				$string = substr($string, 0, $breakpoint) . $pad;
				}
			}
		return $string;
	}


    public function getCustomerData()
    {
        if (Mage::helper('customer')->isLoggedIn()) {
            return Mage::getSingleton('customer/session')->getCustomer();
        }
        return false;
    }
    public function getPrimaryShippingAddressRegion()
    {
        if ($customer = $this->getCustomerData()) {
            $primaryShippingAddress = $customer->getPrimaryShippingAddress();
            if ($region = isset($primaryShippingAddress)) {
                return $primaryShippingAddress->getRegion();
            }
            else {
                return false;
            }
        }
        return false;
    }
	public function getMessageError()
    {
        $message = Mage::getStoreConfig('jnqadmin_config/custompromos/message');
        if (!$message) {
            $message = "No se puede agregar el producto, el monto del carrito no es suficiente.";
        }
        return $message;
    }
    public function getSkus()
    {
        $skus = Mage::getStoreConfig('jnqadmin_config/custompromos/sku');
        if ($skus) {
            $skus = trim(preg_replace( "/\r|\n/", "", $skus));
            $skus = explode (',', $skus);
            return $skus;
        }
        return false;
    }

	function getMarcaProduct($_product){
		$marca_title = "";
		$_manufacturer = Mage::getModel('manufacturer/manufacturer')
		              ->loadByManufacturerId($_product->getData('manufacturer'));
		if ($_manufacturer->getStatus()) {
			$marca_title = '<span class="marca">'.$_manufacturer->getTitle() .' <span class="reg">®</span>' . '</span>';
		}
		return $marca_title;
	}

	function getManufacturerProduct($_product){
		$marca_title = "";
		if ($marca = $_product->getAttributeText('manufacturer')) {
			$marca_title = '<span class="marca">'.$marca .' <span class="reg">®</span>' . '</span>';
		}
		return $marca_title;
	}

	function getManufacturersFilter(){
		$manufacturers = Mage::getModel('manufacturer/manufacturer')->getCollection()->addFieldToFilter('status',Array('eq'=>1))->setOrder('position','asc');
		$marcas = array();
		foreach($manufacturers as $manufacturer){
			$marcas[] = array(
				'category' => 'Marcas',
				'value' => $manufacturer->getUrl(),
				'label' => $manufacturer->getTitle(),
			);
		}
		return $marcas;
	}

	function getManufacturersList(){
		$manufacturers = Mage::getModel('manufacturer/manufacturer')->getCollection()->addFieldToFilter('status',Array('eq'=>1))->setOrder('position','asc');
		$marcas = '<li class="filtermarcas">';
		$marcas .= '<header class="header-auto"><h4>Marcas</h4></header>';
		$marcas .= '<ul>';
		foreach($manufacturers as $manufacturer){
		 $marcas .= '<li class="marca"><a href="'.$manufacturer->getUrl().'">'.$manufacturer->getTitle().'</a></li>';
		}
		$marcas .= '</ul>';
		return $marcas . '</li>';
	}

	function getManufacturers(){
		return Mage::getModel('manufacturer/manufacturer')->getCollection()->addFieldToFilter('status',Array('eq'=>1))->setOrder('position','asc');
	}

	function getCatChildren() {
		$categoryMenu = Mage::getModel('catalog/category')->load(2);
		$children = Mage::getModel('catalog/category')->getCollection()
					->setStoreId(Mage::app()->getStore()->getId());
		$children->addAttributeToSelect('*')
		      ->addAttributeToFilter('parent_id', $categoryMenu->getId())
		      ->addAttributeToFilter('is_active', 1)//get only active categories if you want
		      ->addAttributeToFilter('include_in_menu', 1)
		      ->addAttributeToFilter('children_count', array('gteq' => 1))
		      ->addAttributeToSort('position');
		return $children;
	}

	public function get_categories($categories, $bool) {
		$catclass = '';
		$emptychildren = '';
		$parentclass = '';
		if ($bool) {
			$catclass = 'class="filtercats-search ui-autocomplete" id="filtercats"';
		}
		$array= '<ul '.$catclass.'>';
		foreach($categories as $category) {
			$cat = Mage::getModel('catalog/category')->load($category->getId());
			//$count = $cat->getProductCount();
			if ($category->hasChildren()) {
				$parentclass = 'parent ';
			}
			elseif(!$category->hasChildren() && $cat->getLevel() == 2){
				$emptychildren = ' hidden';
			}
			$array .= '<li class="'.$parentclass.'cat-'. $cat->getId().$emptychildren.'">';
			if ($cat->getLevel() == 2) {
			  $array .= '<header class="header-auto"><h4>'.$category->getName().'</h4></header>';
			}
			else{
				$array .= '<a href="' . Mage::getUrl($cat->getUrlPath()). '">' .
				$category->getName() . "</a>\n";
			}

			if($category->hasChildren()) {
				if ($category->getParentId() != 8) {
					$children = Mage::getModel('catalog/category')->getCategories($category->getId());
					$array .= $this->get_categories($children, false);
				}
			}
			$array .= '</li>';
		}
		if ($bool) {
			$array .= $this->getManufacturersList();
		}
		return $array . '</ul>';
	}

	function getCategories() {
		$storeId = Mage::app()->getStore()->getId();
		$categories = Mage::getModel('catalog/category')->getCollection()
		  ->setStoreId($storeId)
		  ->addAttributeToSelect('id')
		  ->addAttributeToSelect('name')
		  ->addAttributeToFilter('is_active', 1);
		$names = array();
		foreach ($categories as $category) {
			$_cat_filter ="";
			if ($category->getLevel() == 2 || $category->getLevel() == 3) {
				foreach ($category->getParentCategories() as $parent) {
					if ($parent->getLevel() == 2) {
						$_cat_filter = $parent->getName();
					}
				}
				$names[] = array(
					'category' => $_cat_filter,
					'value' => $category->getUrl(),
					'label' => $category->getName(),
				);
			}
		}
		return $names;
	}

	function getFiltersToSearch(){
		$mar_filter = $this->getManufacturersFilter();
		/*$cat_filter = $this->getCategories();
		$_search_fields = array_merge($mar_filter,$cat_filter);*/
		return $mar_filter;
	}

	function getRatingNumber($_product){
		$html = '';
		$rating_number = '';
		$storeId = Mage::app()->getStore()->getId();
		$summaryData = Mage::getModel('review/review_summary')->setStoreId($storeId)
		            ->load($_product->getId());
		$rating_number = ($summaryData['rating_summary'] )/10;
		$rating_number = number_format($rating_number,1,'.',' ');
		if ($rating_number != 0) {
		$html = '<div class="item-point">';
		$html .= '<div class="item-point-content">'.$rating_number.'<h6>PTS</h6></div>';
		$html .= '</div>';
		}
		return $html;
	}

	function getProductAvailableSizes($_product){
		$content_attr = "";
		$cProduct = Mage::getModel('catalog/product')->load($_product->getId());
		if ($cProduct->getData('type_id') == "configurable"){
			$configAttrArray = $_product->getTypeInstance(true)->getConfigurableAttributesAsArray($_product);
			$configAttrFilteredBySize = array_filter(
				$configAttrArray, function($v) { 
				return $v['attribute_code'] == 'size'; 
			});
			//get code attribute
			$code_attr = $configAttrFilteredBySize[key($configAttrFilteredBySize)]['attribute_code'];

			$configAttrFilteredBySizeValues = $configAttrFilteredBySize[key($configAttrFilteredBySize)]['values'];
			//get the configurable product its childproducts
			$childProducts = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null,$_product);
			$content_attr .= "<div class='available-".$code_attr."'>";
			//loop the values, and the childproducts and match them
			$content_attr .= "<ul class='avattr-content list-inline'>";
			$content_attr .= "<h4>Disponible en: </h4>";
			$values_attr = array();
			$values_attr_un = array();
			$temp = "";
			foreach($configAttrFilteredBySizeValues as $configAttributeValue){
				//$content_attr .= $configAttributeValue['label'] . ' ';
				$value = $configAttributeValue['value_index'];
				foreach($childProducts as $childProduct){
			      	if($childProduct->getSize() == $value){
			          	$values_attr[] = $childProduct->getAttributeText('size');
			      	}
			  	}
			}
			$values_attr_un = array_unique($values_attr);
			foreach ($values_attr_un as $valueattr) {
				$temp .= "<li>".$valueattr . '</li>';
			}
			$content_attr .= $temp;
			$content_attr .= "</ul>";
			$content_attr .= "</div>";
		}
		echo $content_attr;
	}

	function getProductAvailableSizesMenu($_product){
		$content_attr = "";
		$cProduct = Mage::getModel('catalog/product')->load($_product->getId());
		if ($cProduct->getData('type_id') == "configurable"){
			$configAttrArray = $_product->getTypeInstance(true)->getConfigurableAttributesAsArray($_product);
			$configAttrFilteredBySize = array_filter($configAttrArray, function($v) { return $v['attribute_code'] == 'size'; });

			$configAttrFilteredBySizeValues = $configAttrFilteredBySize[key($configAttrFilteredBySize)]['values'];
			//get the configurable product its childproducts
			$childProducts = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null,$_product);
			//loop the values, and the childproducts and match them
			$values_attr = array();
			$values_attr_un = array();
			$temp = "";
			foreach($configAttrFilteredBySizeValues as $configAttributeValue){
				$value = $configAttributeValue['value_index'];
				foreach($childProducts as $childProduct){
					if($childProduct->getSize() == $value){
						$values_attr[] = $childProduct->getAttributeText('size');
					}
				}
			}
			$values_attr_un = array_unique($values_attr);
			foreach ($values_attr_un as $valueattr) {
				$temp .= $valueattr .' | ';
			}
			$content_attr .= $temp;
			$content_attr = trim($content_attr);
			$content_attr = trim($content_attr,'|');
		}
		return $content_attr;
	}

	public function getStockText($stock)
	{
		$text = "";
		if ($stock) {
			$_stock = "<strong>" . $stock . "</strong>";
			if ((floatval($stock) <= 1)) {
				$text = "<p>". $this->__("There is %s unit.", $_stock) . "</p>";
			}
			else{
				$text = "<p>". $this->__("There are %s units.", $_stock) . "</p>";
			}
		}
		return $text;
	}

	public function getPriceCountDown($product){
		if (!$this->isTimerEnabled()) {
			return false;
		}
    	if($product && $product->getShowTimer()) {
    		$currentDate = Mage::getModel('core/date')->date('Y-m-d');
    		$fromdate =  $product->getSpecialFromDate();
    		$todate =  $product->getSpecialToDate();
    		if($product->getSpecialPrice() != 0 || $product->getSpecialPrice()) {
    			if($product->getSpecialToDate() != null) {
    				if(strtotime($todate) >= strtotime($currentDate) && strtotime($fromdate) <= strtotime($currentDate)){
    					return true;
    				}	
    			}
    		}
    	}
    	return false;
    }

    public function resizeImage($imageURL, $width, $height=NULL, $imagePath=NULL)
    {
        $imageName = basename($imageURL);

        $mySaveDir = Mage::getBaseDir('media') . DS . 'feed' . DS ;
        $io = new Varien_Io_File();
        $io->checkAndCreateFolder($mySaveDir);

        $completeSaveLoc = $mySaveDir.$imageName;
        if(!file_exists($completeSaveLoc)){
            try {
                file_put_contents($completeSaveLoc, file_get_contents($imageURL));
            } catch (Exception $e) {

            }
        }

        $imagePath = str_replace("/", DS, $imagePath);
        $imagePathFull = Mage::getBaseDir('media') . DS . $imagePath . DS . $imageName;
        
        if($width == NULL && $height == NULL) {
            $width = 300;
            $height = 300;
        }
        $resizePath = $width . 'x' . $height;
        $resizePathFull = Mage::getBaseDir('media') . DS . $imagePath . DS . $resizePath . DS . $imageName;
                
        if (file_exists($imagePathFull) && !file_exists($resizePathFull)) {
            $imageObj = new Varien_Image($imagePathFull);
            $imageObj->constrainOnly(TRUE);
            $imageObj->keepAspectRatio(TRUE);
            $imageObj->resize($width,$height);
            $imageObj->save($resizePathFull);
        }

        $imagePath = str_replace(DS, "/", $imagePath);
        return Mage::getBaseUrl("media") . $imagePath . "/" . $resizePath . "/" . $imageName;
    }

    public function getPlaceholderImage($type="base", $sizes=200)
    {
    	if ($type == "base") {
    		$placeholder = Mage::getModel('catalog/product')->getBaseUrl($sizes,$sizes);
    	}
    	else if($type == "thumbnail") {
    		$placeholder = Mage::getModel('catalog/product')->getThumbnailUrl($sizes,$sizes);
    	}
    	else {
    		$placeholder = Mage::getModel('catalog/product')->getSmallImageUrl($sizes,$sizes);
    	}
    	return $placeholder;
    }

    public function getSVGPath($url, $extension = false)
    {
    	$path = $url;
    	if ($extension) {
    		$path = $path .'.svg';
    	}
    	$file = @file_get_contents($path);
    	if ($file) {
    		$find_string  = '<svg';
			$position = strpos($file, $find_string);

			$svg_file = substr($file, $position);
    	}
    	else {
    		$default = Mage::getModel('core/design_package')->getSkinUrl('images/favicon.svg');
    		$svg_file = file_get_contents($default);
    	}

		return $svg_file;
    }

}