<?php
/**
* @author janaq
*/
class Janaq_AdminJanaq_AjaxController extends Mage_Core_Controller_Front_Action
{
	public function productsAction()
	{
		$limit = 15;
		$id = $this->getRequest()->getParam("id");
		//$order = Mage::getStoreConfig('catalog/frontend/default_sort_by');

		$category = Mage::getModel('catalog/category')->load($id);

		$products = $category->getProductCollection()
					->addAttributeToSort("position","asc")
					->addAttributeToSelect('*') // add all attributes - optional
					->addAttributeToFilter('status', 1) // enabled
					->addAttributeToFilter('visibility', 4);
		$products->getSelect()->limit($limit);
		//Mage::log($category->getData());

		$productBlock = $this->getLayout()->createBlock('catalog/product_price');
		$html = '<div class="container">';
		$html .= '<ul class="owl-carousel owl-theme owl-grid">';

		foreach ($products as $product) {
			$image = Mage::helper('catalog/image')->init($product, 'small_image')->resize(200,200);
			$marca = Mage::helper('jnqadmin')->getManufacturerProduct($product);
			
			$html .= '<li class="item slider-item col-md-4">';
			$html .= '<div class="product-image-block">';
			$html .= '<a href="'. $product->getProductUrl() .'" title="'.$product->getName().'">';
			$html .= '<img alt="'.$product->getName().'" src="'.$image.'" width="200" height="200">';
			$html .= '</a>';
			$html .= '</div>';
			$html .= '<h2 class="product-name">';
			$html .= '<a href="'. $product->getProductUrl() .'">'.$product->getName();
			$html .= $marca;
			$html .= '</a>';
			$html .= '</h2>';
			$html .= $productBlock->getPriceHtml($product, true);
			$html .= $this->getCartButton($product);
			$html .= '</li>';
		}
		$html .= '</ul>';
		$html .= '<div class="owl-loading"><i class="fa fa-spinner fa-spin"></i></div>';
		$html .= '</div>';
		$this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
		$this->getResponse()->setBody(json_encode($html));
	}

	public function getCartButton($_product) {
		$_button = "";
		$click = Mage::helper('udevix_google_tag_manager_ee')->click($_product);
		if(!$_product->canConfigure() && $_product->isSaleable()) {
			$setLocation = "'".Mage::helper('checkout/cart')->getAddUrl($_product)."'";
			$_button = '<button '.$click.' type="button" title="'.$this->__('Buy') .'" class="button btn-cart" onclick="setLocation('.$setLocation.')"><span>'.$this->__('Buy') .'</span></button>';
		}
		else {
			$setLocation = $_product->getUrl();
			$_button = '<a '.$click.' title="Ver Detalles" class="button btn-cart" href="'.$setLocation.'"><span>Ver Detalles</span></a>';
		}
		return $_button;
	}

	public function storeslistAction()
	{
		$html = '';
		$d = $this->__('Direction');
		$baseUrl = Mage::getBaseUrl();
		$arrayImage = Mage::helper('storelocator')->getArrayImage();
		$storeCollection = Mage::helper('storelocator')->getListStoreCollection();
		$html .= '<ul id="h_list-store-detail">';
		foreach ($storeCollection as $store) {
			$storeUrl = $baseUrl.$store->getData('rewrite_request_path');
			$html .= '<li class="el-content">';
			$html .= '<div class="top-box table d-table no-margin">';
			$html .= '<div class="col-sm-4 col-xs-4 tag-store no-padding">';
			$html .= '<a href="'.$storeUrl.'">';
			$image = ($arrayImage[$store->getId()])?Mage::getBaseUrl('media').'storelocator/images'.$arrayImage[$store->getId()]:Mage::getDesign()->getSkinUrl('images/storelocator/image-default.png');
			$html .= '<img alt="'.$store->getName().'" src="'.$image.'"/>';
			$html .= '</a></div>';
	        $html .= '<div class="col-sm-8 col-xs-8 tag-content">';
	        $html .= '<h4 class="text-uppercase">';
	        $html .= '<a href="'.$storeUrl.'" class="view-detail">'.$store->getName().'</a></h4>';
	        $html .= '<div class="buttons-set no-padding no-margin a-left">';
	        $html .= '<a href="'.$storeUrl.'" class="btn btn-link direction">'.$d .'</a>';
	        $html .= '<span class="btn btn-link store-hours">'.$this->__('Opening hours').'</span>';
	        $html .= '</div></div></div>';
	        $html .= '<div class="h_tab_content open" style="display: none;">';
	        $html .= '<div id="h_open_hour'.$store->getStorelocatorId().'" class="clearfix">';
	        $html .= '<h2 class="open_hour_title text-center"><span>'.$this->__('Opening hours').'</span></h2>';
	        $html .= '<div class="table-responsive">';
	        $html .= '<table class="table no-margin">';
	        $html .= Mage::helper('storelocator')->getWorkingTime($store, "G:i");
	        $html .= '</table></div></div>';
	        $specialDays = Mage::helper('storelocator')->getSpecialDays($store->getId());
	        $holidays = Mage::helper('storelocator')->getHolidayDays($store->getId());
	        $html .= '<div class="col-xs-12 tabs">';
	        $html .= '<ul>';
	        if (count($specialDays)) {
	        	$holidaysClass = (!count($holidays)) ? " full-width":" active full-width";
	        	$html .= '<li class="tab_open active'.$holidaysClass.' name="tab" id="h_tab_1" href="javascript:void(0)" onClick="storetabs(1)" >'.$this->__('Special Days').'</li>';
	        }
	        if (count($holidays)) {
	        	$hdstyle = (!count($specialDays))?"width:100%":"";
	        	$html .= '<li class="tab_open'.$holidaysClass.' style="'.$hdstyle.'" name="tab" id="h_tab_2" href="javascript:void(0)" onClick="storetabs(2)">'.$this->__('Holidays').'</li>';
	    	}
	    	$html .= '</ul></div>';
	        if (count($specialDays)) {
		        $html .= '<div name="tab_content" id="h_tab_content_1" class="tab_content display active">';
		        $html .= '<div id="h_open_hour" class="speacial-days">';
		        foreach ($specialDays as $specialDay) {
		        	$html .= '<h3 class="title">'.$specialDay['name'].'</h3>';
		        	foreach ($specialDay['date'] as $sp) {
		        		$html .= '<li><div class="content1">';
		        		$html .= date_format(date_create($sp), 'l jS F');
		        		$html .= '</div><div class="content2">';
		        		if ($specialDay['time_open'] != null && $specialDay['time_open'] != $specialDay['time_close']){
		        			$html .= date("H:i", strtotime($specialDay['time_open'])) . ' - ' . date("H:i", strtotime($specialDay['time_close']));
		        		} else {
		        			$html .= $this->__('Open');
		        		}
		        		$html .= '</div></li>';
		        	}
		        }
		        $html .= ' </div></div>';
	    	}
	    	if (count($holidays)) {
	    		$hdclass = (!count($specialDays)) ? " active":"";
	    		$html .= '<div name="tab_content" id="h_tab_content_2" class="tab_content display'.$hdclass.'">';
	    		$html .= '<div id="h_open_hour" class="holidays">';
	    		foreach ($holidays as $holiday) {
	    			$html .= '<h3 class="title">'.$holiday['name'].'</h3>';
	    			foreach ($holiday['date'] as $hl) {
	    				$html .= '<li><div class="content1">';
	    				$html .= date_format(date_create($hl), 'l jS F');
	    				$html .= '</div><div class="content2">'.$this->__('Closed');
	    				$html .= '</div></li>';
	    			}
	    		}
	    		$html .= '</div></div>';
	    	}
            $html .= '</div></li>';
        }
        $html .= '</ul>';
		$this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
		$this->getResponse()->setBody(json_encode($html));
	}

	public function getcurrentdateAction()
	{
		$date = Mage::getModel('core/date')->date('m/d/y h:i:s A');
		$this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
		$this->getResponse()->setBody(json_encode($date));
	}
}
