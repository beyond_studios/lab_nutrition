<?php
/**
 * @author janaq
 */
class Janaq_AdminJanaq_Adminhtml_JnqadminController extends Mage_Adminhtml_Controller_Action
{
	public function updateshippingpriceAction() {
        $order_id = $this->getRequest()->getParam('order_id', false);
        $order = Mage::getModel('sales/order')->load($order_id);

        $price = $this->getRequest()->getParam('price', false);
        $s_method = $this->getRequest()->getParam('shipping_method', false);
        $sm_title = $this->getRequest()->getParam('sm_title', false);
        
        $lat_lng = $this->getRequest()->getParam('lat_lng', false);
        $urb_order_type = $this->getRequest()->getParam('urb_order_type', false);
        $urb_vehicle = $this->getRequest()->getParam('urb_vehicle', false);
        $storepickup_id = $this->getRequest()->getParam('storepickup_id', false);
        $result = array('error' =>true);
        if ($order) {
            $order->setShippingAmount($price);
            $order->setBaseShippingAmount($price);

            //$order->setBaseSubtotal($order->getBaseSubtotal() + $price);
            $order->setGrandTotal($order->getSubtotal() + $price);
            $order->setBaseGrandTotal($order->getSubtotal() + $price);

            if ($s_method) {
                $order->setShippingMethod($s_method);
                $order->setShippingDescription($sm_title);

                if ($s_method == 'urbaner_urbaner' || $s_method == 'alldaydelivery_alldaydelivery') {
                    $id_default_store = Mage::helper('urbaner')->getIdStoreDefault();
                    $order->setUrbanerAddresses($order->getShippingAddress()->getStreet(1));
                    $order->setUrbanerLatLng($lat_lng);
                    $order->setUrbanerOrderType($urb_order_type);
                    $order->setUrbanerVehicle($urb_vehicle);
                    $order->setUrbanerAlmacenId($id_default_store);
                } else {
                    $order->setUrbanerAddresses(NULL);
                    $order->setUrbanerLatLng(NULL);
                    $order->setUrbanerOrderType(NULL);
                    $order->setUrbanerVehicle(NULL);
                    $order->setUrbanerAlmacenId(NULL);
                    if ($s_method == 'storepickup_storepickup') {
                        $order->setStoreStorepickup($storepickup_id);
                        $order->setServientregaStatus($sm_title);
                    }
                }
            }

            $order->save();
            $result['error'] = false;

            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        }
        
    }

    public function getadminshippingmethodsAction()
    {
        $order_id = $this->getRequest()->getParam('order_id', false);
        $order = Mage::getModel('sales/order')->load($order_id);
        $html = '<div class="field-row"><ul>';
        $result = array('error' => true);
        $urbaner_helper = Mage::helper('urbaner');

        $methodswithrates = array('ondeliveryship', 'olva', 'servientrega', 'urbaner', 'alldaydelivery');
        $methods = Mage::getSingleton('shipping/config')->getActiveCarriers();
        $shipping_address = $order->getShippingAddress();
        $shipping_method = $order->getShippingMethod();
        $dest_country_id = $shipping_address->getCountryId();
        $dest_region_id = $shipping_address->getRegionId();
        $dest_postcode = $shipping_address->getPostcode();
        $street = $shipping_address->getStreet(1);
        $o_store_id = $order->getStoreId();
        $o_weight = $order->getPackageWeight();
        //Mage::log($order->getData());

        $shipMethods = array();
        foreach ($methods as $_code => $_method)
        {
            $shippingTitle = Mage::getStoreConfig("carriers/$_code/title");
            $shippingName = Mage::getStoreConfig("carriers/$_code/name");
            $shipping_price = 0;
            $shipping_latlong = "";
            $store_list = "";
            $_codewith_rate = ($_code == 'urbaner' || $_code == 'alldaydelivery') ? true : false;
            if (in_array($_code, $methodswithrates)) {
                $priceAditional = 0;
                $sm_price = $this->getShippingPrice($_code, $o_store_id, $dest_country_id, $dest_region_id, $dest_postcode, $o_weight);
                //Mage::log($sm_price);
                if ($sm_price) {
                    $shipping_price = $sm_price['price'];
                }
                if ($_codewith_rate) {
                    $vehicle = $order->getUrbanerVehicle() ? $order->getUrbanerVehicle() : 1;
                    $order_type = $order->getUrbanerOrderType() ? $order->getUrbanerOrderType() : 1;
                    $almacen = $order->getUrbanerAlmacenId() ? $order->getUrbanerAlmacenId() : Mage::helper('urbaner')->getIdStoreDefault();
                    $location = $street.', '.$dest_postcode;
                    if (!$order->getUrbanerLatLng()) {
                        $latlong  = $this->getLatLong($location);
                    } else {
                        $latlong = $order->getUrbanerLatLng();
                    }
                    $shipping_latlong = $latlong;
                    //Mage::log($latlong);
                    $urbaner = Mage::getModel('urbaner/service');
                    if ($price = $urbaner->getPriceShipping($latlong, $vehicle, $order_type, $almacen)) {
                        $shipping_price = $price;
                    }
                    $weight_base = $o_weight - $sm_price['weight_base'];
                    if ($weight_base > 0){
                        $priceAditional = ceil($weight_base);
                    }
                    $shipping_price = ($shipping_price + $priceAditional);
                } else {
                    $weight_base = $o_weight - $sm_price['weight_base'];
                    if ($weight_base > 0){
                        $priceAditional = ceil($weight_base) * $sm_price['unit_cost'];
                    }

                    if ($_code == 'olva') {
                        $igv = 0.18;

                        $new_shippingPrice = ($shipping_price + $priceAditional);
                        $shipping_price = $new_shippingPrice + ($new_shippingPrice * $igv);
                    } else {
                        $shipping_price = ($shipping_price + $priceAditional);
                    }
                }
            } else if ($_code == 'storepickup') {
                $store_collection = Mage::app()->getLayout()->getBlockSingleton('storepickup/storepickup')->getCollectionStoreLocation();
                if (count($store_collection)) {
                    $store_list .= '<ul class="store_list"><li>Tiendas Disponibles:</li>';
                    foreach ($store_collection as $key => $store) {
                        $storeDepartamento = $store->getState();
                        $desc = $store->getName();
                        $s_checked = "";
                        if ($order->getStoreStorepickup() == $store->getId()) {
                            $desc .= '<br><span style="padding-left: 13px;">Ubicación: '.$storeDepartamento.'</span>';
                            $s_checked = "checked";
                        }
                        $store_list .= '<li><label for="storepickup_'.$store->getId().'" style="width: 100%;"><input type="radio" '.$s_checked.' id="storepickup_'.$store->getId().'" name="storepickup_id" value="'.$store->getId().'">'.$desc.'</label></li>';
                    }
                    $store_list .= '</ul>';
                }
            }
            $_code = $_code . '_'. $_code;
            $fshipping_price = Mage::helper('core')->currency($shipping_price, true, false);
            if ($shipping_method == $_code) {
                $checked = 'checked';
                $display = 'display: block';
            } else {
                $checked = '';
                $display = 'display: none';
            }
            $html .= '<li '.($_codewith_rate ? 'class="rate"': '').'><label for="jnqship_sm'.$_code.'" style="width: 100%"><strong>'.$shippingTitle.'</strong> <br><input name="shipping_method" id="jnqship_sm'.$_code.'" type="radio" data-latlong="'.$shipping_latlong.'" data-price="'.$shipping_price.'" '.$checked.' data-title="'.$shippingTitle.'" value="'.$_code.'"> <strong class="s-price">'.($shippingName ? $shippingName .' - '. '<span>'.$fshipping_price.'</span>' : '<span>'.$fshipping_price.'</span>').'</strong></label><div class="data-methodid" data-methodid="'.$_code.'" style="'.$display.';padding-left: 16px;">';
            if ($_codewith_rate) {
                $order_types = ($_code == 'urbaner_urbaner') ? $urbaner_helper->getOrderTypes() : Mage::helper('alldaydelivery')->getOrderTypes();
                $vehicle_types = $urbaner_helper->getVehicleTypes();
                $html .= '<div class="field-row"><label for="ordertypes_'.$_code.'">Tipo de Orden: </label><select class="select" name="urb_order_type" id="ordertypes_'.$_code.'" onchange="calculateShippingPrice(this, $(\'ordertypes_'.$_code.'\'))">';
                foreach ($order_types as $key => $type):
                    $selected = $order->getUrbanerOrderType() == $key ? 'selected' : '';
                    $html .= '<option '.$selected.' value="'.$key.'">'.$type['name'].'</option>';
                endforeach;
                $html .= '</select></div>
                    <div class="field-row">
                    <label for="vehicles_'.$_code.'">Vehículo: </label>
                    <select class="select" name="urb_order_vehicle" id="vehicles_'.$_code.'" onchange="calculateShippingPrice($(\'ordertypes_'.$_code.'\'), this)">';
                    foreach ($vehicle_types as $key => $type):
                        $selected = $order->getUrbanerVehicle() == $key ? 'selected' : '';
                        $html .= '<option '.$selected.' value="'.$key.'">'.$type.'</option>';
                    endforeach;
                $html .= '</select></div>';
            }
            $html .= $store_list;
            $html .= '</div></li>';
        }
        $html .= '</ul></div>';
        $html .= '<div class="f-right"><button class="scalable save" onClick="changeShippingData();"><span>Guardar</span></button></div>';
        $result['error'] = false;
        $result['html'] = $html;

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    // Get lat long from google
    protected function getLatLong($address){
        $address = urlencode($address);

        $json = file_get_contents("https://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&region=Peru&key=".Mage::helper('storelocator')->getConfig('gkey'));
        $json = json_decode($json);
        if ($json && $json->{'results'}) {
            $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
            $long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
            $latlong = $lat.','.$long;
            return $latlong;
        }
        return false;
    }

    protected function getShippingPrice($shipping_method_code, $order_store_id, $dest_country_id, $dest_region_id, $dest_postcode, $order_weight)
    {
        $resource = Mage::getSingleton('core/resource');
        $write = $resource->getConnection('core_write');
        
        $bind = array(
            ':website_id' => (int) $order_store_id,
            ':country_id' => $dest_country_id,
            ':region_id' => (int) $dest_region_id,
            ':postcode' => $dest_postcode
        );
        $select = $write->select()
            ->from($shipping_method_code . "_rate")
            ->where('website_id = :website_id')
            ->order(array('dest_country_id DESC', 'dest_region_id DESC', 'dest_zip DESC','weight_base DESC'))
            ->limit(1);

        // Render destination condition
        $orWhere = '(' . implode(') OR (', array(
                "dest_country_id = :country_id AND dest_region_id = :region_id AND dest_zip = :postcode",
                "dest_country_id = :country_id AND dest_region_id = :region_id AND dest_zip = ''",

                // Handle asterix in dest_zip field
                "dest_country_id = :country_id AND dest_region_id = :region_id AND dest_zip = '*'",
                "dest_country_id = :country_id AND dest_region_id = 0 AND dest_zip = '*'",
                "dest_country_id = '0' AND dest_region_id = :region_id AND dest_zip = '*'",
                "dest_country_id = '0' AND dest_region_id = 0 AND dest_zip = '*'",

                "dest_country_id = :country_id AND dest_region_id = 0 AND dest_zip = ''",
                "dest_country_id = :country_id AND dest_region_id = 0 AND dest_zip = :postcode",
                "dest_country_id = :country_id AND dest_region_id = 0 AND dest_zip = '*'",
            )) . ')';
        $select->where($orWhere);
        $weight = ($order_weight == 0) ? 0.01 : $order_weight;
        $bind[':weight_base'] = $weight;

        if ($shipping_method_code == 'motoboy') {
            $volume = $request->getVolume(); // get value Volume
            $bind[':volume'] = $volume;
            $select->where('volume >= :volume');
        }

        if ($shipping_method_code == 'motoboy') {
            $select->where('weight_base >= :weight_base');
        } else {
            $select->where('weight_base <= :weight_base');
        }

        $result = $write->fetchRow($select, $bind);
        //Mage::log($select->__toString());
        // Normalize destination zip code
        if ($result && $result['dest_zip'] == '*') {
            $result['dest_zip'] = '';
        }
        if (!isset($result['price'])) {
            $result['price'] = 0;
        }
        if (!isset($result['weight_base'])) {
            $result['weight_base'] = 0;
        }
        return $result;
    }

    protected function _isAllowed()
    {
        return true;
    }
}