<?php
$installer = $this;

$installer->startSetup();

// Required tables
$statusTable = $installer->getTable('sales/order_status');
$statusStateTable = $installer->getTable('sales/order_status_state');
$statusStateLabelTable = $installer->getTable('sales/order_status_label');

$store_id = Mage::app()->getStore()->getStoreId();
 
// Insert statuses
$installer->getConnection()->insertArray(
    $statusTable,
    array(
        'status',
        'label'
    ),
    array(
        array('status' => 'approved_payment', 'label' => 'Approved Payment'),
        array('status' => 'rejected_payment', 'label' => 'Rejected Payment'),
        array('status' => 'chargeback_payment', 'label' => 'Charge Back Payment'),
        array('status' => 'refunded_payment', 'label' => 'Refunded Payment'),
    )
);

// Insert states and mapping of statuses to states
$installer->getConnection()->insertArray(
    $statusStateLabelTable,
    array(
        'status',
        'store_id',
        'label'
    ),
    array(
        array(
            'status' => 'approved_payment',
            'store_id' => $store_id,
            'label' => 'Approved Payment',
        ),
        array(
            'status' => 'rejected_payment',
            'store_id' => $store_id,
            'label' => 'Rejected Payment',
        ),
        array(
            'status' => 'chargeback_payment',
            'store_id' => $store_id,
            'label' => 'Charge Back Payment',
        ),
        array(
            'status' => 'refunded_payment',
            'store_id' => $store_id,
            'label' => 'Refunded Payment',
        )
    )
);

// Insert states and mapping of statuses to states
$installer->getConnection()->insertArray(
    $statusStateTable,
    array(
        'status',
        'state',
        'is_default'
    ),
    array(
        array(
            'status' => 'approved_payment',
            'state' => 'approved_payment',
            'is_default' => 1
        ),
        array(
            'status' => 'rejected_payment',
            'state' => 'rejected_payment',
            'is_default' => 1
        ),
        array(
            'status' => 'chargeback_payment',
            'state' => 'chargeback_payment',
            'is_default' => 1
        ),
        array(
            'status' => 'refunded_payment',
            'state' => 'refunded_payment',
            'is_default' => 1
        )
    )
);

$installer->endSetup();