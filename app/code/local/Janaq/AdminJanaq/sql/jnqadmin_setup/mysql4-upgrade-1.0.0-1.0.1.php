<?php

$installer = $this;
/** @var $installer Mage_Core_Model_Resource_Setup */
$installer->startSetup();

$installer->run("

DROP TABLE IF EXISTS {$this->getTable('citylab')};
CREATE TABLE {$this->getTable('citylab')} (
    `citylab_id` INT unsigned NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    `email` VARCHAR(255) NOT NULL,
    `birthday` DATE,
    `phone` VARCHAR(9) NOT NULL,
    `dni` CHAR(8) NOT NULL,
    `share_post` CHAR(1) NOT NULL,
    `fb_id` VARCHAR(255) NOT NULL,
    `registered_date` DATETIME NOT NULL,
    PRIMARY KEY (citylab_id)
)  ENGINE=INNODB DEFAULT CHARSET=utf8;
");

$installer->endSetup();