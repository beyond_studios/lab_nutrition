<?php

class Janaq_AdminJanaq_Model_Product_Image extends Mage_Catalog_Model_Product_Image
{
    public function getUrl()
    {
        $baseDir = Mage::getBaseDir('media');
        $file = ('gif' == strtolower(pathinfo($this->_baseFile, PATHINFO_EXTENSION))) ? $this->_baseFile : $this->_newFile;
        $path = str_replace($baseDir . DS, "", $file);
        return Mage::getBaseUrl('media') . str_replace(DS, '/', $path);
    }
}
