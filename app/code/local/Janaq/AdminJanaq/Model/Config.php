<?php
class Janaq_AdminJanaq_Model_Config extends Mage_Catalog_Model_Config
{
    public function getAttributeUsedForSortByArray()
    {
        $options = array(
            'position'  => Mage::helper('catalog')->__('Position')
        );
        foreach ($this->getAttributesUsedForSortBy() as $attribute) {
            /* @var $attribute Mage_Eav_Model_Entity_Attribute_Abstract */
            $options[$attribute->getAttributeCode()] = $attribute->getStoreLabel();
        }
        if (isset($options['manufacturer'])) {
            $options = array('manufacturer' => Mage::helper('catalog')->__('Relevance')) + $options;
        }
        //$options['rating_summary'] => Mage::helper('catalog')->__('Rating');

        return $options;
    }
}
