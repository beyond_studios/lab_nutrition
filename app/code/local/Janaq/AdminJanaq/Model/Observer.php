<?php
/**
* 
*/
class Janaq_AdminJanaq_Model_Observer extends Mage_Core_Model_Observer
{
    public function addProductCart(Varien_Event_Observer $observer){
        $helper = Mage::helper('jnqadmin');
        if ($helper->isCustomPromosEnable()) {
            $skus = $helper->getSkus();
            $product = $observer->getEvent()->getQuoteItem();
            //$qty = $product->getQtyToAdd();

            if ($product->getId() && $skus) {
                $subtotal = Mage::getSingleton('checkout/session')->getQuote()->getSubtotal();
                $message = $helper->getMessageError();
                foreach ($skus as $value) {
                    $sku = explode ('/', $value);
                    if (count($sku) > 1) {
                        if ($subtotal && $subtotal < $sku[1] && in_array($product->getSku(), $sku)) {
                            $text = Mage::helper('catalog')->__($message, $sku[1], $sku[2]);
                            Mage::throwException($text);
                            break;
                        }
                    } else{
                        Mage::log("Add: El formato $value no es el correcto");
                        break;
                    }
                }
            }
        }
    }

    public function saveAfter($observer)
    {
        $helper = Mage::helper('jnqadmin');
        if ($helper->isCustomPromosEnable()) {
            $skus = $helper->getSkus();
            if (!$skus) {
                return;
            }
            $quote = $observer->getEvent()->getQuote();
            $subtotal = $quote->getSubtotal();
            $items = $quote->getAllVisibleItems();
            $cart_items = array();
            $message = $helper->getMessageError();
            $position = false;
            foreach ($items as $key => $item) {
                $position = array_search($item['sku'], array_column($cart_items,"sku"));
                if (is_bool($position)) {
                    array_push($cart_items, [
                        "sku" => $item->getSku(),
                        "name" => $item->getName(),
                        "qty" => $item->getQty()
                    ]);
                }
                else{
                    $cart_items[$position]['qty'] = $cart_items[$position]['qty'] + $item->getQty();
                }
            }
            //$url = Mage::getUrl('checkout/cart', array('_secure' => true));

            foreach ($skus as $value) {
                $sku = explode ('/', $value);
                if (count($sku) > 1) {
                    foreach ($cart_items as $_item) {
                        if (($subtotal < $sku[1] && $sku[0] == $_item['sku']) || 
                            ($sku[0] == $_item['sku'] && $_item['qty'] > $sku[2])) {
                            //Mage::throwException("El monto del carrito no es suficiente.");
                            $text = Mage::helper('catalog')->__($message, $sku[1], $sku[2]);
                            $error = Mage::helper('checkout')->__("\"" . $_item['name'] . "\" " .$text);
                            $quote->setHasError(true);
                            $quote->addErrorInfo(
                                'error',
                                'checkout',
                                null,
                                Mage::helper('checkout')->__("\"" . $_item['name'] . "\" " .$text),
                                null
                            );
                            //Mage::getSingleton('checkout/session')->addError($error);
                            /*if (Mage::app()->getRequest()->getControllerName() != "cart") {
                                Mage::app()->getResponse()->setRedirect($url);
                            }*/
                            break 2;
                        }
                    }
                } else{
                    Mage::log("Save: El formato $value no es el correcto");
                    break;
                }

            }
        }
    }

    /**
     * Redirect from disabled product to product last category
     * Obseve event - controller_action_predispatch_catalog_product_view
     * 
     * @param Varien_Event_Observer $observer
     */
    public function catalogProductViewPredispatch(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('jnqadmin')->redirectProductEnabled()) {
            return;
        }
        Varien_Profiler::start(__METHOD__);
        
        $product_id = intval(Mage::app()->getRequest()->getParam('id'));
        $_product   = Mage::getSingleton('catalog/product')->load($product_id);
        
        if($_product->getStatus() == Mage_Catalog_Model_Product_Status::STATUS_DISABLED) {

            $category_ids = $_product->getCategoryIds();
            if ($category_ids) {
                $last_id      = end($category_ids);
                $redirect_url = Mage::getSingleton("catalog/category")->load($last_id)->getUrl();

                Mage::app()->getResponse()->setRedirect($redirect_url, 302);
            }
        }
        
        Varien_Profiler::stop(__METHOD__);
    }

    public function paymentMethodIsActive(Varien_Event_Observer $observer) {
        $event           = $observer->getEvent();
        $method          = $event->getMethodInstance();
        $_code           = $method->getCode();
        $result          = $event->getResult();
        $s_address = Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress();
        $s_method = $s_address->getShippingMethod();
        $s_region = $s_address->getRegion();
        //$currencyCode    = Mage::app()->getStore()->getCurrentCurrencyCode();
        //Mage::log($s_method);

        $params = Mage::app()->getRequest()->getParams();
        $upon_delivery = false;
        if (isset($params['urb_type']) && $params['urb_type'] == '4') {
            $upon_delivery = true;
        }

        if($s_method == 'motoboy_motoboy') {
            if($_code == 'payupagoefectivo') {
                $result->isAvailable = false;
            }
        }
        else if ($s_method == 'ondeliveryship_ondeliveryship') {
            if($_code == 'posvisa' || $_code == 'posmastercard' || $_code == 'ondelivery') {
                $result->isAvailable = $result->isDeniedInConfig ? false : true;
            } else {
                $result->isAvailable = false;
            }
            if ($_code == 'ondelivery' && $s_region == "Lima") {
                $result->isAvailable = $result->isDeniedInConfig ? false : true;
            }
        } else {
            if($_code == 'posvisa' || $_code == 'posmastercard') {
                $result->isAvailable = false;
            }
            if($s_method != 'servientrega_servientrega') {
                if($_code == 'ondelivery') {
                    $result->isAvailable = false;
                }
            }
            /*if ($s_method == 'urbaner_urbaner' && !$upon_delivery) {
                if($_code == 'posvisa' || $_code == 'posmastercard') {
                    $result->isAvailable = false;
                }
            }*/
        }
        
        return  $result->isAvailable;
    }
}