<?php 

class Janaq_AdminJanaq_Model_EntityAttributeSourceTable extends Mage_Eav_Model_Entity_Attribute_Source_Table
{
	public function addValueSortToCollection($collection, $dir = Varien_Db_Select::SQL_ASC)
    {
        $valueTable1    = $this->getAttribute()->getAttributeCode() . '_t1';
        $valueTable2    = $this->getAttribute()->getAttributeCode() . '_t2';
        $collection->getSelect()
            ->joinLeft(
                array($valueTable1 => $this->getAttribute()->getBackend()->getTable()),
                "e.entity_id={$valueTable1}.entity_id"
                . " AND {$valueTable1}.attribute_id='{$this->getAttribute()->getId()}'"
                . " AND {$valueTable1}.store_id=0",
                array())
            ->joinLeft(
                array($valueTable2 => $this->getAttribute()->getBackend()->getTable()),
                "e.entity_id={$valueTable2}.entity_id"
                . " AND {$valueTable2}.attribute_id='{$this->getAttribute()->getId()}'"
                . " AND {$valueTable2}.store_id='{$collection->getStoreId()}'",
                array()
            );
        $valueExpr = $collection->getSelect()->getAdapter()
            ->getCheckSql("{$valueTable2}.value_id > 0", "{$valueTable2}.value", "{$valueTable1}.value");

        Mage::getResourceModel('eav/entity_attribute_option')
            ->addOptionValueToCollection($collection, $this->getAttribute(), $valueExpr);

        if ($this->getAttribute()->getAttributeCode() == 'manufacturer') {
        	$valueTableOption = $this->getAttribute()->getAttributeCode(). '_option';
        	$valueTable1Aux = $this->getAttribute()->getAttributeCode() . '_option_value_t1';
        	$collection->getSelect()
            ->joinLeft(array($valueTableOption => 'eav_attribute_option'),
                "{$valueTable1Aux}.option_id={$valueTableOption}.option_id"
                ,array('order_attribute' => $valueTableOption.'.sort_order'));

            $sortBySeason = 'ISNULL(order_attribute),order_attribute '.$dir;
            $collection->getSelect()->order(new Zend_Db_Expr($sortBySeason));
            $collection->getSelect()
            ->order("cat_index_position {$dir}");
        } else {
        	$collection->getSelect()
            ->order("{$this->getAttribute()->getAttributeCode()} {$dir}");
        }
        return $this;
    }
}