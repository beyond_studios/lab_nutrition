<?php
/**
 * @category    Janaq
 * @package     Janaq_PriceCount
 * @version     0.1.4
 * @author      Janaq Team <developers@contus.in>
 * @copyright   Copyright (c) 2019 Janaq. (https://janaq.com)
 *
 */
class Janaq_AdminJanaq_Model_System_Config_Source_Showin
{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => 'showall', 'label'=>Mage::helper('adminhtml')->__('Show in catalog/products pages')),
            array('value' => 'listpage', 'label'=>Mage::helper('adminhtml')->__('Show in catalog page')),
            array('value' => 'viewpage', 'label'=>Mage::helper('adminhtml')->__('Show in product page')),
            array('value' => 'hideall', 'label'=>Mage::helper('adminhtml')->__('Hide in all pages')),
           
        );
    }

}
