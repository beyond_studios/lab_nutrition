<?php
/**
* 
*/
class Janaq_AdminJanaq_Block_Rewrite_Cart_Crosssell extends Mage_Checkout_Block_Cart_Crosssell
{
	public function __construct()
    {
        $this->_maxItemCount = Mage::getStoreConfig('jnqadmin_config/crosssell/items', Mage::app()->getStore());
    }
}