<?php
/**
 * 
 */
class Janaq_AdminJanaq_Block_Rewrite_Product_List_Toolbar extends Mage_Catalog_Block_Product_List_Toolbar
{

	/**
     * List of available custom sort types
     *
     * @var array
     */
    public function getAvailableOrders()
    {
    	$this->_availableOrder['mostviewed'] = $this->__('Most viewed items');
    	//$this->_availableOrder['rating_summary'] = $this->__('Rating');

        //$this->_availableOrder = array('manufacturer' => $this->_availableOrder['manufacturer']) + $this->_availableOrder;

        return $this->_availableOrder;
    }

    /**
     * Set collection to pager
     *
     * @param Varien_Data_Collection $collection
     * @return Mage_Catalog_Block_Product_List_Toolbar
     */
    public function setCollection($collection)
    {
        $this->_collection = $collection;

        $this->_collection->setCurPage($this->getCurrentPage());

        // we need to set pagination only if passed value integer and more that 0
        $limit = (int)$this->getLimit();
        if ($limit) {
            $this->_collection->setPageSize($limit);
        }

        //Add views to sort
        $this->_collection->getSelect()->
        joinLeft('report_event AS _table_views',
        ' _table_views.object_id = e.entity_id',
        'COUNT(_table_views.event_id) AS views')->
        group('e.entity_id');

        //Add stock to sort
        $stockId = Mage_CatalogInventory_Model_Stock::DEFAULT_STOCK_ID;
        $websiteId = Mage::app()->getStore($this->_collection->getStoreId())->getWebsiteId();

        $this->_collection->getSelect()->joinLeft(
            array('_inv' => $this->_collection->getResource()->getTable('cataloginventory/stock_status')),
            "_inv.product_id = e.entity_id and _inv.website_id=$websiteId and _inv.stock_id=$stockId",
            array('stock_status')
        );
        $this->_collection->addExpressionAttributeToSelect('in_stock', 'IFNULL(_inv.stock_status,0)', array());
        $this->_collection->getSelect()->order('in_stock DESC');

        if ($this->getCurrentOrder()) {
            $this->_collection->setOrder($this->getCurrentOrder(), $this->getCurrentDirection());
            if ($this->getCurrentOrder() == "mostviewed") {
                $this->_collection->getSelect()->order('views desc');
            }
        }
        //Mage::log((string)$this->_collection->getSelect());
        return $this;
    }

}