<?php
/**
 * Apptha
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.apptha.com/LICENSE.txt
 *
 * ==============================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * ==============================================================
 * This package designed for Magento COMMUNITY edition
 * Apptha does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * Apptha does not provide extension support in case of
 * incorrect edition usage.
 * ==============================================================
 *
 * @category    Janaq
 * @package     Admin_Janaq
 * @version     0.0.1
 * @author      Janaq Team <janaq.com>
 * @copyright   Copyright (c) 2017 Janaq. (https://janaq.com)
 *
 */
class Janaq_AdminJanaq_Block_Rewrite_Product_View extends Mage_Catalog_Block_Product_View  
{
    public function getPriceCountDown(){
    	if(Mage::helper('jnqadmin')->isTimerEnabled() && $this->getProduct()->getShowTimer()){
    		$currentDate = Mage::getModel('core/date')->date('Y-m-d');
    		$todate =  $this->getProduct()->getSpecialToDate();
    		$fromdate =  $this->getProduct()->getSpecialFromDate();
    		if($this->getProduct()->getSpecialPrice() != 0 || $this->getProduct()->getSpecialPrice()) {
    			if($this->getProduct()->getSpecialToDate() != null) {
    				if(strtotime($todate) >= strtotime($currentDate) && strtotime($fromdate) <= strtotime($currentDate)){
    					return true;
    				}	
    			}
    		}
    	}
    	return false;
    }

    /**
     * Retrieve list of names of objectives
     *
     * @return string
     */

    public function getObjetivos(){
        $objetivos = '';
        $categories = $this->getProduct()->getCategoryIds();
        $children = Mage::getModel('catalog/category')->getCategories(8, 0, true, true, true);
        $flatEnable = Mage::helper('catalog/category_flat')->isEnabled();

        foreach ($children as $category) {
            if (in_array($category->getId(), $categories) && $category->getLevel() == 3) {
                if ($flatEnable) {
                    $category = Mage::getModel("catalog/category")->load($category->getId());
                }
                $image = Mage::getModel ( 'core/design_package' )->getSkinUrl("images/iconos/".$category->getUrlKey().".svg");  
                $objetivos .= '<li>';
                $objetivos .= '<div class="svg-container">'.Mage::helper('jnqadmin')->getSVGPath($image).'</div>';
                $objetivos .= '<div class="tooltip right"><div class="tooltip-arrow hidden-xs hidden-sm"></div><div class="tooltip-inner">'.$category->getName().'</div></div>';
                $objetivos .= '</li>';
            }
        }
        if ($objetivos) {
            $objetivos = '<ul>'.$objetivos.'</ul>';
        }
        return $objetivos;
    }
  
}