<?php
class Janaq_AdminJanaq_Block_Rewrite_Product_List extends Mage_Catalog_Block_Product_List
{
    protected $_categories;

    public function __construct() {
        parent::__construct();

        if (Mage::registry('current_category')) {
            $this->_categories = Mage::getModel('catalog/category')->getCategories(8, 0, true, true, true);
        }
    }

    protected function _getProductCollectionOLD()
    {
        if (is_null($this->_productCollection)) {
            $layer = $this->getLayer();
            // @var $layer Mage_Catalog_Model_Layer
            if ($this->getShowRootCategory()) {
                $this->setCategoryId(Mage::app()->getStore()->getRootCategoryId());
            }

            // if this is a product view page
            if (Mage::registry('product')) {
                // get collection of categories this product is associated with
                $categories = Mage::registry('product')->getCategoryCollection()
                    ->setPage(1, 1)
                    ->load();
                // if the product is associated with any category
                if ($categories->count()) {
                    // show products from this category
                    $this->setCategoryId(current($categories->getIterator()));
                }
            }

            $origCategory = null;
            if ($this->getCategoryId()) {
                $category = Mage::getModel('catalog/category')->load($this->getCategoryId());
                if ($category->getId()) {
                    $origCategory = $layer->getCurrentCategory();
                    $layer->setCurrentCategory($category);
                    $this->addModelTags($category);
                }
            }
            $this->_productCollection = $layer->getProductCollection();

            /*$this->_productCollection->joinField('rating_summary', 
                'review_entity_summary', 'rating_summary', 'entity_pk_value=entity_id', 
                array('entity_type'=>1, 'store_id'=> Mage::app()->getStore()->getId()), 'left');*/
            $this->prepareSortableFieldsByCategory($layer->getCurrentCategory());

            if ($origCategory) {
                $layer->setCurrentCategory($origCategory);
            }
        }
        return $this->_productCollection;
    }

    public function getPriceCountDown($_product) {
        if(Mage::helper('jnqadmin')->isTimerEnabled() && $_product->getShowTimer()) {
            $currentDate = Mage::getModel('core/date')->date('Y-m-d');
            $todate =  $_product->getSpecialToDate();
            $fromdate =  $_product->getSpecialFromDate();
            if($_product->getSpecialPrice() != 0 || $_product->getSpecialPrice()) {
                if($_product->getSpecialToDate() != null) {
                    if(strtotime($todate) >= strtotime($currentDate) && strtotime($fromdate) <= strtotime($currentDate))
                        return true;
                }
            }
        }
        return false;
    }

    public function getPriceHtml($product, $displayMinimalPrice = false, $idSuffix = '')
    {
        $html = parent::getPriceHtml($product, $displayMinimalPrice, $idSuffix);
        if(Mage::getStoreConfig('amconf/list/enable_list') == 1 && $product->isConfigurable()){
            $html .= Mage::helper('amconf')->getHtmlBlock($product, '');
        }
        
        return $html;
    }

    /**
     * Retrieve list of names of objectives
     *
     * @return string
     */

    public function getObjetivos($_product) {
        $objetivos = '';
        $categories = $_product->getCategoryIds();
        $children = $this->_categories;

        if($children)
        foreach ($children as $category) {
            if (in_array($category->getId(), $categories) && $category->getLevel() == 3) {
                $objetivos .=  $category->getName().', ';
            }
        }
        $objetivos = trim($objetivos,', ');
        return $objetivos;
    }
}
