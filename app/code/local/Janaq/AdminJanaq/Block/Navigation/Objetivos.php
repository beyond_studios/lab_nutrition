<?php
/**
 * 
 */
class Janaq_AdminJanaq_Block_Navigation_Objetivos extends Mage_Catalog_Block_Navigation
{
	protected function _construct()
    {
        parent::_construct();
    }

    public function getObjetivos()
    {
    	$category_parent = 8;
		$_category = Mage::getModel('catalog/category')->load($category_parent);
		$_categories = $_category->getCollection()
	        ->addAttributeToSelect("*")
	        ->addIdFilter($_category->getChildren());
	    return $_categories;
    }
}
?>