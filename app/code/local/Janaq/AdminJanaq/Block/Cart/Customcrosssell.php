<?php
/**
 * @author janaq
 */
class Janaq_AdminJanaq_Block_Cart_Customcrosssell extends Mage_Catalog_Block_Product_Abstract
{
	protected $_enable;
	protected $_skus;
	protected $_title;

	public function __construct()
	{
		parent::_construct();
		$this->_enable = (bool)Mage::getStoreConfig('jnqadmin_config/crosssell/enable', Mage::app()->getStore());

		if (!$this->_enable) {
			return;
		}
        // Block caching setup
        $this->addData(array(
            'cache_lifetime' => 604800, // (seconds) data lifetime in the cache
            'cache_tags' => array(
                Mage_Core_Model_Store::CACHE_TAG,
                Mage_Cms_Model_Block::CACHE_TAG,
            ),
            'cache_key' => 'JNQ_CUSTOM_CROSSSELL',
        ));
        $this->_skus = Mage::getStoreConfig('jnqadmin_config/crosssell/skus', Mage::app()->getStore());
        $this->_title = Mage::getStoreConfig('jnqadmin_config/crosssell/title', Mage::app()->getStore());
		$this->setTemplate('checkout/cart/customcrosssell.phtml');
	}

	public function getItems()
	{
		$items = $this->getData('items');
        if (is_null($items)) {
            $items = array();
            $items_array = $this->getCartCrosssellSkus();
            if ($items_array) {
            	$items = Mage::getModel('catalog/category')->getProductCollection()
            			->addAttributeToSelect('*')
            			->addAttributeToFilter('status', 1) // enabled
 						//->addAttributeToFilter('visibility', 4)
            			->addFieldToFilter('sku', array('in'=> $items_array));
                Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($items);
            }

            $this->setData('items', $items);
        }
        return $items;
	}

	public function getItemCount()
    {
        return count($this->getItems());
    }

    public function getTitle()
    {
        return $this->_title;
    }

    protected function getCartCrosssellSkus()
    {
    	$skus = $this->getData('_cart_customcrosssell_skus');
        if (is_null($skus)) {
            $skus = explode(",", $this->_skus);
            if (count($skus)) {
            	$this->setData('_cart_customcrosssell_skus', $skus);	
            }
        }
        return $skus;
    }
}