<?php
/**
 * @author janaq
 */
class Janaq_AdminJanaq_Block_Productsale extends Mage_Catalog_Block_Product_Abstract
{
	protected $banner_url = '';
	protected $helper;

	public function __construct()
	{
		parent::__construct();

		$this->helper = Mage::helper('jnqadmin');
		if (!$this->getProduct()) {
			return;
		}
		// Block caching setup
		$this->addData(array(
			'cache_lifetime'=> 604800, // (seconds) data lifetime in the cache
			'cache_tags' => array(
				Mage_Core_Model_Store::CACHE_TAG,
				Mage_Cms_Model_Block::CACHE_TAG,
			),
			'cache_key' => 'JNQ_PRODUCT_SALE',
		));
	}

	public function getProduct()
	{
		$enabled = (bool)$this->helper->isEnabled();
		if ($enabled) {
			$sku = Mage::getStoreConfig("jnqadmin_config/sale_banner/sku");
        	$_product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
        	if ($_product) {
        		$this->banner_url = $_product->getProductUrl();
        		return $_product;
        	}
		}
		return false;
	}

	public function getSaleBannerUrl()
	{
		$url = $this->helper->getSaleBannerUrl();
    	return $url ? $url : $this->banner_url;
	}

	protected function getSaleMedia()
	{
		$uploadDir = Mage::getBaseDir('media') . DIRECTORY_SEPARATOR . 'salebanner' . DIRECTORY_SEPARATOR;
		return $uploadDir;
	}

	public function saleBannerPopupEnable()
	{
		return $this->helper->saleBannerPopupEnable();
	}

	public function saleBannerBannerImage()
	{
        $image_url = $this->helper->saleBannerBannerImage();
        if ($image_url && file_exists($this->getSaleMedia() . $image_url)) {
            return Mage::getBaseUrl('media') .'salebanner/'. $image_url;
        }
        else {
        	return Mage::helper('catalog/image')->init($this->getProduct(), 'small_image')->resize(300,300);
        }
	}

	public function saleBannerPopupImage()
	{
        $image_url = $this->helper->saleBannerPopupImage();
        if ($image_url && file_exists($this->getSaleMedia() . $image_url)) {
            return Mage::getBaseUrl('media') .'salebanner/'. $image_url;
        }
        else {
        	return Mage::helper('catalog/image')->init($this->getProduct(), 'small_image')->resize(400,400);
        }
	}
}