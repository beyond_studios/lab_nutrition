<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 15/12/2016
 * Time: 12:12 PM
 */

class Janaq_NavaSoft_Model_Observer extends Mage_Payment_Model_Method_Abstract
{

    public function saveOrder($observer){

        $helper = Mage::Helper('janaq_navasoft');

        if ($helper->getStatus()) {
          $date = Mage::getModel('core/date')->date('Y-m-d H:i:s');

          $model = Mage::getSingleton('janaq_navasoft/integrator');
          $increment_id = $observer['order']->getIncrementId();
          $modelUpdate = Mage::getModel('janaq_navasoft/integrator');
          $modelUpdate->load($increment_id,'order_id');
          if ($increment_id && !$modelUpdate->getId()) {
              $model->setOrderId($increment_id);
              $model->setTypeTask('Order');
              $model->setStatus('pendiente');
              $model->setCreatedAt($date);
              $model->setUpdatedAt($date);
          }else{
              $model->setId($modelUpdate->getId());
              //$model->setStatus('pendiente');
              $model->setUpdateAt($date);
          }
          try {
              $model->save();
          } catch (Exception $e) {
              Mage::log('Error al guardar registro de NAVASOFT (Tareas): '. $e->getMessage());
          }

        }
        return $this;

    }

    public function addProductCart(Varien_Event_Observer $observer)
    {
      $helper = Mage::Helper('janaq_navasoft');
      if ($helper->getStatus() && $helper->getStatusCart()) {
          $quoteItem = $observer->getEvent()->getQuoteItem();
          $product = $observer->getEvent()->getProduct();
          $sku = $product->getSku();
          $items = explode('-', $sku);
          $store_default = $helper->getStoreDefault();
          $model = Mage::getModel('janaq_navasoft/generate');
          $quote = Mage::getSingleton('checkout/session')->getQuote();
          $cartItems = $quote->getAllVisibleItems();
          
          if ($product->getTypeId() == 'bundle') {
            $bundled_items = array();
            $optionCollection = $product->getTypeInstance()->getOptionsCollection();
            $selectionCollection = $product->getTypeInstance()->getSelectionsCollection($product->getTypeInstance()->getOptionsIds());
            $options = $optionCollection->appendSelections($selectionCollection);
            foreach($options as $option) {
                $_selections = $option->getSelections();
                foreach($_selections as $selection) {
                    
                    $productSku = $selection->getSku();
                    $qty= $selection->getQty();
                    $qty = ($qty == 0 ) ? 1 : $qty ;
                    $params  = array('items' => $productSku ,'almacen_id' => $store_default );
                    $_product_navasoft = $model->getStock($params);
                    if ($_product_navasoft && $_product_navasoft['status']) {
                       $existencia = $_product_navasoft['stock'];
                    }else{
                      $existencia = 0;
                    }
                    if((int)$qty > (int)$existencia){
                      $quoteItem->getQuote()->removeItem($selection->getId());
                      Mage::throwException('El producto "'.$selection->getName().'" no esta disponible');
                    }

                }
            }
        }else{

          $productSku = $sku;
          $qty= $product->getQty();
          $qty = ($qty == 0 ) ? 1 : $qty ;
          $params  = array('items' => $productSku ,'almacen_id' => $store_default );
          $_product_navasoft = $model->getStock($params);
          if ($_product_navasoft && $_product_navasoft['status']) {
             $existencia = $_product_navasoft['stock'];
          }else{
            $existencia = 0;
          }
          if((int)$qty > (int)$existencia){
            $quoteItem->getQuote()->removeItem($quoteItem->getId());
            Mage::throwException('El producto "'.$quoteItem->getName().'" no esta disponible.');
          }
        }
      }
    }

    public function updateProductCart(Varien_Event_Observer $observer){
      $helper = Mage::Helper('janaq_navasoft');

      if ($helper->getStatus() && $helper->getStatusCart()) {
        $data = $observer->getEvent()->getInfo();
        //Mage::log($data);
        foreach ($data as $itemId => $itemInfo) {
            //$item = $this->getQuote()->getItemById($itemId);
            $item = Mage::getSingleton('checkout/session')->getQuote()->getItemById($itemId);
            if (!$item) {
                continue;
            }
            $store_default = $helper->getStoreDefault();
            $model = Mage::getModel('janaq_navasoft/generate');
            if ($item->getProductType() == 'bundle') {
                $items = Mage::getSingleton('checkout/cart')->getQuote()->getAllItems();
                foreach ($items as $value) {
                  if ($value->getParentItemId() == $itemId) {
                      $params  = array('items' => $value->getSku() ,'almacen_id' => $store_default );
                      $_product_navasoft = $model->getStock($params);
                      if ($_product_navasoft && $_product_navasoft['status']) {
                         $existencia = $_product_navasoft['stock'];
                      }else{
                        $existencia = 0;
                      }
                      if ((int)$existencia < (int)$itemInfo['qty'] || (int)$existencia == 0) {
                        Mage::throwException('El producto "'.$value->getName().'" se encuentra fuera de stock');
                      }
                  }
                }
            }else{
                $params  = array('items' => $item->getSku() ,'almacen_id' => $store_default );
                $_product_navasoft = $model->getStock($params);
                if ($_product_navasoft && $_product_navasoft['status']) {
                   $existencia = $_product_navasoft['stock'];
                }else{
                  $existencia = 0;
                }
                if ((int)$existencia < (int)$itemInfo['qty'] || (int)$existencia == 0) {
                  Mage::throwException('El producto "'.$item->getName().'" se encuentra fuera de stock.');
                }
            }
            
        }
      }

    }

    public function saveProduct(Varien_Event_Observer $observer){
        $helper = Mage::Helper('janaq_navasoft');
        //Mage::log('Save Product observer');
        $product = $observer->getProduct();
        if ($helper->getStatus()  && ($product->getTypeId() == 'simple' || $product->getTypeId() == 'bundle' )) {
          //Mage::log($product->getStatus());
          if ($product->getSku() == '' || $product->getStatus() == '2') {
            return true;
          }
          $navasoft = Mage::getModel('janaq_navasoft/generate');
          $navasoft_status = true;
          $store_default = $helper->getStoreDefault();
          /*if ($product->getTypeId() == 'bundle' && $helper->getValidateProductBundleAdmin() ) {

              $navasoft_status = $navasoft->getValidateProductBundlenavasoft($product->getSku());
          }elseif ($product->getTypeId() == 'simple' &&  $helper->getValidateProductAdmin() ){
              $navasoft_status = $navasoft->getValidateProductnavasoft($product->getSku()); 
          }*/
          
          if ($product->getTypeId() == 'simple' && $helper->getValidateProductAdmin()) {
            $params  = array('items' => $product->getSku() ,'almacen_id' => $store_default );

            $stock = $navasoft->getStock($params,true);
            $navasoft_status = $stock['status'];
          }
          if (!$navasoft_status) {
            try {
                $reflectionClass = new ReflectionClass('Mage_Catalog_Model_Product');
                $reflectionProperty = $reflectionClass->getProperty('_dataSaveAllowed');
                $reflectionProperty->setAccessible(true);
                $reflectionProperty->setValue($product, false);
                Mage::getSingleton('adminhtml/session')->addError('El sku "'.$product->getSku().'" no existe en NAVASOFT.');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError('Ocurrió un error al verificar con NAVASOFT');
                
            }
          }
          
        }
    }

    
  }
