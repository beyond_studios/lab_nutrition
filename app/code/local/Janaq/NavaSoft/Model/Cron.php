<?php

class Janaq_NavaSoft_Model_Cron
{
    protected $_bank = array(
      'SCOTIABANK' => '01',
      'GNB' => '02',
      'INTERBANK' => '03',
      'BCP' => '04',
      'BANCO DE LA NACION' => '05',
      'SAGA FALABELLA' => '06',
      'BBVA CONTINENTAL' => '07',

    );
    protected $_status_mavasoft = array(
      '-1' => 'not_exist_navasoft',
      '*'  => "anulado_navasoft",
      "0" => "pendiente_navasoft",
      "1" => "facturado_navasoft",
      "2" => "aprobado_navasoft"
    );
    /* -----------------------------------------------------------------------------------------
            - Proceso de Enviar pedido a PIM Laravel
    */
    public function run()
    {
        $helper = Mage::helper('janaq_navasoft');
        $date = Mage::getModel('core/date')->date('Y-m-d H:i:s');
        $Collection = Mage::getResourceModel('janaq_navasoft/integrator_collection');
        $Collection->addFieldToFilter('status', array('in' => array('pendiente')));
        //ORDER

        if ($helper->getStatus()) {
          foreach ($Collection as  $tareas) {
            $model = Mage::getModel('janaq_navasoft/integrator');
            $model->load($tareas->getId());
            if ($tareas->getTypeTask() == 'Order') {
            	try{
                    $incrementId = $model->getOrderId();
                    $order = Mage::getModel('sales/order')->loadByIncrementId($incrementId);
                    $type_doc = "32";
                    $navasoft = Mage::getModel('janaq_navasoft/generate');
                    $entity_id  =  (int) substr($order->getIncrementId(),1);
                    $series = $helper->getSeries();
                    if ($model->getEditStatus() == 1) {
                         $file_name = $helper->getRuc()."-".$type_doc."-".$series."-".str_pad($entity_id, 7, '0', STR_PAD_LEFT);
                         $response = $navasoft->sendOrder($file_name,$model->getDataSent());
                         if ($response == 'Aceptado') {
                            $model->setStatus('completado');
                         }
                         $model->setUpdatedAt($date);
                         $model->setResponse($response);
                         $model->save();
                    }else{
                      $billing_address = $order->getBillingAddress();
                      $billing_postcode = $billing_address->getPostcode();
                      $billing_postcode = explode('-',$billing_postcode);
                      $shipping_address = $order->getShippingAddress();
                      $shipping_postcode = $shipping_address->getPostcode();
                      $shipping_postcode = explode('-',$shipping_postcode);
                      $orderItems = $order->getAllItems();
                      $almacen_id = false;
                      
                      $payemnt_method_id = '0';
                      $payemnt_method_type = '';
                      $payemnt_method_card = '';
                      $payment = $order->getPayment();
                      switch ($payment->getMethod()) {
                        case 'mercadopago_custom':
                          $payemnt_method_id = '2';
                          $type = $payment->getAdditionalInformation('payment_method');
                          $card = $payment->getAdditionalInformation('trunc_card');
                          if ($type != "") {
                            if (strpos($type,'visa') !==false ) {
                                $payemnt_method_type = '01';
                            }elseif(strpos($type,'master') !==false){
                                $payemnt_method_type = '02';
                            }
                            elseif(strpos($type,'amex') !==false){
                                $payemnt_method_type = '03';
                            }
                            elseif(strpos($type,'diners') !==false){
                                $payemnt_method_type = '04';
                            }else{
                              $payemnt_method_type = '00';
                            }
                          }
                          if ($card != "") {
                            $payemnt_method_card = $card;
                          }
                          break;
                        case 'mercadopago_customticket':
                          $payemnt_method_id = '3';
                          break;
                        case 'ondelivery':
                          $payemnt_method_id = '1';
                          break;
                        case 'posvisa':
                          $payemnt_method_id = '4';
                          break;
                        case 'posmastercard':
                          $payemnt_method_id = '4';
                          break;
                        
                        default:
                          $payemnt_method_id = '0';
                          break;
                      }
                      $items = array();
                      $_item = array('igv' => 'S','UM' => 'UND');
                      
                      $discount_product_total = 0.00;
                      $discount_product_amount = 0.00;
                      foreach ($orderItems as $item) {
                        
                        $data = $item->getData();
                        //Mage::log($data['product_type'] .' - '. $data['name'] . ' - '.$data['price'] . ' - '.$data['qty_ordered']);
                        if ($data['product_type'] == 'configurable') {
                          $row_total = $data['row_total'];
                          $_price = $data['price'];
                          foreach ($orderItems as $simple) {
                              $discount_product = "0.00";
                              if ($simple['product_type'] == 'simple' && $simple['parent_item_id'] == $data['item_id']) {
                                  $product = Mage::getModel('catalog/product')->load($simple["product_id"]);
                                  $price = $product->getPrice();
                                  if ((double)$product->getPrice() != (double)$_price ) {
                                      $discount_product = (double)$product->getPrice() - (double)$_price;
                                      $discount_product_total += $discount_product;
                                      $discount_product_amount = $discount_product ;
                                      $discount_product = ($discount_product  * 100)/(double)$product->getPrice();
                                  }
                                  
                                  $_item['item_id'] = $simple['item_id'];
                                  $_item['magento_order_id'] = $simple['order_id'];
                                  $_item['parent_item_id'] = $simple['parent_item_id'];
                                  $_item['product_id'] = $simple['product_id'];
                                  $_item['product_type'] = $simple['product_type'];
                                  $_item['name'] = $simple['name'];
                                  $_item['sku'] = $simple['sku'];
                                  //$_item['qty'] = number_format($simple['qty_ordered'],2,'.','');
                                  $_item['qty'] = '1';
                                  $_item['price'] = number_format($price,2,'.','');
                                  $_item['bonus'] = ((double)$price == 0.00 ) ? '1' : '0';
                                  $_item['discount'] = number_format( $discount_product,2,'.','');
                                  $_item['discount_amount'] = number_format( $discount_product_amount,2,'.','');
                                  $_item['row_total'] = number_format($row_total,2,'.','');
                                  $items[] = $_item;
                              }
                          }
                          
                        }elseif ($data['product_type'] == 'bundle') {
                            $data = $item->getData();
                            $product = Mage::getModel('catalog/product')->load($data["product_id"]);
                            $discount_product = "0.00";
                            //Mage::log($product->getPrice());
                            if ((double)$product->getPrice() != (double)$data['price'] ) {
                                $discount_product = (double)$product->getPrice() - (double)$data['price'];
                                $discount_product_total += $discount_product;
                                $discount_product_amount = $discount_product;
                                $discount_product = ($discount_product  * 100)/(double)$product->getPrice();
                                
                            }
                            $_item['item_id'] = $data['item_id'];
                            $_item['magento_order_id'] = $data['order_id'];
                            $_item['parent_item_id'] = $data['parent_item_id'];
                            $_item['product_id'] = $data['product_id'];
                            $_item['product_type'] = $data['product_type'];
                            $_item['name'] = 'Descuento de '.$data['name'];
                            $_item['sku'] = 'DS00';
                            $_item['qty'] = number_format($data['qty_ordered'],2,'.','');
                            $_item['bonus'] = '0';
                            $_item['discount_amount'] = '0.00';
                            
                            $_tota_price = 0.00;
                            $_simple_item= array('igv' => 'S','UM' => 'UND');
                            foreach ($orderItems as $simple) {
                              $discount_product = "0.00";
                              if ($simple['product_type'] == 'simple' && $simple['parent_item_id'] == $data['item_id']) {
                                  
                                  $_product = Mage::getModel('catalog/product')->load($simple["product_id"]);
                                  $_price = $_product->getPrice();

                                  $_simple_item['item_id'] = $simple['item_id'];
                                  $_simple_item['magento_order_id'] = $simple['order_id'];
                                  $_simple_item['parent_item_id'] = $simple['parent_item_id'];
                                  $_simple_item['product_id'] = $simple['product_id'];
                                  $_simple_item['product_type'] = $simple['product_type'];
                                  $_simple_item['name'] = $simple['name'];
                                  $_simple_item['sku'] = $simple['sku'];
                                  $_simple_item['qty'] = number_format($simple['qty_ordered'],2,'.','');
                                  $_simple_item['price'] = number_format($_price,2,'.','');
                                  $_simple_item['bonus'] = ((int)$_product->getFreeBundle() == 1 ) ? '1' : '0';
                                  $_simple_item['discount'] = $discount_product;
                                  $_simple_item['row_total'] = number_format(($_price * $simple['qty_ordered']),2,'.','');
                                  $_simple_item['discount_amount'] = $discount_product;
                                  $_tota_price += $_simple_item['row_total'];
                                  

                                  $items[] = $_simple_item;
                              }
                            }
                            $_item['price'] = number_format($data['price'] - $_tota_price ,2,'.','');
                            $_item['row_total'] = number_format((double)$_item['price'] * (double)$_item['qty'],2,'.','');
                            if ((double)$_item['price'] != 0 ) {
                               $items[] = $_item;
                            }


                        }else{
                            if ($data['product_type'] == 'simple' && $data['parent_item_id'] == NULL ) {
                              $product = Mage::getModel('catalog/product')->load($data["product_id"]);
                              if ((double)$product->getPrice() != (double)$data["price"]) {
                                  $discount_product = (double)$product->getPrice() - (double)$data["price"];
                                  $discount_product_total += $discount_product;
                                  $discount_product_amount = $discount_product;
                                  $discount_product = ($discount_product  * 100)/(double)$product->getPrice();
                                  

                              }
                              $_item['item_id'] = $data['item_id'];
                              $_item['magento_order_id'] = $data['order_id'];
                              $_item['parent_item_id'] = $data['parent_item_id'];
                              $_item['product_id'] = $data['product_id'];
                              $_item['product_type'] = $data['product_type'];
                              $_item['name'] = $data['name'];
                              $_item['sku'] = $data['sku'];
                              $_item['qty'] = number_format($data['qty_ordered'],2,'.','');
                              $_item['price'] = number_format($product->getPrice(),2,'.','');
                              $_item['bonus'] = ((double)$product->getPrice() == 0.00 ) ? '1' : '0';
                              $_item['row_total'] = number_format($data['row_total'],2,'.','');
                              $_item['discount'] =  number_format($discount_product,2,'.','');
                              $_item['discount_amount'] = $discount_product_amount;
                              $items[] = $_item;
                              $_item = array();

                            }
                        }
                        
                      }

                      $shipping_amount = number_format($order->getShippingAmount(),2,'.','');

                      if ((double)$shipping_amount != 0.00) {
                          $_item = array('igv' => 'S','UM' => 'UND');
                          
                          $_item['name'] = 'FLETE';
                          $_item['sku'] = $helper->getShippingCode();
                          $_item['qty'] = '1.00';
                          $_item['price'] = $shipping_amount;
                          $_item['bonus'] = '0';
                          $_item['row_total'] = $shipping_amount;
                          $_item['discount'] =  '0.00';
                          $_item['discount_amount'] = '0.00';
                          $items[] = $_item;
                      }
                      //factura 
                      $name = $billing_address->getFirstname();
                      $lastname = $billing_address->getLastname();
                      $type_document = $billing_address->getDoctype();
                      $num_document = $billing_address->getFax();
                      $is_billing = '0';$ruc = '';$raz_soc = '';
                      if ($type_document != "1") {
                        $num_document = "00000000";
                      }
                      if ($order->getRucPim() && $order->getRazonSocialPim()) {
                        $type_document = "6";
                        $ruc = $order->getRucPim();
                        $raz_soc = $order->getRazonSocialPim();
                        $is_billing = "1";
                      }
                      $shipping_date = '';
                      $store_code = '';
                      switch ($order->getShippingMethod()) {
                        case 'urbaner_urbaner':
                          $shipping_method_id = '1';
                          break;
                        case 'ondeliveryship_ondeliveryship':
                          $shipping_method_id = '1';
                          break;
                        case 'motoboy_motoboy':
                          $shipping_method_id = '1';
                          break;
                        case 'olva_olva':
                          $shipping_method_id = '2';
                          $shipping_date = Mage::getModel('core/date')->date('Y-m-d', strtotime($order->getOlvaShippingDate()));
                          break;
                        case 'servientrega_servientrega':
                          $shipping_method_id = '2';
                          break;
                        case 'storepickup_storepickup':
                          $shipping_method_id = '3';
                          $_store = Mage::getModel('storelocator/storelocator')->load($order->getStoreStorepickup());
                          if ($_store && $_store->getId()) {
                            $store_code = $_store->getAlmacenIdNavasoft();
                          }
                          break;
                        default:
                          $shipping_method_id = '0';
                          break;
                      }

                      $igv = 0.18;
                      $subtotal = $order->getGrandTotal();
                      $igv_amount = $subtotal * $igv;
                      $subtotal = $subtotal - $igv_amount;
                      
                      $params = array(
                        'config_ruc' => $helper->getRuc(),
                        'type_doc' => '32',
                        'entity_id' => str_pad($entity_id, 7, '0', STR_PAD_LEFT),
                        'series' => $helper->getSeries(),
                        'increment_id' => $order->getIncrementId(),
                        'shipping_type' => $shipping_method_id,
                        'shipping_method' => $order->getShippingMethod(),
                        'shipping_date' =>$shipping_date,
                        'store_code' => $store_code,
                        'almacen_id' => $almacen_id,
                        'customer_id' => $order->getCustomerId(),
                        'customer_email' => $order->getCustomerEmail(),
                        'customer_name' => $order->getCustomerFirstname().' '.$order->getCustomerLastname(),
                        'is_ruc' =>$is_billing,
                        'bussines_name'=> $raz_soc,
                        'ruc'=> $ruc,
                        'igv'=> number_format($igv_amount,2,'.',''),
                        'currency'=> $order->getOrderCurrencyCode(),
                        'exchange_rate' => '0.00',
                        'shipping_amount' => $shipping_amount,
                        'subtotal' => number_format($subtotal,2,'.',''),
                        'grand_total' => number_format($order->getGrandTotal(),2,'.',''),
                        //'discount_amount' => number_format( (double)$order->getDiscountAmount() -  (double)$discount_product_total , 4 ,'.',''),
                        'discount_item_amount' => number_format($discount_product_total,2,'.',''),
                        'discount_promotion_amount' => '0.00',
                        'discount_amount_rules' => number_format((double)$order->getDiscountAmount()*-1 ,2,'.',''),
                        'billing_firstname' => $name,
                        'billing_lastname' => $lastname,
                        'billing_name' => $name.' '.$lastname,
                        'billing_type_document' => $type_document,
                        'billing_num_doc' => $num_document,
                        'billing_region' => $billing_address->getRegion(),
                        'billing_province' => $billing_postcode[0],
                        'billing_district' => $billing_postcode[1],
                        'billing_address' => $billing_address->getData('street'),
                        'billing_reference' => $billing_address->getCompany(),
                        'billing_phone' => $billing_address->getTelephone(),
                        'shipping_firstname' => $shipping_address->getFirstname(),
                        'shipping_lastname' => $shipping_address->getLastname(),
                        'shipping_type_document' => $shipping_address->getDoctype(),
                        'shipping_document' => $shipping_address->getFax(),
                        'shipping_region' =>  $shipping_address->getRegion(),
                        'shipping_province' => $shipping_postcode[0],
                        'shipping_district' => $shipping_postcode[1],
                        'shipping_address' => $shipping_address->getData('street'),
                        'shipping_reference' => $shipping_address->getCompany(),
                        'shipping_phone' => $shipping_address->getTelephone(),
                        'payment_method' => $order->getPayment()->getMethod(),
                        'payment_method_id' => $payemnt_method_id,
                        'type_card' => $payemnt_method_type,
                        'number_card' => $payemnt_method_card,
                        'onestepcheckout_order_comment' => $order->getOnestepcheckoutOrderComment(),
                        'coupon' => $order->getCouponCode(),
                        'order_date' => Mage::getModel('core/date')->date('Y-m-d', strtotime($order->getCreatedAt())),
                        'order_updated_at' => $order->getUpdatedAt(),
                        'points' => $order->getGiantpointsEarn(),
                        'deposit_bank' => '',
                        'order_item' =>$items
                      );
                      //Mage::log($params);
                      $create_order = $navasoft->createOrder($params);
                      //Mage::log($create_order);
                      if ($create_order['status']) {
                        $model->setDataSent($create_order['data']);

                         $response = $navasoft->sendOrder($create_order['file_name'],$create_order['data']);
                         //Mage::log('response');
                         //Mage::log($response);
                         if ($response == 'Aceptado') {
                            $model->setStatus('completado');
                         }
                         $model->setUpdatedAt($date);
                         $model->setResponse($response);
                         $model->save();
                      }

                    }

                    
                } catch (Exception $e){
                        $model->setResponse($e->getMessage());
                        $model->setDataSent(json_encode($params));
                        $model->setUpdatedAt($date);
                        $model->save();
                        Mage::log('Error al actualizar estado de la Tarea Order '.$order->getIncrementId(),null,'navasoft.log');
                }
            }
        }
    	}
      $this->invoice();
	}

  public function setStockAll(){
    $response = array('status' => false ,'success' => 0 , 'error' => 0);

    $helper = Mage::Helper('janaq_navasoft');
    if ($helper->getStatus()) {

      $navasoft = Mage::getModel('janaq_navasoft/generate');
      $almacen_id = $helper->getStoreDefault();
      $count_update = 0;
      $error_sku = '';
      $collection_simple = $this->_getCollectionProductSimple();
      $page_number = 1;
      $pages = $collection_simple->getLastPageNumber();
      do {
          $collection_simple->setCurPage($page_number);
          $skus = '';
          foreach ($collection_simple as $product) {
            $skus .= $product->getSku().',';
          }
          $skus = substr($skus, 0, -1);

          $params = array('skus' => $skus, 'almacen_id' => $almacen_id);
          $products_navasoft = $navasoft->getStockAll($params);
          //Mage::log($products_navasoft);
          foreach ($collection_simple as $product) {
             //Mage::log($product->getTypeId() .' - '. $product->getSku());
            try {

               $key = array_search($product->getSku(), array_column($products_navasoft, 'sku'));
               //Mage::log('encontrado :'.$product->getSku().' - '.$key);
               
               if ($key !== false) {
                 $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product->getId());
                   if ($stockItem->getId() > 0 && $stockItem->getManageStock()) {
                      $qty = $products_navasoft[$key]['stock'];
                      $stockItem->setQty($qty);
                      $stockItem->setIsInStock((int)($qty > 0));
                      $stockItem->save();
                      $count_update++;
                   }
                 }else{
                    //Mage::log('error '.$products_pim[$key]['sku']);
                    $error_sku .= $product->getSku() . '; ';
                 }
              
            } catch (Exception $e) {
                Mage::log($e->getMessage());
            }
             
          }
          
          $page_number++;
          $collection_simple->clear();
          $collection_simple = $this->_getCollectionProductSimple();

      } while ($page_number <= $pages);

      $response['success'] = $count_update;
      $response['error'] = $error_sku;
      $response['status'] = true;

      return $response;

    }else{
       return $response;
    }

  }

  protected function _getCollectionProductSimple(){
      $max_page_default = 20;
      $collection = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToFilter('type_id', array('eq' => 'simple'))
            ->addAttributeToFilter('status', array('eq' => '1'))
            ->addAttributeToSelect('*')
            ->setPageSize($max_page_default);

      return $collection;
  }

  public function invoice(){
    $helper = Mage::Helper('janaq_pim');
    if ($helper->getStatus()) {
      $pim = Mage::getModel('janaq_pim/service');
      $order_pim = $pim->getOrderInvoiced();
      //Mage::log($order_pim);
      $status = 1; // EN DESPACHO.
      $new_status = 'in_dispatch'; // EN DESPACHO.
      foreach ($order_pim as $_order) {
        if ($_order['status_pim'] == 'invoiced_invoiced') {

            $order = Mage::getModel('sales/order')->load($_order['entity_id']);
            if ($order->getId()) {
                //Mage::log($order->getIncrementId());
                $order->setStatusLogistics($status);
                $order->save();
                $res = $pim->orderChangeStatusPim($_order['increment_id'],$new_status);
                //Mage::log($res);
            }
        }
      }
    }
  }

  public function runStatus(){
    
    $date = Mage::getModel('core/date')->date('Y-m-d H:i:s');

    $helper = Mage::helper('janaq_navasoft');
    $days_invoiced = $helper->getDaysInvoiced();

    $fromDate_ = Mage::getModel('core/date')->date('Y-m-d',strtotime('-'.((int)$days_invoiced).' day'));

    $Collection = Mage::getResourceModel('janaq_navasoft/integrator_collection');
    $Collection->addFieldToFilter('status', array('in' => array('completado','pendiente_navasoft','facturado_navasoft')))
            ->addFieldToFilter('created_at', array('gt' => $fromDate_));
    //Mage::log($Collection->getSelect()->__toString());
    if ($helper->getStatus()) {
      foreach ($Collection as  $tareas) {
        $count++;
        $model = Mage::getModel('janaq_navasoft/integrator');
        $model->load($tareas->getId());
        if ($tareas->getTypeTask() == 'Order') {
          try{
            $incrementId = $model->getOrderId();
            $order = Mage::getModel('sales/order')->loadByIncrementId($incrementId);
            if ($order->getId()) {
              $series = $helper->getSeries();
              $entity_id  =  (int) substr($order->getIncrementId(),1);
              $document = $series.'-'.str_pad($entity_id, 7, '0', STR_PAD_LEFT);
              $navasoft = Mage::getModel('janaq_navasoft/generate');
              $response = $navasoft->getStatusOrder(array('serie_entity_id'=> $document),true);
              if ($response && $response[0]) {
                if ($model->getStatus() != $this->_status_mavasoft[trim($response[0])]) {
                    switch (trim($response[0])) {
                    case '*':
                      $this->cancelOrder($order);
                      break;
                    case '-1':
                      //$this->cancelOrder($order);
                      break;
                    case '1':
                      $this->completeOrder($order);
                      break;
                    case '2':
                      $this->completeOrder($order);
                      break;
                  }
                  $model->setUpdatedAt($date);
                  $model->setStatus($this->_status_mavasoft[trim($response[0])]);
                  $model->save();
                }
                
              }
            }
          } catch (Exception $e) {
              Mage::log('Error al cambiar estados en Navasoft: '. $e->getMessage(),null,'navasoft.log');
          }
        }
      }
    }
  }

  protected function cancelOrder($order){
    if ($order->getStatus() != 'canceled') {
        $order->setData('state', "canceled");
        $order->setStatus("canceled");       
        $history = $order->addStatusHistoryComment('canceled', false);
        $history->setIsCustomerNotified(false);
        $order->save();
    }
  }

  protected function completeOrder($order){
      if ($order->getStatus() != 'complete') {
        $new_status = false;
        $_order_1 = $this->createInvoce($order);
        if ($_order_1) {
            $new_status = true;
        }
        $_order_2 = $this->createShipping($order);

      }
  }

  protected function createInvoce($order){

        $orderData = array();

        try {
            if(!$order->canInvoice())
            {
                return false;
            } else {
                $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
                if (!$invoice->getTotalQty()) {
                    return false; // no existen productos
                } else {
                    $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_ONLINE);
                    $invoice->register();
                    $transactionSave = Mage::getModel('core/resource_transaction')
                        ->addObject($invoice)
                        ->addObject($invoice->getOrder());

                    $transactionSave->save();
                    return true;
                }
            }
        } catch (Mage_Core_Exception $e) {
            return false;
        }
  }

  protected function createShipping($order){

      try {
          if (!$order->canShip()) {
              return false;
          } else {
              $itemQty =  $order->getItemsCollection()->count();
              $shipment = Mage::getModel('sales/service_order', $order)->prepareShipment($itemQty);
              $shipment = new Mage_Sales_Model_Order_Shipment_Api();
              $shipmentId = $shipment->create( $order->getIncrementId(), array(), 'Shipment created through ShipMailInvoice', false, false);

              $order->setData('state', "complete");
              $order->setStatus("complete");
              $history = $order->addStatusHistoryComment('Order was set to Complete by our automation tool.', false);
              $history->setIsCustomerNotified(false);
              $order->setStatusLogistics('1');
              $order->save();
              
              return true;
              
          }
      } catch (Mage_Core_Exception $e) {
          return false;
      }
  }

}
