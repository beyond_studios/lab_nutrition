<?php

class Janaq_NavaSoft_Model_Generate
{
	public function createOrder($data){
		$file_name = $this->createFile($data['config_ruc'],$data['type_doc'],$data['series'],$data['entity_id']);
		$data_text = '';
		$response = array('status' => false ,'file_name' => '', 'data' => '');
		if ($file_name) {
			try {
				$head = "01|".$data['billing_name']."|".$data['billing_num_doc']
						."|".$data['customer_email']."|".$data['is_ruc']
						."|".$data['billing_phone']."|".$data['billing_address']
						."|".$data['shipping_address']."|".$data['bussines_name']
						."|".$data['ruc']."|".$data['onestepcheckout_order_comment']."|"."\n";
				$data_text .= $head;
				//$this->setElement($head,$file_name);
				$order = "02|".$data['order_date']."|".$data['type_doc']
						."|".$data['series']."-".$data['entity_id']."|".$data['subtotal']
						."|".$data['grand_total']."|".$data['shipping_amount']
						."|".$data['igv']."|".$data['discount_item_amount']
						."|".$data['currency']."|".$data['discount_promotion_amount']
						."|".$data['discount_amount_rules']."|".$data['exchange_rate']
						."|".$data['payment_method_id']."|".$data['type_card']
						."|".$data['number_card']."|".$data['deposit_bank']
						."|".$data['shipping_type']."|".$data['shipping_date']
						."|".$data['store_code']."|".$data['points']
						."|".$data['coupon']."|";
				$data_text .= $order;
				//$this->setElement($order,$file_name);
				foreach ($data['order_item'] as $item) {
				$_item = "\n"."03|".$item['sku']."|".$item['UM']
						."|".$item['name']."|".$item['qty']
						."|".$item['price']."|".$item['igv']
						."|".$item['discount_amount']."|".$item['row_total']
						."|".$item['bonus']."|";
				$data_text .= $_item;

				//$this->setElement($_item,$file_name);
					
				}
				$response['status'] = true;
				$response['file_name'] = $file_name;
				$response['data'] = $data_text;

				return $response;

			} catch (Exception $e) {
				return false;
			}
		}else{
			return false;
		}
	}
	public function createFile($ruc,$type_doc,$entity_id,$increment_id){
		try {
			$path = Mage::getBaseDir('media') . DS ."navasoft". DS ."order". DS ;
			//$this->_createDestinationFolder($path);
			$file_name = $ruc."-".$type_doc."-".$entity_id."-".$increment_id;
			return $file_name;
			$path_file = Mage::getBaseDir('media') . DS ."navasoft". DS ."order". DS . $file_name.".txt" ;
			$miarchivo=fopen($path_file,'w');
			fclose($miarchivo);
			return $file_name;
		} catch (Exception $e) {
			return false;
		}
		
	}

	public function setElement($element,$file_name){
		$path_file = Mage::getBaseDir('media') . DS ."navasoft". DS ."order". DS . $file_name.".txt" ;
		$miarchivo=fopen($path_file,'a');
		fwrite($miarchivo,$element);
		fclose($miarchivo);
		return true;

	}

	private function _createDestinationFolder($destinationFolder)
	{
	    if (!$destinationFolder) {
	        return false;
	    }

	    if (substr($destinationFolder, -1) == DIRECTORY_SEPARATOR) {
	        $destinationFolder = substr($destinationFolder, 0, -1);
	    }

	    if (!(@is_dir($destinationFolder) || @mkdir($destinationFolder, 0777, true))) {
	        throw new Exception("Unable to create directory '{$destinationFolder}'.");
	    }
	    return true;
	}

	public function sendOrder($file_name,$_data = false){
		$response = false;
		$navasoft = Mage::helper("janaq_navasoft");
		$urlBase = $navasoft->getUrl() . "billdatadoc.asmx?wsdl";
		$method = "UploadFile";
		
		if ($_data) {
			$file = base64_encode($_data);
		}else{

			$path_file = Mage::getBaseDir('media') . DS ."navasoft". DS ."order". DS . $file_name.".txt";
			$file = file_get_contents($path_file);
			$file = base64_encode($file);
		}

		$data = [
	            new \SoapVar([
	                new \SoapVar($file, XSD_STRING, null, null, 'ns1:f'),
	                new \SoapVar($file_name . '.txt', XSD_STRING, null, null, 'ns1:fileName'),
	                new \SoapVar('', XSD_STRING, null, null, 'ns1:path'),
	            ], SOAP_ENC_OBJECT, null, null, 'ns1:request'),
	        ];
		
        /*$data = array (
			  'f' => $file,
			  'fileName' => $file_name.'.txt',
			  'path' => ""
			);
*/
		$response = $this->executeService($urlBase, $method, $data);
        return $response->UploadFileResult;
	}

	public function getStock($params,$all = false){

		$navasoft = Mage::helper("janaq_navasoft");
		$urlBase = $navasoft->getUrl() . "billdatadoc.asmx?wsdl";
		$method = "GetStockAlm";
		$type = "JSON";

		$data = [
            new \SoapVar([
                new \SoapVar($params['items'], XSD_STRING, null, null, 'ns1:items'),
                new \SoapVar($type, XSD_STRING, null, null, 'ns1:tipo'),
            ], SOAP_ENC_OBJECT, null, null, 'ns1:request'),
        ];
        $response_service = $this->executeService($urlBase, $method, $data);
        $stock = $response_service->GetStockAlmResult;
        //Mage::log($stock);
        $response = array('status' => false, 'stock' => '0');
        if ($stock) {
        	$almacen = json_decode($stock,true);
        	$stock = '0';
        	$exist = false;
        	foreach ($almacen as $item) {
        		$exist = true;
        		if ($item['codtie'] == $params['almacen_id']) {
        			$stock = $item['stoc'];
        			break;
        		}
        	}
        	if ($all) {
        		$response['status']= $exist;
        	}else{
        		$response['status']= true;
        	}
        	$response['stock']= $stock;
        	//Mage::log($response);

        	return $response;
        }else{
        	return $response;
        }
	}

	public function getStockAll($params){
		$navasoft = Mage::helper("janaq_navasoft");
		$urlBase = $navasoft->getUrl() . "billdatadoc.asmx?wsdl";
		$method = "GetStockAlm";
		$type = "JSON";

		$data = [
            new \SoapVar([
                new \SoapVar($params['skus'], XSD_STRING, null, null, 'ns1:items'),
                new \SoapVar($type, XSD_STRING, null, null, 'ns1:tipo'),
            ], SOAP_ENC_OBJECT, null, null, 'ns1:request'),
        ];
        $response_service = $this->executeService($urlBase, $method, $data);
        $stock = $response_service->GetStockAlmResult;
        $items = array();
        if ($stock) {
        	$almacen = json_decode($stock,true);
        	foreach ($almacen as $item) {
        		if ($item['codtie'] == $params['almacen_id']) {
        			$_item = array();
        			$_item['sku'] = trim($item['codf']);
        			$_item['stock'] = trim($item['stoc']);
        			$items[] = $_item;
        		}

        	}
        }
        return $items;
	}

	public function getStatusOrder($params,$return_array = false){
		$navasoft = Mage::helper("janaq_navasoft");
		$urlBase = $navasoft->getUrl() . "billdatadoc.asmx?wsdl";
		$method = "GetStatusPedido";
		$data = [
            new \SoapVar([
                new \SoapVar($params['serie_entity_id'], XSD_STRING, null, null, 'ns1:nroped'),
            ], SOAP_ENC_OBJECT, null, null, 'ns1:request'),
        ];
        $response_service = $this->executeService($urlBase, $method, $data);
        $status = $response_service->GetStatusPedidoResult;
        if (!$return_array) {
        	$status = str_replace('<status>', '<h4>Code: ',$status);
	        $status = str_replace('</status>', ' </h4>',$status);
	        $status = str_replace('statusname', 'h4',$status);
        }else{
        	$status = str_replace('<status>', '',$status);
	        $status = str_replace('</status>', ',',$status);
	        $status = str_replace('<statusname>', '',$status);
	        $status = str_replace('</statusname>', ',',$status);
	        $status = explode(',',$status);
        }
        
        return $status;
	}



	// Función API Service 
    public function executeService($url, $funcion, $data)
    {
        //Mage::log("NAVASOFT SERVICIO: ".$url." - FUNCION:".$funcion);
        try {

        	
            $client = new SoapClient($url,array('trace' => 1));
            $response = $client->__soapCall(
                $funcion,
                $data
            );
            //Mage::log($client->__getLastRequest());
            return $response;
        } catch(Exception $e) {
            Mage::log($e->getMessage());
            return false;
        }
    }
}