<?php
$installer = $this;
$installer->startSetup();

$installer->run("
	ALTER TABLE `{$this->getTable('janaq_navasoft/integrator')}` ADD COLUMN `edit_status` INT(1) DEFAULT 0 ;
    ");

$installer->endSetup();