<?php

$installer = $this;
$installer->startSetup();



$table = $installer->getConnection()
    ->newTable($installer->getTable('janaq_navasoft/integrator'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'ID')
    ->addColumn('order_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, 0, array(
        'nullable'  => true,
        ), 'ID ORDER')
    ->addColumn('data_sent', Varien_Db_Ddl_Table::TYPE_VARCHAR, 0, array(
        'nullable'  => true,
        ), 'data_send')
    ->addColumn('type_task', Varien_Db_Ddl_Table::TYPE_VARCHAR, 0, array(
        'nullable'  => false,
        ), 'Type Task')
    ->addColumn('response', Varien_Db_Ddl_Table::TYPE_VARCHAR, 0, array(
        'nullable'  => true,
      ), 'Respuesta Web service')

    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, 0, array(
        'nullable'  => false,
      ), 'Fecha Creación')
    ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_DATETIME, 0, array(
        'nullable'  => false,
      ), 'Fecha Actualización')
    ->addColumn('status', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        'nullable'  => false,
        ), 'Estado');
$installer->getConnection()->createTable($table);

$installer->endSetup();


?>
