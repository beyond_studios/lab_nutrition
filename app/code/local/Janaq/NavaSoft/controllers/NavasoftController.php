<?php
class Janaq_NavaSoft_NavasoftController extends Mage_Core_Controller_Front_Action {
  public function checkAction(){
    $result = array('message'=> null , 'status' => false);
    if ($data = $this->getRequest()->getPost())
    {
      $helper = Mage::helper('janaq_navasoft');
      if ($helper->getStatus() && $helper->getStatusCart()) {
          $store = $this->getRequest()->getParam('store', false);
          $_store_id = '';
          $_store = Mage::getModel('storelocator/storelocator')->load($store);
          if ($_store && $_store->getId()) {
            $_store_id = $_store->getAlmacenIdNavasoft();
          }
          if ($store !='' && $_store_id != '') {

            $quote = Mage::getSingleton('checkout/session')->getQuote();
        		$cartItems = $quote->getAllItems();
            $result['status'] = true;
            foreach ($cartItems as $item) {
              $productSku = $item->getSku();
                if ($item->getProductType() == 'simple') {
                  $model = Mage::getModel('janaq_navasoft/generate');
                  $params  = array('items' => $productSku ,'almacen_id' => $_store_id );
                  $_product_navasoft = $model->getStock($params);

                  if ($_product_navasoft && $_product_navasoft['status']) {
                     $existencia = $_product_navasoft['stock'];
                  }else{
                    $existencia = 0;
                  }
                  $cant = $item->getQty();
                  if ((int)$existencia < (int)$cant) {
                        $result['status'] = false;
                        $result['message'] = 'El producto '.$item->getName().' no esta disponible en la tienda seleccionada';
                        break;
                  }
                }
                
            }
          }else{
            $result['message'] ="No hay stock en la tienda seleccionada.";
          }
      }else{
        $result['status'] = true;
      }
    }
    $result = Mage::helper('core')->jsonEncode($result);
    $this->getResponse()->setBody($result);

  }
}
