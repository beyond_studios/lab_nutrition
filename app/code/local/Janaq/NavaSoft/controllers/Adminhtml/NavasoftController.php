<?php
class Janaq_NavaSoft_Adminhtml_NavasoftController extends Mage_Adminhtml_Controller_Action
{
  public function indexAction()
  {
    $this->_title($this->__('NavaSoft'))->_title($this->__('Integrador List'));

      $this->loadLayout();
      $this->_setActiveMenu('janaq_navasoft/navasoft_integrator');
      $this->_addBreadcrumb(Mage::helper('adminhtml')->__('NavaSoft'), Mage::helper('adminhtml')->__('Integrador List'));
      $this->_addContent($this->getLayout()->createBlock('janaq_navasoft/adminhtml_integrator'));
      $this->renderLayout();
  }
  public function gridAction()
  {
      $this->loadLayout();
      $this->getResponse()->setBody($this->getLayout()->createBlock('janaq_navasoft/adminhtml_integrator_grid')->toHtml());
  }

  public function editAction()
  {


     $id  = $this->getRequest()->getParam('id');
     $model = Mage::getModel('janaq_navasoft/integrator');

     if ($id) {
         $model->load($id);

         if (!$model->getId()) {
             Mage::getSingleton('adminhtml/session')->addError(Mage::helper('janaq_navasoft')->__('No existe'));
             $this->_redirect('*/*');

             return;
         }
     }
     if (!$model->getId()){

     }

     Mage::register('navasoft', $model);

     $this->loadLayout()
     ->_setActiveMenu('janaq_navasoft/integratoradmin')
         ->_addBreadcrumb($id ? $this->__('Integrador') : Mage::helper('adminhtml')
             ->__('Detalles'), $id ? Mage::helper('adminhtml')
             ->__('Detalles') : Mage::helper('adminhtml')->__('Detalles'))
         ->_addContent($this->getLayout()->createBlock('janaq_navasoft/adminhtml_integrator_edit')->setData('action', $this->getUrl('*/*/save')))
         ->renderLayout();
  }

  public function saveAction(){

        if ($data = $this->getRequest()->getPost()) {
            $id = $this->getRequest()->getParam('integrator_id');
            $data_sent = $data['data_sent'];
            $date = Mage::getModel('core/date')->date('Y-m-d H:i:s');
            if ($id) {
               $model = Mage::getModel('janaq_navasoft/integrator')->load($id);
               if ($model->getId() && $model->getStatus() != 'facturado_navasoft') {
                   $model->setDataSent($data_sent);
                   $model->setUpdateAt($date);
                   $model->setEditStatus(1);
                   $model->save();
                   Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Un registro actualizado.'));
               }else{
                    Mage::getSingleton('adminhtml/session')->addError($this->__('El registro no existe o ya fue enviado a Navasoft'));
               }
            }
        }
        $this->_redirect('*/*/');
    }

  public function exportInchooCsvAction()
  {
      $fileName = 'order_pim.csv';
      $grid = $this->getLayout()->createBlock('janaq_navasoft/adminhtml_integrator_grid');
      $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
  }

  public function exportInchooExcelAction()
  {
      $fileName = 'order_pim.xml';
      $grid = $this->getLayout()->createBlock('janaq_navasoft/adminhtml_integrator_grid');
      $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
  }

  public function massStatusAction() {
      $integrator_ids = $this->getRequest()->getParam('integrator');
      if (!is_array($integrator_ids)) {
          Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
      } else {
          try {
              $i = 0;
              foreach ($integrator_ids as $id) {
                  $model = Mage::getModel('janaq_navasoft/integrator')->load($id);
                  if ($model->getStatus() !='completado') {
                      $model->setStatus($this->getRequest()->getParam('status'))
                          ->setIsMassupdate(true)
                          ->save();
                          $i++;
                  }
              }
              $this->_getSession()->addSuccess(
                      $this->__('Total of %d record(s) were successfully updated', $i)
              );
          } catch (Exception $e) {
              $this->_getSession()->addError($e->getMessage());
          }
      }
      $this->_redirect('*/*/index');
  }
  public function executecronAction(){

    $cron = Mage::getModel('janaq_navasoft/cron')->run();
    $this->_getSession()->addSuccess(
                        $this->__('Se a ejecutado el envio de items.')
                );
    $this->_redirect('*/*/index');
  }
  public function sendnavasoftAction() {
      $integrator_ids = $this->getRequest()->getParam('order_ids');
      $this->_redirect('*/sales_order/index');
      if (!is_array($integrator_ids)) {
          Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
      } else {
          try {
              $i = 0;
              foreach ($integrator_ids as $id) {
                  $order = Mage::getModel('sales/order')->load($id);
                  if ($order) {
                      $params['order'] = $order;
                      Mage::dispatchEvent('janaq_navasoft_order_save', $params);
                      $i++;
                  }
              }
              $this->_getSession()->addSuccess(
                      $this->__('Se agregaron %d registros al integrator NAVASOFT.', $i)
              );
          } catch (Exception $e) {
              $this->_getSession()->addError($e->getMessage());
          }
      }


  }

  public function generateOrderAction(){

      $increment_id= $this->getRequest()->getParam('increment_id', false);
      //Mage::log($increment_id);
      $result = array();
      $model = Mage::getModel('janaq_navasoft/integrator');

      $model->load($increment_id,'order_id');
      if ($model->getId()) {
          //Mage::log($model->getStatus());
          if ($model->getStatus() == 'completado') {
              $result['error'] = false;
              $result['message'] = 'La orden ya ha sido enviado a NAVASOFT';
          }elseif ($model->getStatus() == 'pendiente') {
              $result['error'] = false;
              $result['message'] = 'La orden ya esta en espera de envió a NAVASOFT';
          }
      }else{
          //Mage::log("SE CREARA ESTE REGISTRO");
          $date = Mage::getModel('core/date')->date('Y-m-d H:i:s');
          $model->setId(null);
          $model->setOrderId($increment_id);
          $model->setTypeTask('Order');
          $model->setStatus('pendiente');
          $model->setCreatedAt($date);
          $model->setUpdatedAt($date);
          try {
              $model->save();
              $result['error'] = false;
              $result['message'] = 'La orden se ha agregado a la lista de envios para NAVASOFT';
          } catch (Exception $e) {
               $result['error'] = false;
              $result['message'] = $e->getMessage();
          }
      }
      
      $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
  }

  public function importstockAction(){
    $navasoft = Mage::getModel('janaq_navasoft/cron')->setStockAll();
      if ($navasoft['status']) {
          
          $this->_getSession()->addSuccess($this->__('Se actualizaron %d productos.',$navasoft['success']));
          if ($navasoft['error'] != 0) {
            $this->_getSession()->addError($this->__('Los siguientes códigos no existen en NAVASOFT: ' . $navasoft['error']));
          }
          

      }else{
          $this->_getSession()->addError(
                      $this->__('Activar el modulo NavaSoft de las configuraciones.')
              );
      }

      $this->_redirect('*/*/index');
  }

  protected function _isAllowed()
  {
      return Mage::getSingleton('admin/session')->isAllowed('janaq_navasoft/integratoradmin');
  }

}
