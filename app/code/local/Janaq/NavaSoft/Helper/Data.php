<?php
/**
 * @category   Janaq
 * @package    Janaq_Urbaner
 * @author     contacto@janaq.com

 */
class Janaq_NavaSoft_Helper_Data extends Mage_Core_Helper_Abstract
{
    // AUTH SERVICES
    const STATUS = "janaq_navasoft/configuration/enable";
    const URL = "janaq_navasoft/configuration/url";
    const RUC = "janaq_navasoft/configuration/ruc";
    const SERIES = "janaq_navasoft/configuration/series";
    const SHIPPING_CODE = "janaq_navasoft/configuration/shipping_code";

    const STATUS_CART = "janaq_navasoft/configuration/enable_stock_cart";
    const STORE_DEFAULT = "janaq_navasoft/configuration/store_default";
    const VALIDATE_PRODUCT = "janaq_navasoft/configuration/enable_validate_product_admin";
    const VALIDATE_PRODUCT_BUNDLE = "janaq_navasoft/configuration/enable_validate_product_bundle_admin";
    const DAYS_INVOICED = "janaq_navasoft/configuration/days_invoiced";
    
    public function getStatus()
    {
        return Mage::getStoreConfig(self::STATUS, Mage::app()->getStore());
    }
    public function getStoreDefault()
    {
        return Mage::getStoreConfig(self::STORE_DEFAULT, Mage::app()->getStore());
    }
    public function getUrl()
    {
        return Mage::getStoreConfig(self::URL, Mage::app()->getStore());
    }
    public function getRuc()
    {
        return Mage::getStoreConfig(self::RUC, Mage::app()->getStore());
    }
    public function getSeries()
    {
        return Mage::getStoreConfig(self::SERIES, Mage::app()->getStore());
    }

    public function getStatusCart()
    {
        return Mage::getStoreConfig(self::STATUS_CART, Mage::app()->getStore());
    }
    public function getValidateProductAdmin()
    {
        return Mage::getStoreConfig(self::VALIDATE_PRODUCT, Mage::app()->getStore());
    }
    public function getValidateProductBundleAdmin()
    {
        return Mage::getStoreConfig(self::VALIDATE_PRODUCT_BUNDLE, Mage::app()->getStore());
    }
    public function getShippingCode()
    {
        return Mage::getStoreConfig(self::SHIPPING_CODE, Mage::app()->getStore());
    }

    public function getDaysInvoiced()
    {
        return Mage::getStoreConfig(self::DAYS_INVOICED, Mage::app()->getStore());
    }

    public function getStatusIntegrator(){

        $status = array(
            'completado' => 'Envío Completado',
            'pendiente'  => 'Envío Pendiente',
            'inhabilitado' => 'Envío Inabilitado',
            "completado_manual" => "Envio Completado Manualmente",
            "not_exist_navasoft" => 'Ya no Existe en Navasoft',
            "anulado_navasoft" => 'Anulado NavaSoft',
            "pendiente_navasoft" => "Pendiente NavaSoft",
            "facturado_navasoft" => "Facturado NavaSoft",
            "aprobado_navasoft" => "Aprobado NavaSoft"
        );

        return $status;
    }

}
