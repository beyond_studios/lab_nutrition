<?php

class Janaq_NavaSoft_Block_Adminhtml_Integrator extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'janaq_navasoft';
        $this->_controller = 'adminhtml_integrator';
        $this->_headerText = Mage::helper('adminhtml')->__('NavaSoft - Integrador');

    	parent::__construct();
    	$this->_removeButton('add');
    	$this->_addButton('execute_cron', array(
            'label'   => Mage::helper('catalog')->__('Envio manual de pendientes'),
            'onclick' => "setLocation('{$this->getUrl('*/navasoft/executecron')}')",
            'class'   => 'add'
        ));
        $helper = Mage::Helper('janaq_navasoft');
        $store_default = $helper->getStoreDefault();
        $this->_addButton('import_stock_store_default', array(
            'label'   => Mage::helper('catalog')->__('Importar Stock de Tienda Por Default'),
            'onclick' => "(function () { var r = confirm('".$this->__('Esta seguro que quiere actualizar el stock de todos los productos de la Tienda por Default.'.'('.$store_default.')')."');if (r == true){setLocation('{$this->getUrl('*/navasoft/importstock')}');}
                            }());",
            'class'   => 'add',
        ));

        
    }
}
