<?php
class Janaq_NavaSoft_Block_Adminhtml_Integrator_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Init class
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('navasoft_form_detall');
        $this->setTitle('Detalles');
    }

    /**
     * Setup form fields for inserts/updates
     *
     * return Mage_Adminhtml_Block_Widget_Form
     */

    protected function _prepareForm()
    {
        $model = Mage::registry('navasoft');
        $form = new Varien_Data_Form(array(
            'id'        => 'edit_form',
            'action'    => $this->getUrl('*/*/save', array('integrator_id' => $this->getRequest()->getParam('id'))),
            'method'    => 'post'
        ));

        $fieldset = $form->addFieldset('base_fieldset', array(
            'legend'    => 'Detalles',
            'class'     => 'fieldset-wide',
        ));

        $fieldset->addField('order_id', 'label', array(
            'name'      => 'order_id',
            'label'     => 'Increment ID',
            'onchange' => "",
            'title'     => 'ID',
        ));
        $fieldset->addField('data_sent', 'textarea', array(
            'name'      => 'data_sent',
            'label'     => 'Datos de Envío',
            'onchange' => "",
            'title'     => 'ID',
        ));

        $fieldset->addField('response', 'label', array(
            'name'      => 'response',
            'label'     => 'Datos de Respuesta',
            'onchange' => "",
            'title'     => 'ID',
        ));

        $fieldset->addField('status', 'label', array(
            'name'      => 'status',
            'label'     => 'Estado Integrador',
            'onchange' => "",
            'title'     => 'ID',
        ));

        $fieldset->addField('created_at', 'label', array(
            'name'      => 'created_at',
            'label'     => 'Fecha creación',
            'onchange' => "",
            'title'     => 'ID',
        ));

        $fieldset->addField('updated_at', 'label', array(
            'name'      => 'updated_at',
            'label'     => 'Fecha Actualización',
            'onchange' => "",
            'title'     => 'ID',
        ));
        $helper = Mage::helper('janaq_navasoft');
        if ($helper->getStatus()) {

            $fieldset->addType('status_order', 'Janaq_NavaSoft_Block_Adminhtml_Renderer_Action');
            $fieldset->addField('status_order_navasoft', 'status_order', array(
                'name'      => 'status_order_navasoft',
                'label'     => 'Estado en NAVASOFT',
                'order_id'  =>  $model->getOrderId(),
                'series'  =>  $helper->getSeries(),
                'renderer' => 'janaq_navasoft/adminhtml_renderer_action'
            ));
        }
        

        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
