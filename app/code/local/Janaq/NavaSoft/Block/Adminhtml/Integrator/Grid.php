<?php

class Janaq_NavaSoft_Block_Adminhtml_Integrator_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('janaq_navasoft_integrator_grid');
        $this->setDefaultDir('DESC');
        $this->setDefaultSort('order_id');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('janaq_navasoft/integrator_collection');
        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header' => 'ID',
            'index'  => 'id',

        ));
        $this->addColumn('order_id', array(
            'header' => 'Increment ID',
            'index'  => 'order_id',

        ));

        $this->addColumn('response', array(
            'header'   => 'Datos Respuesta',
            'index'    => 'response',
            'sortable' => false
        ));
        $this->addColumn('created_at', array(
            'header'   => 'CreatedAt',
            'index'    => 'created_at',
            'sortable' => false
        ));
        $this->addColumn('updated_at', array(
            'header'   => 'UpdatedAt',
            'index'    => 'updated_at',
            'sortable' => false
        ));
        /*$this->addColumn('status', array(
            'header'   => 'Estado de envío',
            'index'    => 'status',
            'sortable' => false
        ));*/

        $this->addColumn('status', array(
            'header' => 'Estado',
            'index' => 'status',
            'type'  => 'options',
            'width' => '120px',
            'options' => Mage::helper('janaq_navasoft')->getStatusIntegrator(),
        ));

        //$this->addExportType('*/*/exportInchooCsv', 'CSV');
        //$this->addExportType('*/*/exportInchooExcel', 'Excel XML');

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    protected function _prepareMassaction(){
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('integrator');

        $statuses = array('pendiente'=>'Pendiente','completado_manual'=>'Completado Manual','inhabilitado'=>'Envio Inhabilitado');

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
            'label'=> Mage::helper('adminhtml')->__('Cambiar de estado'),
            'url'   => $this->getUrl('*/*/massStatus', array('_current'=>true)),
            'additional' => array(
                'visibility' => array(
                    'name'  => 'status',
                    'type'  => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('adminhtml')->__('Status'),
                    'values'=> $statuses
                ))
        ));

        //$this->getMassactionBlock()->addItem('delete', array(
        //    'label'     => Mage::helper('adminhtml')->__('Delete'),
        //    'url'       => $this->getUrl('*/*/massDelete'),
        //    'confirm'   => Mage::helper('adminhtml')->__('Are you sure?')
        //));

        return $this;
    }
}
