<?php
class Janaq_NavaSoft_Block_Adminhtml_Renderer_Action extends Varien_Data_Form_Element_Abstract {

	protected $_element;
    public function getElementHtml()
    {   
        $html = '';
        	$series = $this->getData('series');
        	$order_id = $this->getData('order_id');
        	$order = Mage::getModel('sales/order')->loadByIncrementId($order_id);
            $entity_id  =  (int) substr($order->getIncrementId(),1);
        	$document = $series.'-'.str_pad($entity_id, 7, '0', STR_PAD_LEFT);
        	$navasoft = Mage::getModel('janaq_navasoft/generate');
        	$response = $navasoft->getStatusOrder(array('serie_entity_id'=> $document));
            $html = $html.$response;
        return $html;
    }
}
?>