<?php 

class Janaq_ImportProduct_Adminhtml_ImportproductController extends Mage_Adminhtml_Controller_Action
{
	public function editimportAction(){

		$this->loadLayout()
        ->_setActiveMenu('catalog/product')
            ->_addBreadcrumb($this->__('Importar Productos'), Mage::helper('adminhtml')->__('Importar Productos'))
            
            ->_addContent($this->getLayout()->createBlock('janaq_importproduct/adminhtml_import_edit'))
            ->renderLayout();

	}
	public function saveAction(){


		if ($data = $this->getRequest()->getPost())
        {
            $result = array();
            if ($_FILES['file_product']['name']){
                if ($_FILES['file_product']['type'] == 'application/vnd.ms-excel' || $_FILES['file_product']['type'] == 'text/csv' ){
                    $result = Mage::getModel('janaq_importproduct/import')->uploadAndImport($_FILES['file_product']['tmp_name']);
                }else{
                    $result['error'] = true;
                    $result['message'] ='Formato de archivo Incorrecto.';
                }
                if (isset($result['failed'])) {
                	if ($result['failed']) {
            		    Mage::getSingleton('core/session')->addError($result['failed']);
                	}
                }
                if (!$result['error']) {
                	Mage::getSingleton('core/session')->addSuccess($result['message']);
                }else{
                	Mage::getSingleton('core/session')->addError($result['message']);
                }
            }else{
            	Mage::getSingleton('core/session')->addError('Por favor carge un archivo .csv');
            }
            //Mage::getSingleton('core/session')->addSuccess($result['error']);
            
        }

        //Mage::getSingleton('core/session')->addSuccess('Los cambios han sido guardados.');

		$this->_redirect('*/*/editimport');
	}

	public function exporttemplateAction()
    {
    	$date = Mage::getModel('core/date')->date('Y-m-d');
        $fileName   = 'import_product-'.$date.'.csv';

        $gridBlock  = $this->getLayout()->createBlock('janaq_importproduct/adminhtml_import_grid');
        
        $content    = $gridBlock->getCsvFile();
        $this->_prepareDownloadResponse($fileName, $content);

    }
    protected function _isAllowed()
    {

        return Mage::getSingleton('admin/session')->isAllowed('catalog/products');
    }
}