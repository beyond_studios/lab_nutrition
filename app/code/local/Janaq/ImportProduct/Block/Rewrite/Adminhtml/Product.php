<?php 

class Janaq_ImportProduct_Block_Rewrite_Adminhtml_Product extends Mage_Adminhtml_Block_Catalog_Product {


	protected function _prepareLayout()
    {
        $this->_addButton('import_products', array(
            'label'   => Mage::helper('catalog')->__('Importar Productos'),
            'onclick' => "setLocation('{$this->getUrl('*/importproduct/editimport')}')",
            'class'   => 'add'
        ));
        return parent::_prepareLayout();
    }
}