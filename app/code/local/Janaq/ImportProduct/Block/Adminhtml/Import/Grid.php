<?php
/**
 * Created by PhpStorm.
 * User: HOME
 * Date: 12/12/2016
 * Time: 11:29
 */

class Janaq_ImportProduct_Block_Adminhtml_Import_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    protected $_websiteId;

    protected $_conditionName;

    public function __construct()
    {
        parent::__construct();
        $this->setId('janaq_importproduct_gird');
        $this->_exportPageSize = 10000;
    }

    protected function _prepareCollection()
    {
        /** @var $collection Mage_Shipping_Model_Mysql4_Carrier_Tablerate_Collection */
        //$collection = Mage::getResourceModel('janaq_importproduct/import');

        $this->setCollection(new Varien_Data_Collection());

        return parent::_prepareCollection();
    }
    protected function _prepareColumns()
    {
        $this->addColumn('code', array(
            'header'    => $this->__('CODIGO'),
            'index'     => 'code',
            //'default'   => '*',
        ));

        $this->addColumn('name', array(
            'header'    => $this->__('Nombre del producto'),
            'index'     => 'name',
            //'default'   => '*',
        ));

        $this->addColumn('stock', array(
            'header'    => $this->__('STOCK'),
            'index'     => 'stock',
            //'default'   => '*',
        ));

        $this->addColumn('weight', array(
            'header'    => $this->__('PESO (KG)'),
            'index'     => 'weight',
            //'default'   => '*',
        ));

        $this->addColumn('regular_price', array(
            'header'    => $this->__('Precio Regular'),
            'index'     => 'regular_price',
        ));

        $this->addColumn('special_price', array(
            'header'    => $this->__('Precio Especial'),
            'index'     => 'special_price',
        ));

        $this->addColumn('special_from_date', array(
            'header'    => $this->__('Fecha Inicio Precio Especial'),
            'index'     => 'special_from_date',
            'default'   => Mage::getModel('core/date')->date('Y-m-d'),
        ));

        $this->addColumn('special_to_date', array(
            'header'    => $this->__('Fecha Fin Precio Especial'),
            'index'     => 'special_to_date',
        ));

        return parent::_prepareColumns();
    }
}