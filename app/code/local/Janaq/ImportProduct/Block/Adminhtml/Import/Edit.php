<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 03/11/2016
 * Time: 03:11 PM
 */
class Janaq_ImportProduct_Block_Adminhtml_Import_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Init class
     */
    public function __construct()
    {
        $this->_blockGroup = 'janaq_importproduct';
        $this->_controller = 'adminhtml_import';

        parent::__construct();

        $this->_updateButton('save', 'label', $this->__('Importar Productos'));
        $this->_removeButton('delete');
        $this->_removeButton('reset');
        $this->_removeButton('back');
        $this->_addButton('back_', array(
            'label'   => Mage::helper('catalog')->__('Volver atrás'),
            'onclick' => "setLocation('{$this->getUrl('*/catalog_product/index')}')",
            'class'   => 'back'
        ));
        $this->_addButton('import_products', array(
            'label'   => Mage::helper('catalog')->__('Exportar Plantilla'),
            'onclick' => "setLocation('{$this->getUrl('*/importproduct/exporttemplate')}')",
            'class'   => 'go'
        ));
        
        //$this->_removeButton('save');
    }

    /**
     * Get Header text
     *
     * @return string
     */
    public function getHeaderText()
    {
        
            return $this->__('Importar Productos');
        
    }
}