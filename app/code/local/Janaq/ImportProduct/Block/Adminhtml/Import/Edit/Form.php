<?php
class janaq_ImportProduct_Block_Adminhtml_Import_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Init class
     */
    public function __construct()
    {  
        parent::__construct();
        $this->setId('janaq_importproduct_form');
        $this->setTitle('Importar Productos');
    }  
     
    /**
     * Setup form fields for inserts/updates
     *
     * return Mage_Adminhtml_Block_Widget_Form
     */
    
    protected function _prepareForm()
    {  
        //$model = Mage::registry('integrador_siesa');
        $form = new Varien_Data_Form(array(
            'id'        => 'edit_form',
            'action'    => $this->getUrl('*/*/save', array('integrador_id' => $this->getRequest()->getParam('integrador_id'))),
            'method'    => 'post',
            'enctype' => 'multipart/form-data'
        ));
     
        $fieldset = $form->addFieldset('base_fieldset', array(
            'legend'    => 'Detalle',
            'class'     => 'fieldset-wide',
        ));
    

        $fieldset->addField('file_product', 'file', array(
          'label'     => Mage::helper('adminhtml')->__('Cargar Archivo'),
          'value'  => 'Uplaod',
          'name'      => 'file_product',
          'disabled' => false,
          'readonly' => true,
          'tabindex' => 1
        ));
        
        

        
     
        //$form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);  
     
        return parent::_prepareForm();
    }  
}