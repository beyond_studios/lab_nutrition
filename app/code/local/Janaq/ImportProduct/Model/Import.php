<?php 

class Janaq_ImportProduct_Model_Import extends Mage_Core_Model_Abstract
{
	protected $_importErrorsAjax = array();

	protected $_importErrors        = array();

	protected $_updated = 0;

	protected $_failed = array();

    public function uploadAndImport($FILES)
    {
    	if (empty($FILES)) {
            return array('error'=> true,'message'=>'Archivo Incorrecto');
        }

        $csvFile = $FILES;
        $this->_importUniqueHash    = array();
        $this->_importErrors        = array();
        $this->_importedRows        = 0;

        $io     = new Varien_Io_File();
        $info   = pathinfo($csvFile);
        $io->open(array('path' => $info['dirname']));
        $io->streamOpen($info['basename'], 'r');

        $headers = $io->streamReadCsv();

        if ($headers === false || count($headers) < 6) {
            $io->streamClose();
            //Mage::throwException(Mage::helper('shipping')->__('Invalid Table Rates File Format'));
            
            return array('error'=> true,'message'=>'Formato de archivo de tarifas no válido');
        }

        //$adapter = $this->_getWriteAdapter();
        //$adapter->beginTransaction();

        try {

        	$rowNumber  = 1;
            $importData = array();

            while (false !== ($csvLine = $io->streamReadCsv())) {
                $rowNumber ++;
                if (empty($csvLine)) {
                    continue;
                }

                $row = $this->_getImportRow($csvLine, $rowNumber);

                if ($row !== false) {
                    $importData[] = $row;
                }

                if (count($importData) == 5000) {
                    $this->_saveImportData($importData);
                    $importData = array();
                }
            }
            $this->_saveImportData($importData);

            $io->streamClose();
        	
        } catch (Mage_Core_Exception $e) {
        	$adapter->rollback();
            $io->streamClose();
            //Mage::throwException($e->getMessage());
            return array('error'=> true,'message'=>$e->getMessage());
        } catch (Exception $e) {
        	$adapter->rollback();
            $io->streamClose();
            Mage::logException($e);
            //Mage::throwException(Mage::helper('shipping')->__('An error occurred while import table rates.'));
            
            return array('error'=> true,'message'=>'Se ha producido un error al importar');
        }

        if ($this->_importErrors) {
            
            return array('error'=> true,'message'=>Mage::helper('shipping')->__('El archivo no se ha importado. Consulte los siguientes errores: </br>%s', implode("</br>", $this->_importErrors)));
            //Mage::throwException($error);
        }
        $message = 'No existen registros para importar';
        $failed = false;
        if ($this->_updated > 0) {
        	$message = 'Se actualizaron exitosamente '.$this->_updated. ' productos';
        }
        if ($this->_failed) {
        	$failed = 'Los siguentes productos no se encontraron : '.implode(", ", $this->_failed);
        }
    	return array('error'=> false ,'message'=>$message,'failed'=>$failed);

    }
    protected function _getImportRow($row, $rowNumber = 0)
    {
    	if (count($row) < 5) {
            $this->_importErrors[] = Mage::helper('shipping')->__('La columna tiene formato inválido #%s', $rowNumber);
            return false;
        }
        foreach ($row as $k => $v) {
            $row[$k] = trim($v);
        }
        //Validate code
        $code = $row[0];
        //validate name
        $name = $row[1];


        //validate stock

        $stock = $this->_parseIntValue($row[2]);

        if ($stock === false) {
        	$this->_importErrors[] = Mage::helper('shipping')->__('El stock "%s" tiene formato inválido en la columna  #%s.', $row[2], $rowNumber);
            return false;
        }

        $weight = $this->_parseDecimalValue($row[3]);

        if ($weight === false) {
        	$this->_importErrors[] = Mage::helper('shipping')->__('El peso "%s" tiene formato inválido en la columna  #%s.', $row[3], $rowNumber);
            return false;
        }

        $regular_price = $this->_parseDecimalValue($row[4]);
        if ($regular_price === false) {
            $this->_importErrors[] = Mage::helper('shipping')->__('El precio regular "%s" tiene formato inválido en la columna #%s.', $row[4], $rowNumber);
            return false;
        }

        $special_price = $this->_parseDecimalValue($row[5]);
        if ($special_price === false) {
            $this->_importErrors[] = Mage::helper('shipping')->__('El precio especial "%s" tiene formato inválido en la columna #%s.', $row[5], $rowNumber);
            return false;
        }
            $special_from_date = $this->_validateDate($row[6]);
        if ($special_price <= $regular_price) {
            if ($special_from_date === false) {
                $this->_importErrors[] = Mage::helper('shipping')->__('La fecha inicio "%s" tiene formato inválido en la columna #%s.', $row[6], $rowNumber);
                return false;
            }
            $special_to_date = $this->_validateDate($row[7]);
            if ($special_to_date === false) {
                $this->_importErrors[] = Mage::helper('shipping')->__('La fecha fin del precio especial "%s" no es correcta en la columna #%s.', $row[7], $rowNumber);
                return false;
            }
            if ($special_to_date != "-" && $special_from_date != "-" 
                && strtotime($special_to_date) < strtotime($special_from_date)) {
                $this->_importErrors[] = Mage::helper('shipping')->__('La fecha fin del precio especial "%s" es menor a la fecha inicio en la columna %s.', $row[7], $rowNumber);
                return false;
            }

        }
        else{
            $this->_importErrors[] = Mage::helper('shipping')->__('El precio especial "%s" es mayor al precio regular "%s" en la columna #%s.', $row[5], $row[4], $rowNumber);
            return false;
        }

        return array(
            $code,
            $name,
            $stock,
            $weight,
            $regular_price,
            $special_price,
            $special_from_date,
            $special_to_date
        );
    }

    protected function _saveImportData(array $data)
    {
        $now = Mage::getModel('core/date')->date('Y-m-d h:i:s');
        foreach ($data as $row) {
        	//Mage::log($product);
        	$product = Mage::getModel('catalog/product')->loadByAttribute('sku', $row[0]);
        	if ($product) {
        		//Mage::log($product->getName());
        		$stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product->getId());
                if ($stockItem->getId() > 0 && $stockItem->getManageStock()) {
                	//Mage::log($stockItem->getQty());
                    $qty = (int)$row[2];
                    if ($qty >= 0) {
                        $stockItem->setQty($qty);
                        $stockItem->setIsInStock((int)($qty > 0));
                        $stockItem->save();
                    }
                    $this->_updated++;

                    if ($row[4] != 0.0000) {
                        $product->setPrice($row[4]);
                    }
                    if ($row[5] != 0.0000) {
                        $product->setSpecialPrice($row[5]);
                        if ($row[6] == "-") {
                            $product->setSpecialFromDate($now);
                        }
                        else{
                            $product->setSpecialFromDate($row[6]);
                        }
                        if ($row[7] == "-") {
                            $product->setSpecialToDate('');
                        }
                        else{
                            $product->setSpecialToDate($row[7]);
                        }
                    }

                    if ($row[3] != 0.0000) {
                    	$product->setWeight($row[3]);
                        //$product->save();
                    }
                    $product->save();
                }

        	}else{
        		$this->_failed[] = $row[0];
        	}
        	
        }

        return $this;
    }

    protected function _parseIntValue($value)
    {
        if (!is_numeric($value)) {
            return false;
        }
       
        return $value;
    }

    protected function _parseDecimalValue($value)
    {
        if (!is_numeric($value)) {
            return false;
        }
        $value = (float)sprintf('%.4F', $value);
        if ($value < 0.0000) {
            return false;
        }
        return $value;
    }

    protected function _validateDate($value)
    {
        $value = str_replace('/', '-', $value);
        $validator = new Zend_Validate_Date('DD-MM-YYYY');
        if(trim($value) == "-"){
            return trim($value);
        }
        else if($validator->isValid($value)){
            $_date = date('Y-m-d h:i:s', strtotime($value));
            return $_date;
        }
        return false;
    }
}