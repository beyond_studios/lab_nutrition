<?php
/*
NOTICE OF LICENSE

This source file is subject to the SafeMageEULA that is bundled with this package in the file LICENSE.txt.

It is also available at this URL: https://www.safemage.com/LICENSE_EULA.txt

Copyright (c)  SafeMage (https://www.safemage.com/)
*/


class SafeMage_Adminlog_Adminhtml_Safemage_Adminlog_LogController extends Mage_Adminhtml_Controller_Action
{
    protected $_helperAlias = 'safemage_adminlog';

    protected $_modelAlias = 'safemage_adminlog/item';


    public function indexAction()
    {
        $this->loadLayout();
        $this->_addContent( $this->getLayout()->createBlock('safemage_adminlog/adminhtml_log') );
        $this->renderLayout();
    }

    public function editAction()
    {
        $id = $this->getRequest()->getParam('item_id');
        $model = Mage::getModel( $this->_modelAlias )->load( $id );
        if ( $model->getItemId() ) {
            Mage::register('item', $model, true);
            $this->loadLayout();
            $this
                ->_addContent($this->getLayout()->createBlock('safemage_adminlog/adminhtml_log_edit'))
                ->_addLeft($this->getLayout()->createBlock('safemage_adminlog/adminhtml_log_edit_tabs'))
            ;
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper( $this->_helperAlias )->__('Item does not exist'));
            $this->_redirect('*/*/');
        }
    }

    public function massDeleteAction()
    {
        $itemIds = $this->getRequest()->getParam('item_ids');

        if( count($itemIds) == 0 ) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('safemage_adminlog')->__('Please select item(s).'));
        } else {
            try {

                $table = Mage::getSingleton('core/resource')->getTableName('safemage_adminlog/item');
                $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                $write->delete(
                    $table,
                    $write->quoteInto('item_id IN (?)', $itemIds)
                );

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('safemage_adminlog')->__('Item(s) were successfully deleted.'));

            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }

        $this->_redirect('*/*/index');
    }

    public function gridAction()
    {
        $grid = $this->getLayout()->createBlock('safemage_adminlog/adminhtml_log_grid');
        $this->getResponse()->setBody( $grid->toHtml() );
    }


    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')
            ->isAllowed('admin/system/safemage_adminlog');
    }
}