<?php
/*
NOTICE OF LICENSE

This source file is subject to the SafeMageEULA that is bundled with this package in the file LICENSE.txt.

It is also available at this URL: https://www.safemage.com/LICENSE_EULA.txt

Copyright (c)  SafeMage (https://www.safemage.com/)
*/

$installer = $this;
$installer->startSetup();

$installer->run("
CREATE TABLE IF NOT EXISTS `{$installer->getTable('safemage_adminlog/item')}` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `object_id` varchar(255) NOT NULL,
  `object` varchar(255) NOT NULL,
  `diff` text NOT NULL,
  `store_id` SMALLINT( 5 ) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip` varchar(50) DEFAULT NULL,
  `action_name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `object_id` (`object_id`),
  KEY `user_id` (`user_id`),
  KEY `ip` (`ip`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=790;
");

$installer->getConnection()->resetDdlCache();
$installer->endSetup();