<?php
/*
NOTICE OF LICENSE

This source file is subject to the SafeMageEULA that is bundled with this package in the file LICENSE.txt.

It is also available at this URL: https://www.safemage.com/LICENSE_EULA.txt

Copyright (c)  SafeMage (https://www.safemage.com/)
*/

use SafeMage\Adminlog\Json;

class SafeMage_Adminlog_Model_Item 
extends Mage_Core_Model_Abstract
{
    const ACTION_NEW    = 1;
    const ACTION_EDIT   = 2;
    const ACTION_DELETE = 3;

    public function _construct()
    {
        parent::_construct();
        $this->_init('safemage_adminlog/item');
    }

    public function getUser()
    {
        $user = Mage::getModel('admin/user')->load( $this->getUserId() );
        if ( $user->getUserId() ) {
            return $user;
        }

        return null;
    }

    public function getDiff()
    {
        $diff = $this->getData('diff');
        $a = Zend_Json::decode($diff);  // removes slashes automatically

        foreach($a as $name => &$change) {
            $change[0] = Json::getInstance()->htmlDecode($change[0]);
            $change[1] = Json::getInstance()->htmlDecode($change[1]);
        }

        return $a;
    }
}
