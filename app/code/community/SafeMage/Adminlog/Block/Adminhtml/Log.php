<?php
/*
NOTICE OF LICENSE

This source file is subject to the SafeMageEULA that is bundled with this package in the file LICENSE.txt.

It is also available at this URL: https://www.safemage.com/LICENSE_EULA.txt

Copyright (c)  SafeMage (https://www.safemage.com/)
*/

class SafeMage_Adminlog_Block_Adminhtml_Log 
extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    protected $_helperAlias = 'safemage_adminlog';
	
	public function __construct()
	{
		$this->_controller = 'adminhtml_log';
		$this->_blockGroup = 'safemage_adminlog';
		$this->_headerText = Mage::helper( $this->_helperAlias )->__('Admin Actions Log');

		parent::__construct();
		$this->_removeButton('add');
	}
}
