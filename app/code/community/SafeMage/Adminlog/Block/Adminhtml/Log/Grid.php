<?php
/*
NOTICE OF LICENSE

This source file is subject to the SafeMageEULA that is bundled with this package in the file LICENSE.txt.

It is also available at this URL: https://www.safemage.com/LICENSE_EULA.txt

Copyright (c)  SafeMage (https://www.safemage.com/)
*/

class SafeMage_Adminlog_Block_Adminhtml_Log_Grid 
extends Mage_Adminhtml_Block_Widget_Grid
{
    protected $_helperAlias = 'safemage_adminlog';

    protected $_massactionIdField = 'item_id';

	protected $_massactionIdFilter = 'item_id';
	
	public function __construct()
	{
		parent::__construct();
		$this->setId('logGrid');
        $this->setDefaultSort('item_id');
		$this->setDefaultDir('DESC');
		$this->setDefaultLimit(50);
		$this->setUseAjax(true);
	}

	protected function _prepareCollection()
	{
		$collection = Mage::getResourceModel('safemage_adminlog/item_collection');
		$select = $collection->getSelect();
		$select
			->joinLeft(
				array('au' => 'admin_user'),
				'au.user_id = main_table.user_id',
				array('admin' => new Zend_Db_Expr('CONCAT(au.firstname, " ", au.lastname, " (", au.username, ")")'))
			)
		;

		$this->setCollection($collection);	

		$parent = parent::_prepareCollection();
		return $parent;
	}

	protected function _prepareColumns()
	{
		$this->addColumn('item_id', array(
			'header' => Mage::helper( $this->_helperAlias )->__('ID'),
			'align' =>'center',
			'width' => '10px',
			'index' => 'item_id',
			'filter' => false,
		));

		$this->addColumn('object_id', array(
			'header' => Mage::helper( $this->_helperAlias )->__('Entity ID'),
			'align' => 'center',
			'index' => 'object_id',
			'filter_index' => 'main_table.object_id',
			'width' => '50px',
		));	

		$this->addColumn('object', array(
			'header' => Mage::helper( $this->_helperAlias )->__('Entity Type'),
			'align' => 'center',
			'index' => 'object',
			'filter_index' => 'main_table.object',
			'width' => '80px',
		));

		if ( !Mage::app()->isSingleStoreMode() ) {
			$col = $this->addColumn('store_id', array(
				'header' => Mage::helper($this->_helperAlias)->__('Store View'),
				'index' => 'store_id',
				'type'  => 'store',
				'renderer' => 'SafeMage_Adminlog_Block_Adminhtml_Log_Grid_Renderer_Store',
				'store_view' => true,
				'display_deleted' => true,
				'width' => '200px',

			));
			$col->setData('skipAllStoresLabel', false);
		}

		$this->addColumn('admin', array(
			'header' => Mage::helper( $this->_helperAlias )->__('User'),
			'align' => 'center',
			'index' => 'admin',
			'filter_index' => new Zend_Db_Expr('CONCAT(au.firstname, " ", au.lastname, " (", au.username, ")")'),
		));

		$this->addColumn('ip', array(
			'header' => Mage::helper( $this->_helperAlias )->__('Remote Address'),
			'align' => 'center',
			'index' => 'ip',
			'width' => '100px',
		));

		$this->addColumn('action_name', array(
			'header' => Mage::helper( $this->_helperAlias )->__('Full Action Name'),
			'index' => 'action_name',
		));

		$this->addColumn('created_at', array(
			'header' => Mage::helper( $this->_helperAlias )->__('Action Time'),
			'index' => 'created_at',
			'type' => 'datetime',
			'width' => '100px',
		));

        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper( $this->_helperAlias )->__('Action'),
				'align'     => 'center',
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getItemId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper( $this->_helperAlias )->__('View'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'item_id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
        ));		
		
		return parent::_prepareColumns();
	}

	protected function _prepareMassaction()
    {
        $this->setMassactionIdField('item_id');
        $this->getMassactionBlock()->setFormFieldName('item_ids');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper( $this->_helperAlias )->__('Delete'),
             'url'      => rtrim( $this->getUrl('*/*/massDelete'), '/' ),
             'confirm'  => Mage::helper( $this->_helperAlias )->__('Are you sure?')
        ));
        
        return $this;
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }	
	
	public function getRowUrl($row)
	{
		return $this->getUrl('*/*/edit', array('item_id' => $row->getItemId()));
	}
	
	public function _afterToHtml($html)
	{
	    return '<style>.grid th span {text-align: center;}</style>' 
		. parent::_afterToHtml($html);
	}
}
