<?php
/*
NOTICE OF LICENSE

This source file is subject to the SafeMageEULA that is bundled with this package in the file LICENSE.txt.

It is also available at this URL: https://www.safemage.com/LICENSE_EULA.txt

Copyright (c)  SafeMage (https://www.safemage.com/)
*/

class SafeMage_Adminlog_Block_Adminhtml_Log_Edit_Tabs 
extends Mage_Adminhtml_Block_Widget_Tabs
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('item_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('safemage_adminlog')->__('Action Details'));
	}

	protected function _beforeToHtml()
    {
    	$this->addTab('main_section', array(
          'label'     => Mage::helper('safemage_adminlog')->__('Item Information'),
          'title'     => Mage::helper('safemage_adminlog')->__('Item Information'),
          'content'   => $this->getLayout()->createBlock('safemage_adminlog/adminhtml_log_edit_tab_main')->toHtml(),
        ));
       
        return parent::_beforeToHtml();
    }
}
