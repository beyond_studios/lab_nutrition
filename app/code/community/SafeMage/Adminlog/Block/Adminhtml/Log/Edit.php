<?php
/*
NOTICE OF LICENSE

This source file is subject to the SafeMageEULA that is bundled with this package in the file LICENSE.txt.

It is also available at this URL: https://www.safemage.com/LICENSE_EULA.txt

Copyright (c)  SafeMage (https://www.safemage.com/)
*/

class SafeMage_Adminlog_Block_Adminhtml_Log_Edit 
extends Mage_Adminhtml_Block_Widget_Form_Container
{
    protected $_helperAlias = 'safemage_adminlog';

	public function __construct()
	{	
	    $dataDelete = array(
            'label'     => Mage::helper( $this->_helperAlias )->__('Delete'),
            'class'     => 'delete',
        );		
		if ( $item = Mage::registry('item') )
		{
		    $dataDelete['onclick']= "setLocation('" . $this->getUrl('*/*/delete', array('item_id' => $item->getItemId())) . "')";
		}

	    parent::__construct();

		$this->_objectId   = 'id';
		$this->_blockGroup = 'safemage_adminlog';
		$this->_controller = 'adminhtml_log';

		$this->_removeButton('reset');
		$this->_removeButton('save');
	}

	public function getHeaderText()
	{
		$user = Mage::registry('item')->getUser();
		return $user->getName();
	}

	public function getHeaderCssClass() {
		return 'icon-head head-customer';
	}
}
