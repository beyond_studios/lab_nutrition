<?php
/*
NOTICE OF LICENSE

This source file is subject to the SafeMageEULA that is bundled with this package in the file LICENSE.txt.

It is also available at this URL: https://www.safemage.com/LICENSE_EULA.txt

Copyright (c)  SafeMage (https://www.safemage.com/)
*/

class SafeMage_Adminlog_Block_Adminhtml_Log_Grid_Renderer_Store
extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Store
{
    public function render(Varien_Object $row)
    {
        $value = $this->_getValue($row);
        if ( empty($value) ) {
            return $this->__('All Store Views');
        }

        return parent::render($row);
    }
}
