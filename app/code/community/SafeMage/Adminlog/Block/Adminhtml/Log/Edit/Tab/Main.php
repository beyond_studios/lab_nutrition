<?php
/*
NOTICE OF LICENSE

This source file is subject to the SafeMageEULA that is bundled with this package in the file LICENSE.txt.

It is also available at this URL: https://www.safemage.com/LICENSE_EULA.txt

Copyright (c)  SafeMage (https://www.safemage.com/)
*/

use SafeMage\Adminlog\Diff;

class SafeMage_Adminlog_Block_Adminhtml_Log_Edit_Tab_Main 
extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _construct()
	{
		parent::_construct();
		$this->setTemplate('safemage/adminlog/item/edit.phtml');
	}

	protected function _prepareForm()
	{
		return parent::_prepareForm();
	}

	protected function _filterOutput($contents)
	{
		$contents = str_replace(array("\n"), array("<br/>"), $contents);
		return $contents;
	}

	protected function _displayDiff($from, $to)
	{
		$diff = Diff::compare($from, $to, true);
		$diff = Diff::toHtml($diff, '');

		$diff = $this->_filterOutput($diff);

		return $diff;
	}
}
