<?php
/**
 * Udevix
 *
 * @author     UdevixTeam <udevix@gmail.com>
 * @copyright  Copyright (c) 2015-2017 Udevix
 */

/**
 * Class Udevix_GoogleTagManagerEE_EnhancedEcommerceController
 */
class Udevix_GoogleTagManagerEE_EnhancedEcommerceController extends Mage_Core_Controller_Front_Action
{
//    public function productClickAction()
//    {
//        $link = $this->getRequest()->getParam('link', '');
//
//        // get path to the product
//        $linkData = parse_url($link);
//        if (isset($linkData['path'])) {
//            $link = $linkData['path'];
//        }
//
//        $link = trim($link, " \t\n\r\0\x0B/");
//
//        $result = false;
//
//        /**
//         * @var Mage_Catalog_Model_Product  $product
//         * @var Mage_Core_Model_Url_Rewrite $catalogUrl
//         */
//        $product = null;
//
//        // get URL rewrite for this link
//        $catalogUrl = Mage::getModel('core/url_rewrite')->getCollection()->addFieldToFilter('request_path', $link)->addStoreFilter(null)->getFirstItem();
//
//        $idProduct = $catalogUrl->getProductId();
//
//        if ($idProduct > 0) {
//            $product = Mage::getModel('catalog/product')->load($idProduct);
//
//            if ($product->getId() > 0) {
//                $categoryIds = $product->getCategoryIds();
//                $categoryId = $categoryIds[sizeof($categoryIds) - 1];
//
//                /**
//                 * @var Mage_Catalog_Model_Category $category
//                 */
//                $category = Mage::getModel('catalog/category')->load($categoryId);
//                $categoryName = $category->getName();
//
//                $result = array(
//                    'event'     => 'productClick',
//                    'ecommerce' => array(
//                        'click' => array(
//                            'products' => array(
//                                array(
//                                    'name'     => $product->getName(),
//                                    'id'       => $product->getSku(),
//                                    'price'    => $product->getFinalPrice(),
//                                    'category' => $categoryName
//                                )
//                            )
//                        )
//                    )
//                );
//            }
//        }
//
//        $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
//        $this->getResponse()->setBody(json_encode($result));
//    }

    public function checkoutAction()
    {
        $step = $this->getRequest()->getParam('step', '');
        $result = array();

        switch ($step) {
            // Billing information
            case 'billing':
                $products = array();

                /**
                 * @var Mage_Sales_Model_Quote      $quote
                 * @var Mage_Sales_Model_Quote_Item $quoteItem
                 * @var Mage_Catalog_Model_Product  $product
                 */
                $quote = Mage::getSingleton('checkout/session')->getQuote();

                /**
                 * @var Udevix_GoogleTagManagerEE_Helper_Data $helper
                 */
                $helper = Mage::helper('udevix_google_tag_manager_ee');

                foreach ($quote->getAllVisibleItems() as $quoteItem) {
                    $product = $quoteItem->getProduct();

                    $products[] = array(
                        'name'     => $product->getName(),
                        'id'       => $helper->getProductId($product),
                        'price'    => $helper->getProductPrice($product),
                        'category' => $helper->getProductCategory($product)
                    );
                }

                $result = array(
                    'event'     => 'checkout',
                    'ecommerce' => array(
                        'checkout' => array(
                            'actionField' => array(
                                'step' => 1
                            ),
                            'products'    => $products
                        )
                    )
                );
                break;

            // Shipping information
            case 'shipping':
                $result = array(
                    'event'     => 'checkout',
                    'ecommerce' => array(
                        'checkout' => array(
                            'actionField' => array(
                                'step' => 2
                            )
                        )
                    )
                );
                break;

            // Shipping method
            case 'shipping_method':
                $shippingMethod = '';

                try {
                    $shippingMethod = Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->getShippingDescription();
                } catch (Exception $e) {
                    Mage::logException($e);
                }

                $result = array(
                    'event'     => 'checkout',
                    'ecommerce' => array(
                        'checkout' => array(
                            'actionField' => array(
                                'step'   => 3,
                                'option' => $shippingMethod
                            )
                        )
                    )
                );
                break;

            // Payment information
            case 'payment':
                $paymentMethod = '';
                try {
                    $paymentMethod = Mage::getSingleton('checkout/session')
                        ->getQuote()
                        ->getPayment()
                        ->getMethodInstance()
                        ->getTitle();
                } catch (Exception $e) {
                    Mage::logException($e);
                }

                $result = array(
                    'event'     => 'checkout',
                    'ecommerce' => array(
                        'checkout' => array(
                            'actionField' => array(
                                'step'   => 4,
                                'option' => $paymentMethod
                            )
                        )
                    )
                );
                break;

            // Order review
            case 'review':
                $result = array(
                    'event'     => 'checkout',
                    'ecommerce' => array(
                        'checkout' => array(
                            'actionField' => array(
                                'step' => 5
                            )
                        )
                    )
                );
                break;
        }

        $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
        $this->getResponse()->setBody(json_encode($result));
    }
}
