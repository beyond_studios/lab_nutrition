<?php
/**
 * Udevix
 *
 * @author     UdevixTeam <udevix@gmail.com>
 * @copyright  Copyright (c) 2015-2017 Udevix
 */

/**
 * Class Udevix_GoogleTagManagerEE_Block_Pixel_Cart
 */
class Udevix_GoogleTagManagerEE_Block_Pixel_Cart extends Mage_Core_Block_Template
{
    /**
     * Return data for tracking additions to cart
     *
     * @return bool|string
     */
    public function getCartAddProductData()
    {
        if ($this->getRequest()->isAjax()) {
            return false;
        }

        /**
         * @var Udevix_GoogleTagManagerEE_Helper_Data $helper
         */
        $helper = Mage::helper('udevix_google_tag_manager_ee');

        // Get added products
        $products = $helper->getGtmCartProducts();

        $result = false;

        if (!is_null($products)) {
            $currencyCode = Mage::app()->getStore()->getCurrentCurrencyCode();

            $ids = array();
            $price = 0;

            foreach ($products as $product) {
                $ids[] = $product['id'];
                $price += $product['price'];
            }

            $ids = array_values(array_unique($ids));

            $result = json_encode(array(
                'event'            => 'udxAddToCart',
                'udx_value'        => round($price, 2),
                'udx_currency'     => $currencyCode,
                'udx_content_type' => 'product',
                'udx_content_ids'  => $ids,
            ));

            return $result;
        }

        return $result;
    }
}
