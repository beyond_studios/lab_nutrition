<?php
/**
 * Udevix
 *
 * @author     UdevixTeam <udevix@gmail.com>
 * @copyright  Copyright (c) 2015-2017 Udevix
 */

/**
 * Class Udevix_GoogleTagManagerEE_Block_EnhancedEcommerce_Product_Detail
 */
class Udevix_GoogleTagManagerEE_Block_EnhancedEcommerce_Product_Detail extends Mage_Core_Block_Template
{
    /**
     * Return json data for product view
     *
     * @return bool|string
     */
    public function getProductData()
    {
        /**
         * @var Mage_Catalog_Model_Product $product
         */
        $product = Mage::registry('current_product');

        if (!is_null($product)) {
            /**
             * @var Udevix_GoogleTagManagerEE_Helper_Data $helper
             */
            $helper = Mage::helper('udevix_google_tag_manager_ee');

            $productsArray = array(
                array(
                    'name'     => $product->getName(),
                    'id'       => $helper->getProductId($product),
                    'price'    => $helper->getProductPrice($product),
                    'category' => $helper->getProductCategory($product)
                )
            );

            $result = array(
                'event'     => 'productDetailView',
                'ecommerce' => array(
                    'detail' => array(
                        'products' => $productsArray
                    ),
                )
            );

            return json_encode($result);
        }

        return false;
    }
}
