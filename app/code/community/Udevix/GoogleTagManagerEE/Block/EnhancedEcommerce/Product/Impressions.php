<?php
/**
 * Udevix
 *
 * @author     UdevixTeam <udevix@gmail.com>
 * @copyright  Copyright (c) 2015-2017 Udevix
 */

/**
 * Class Udevix_GoogleTagManagerEE_Block_EnhancedEcommerce_Product_Impressions
 */
class Udevix_GoogleTagManagerEE_Block_EnhancedEcommerce_Product_Impressions extends Mage_Core_Block_Template
{
    /**
     * Return json data for product impressions
     *
     * @return bool|string
     */
    public function getProductImpressionsData()
    {
        $currencyCode = Mage::app()->getStore()->getCurrentCurrencyCode();

        $result = array(
            'event'     => 'productImpressions',
            'ecommerce' => array(
                'currencyCode' => $currencyCode,
                'impressions'  => array()
            )
        );

        // Get type of opened page
        $pageType = $this->getPageType();

        // Generate data by page type
        switch ($pageType) {
            case 'category':
                $impressions = $this->getImpressions(true);

                if (empty($impressions)) {
                    return false;
                }

                $result['ecommerce']['impressions'] = $impressions;

                break;

            case 'search':
                $impressions = $this->getImpressions(false);

                if (empty($impressions)) {
                    return false;
                }

                $result['ecommerce']['impressions'] = $impressions;

                break;

            default:
                return false;
        }

        return json_encode($result);
    }

    /**
     * Return products impressions
     *
     * @param bool $isCategoryView
     *
     * @return array
     */
    private function getImpressions($isCategoryView)
    {
        /**
         * @var Mage_Catalog_Block_Product_List  $productListBlock
         * @var Mage_Catalog_Block_Category_View $productCategoryView
         */
        $productListBlock = Mage::getBlockSingleton('catalog/product_list');

        $isProductMode = true;

        if ($isCategoryView) {
            $productCategoryView = Mage::getBlockSingleton('catalog/category_view');

            if ($productCategoryView->isContentMode()) {
                $isProductMode = false;
            }
        }

        $products = array();

        // Do not load products if there is content mode
        if ($isProductMode) {
            /**
             * @var Mage_Eav_Model_Entity_Collection_Abstract $products
             */
            $products = $productListBlock->getLoadedProductCollection();
        }

        $impressions = array();
        $position = 1;

        /**
         * @var Udevix_GoogleTagManagerEE_Helper_Data $helper
         */
        $helper = Mage::helper('udevix_google_tag_manager_ee');

        /**
         * @var Mage_Catalog_Model_Product $product
         */
        foreach ($products as $product) {
            $impressionData = array(
                'name'     => $product->getName(),
                'id'       => $helper->getProductId($product),
                'price'    => $helper->getProductPrice($product),
                'position' => $position
            );

            $position++;
            $impressions[] = $impressionData;
        }

        return $impressions;
    }
}
