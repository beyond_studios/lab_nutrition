<?php
/**
 * Udevix
 *
 * @author     UdevixTeam <udevix@gmail.com>
 * @copyright  Copyright (c) 2015-2017 Udevix
 */

/**
 * Class Udevix_GoogleTagManagerEE_Block_EnhancedEcommerce_Checkout
 */
class Udevix_GoogleTagManagerEE_Block_EnhancedEcommerce_Checkout extends Mage_Core_Block_Template
{
    public function getLastStepData()
    {
        $result = array(
            'event'     => 'checkout',
            'ecommerce' => array(
                'checkout' => array(
                    'actionField' => array(
                        'step' => 6
                    )
                )
            )
        );

        return json_encode($result);
    }
}
