<?php
/**
 * Udevix
 *
 * @author     UdevixTeam <udevix@gmail.com>
 * @copyright  Copyright (c) 2015-2017 Udevix
 */

/**
 * Class Udevix_GoogleTagManagerEE_Block_EnhancedEcommerce_Cart
 */
class Udevix_GoogleTagManagerEE_Block_EnhancedEcommerce_Cart extends Mage_Core_Block_Template
{
    /**
     * Return data for tracking additions to cart
     *
     * @return bool|string
     */
    public function getCartAddProductData()
    {
        if ($this->getRequest()->isAjax()) {
            return false;
        }

        /**
         * @var Udevix_GoogleTagManagerEE_Helper_Data $helper
         */
        $helper = Mage::helper('udevix_google_tag_manager_ee');

        // Get added products
        $products = $helper->getGtmCartProducts();
        $currencyCode = Mage::app()->getStore()->getCurrentCurrencyCode();
        $result = false;

        if (!is_null($products)) {
            $result = array(
                'event'     => 'addToCart',
                'ecommerce' => array(
                    'currencyCode' => $currencyCode,
                    'add'          => array(
                        'products' => $products
                    )
                )
            );

            return json_encode($result);
        }

        return $result;
    }

    /**
     * Return data for tracking removals from cart
     *
     * @return array|bool|string
     */
    public function getCartRemoveProductData()
    {
        if ($this->getRequest()->isAjax()) {
            return false;
        }

        // Get removed products
        $products = Mage::getSingleton('core/session')->getGtmCartRemoveProducts();
        $currencyCode = Mage::app()->getStore()->getCurrentCurrencyCode();
        $result = false;

        if (!is_null($products)) {
            $result = array(
                'event'     => 'removeFromCart',
                'ecommerce' => array(
                    'currencyCode' => $currencyCode,
                    'remove'       => array(
                        'products' => $products
                    )
                )
            );

            Mage::getSingleton('core/session')->unsGtmCartRemoveProducts();

            return json_encode($result);
        }

        return $result;
    }
}
