<?php
/**
 * Udevix
 *
 * @author     UdevixTeam <udevix@gmail.com>
 * @copyright  Copyright (c) 2015-2017 Udevix
 */

/**
 * Class Udevix_GoogleTagManagerEE_Block_EnhancedEcommerce_Purchase
 */
class Udevix_GoogleTagManagerEE_Block_EnhancedEcommerce_Purchase extends Mage_Core_Block_Template
{
    public function getPurchaseData()
    {
        $orderId = Mage::getSingleton('checkout/session')->getLastRealOrderId();

        if ($orderId) {
            /**
             * @var Udevix_GoogleTagManagerEE_Helper_Data $helper
             */
            $helper = Mage::helper('udevix_google_tag_manager_ee');

            /**
             * @var Mage_Sales_Model_Order $order
             */
            $order = Mage::getModel('sales/order')->loadByAttribute('increment_id', $orderId);

            $result = array(
                'event'     => 'purchase',
                'ecommerce' => array(
                    'purchase' => array(
                        'actionField' => array(
                            'id'          => $order->getIncrementId(),
                            'affiliation' => $helper->getTransactionAffiliation(),
                            'revenue'     => max(round($order->getGrandTotal(), 2), 0),
                            'tax'         => round($order->getTaxAmount(), 2),
                            'shipping'    => round($order->getShippingAmount(), 2)
                        ),
                        'products'    => array()
                    ),
                )
            );

            $items = $order->getAllVisibleItems();

            /**
             * @var Mage_Sales_Model_Order_Item $item
             */
            foreach ($items as $item) {
                $price = $item->getPrice();

                /**
                 * @var Mage_Catalog_Model_Product $product
                 */
                $product = $item->getProduct();

                $result['ecommerce']['purchase']['products'][] = array(
                    'id'       => $helper->getProductId($product),
                    'name'     => $item->getName(),
                    'category' => $helper->getProductCategory($product),
                    'price'    => round($price, 2),
                    'quantity' => (int)$item->getQtyOrdered()
                );
            }

            return json_encode($result);
        }

        return false;
    }
}
