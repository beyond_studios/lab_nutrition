<?php
/**
 * Udevix
 *
 * @author     UdevixTeam <udevix@gmail.com>
 * @copyright  Copyright (c) 2015-2017 Udevix
 */

/**
 * Class Udevix_GoogleTagManagerEE_Model_Config
 */
class Udevix_GoogleTagManagerEE_Model_Config extends Mage_Core_Model_Config_Data
{
    public function save()
    {
        $value = trim($this->getValue());

        $info = parse_url(Mage::getBaseUrl());
        $host = '';

        if (isset($info['host'])) {
            $host = $info['host'];
        }

        /**
         * @var Udevix_GoogleTagManagerEE_Helper_Name $helper
         */
        $helper = Mage::helper('udevix_google_tag_manager_ee/name');
        $config = trim(Mage::getStoreConfig(Udevix_GoogleTagManagerEE_Helper_Data::XML_PATH_CONFIG_KEY));

        try {
            if ($value !== $config) {
                $client = new Varien_Http_Client('https://udevix.com/verify-config', array(
                    'timeout' => 3
                ));
                $client->setMethod(Varien_Http_Client::POST);
                $client->setParameterPost('code_name', $helper->getCodeName());
                $client->setParameterPost('host', $host);
                $client->setParameterPost('key', $value);
                $client->request();
            }
        } catch (Exception $e) {
        }

        if (hash('sha512', $host . '5f!%' . $helper->getCodeName()) !== $value) {
            Mage::throwException("Wrong license key!");
        }

        return parent::save();
    }
}
