<?php
/**
 * Udevix
 *
 * @author     UdevixTeam <udevix@gmail.com>
 * @copyright  Copyright (c) 2015-2017 Udevix
 */

/**
 * Class Udevix_GoogleTagManagerEE_Model_Observer
 */
class Udevix_GoogleTagManagerEE_Model_Observer
{
    /**
     * Observer for event "checkout_cart_add_product_complete"
     *
     * @param Varien_Event_Observer $observer
     */
    public function cartAddProductComplete($observer)
    {
        $event = $observer->getEvent();

        /**
         * @var Udevix_GoogleTagManagerEE_Helper_Data $helper
         */
        $helper = Mage::helper('udevix_google_tag_manager_ee');

        /**
         * @var Mage_Catalog_Model_Product $product
         */
        $product = $event->getProduct();

        // Get added products
        $products = $helper->getGtmCartProducts();

        if ($helper->isActiveCartAddRemove()) {
            $productArray = array(
                'name'     => $product->getName(),
                'id'       => $helper->getProductId($product),
                'price'    => $helper->getProductPrice($product),
                'category' => $helper->getProductCategory($product),
                'quantity' => intval($product->getCartQty())
            );
        } else {
            $productArray = array(
                'id'    => $helper->getProductId($product),
                'price' => $helper->getProductPrice($product),
            );
        }

        if (is_null($products)) {
            $products = array($productArray);
        } else {
            $products[] = $productArray;
        }

        // Write products to the session
        Mage::getSingleton('core/session')->setGtmCartAddProducts($products);
    }

    public function salesQuoteRemoveItem($observer)
    {
        $event = $observer->getEvent();

        /**
         * @var Mage_Sales_Model_Quote_Item $quoteItem
         */
        $quoteItem = $event->getQuoteItem();

        /**
         * @var Mage_Catalog_Model_Product $product
         */
        $product = $quoteItem->getProduct();

        /**
         * @var Udevix_GoogleTagManagerEE_Helper_Data $helper
         */
        $helper = Mage::helper('udevix_google_tag_manager_ee');

        // Get removed products
        $products = Mage::getSingleton('core/session')->getGtmCartRemoveProducts();

        $productArray = array(
            'name'     => $product->getName(),
            'id'       => $helper->getProductId($product),
            'price'    => $quoteItem->getPrice(),
            'category' => $helper->getProductCategory($product),
            'quantity' => intval($quoteItem->getQty())
        );

        if (is_null($products)) {
            $products = array($productArray);
        } else {
            $products[] = $productArray;
        }

        // Write products to the session
        Mage::getSingleton('core/session')->setGtmCartRemoveProducts($products);
    }
}
