<?php
/**
 * Udevix
 *
 * @author     UdevixTeam <udevix@gmail.com>
 * @copyright  Copyright (c) 2015-2017 Udevix
 */

/**
 * Class Udevix_GoogleTagManagerEE_Helper_Name
 */
class Udevix_GoogleTagManagerEE_Helper_Name extends Mage_Core_Helper_Abstract
{
    CONST CODE_NAME = 'Udevix_GoogleTagManagerEE';

    public function getCodeName()
    {
        return self::CODE_NAME;
    }
}
