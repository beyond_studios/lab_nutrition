<?php
/**
 * Udevix
 *
 * @author     UdevixTeam <udevix@gmail.com>
 * @copyright  Copyright (c) 2015-2017 Udevix
 */

/**
 * Class Udevix_GoogleTagManagerEE_Helper_Data
 */
class Udevix_GoogleTagManagerEE_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_ENABLED = 'udevix_gtm_ee/udevix_gtm_group/enable';
    const XML_PATH_CONFIG_KEY = 'udevix_gtm_ee/udevix_gtm_group/license_key';
    const XML_PATH_CONTAINER_ID = 'udevix_gtm_ee/udevix_gtm_group/container_id';
    const XML_PATH_ENABLE_REMARKETING = 'udevix_gtm_ee/udevix_gtm_group/enable_remarketing';
    const XML_PATH_ENABLE_ADWORDS_CONVERSION = 'udevix_gtm_ee/udevix_gtm_group/enable_adwords_conversion';
    const XML_PATH_ENABLE_PIXEL = 'udevix_gtm_ee/udevix_gtm_group/enable_pixel';
    const XML_PATH_ENABLE_PRODUCT_IMPRESSION = 'udevix_gtm_ee/ee_group/enable_product_impression';
    const XML_PATH_ENABLE_PRODUCT_DETAIL = 'udevix_gtm_ee/ee_group/enable_product_detail';
    const XML_PATH_ENABLE_PRODUCT_CLICK = 'udevix_gtm_ee/ee_group/enable_product_click';
    const XML_PATH_ENABLE_CART_ADD_REMOVE_PRODUCT = 'udevix_gtm_ee/ee_group/enable_cart_add_remove_product';
    const XML_PATH_ENABLE_PURCHASE = 'udevix_gtm_ee/ee_group/enable_purchase';
    const XML_PATH_ENABLE_CHECKOUT = 'udevix_gtm_ee/ee_group/enable_checkout';
    const XML_PATH_TRANSACTION_AFFILIATION = 'udevix_gtm_ee/ee_group/transaction_affiliation';

    private $isActive;
    protected $baseCurrencyCode;
    protected $currentCurrencyCode;

    /**
     * Return module status
     *
     * @return mixed
     */
    public function isActive()
    {
        if (null === $this->isActive) {
            $info = parse_url(Mage::getBaseUrl());
            $host = '';

            if (isset($info['host'])) {
                $host = $info['host'];
            }

            /**
             * @var Udevix_GoogleTagManagerEE_Helper_Name $helper
             */
            $helper = Mage::helper('udevix_google_tag_manager_ee/name');
            $config = trim(Mage::getStoreConfig(self::XML_PATH_CONFIG_KEY));

            if (hash('sha512', $host . '5f!%' . $helper->getCodeName()) === $config
                && Mage::getStoreConfig(self::XML_PATH_ENABLED)
            ) {
                $this->isActive = true;
            } else {
                $this->isActive = false;
            }
        }
        return true;
        //return $this->isActive;
    }

    /**
     * Return Google Tag Manager container id
     *
     * @return mixed
     */
    public function getContainerId()
    {
        return Mage::getStoreConfig(self::XML_PATH_CONTAINER_ID);
    }

    /**
     * Return remarketing status
     *
     * @return mixed
     */
    public function isActiveRemarketing()
    {
        return Mage::getStoreConfig(self::XML_PATH_ENABLE_REMARKETING);
    }

    /**
     * Return transaction affiliation
     *
     * @return string
     */
    public function getTransactionAffiliation()
    {
        return Mage::getStoreConfig(self::XML_PATH_TRANSACTION_AFFILIATION);
    }

    /**
     * Return adwords conversion status
     *
     * @return mixed
     */
    public function isActiveAdwordsConversion()
    {
        return Mage::getStoreConfig(self::XML_PATH_ENABLE_ADWORDS_CONVERSION);
    }

    /**
     * Return pixel status
     *
     * @return mixed
     */
    public function isActivePixel()
    {
        return Mage::getStoreConfig(self::XML_PATH_ENABLE_PIXEL);
    }

    /**
     * Return product impressions status
     *
     * @return mixed
     */
    public function isActiveProductImpression()
    {
        return Mage::getStoreConfig(self::XML_PATH_ENABLE_PRODUCT_IMPRESSION);
    }

    /**
     * Return product detail view status
     *
     * @return mixed
     */
    public function isActiveProductDetailView()
    {
        return Mage::getStoreConfig(self::XML_PATH_ENABLE_PRODUCT_DETAIL);
    }

    /**
     * Return product click status
     *
     * @return mixed
     */
    public function isActiveProductClick()
    {
        return Mage::getStoreConfig(self::XML_PATH_ENABLE_PRODUCT_CLICK);
    }

    /**
     * Return additions or removals from a shopping cart status
     *
     * @return mixed
     */
    public function isActiveCartAddRemove()
    {
        return Mage::getStoreConfig(self::XML_PATH_ENABLE_CART_ADD_REMOVE_PRODUCT);
    }

    /**
     * Return purchases status
     *
     * @return mixed
     */
    public function isActivePurchases()
    {
        return Mage::getStoreConfig(self::XML_PATH_ENABLE_PURCHASE);
    }

    /**
     * Return checkout status
     *
     * @return mixed
     */
    public function isActiveCheckout()
    {
        return Mage::getStoreConfig(self::XML_PATH_ENABLE_CHECKOUT);
    }

    /**
     * Return HTML attributes for tracking product click
     *
     * @param Mage_Catalog_Model_Product $product
     *
     * @return string
     */
    public function click($product)
    {
        $str = '';

        if ($this->isActiveProductClick() && !is_null($product) && $product->getId()) {
            $attributes = array(
                'data-gtm-click-id'       => $this->getProductId($product),
                'data-gtm-click-name'     => $product->getName(),
                'data-gtm-click-price'    => $this->getProductPrice($product),
                'data-gtm-click-category' => $this->getProductCategory($product)
            );

            $str .= ' ';

            foreach ($attributes as $key => $value) {
                $str .= $key . '="' . htmlspecialchars($value) . '" ';
            }
        }

        return $str;
    }

    /**
     * Get SKU or product id
     *
     * @param Mage_Catalog_Model_Product $product
     *
     * @return string
     */
    public function getProductId($product)
    {
        $id = $product->getSku();

        if (!$id) {
            $id = $product->getId();
        }

        return $id;
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     *
     * @return float
     */
    public function getProductPrice($product)
    {
        $price = $product->getFinalPrice();

        // if Bundle Product - get minimal price
        if ($product->getTypeId() === Mage_Catalog_Model_Product_Type::TYPE_BUNDLE) {
            $price = Mage::getModel('bundle/product_price')->getTotalPrices($product, 'min', 1);
        }

        if (null === $this->baseCurrencyCode) {
            $this->baseCurrencyCode = Mage::app()->getStore()->getBaseCurrencyCode();
        }

        if (null === $this->currentCurrencyCode) {
            $this->currentCurrencyCode = Mage::app()->getStore()->getCurrentCurrencyCode();
        }

        if ($this->baseCurrencyCode !== $this->currentCurrencyCode) {
            $price = Mage::helper('directory')->currencyConvert(
                $price,
                $this->baseCurrencyCode,
                $this->currentCurrencyCode
            );
        }

        $price = round($price, 2);

        return $price;
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     *
     * @return string
     */
    public function getProductCategory($product)
    {
        $name = '';

        $categoryIds = $product->getCategoryIds();

        if (isset($categoryIds[sizeof($categoryIds) - 1])) {
            $categoryId = $categoryIds[sizeof($categoryIds) - 1];

            /**
             * @var Mage_Catalog_Model_Category $category
             */
            $category = Mage::getModel('catalog/category')->load($categoryId);
            $name = $category->getName();
        }

        return $name;
    }

    /**
     * Get added to cart products from session
     *
     * @return array
     */
    public function getGtmCartProducts()
    {
        $key = 'gtm_cart_add_products';
        $products = Mage::registry($key);

        if (null !== $products) {
            return $products;
        }

        $products = Mage::getSingleton('core/session')->getGtmCartAddProducts();
        Mage::register($key, $products);
        Mage::getSingleton('core/session')->unsGtmCartAddProducts();

        return $products;
    }
}
