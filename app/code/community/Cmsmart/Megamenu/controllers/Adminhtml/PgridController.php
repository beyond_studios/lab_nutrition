<?php

class Cmsmart_Megamenu_Adminhtml_PgridController extends Mage_Adminhtml_Controller_action
{

	/* protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('pgrid/items')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
		
		return $this;
	}   */ 
 
	public function indexAction() {
		$this->_initAction()
			->renderLayout();
	}

	public function gridAction()
     {
        $this->loadLayout();
        $this->getResponse()->setBody(
               $this->getLayout()->createBlock('megamenu/adminhtml_pgrid_grid')
			   ->toHtml()
        );
	}
	
	public function vgridAction()
     {
        $this->loadLayout();
        $this->getResponse()->setBody(
               $this->getLayout()->createBlock('megamenu/adminhtml_pgrid_vergrid')
			   ->toHtml()
        );
     }
	 
	 public function mgridAction()
     {     
        echo      
			$this->getLayout()->createBlock('megamenu/adminhtml_pgrid_grid')
			->setTemplate('widget/grid.phtml')->toHtml();
     }
	 
	 public function mvergridAction()
     {     
        echo      
			$this->getLayout()->createBlock('megamenu/adminhtml_pgrid_vergrid')
			->setTemplate('widget/grid.phtml')->toHtml();
     }
}