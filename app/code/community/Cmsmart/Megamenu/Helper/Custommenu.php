<?php
/*------------------------------------------------------------------------
* Netbase Mega Menu
* author    CMSMart Team
* copyright: Copyright (c) 2012 http://cmsmart.net. All Rights Reserved.
* @license - http://www.opensource.org/licenses/osl-3.0.php
* Websites: http://cmsmart.net
* Email: team@cmsmart.net
* Technical Support:  Forum - http://cmsmart.net/forum
-------------------------------------------------------------------------*/
class Cmsmart_Megamenu_Helper_Custommenu extends Mage_Core_Helper_Abstract
{
    private $_menuData = null;

    public function saveCurrentCategoryIdToSession()
    {
        $currentCategory = Mage::registry('current_category');
        $currentCategoryId = 0;
        if (is_object($currentCategory)) {
            $currentCategoryId = $currentCategory->getId();
        }
        Mage::getSingleton('catalog/session')
            ->setCustomMenuCurrentCategoryId($currentCategoryId);
    }

    public function initCurrentCategory()
    {
        $currentCategoryId = Mage::getSingleton('catalog/session')->getCustomMenuCurrentCategoryId();
        $currentCategory = null;
        if ($currentCategoryId) {
            $currentCategory = Mage::getModel('catalog/category')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->load($currentCategoryId);
        }
        Mage::unregister('current_category');
        Mage::register('current_category', $currentCategory);
    }

    public function getMenuData()
    {
        if (!is_null($this->_menuData)) return $this->_menuData;
        $blockClassName = Mage::getConfig()->getBlockClassName('megamenu/cusnavigation');
        $block = new $blockClassName();
        $categories = $block->getStoreCategories();
        if (is_object($categories)) $categories = $block->getStoreCategories()->getNodes();
		
		/* comment */
        /* if (Mage::getStoreConfig('custom_menu/general/ajax_load_content')) {
            $_moblieMenuAjaxUrl = str_replace('http:', '', Mage::getUrl('megamenu/ajaxmobilemenucontent'));
            $_menuAjaxUrl = str_replace('http:', '', Mage::getUrl('megamenu/ajaxmenucontent'));
        } else {
            $_moblieMenuAjaxUrl = '';
            $_menuAjaxUrl = '';
        } */
		
		$_moblieMenuAjaxUrl = '';
        $_menuAjaxUrl = '';
		
        $this->_menuData = array(
            '_block'                        => $block,
            '_categories'                   => $categories,
            '_moblieMenuAjaxUrl'            => $_moblieMenuAjaxUrl,
            '_menuAjaxUrl'                  => $_menuAjaxUrl,
            '_showHomeLink'                 => "",
            '_popupWidth'                   => 0,
            '_popupTopOffset'               => 0,
            '_popupDelayBeforeDisplaying'   => 150,
            '_popupDelayBeforeHiding'       => 100,
            '_rtl'                          => 0,
            '_mobileMenuEnabled'            => 1,
            '_mobileMenuWidthInit'          => 991,
        );
        return $this->_menuData;
    }

    public function getMobileMenuContent()
    {
        $menuData = $this->getMenuData();
        extract($menuData);
        if (!$_mobileMenuEnabled) return '';
        // --- Home Link ---
        $homeLinkUrl        = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
        $homeLinkText       = $this->__('Home');
        $homeLink           = '';
        if ($_showHomeLink) {
            $homeLink = <<<HTML
<div id="menu-mobile-0" class="menu-mobile level0">
    <div class="parentMenu">
        <a href="$homeLinkUrl">
            <span>$homeLinkText</span>
        </a>
    </div>
</div>
HTML;
        }
        // --- Menu Content ---
        $mobileMenuContent = '';
        $mobileMenuContentArray = array();
        foreach ($_categories as $_category) {
            $mobileMenuContentArray[] = $_block->drawCustomMenuMobileItem($_category);
        }
        if (count($mobileMenuContentArray)) {
            $mobileMenuContent = implode("\n", $mobileMenuContentArray);
        }
        $textStore = $this->__('Stores');
        $urlStore = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB).'tiendas';
        // --- Result ---
        $menu = <<<HTML
$homeLink
$mobileMenuContent

<!--Add links-->
<div class="menu-mobile level0"><div class="parentMenu"><a href="//labnutrition.com/blog/"><span>Blog</span></a></div></div>
<div class="menu-mobile level0"><div class="parentMenu"><a href="$urlStore"><span>$textStore</span></a></div></div>

HTML;
        return $menu;
    }

    public function getMenuContent()
    {
        $menuData = $this->getMenuData();
        extract($menuData);
        // --- Home Link ---
        $homeLinkUrl        = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
        $homeLinkText       = $this->__('Home');
        $homeLink           = '';
        if ($_showHomeLink) {
            $homeLink = <<<HTML
<div class="menu">
    <div class="parentMenu menu0">
        <a href="$homeLinkUrl">
            <span>$homeLinkText</span>
        </a>
    </div>
</div>
HTML;
    }
        // --- Menu Content ---
        $menuContent = '';
        $menuContentArray = array();
        foreach ($_categories as $_category) {
            $_block->drawCustomMenuItem($_category);
        }
        $topMenuArray = $_block->getTopMenuArray();
        if (count($topMenuArray)) {
            $topMenuContent = implode("\n", $topMenuArray);
        }
        $popupMenuArray = $_block->getPopupMenuArray();
        $popupMenuContent = '';
        if (count($popupMenuArray)) {
            $popupMenuContent = implode("\n", $popupMenuArray);
        }
        // --- Result ---
        $topMenu = <<<HTML
$homeLink
$topMenuContent
HTML;
        return array('topMenu' => $topMenu, 'popupMenu' => $popupMenuContent);
    }
}
