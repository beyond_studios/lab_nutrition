<?php
/*
* Name Extension: Cmsmart megamenu
* Author: The Cmsmart Development Team 
* Date Created: 06/09/2013
* Websites: http://cmsmart.net
* Technical Support: Forum - http://cmsmart.net/support
* GNU General Public License v3 (http://opensource.org/licenses/GPL-3.0)
* Copyright © 2011-2013 Cmsmart.net. All Rights Reserved.
*/
class Cmsmart_Megamenu_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function getAllOptions()
	{
        $this->_options = null;
		if (!$this->_options) {
             $this->_options = Mage::getResourceModel('cms/block_collection')
                 ->load()
                 ->toOptionArray();
             array_unshift($this->_options, array('value'=>'', 'label'=>Mage::helper('catalog')->__('Please select static block ...')));
        }
        return $this->_options;
	}
	
	public function getCategoryArr(){
		$categoryArr = $this->_categotyFilter();
		return $categoryArr; 
	}
	
	public function _categotyFilter()
	{
	    $category_id = $this->getCategory()->getId();		
		$storeId = Mage::app()->getRequest()->getParam('store');
		$categoryFilterCollections = Mage::getModel('megamenu/megamenu')->getCollection()->addFieldToFilter('category_id', $category_id)->getData();


		$categoryFilterCollectionsAll = Mage::getModel('megamenu/megamenu')->getCollection()->addFieldToFilter('category_id', $category_id)->getData();	
			if(count($categoryFilterCollections)>0)
			{
				return $categoryFilterCollections;
			}
			else{
				return $categoryFilterCollectionsAll;
			}

    }
	
	public function _getPosition($cat_id)
	{
		$position = "";
		$collection = Mage::getModel('megamenu/megamenu')->getCollection()
							->addFieldToFilter('category_id', $cat_id);
		foreach($collection as $c){
			$position = $c->getPosition();
			break;
		}
		return $position;
    }
	
	public function getCategory()
    {
        return Mage::registry('current_category');
    }
	
	public function toOptionArray()
    {
        $children = $this->getCategory()->getChildren();	
        $cchildren = explode(",",$children);
        $count=count($cchildren);
        $data = array();
        
        if($children) {
			for($i = 1; $i <= $count; $i++ ) { 
					$data['value'] = $i;
					$data['label'] = $i . ' Columns';
					$dat[]= $data;  		
			}                  
        return $dat;
        } else {
            return;
        }
    }
	
	/* new code  */
	public function getSubCategories($cate_id){
		$childrens = Mage::getModel('catalog/category')->load($cate_id)->getChildrenCategories();
		//return $childrens;
		$cat_arr = array();
		foreach ($childrens as $category){
			$catData = Mage::getModel('catalog/category')->load($category->getId());
			if($category->getIsActive() && $catData->getData('include_in_menu')){
				$cat_arr[] = $category->getData();
			}
		}
		return $cat_arr;
	}
 
	public function getSubCategories_bak($cate_id){
       	$childrens = Mage::getModel('catalog/category')->load($cate_id)->getChildrenCategories();
		//return $childrens;
		$cat_arr = array();
		foreach ($childrens as $category){
			if($category->getIsActive()){
				$cat_arr[] = $category->getData();
			}
		}
		return $cat_arr;
	}
   
	public function getSubCategories111($level){
		$categories = Mage::getModel('catalog/category')
                         ->getCollection()
                         ->addAttributeToSelect('*')
                         ->addAttributeToFilter('level', array('eq'=>$level))
                         ->load();
		return $categories;
	}
	
	public function getStoreId(){
		return Mage::app()->getStore()->getId();
	}
	
	public function isShowHomePage(){
    	return Mage::getStoreConfig('megamenu/mainmenu/home', $this->getStoreId());
	}
	
	public function getMenuType(){
    	return Mage::getStoreConfig('megamenu/mainmenu/menu_type', $this->getStoreId());
	}
	
	public function getRootCatId(){
		return Mage::getStoreConfig('megamenu/mainmenu/root_cat_id', $this->getStoreId());
	}
	
	public function getProductUrl($pid){
		$store = Mage::app()->getStore();
		$path = Mage::getResourceModel('core/url_rewrite')->getRequestPathByIdPath('product/'.$pid, $store);
		$url = $store->getBaseUrl($store::URL_TYPE_WEB) . $path;
		return $url;
	}
	
	public function limit_text($text, $limit) {
      if (str_word_count($text, 0) > $limit) {
          $words = str_word_count($text, 2);
          $pos = array_keys($words);
          $text = substr($text, 0, $pos[$limit]) . '...';
      }
      return $text;
    }
}