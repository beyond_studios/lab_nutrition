<?php
/**
 *
 * @category   MW
 * @package    HelpDesk
 * @author     khanhpn, Mage-World Company <khanhpnk@gmail.com>
 */
class Cmsmart_Megamenu_Model_Label extends Varien_Object
{
    const lblNew		= 'new';
    const lblHot		= 'hot';
    const lblSale		= 'sale';

    static public function getOptionArray()
    {
        return array(
            self::lblNew    => Mage::helper('megamenu')->__('New'),
            self::lblHot 	=> Mage::helper('megamenu')->__('Hot'),
            self::lblSale 	=> Mage::helper('megamenu')->__('Sale')
        );
    }

	public function toOptionArray()
    {	
      	$dur = array();
      	$dur[] = array('value' => '', 'label'=>Mage::helper('megamenu')->__('Please select type label'));
      	$dur[] = array('value' => self::lblNew, 'label'=>Mage::helper('megamenu')->__('New'));
      	$dur[] = array('value' => self::lblHot, 'label'=>Mage::helper('megamenu')->__('Hot'));
      	$dur[] = array('value' => self::lblSale, 'label'=>Mage::helper('megamenu')->__('Sale'));
        return $dur;
    }
}