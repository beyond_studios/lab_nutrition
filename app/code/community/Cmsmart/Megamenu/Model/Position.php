<?php

class Cmsmart_Megamenu_Model_Position extends Varien_Object
{
	const top_menu		= 'top';
    const left_menu		= 'left';
    const both		= 'both';
 
    public function getPosition()
    {	
      	$dur = array();
      	$dur[] = array('value' => '', 'label'=>Mage::helper('megamenu')->__('-- Please select position --'));
      	$dur[] = array('value' => self::top_menu, 'label'=>Mage::helper('megamenu')->__('Top'));
      	$dur[] = array('value' => self::left_menu, 'label'=>Mage::helper('megamenu')->__('Left'));
      	$dur[] = array('value' => self::both, 'label'=>Mage::helper('megamenu')->__('Both'));
		return $dur;
    }
}