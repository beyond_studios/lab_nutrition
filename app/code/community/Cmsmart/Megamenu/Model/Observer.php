<?php
/*
* Name Extension: Cmsmart megamenu
* Author: The Cmsmart Development Team 
* Date Created: 06/09/2013
* Websites: http://cmsmart.net
* Technical Support: Forum - http://cmsmart.net/support
* GNU General Public License v3 (http://opensource.org/licenses/GPL-3.0)
* Copyright © 2011-2013 Cmsmart.net. All Rights Reserved.
*/
class Cmsmart_Megamenu_Model_Observer
{
    
    /**
	 * Adds a block at the end of the content block.
	 * 
	 * Uses the event 'controller_action_layout_load_before'.
	 * 
	 * @param Varien_Event_Observer $observer
	 * @return YourCompany_YourExtension_Model_Observer
	 */
	public function checkModules(Varien_Event_Observer $observer)
	{
		$enable=Mage::getStoreConfig('megamenu/mainmenu/enabled');
		$xmlPath = Mage::getBaseDir('app').DS.'etc'.DS.'modules'.DS.'Cmsmart_Megamenu.xml';
		
		if(file_exists($xmlPath))
		{	
			if($enable){
				$info = simplexml_load_file($xmlPath);
				$info->modules->Cmsmart_Megamenu->active ='true';
				$info->saveXML($xmlPath);
			} else {
				$info = simplexml_load_file($xmlPath);
				$info->modules->Cmsmart_Megamenu->active ='false';
				$info->saveXML($xmlPath);
			}
		}			
		return $this;
	}
    
    public function checkLayouts()
    {
        if (Mage::getStoreConfig('megamenu/mainmenu/enabled'))
        {
            $layout=Mage::app()->getLayout();
            $layout->unsetBlock('catalog.topnav.menu');   
        }
    }
    
	public function saveCategoryTop(Varien_Event_Observer $observer)
   	{
		$dat          = $observer->getRequest()->getParams();
		$categoryId   = $observer->getCategory()->getId();
		$read         = Mage::getSingleton('core/resource')->getConnection('core_read');
		$db           = (string)Mage::getConfig()->getNode('global/resources/default_setup/connection/dbname');
		$tablename    = Mage::getSingleton('core/resource')->getTableName('catalog_category_entity');
		$result       = $read->fetchAll("SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_NAME = '$tablename' AND TABLE_SCHEMA = '$db'");
		$data['category_id'] = $categoryId;
              
		/*** Top Menu ***/
        $data['static_block_top']                      = $dat['static_block_top'];
        $data['static_block_left']                     = $dat['static_block_left'];
        $data['static_block_bottom']                   = $dat['static_block_bottom'];  
        $data['static_block_right']                    = $dat['static_block_right'];
        $data['label']                                 = $dat['label'];
		$data['content_type']                          = $dat['content_type'];
		
		if($dat['position']) {
            $data['position']                = trim($dat['position']);
        } else {
            $data['position']                = "";
        }
		
		if($dat['top_sku']) {
            $data['sku']                = trim($dat['top_sku']);
        } else {
            $data['sku']                = "";
        }
		
		if($dat['left_sku']) {
            $data['left_sku']           = trim($dat['left_sku']);
        } else {
            $data['left_sku']           = "";
        }
		
		if($dat['right_sku']) {
            $data['right_sku']          = trim($dat['right_sku']);
        } else {
            $data['right_sku']          = "";
        }
		
		
		if($dat['label_container']) {
            $data['label_container']                = trim($dat['label_container']);
        } else {
            $data['label_container']                = "";
        }
		
		if($dat['special_class']) {
            $data['special_class']                = trim($dat['special_class']);
        } else {
            $data['special_class']                = "";
        }
		
		if($dat['pgrid_box_title']) {
            $data['pgrid_box_title']                = trim($dat['pgrid_box_title']);
        } else {
            $data['pgrid_box_title']                = "";
        }
		
		if($dat['pgrid_num_columns']) {
            $data['pgrid_num_columns']                = trim($dat['pgrid_num_columns']);
        } else {
            $data['pgrid_num_columns']                = "";
        }
		
		if($dat['pgrid_products']) {
            $data['pgrid_products']                = trim($dat['pgrid_products']);
        } else {
            $data['pgrid_products']                = "";
        }
		
		if($dat['left_sku_title']) {
            $data['left_sku_title']                = trim($dat['left_sku_title']);
        } else {
            $data['left_sku_title']                = "";
        }
		
		if($dat['right_sku_title']) {
            $data['right_sku_title']                = trim($dat['right_sku_title']);
        } else {
            $data['right_sku_title']                = "";
        }
		
		if($dat['pgrid_cats']) {
            $data['pgrid_cats']                = trim($dat['pgrid_cats']);
        } else {
            $data['pgrid_cats']                = "";
        }
		
		if($dat['top_hot_products']) {
            $data['top_hot_products']                = trim($dat['top_hot_products']);
        } else {
            $data['top_hot_products']                = "";
        }
		
		if($dat['top_new_products']) {
            $data['top_new_products']                = trim($dat['top_new_products']);
        } else {
            $data['top_new_products']                = "";
        }
		
		if($dat['top_sale_products']) {
            $data['top_sale_products']                = trim($dat['top_sale_products']);
        } else {
            $data['top_sale_products']                = "";
        }
		
		$data['content_block']                 = $dat['content_block'];
		
		/*** Vertical Menu ***/
        $data['vertical_static_block_top']                      = $dat['vertical_static_block_top'];
        $data['vertical_static_block_left']                     = $dat['vertical_static_block_left'];
        $data['vertical_static_block_bottom']                   = $dat['vertical_static_block_bottom'];  
        $data['vertical_static_block_right']                    = $dat['vertical_static_block_right'];
        $data['vertical_label']                                 = $dat['vertical_label'];
		
		
		if($dat['vertical_sku']) {
            $data['vertical_sku']                = trim($dat['vertical_sku']);
        } else {
            $data['vertical_sku']                = "";
        }
		
		if($dat['vertical_label_container']) {
            $data['vertical_label_container']                = trim($dat['vertical_label_container']);
        } else {
            $data['vertical_label_container']                = "";
        }
		
		if($dat['vertical_special_class']) {
            $data['vertical_special_class']                = trim($dat['vertical_special_class']);
        } else {
            $data['vertical_special_class']                = "";
        }
		
		/* new fields */
		$data['ver_content_type']                          = $dat['ver_content_type'];
		/* zend_debug::dump($data['ver_content_type']);die; */
		if($dat['ver_left_sku']) {
			$data['ver_left_sku']           = trim($dat['ver_left_sku']);
		} else {
			$data['ver_left_sku']           = "";
		}

		if($dat['ver_right_sku']) {
			$data['ver_right_sku']          = trim($dat['ver_right_sku']);
		} else {
			$data['ver_right_sku']          = "";
		}
				
		if($dat['ver_pgrid_box_title']) {
			$data['ver_pgrid_box_title']                = trim($dat['ver_pgrid_box_title']);
		} else {
			$data['ver_pgrid_box_title']                = "";
		}

		if($dat['ver_pgrid_num_columns']) {
			$data['ver_pgrid_num_columns']                = trim($dat['ver_pgrid_num_columns']);
		} else {
			$data['ver_pgrid_num_columns']                = "";
		}

		if($dat['ver_pgrid_products']) {
			$data['ver_pgrid_products']                = trim($dat['ver_pgrid_products']);
		} else {
			$data['ver_pgrid_products']                = "";
		}

		if($dat['ver_left_sku_title']) {
			$data['ver_left_sku_title']                = trim($dat['ver_left_sku_title']);
		} else {
			$data['ver_left_sku_title']                = "";
		}

		if($dat['ver_right_sku_title']) {
			$data['ver_right_sku_title']                = trim($dat['ver_right_sku_title']);
		} else {
			$data['ver_right_sku_title']                = "";
		}

		if($dat['ver_pgrid_cats']) {
			$data['ver_pgrid_cats']                = trim($dat['ver_pgrid_cats']);
		} else {
			$data['ver_pgrid_cats']                = "";
		}

		if($dat['ver_hot_products']) {
			$data['ver_hot_products']                = trim($dat['ver_hot_products']);
		} else {
			$data['ver_hot_products']                = "";
		}

		if($dat['ver_new_products']) {
			$data['ver_new_products']                = trim($dat['ver_new_products']);
		} else {
			$data['ver_new_products']                = "";
		}

		if($dat['ver_sale_products']) {
			$data['ver_sale_products']                = trim($dat['ver_sale_products']);
		} else {
			$data['ver_sale_products']                = "";
		}

		$data['ver_content_block']                 = $dat['ver_content_block'];
		
		/* set icon image for categories */
		if($dat['delete_icon']){
			$data['vertical_cat_icon'] = "";
        } else {
			if(isset($_FILES['uploads']['name']) && $_FILES['uploads']['name'] != '') {
				try {	
					/* Starting upload */	
					$uploader = new Varien_File_Uploader('uploads');
					
					// Any extention would work
					$uploader->setAllowRenameFiles(false);
					
					// Set the file upload mode 
					// false -> get the file directly in the specified folder
					// true -> get the file in the product like folders 
					//	(file.jpg will go in something like /media/f/i/file.jpg)
					$uploader->setFilesDispersion(false);
							
					// We set media as the upload dir
					$path = Mage::getBaseDir('media') . DS .'megamenu' ;
					$file_name = $uploader->getCorrectFileName($_FILES['uploads']['name']);	
					$uploader->save($path, $file_name);
				} catch (Mage_Core_Exception $e) {
				$this->_getSession()->addError($e->getMessage())
					->setProductData($data);
				$redirectBack = true;	
				} catch (Exception $e) {
				   Mage::logException($e);
					$this->_getSession()->addError($e->getMessage());
					$redirectBack = true;
				}	        	        
				$data['vertical_cat_icon'] = 'megamenu/'.$file_name;
			}
		}
		
		/* Mage::log($data, null, 'dat.log', true); */
        $model = Mage::getModel('megamenu/megamenu');
        $model->setData($data);
        $dataorthe = Mage::helper('megamenu')->getCategoryArr();
        if($dataorthe[0]['adminmenutop_id']){
            $id = $dataorthe[0]['adminmenutop_id'];
            $model = Mage::getModel('megamenu/megamenu')->load($id)->addData($data);
            try {
                $model->setId($id)->save();
            } catch(Exception $e) {}
        } else {
    		try
    		 	{
    				$model->save();
    			}
    		catch (Exception $e) {}
        }
	}
}
