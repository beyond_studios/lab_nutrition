<?php

class Cmsmart_Megamenu_Model_Menutype extends Varien_Object
{
    const HORIZONTAL	= 1;
    const VERTICAL		= 2;
    const BOTH			= 3;

    static public function getOptionArray()
    {
        return array(
            self::HORIZONTAL    => Mage::helper('megamenu')->__('Horizontal'),
            self::VERTICAL   => Mage::helper('megamenu')->__('Vertical'),
            self::BOTH   => Mage::helper('megamenu')->__('Both')
        );
    }
	
	static public function toOptionArray()
    {
        return array(
            self::HORIZONTAL => Mage::helper('megamenu')->__('Horizontal'),
            self::VERTICAL   => Mage::helper('megamenu')->__('Vertical'),
            self::BOTH   	 => Mage::helper('megamenu')->__('Both')
        );
    }
}