
<?php
/*
* Name Extension: Megamenu
* Version: 0.1.0
* Author: The Cmsmart Development Team
* Date Created: 16/08/2013
* Websites: http://cmsmart.net
* Technical Support: Forum - http://cmsmart.net/support
* GNU General Public License v3 (http://opensource.org/licenses/GPL-3.0)
* Copyright � 2011-2013 Cmsmart.net. All Rights Reserved.
*/

class Cmsmart_Megamenu_Block_Navigation extends Mage_Catalog_Block_Navigation
{

	public function _prepareLayout()
    {
        if (!Mage::getStoreConfig('megamenu/mainmenu/enabled')) return;
        $this->setTemplate('cmsmart/megamenu/megamenu.phtml');
    }

	/*** new functions ***/
	public function _getProductData($sku){
		return Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
	}

	public function _getCatUrl($cat_id){
		return Mage::getModel('catalog/category')->load($cat_id)->getUrl();
	}

	public function ShowBlock($id){
		return $this->getLayout()->createBlock('cms/block')->setBlockId($id)->toHtml();
	}

	public function getMenuCollection($cat_id){
		return Mage::getModel("megamenu/megamenu")
			->getCollection()
			->addFieldToFilter("category_id", $cat_id);
	}

	/*** Top Menu ***/

	public function _getPosition($cat_id){
		$pos = "";
		$menu = $this->getMenuCollection($cat_id);
		foreach($menu as $m) {
			$pos = $m->getPosition();
			break;
		}
		return $pos;
	}

	public function _getBlockTop($cat_id){
		$menu = $this->getMenuCollection($cat_id);
		foreach($menu as $m) {
			$block_top = $m->getStaticBlockTop();
			if($block_top)
				return $this->ShowBlock($block_top);
		}
		return;
	}

	public function _getBlockRight($cat_id){
		$menu = $this->getMenuCollection($cat_id);
		foreach($menu as $m) {
			$block_right = $m->getStaticBlockRight();
			if($block_right)
				return $this->ShowBlock($block_right);
		}
		return;
	}

	public function _getBlockLeft($cat_id){
		$menu = $this->getMenuCollection($cat_id);
		foreach($menu as $m) {
			$block_left = $m->getStaticBlockLeft();
			if($block_left)
				return $this->ShowBlock($block_left);
		}
		return;
	}

	public function _getBlockBottom($cat_id){
		$menu = $this->getMenuCollection($cat_id);
		foreach($menu as $m) {
			$block_bottom = $m->getStaticBlockBottom();
			if($block_bottom)
				return $this->ShowBlock($block_bottom);
		}
		return;
	}

	public function _getBlockContent($cat_id, $position_menu = Cmsmart_Megamenu_Model_Menutype::HORIZONTAL){
		$menu = $this->getMenuCollection($cat_id);
		foreach($menu as $m) {
			if($position_menu == Cmsmart_Megamenu_Model_Menutype::HORIZONTAL) {
				$blockid = $m->getContentBlock();
			} else {
				$blockid = $m->getVerContentBlock();
			}
			if($blockid != "")
				$block = Mage::getModel('cms/block')->load($blockid);
				return $block->getIdentifier();
		}
		return "";
	}

	public function _getCatLabel($cat_id){
		$menu = $this->getMenuCollection($cat_id);
		foreach($menu as $m) {
			$label = $m->getLabel();
			if($label)
				return $label;
		}
		return;
	}

	public function _getCatLabelImage($cat_id){
		$label = $this->_getCatLabel($cat_id);
		switch ($label) {
			case Cmsmart_Megamenu_Model_Label::lblNew :
				return $this->getSkinUrl('cmsmart/megamenu/images/icon_new.png');
			case Cmsmart_Megamenu_Model_Label::lblHot :
				return $this->getSkinUrl('cmsmart/megamenu/images/icon_hot.png');
			case Cmsmart_Megamenu_Model_Label::lblSale :
				return $this->getSkinUrl('cmsmart/megamenu/images/icon_sale.png');
			default:
			   return 0;
		}
	}

	public function _getCatSku($cat_id){
		$menu = $this->getMenuCollection($cat_id);
		foreach($menu as $m) {
			$sku = $m->getSku();
			if($sku)
				return $sku;
		}
		return;
	}

	public function _getLeftCatSku($cat_id, $position_menu = Cmsmart_Megamenu_Model_Menutype::HORIZONTAL){
		$menu = $this->getMenuCollection($cat_id);
		foreach($menu as $m) {
			if($position_menu == Cmsmart_Megamenu_Model_Menutype::HORIZONTAL) {
				$sku = $m->getLeftSku()."|".$m->getLeftSkuTitle();
			} else {
				$sku = $m->getVerLeftSku()."|".$m->getVerLeftSkuTitle();
			}
			if($sku)
				return $sku;
		}
		return;
	}

	public function _getRightCatSku($cat_id, $position_menu = Cmsmart_Megamenu_Model_Menutype::HORIZONTAL){
		$menu = $this->getMenuCollection($cat_id);
		foreach($menu as $m) {
			if($position_menu == Cmsmart_Megamenu_Model_Menutype::HORIZONTAL) {
				$sku = $m->getRightSku()."|".$m->getRightSkuTitle();
			} else {
				$sku = $m->getVerRightSku()."|".$m->getVerRightSkuTitle();
			}
			if($sku)
				return $sku;
		}
		return;
	}

	public function _getContentType($cat_id, $position_menu = Cmsmart_Megamenu_Model_Menutype::HORIZONTAL){
		$menu = $this->getMenuCollection($cat_id);
		foreach($menu as $m) {
			if($position_menu == Cmsmart_Megamenu_Model_Menutype::HORIZONTAL) {
				$type = $m->getContentType();
			} else {
				$type = $m->getVerContentType();
			}
			if($type)
				return $type;
		}
		return;
	}

	public function _getProductTitle($cat_id){
		$menu = $this->getMenuCollection($cat_id);
		foreach($menu as $m) {
			$lbl_container = $m->getLabelContainer();
			if($lbl_container)
				return $lbl_container;
		}
		return;
	}

	public function _getSpecClass($cat_id){
		$menu = $this->getMenuCollection($cat_id);
		foreach($menu as $m) {
			$spec_class = $m->getSpecialClass();
			if($spec_class)
				return $spec_class;
		}
		return;
	}

	public function _getGridProductTitle($cat_id, $position_menu = Cmsmart_Megamenu_Model_Menutype::HORIZONTAL){
		$menu = $this->getMenuCollection($cat_id);
		foreach($menu as $m) {
			if($position_menu == Cmsmart_Megamenu_Model_Menutype::HORIZONTAL){
				$title = $m->getPgridBoxTitle();
			} else {
				$title = $m->getVerPgridBoxTitle();
			}
			if($title)
				return $title;
		}
		return;
	}

	public function _getGridProductColumn($cat_id){
		$menu = $this->getMenuCollection($cat_id);
		foreach($menu as $m) {
			$col = $m->getPgridNumColumns();
			if($col)
				return $col;
		}
		return;
	}

	public function _getGridProductIds($cat_id, $position_menu = Cmsmart_Megamenu_Model_Menutype::HORIZONTAL){
		$menu = $this->getMenuCollection($cat_id);
		foreach($menu as $m) {
			if($position_menu == Cmsmart_Megamenu_Model_Menutype::HORIZONTAL){
				$ids = $m->getPgridProducts();
			} else {
				$ids = $m->getVerPgridProducts();
			}
			if($ids)
				return $ids;
		}
		return;
	}

	public function _getNumOfCol($cat_id, $position_menu = Cmsmart_Megamenu_Model_Menutype::HORIZONTAL){
		$menu = $this->getMenuCollection($cat_id);
		foreach($menu as $m) {
			if($position_menu == Cmsmart_Megamenu_Model_Menutype::HORIZONTAL) {
				$col = $m->getPgridNumColumns();
			} else {
				$col = $m->getVerPgridNumColumns();
			}

			if($col)
				return $col;
		}
		return;
	}

	public function _getDynamicCat($cat_id, $position_menu = Cmsmart_Megamenu_Model_Menutype::HORIZONTAL){
		$menu = $this->getMenuCollection($cat_id);
		foreach($menu as $m) {
			if($position_menu == Cmsmart_Megamenu_Model_Menutype::HORIZONTAL) {
				$cats = $m->getPgridCats();
			} else {
				$cats = $m->getVerPgridCats();
			}
			if($cats)
				return $cats;
		}
		return;
	}

	public function _getTopLabel($cat_id, $pid, $lbl_type){
		$menu = $this->getMenuCollection($cat_id);
		foreach($menu as $m) {
			if($lbl_type == Cmsmart_Megamenu_Model_Label::lblHot) {
				$p_ids = explode(",", $m->getTopHotProducts());
			} else if($lbl_type == Cmsmart_Megamenu_Model_Label::lblNew) {
				$p_ids = explode(",", $m->getTopNewProducts());
			} else if($lbl_type == Cmsmart_Megamenu_Model_Label::lblSale) {
				$p_ids = explode(",", $m->getTopSaleProducts());
			}
			break;
		}
		if(in_array($pid, $p_ids)) return true;
		return;
	}

	/*** Vertical Menu ***/

	public function _getVerticalBlockTop($cat_id){
		$menu = $this->getMenuCollection($cat_id);
		foreach($menu as $m) {
			$block_top = $m->getVerticalStaticBlockTop();
			if($block_top)
				return $this->ShowBlock($block_top);
		}
		return;
	}

	public function _getVerticalBlockRight($cat_id){
		$menu = $this->getMenuCollection($cat_id);
		foreach($menu as $m) {
			$block_right = $m->getVerticalStaticBlockRight();
			if($block_right)
				return $this->ShowBlock($block_right);
		}
		return;
	}

	public function _getVerticalBlockLeft($cat_id){
		$menu = $this->getMenuCollection($cat_id);
		foreach($menu as $m) {
			$block_left = $m->getVerticalStaticBlockLeft();
			if($block_left)
				return $this->ShowBlock($block_left);
		}
		return;
	}

	public function _getVerticalBlockBottom($cat_id){
		$menu = $this->getMenuCollection($cat_id);
		foreach($menu as $m) {
			$block_bottom = $m->getVerticalStaticBlockBottom();
			if($block_bottom)
				return $this->ShowBlock($block_bottom);
		}
		return;
	}

	public function _getVerticalCatLabel($cat_id){
		$menu = $this->getMenuCollection($cat_id);
		foreach($menu as $m) {
			$label = $m->getVerticalLabel();
			if($label)
				return $label;
		}
		return;
	}

	public function _getVerticalCatLabelImage($cat_id){
		$label = $this->_getVerticalCatLabel($cat_id);
		switch ($label) {
			case Cmsmart_Megamenu_Model_Label::lblNew :
				return $this->getSkinUrl('cmsmart/megamenu/images/icon_new.png');
			case Cmsmart_Megamenu_Model_Label::lblHot :
				return $this->getSkinUrl('cmsmart/megamenu/images/icon_hot.png');
			case Cmsmart_Megamenu_Model_Label::lblSale :
				return $this->getSkinUrl('cmsmart/megamenu/images/icon_sale.png');
			default:
			   return 0;
		}
	}

	public function _getVerLabel($cat_id, $pid, $lbl_type){
		$menu = $this->getMenuCollection($cat_id);
		foreach($menu as $m) {
			if($lbl_type == Cmsmart_Megamenu_Model_Label::lblHot) {
				$p_ids = explode(",", $m->getVerHotProducts());
			} else if($lbl_type == Cmsmart_Megamenu_Model_Label::lblNew) {
				$p_ids = explode(",", $m->getVerNewProducts());
			} else if($lbl_type == Cmsmart_Megamenu_Model_Label::lblSale) {
				$p_ids = explode(",", $m->getVerSaleProducts());
			}
			break;
		}
		if(in_array($pid, $p_ids)) return true;
		return;
	}

	public function _getVerticalCatSku($cat_id){
		$menu = $this->getMenuCollection($cat_id);
		foreach($menu as $m) {
			$sku = $m->getVerticalSku();
			if($sku)
				return $sku;
		}
		return;
	}

	public function _getVerticalProductTitle($cat_id){
		$menu = $this->getMenuCollection($cat_id);
		foreach($menu as $m) {
			$lbl_container = $m->getVerticalLabelContainer();
			if($lbl_container)
				return $lbl_container;
		}
		return;
	}

	public function _getVerticalSpecClass($cat_id){
		$menu = $this->getMenuCollection($cat_id);
		foreach($menu as $m) {
			$spec_class = $m->getVerticalSpecialClass();
			if($spec_class)
				return $spec_class;
		}
		return;
	}

	public function _getVerticalIcon($cat_id){
		$menu = $this->getMenuCollection($cat_id);
		foreach($menu as $m) {
			$icon = $m->getVerticalCatIcon();
			if($icon)
				return $icon;
		}
		return;
	}
}
