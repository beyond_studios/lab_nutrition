<?php
/**
* Name Extension: Megamenu
* Version: 0.1.0
* Author: The Cmsmart Development Team 
* Date Created: 06/09/2013
* Websites: http://cmsmart.net
* Technical Support: Forum - http://cmsmart.net/support
* GNU General Public License v3 (http://opensource.org/licenses/GPL-3.0)
* Copyright � 2011-2013 Cmsmart.net. All Rights Reserved.
*/
class Cmsmart_Megamenu_Block_Catalog_Category_Tabs_Form extends Mage_Adminhtml_Block_Widget_Form
{
	public function getCategory()
    {
        return Mage::registry('current_category');
    }
	
	protected function _prepareForm()
	{
		$level = $this->getCategory()->getLevel();
		$form = new Varien_Data_Form();
		$this->setForm($form);
		$fieldset = $form->addFieldset('megamenu_form', array('legend'=>Mage::helper('megamenu')->__('Mega Menu')));
		$this->setTemplate('cmsmart/megamenu/megamenu.phtml');
		
		$fieldset->addField('contents', 'text',
			array (
			'name' => 'contents',
			'label' => Mage::helper('megamenu')->__('Content'),
			'title' => Mage::helper('megamenu')->__('Content'),
			/* 'style' => 'height:36em;', */
			/* 'config'    => Mage::getSingleton('cms/wysiwyg_config')->getConfig(), */
			));
						
		return parent::_prepareForm();
	}
}
