<?php

class Cmsmart_Megamenu_Block_Adminhtml_Pgrid_Vergrid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('vgridGrid');
      $this->setDefaultSort('pgrid_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
	  $this->setUseAjax(true);
  }

  protected function _prepareCollection()
  {
      /* $collection = Mage::getModel('pgrid/pgrid')->getCollection(); */
		$collection = Mage::getModel('catalog/product')
					->getCollection()
					->addAttributeToSelect('entity_id')
					->addAttributeToSelect('sku')
					->addAttributeToSelect('name');
		$collection->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED);
		$collection->addFieldToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
		$this->setCollection($collection);
		return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
	  $this->addColumn('choose_id', array(
            'header_css_class' => 'a-center',
            'header'    => Mage::helper('adminhtml')->__('Select'),
            'type'      => 'checkbox',
			'index' 	=> 'choose_id',
            'align'     => 'center',
			'width'     => '50px',
			'sortable'  => false,
            'renderer'  => 'megamenu/adminhtml_renderer_vcheckbox',
			'filter_condition_callback' => array($this, '_filterIDCondition'),
      ));
	  
      $this->addColumn('entity_id', array(
          'header'    => Mage::helper('adminhtml')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'entity_id',
      ));

      $this->addColumn('sku', array(
          'header'    => Mage::helper('adminhtml')->__('Sku'),
          'align'     =>'left',
          'index'     => 'sku',
      ));
	  
	  $this->addColumn('name', array(
          'header'    => Mage::helper('adminhtml')->__('Name'),
          'align'     =>'left',
          'index'     => 'name',
      ));

      return parent::_prepareColumns();
  }

    // protected function _prepareMassaction()
    // {
        // $this->setMassactionIdField('pgrid_id');
        // $this->getMassactionBlock()->setFormFieldName('pgrid');

        // $this->getMassactionBlock()->addItem('delete', array(
             // 'label'    => Mage::helper('pgrid')->__('Delete'),
             // 'url'      => $this->getUrl('*/*/massDelete'),
             // 'confirm'  => Mage::helper('pgrid')->__('Are you sure?')
        // ));

        // $statuses = Mage::getSingleton('pgrid/status')->getOptionArray();

        // array_unshift($statuses, array('label'=>'', 'value'=>''));
        // $this->getMassactionBlock()->addItem('status', array(
             // 'label'=> Mage::helper('pgrid')->__('Change status'),
             // 'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             // 'additional' => array(
                    // 'visibility' => array(
                         // 'name' => 'status',
                         // 'type' => 'select',
                         // 'class' => 'required-entry',
                         // 'label' => Mage::helper('pgrid')->__('Status'),
                         // 'values' => $statuses
                     // )
             // )
        // ));
        // return $this;
    // }

  // public function getRowUrl($row)
  // {
      // return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  // }
  
 public function getGridUrl()
 {
   return $this->getUrl('megamenu/adminhtml_pgrid/vgrid', array('_current'=>false));
 }
 
 protected function _filterIDCondition($collection, $column)
    {
       if (!$value = $column->getFilter()->getValue()) {
            return;
        }
       $pids = array();
	   $pids = explode(",", Mage::getSingleton('core/session')->getVerSelectedProducts());
       
       $this->getCollection()->addAttributeToFilter('entity_id', array('in' => $pids));;
    }

}