<?php

class Cmsmart_Megamenu_Block_Adminhtml_Renderer_Vcheckbox extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
    	$result = '';
    	$id = $row['entity_id'];
		$pids = explode(",", Mage::getSingleton('core/session')->getVerSelectedProducts());
		$chk = "";
		
		if(in_array($id, $pids)){
			$chk = ' checked="true"';
		}
		$result = '<input class="checkbox" type="checkbox" value="'.$id.'" '.$chk.' id = "ver_select_'.$id.' name="select_'.$id.'" onClick="myFunctionSelectVer(this)">';
    	return $result;
    }

}