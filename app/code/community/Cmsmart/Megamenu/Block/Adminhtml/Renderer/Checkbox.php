<?php

class Cmsmart_Megamenu_Block_Adminhtml_Renderer_Checkbox extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
    	$result = '';
    	$id = $row['entity_id'];
		$pids = explode(",", Mage::getSingleton('core/session')->getSelectedProducts());
		$chk = "";
		
		if(in_array($id, $pids)){
			$chk = ' checked="true"';
		}
		$result = '<input class="checkbox" type="checkbox" value="'.$id.'" '.$chk.' id = "select_'.$id.' name="select_'.$id.'" onClick="myFunctionSelect(this)">';
    	return $result;
    }

}