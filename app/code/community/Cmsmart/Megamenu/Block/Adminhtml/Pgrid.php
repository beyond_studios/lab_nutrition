<?php
class Cmsmart_Megamenu_Block_Adminhtml_Pgrid extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_pgrid';
    $this->_blockGroup = 'cmsmart_megamenu';
    $this->_headerText = Mage::helper('megamenu')->__('Item Manager');
    $this->_addButtonLabel = Mage::helper('megamenu')->__('Add Item');
    parent::__construct();
  }
}