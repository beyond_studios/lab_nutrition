<?php

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$resource = Mage::getSingleton('core/resource');
$installer->startSetup();

$installer->getConnection()->addColumn($resource->getTableName('megamenu/megamenu'), 'left_sku_title',
    'text DEFAULT ""'
);

$installer->getConnection()->addColumn($resource->getTableName('megamenu/megamenu'), 'right_sku_title',
    'text DEFAULT ""'
);

$installer->endSetup();
