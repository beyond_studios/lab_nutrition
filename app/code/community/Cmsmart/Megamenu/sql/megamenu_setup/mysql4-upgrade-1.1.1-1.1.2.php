<?php

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$resource = Mage::getSingleton('core/resource');
$installer->startSetup();

$installer->getConnection()->addColumn($resource->getTableName('megamenu/megamenu'), 'ver_content_type',
    'int(11) NOT NULL DEFAULT 0'
);

$installer->getConnection()->addColumn($resource->getTableName('megamenu/megamenu'), 'ver_left_sku',
    'varchar(255) DEFAULT ""'
);

$installer->getConnection()->addColumn($resource->getTableName('megamenu/megamenu'), 'ver_right_sku',
    'varchar(255) DEFAULT ""'
);

$installer->getConnection()->addColumn($resource->getTableName('megamenu/megamenu'), 'ver_pgrid_box_title',
    'text DEFAULT ""'
);

$installer->getConnection()->addColumn($resource->getTableName('megamenu/megamenu'), 'ver_pgrid_num_columns',
    'text DEFAULT ""'
);

$installer->getConnection()->addColumn($resource->getTableName('megamenu/megamenu'), 'ver_pgrid_products',
    'text DEFAULT ""'
);

$installer->getConnection()->addColumn($resource->getTableName('megamenu/megamenu'), 'ver_left_sku_title',
    'text DEFAULT ""'
);

$installer->getConnection()->addColumn($resource->getTableName('megamenu/megamenu'), 'ver_right_sku_title',
    'text DEFAULT ""'
);

$installer->getConnection()->addColumn($resource->getTableName('megamenu/megamenu'), 'ver_pgrid_cats',
    'text DEFAULT ""'
);

$installer->getConnection()->addColumn($resource->getTableName('megamenu/megamenu'), 'ver_content_block',
    'int(11) NOT NULL DEFAULT 0'
);

$installer->getConnection()->addColumn($resource->getTableName('megamenu/megamenu'), 'ver_hot_products',
    'text DEFAULT ""'
);

$installer->getConnection()->addColumn($resource->getTableName('megamenu/megamenu'), 'ver_new_products',
    'text DEFAULT ""'
);

$installer->getConnection()->addColumn($resource->getTableName('megamenu/megamenu'), 'ver_sale_products',
    'text DEFAULT ""'
);

$installer->endSetup();
