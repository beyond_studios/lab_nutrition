<?php

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$resource = Mage::getSingleton('core/resource');
$installer->startSetup();

$installer->getConnection()->addColumn($resource->getTableName('megamenu/megamenu'), 'sku',
    'varchar(255) DEFAULT ""'
);
$installer->getConnection()->addColumn($resource->getTableName('megamenu/megamenu'), 'label_container',
    'varchar(255) DEFAULT ""'
);

$installer->endSetup();
