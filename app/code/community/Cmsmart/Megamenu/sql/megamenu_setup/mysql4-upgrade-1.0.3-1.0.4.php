<?php

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$resource = Mage::getSingleton('core/resource');
$installer->startSetup();

$installer->getConnection()->addColumn($resource->getTableName('megamenu/megamenu'), 'vertical_cat_icon',
    'text DEFAULT ""'
);

$installer->endSetup();
