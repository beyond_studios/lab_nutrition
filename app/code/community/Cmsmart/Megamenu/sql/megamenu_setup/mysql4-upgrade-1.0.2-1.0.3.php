<?php

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$resource = Mage::getSingleton('core/resource');
$installer->startSetup();

$installer->getConnection()->addColumn($resource->getTableName('megamenu/megamenu'), 'vertical_static_block_top',
    'int(11) NOT NULL DEFAULT 0'
);

$installer->getConnection()->addColumn($resource->getTableName('megamenu/megamenu'), 'vertical_static_block_left',
    'int(11) NOT NULL DEFAULT 0'
);

$installer->getConnection()->addColumn($resource->getTableName('megamenu/megamenu'), 'vertical_static_block_right',
    'int(11) NOT NULL DEFAULT 0'
);

$installer->getConnection()->addColumn($resource->getTableName('megamenu/megamenu'), 'vertical_static_block_bottom',
    'int(11) NOT NULL DEFAULT 0'
);

$installer->getConnection()->addColumn($resource->getTableName('megamenu/megamenu'), 'vertical_label',
    'varchar(255) NOT NULL DEFAULT ""'
);

$installer->getConnection()->addColumn($resource->getTableName('megamenu/megamenu'), 'vertical_sku',
    'varchar(255) DEFAULT ""'
);

$installer->getConnection()->addColumn($resource->getTableName('megamenu/megamenu'), 'vertical_label_container',
    'varchar(255) DEFAULT ""'
);

$installer->getConnection()->addColumn($resource->getTableName('megamenu/megamenu'), 'vertical_special_class',
    'varchar(255) DEFAULT ""'
);

$installer->endSetup();
