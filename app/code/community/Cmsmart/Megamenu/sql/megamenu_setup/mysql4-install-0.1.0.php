<?php
/*
* Name Extension: Cmsmart megamenu
* Author: The Cmsmart Development Team 
* Date Created: 06/09/2013
* Websites: http://cmsmart.net
* Technical Support: Forum - http://cmsmart.net/support
* GNU General Public License v3 (http://opensource.org/licenses/GPL-3.0)
* Copyright © 2011-2013 Cmsmart.net. All Rights Reserved.
*/
$installer = $this;
$resource = Mage::getSingleton('core/resource');
$installer->startSetup();
$installer->run("
   DROP TABLE IF EXISTS {$resource->getTableName('megamenu/megamenu')};
   CREATE TABLE {$resource->getTableName('megamenu/megamenu')}(
  `adminmenutop_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `static_block_top` int(11) NOT NULL DEFAULT '0',
  `static_block_bottom` int(11) NOT NULL DEFAULT '0',
  `static_block_left` int(11) NOT NULL DEFAULT '0',
  `static_block_right` int(11) NOT NULL DEFAULT '0',
  `label` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (adminmenutop_id), 
  UNIQUE KEY (category_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    ");
$installer->endSetup(); 
