<?php

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$resource = Mage::getSingleton('core/resource');
$installer->startSetup();

$installer->getConnection()->addColumn($resource->getTableName('megamenu/megamenu'), 'top_hot_products',
    'text DEFAULT ""'
);

$installer->getConnection()->addColumn($resource->getTableName('megamenu/megamenu'), 'top_new_products',
    'text DEFAULT ""'
);

$installer->getConnection()->addColumn($resource->getTableName('megamenu/megamenu'), 'top_sale_products',
    'text DEFAULT ""'
);

$installer->endSetup();
