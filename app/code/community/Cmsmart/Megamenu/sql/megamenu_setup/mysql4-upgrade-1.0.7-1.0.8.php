<?php

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$resource = Mage::getSingleton('core/resource');
$installer->startSetup();

$installer->getConnection()->addColumn($resource->getTableName('megamenu/megamenu'), 'pgrid_cats',
    'text DEFAULT ""'
);

$installer->getConnection()->addColumn($resource->getTableName('megamenu/megamenu'), 'content_block',
    'int(11) NOT NULL DEFAULT 0'
);

$installer->getConnection()->addColumn($resource->getTableName('megamenu/megamenu'), 'position',
    'text DEFAULT ""'
);

$installer->endSetup();
