<?php

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */
$resource = Mage::getSingleton('core/resource');
$installer->startSetup();

$installer->getConnection()->addColumn($resource->getTableName('megamenu/megamenu'), 'content_type',
    'int(11) NOT NULL DEFAULT 0'
);

$installer->getConnection()->addColumn($resource->getTableName('megamenu/megamenu'), 'left_sku',
    'varchar(255) DEFAULT ""'
);

$installer->getConnection()->addColumn($resource->getTableName('megamenu/megamenu'), 'right_sku',
    'varchar(255) DEFAULT ""'
);

$installer->endSetup();
