<?php
/**
 * Order Statuses source model
 */
class MercadoPago_Core_Model_Source_Order_Status extends Mage_Adminhtml_Model_System_Config_Source_Order_Status
{
    // set null to enable all possible
    protected $_stateStatuses = array(
        Mage_Sales_Model_Order::STATE_NEW,
        Mage_Sales_Model_Order::STATE_PENDING_PAYMENT,
        Mage_Sales_Model_Order::STATE_PROCESSING,
        Mage_Sales_Model_Order::STATE_COMPLETE,
        Mage_Sales_Model_Order::STATE_CLOSED,
        Mage_Sales_Model_Order::STATE_CANCELED,
        Mage_Sales_Model_Order::STATE_HOLDED,
        //New states
        Janaq_Storepickup_Model_Sales_Order::STATE_APPROVED_PAYMENT,
        Janaq_Storepickup_Model_Sales_Order::STATE_REJECTED_PAYMENT,
        Janaq_Storepickup_Model_Sales_Order::STATE_CHARGEBACK_PAYMENT,
        Janaq_Storepickup_Model_Sales_Order::STATE_REFUNDED_PAYMENT,
    );

    public function toOptionArray()
    {
        if ($this->_stateStatuses) {
            $statuses = Mage::getSingleton('sales/order_config')->getStateStatuses($this->_stateStatuses);
        }
        else {
            $statuses = Mage::getSingleton('sales/order_config')->getStatuses();
        }
        $options = array();
        $options[] = array(
            'value' => '',
            'label' => Mage::helper('adminhtml')->__('-- No Status Change --')
        );
        foreach ($statuses as $code=>$label) {
            $options[] = array(
                'value' => $code,
                'label' => $label
            );
        }
        return $options;
    }

}
