<?php 

ini_set("display_errors", 1);
require_once 'app/Mage.php';
umask(0);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
$_number = 20;
$_productCollection = Mage::getResourceModel('catalog/product_collection');
$now = Mage::getModel('core/date')->date('d-m-Y');
$_now = Mage::getModel('core/date')->date('Y-m-d') . " 00:00:00";

foreach ($_productCollection as $_product) {
	$_product = Mage::getModel('catalog/product')->load($_product->getId());
	$_name = $_product->getName();
	$_price = $_product->getPrice();
	$_spec_price = $_product->getSpecialPrice();
	$_new_price = 0;

	if ($_product->getTypeId() != 'bundle') {

		/*if ($_spec_price == '') {
			$_spec_price = ($_price * 100) / ($price + $_number);
		}*/
		Mage::log($_product->getTypeId());
		Mage::log($_name);
		Mage::log("Precio regular: ". $_price);

		if ($_spec_price == '') {
			/*No existe precio especial*/
			$_new_price = $_price + $_number;
			$_spec_price = $_price;
			$_product->setSpecialFromDate($_now);
			$_product->setSpecialToDate('');
			Mage::log('no tiene precio especial');
		}
		else {
			/*Existe precio especial*/
			Mage::log('si tiene precio especial');

			if ($_product->getSpecialToDate() != '') {
				Mage::log($_product->getSpecialToDate());
				/*Existe fecha fin*/
				$date_to = Mage::getModel('core/date')->gmtDate('d-m-Y', $_product->getSpecialToDate());
				$date_to_format = strtotime($date_to);
				$now_format = strtotime($now);

				if ($date_to_format < $now_format) {
					Mage::log('tiene precio especial vencido');
					//if (floatval($_spec_price) <= floatval($_price)) {
						$_spec_price = $_price;
					//}
					$_new_price = $_price + $_number;
				}

				else{
					$_new_price = $_price;
					Mage::log('tiene precio especial activo');
				}
			}
			else{
				/*No existe fecha fin*/
				if (floatval($_spec_price) == floatval($_price)) {
					$_new_price = $_price + $_number;
				}
				else{
					$_new_price = $_price;
				}
			}

		}
		$_new_price = number_format((float)$_new_price, 4, null, '');
		//$_new_price = $_price + $_number;
		//$_spec_price = number_format((float)$_spec_price, 4, null, '');
		Mage::log("Nuevo precio regular: ". $_new_price);
		Mage::log("Precio especial: ". $_spec_price ."\n");

		$_product->setPrice($_new_price);
		$_product->setSpecialPrice($_spec_price);
		$_product->setSpecialToDate('');
		$_product->save();
	}
}